(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["MainMenu"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
user:Object,
page:String,
cart:Number,
pagename:String},

mounted:function mounted(){
$('#menu_btn').click(function(){
$("#menu_btn .nav-icon").toggleClass('nav_open');
$("#main-menu").toggleClass("on");
$("#menu").toggleClass("on");
$("#menu_body_hide").toggleClass("on");
$("#content").toggleClass("opacity");
$(".menu").each(function(){
$(this).toggleClass("opacity");
});
$('#main-menu-bar').toggleClass('menu-open');
});
}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,"#content {\n  padding-top: 96px;\n}\n#menu-bar {\n  background-color: #fff;\n  width: 100%;\n  height: 96px;\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 1010;\n}\n#menu-bar .top-stripe {\n    position: relative;\n    z-index: 1010;\n    height: 28px;\n    padding-top: 3px;\n    overflow: hidden;\n    background: linear-gradient(-45deg, #DD3433, #C32C2E, #AF2726, #9F221A);\n    background-size: 400% 400%;\n    -webkit-animation: gradient 10s ease infinite;\n            animation: gradient 10s ease infinite;\n}\n#menu-bar .top-stripe p {\n      font-size: 14px;\n}\n#menu-bar .menu-row {\n    position: relative;\n    z-index: 1010;\n    background-color: #fff;\n}\n@-webkit-keyframes gradient {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n@keyframes gradient {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n#main-menu {\n  position: fixed;\n  right: -500px;\n  top: 0;\n  width: 300px;\n  height: 100%;\n  background-color: white;\n  color: #000;\n  z-index: 1000;\n  transition: all 300ms ease;\n}\n#main-menu.on {\n    right: 0;\n}\n#main-menu .menu-inner {\n    margin-top: 60px;\n    padding-top: 50px;\n    text-align: right;\n}\n#main-menu .menu-inner a {\n      color: #000;\n}\n#main-menu .menu-inner a:hover {\n        color: #D72737;\n}\n#main-menu .menu-inner a.nav-link {\n        display: inline-block;\n        font-size: 20px;\n        padding-bottom: 0.3rem;\n        padding-left: 0;\n        padding-right: 0;\n        padding-top: 1rem;\n}\n#main-menu .menu-inner a.nav-link.underline {\n          border-bottom: 2px solid #D72737;\n}\n#main-menu .menu-inner .nav-item {\n      position: relative;\n}\n#main-menu .menu-inner .nav-item.active:after {\n        content: \"\";\n        position: absolute;\n        bottom: 0;\n        right: -3rem;\n        width: 110px;\n        height: 2px;\n        background-color: #D72737;\n}\n#main-menu .menu-inner .menu-inner-bottom {\n      position: absolute;\n      width: 100%;\n      left: 0;\n      bottom: 0;\n      margin-top: 0 -15px 0 -15px;\n      padding: 1rem 1rem 0;\n      background: #2D2F32;\n      box-shadow: 0px -4px 6px -2px rgba(0, 0, 0, 0.1) !important;\n}\n#main-menu .menu-inner .menu-inner-bottom .social-circle {\n        background-color: transparent;\n        color: #fff;\n        padding-top: 4px;\n        border-radius: 100%;\n        width: 35px;\n        font-size: 20px;\n        height: 35px;\n        margin-left: 5px;\n        text-align: center;\n        display: inline-block;\n        transition: all 300ms ease;\n}\n#main-menu .menu-inner .menu-inner-bottom .social-circle:hover {\n          background-color: transparent;\n          color: #D72737;\n}\n.menu-cart {\n  position: absolute;\n  top: 22px;\n  right: calc(1rem + 50px);\n  z-index: 1100;\n  cursor: pointer;\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n}\n.menu-cart i {\n    font-size: 28px;\n}\n.menu-cart .basket-count {\n    position: absolute;\n    top: -7px;\n    right: -6px;\n    border-radius: 100%;\n    background-color: #D82737;\n    color: #fff;\n    text-align: center;\n    width: 18px;\n    height: 18px;\n    font-size: 12px;\n    cursor: pointer;\n}\n#menu_btn {\n  position: absolute;\n  top: 26px;\n  right: 1rem;\n  z-index: 1100;\n  cursor: pointer;\n  display: inline-block;\n  width: 30px;\n  height: 30px;\n  transition: all 300ms ease;\n}\n#menu_btn .nav-icon {\n    width: 30px;\n    height: 30px;\n    position: relative;\n    z-index: 110;\n    cursor: pointer;\n    transition: all 300ms ease;\n}\n#menu_btn .nav-icon span {\n      display: block;\n      position: absolute;\n      border-radius: 0px;\n      height: 3px;\n      width: 100%;\n      background: #1C2D4B;\n      opacity: 1;\n      right: 0;\n      cursor: pointer;\n      transition: all 300ms ease;\n      box-shadow: 0rem 0rem 2rem rgba(0, 0, 0, 0.3) !important;\n}\n#menu_btn .nav-icon span:nth-child(1) {\n      top: 0px;\n}\n#menu_btn .nav-icon span:nth-child(2) {\n      top: 8px;\n      transition: all 300ms ease;\n}\n#menu_btn .nav-icon span:nth-child(3) {\n      top: 16px;\n      width: 60%;\n}\n#menu_btn .nav-icon.nav_open span {\n      border-radius: 10px;\n      background: #1C2D4B;\n}\n#menu_btn .nav-icon.nav_open span:nth-child(1) {\n      top: 9px !important;\n      transform: rotate(135deg) !important;\n}\n#menu_btn .nav-icon.nav_open span:nth-child(2) {\n      opacity: 0 !important;\n      left: 20vw !important;\n}\n#menu_btn .nav-icon.nav_open span:nth-child(3) {\n      width: 100%;\n      top: 9px !important;\n      transform: rotate(-135deg) !important;\n}\n#menu_btn:hover .nav-icon span:nth-child(3) {\n    width: 100%;\n}\n#menu_btn.on {\n    background-color: #e83e8c;\n}\n#menu_btn.dark .nav-icon span {\n    background: #F6CF3C;\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {\n.menu .menu-dropdown-holder .menu-dropdown a {\n    font-size: 1.1rem;\n}\n}\n@media only screen and (max-width: 767px) {\n.menu .menu-links .menu-item {\n    font-size: 4.5vw;\n}\n#menu-bar .top-stripe {\n    padding-top: 0;\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/menus/MainMenu.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/menus/MainMenu.vue ***!
  \****************************************************/
/***/function resourcesJsComponentsMenusMainMenuVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./MainMenu.vue?vue&type=template&id=01c30dc4& */"./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4&");
/* harmony import */var _MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./MainMenu.vue?vue&type=script&lang=js& */"./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js&");
/* harmony import */var _MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__.render,
_MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/menus/MainMenu.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/***/function resourcesJsComponentsMenusMainMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************/
/***/function resourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4& ***!
  \***********************************************************************************/
/***/function resourcesJsComponentsMenusMainMenuVueVueTypeTemplateId01c30dc4(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_01c30dc4___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=template&id=01c30dc4& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/menus/MainMenu.vue?vue&type=template&id=01c30dc4& ***!
  \**************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeTemplateId01c30dc4(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"nav",
{staticClass:"container-fluid",attrs:{id:"menu-bar"}},
[
_c("div",{staticClass:"row top-stripe text-white px-3 mob-px-0"},[
_vm._m(0),
_vm._v(" "),
_c("div",{staticClass:"col-4 text-right"},[
_vm.user!=null?
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{staticClass:"text-white",attrs:{href:"/dashboard"}},
[
_vm._v("Hi "+_vm._s(_vm.user.first_name)),
_c("i",{staticClass:"fa fa-user-circle ml-2"})])]):



_c("p",{staticClass:"mb-0"},[_vm._m(1)])])]),


_vm._v(" "),
_c("div",{staticClass:"row menu-row shadow px-3 mob-px-0"},[
_c("div",{staticClass:"col-8"},[
_c("a",{attrs:{href:"/"}},[
_vm.page!="light"?
_c("img",{
staticClass:"img-fluid my-3",
attrs:{
id:"top-logo",
src:"/img/logos/logo-long.svg",
alt:"Tom Morrison logo",
width:"162",
height:"35"}}):


_c("img",{
staticClass:"img-fluid my-3",
attrs:{
id:"top-logo",
src:"/img/logos/logo-white-long.svg",
alt:"Tom Morrison logo",
width:"162",
height:"35"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4"},[
_c("a",{staticClass:"menu-cart",attrs:{href:"/basket"}},[
_c("i",{staticClass:"fa fa-shopping-cart text-dark"}),
_vm._v(" "),
_vm.cart>0?
_c("div",{staticClass:"basket-count"},[
_vm._v(_vm._s(_vm.cart))]):

_vm._e()]),

_vm._v(" "),
_vm._m(2)])]),


_vm._v(" "),
_c("div",{staticClass:"shadow",attrs:{id:"main-menu"}},[
_c("div",{staticClass:"menu-inner pr-5"},[
_c(
"div",
{
staticClass:"nav-item",
class:{active:_vm.page=="Homepage"}},

[_vm._m(3)]),

_vm._v(" "),
_c(
"div",
{staticClass:"nav-item",class:{active:_vm.page=="Shop"}},
[_vm._m(4)]),

_vm._v(" "),
_c(
"div",
{staticClass:"nav-item",class:{active:_vm.page=="Merch"}},
[_vm._m(5)]),

_vm._v(" "),
_c(
"div",
{
staticClass:"nav-item",
class:{active:_vm.page=="Seminars"}},

[_vm._m(6)]),

_vm._v(" "),
_c(
"div",
{staticClass:"nav-item",class:{active:_vm.page=="Facts"}},
[_vm._m(7)]),

_vm._v(" "),
_c(
"div",
{staticClass:"nav-item",class:{active:_vm.page=="About"}},
[_vm._m(8)]),

_vm._v(" "),
_c(
"div",
{staticClass:"nav-item",class:{active:_vm.page=="Blog"}},
[_vm._m(9)]),

_vm._v(" "),
_c(
"div",
{
staticClass:"nav-item",
class:{
active:_vm.page=="Reviews - Simplistic Mobility Method"}},


[_vm._m(10)]),

_vm._v(" "),
_c(
"div",
{
staticClass:"nav-item",
class:{active:_vm.page=="Contact"}},

[_vm._m(11)]),

_vm._v(" "),
_vm._m(12)])])]);




};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-8"},[
_c("p",{staticClass:"mb-0"},[
_vm._v("Have you done your SMM today?")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("a",{staticClass:"text-white",attrs:{href:"/login"}},[
_vm._v("Login "),
_c("i",{staticClass:"fa fa-sign-in ml-2"})]);

},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{attrs:{id:"menu_btn"}},[
_c("div",{staticClass:"nav-icon"},[
_c("span"),
_c("span"),
_c("span")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/"}},[
_vm._v("Home")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/shop"}},[
_vm._v("Shop")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/merch"}},[
_vm._v("Merch")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/seminars"}},[
_vm._v("Seminars")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/facts"}},[
_vm._v("Tom Facts")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/about"}},[
_vm._v("About Us")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/blog"}},[
_vm._v("Blog")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"nav-link",
attrs:{href:"/simplistic-mobility-method-reviews"}},

[_vm._v("Reviews")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("p",{staticClass:"mb-0"},[
_c("a",{staticClass:"nav-link",attrs:{href:"/contact"}},[
_vm._v("Get In Touch")])]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"menu-inner-bottom"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-12"},[
_c("p",{staticClass:"text-right"},[
_c(
"a",
{
attrs:{
href:"https://www.facebook.com/Movementandmotion",
target:"_blank",
rel:"noopener",
"aria-label":"Facebook"}},


[
_c("span",{staticClass:"social-circle"},[
_c("i",{staticClass:"fa fa-facebook"})])]),



_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://www.tiktok.com/@tom_morrison",
target:"_blank",
rel:"noopener",
"aria-label":"TikTok"}},


[
_c("span",{staticClass:"social-circle"},[
_c("img",{
attrs:{
src:"/img/icons/tiktok-brands.svg",
width:"18",
height:"18",
alt:"tom morrison tiktok"}})])]),





_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://www.instagram.com/tom.morrison.training",
target:"_blank",
rel:"noopener",
"aria-label":"Instagram"}},


[
_c("span",{staticClass:"social-circle"},[
_c("i",{staticClass:"fa fa-instagram"})])]),



_vm._v(" "),
_c(
"a",
{
attrs:{
href:"https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A",
target:"_blank",
rel:"noopener",
"aria-label":"YouTube"}},


[
_c("span",{staticClass:"social-circle"},[
_c("i",{staticClass:"fa fa-youtube-play"})])])])])])]);







}];

_render._withStripped=true;



/***/}}]);

}());
