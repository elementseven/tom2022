(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["BundlesStabilityBuilder"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()}};



/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".no-webp .intermediate-bundle-card {\n  background-image: url(\"/img/bundles/intermediate-bundle-card.jpg\");\n}\n.no-webp .complete-bundle-card {\n  background-image: url(\"/img/bundles/complete-bundle-card.jpg\");\n}\n.webp .intermediate-bundle-card {\n  background-image: url(\"/img/bundles/intermediate-bundle-card.webp\");\n}\n.webp .complete-bundle-card {\n  background-image: url(\"/img/bundles/complete-bundle-card.webp\");\n}\n.owl-carousel .owl-stage-outer {\n  overflow: visible !important;\n}\n.owl-dots {\n  margin-left: 100px;\n}\n#smm-bundles .bundle-card {\n  position: relative;\n  background-size: cover;\n  background-position: center center;\n  background-repeat: no-repeat;\n  min-height: 365px;\n  min-width: 320px;\n  overflow: hidden;\n  cursor: pointer;\n  border-radius: 0.25rem;\n}\n#smm-bundles .bundle-card .bundle-info {\n    position: absolute;\n    height: 100%;\n    width: 100%;\n    z-index: 2;\n    cursor: pointer;\n    color: #fff;\n}\n#smm-bundles .bundle-card .bundle-info .bundle-name {\n      font-family: \"Oswald\", sans-serif;\n      font-size: 2rem;\n      line-height: 1.2;\n      cursor: pointer;\n}\n#smm-bundles .bundle-card .bundle-info .bundle-short {\n      line-height: 1.3;\n}\n#smm-bundles .bundle-card .bundle-info .bundles-full-price {\n      position: absolute;\n      bottom: 2.2rem;\n      left: 1rem;\n      width: calc(100% - 2rem);\n      font-family: \"Oswald\", sans-serif;\n      font-size: 1.4rem;\n      font-weight: 300;\n      line-height: 1.2;\n      cursor: pointer;\n}\n#smm-bundles .bundle-card .bundle-info .bundle-price {\n      position: absolute;\n      bottom: 1rem;\n      left: 1rem;\n      width: calc(100% - 2rem);\n      font-family: \"Oswald\", sans-serif;\n      font-size: 1.2rem;\n      line-height: 1.2;\n      cursor: pointer;\n}\n#smm-bundles .bundle-card .bundle-info .bundle-price .only {\n        font-size: 0.8rem;\n}\n#smm-bundles .bundle-card .bundle-info .bundle-price .learn-more {\n        float: right;\n        margin-top: 5px;\n        font-size: 1rem;\n        cursor: pointer;\n        font-family: \"Karla\", sans-serif;\n}\n#smm-bundles .bundle-card .bundle-info .fa-star {\n      color: #F8D21B;\n}\n#smm-bundles .bundle-card .special-offer {\n    position: absolute;\n    bottom: 0.5rem;\n    left: 1rem;\n    display: inline-block;\n    text-align: center;\n    width: 85px;\n    height: 25px;\n    font-size: 16px;\n    font-weight: 900;\n    background-color: #D72737;\n    color: #fff;\n    border-radius: 15px;\n}\n#smm-bundles .bundle-card .most-popular {\n    text-align: center;\n    display: inline-block;\n    height: auto;\n    padding: 3px 8px;\n    font-weight: 700;\n    border-radius: 30px;\n    font-size: 14px;\n    background-color: #D6F6E7;\n    color: #00BC0D;\n}\n#smm-bundles .bundle-card .most-popular .most-popular-check {\n      width: 17px !important;\n      margin-top: -2px;\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/shop/BundlesStabilityBuilder.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/shop/BundlesStabilityBuilder.vue ***!
  \******************************************************************/
/***/function resourcesJsComponentsShopBundlesStabilityBuilderVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295& */"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295&");
/* harmony import */var _BundlesStabilityBuilder_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./BundlesStabilityBuilder.vue?vue&type=script&lang=js& */"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js&");
/* harmony import */var _BundlesStabilityBuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_BundlesStabilityBuilder_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__.render,
_BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/shop/BundlesStabilityBuilder.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/function resourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BundlesStabilityBuilder.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************/
/***/function resourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295& ***!
  \*************************************************************************************************/
/***/function resourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeTemplateId0cac6295(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BundlesStabilityBuilder_vue_vue_type_template_id_0cac6295___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/shop/BundlesStabilityBuilder.vue?vue&type=template&id=0cac6295& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsShopBundlesStabilityBuilderVueVueTypeTemplateId0cac6295(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{staticClass:"container pb-5",attrs:{id:"smm-bundles"}},
[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-12 half_col d-lg-none"},[
_c(
"div",
{staticStyle:{width:"320px"}},
[
_c(
"carousel",
{
attrs:{
items:1,
margin:15,
center:false,
autoHeight:false,
autoWidth:false,
nav:false,
dots:true,
loop:true}},


[
_c(
"div",
{
staticClass:
"card border-0 shadow bundle-card intermediate-bundle-card"},

[
_c("div",{staticClass:"bundle-info p-3"},[
_c(
"div",
{staticClass:"special-offer mb-2 mx-auto"},
[_vm._v("Save 17%")]),

_vm._v(" "),
_c("div",{staticClass:"most-popular mb-1"},[
_c("img",{
staticClass:"most-popular-check d-inline mr-1",
attrs:{
src:"/img/icons/list-check-dark-green.svg",
width:"17",
height:"15"}}),


_vm._v("The Next Step")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-name mimic-h2 mb-2"},[
_vm._v("Intermediate Bundle")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-short"},[
_c("i",[
_vm._v("Includes:"),
_c("br"),
_vm._v("Stability Builder™"),
_c("br"),
_vm._v("End Range Training")])]),


_vm._v(" "),
_c("p",{staticClass:"bundles-full-price"},[
_c("s",[_vm._v("£166.95")]),
_c("br"),
_c("b",[_vm._v("Only £138")])]),

_vm._v(" "),
_c("p",{staticClass:"bundle-price mb-0"},[
_c(
"a",
{
staticClass:"learn-more text-right",
attrs:{href:"/products/intermediate-bundle"}},

[
_vm._v("Learn More "),
_c("i",{
staticClass:"fa fa-angle-double-right"})])])])]),







_vm._v(" "),
_c(
"div",
{
staticClass:
"card border-0 shadow bundle-card complete-bundle-card"},

[
_c("div",{staticClass:"bundle-info p-3"},[
_c(
"div",
{staticClass:"special-offer mb-2 mx-auto"},
[_vm._v("Save 23%")]),

_vm._v(" "),
_c("div",{staticClass:"most-popular mb-1"},[
_c("img",{
staticClass:"most-popular-check d-inline mr-1",
attrs:{
src:"/img/icons/list-check-dark-green.svg",
width:"17",
height:"15"}}),


_vm._v("Biggest Discount")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-name mimic-h2 mb-2"},[
_vm._v("Complete Bundle")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-short"},[
_c("i",[
_vm._v("Includes:"),
_c("br"),
_vm._v("Simplistic Mobility Method®"),
_c("br"),
_vm._v("Ultimate Core"),
_c("br"),
_vm._v("Where I Went Wrong"),
_c("br"),
_vm._v("Stability Builder™"),
_c("br"),
_vm._v("End Range Training")])]),


_vm._v(" "),
_c("p",{staticClass:"bundles-full-price"},[
_c("s",[_vm._v("£256.92")]),
_c("br"),
_c("b",[_vm._v("Only £199")])]),

_vm._v(" "),
_c("p",{staticClass:"bundle-price mb-0"},[
_c(
"a",
{
staticClass:"learn-more text-right",
attrs:{href:"/products/complete-bundle"}},

[
_vm._v("Learn More "),
_c("i",{
staticClass:"fa fa-angle-double-right"})])])])])])],










1)]),


_vm._v(" "),
_vm._m(0),
_vm._v(" "),
_vm._m(1)])]);



};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-lg-5 mb-3 d-none d-lg-block"},[
_c("a",{attrs:{href:"/products/intermediate-bundle"}},[
_c(
"div",
{
staticClass:
"card border-0 shadow bundle-card intermediate-bundle-card"},

[
_c("div",{staticClass:"bundle-info p-3"},[
_c("div",{staticClass:"special-offer mb-2 mx-auto"},[
_vm._v("Save 17%")]),

_vm._v(" "),
_c("div",{staticClass:"most-popular mb-1"},[
_c("img",{
staticClass:"most-popular-check d-inline mr-1",
attrs:{
src:"/img/icons/list-check-dark-green.svg",
width:"17",
height:"15"}}),


_vm._v("The Next Step")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-name mimic-h2 mb-2"},[
_vm._v("Intermediate Bundle")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-short"},[
_c("i",[
_vm._v("Includes:"),
_c("br"),
_vm._v("Stability Builder™"),
_c("br"),
_vm._v("End Range Training")])]),


_vm._v(" "),
_c("p",{staticClass:"bundles-full-price"},[
_c("s",[_vm._v("£166.95")]),
_c("br"),
_c("b",[_vm._v("Only £138")])]),

_vm._v(" "),
_c("p",{staticClass:"bundle-price mb-0"},[
_c("span",{staticClass:"learn-more text-right"},[
_vm._v("Learn More "),
_c("i",{staticClass:"fa fa-angle-double-right"})])])])])])]);







},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"col-lg-5 mb-3 d-none d-lg-block"},[
_c("a",{attrs:{href:"/products/complete-bundle"}},[
_c(
"div",
{
staticClass:
"card border-0 shadow bundle-card complete-bundle-card"},

[
_c("div",{staticClass:"bundle-info p-3"},[
_c("div",{staticClass:"special-offer mb-2 mx-auto"},[
_vm._v("Save 23%")]),

_vm._v(" "),
_c("div",{staticClass:"most-popular mb-1"},[
_c("img",{
staticClass:"most-popular-check d-inline mr-1",
attrs:{
src:"/img/icons/list-check-dark-green.svg",
width:"17",
height:"15"}}),


_vm._v("Biggest Discount")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-name mimic-h2 mb-2"},[
_vm._v("Complete Bundle")]),

_vm._v(" "),
_c("p",{staticClass:"bundle-short"},[
_c("i",[
_vm._v("Includes:"),
_c("br"),
_vm._v("Simplistic Mobility Method®"),
_c("br"),
_vm._v("Ultimate Core"),
_c("br"),
_vm._v("Where I Went Wrong"),
_c("br"),
_vm._v("Stability Builder™"),
_c("br"),
_vm._v("End Range Training")])]),


_vm._v(" "),
_c("p",{staticClass:"bundles-full-price"},[
_c("s",[_vm._v("£256.92")]),
_c("br"),
_c("b",[_vm._v("Only £199")])]),

_vm._v(" "),
_c("p",{staticClass:"bundle-price mb-0"},[
_c("span",{staticClass:"learn-more text-right"},[
_vm._v("Learn More "),
_c("i",{staticClass:"fa fa-angle-double-right"})])])])])])]);







}];

_render._withStripped=true;



/***/}}]);

}());
