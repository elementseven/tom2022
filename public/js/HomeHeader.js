(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["HomeHeader"],{

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHeaderVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".no-webp .new-home-smm {\n  background-image: url(\"/img/home/new-home-smm.jpg\");\n}\n.no-webp .new-home-bb {\n  background-image: url(\"/img/home/new-home-bb.jpg\");\n}\n.webp .new-home-smm {\n  background-image: url(\"/img/home/new-home-smm.webp\");\n}\n.webp .new-home-bb {\n  background-image: url(\"/img/home/new-home-bb.webp\");\n}\n#home-header .full-height-minus-menu {\n  height: calc(100vh - 96px);\n}\n#home-header .or {\n  position: absolute;\n  left: -27px;\n  top: calc(50% - 20px);\n  width: 54px;\n  height: 40px;\n  background-color: white;\n  border-radius: 3px;\n  color: black;\n  font-size: 23px;\n  line-height: 23px;\n  font-weight: 700;\n  text-align: center;\n  padding-top: 9px;\n}\n#home-header .special-offer {\n  display: inline-block;\n  text-align: center;\n  width: 130px;\n  height: 33px;\n  font-weight: 900;\n  background-color: #D72737;\n  color: #fff;\n  padding-top: 3px;\n  border-radius: 15px;\n}\n@media only screen and (max-width: 767px) {\n#home-header .full-height-minus-menu {\n    height: calc(50vh - 51px);\n}\n#home-header p.mimic-h2 {\n    font-size: 8vw;\n    line-height: 1.4;\n}\n#home-header p.mimic-h2 .tm {\n      font-size: 5vw;\n      top: -14px;\n}\n#home-header .or {\n    top: -20px;\n    left: calc(50% - 20px);\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHeaderVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Header.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/home/Header.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/home/Header.vue ***!
  \*************************************************/
/***/function resourcesJsComponentsHomeHeaderVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./Header.vue?vue&type=template&id=6fe35e62& */"./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62&");
/* harmony import */var _Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./Header.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script={};



/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
script,
_Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__.render,
_Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/home/Header.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/***/function resourcesJsComponentsHomeHeaderVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Header.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62& ***!
  \********************************************************************************/
/***/function resourcesJsComponentsHomeHeaderVueVueTypeTemplateId6fe35e62(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_6fe35e62___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Header.vue?vue&type=template&id=6fe35e62& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/home/Header.vue?vue&type=template&id=6fe35e62& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHeaderVueVueTypeTemplateId6fe35e62(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"header",
{
staticClass:"container-fluid text-center",
attrs:{id:"home-header"}},

[
_c("div",{staticClass:"row"},[
_c(
"div",
{staticClass:"col-lg-6 full-height-minus-menu new-home-smm bg"},
[
_c("div",{staticClass:"d-table w-100 h-100"},[
_c(
"div",
{staticClass:"d-table-cell align-middle w-100 h-100"},
[
_c("p",{staticClass:"text-white mb-0"},[
_c("i",[_vm._v("I want to improve my mobility:")])]),

_vm._v(" "),
_c("p",{staticClass:"mimic-h2 text-white"},[
_vm._v("The Simplistic"),
_c("br",{staticClass:"d-md-none"}),
_vm._v(" Mobility Method"),
_c("span",{staticClass:"tm"},[_vm._v("®")])]),

_vm._v(" "),
_c(
"a",
{
attrs:{
href:"/products/the-simplistic-mobility-method"}},


[
_c(
"button",
{
staticClass:"btn btn-white-outline mx-auto",
attrs:{type:"button"}},

[
_vm._v("Find out more "),
_c("i",{
staticClass:"fa fa-angle-double-right ml-2"})])])])])]),










_vm._v(" "),
_c(
"div",
{
staticClass:
"col-lg-6 full-height-minus-menu new-home-bb bg position-relative"},

[
_c("div",{staticClass:"d-table w-100 h-100"},[
_c(
"div",
{staticClass:"d-table-cell align-middle w-100 h-100"},
[
_c("div",{staticClass:"special-offer mb-2 mx-auto"},[
_vm._v("Save 12%")]),

_vm._v(" "),
_c("p",{staticClass:"text-white mb-0"},[
_c("i",[
_vm._v("I want SMM + bonus programs & inspiration:")])]),


_vm._v(" "),
_c("p",{staticClass:"mimic-h2 text-white"},[
_vm._v("Beginner's Bundle")]),

_vm._v(" "),
_c("a",{attrs:{href:"/products/beginners-bundle"}},[
_c(
"button",
{
staticClass:"btn btn-white-outline mx-auto",
attrs:{type:"button"}},

[
_vm._v("Find out more "),
_c("i",{
staticClass:"fa fa-angle-double-right ml-2"})])])])]),







_vm._v(" "),
_c("div",{staticClass:"or"},[_vm._v("OR")])])])]);





}];

_render._withStripped=true;



/***/}}]);

}());
