@php
$page = "Merch";
$pagename = 'Merch';
$pagetitle = "Merchandise - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison Merchandise";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('head_section')
{{-- <script>
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'GBP',
    'impressions': [
	    @foreach($products as $key => $product)
	   {
	      "id": "{{$product->id}}",
	     'sku': "{{$product->id}}",
	     'name': "{{$product->name}}",
	     'brand': 'Tom Morrison',
	     'category': "{{$product->category->name}}",
	     'price': '{{$product->price}}',
	     'position': {{$key + 1}}
	   } @if($key != count($products) - 1) , @endif
	   @endforeach
	]
  }
});
</script> --}}
@endsection
@section('content')
<div class="container py-5">
	<div class="row">
		<div class="col-lg-4 col-md-6 mb-5 product-holder">
    	<img src="{{$merch->product->sync_product->thumbnail_url}}" class="w-100" alt="{{$merch->product->sync_product->name}} image"/>
    </div>
    <div class="col-lg-8">
      <h1 class="mb-2 scroll">{{$merch->product->sync_product->name}}</h1>
      @php
      $price = 100000;
      foreach($merch->variants as $v){
      	if(doubleval($v->price) < $price){
      		$price = doubleval($v->price);
      	}
      }
      @endphp
      <p class="text-primary"><b>&pound;{{number_format($price, 2)}}</b></p>
      <form action="{{route('printful-add-to-basket')}}" class="row half_row">
        
        <div class="col-lg-4 col-6 half_col mb-3">
          <label for="variant"><b>Select Size:</b></label>
          <select id="variant" class="form-control" name="variant">
            @foreach($merch->variants as $v)
            <option value="{{$v->id}}">{{$v->size}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-lg-4 col-6 half_col mb-3">
          <label for="quantity"><b>Quantity:</b></label>
          <select id="quantity" class="form-control" name="quantity">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
          </select>
        </div>
        <div class="col-lg-4 half_col pt-3 mt-1 mob-pt-0 mob-mt-0">
          <button type="submit" class="btn btn-primary mt-3">Add to basket</button>
        </div>
        <div class="col-12 half_col">
          <hr class="my-4">
        </div>
      </form>
      {!!$merch->description!!}
      <form action="{{route('printful-add-to-basket')}}" class="row half_row">
        <div class="col-12 half_col">
          <hr class="my-4">
        </div>
        <div class="col-lg-4 col-6 half_col mb-3">
          <label for="variant"><b>Select Size:</b></label>
          <select id="variant" class="form-control" name="variant">
            @foreach($merch->variants as $v)
            <option value="{{$v->id}}">{{$v->size}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-lg-4 col-6 half_col mb-3">
          <label for="quantity"><b>Quantity:</b></label>
          <select id="quantity" class="form-control" name="quantity">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
          </select>
        </div>
        <div class="col-lg-4 half_col pt-3 mt-1 mob-pt-0 mob-mt-0">
          <button type="submit" class="btn btn-primary mt-3">Add to basket</button>
        </div>
      </form>
    </div>
	</div>
</div>
@endsection