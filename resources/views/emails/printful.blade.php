<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>

				<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">{{$subject}}</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$name}},</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">We have an update on your Tom Morrison merchandise order, details below:</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px; text-transform: capitalize;"><b>Order Status:</b> {{$order_status}}</p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0 auto 25px;"><b>Order ID:</b> {{$order_id}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>Items:</b></p>
				@foreach($items as $item)
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0px auto 10px;">{{$item['name']}} <span style="font-size:12px">x{{$item['quantity']}}</span></p>
				@endforeach

				@if($shipment)
				<hr style="margin: 40px auto 0;">
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 10px;"><b>Shipping Information:</b></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0px auto 5px;"><b>Carrier</b> - {{$shipment['carrier']}} - {{$shipment['service']}}</p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0px auto 5px;"><b>Ship date</b> - {{$shipment['ship_date']}}</p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0px auto;"><b>Tracking Number</b> - <a href="{{$shipment['tracking_url']}}" style="color:#D82737;">{{$shipment['tracking_number']}}</a></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 0px auto 25px;"><a href="{{$shipment['tracking_url']}}" style="background-color: #D82737; color:#ffffff !important; width: 200px;padding: 15px; text-align: center; display: inline-block; border-radius: 5px; margin-top: 20px; text-decoration: none;">Track your package</a></p>

				@endif
				<p style="font-size:18px; color:#2c2c2c; text-align: left; margin-top: 25px; font-family:'Arial', arial, sans-serif;">Don't forget you can track the status of your order in your <a href="https://tommorrison.uk/dashboard/orders" style="color:#D82737;">dashboard</a> at any time! If you have any questions about your order, just reply to this email and we'll get back to you as soon as we can!</p>

				</td>
				</tr>
				<tr><td>
					<hr style="margin: 30px auto 0;">
				<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>