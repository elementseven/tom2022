<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>

				<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Account Created</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$user->first_name}},</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">When you buy a gift for someone, you also need an account on our system so we can store the purchase information.<br><br>So, your account is now active! You can login using the details below and you can use this for any future purchases.</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">But don't worry if you don't see any products in there! The gift you purchased has been added to the recipients account, they will get an email to create their own account & login details."</p>
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto 15px;"><b style="width: 220px; display: inline-block;">Your Account Details:</b></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b style="width: 220px; display: inline-block;">Email:</b> {{$user->email}}</p>

				<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">(You can change your password at any time in the <a href="https://tommorrison.uk/dashboard/account-management" style="color: #D82737;"><u>'My Account'</u></a> section of your dashboard)</b></p>

				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for your purchase and support!</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Kind Regards,<br><br><b>Tom Morrison</b></p>
				
				</td>
				</tr>
				<tr><td>
				<hr style="margin: 30px auto 0;">
				<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>