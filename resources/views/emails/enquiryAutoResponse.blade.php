<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
</head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; font-weight:900; margin-bottom: 20px;"><b>Thanks for your email!</b></p>
				<p>We get a very high volume of messages & we'll aim to get back to you within 3-5 working days, if not please send us a follow up email!<br/></p>

				</td>
				</tr>
				<tr>
				<td style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">
				<p style="font-size:18px; margin-bottom:5px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; font-weight:900;"><br/><br/><b>Your message:</b></p>

				{!! nl2br($content) !!}

				<hr style="margin: 30px auto 0;">

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><br/><br/>Have an outstanding day!<br/>- Tom & Jenni!</p>

				</td>
				</tr>
				<tr><td>
					<hr style="margin: 30px auto 0;">
				<img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="150" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>