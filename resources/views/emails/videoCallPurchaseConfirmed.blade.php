<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100" alt="Tom Morrison Logo"/></p>

				<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Purchase Confirmed</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi Tom,</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$invoice->user->full_name}} has purchased a video call on the Tom Morrison website.<br><b>The reference number is <span style="color:#D82737;">{{$invoice->transaction_id}}</span></b>.</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Please check the details of the purchase below.</p>
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Name:</b> {{$user->first_name}} {{$user->last_name}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Email:</b> {{$user->email}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Country:</b> {{$user->country}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Products:</b><br> @foreach($invoice->products as $p) {{$p->name}} <br> @endforeach</p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Total:</b> &pound;{{number_format($invoice->price, 2)}}</p>
				
				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

				</td>
				</tr>
				<tr><td>
				<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>