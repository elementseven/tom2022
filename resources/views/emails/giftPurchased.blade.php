<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>

						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Gift order confirmed</span></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$gift->first_name}},</span></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">What an incredible human being you are to give someone the gift of being Flong & Sexible!</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">They’ll receive an email telling them about your gift @if($gift->when == 'immediately') shortly @else on {{$gift->date->format('Y/m/d')}}@endif with instructions on how to claim it, and you’ll be notified when they do!</p>


						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If they don’t receive their email, remind them to check their spam/junk folder, and if it’s not there then get in touch with <a href="mailto:support@tommorrison.uk" style="color: #D82737;">support@tommorrison.uk</a></p>

						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						<p style="font-size:22px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Gift Details:</b></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Please check the details below are correct, and if you spot any issue email <a href="mailto:support@tommorrison.uk" style="color: #D82737;">support@tommorrison.uk</a></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Recipient Name:</b> {{$gift->first_name}} {{$gift->last_name}}</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Recipient Email:</b> {{$gift->email}}</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Product(s):</b> @foreach($gift->products as $p) {{$p->name}}, &nbsp; @endforeach </p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Date to receive:</b> @if($gift->when == 'immediately') {{\Carbon\Carbon::now()->format('Y/m/d')}} @else {{$gift->date->format('Y/m/d')}} @endif</p>
						
						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						<p style="font-size:22px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Purchase Details:</b></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Reference number: <span style="color:#D82737;">{{$gift->invoice->transaction_id}}</span></b>.</p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Product(s):</b> @foreach($gift->products as $p) {{$p->name}}, &nbsp; @endforeach </p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Total:</b> &pound;{{number_format($gift->invoice->price, 2)}}</p>

						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						<p style="font-size:22px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Print A Gift Voucher!</b></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Attached to this email is a jpg of a gift voucher, feel free to print this off and give it to your giftee!</p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">They'll still get their email on the date you requested, and they'll need to use the link provided in the email to create their account & claim their gift... but it can be nice to have something to pop in a card!</p>
						
						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for your purchase & support!</p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Have an outstanding day!<br><br>Tom Morrison</p>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;">
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>

