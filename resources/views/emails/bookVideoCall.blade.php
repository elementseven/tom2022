<html>
<head></head>
<body style="background: white; color: black;">
  
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td valign="top" align="left" background="">
          <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
        <tr style="margin:40px 0 40px 0">
          <td>
        <p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Book your video call</span></p>
        </td>
        </tr>
        <tr>
        <td style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">
          <p style="margin-top:30px;">We use Calendly to schedule video calls, please click the button below to book your time slot.</p>
          <a href="https://calendly.com/tom-morrison/45min?month={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m')}}&date={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m-d')}}" target="_blank">
            <button class="btn btn-primary d-inline-block" style="-webkit-text-size-adjust: none;border-radius: 4px;color: #fff;display: inline-block;overflow: hidden;text-decoration: none;background-color: #d82737;border-bottom: 8px solid #d82737;border-left: 18px solid #d82737;border-right: 18px solid #d82737;border-top: 8px solid #d82737;">Book time slot</button>
          </a>
        </td>
        </tr>
        <tr><td>
          <hr style="margin: 30px auto 0;">
        <img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="150" style="margin: 30px auto 0; display: block;"/>
        <p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>
      </table>
    </td>
    </tr>
  </table>
</div>
</body>
</html>