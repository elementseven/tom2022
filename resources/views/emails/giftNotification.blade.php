<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>

						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">You have received a gift from {{$gift->invoice->user->first_name}}!</span></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Hi {{$gift->first_name}}, {{$gift->invoice->user->full_name}} has sent you a gift!</b></span></p>

						@if($gift->message != null && $gift->message != "")
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><i>A message from {{$gift->invoice->user->first_name}}:</i></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$gift->message}}</p>

						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						@endif

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">What an incredibly awesome person they must be! Check out what they got for you:</p>

						<ul>
						@if(count($gift->products))
						<li><p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">@foreach($gift->products as $p) <b>{{$p->name}}</b> <br> @endforeach</p></li>
						@endif
						</ul>

						<p style="font-size:18px; color:#2c2c2c; margin-bottom:  50px; text-align: left; font-family:'Arial', arial, sans-serif;">Let’s get stuck into being strong and flexible... or Flong and Sexible as we like to say!</p>

						<p style="font-size:20px; color: #D82737; text-align: left; font-family:'Arial', arial, sans-serif;"><b>How to Claim Your Gift:</b></p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-bottom:  50px;">To claim your gift & access your new programme(s), you'll need to set up an account on our website. Once you’ve signed up, you’ll be able to access your programme(s) for life via your dashboard!</p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-bottom:  30px;">Click the button below to create your new account and claim your gift!</p>
						
						<div><!--[if mso]>
						  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://tommorrison.uk" style="height:40px;v-text-anchor:middle;width:200px;" arcsize="10%" strokecolor="#D82737" fillcolor="#D82737">
						    <w:anchorlock/>
						    <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">Claim Your Gift</center>
						  </v:roundrect>
						<![endif]--><a href="https://tommorrison.uk/claim-your-gift/{{$gift->id}}/{{$gift->code}}"
						style="background-color:#D82737;border:1px solid #D82737;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:40px;text-align:center;text-decoration:none;width:200px;-webkit-text-size-adjust:none;mso-hide:all;">Claim Your Gift</a></div>

						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

						@foreach($gift->products as $p) 

						@if($p->id == 4)
						<p style="font-size:22px; color:#D82737; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">In order to proceed with the personalised mobility plan, please fill in the questionnaire.<br><b><a href="https://tommorrison.uk/questionnaire">Click here to complete the questionnaire.</a></b></p>
						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
						@endif

						@if($p->id == 5)
						<p style="font-size:22px; color:#D82737; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;">In order to proceed with your video call, please fill in a quick form to let us know when to contact you.<br><b><a href="https://tommorrison.uk/video-call-questionnaire">Click here to complete the form.</a></b></p>
						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
						@endif

						@endforeach

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you have any questions or queries, please send us an email to <a href="mailto:support@tommorrison.uk" style="color: #D82737;">support@tommorrison.uk</a></p>
						
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Have an outstanding day!<br><br>Tom Morrison</p>

					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;">
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>

