<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>
						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">It’s been two weeks! Time to investigate!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hey {{$user->first_name}}, hopefully you’ve been hitting SMM for two weeks now!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">At this stage, some really important things to reflect on are:</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; padding-left: 50px;"><b>- Where are you making the most progress?</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; padding-left: 50px;"><b>- Where do you feel like you’re stuck?</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">This is a great way to work out if you need more flexibility or more stability, i.e:</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; padding-left: 50px;">- Do you struggle with the exercises focused on range of motion?  Such as the Deep Lunge, 90/90, etc.? Then flexibility is probably a big limiting factor for you.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; padding-left: 50px;">- Do you struggle with the more active exercises? Such as Side Plank March, Single Leg Deadlifts, etc.? Then you may have the range but lack stability.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; padding-left: 50px;">- If you struggle with a mix, then that’s good to know too! You just need both mobility & stability.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">You can use this information to help guide your Head to Toe mobility sessions, maybe add in more flexibility, or more stability exercises depending on where you struggle.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Or! Is there one particular movement that just isn’t improving? Stuck on side planks? Or the f*!#ing Deep Lunge?! Try spending time with the regressions (from the ebook), sometimes taking a step back is the best way to move forwards!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">Remember, your results will depend on the work you put in!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">I’ll be checking back in with you in 2 weeks for your first big retest!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">- Tom</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">P.s. have you signed up to our mailing list yet? If you haven’t already, you can sign up to the <a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">7 Days of Awesome</a> -  a free week of daily mobility exercises you can try alongside SMM! <b>Plus tips, tricks, exercises & stories from us every week!</b></p>

					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;"/>
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>