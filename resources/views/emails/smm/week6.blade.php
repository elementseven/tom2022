<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>
						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">SMM 6 week check in!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Well, {{$user->first_name}}, it’s been 6 weeks since you have started your SMM journey!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Hopefully by now you really have a good grasp of the movements and understand importance of them! …but you might also have fallen into bad habits!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Have you re-watched the videos? Is there anywhere you could improve? Are you just “going through the motions” with any of the exercises?</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Have you tried a set of 20 side plank marches yet? 😝</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">I hope SMM is helping you build an intuitive sense of what to do when you’re tight/sore/niggly. </p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">When your lower back is stiff after a long car journey, or you’re a bit beaten up from a hard training session, you want just to feel which movements will help you (on top of following the full SMM, of course!)</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Body awareness is key to making your training better, don’t forget that!! SMM is teaching you about your body, not just blindly leading you through exercises!</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Speak soon!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">- Tom</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">P.s. have you signed up to our mailing list yet? If you haven’t already, you can sign up to the <a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">7 Days of Awesome</a> -  a free week of daily mobility exercises you can try alongside SMM! <b>Plus tips, tricks, exercises & stories from us every week!</b></p>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;"/>
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>