<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>
						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Welcome to SMM {{$user->first_name}}! Here’s how to get started!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hey {{$user->first_name}},</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Great to see you’ve picked up SMM and started to take control of your own mobility.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">First thing you want to do is <a href="https://tommorrison.uk/dashboard/products/view/the-simplistic-mobility-method" style="color: #D82737;">login to your dashboard</a>, printout the PDF’s and watch the Intro video. </p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you never received your Account Created email, just use the “Forgot Your Password?” function to login</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Your very first session of SMM will be the longest because we recommend you Test, do the Exercises, then Retest in one go. <br/>This can take an hour or so, but it’s great to get a full overview of where you’re at, what can be improved immediately (you’ll be surprised how much difference 1 session will make!), and what needs to be worked on more long term.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you aren’t able to set aside that long, still do the Tests first – even if you then complete the Exercises the next day!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>1. Your First Tests</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">After watching the intro, run through the Tests.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">I reeally recommend you film these to get an indication of where you’re at and to monitor your progress – honestly, you think you’ll remember or notice as you go, but you won’t! So, film them! I’ll know if you don’t…!!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Note down anything you <i>feel</i> on the PDFs. Sometimes you might look the same from side to side, but you may feel different on one side compared to the other.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Don’t get disheartened or think you’ve “failed” any of them, the tests are <b>purely your baseline</b>, there’s no way to fail! They’re prompts to get you noticing more about your body and a guide of what to look for, so you know what to improve.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">This is the point where you stop saying, “I’m inflexible”, “I have bad balance” or “a weak core” and start saying: “Okay, this is what I can’t do yet, so now I’m going to work on it”. </p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>2. The Exercises</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">After your tests, it’s time to get stuck in with the Exercises!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">They all made it to the final cut of SMM for specific reasons, with the routine built in a specific order. You open the correct things before strengthening them, then lock the benefits in with stability.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">The first time you do the exercises, do them alongside me & Jenni with the Exercises video. Yes, it’s 40 minutes, because we go through cues, progressions, and invaluable information for getting the most out of SMM.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Don’t just copy the movements! Do the exercises with purpose and not just go through the motions!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Take videos of yourself & notes here too, each exercise can be a test! Something as small as not being able to feel your glutes while doing Glute Bridges is a big deal and something you want to focus on correcting.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Think about it this way: the more exercises you struggle with the more you have to gain from practicing! View struggles in a positive way!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>3. The Retests</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Lastly, you want to Retest!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">This Retest is to see how fast you can make a change! You may notice a massive difference between your first test and retest and that’s great! Or you may only see minor improvements, which just means you’ll need a bit longer to make changes (like me!). </p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>You don’t need to Test & Retest after every SMM session</b>, but we recommend testing yourself every 4 weeks or so to check in with your progress. I’ll send you another email then to see how you’re getting on!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>4. Sign Up to Our Mailing List!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you haven’t already, you can sign up to the <a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">7 Days of Awesome</a> - a free week of daily mobility exercises you can try alongside SMM.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">After that, you’ll get weekly tips, tricks, exercises and stories from us to help you with our journey! You’ll also be told about any sales (and secret pre-sales!)!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">Sign Up Here!</a></p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">So welcome to the cool club and I’m looking forward seeing you become more strong and flexible! Or flong and sexible, as we like to say 😎</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">- Tom</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>P.s. Don’t forget to join our <a href="https://www.facebook.com/groups/1712646825732552" style="color: #D82737;">Facebook Community</a> to keep up with all the latest tips and to get invaluable support from the other SMMers!</b></p>

					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;"/>
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>