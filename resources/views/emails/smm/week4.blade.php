<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>
						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">It’s been 4 weeks! Time to Retest!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hello {{$user->first_name}}, you beautiful thing!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Well, it’s been 4 weeks since you picked up SMM! Hopefully, you managed to start straight away, otherwise this is your kick up the butt to get started!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>Retest Yourself!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you did, then it is time for your big 4 week Retest to see where you’re at. Film, take notes and compare to your first notes, videos and pictures!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Also, take some time to reflect on how you feel too: Are daily tasks easier? Do you notice a spring in your step? Do warm ups go quicker? Maybe you don’t feel as achy?</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Even if you’re only seeing little progress with movements if you’re feeling better, then that’s a big win!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>Review the Videos</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Take this time to review the SMM Tests/Exercises videos to see if there’s any tips you missed first time round – or if there’s any bad habits you’ve picked up! Now you have a good feel for the routine you’ll be able to spot things a lot easier!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>Share Your Progress!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Make sure to share your progress in the <a href="https://www.facebook.com/groups/1712646825732552" style="color: #D82737;">Facebook group</a> too! And let’s keep on track for your 8-week retest!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>What Now?</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you’re flying with the movements and seeing lots of progress, then keep up what you’re doing! Maybe it’s time to sneak a look at the next step, <a href="https://tommorrison.uk/stability-builder">Stability Builder™</a>?</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Or, if progress feels quite slow, then are you able to add an extra session (or half session) into your week?</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Don’t forget to ask in the <a href="https://www.facebook.com/groups/1712646825732552" style="color: #D82737;">Facebook group</a> if there’s something you’re struggling with!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Keep being awesome, and I’ll be in touch again soon!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">- Tom</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;">P.s. have you signed up to our mailing list yet? If you haven’t already, you can sign up to the <a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">7 Days of Awesome</a> -  a free week of daily mobility exercises you can try alongside SMM! <b>Plus tips, tricks, exercises & stories from us every week!</b></p>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;"/>
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>