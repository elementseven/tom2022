<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>
						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">It’s been 8 weeks! Retest time again!</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Good after evening {{$user->first_name}} 😎</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">It’s been 8 weeks since you got SMM!</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>It’s time for your 8 week retest!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">How did it go? How do you feel? Take videos & pictures to see how far you have come since your very first tests!! </p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>Still Got Stuff to Work On?</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If there are areas you still need to improve, THAT’S OK! Progress takes different lengths of time for different people – there’s no need to rush. The success will be sweet when you get there… plus you’ll have learnt a lot more along the way than those who saw quick gains.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Keep up to the volume and don’t get disheartened! You can reprint your sheets and treat this as the start of a new 4 week block! Anything you are really not feeling progress with make sure to use the group to <a href="https://www.facebook.com/groups/1712646825732552" style="color: #D82737;">ask a question in the Facebook group</a>, or <a href="mailto:hello@tommorrison.uk" style="color: #D82737;">send me an email!</a></p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>It's Time for the Next Step!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you’re feeling awesome and you’re now a lifetime SMMer who wants to keep up your progress, check out the next steps:</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="https://tommorrison.uk/products/ultimate-core-seminar">Ultimate Core</a> (which most of you already have!) is your ticket to a strong core – no, not just abs, a real strong core. One that will help you use your strength efficiently & effectively while keeping your back safe. It’ll teach you the 4 principles of Core Strength and how you can improve them.</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="https://tommorrison.uk/products/stability-builder">Stability Builder™</a> takes the most important exercises for complete joint & full body strength and puts them into a follow along format that means you can’t miss anything out. It builds on the foundation you’ve given yourself with SMM and adds extra strength & stability to your joints from all angles.</p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>Proud of Your Progress?</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">So am I! We would love to see your progress pics, so don’t forget to post them in <a href="https://www.facebook.com/groups/1712646825732552" style="color: #D82737;">the Facebook group! Or it'd be so awesome and really helpful for us if you could leave a <a href="https://www.google.co.uk/search?q=Tom+Morrison+Ltd&ludocid=581983343819244963#lrd=0x4861054a7fdb59f9:0x8139ec4898a45a3,3" style="color: #D82737;">Google Review</a> and help us spread the word!</a></p>

						<p style="font-size:24px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif; margin-top: 50px;"><b>So, is this Goodbye?!</b></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">This will be my last email to you (*sobs*) but, if you haven’t already, you can join our mailing list!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">We send out tips, tricks, interesting thoughts – plus, updates on sales & discounts! You can unsubscribe and any time, and I promise we don’t send out anything that we don’t think you’ll love!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="https://tommorrison.uk/7-days-of-awesome"style="color: #D82737;">Sign Up Here!</a>, and your very first emails will be the Seven Days of Awesome, where we take you through 7 days of awesome drills that you can add into your day!</p>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">- Tom</p>

					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin: 30px auto 0;"/>
						<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
						<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p>
					</td>
				</tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>