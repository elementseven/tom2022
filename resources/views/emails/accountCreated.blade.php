<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
						<p style="text-align: center; padding: 40px;"><img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100px" alt="Tom Morrison Logo"/></p>

						<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Account Created</span></p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi {{$user->first_name}},</span></p>

						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Clearly you're an amazing individual in pursuit of awesomeness - which is great because <b>your Tom Morrison account is now active!</b></p>

						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="https://tommorrison.uk/login" style="color: #D82737;"><u>Click this link</u></a> to login using the details below.</p>
						
						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">(You can change your password at any time in the <a href="https://tommorrison.uk/dashboard/account-management" style="color: #D82737;"><u>'My Account'</u></a> section of your dashboard)</b></p>

						<table bgcolor="#F7F7F7" border="0" cellpadding="0" cellspacing="0" class="container" width="100%" style="margin: 30px auto;">
              <tr>
                <td align="left">
                  <p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px;"><b style="width: 120px; display: inline-block;">Email:</b> {{$user->email}}</p>
                </td>
              </tr>
            </table>

						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for your support & have an outstanding day!</p>
						<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Tom Morrison & Jenni</b></p>

						<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>
						<p style="margin-bottom: 0;font-size:24px; text-align: left; font-family:'Arial', arial, sans-serif;"><b><span style="color:#D82737;">PLEASE NOTE:</span> This is not a purchase confirmation.</b></p>
						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">That will come in a separate email.</p>
						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>If you didn't enter your payment details you haven't completed your order</b> and the new product(s) won't be attached to your account.</p>
						<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">If you think this has happened then get in touch on <a href="mailto:support@tommorrison.uk"><u>support@tommorrison.uk</u></a>.</p>

					</td>
				</tr>
				<tr><td>
				<hr style="margin: 30px auto 0;">
				<img src="https://tommorrison.uk/img/logos/logo_dark.png" width="250px" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>

			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>