@php
	$billing = $user->addresses[1];
@endphp

<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif;, serif; text-align: left; padding-top:40px; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>

				<p style="font-size:22px; background-color: #D82737; padding: 13px 15px; height:25px;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;float: left;font-weight:700;">Tom Morrison - Receipt</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Name:</b> {{$user->full_name}}</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Billing Address:</b><br/>{{$billing->streetAddress}}, @if($billing->extendedAddress){{$billing->extendedAddress}},@endif {{$billing->city}}, {{$billing->postalCode}}, {{$billing->country}}</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Invoice No:</b> {{$invoice->id}}</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Date:</b> {{Carbon\Carbon::create($invoice->created_at)->format('jS F Y')}}</span></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;"><b>Payment Method:</b> {{$invoice->payment_method}}</span></p>

				<table bgcolor="#fff" border="1" cellpadding="0" cellspacing="0" class="container" width="100%" style="margin: 30px auto 30px; border-style: solid; border-color: #000;">
          <tr style="font-size:18px; text-align: left; color: #fff; background-color: #D82737;">
          	<th style="padding:10px">Products</th>
          	<th style="padding:10px; text-align: right;">Cost</th>
          </tr>

          @if(count($invoice->products))
          @foreach($invoice->products as $p)
          <tr border="0">
            <td  border="0" align="left" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">{{$p->name}}</p>
						</td>
						<td  border="0" align="right" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">£{{$p->pivot->price}}</p>
						</td>
					</tr>
					@endforeach
					@endif

					@if(count($invoice->bundles))
          @foreach($invoice->bundles as $b)
          <tr border="0">
            <td  border="0" align="left" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">{{$b->title}}</p>
						</td>
						<td  border="0" align="right" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">£{{$b->pivot->price}}</p>
						</td>
					</tr>
					@endforeach
					@endif

					@if(count($invoice->upgrades))
          @foreach($invoice->upgrades as $u)
          <tr border="0">
            <td  border="0" align="left" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">{{$u->title}}</p>
						</td>
						<td  border="0" align="right" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">£{{$u->pivot->price}}</p>
						</td>
					</tr>
					@endforeach
					@endif

					@if(count($invoice->variants))
          @foreach($invoice->variants as $v)
          <tr border="0">
            <td align="left" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">{{$v->merch->name}} - {{$v->size}}</p>
						</td>
						<td align="right" style="vertical-align: top;">
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">£{{$v->pivot->price}}</p>
						</td>
					</tr>
					@endforeach
					@endif

					<tr border="0">
						<td  border="0" align="left" style="vertical-align: top;">
							@if($invoice->shipping)
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;"><b>Shipping:</b></p>
							@endif
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;"><b>Total:</b></p>
						</td>
						<td  border="0" align="right" style="vertical-align: top;">
							@if($invoice->shipping)
							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">&pound;{{number_format($invoice->shipping, 2)}}</p>
							@endif

							<p style="font-size:18px; color:#2c2c2c; text-align: right; font-family:'Arial', arial, sans-serif;margin: 15px 10px;">&pound;{{number_format($invoice->price, 2)}}</p>
						</td>
					</tr>
				</table>

				@foreach($invoice->products as $p) 
				@if($p->id == 4)
				<p style="font-size:22px; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px 0;"><b>In order to proceed with the personalised mobility plan, please fill in the questionnaire.</b><br><br><b><a href="https://tommorrison.uk/questionnaire" style="color:#D82737; ">Click here to complete the questionnaire.</a></b><br></p>
				@endif
				@if($p->id == 5)
				<p style="font-size:22px; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px 0;"><b>In order to proceed with your video call, please fill in a quick form to let us know when to contact you.</b><br><br><b><a href="https://tommorrison.uk/video-call-questionnaire" style=" color:#D82737;">Click here to complete the form.</a></b><br></p>
				@endif
				@endforeach   

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Thank you for your support & have an outstanding day!</p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>Tom Morrison & Jenni</b></p>

				<p style="border-bottom: 1px solid #2c2c2c; margin: 40px auto;"></p>

				<p style="margin-bottom: 25px;font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">Any billing problems or questions, please get in touch at <a href="mailto:accounts@tommorrison.uk"><u>accounts@tommorrison.uk</u></a></p>

				</td>
				</tr>
				<tr><td>
				<img src="https://tommorrison.uk/img/logos/logo_dark_circle.png" width="100" style="margin: 30px auto 0; display: block;"/>
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the Tom Morrison website<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>
				<p style="font-size:10px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">VAT Number: GB354044714</p>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>