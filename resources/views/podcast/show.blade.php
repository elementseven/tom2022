@php
$page = 'light';
$pagetitle = $podcast->name . " - You Can Fix You Podcast - Tom Morrison";
$meta_description = $podcast->name . " - You Can Fix You Podcast - Tom Morrison";
$pagename = "Podcast";
$og_image = $podcast->getFirstMediaUrl('podcasts','large');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	figure .attachment__caption{
		display: none !important;
	}
</style>
@endsection
@section('header')
<header class="container position-relative pt-5">
	<div class="row pt-lg-5">
		<div class="col-12">
			<p class="mb-1 text-light-grey"><b><span class="text-primary">Ep. {{$podcast->episode}}</span> // You Can Fix You Podcast</b></p>
			<h1 class="mb-2 podcast-title">{{$podcast->name}}</h1>
			<p class="text-light-grey">{{Carbon\Carbon::parse($podcast->date)->format('d M Y')}}</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5">
	<div class="row">
		<div class="col-lg-8 mb-5">
			<div class="embed-responsive embed-responsive-16by9 mb-4 shadow rounded overflow-hidden">
				{!! $podcast->youtube !!}				
			</div>
			{!!$podcast->description!!}
		</div>
		<div class="col-lg-4">
			@if(count($podcast->links))
			<div class="card p-3 border-0 shadow mb-4">
				<h4 class="mb-3">Important Links</h4>
				@foreach($podcast->links as $link)
				<div class="card link-card mb-2 overflow-hidden">
					<a href="{{$link->link}}">
						<picture> 
							<source srcset="{{$link->getFirstMediaUrl('links','normal-webp')}}" type="image/webp"/> 
							<source srcset="{{$link->getFirstMediaUrl('links','normal')}}" type="image/jpeg"/>
							<img src="{{$link->getFirstMediaUrl('links','normal')}}" width="540" height="385" class="zoom-in w-100 h-auto mb-1" alt="{{$link->name}} - Image" />
						</picture>
						<div class="link-card-text px-2 py-2">
							<p class="text-dark mb-0"><i class="fa fa-link"></i> <b>{{$link->name}}</b></p>
						</div>
					</a>
				</div>
				@endforeach
			</div>	
			@endif
			<div class="podcast-programs card p-3 border-0 shadow">
				<h4 class="mb-3">Related Programs</h4>
				@foreach($podcast->products as $product)
				<product-card :product="{{$product}}" :page="'shop'" class="mb-3"></product-card>
				@endforeach
			</div>
		</div>
	</div>
	<div class="row half_row mt-5">
		@if($previous)
		<div class="col-lg-4 half_col mb-3 d-none d-lg-block">
			<div class="card p-4">
				<p class="mb-0 text-small text-light-grey"><b><span class="text-primary">Ep. {{$previous->episode}}</span> // You Can Fix You Podcast</b></p>
				<p class="text-title text-larger">{{ \Illuminate\Support\Str::limit($previous->name, 35, '...') }}</p>
				<a href="/podcast/{{$previous->id}}/{{$previous->slug}}">
					<button type="button" class="btn btn-small btn-primary"><i class="fa fa-angle-left mr-2"></i> Previous Episode</button>
				</a>
			</div>
		</div>
		@endif
		@if($next)
		<div class="col-lg-4 mb-3 half_col">
			<div class="card p-4">
				<p class="mb-0 text-small text-light-grey"><b><span class="text-primary">Ep. {{$next->episode}}</span> // You Can Fix You Podcast</b></p>
				<p class="text-title text-larger">{{ \Illuminate\Support\Str::limit($next->name, 35, '...') }}</p>
				<a href="/podcast/{{$next->id}}/{{$next->slug}}">
					<button type="button" class="btn btn-small btn-primary">Next Episode <i class="fa fa-angle-right ml-2"></i></button>
				</a>
			</div>
		</div>
		@endif
		@if($previous)
		<div class="col-lg-4 half_col mb-3 d-lg-none">
			<div class="card p-4">
				<p class="mb-0 text-small text-light-grey"><b><span class="text-primary">Ep. {{$previous->episode}}</span> // You Can Fix You Podcast</b></p>
				<p class="text-title text-larger">{{ \Illuminate\Support\Str::limit($previous->name, 35, '...') }}</p>
				<a href="/podcast/{{$previous->id}}/{{$previous->slug}}">
					<button type="button" class="btn btn-small btn-primary"><i class="fa fa-angle-left mr-2"></i> Previous Episode</button>
				</a>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection
@section('scripts')
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var preElements = document.querySelectorAll('pre');

    preElements.forEach(function(pre) {
        if (pre.textContent.includes('youtubeid=')) {
            var ytid = pre.textContent.split('=')[1];
            var vidhtml = '<div class="embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + ytid + '?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>';
            pre.innerHTML = vidhtml;
        }
    });

    var figcaptions = document.querySelectorAll('figcaption');

    figcaptions.forEach(function(figcaption) {
        figcaption.style.display = 'none';
    });
});

</script>
@stop