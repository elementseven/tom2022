@php
$pagename = "Podcast";
$page = 'Podcast';
$pagetitle = "You Can Fix You Podcast - Tom Morrison";
$meta_description = "We share mobility tips & secrets from our methods, used by 1000s worldwide with all different injuries and limitations! If you’re inflexible, too flexible, a complete beginner or more advanced, you’re very welcome here!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('header')
<div class="container pt-lg-5">
	<div class="row pt-5">
		<div class="col-lg-7">
			<h1 class="podcast-header-text mb-4">YOU CAN FIX YOU</h1>
			<p>In our podcast, we share mobility tips & secrets from our methods, used by 1000s worldwide with all different injuries and limitations! If you’re inflexible, too flexible, a complete beginner or more advanced, you’re very welcome here!</p>
		</div>
		<div class="col-lg-5">
			<picture> 
				<source srcset="/img/podcast/top.webp" type="image/webp"/> 
				<source srcset="/img/podcast/top.jpg" type="image/jpeg"/>
				<img src="/img/podcast/top.jpg" width="540" height="385" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison & JP Podcast stick men" />
			</picture>
		</div>
	</div>
</div>
@endsection
@section('content')
<div class="container-fluid page-scroll-to-top mt-4">
	<div class="row">
		<podcast-index :savedsearch="'{{session('saved-search-podcast')}}'"></podcast-index>
	</div>
</div>
@endsection