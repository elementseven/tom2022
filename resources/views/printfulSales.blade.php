@php
$page = "Shop";
$pagename = "Printful Sales";
$pagetitle = "Printful Sales - Tom Morrison";
$meta_description = "Printful Sales";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('header')
<header class="container pt-5">
	<h1 class="text-primary">Prinful Sales</h1>
	<p class="mb-5">All printful sales from April 1st 2023</p>
</header>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="table-responsive">
			  <table class="table table-striped">
			  	<thead>
				    <tr>
				      <th scope="col"><p class="mb-3">ID</p></th>
				      <th scope="col"><p class="mb-3">Name</p></th>
				      <th scope="col"><p class="mb-3">Email</p></th>
				      <th scope="col"><p class="mb-3">Invoice Date</p></th>
				      <th scope="col"><p class="mb-3">Products</p></th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($sales as $s)
				    <tr>
				      <td><a href="https://tommorrison.uk/nova/resources/invoices/" target="_blank">{{$s->id}}</a></td>
				      <td>{{$s->user->full_name}}</td>
				      <td>{{$s->user->email}}</td>
				      <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $s->created_at)->format('d M Y')}}</td>
				      <td>
				      	@foreach($s->variants as $v)
				      	<p>{{{$v->merch->name}}} <br/><b>Size:</b> {{$v->size}}</p>
				      	@endforeach
				      </td>
				    </tr>
				    @endforeach
				  </tbody>
			  </table>
			</div>
		</div>
	</div>
</div>
@endsection