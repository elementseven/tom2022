@php
$page = 'T&Cs';
$pagename = 'Terms and Conditions';
$pagetitle = "Terms and Conditions - Tom Morrison";
$meta_description = "All the legal information you may require.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-title my-5">Terms &amp; Conditions</h1>
        </div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row">
		<div class="col-12">
			<p><b>By using this website you agree to the following terms:</b></p>
			<p><strong>1.</strong> We do not store credit/debit card details on our systems, if you decide to store you card details for later use, card details are stored by our payment processor <a href="https://stripe.com" target="_blank" rel="noopener">Stripe</a>. This means we have no access to card numbers used on the website and ensures the security of your data.</p>
			<p><strong>2.&nbsp;</strong>We do not share customer’s personal details with any 3rd parties. We may collect and store your name and email address to allow you to use some of our web apps effectively. We may collect information about your country of origin in order to monitor where sales are taking place. For full privacy policy visit the <a href="{{route('privacy-policy')}}">Privacy Policy Page</a>.</p>
			<p><strong>3.</strong> As all of Tom Morrison’s products are digital and automated you will receive them directly after the payment is processed within one hour. If you did not receive your email confirmation, double check your junk/spam folder and if you still cannot find it then email <a class="" href="mailto:support@tommorrison.uk">support@tommorrison.uk</a> and we will resend manually</p>
			<p><strong>4.</strong> If you have any queries please send to <a class="" href="mailto:support@tommorrison.uk">support@tommorrison.uk</a> we reply to all of our emails so if you do not receive a reply within 24-48 hours please double check the email address you sent to and/or resend your message.</p>
			<p><strong>5.</strong> The information contained in these online programs/products/e-books/camps is presented to improve movement, not treat medical conditions or injuries. This information is not a substitute for medical advice or treatment of medical conditions or injuries. If you feel any pain stop immediately. If in doubt, always seek the advice of your Doctor.</p>

			<p class="mimic-h3 mt-5">Refund / Cancellation Policy</p>
			<p>Digital & downloadable products (e.g. the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method">Simplistic Mobility Method</a>) aren’t eligible for refund as they cannot be returned.</p>
			<p>Refunds on seminar places may be subject to a fee of your total cost for the space due to processing costs and charges to Tom Morrison, please insure you definitely have the seminar date free before booking. </p>
			<p>For returns of merchandise, please go to <a href="https://tommorrison.uk/delivery-and-returns">Delivery and Returns</a></p>
		</div>
	</div>
</div>
@endsection