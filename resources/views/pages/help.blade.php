@php
$page = 'Help';
$pagename = 'Help';
$pagetitle = "Help - Find answers to your questions or get in touch";
$meta_description = "We're here to help! Have a look through our FAQs, but if you can't find an answer, send us an email from the category that matches your query best!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.help-holder{
		min-height: 596px;	
	}
</style>
@endsection
@section('header')
<header class="container pt-5">
	<div class="row">
		<div class="col-lg-7 pt-5 mob-pt-0">
			<h1 class="top-title mb-4">Help</h1>
			<p class="mb-4 text-large mob-text-small">We're here to help! Have a look through our FAQs, but if you can't find an answer, send us an email from the category that matches your query best!</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="help-holder">
	<help :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></help>
</div>
@endsection
@section('scripts')

@endsection