@php
$page = 'Delivery and Returns';
$pagename = 'Delivery and Returns';
$pagetitle = "Delivery and Returns - Tom Morrison";
$meta_description = "Learn how we use the data collected through this website, what rights you have and what requests you can make.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row">
		<div class="col-md-8">
			<h1 class="page-title my-5">Delivery & Returns</h1>
			<p><b>The information on this page refers to our <a href="{{route('merch')}}">Merchandise</a>. For our Returns Policy for our <a href="{{route('shop')}}">Digital Products</a> please see our <a href="{{route('tandcs')}}">Terms and Conditions</a></b></p>
        </div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row">
		<div class="col-12 my-4">
			<h3 class="text-capitalize">Delivery Charges</h3>
			<p>We offer free shipping to most countries worldwide.</p>
			<p>Additional charges may be added to the order for remote or difficult to access locations that require special attention. We reserve the right to advise you of any additional delivery charges that apply to your specific delivery address.</p>
		</div>
		<div class="col-12 mb-5">
			<h3>Delivery Times</h3>
			<p>Because each of our items are custom printed for your order, it takes 2–7 business days to create your products (“Fulfilment”). Then, average shipping time is 4 business days depending on the order's destination.</p>
			<p>Generally, your custom-made products should be with you between 6 – 12 business days. </p>
			<p>This is only an average estimation, and some delivery can take longer, or alternatively be delivered much faster. All delivery estimates given at the time of placing and confirming order can be subject to change. In any case, we will do our best to contact you and advise you of all changes.</p>
			<p>Some Products are packaged and shipped separately. We cannot guarantee delivery dates and to the extent permitted by law accept no responsibility, apart from advising you of any known delay, for Products that are delivered after the estimated delivery date.</p>
			<p>Ownership of the Products will only pass to you after we receive full payment of all sums due in respect of the Products, including delivery charges and taxes, and deliver the Products to the carrier. </p>
		</div>
		<div class="col-12 mb-5">
			<h3>Editing Delivery Details</h3>
			<p>Once you have confirmed your order, it might not be possible to edit or cancel it. If you want to change some parameters, delivery addresses, etc., please <a href="{{route('contact')}}">get in touch</a> with us as soon as possible. We may not be able to make modifications to your order, but we will do our best on a case-by-case basis. </p>
		</div>
		<div class="col-12 mb-5">
			<h3>Shipping Damage/Loss</h3>
			<p>The risk of loss of, damage to and title for Products pass to you upon our delivery to the carrier. It shall be your (the customer) responsibility to file any claim with a carrier for a lost shipment if carrier tracking indicates that the Product was delivered. In such case where the delivery company was at fault, we will not make any refunds and will not resend the Product. For Users in the European Economic Area the risk of loss of, damage to and title for Products will pass to you when you or a third party indicated by you has acquired the physical possession of the Products.</p>
			<p>If carrier tracking indicates that a Product was lost in transit, you or your Customer may make a written claim for replacement of the lost Product in compliance with our <a href="#returns">Returns Policy</a>. For Products lost in transit, all claims must be submitted no later than 30 days after the estimated delivery date.  All such claims are subject to investigation and sole discretion.</p>
		</div>
		<div class="col-12 mb-5">
			<h3>Description of Products</h3>
			<p>We always try to represent each design as accurately as possible. We use our best efforts to provide you with the best images and descriptions, but unfortunately cannot guarantee that colours and details in website images are 100% accurate representations of the product, and sizes might in some cases be approximate.</p>
			<p>Before ordering, we invite you to have a close look at the Product description and design.</p>
		</div>
		<div class="col-12 mb-5">
			<h3>Damaged Products</h3>
			<p>Sometimes during the manufacturing process Products can be damaged.  Obviously, we won’t knowingly ship damaged items to you, but if you notice damage on your order please <a href="{{route('contact')}}">get in touch</a> and we will review the item based on our <a href="#returns">Returns Policy</a>.</p>
		</div>
		<div class="col-12 mb-5">
			<h3>Purchase of Products</h3>
			<p>Your order is purchase of a Product for which you have paid the applicable fee and/or other charges that we have accepted and received. Any Products in the same order which we have not accepted do not form part of that contract.  We may choose not to accept any orders in our sole discretion.</p>
			<p>Orders are placed and received exclusively via the Site. Before ordering from us, it is your responsibility to check and determine full ability to receive the Products. Correct name of the recipient, delivery address and postal code/zip code, up-to-date telephone number, and email address are absolutely necessary to ensure successful delivery of Products.</p>
			<p>All information asked on the checkout page must be filled in precisely and accurately. We will not be responsible for missed delivery because of a wrong or misspelled recipient name or surname, delivery address or an inappropriate phone number. Should you like to ask for a change in the delivery address, phone number, or any other special requirements, <a href="{{route('contact')}}">please contact us</a>.</p>
		</div>
		<div id="returns" class="col-12 mb-5">
			<h3>Returns Policy</h3>
			<p>Digital & downloadable products (e.g. the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method">Simplistic Mobility Method</a>) aren’t eligible for refund as they cannot be returned.</p>

			<p>Any claims for misprinted / damaged / defective items must be submitted within 4 weeks after the product has been received. For packages lost in transit, all claims must be submitted no later than 4 weeks after the estimated delivery date. Claims deemed an error on our part are covered at our expense.</p>

			<p><b>PLEASE NOTE: We do not refund orders for buyer’s remorse or size exchanges.</b></p>

			<p>Unfortunately, due to the custom nature of our products, they cannot be returned if you do not like your product or it is the wrong size.</p>

			<p>Notification for EU consumers: According to Article 16(c) of the Directive 2011/83/EU of the European Parliament and of the Council of 25 October 2011 on consumer rights, the right of withdrawal may not be provided for the supply of goods made to the consumer's specifications or clearly personalized, therefore we reserve rights to refuse returns at our discretion.</p>

			<p>If you would like to return your item, <a href="{{route('contact')}}">please contact us</a>.</p>

			<p><b>Additional Notes:</b></p>

			<p><b>Wrong Address:</b> If you provide an address that is considered insufficient by the courier, the shipment will be returned to our facility. You will be liable for reshipment costs once we have confirmed an updated address with you.</p>

			<p><b>Unclaimed:</b> Shipments that go unclaimed are returned to our facility and you will be liable for the cost of a reshipment to yourself or your end customer.</p>

			<p>This Policy shall be governed and interpreted in accordance with the English language, regardless of any translations made for any purpose whatsoever.</p>
		</div>
	</div>
</div>
@endsection