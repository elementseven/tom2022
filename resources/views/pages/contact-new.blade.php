@php
$page = 'Contact';
$pagename = 'Contact';
$pagetitle = "Contact - Get in touch with Tom Morrison";
$meta_description = "Speak to the man, the myth, the legend, Tom Morrison about training, mobility programs, online products, or about seminars, workshops.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('header')
<header class="container pt-5">
	<div class="row">
		<div class="col-lg-6 pt-5">
			<h1 class="top-title mb-3">Get In Touch</h1>
			<p class="mb-4 larger">We're here to help! Select which option that fits the best & don't forget to check the FAQ!</p>
			<a href="#" class="contact-option shadow d-block mb-3"><p class="mb-0"><b>Program / Movement / Injury Questions</b></p></a>
			<a href="#" class="contact-option shadow d-block mb-3"><p class="mb-0"><b>Technical Support / Account / Payment Questions</b></p></a>
			<a href="#" class="contact-option shadow d-block"><p class="mb-0"><b>Seminar / Collaboration / Any Other Enquiry</b></p></a>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture> 
				<source srcset="/img/tom-morrison-get-in-touch.webp" type="image/webp"> 
				<source srcset="/img/tom-morrison-get-in-touch.jpg" type="image/jpeg">
				<img src="/img/tom-morrison-get-in-touch.jpg" class="w-100 d-none d-md-block" alt="Tom morrison touching another Tom Morrison on the bum" />
			</picture>
		</div>
	</div>
</header>
@endsection
@section('content')
<main class="container-fluid bg-light position-relative py-5 mb-5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 pb-5">
					<p class="mimic-h2">Frequently Asked Questions</p>
				</div>
				<div class="col-lg-9 mb-5">
					<p class="text-primary larger"><b>Program-Related Support</b></p>
					<div id="faq-accordion">
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1">I’m not very flexible, are your programs suitable for total beginners? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_1" class="collapse" data-parent="#faq-accordion">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2">I have an injury/pain, can I still do your programs? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_2" class="collapse" data-parent="#faq-accordion">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
					</div>
        </div>
        <div class="col-lg-9">
					<p class="text-primary larger"><b>Technical Support</b></p>
					<div id="faq-accordion-2">
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1">I bought a program but didn't get an 'Account Created' email - how do I access my programs? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_1" class="collapse" data-parent="#faq-accordion-2">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2">I can access my Account, but the program I bought isn't there? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_2" class="collapse" data-parent="#faq-accordion-2">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_3" aria-expanded="false" aria-controls="answer_3">There was a glitch/error on the website before I completed my purchase, what should I do? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_3" class="collapse" data-parent="#faq-accordion-2">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_4" aria-expanded="false" aria-controls="answer_4">I purchased a gift for someone, but they haven't received it. What should I do? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_4" class="collapse" data-parent="#faq-accordion-2">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
						<p class="larger mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_5" aria-expanded="false" aria-controls="answer_5">Can I buy a program in another currency? <i class="fa fa-angle-down collapsed float-right mt-2"></i><i class="fa fa-angle-up expanded float-right mt-2"></i></a></p>
						<div id="answer_5" class="collapse" data-parent="#faq-accordion-2">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<hr/>
					</div>
        </div>
			</div>
		</div>
	</div>
</main>
@endsection
@section('scripts')

@endsection