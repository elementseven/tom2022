@php
$page = 'About';
$pagename = 'About';
$pagetitle = "About - Tom Morrison";
$meta_description = "About the man, the myth, the legend, Tom Morrison - training, mobility programs, online products, seminars, workshops.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('content')
<main class="container bg-white position-relative pb-5">
	<div class="row">
		<div class="col-12 pb-4 mt-5 mob-mt-0 mob-pb-3">
			<h1 class="page-title mt-5">About Us</h1>
		</div>
		<div class="col-lg-6">
			<picture> 
				<source srcset="/img/about/team2.webp" type="image/webp"/> 
				<source srcset="/img/about/team2.jpg" type="image/jpeg"/>
				<img src="/img/about/team2.jpg" class="img-fluid mt-3 mob-mb-5" alt="Tom morrison & jenni sanders simplistic mobility method." />
			</picture>
		</div>
		<div class="col-lg-6 mob-mb-5">
			<p><b>Tom and Jenni’s mission is to change lives through movement, helping people reclaim what they’ve lost & learn how to have fun again!</b></p>
			<p>Tom & Jenni met in 2014 when Jenni moved to Northern Ireland and joined the gym Tom coached at - they instantly clicked. They both knew what it was like to be worn out with constant injuries when you just wanted to train and have fun!</p>
			<p>After spending years coaching, studying, and helping people become stronger & pain free (including themselves!) Tom & Jenni started to get a global online following after writing for magazines like <a href="https://www.boxrox.com/author/tom-morrison/" target="_blank">Boxrox</a>, <a href="https://www.t-nation.com/all-articles/authors/tom-morrison" target="_blank">T-Nation</a>, <a href="https://breakingmuscle.com/coaches/tom-morrison" target="_blank">Breaking Muscle</a>, and more!</p>
			<p>With Jenni’s design background and knowledge in computer wizardry she designed their first website, and in 2017 they launched their first version of the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method">Simplistic Mobility Method</a>, reaching so many more people worldwide!</p>
			<p>Since then Tom and Jenni have become proper business partners! Coaching people daily online, holding <a href="/seminars">workshops & seminars</a>, and sharing loads of free information on their <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A">YouTube channel</a>!</p>
		</div>
		
	</div>
	<div class="row py-5 mob-py-0">
		<div class="col-12">
			<h2 class="text-primary">Meet The Team!</h2>
			<picture>
				<source srcset="/img/about/team.webp" type="image/webp"/> 
				<source srcset="/img/about/team.jpg" type="image/jpeg"/>
				<img src="/img/about/team.jpg" class="img-fluid mb-4" alt="Tom morrison team." />
			</picture>
		</div>
	</div>   
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-tom.webp" type="image/webp"/> 
								<source srcset="/img/about/team-tom.png" type="image/png"/>
								<img src="/img/about/team-tom.png" class="img-fluid mb-0" alt="Meet the team - Tom Morrison"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Tom Morrison</h3>
							<p class="mb-2">Tom over-enthusiastically found fitness in his mid-20s but his extreme lack of flexibility caused him to become a walking injury (including an L4/L5 L5/S1 disc protrusion & extrusion with nerve impingement).</p>
							<p class="mb-2">He went from a CrossFit & Weightlifting coach & competitor… to not being able to put his own socks on.</p>
							<p class="mb-2">His journey back from his lowest point (both physically and mentally) is what makes him the globally renown coach he is today, helping 1000’s of people worldwide become pain free. </p>
							<p class="mb-0 mob-mb-3">Tom’s background is in Martial Arts, plus he’s a qualified Personal Trainer as well as qualified CrossFit, British Weightlifting & Powerlifting coach. Tom’s certified to teach Kettlebells, an FRC Mobility Specialist & a Pain-Free Performance Specialist. He constantly takes part in courses & certifications in rehab, S&C, movement, gymnastics, martial arts - literally anything he can find! Plus, he has incredible biceps 💪🏻</p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-tom.webp" type="image/webp"/> 
								<source srcset="/img/about/team-tom.png" type="image/png"/>
								<img src="/img/about/team-tom.png" class="img-fluid mb-0" alt="Meet the team - Tom Morrison" style="margin-top: -10px;" />
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-jenni.webp" type="image/webp"/> 
								<source srcset="/img/about/team-jenni.png" type="image/png"/>
								<img src="/img/about/team-jenni.png" class="img-fluid mb-0" alt="Meet the team - Jenni Sanders" style="margin-top: -90px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Jenni Sanders</h3>
							<p class="mb-2">Jenni is the core of the company and basically runs everything! She has always been active from a young age, but it was always an uphill battle.</p>
							<p class="mb-2">She has Joint Hypermobility Syndrome which causes her to struggle with everyday things.
							Walking would leave her close to tears, and doing the things she loved kept leading to worse & worse injuries.</p>
							<p class="mb-2">Jenni had two options: give up & accept she’d never be able to do the things she wanted or find a way to build strength.</p>
							<p class="mb-0 mob-mb-3">Of course, Jenni never gave up and would always put in the work! Tweaks, pains & injuries reduced as she became stronger, until she became the badass that we know her as today, including her remarkable shoulders!</p>
							
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-jenni.webp" type="image/webp"/> 
								<source srcset="/img/about/team-jenni.png" type="image/png"/>
								<img src="/img/about/team-jenni.png" class="img-fluid mb-0" alt="Meet the team - Jenni Sanders" style="margin-top: -50px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-jp.webp" type="image/webp"/> 
								<source srcset="/img/about/team-jp.png" type="image/png"/>
								<img src="/img/about/team-jp.png" class="img-fluid mb-0" alt="Meet the team - JP" style="margin-top: -40px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">JP Mitchell</h3>
							<p class="mb-2">JP is our Big Chief of Content! JP came onboard after many years of owning his own gym and now passes on his wealth of knowledge on our platforms & via the videos he helps us create & edit. JP has a meticulous care about everything that he does and has pushed our content to be better and better, constantly striving for excellence all while managing his two Golden pups Lilly & Poppy. </p>
							<p class="mb-2">While earning his bachelors of science in Human Biology he rowed on the Irish team for 6 years before focusing on the fitness industry with a Personal Training Diploma, CrossFit Trainer Level 2 along with  Gymnastics, Weightlifting and Rowing certifications.</p>
							<p class="mb-0 mob-mb-3">His attention to detail and ability to put in the work (even when it’s boring!) comes across in everything he does. When you see him move, train & coach you’ll see the years of discipline, hard work, and experience, plus, incredible quads. </p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-jp.webp" type="image/webp"/> 
								<source srcset="/img/about/team-jp.png" type="image/png"/>
								<img src="/img/about/team-jp.png" class="img-fluid mb-0" alt="Meet the team - JP" style="margin-top: -50px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-lindsey.webp" type="image/webp"/> 
								<source srcset="/img/about/team-lindsey.png" type="image/png"/>
								<img src="/img/about/team-lindsey.png" class="img-fluid mb-0" alt="Meet the team - Lindsey Rooney" style="margin-top: -110px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Lindsey Rooney</h3>
							<p class="mb-2">Lindsey is our Head of Marketing, so if our adverts have been following you around the Internet, it’s her fault!!</p>
							<p class="mb-2">Lindsey is a new Mummy and an absolute inspiration. She takes on any new challenge in her stride, and always with an infectious cheerfulness that lights up any room. Lindsey is a Pole Dance Instructor and has started studying for her Personal Trainer qualification!</p>
							<p class="mb-0 mob-mb-3">She’s definitely the most sociable out of us all and is always hitting us up with amazing locations for team trips. If you have ever seen her dance you’ll be mesmerised by her ability to go upside down and maintain an incredible toe point!</p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-lindsey.webp" type="image/webp"/> 
								<source srcset="/img/about/team-lindsey.png" type="image/png"/>
								<img src="/img/about/team-lindsey.png" class="img-fluid mb-0" alt="Meet the team - Lindsey Rooney" style="margin-top: -70px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-shelley.webp" type="image/webp"/> 
								<source srcset="/img/about/team-shelley.png" type="image/png"/>
								<img src="/img/about/team-shelley.png" class="img-fluid mb-0" alt="Meet the team - Shelley Morrison" style="margin-top: -140px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Shelley Morrison</h3>
							<p class="mb-2">Tom’s wife and Mummy of their two children Roxy & Seth, Shelley is our Head of Finance and Internal Admin. She was a Surgical Nurse before coming to work for the company full time and her level of care & compassion for everyone around her is felt by us all!</p>
							<p class="mb-0 mob-mb-3">Shelley has incredible patience which is lucky because if it wasn’t for her and Jenni putting up with Tom’s nonsense, the Flong & Sexible message never would have reached as far as it has. Tom suggested some of Shelley’s incredible attributes to write here…. and Tom now has to have a meeting with HR….</p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-shelley.webp" type="image/webp"/> 
								<source srcset="/img/about/team-shelley.png" type="image/png"/>
								<img src="/img/about/team-shelley.png" class="img-fluid mb-0" alt="Meet the team - Shelley Morrison" style="margin-top: -70px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-maciek.webp" type="image/webp"/> 
								<source srcset="/img/about/team-maciek.png" type="image/png"/>
								<img src="/img/about/team-maciek.png" class="img-fluid mb-0" alt="Meet the team - Maciej Rotko" style="margin-top: -110px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Maciej Rotko</h3>
							<p class="mb-2">Maciek is our YouTube Content Editor. He competed in many different sports in his youth and played basketball for 20 years before becoming hooked on CrossFit, then moving into competitive Weightlifting. Recently he’s competed at National, European & World level in Olympic Weightlifting! You may also see Maciek filming content on our Seminars & Workshops!</p>
							<p class="mb-0 mob-mb-3">As Jenni’s fiancé, his calm, confident attitude is the perfect balance to Tom & Jenni’s frantic energy. He’s a British Weightlifting coach who’ll be able to teach you a thing or two about lifting if you can take your eyes off his incredible calves!</p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-maciek.webp" type="image/webp"/> 
								<source srcset="/img/about/team-maciek.png" type="image/png"/>
								<img src="/img/about/team-maciek.png" class="img-fluid mb-0" alt="Meet the team - Maciej Rotko" style="margin-top: -70px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row my-5">
		<div class="col-12">
			<div class="card pt-4 pr-5 mob-pr-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 d-none d-lg-block">
							<picture> 
								<source srcset="/img/about/team-shannon.webp" type="image/webp"/> 
								<source srcset="/img/about/team-shannon.png" type="image/png"/>
								<img src="/img/about/team-shannon.png" class="img-fluid mb-0" alt="Meet the team - Shannon Muirhead" style="margin-top: -110px;"/>
							</picture>
						</div>
						<div class="col-lg-8">
							<h3 class="text-capitalize text-primary mb-3">Shannon Muirhead</h3>
							<p class="mb-2">Shannon is our Executive Admin & Customer Service and is charged with the task of trying to bring order to the madness that is Tom Morrison Limited! She’s blessed/cursed with Joint Hypermobility Syndrome and bringing a fresh eye into how we teach (She’s also started her Personal Trainer journey!)</p>
							<p class="mb-2">With a background in management, as we continue to grow Shannon will be the glue that holds everything together …and forces Tom to answer his million emails!</p>
							<p class="mb-0 mob-mb-3">An avid animal lover she has bunnies, a pig, and a horse! Shannon is taking to training with the team amazingly and after 7 years of managing a farm she’s coming built with incredible strength that keeps the rest of us on our toes!</p>
						</div>
						<div class="col-lg-4 d-lg-none">
							<picture> 
								<source srcset="/img/about/team-shannon.webp" type="image/webp"/> 
								<source srcset="/img/about/team-shannon.png" type="image/png"/>
								<img src="/img/about/team-shannon.png" class="img-fluid mb-0" alt="Meet the team - Shannon Muirhead" style="margin-top: -70px;"/>
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection
@section('scripts')
<style>
	.card{
		border: 1px solid #000;
	}
</style>
<script>
	var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
</script>
@endsection