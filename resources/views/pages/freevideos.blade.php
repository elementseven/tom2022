@php
$page = 'Free Videos';
$pagename = 'Free Videos';
$pagetitle = "Free Videos - Movement, Mobility and Strength videos";
$meta_description = "Free mobility advice & information by Tom Morrison about back pain, hip mobility exercises, shoulder mobility exercises, squat mobility, thoracic rotation, sciatica, ankle mobility, hamstring stretches, lifting technique and more!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('header')
<header class="container top-section">
	<div class="row">
		<div class="col-12">
			<div class="d-table">
				<div class="d-table-cell align-middle">
					<h1 class="page-title mb-3">FREE<br>VIDEOS</h1>
					<p class="mb-5 mob-mb-0">Find more free videos on my<br><a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank">YouTube channel!</a></p>
				</div>
			</div>
			<picture> 
                <source srcset="/img/tom-morrison-videos.webp" type="image/webp"> 
                <source srcset="/img/tom-morrison-videos.jpg" type="image/jpeg">
                <img src="/img/tom-morrison-videos.jpg" class="free-videos-image" alt="Tom morrison looking amazed." />
            </picture>
        </div>
	</div>
</header>
@endsection
@section('content')
<free-videos :count="{{$count}}"></free-videos>
@endsection