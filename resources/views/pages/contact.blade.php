@php
$page = 'Contact';
$pagename = 'Contact';
$pagetitle = "Contact - Get in touch with Tom Morrison";
$meta_description = "Speak to the man, the myth, the legend, Tom Morrison about training, mobility programs, online products, or about seminars, workshops.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('content')
<main class="container bg-white position-relative">
	<div class="row">
		<div class="col-12 pb-5">
			<picture> 
                <source srcset="/img/tom-morrison-get-in-touch.webp" type="image/webp"> 
                <source srcset="/img/tom-morrison-get-in-touch.jpg" type="image/jpeg">
                <img src="/img/tom-morrison-get-in-touch.jpg" class="contact-image d-none d-md-block" alt="Tom morrison touching another Tom Morrison on the bum" />
            </picture>
			<div class="row">
				<div class="col-lg-10">
					<h1 class="page-title mb-3 mt-5">GET IN TOUCH</h1>
				</div>
				<div class="col-lg-8">
					<p class="mb-5 mob-mb-0">I love talking to you guys, send me your progress videos/photos or your questions to <a href="mailto:support@tommorrison.uk">support@tommorrison.uk</a> or fill in the form below and I’ll get back to you as soon as I can!</p>
				</div>
				<div class="col-md-10 col-lg-7 pb-5">
					<contact-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></contact-form>
				</div>
			</div>
        </div>
        <div class="col-12 d-md-none">
        	<picture> 
                <source srcset="/img/tom-morrison-get-in-touch.webp" type="image/webp"> 
                <source srcset="/img/tom-morrison-get-in-touch.jpg" type="image/jpeg">
                <img src="/img/tom-morrison-get-in-touch.jpg" class="img-fluid" alt="Tom morrison touching another Tom Morrison on the bum" />
            </picture>
        </div>
	</div>
</main>
@endsection
@section('scripts')
<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">window.dojoRequire(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"073d52800d0030b24babff863","lid":"28a6096677","uniqueMethods":true}) })</script>
@endsection