@php
$page = 'Facts';
$pagename = 'Facts';
$pagetitle = "Tom Morrison Facts - everything you didn't know about the man";
$meta_description = "Find out more about the infamous Tom Morrison, for example, did you know that Tom Morrison once walked so fast he invented running?";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('header')
<header class="container-fluid bg-dark facts-top">
    <div class="row">
		<facts></facts>
	</div>
</header>
@endsection
@section('content')
<main class="container pt-5">
	<div class="row justify-content-center">
		<div class="col-md-4">
			<img src="/img/tom-morrison-facts.gif" class="img-fluid" alt="Tom Morrison with his hair blowing in the wind"/>
		</div>
	</div>
</main>
@endsection