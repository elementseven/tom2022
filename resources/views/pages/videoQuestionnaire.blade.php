@php
$page = 'Thank you for purchasing a Video Call!';
$pagename = 'Video Questionaire';
$pagetitle = "Thank you for purchasing a Video Call!";
$meta_description = "Please fill out this quick form and Tom will get back to you to arrange a time";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('content')
<main class="container bg-white position-relative">
	<div class="row">
		<div class="col-12 pb-5">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="mb-3 mt-5 text-primary" style="font-size: 3rem;">Thank you for purchasing a Video Call!</h1>
				</div>
				<div class="col-lg-10">
					<p class="">Please fill out this quick form and Tom will get back to you to arrange a time:</p>
				</div>
				<div class="col-md-12 pb-5">
					@if(!$currentUser)
					<video-questionnaire :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :name="''" :email="''"></video-questionnaire>
					@else
					<video-questionnaire :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :name="'{{$currentUser->full_name}}'" :email="'{{$currentUser->email}}'"></video-questionnaire>
					@endif
				</div>
			</div>
        </div>
	</div>
</main>
@endsection