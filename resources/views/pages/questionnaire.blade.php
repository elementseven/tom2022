@php
$page = 'Questionnaire';
$pagename = 'Questionnaire';
$pagetitle = "Questionnaire - Online Personal Coaching questionnaire";
$meta_description = "Please take the time to fill in this questionnaire honestly and thoroughly – it will be informing a lot of your new program so the more detail the better! Once complete, email it back to me and we can get started!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('content')
<main class="container bg-white position-relative">
	<div class="row">
		<div class="col-12 pb-5">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="mb-3 mt-5 text-primary" style="font-size: 3rem;">Online Personal Coaching Questionnaire</h1>
				</div>
				<div class="col-lg-10">
					<p class="mb-5 mob-mb-0">Please take the time to fill in this questionnaire honestly and thoroughly – it will be informing a lot of your new program so the more detail the better! Once complete, email it back to me and we can get started!</p>
				</div>
				<div class="col-md-12 pb-5">
					@if(!$currentUser)
					<questionnaire :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :name="''"></questionnaire>
					@else
					<questionnaire :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :name="'{{$currentUser->full_name}}'"></questionnaire>
					@endif
				</div>
			</div>
        </div>
	</div>
</main>
@endsection