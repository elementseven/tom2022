@php
$page = 'Gift Basket';
$pagename = 'Shop';
$pagetitle = 'Gift Basket | Purchase Tom Morrison Products for friends & family';
$meta_description = 'View your gift basket and make sure you have all the right products before completing your purchase.';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('header')
<header class="container pt-5">
	<div class="row">
		<div class="col-12">

			@if(session('duplicate-product'))
			<p class="text-danger d-sm-none">You already have {{session('duplicate-product')}} in your basket.</p>
			@endif
			@if(session('gift-still-in-basket'))
			<p class="text-danger d-sm-none">You still have items in your gift basket. Before you can purchase something for youself you must either complete your gift purchase or <u><a href="/empty-gift-basket">empty your gift basket</a></u>.</p>
			@endif
			@if(count(Cart::content()))
			<h1 class="text-primary checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0">Gift Basket<span class="checkout-title-line gift-checkout-title-line"></span><span class="text-light-grey">Checkout</span><span class="checkout-title-line gift-checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line my-4">
			@else
			<h1 class="page-title pt-5 pb-3">Gift Basket Empty</h1>
			<p class="">Oh no! You have nothing in your gift basket! Why not head back to the <a href="/shop">shop</a> and pick yourself out something nice...</p>
			<a href="/shop">
				<div class="btn btn-primary mb-5">Shop</div>
			</a>
			@endif

		</div>
	</div>
</header>
@endsection

@section('content')
<div class="container">
	<div class="bg-grey px-3 py-2 mb-4 text-center text-lg-left">
		<p class="mb-0 mob-text-small"><b>Buying as a gift! You'll enter the recipients details & gift date at checkout</b></p>
	</div>
</div>
@if(session('duplicate-product'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row">
		<div class="col-12">
			<p class="text-danger">You already have {{session('duplicate-product')}} in your basket.</p>
			@php session()->forget('duplicate-product'); @endphp
		</div>
	</div>
</div>
@endif
@if(session('gift-still-in-basket'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<p class="text-danger">You still have items in your gift basket. Before you can purchase something for youself you must either complete your gift purchase or <u><a href="/empty-gift-basket">empty your gift basket</a></u>.</p>
			@php session()->forget('gift-still-in-basket'); @endphp
		</div>
	</div>
</div>
@endif
@if(session('already-purchased'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row">
		<div class="col-12">
			<p class="text-danger">You already have already purchased {{session('already-purchased')}}. <a href="/login">Login to the members area</a> to download!</p>
			@php session()->forget('already-purchased'); @endphp
		</div>
	</div>
</div>
@endif
@if(count(Cart::content()))
<div class="container pb-5 pt-2">
	<div class="row">
		<div class="col-12">
			@php 
				$totalproducts = 0;
				foreach(Cart::content() as $cc){
					if($cc->options->type != 'merch'){
						$totalproducts++;
					}
				}
			@endphp
			@foreach(Cart::content() as $i)
			<div class="row half_row mb-3">
				<div class="col-md-2 col-3 half_col">
					@if($i->options->type == 'merch')
					<img src='{{$i->options->image}}' class="w-100" alt="{{$i->name}} picture"/>
					@elseif($i->options->type == 'bundle')
					<img src='{{$i->options->image}}' class="w-100" alt="{{$i->name}} picture"/>
					@else
					<div class="square bg" style="background-image: url('{{$i->model->getFirstMediaUrl('products','thumbnail')}}');"></div>
					@endif
				</div>
				<div class="col-md-5 col-9 half_col">
					<h3 class="mt-4 mob-mt-0 mob-smaller-title mob-mb-0">{{$i->name}}</h3>
					<div class="row">
						<div class="col-6 col-lg-12">
							<p class="mob-mb-0"><a href="{{route('remove-gift',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
						</div>
						<div class="col-6 col-lg-12 text-right d-lg-none">
							@if($totalproducts > 1 && $bundle->status == "Active" && $i->options->type != 'merch')
							<p class="mb-0 mob-mt-0"><s class="caption text-danger ">£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> £{{number_format((float)$i->price, 2, '.', '')}}</p>
							@else
								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								<p class="mb-0 mob-mt-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> &nbsp; <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
								@else
									@if($i->options->type == 'merch')
									<p class="mb-0 mob-mt-0">£{{number_format((float)($i->price * $i->qty), 2, '.', '')}}</p>
									<p class="small mt-1 mb-0 text-grey">Quantity - {{$i->qty}}</p>
									<p class="small mb-0 text-grey">Size - {{$i->options->size}}</p>
									@else
									<p class="mb-0 mob-mt-0">£{{number_format((float)$i->price, 2, '.', '')}}</p>
									@endif
								@endif
							@endif
						</div>
					</div>
				</div>
				<div class="col-md-5 col-4 text-right half_col d-none d-md-block">
					@if($totalproducts > 1 && $bundle->status == "Active" && $i->options->type != 'merch')
					<h3 class="mb-0 mt-4 mob-mt-0"><s class="caption text-danger ">£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> £{{number_format((float)$i->price, 2, '.', '')}}</h3>
					@else
						@if($i->options->original_price != NULL && $i->options->type != 'merch')
						<h3 class="mb-0 mt-4 mob-mt-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> &nbsp; <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></h3>
						@else
							@if($i->options->type == 'merch')
							<h3 class="mb-0 mt-4 mob-mt-0">£{{number_format((float)($i->price * $i->qty), 2, '.', '')}}</h3>
							<p class="small mt-1 mb-0 text-grey">Quantity - {{$i->qty}}</p>
							<p class="small mb-0 text-grey">Size - {{$i->options->size}}</p>
							@else
							<h3 class="mb-0 mt-4 mob-mt-0">£{{number_format((float)$i->price, 2, '.', '')}}</h3>
							@endif
						@endif
					@endif
				</div>
			</div>
			@endforeach
		</div>
		<div class="col-md-12 text-right">
			@if($totalproducts > 1 && $bundle->status == "Active")
			<p class="text-primary mb-0"><b>Your {{$bundle->value}}% bundle discount has been applied!</b></p>
			@endif
			@if(count(Cart::content()) > 1)
			<a href="/empty-gift-basket" class="grey-link"><i class="fa fa-trash-o"></i> Empty Basket</a>
			@endif
			<hr class="grey-line">
		</div>
	</div>
	<div class="row mt-5 mob-mt-0">
		<div class="col-lg-6">
			<div class="row">
				<div class="container mb-3 border-1 d-none d-lg-block">
					<div class="row half_row">
						<div class="col-2 text-center bg-grey pr-0 py-2 half_col">
							<img src="/img/graphics/currencies.svg" alt="we accept your currency" width="80" height="58" class="h-auto pt-2" />
						</div>
						<div class="col-10 col-lg-8 bg-grey py-2 half_col mob-px-3">
							<p class="mb-0"><b>We accept your currency!</b></p>
							<p class="mb-0 text-light-grey">Payments are converted at the current rate. No extra fees!</p>
						</div>
					</div>
				</div>
				<div class="container mb-3 border-1 d-none d-lg-block">
					<div class="row half_row">
						<div class="col-2 text-center bg-grey pr-0 py-2 half_col">
							<img src="/img/graphics/subscription.svg" alt="All our programs are one-time payments for lifetime access."  width="80" height="58" class="h-auto pt-2" />
						</div>
						<div class="col-10 col-lg-8 bg-grey py-2 half_col mob-px-3">
							<p class="mb-0"><b>No Subscription!</b></p>
							<p class="mb-0 text-light-grey">All our programs are one-time payments for lifetime access.</p>
						</div>
					</div>
				</div>
				@if(
					session('voucher_notification') == 'Sorry this voucher has expired.' ||
					session('voucher_notification') == 'Voucher not found.' ||
					!session('voucher_notification') && $voucher == null
				)
				@if($showvoucher > 0)
				<div class="container mob-pb-3">
					<label class="mb-1" for="voucher"><b>Got a voucher code?</b></label>
					<form action="{{route('gift-basket-apply-voucher')}}" method="post" class="row  half_row">
						{{csrf_field()}}
						<div class="col-6 half_col">
							<input class="form-control" type="text" id="voucher" name="voucher" placeholder="Enter Voucher Code"/>
						</div>
						<div class="col-4 half_col">
							<button type="submit" class="btn btn-grey">Apply</button>
						</div>
						@if(session('voucher_notification'))
						<div class="col-12 half_col">
							<p class="text-primary caption mt-2">{{session('voucher_notification')}}</p>
						</div>
						@endif
					</form>
				</div>
				@endif
				@elseif($voucher)
				<div class="container">
					<p class="text-primary text-center text-md-left"><b>{{$voucher->code}} Voucher Applied</b></p>
				</div>
				@endif
				@php session()->forget('voucher_notification'); @endphp
			</div>
		</div> 
		
		<div class="col-12 col-lg-6 text-right mob-text-left mob-mt-3">
			@if($totalproducts > 1 && $bundle->status == "Active")
			@php 
			$original_total = 0;
			foreach(Cart::content() as $i){
				$original_total = $original_total + ($i->options->original_price * $i->qty);
			}
			@endphp
			<p class="mb-0 text-center text-md-right"><s class="text-danger">£{{number_format((float)$original_total, 2, '.', '')}}</s></p>
			@endif
			<p class="text-center text-even-larger text-md-right title mb-0" style="font-weight: 300;">Total: £{{Cart::total()}}</p>
			<div class="d-block text-center text-lg-right w-100">
				<a href="{{route('gift-checkout')}}">
					<div class="btn btn-primary mt-2 mx-auto d-inline-block mb-2">Proceed to Checkout</div>
				</a>
			</div>
			<div class="d-block w-100 mb-2 text-center text-lg-right">
				<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="60" class="pp-logo d-inline mr-2"/>
				<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="30" class="m-logo d-inline mr-2"/>
				<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="40" class="v-logo d-inline mr-2"/>
				<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="20" class="am-logo d-inline"/>
			</div>
			@if(!Auth::check())
			<p class="text-center text-md-right text-light-grey text-small mob-px-3">If you already own any products, please <a href="/login?redirect=basket" class="text-light-grey"><u>sign in</u></a> before you checkout</p>
			@else
			<p class="text-center text-md-right mb-0">You’re logged in as  <b class="text-capitalize">{{$currentUser->full_name}}</b>, products will be added to your account.</p>
			<p class="text-center text-md-right"><a href="/not-you">Not you? Click here to log out.</a></p>
			@endif

			<div class="container mb-3 border-1 d-lg-none">
				<div class="row">
					<div class="col-3 text-center bg-grey pr-0 py-2">
						<img src="/img/graphics/currencies.svg" alt="we accept your currency" width="80" height="58" class="h-auto pt-2" />
					</div>
					<div class="col-9 col-lg-8 bg-grey py-2 pl-2">
						<p class="mb-0 mt-2 font-16-14"><b>We accept your currency!</b></p>
						<p class="mb-0 text-light-grey font-16-14" style="line-height: 1.4;">Payments are converted at the current rate. No extra fees!</p>
					</div>
				</div>
			</div>
			<div class="container mb-3 border-1 d-lg-none">
				<div class="row">
					<div class="col-3 text-center bg-grey pr-0 py-2">
						<img src="/img/graphics/subscription.svg" alt="All our programs are one-time payments for lifetime access."  width="80" height="58" class="h-auto pt-2" />
					</div>
					<div class="col-9 col-lg-8 bg-grey py-2 pl-2">
						<p class="mb-0 mt-2 font-16-14"><b>No Subscription!</b></p>
						<p class="mb-0 text-light-grey font-16-14" style="line-height: 1.4;">All our programs are one-time payments for lifetime access.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@if(count(Cart::content()))
<div class="container mt-5">
@else
<div class="container py-5 mb-5">
@endif
	<div class="row">
		<div class="col-12">
			@if(count(Cart::content()) > 1 && $bundle->status == "Active")
			<h3 class="text-primary mob-text-left mb-3">People also bought...</h3>
			@elseif(count(Cart::content()) == 1 && $bundle->status == "Active")
			<h3 class="text-primary mob-text-left">PSSST! Want an extra {{$bundle->value}}% off?</h3>
			<p>Add a second product and get an <b>extra {{$bundle->value}}% off your digital products!</b></p>
			@elseif(count(Cart::content()) == 0)
			<h3 class="text-primary mob-text-left">Why not one of these?</h3>
			<p>Add a product to your basket and get started on your way to better mobility &amp; strength!</b></p>
			@endif
		</div>
		@foreach($products as $p)
		<div class="col-md col-6 add-on-product">
			<a href="/gift-basket/add/{{$p->id}}">
				<div class="square bg" style="background-image: url('/storage/{{$p->image}}');"></div>
			</a> 
			<p class="title mt-2 mb-0"><a href="/gift-basket/add/{{$p->id}}" class="text-dark">{{$p->name}}</a></p>
			@if($p->sale_price == NULL)
			<p><a href="/gift-basket/add/{{$p->id}}" class="text-primary">&pound;{{$p->price}}</a></p>
			@else
			<p><a href="/gift-basket/add/{{$p->id}}" class="text-primary">&pound;{{$p->sale_price}}</a></p>
			@endif
		</div>
		@endforeach
	</div>
</div>
@if(count(Cart::content()))
<div class="container pb-5">
	<div class="row pt-5">
		<div class="col-12">
			<hr class="grey-line"/>
			<a href="{{route('gift-checkout')}}">
				<div class="btn btn-primary mt-2 mx-auto mb-3 float-right mob-no-float">Proceed to Checkout</div>
			</a>
		</div>
		<div class="col-12 text-center text-lg-right">
			<div class="d-block mb-2">
				<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="70" class="pp-logo d-inline mr-2"/>
				<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="35" class="m-logo d-inline mr-2"/>
				<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="50" class="v-logo d-inline mr-2"/>
				<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="25" class="am-logo d-inline"/>
			</div>
		</div>
		@if(!Auth::check())
		<div class="col-12">
			<p class="text-center text-md-right">If you already own any products, please <a href="/login?redirect=basket"><b>sign in</b></a> before you checkout</p>
		</div>
		@else
		<div class="col-12">
			<p class="text-center text-md-right mb-0">You’re logged in as  <b class="text-capitalize">{{$currentUser->full_name}}</b>, products will be added to your account.</p>
			<p class="text-center text-md-right"><a href="/not-you">Not you? Click here to log out.</a></p>
		</div>
		@endif
	</div>
</div>
@endif
@endsection

@section('scripts')

@endsection
