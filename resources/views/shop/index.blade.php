@php
$page = "Shop";
$pagename = 'Shop';
$pagetitle = "Shop - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison’s mobility programs and strength programs such as the Simplistic Mobility Method. They’re designed to balance your body, improve flexibility, build core strength, prevent injuries, aid rehab and improve lifting techniques";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('head_section')
<script>
// Measures product impressions and also tracks a standard
// pageview for the tag configuration.
// Product impressions are sent by pushing an impressions object
// containing one or more impressionFieldObjects.
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'GBP',                       // Local currency is optional.
    'impressions': [
	    @foreach($products as $key => $product)
	   {
	      "id": "{{$product->id}}",
	     'sku': "{{$product->id}}",
	     'name': "{{$product->name}}",
	     'brand': 'Tom Morrison',
	     'category': "{{$product->category->name}}",
	     'price': '{{$product->price}}',
	     'position': {{$key + 1}}
	   } @if($key != count($products) - 1) , @endif
	   @endforeach
	]
  }
});
</script>
@endsection
@section('content')
<shop :categories="{{$categories}}"></shop>
@endsection