@php
use App\Models\Product;
$page = 'Checkout';
$pagename = 'Shop';
$pagetitle = 'Checkout | Purchase Tom Morrison Products and Seminar Tickets';
$meta_description = 'View your basket and make sure you have all the right products and tickets before completing your purchase.';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('head_section')
<script>
	dataLayer.push({ ecommerce: null });
	dataLayer.push({
		event: 'begin_checkout',
		ecommerce: {
			currency: "GBP",
			value: {{number_format(Cart::total(), 2)}},
			items: [
	      @foreach(Cart::content() as $key => $i)
	      @php
	      if($i->options->type == 'product'){
	      	$product = Product::find($i->id);
	      	$cat = $product->category->name;
	      }else if($i->options->type == 'merch'){
	      	$cat = 'merch';
	      }else if($i->options->type == 'bundle'){
	      	$cat = 'bundle';
	      }else if($i->options->type == 'upgrade'){
	      	$cat = 'upgrade';
	      }
				
	      @endphp
	      {
	        item_id: "{{$i->id}}",
	        item_name: "{{$i->name}}",
	        item_category: "{{$cat}}",
	        price: {{number_format($i->price, 2)}}, 
	        quantity: 1
	      }@if($key != count(Cart::content()) - 1) , @endif
	      @endforeach
	    ]
		}
	});
</script>

<style>
	.make-me-sticky {
	  position: -webkit-sticky;
		position: sticky;
    top: 5px;
	}
</style>
@endsection
@section('header')
<header class="container pt-5">
	<div class="row">
		<div class="col-12">
			<h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Basket</span><span class="checkout-title-line "></span><span class="text-primary">Checkout</span><span class="checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line my-4">
			<order-status :stage="2"></order-status>
			<h2 class="text-primary smaller-title mob-mt-3 text-capitalize">You're nearly there!</h2>
			<h4>We just need a few details from you</h4>
			@if(session('account-created'))
			<div class="alert alert-success border-0 alert-dismissible fade show mt-4" role="alert">
			  <strong>Account Created!</strong> Please enter your payment details below.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
			@php session()->forget('account-created'); @endphp
			@endif
		</div>
	</div>
</header>
@endsection
@php 
$hasAddress = false;
$sameAddress = false;
if(Auth::check()){
	if(count($currentUser->addresses)){
		$hasAddress = true;
		foreach($currentUser->addresses as $a){
			if($a->type == 'billing'){
				$billingAddress = $a;
			}else if($a->type == 'shipping'){
				$shippingAddress = $a;
			}
		}
		if($billingAddress->streetAddress == $shippingAddress->streetAddress){
			$sameAddress = true;
		}
	}
}
$hasMerch = false;
foreach(Cart::content() as $cc){
	if($cc->options->type == 'merch'){
		$hasMerch = true;
	}
}
@endphp
@section('content')
<div class="container mt-3 mb-5 pb-5">
	<div class="row">
		<div class="col-lg-8">
			<div class="card border-0 shadow p-4 position-relative">
				@if($hasMerch 
					&& $hasAddress 
					&& $billingAddress->country != 'UK' 
					&& $billingAddress->country != 'GB' 
					&& $billingAddress->country != 'IE' 
					&& $shippingAddress->country != 'UK' 
					&& $shippingAddress->country != 'GB' 
					&& $shippingAddress->country != 'IE' 
					&& $billingAddress->region == null 
					&& $shippingAddress->region == null)
				<div id="address-warning">
					<p class="mimic-h3">Before you continue</p>
					<p>A part of your address is missing, in order to continue with your purchase please update it in your dashboard.</p>
					<a href="/dashboard/address">
						<button class="btn btn-primary">Update address</button>
					</a>
				</div>
				@else
				<div id="payment-form" class="row half_row">
					<div class="col-lg-12 pt-2">
						<ul class="basket-check pl-0">
							<li class="mb-0 py-0">
								<p class="mb-0">You are logged in as <b>{{$currentUser->full_name}}</b>.<br>Products will be added to your account.</p>
								<p class="mb-0 text-small mt-2"><a href="/not-you">Not you? Click here to log out.</a></p>
							</li>
						</ul>
					</div>
					<div class="col-12">
						<hr class="grey-line my-4"/>
					</div>
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-6">
								<p class="mb-0"><b>Billing Address</b></p>
								<p class="mb-0">{{$billingAddress->streetAddress}},<br/>{{$billingAddress->company}},<br/>@if($billingAddress->extendedAddress){{$billingAddress->extendedAddress}},<br/>@endif{{$billingAddress->city}} {{$billingAddress->postalCode}},<br/>{{$billingAddress->region}} {{$billingAddress->country}}</p>
							</div>
							<div class="col-lg-6 mob-mt-3">
								<p class="mb-0"><b>Shipping Address</b></p>
								<p class="mb-0">{{$shippingAddress->streetAddress}},<br/>{{$shippingAddress->company}},<br/>@if($shippingAddress->extendedAddress){{$shippingAddress->extendedAddress}},<br/>@endif{{$shippingAddress->city}} {{$shippingAddress->postalCode}},<br/>{{$shippingAddress->region}} {{$shippingAddress->country}}</p>
							</div>
							<div class="col-12 mt-2">
								<p class="mb-0 text-small"><a href="/dashboard/account-management#change-address">Address changed? Click here to update.</a></p>
							</div>
						</div>
					</div>
					<div class="col-12">
						<hr class="grey-line my-4"/>
						<div class="row">
							<div class="col-12">
								<p class="mb-2"><b>Enter Card Details</b></p>
								<stripe-checkout :stripekey="'{{env('STRIPE_KEY')}}'" :clientsecret="'{{$intent->client_secret}}'" :paymentmethod="'{{$intent->payment_method}}'" :total="{{Cart::total()}}" :returnurl="'{{env("APP_URL")}}/payment-success'"></stripe-checkout>
								{{-- <div id="payment-element" style="min-height:151px;"></div> --}}
							</div>
							
						</div>
						<hr class="grey-line my-4"/>
							<a href="{{ route('processTransaction') }}">
								<button id="paypalButton" type="button" class="btn btn-paypal mt-2 ml-auto mr-0 mob-mx-auto">Checkout</button>
							</a>
					</div>
				</div>
				@endif
				<p class="text-danger">{!!session('payment_error')!!}</p>
				@php session()->forget('payment_error'); @endphp
				@if(!Auth::check())
				<p id="form-warning" class="text-small text-right mb-1 text-primary mob-text-center" style="font-size: 12px;">Before you complete your payment, please fill in your card details.<br>If you are paying via PayPal, select PayPal above then click the yellow PayPal button.</p>
				@endif
			</div>
		</div>
		<div class="col-lg-4 d-none d-lg-block">
			@php 
			$totalproducts = 0;
			foreach(Cart::content() as $cc){
				if($cc->options->type != 'merch'){
					$totalproducts++;
				}
			}
			@endphp
			<div class="card shadow border-0 mob-mt-5 make-me-sticky">
				<div class="container p-4">
					<p class="text-large"><b>Basket</b></p>
					@foreach(Cart::content() as $i)
					<div class="row mb-3">
						<div class="col-8">
							<p class="mb-0">{{$i->name}}</p>

							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							@else
							@if($i->options->type == 'merch')
							<p class="small mb-0 text-light-grey">Quantity - {{$i->qty}} Size - {{$i->options->size}}</p>
							@else
							@endif
							@endif
							@if($i->options->type == 'merch')
							<p class="mob-mb-0 small"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@else
							<p class="mob-mb-0 small"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@endif
						</div>
						<div class="col-4 text-right">
		
							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							<p class="mb-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
							@else
							<p class="mb-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</p>
							@endif

						</div>
						<div class="col-12 text-right">
							<hr class="my-2" />
						</div>
					</div>
					@endforeach
					{{-- <p class="mb-0 text-center text-md-right"><s class="text-danger">£{{number_format((float)$original_total, 2, '.', '')}}</s></p> --}}
					<p class="text-right mb-0"><b><span class="text-primary">Total: </span> £<span id="total">{{Cart::total()}}</span></b></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="mob-basket" class="d-lg-none">
	<div class="container">
		<div class="row">
			<div class="col-12 px-0">
				<div class="card shadow border-0 p-3">
					<a data-toggle="collapse" href="#basketCollapse" role="button" aria-expanded="false" aria-controls="basketCollapse">
						<p class="mb-0 text-primary text-center collapsed"><b>View Basket <i class="fa fa-angle-up"></i></b></p>
						<p class="mb-0 text-primary text-center expanded"><b>Close Basket <i class="fa fa-angle-down"></i></b></p>
					</a>
					<div class="collapse" id="basketCollapse">
						@foreach(Cart::content() as $i)
						<div class="row mb-3 pt-4">
							<div class="col-8">
								<p class="mb-0">{{$i->name}}</p>

								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								@else
								@if($i->options->type == 'merch')
								<p class="small mb-0 text-light-grey">Quantity - {{$i->qty}} Size - {{$i->options->size}}</p>
								@else
								@endif
								@endif
								@if($i->options->type == 'merch')
								<p class="mob-mb-0 small"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
								@else
								<p class="mob-mb-0 small"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
								@endif
							</div>
							<div class="col-4 text-right">
								
								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								<p class="mb-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
								@else
								<p class="mb-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</p>
								@endif
				
							</div>
							<div class="col-12 text-right">
								<hr class="my-2" />
							</div>
						</div>
						@endforeach
						<p class="text-right mb-0"><b><span class="text-primary">Total: </span> £<span id="total">{{Cart::total()}}</span></b></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('prescripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
	document.getElementById('paypalButton').addEventListener('click', function() {
    document.getElementById('loader').classList.add('on');
    document.getElementById('loader-message').innerHTML = 'Redirecting to PayPal';
	});
</script>
@endsection
@section('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AVZYQtB-Sdv3bpKwyP5XMtAdN5Xq7bPSGjrgxoU3pR7KfScqekTu7sGr3PuUf5unet-BZ_ZpUjLqEwcG&currency=GBP"></script>
@endsection