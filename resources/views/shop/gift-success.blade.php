@php
$page = 'Gift Success';
$pagename = 'Shop';
$pagetitle = 'Success | Your purchase was successful';
$meta_description = 'You have successfully purchased the awesome Tom Morrison product(s) you wanted!';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('head_section')
<script>
  dataLayer.push({ ecommerce: null });
  dataLayer.push({
    event: "purchase",
    ecommerce: {
      transaction_id: "{{$invoice->transaction_id}}",
      value: {{number_format($invoice->price, 2)}},
      tax: 0,
      shipping: 0,
      currency: "GBP",
      payment_type: "{{$invoice->payment_method}}",
      items: [
        @foreach($invoice->products as $key => $product)
        @php
          if($product->sale_price > 0 && $product->sale_price < $product->price){
            $productprice = $product->sale_price;
          }else{
            $productprice = $product->price;
          }
          if(count($invoice->vouchers)){
            $voucher = $invoice->vouchers[0];
            $productprice = $productprice - (($productprice / 100) * $voucher->percent);
          }
          Cart::destroy();
        @endphp
        {
          item_id: "{{$product->id}}",
          item_name: "{{$product->name}}",
          affiliation: "Tom Morrison",
          item_category: "{{$product->category->name}}",
          price: {{number_format($productprice, 2)}}, 
          quantity: 1
        }@if($key != count($invoice->products) - 1) , @endif
        @endforeach
      ]
    }
  });
</script>
@endsection
@section('header')
<header class="container my-5">
  <div class="row">
    <div class="col-lg-10 text-left">
      <h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Gift Basket</span><span class="checkout-title-line gift-checkout-title-line"></span><span class="text-dark">Checkout</span><span class="checkout-title-line gift-checkout-title-line"></span><span class="text-primary">Success!</span></h1>
      <hr class="dark-line my-4">
      <order-status :stage="3"></order-status>
      <h3 class="mb-2 text-primary">Thanks for your Order!</h4>
      <p class="text-large"><b>Your payment has been processed successfully</b></p>
      <div class="bg-light p-3 mb-4" style="border-radius: 5px;">
        <p>An order confirmation has been sent to <span class="text-primary">{{$currentUser->email}}</span></p>
        <p class="mb-0">If you're a new customer, you'll also receive an <b>Account Created</b> email containing your password.<br/>(Please check both your inbox & spam/junk!)</p>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5">
  <div class="row pad_top mob_small_pad">
    <div class="col-12 text-left">
      <h4 class="mob_smaller">Gift Details:</h4>

      <p>Please check the details below are correct, and if you spot any issue email <a href="mailto:support@tommorrison.uk" style="color: #D82737;">support@tommorrison.uk</a></p>

      <p><b>Recipient Name:</b> {{$gift->first_name}} {{$gift->last_name}}</p>

      <p><b>Recipient Email:</b> {{$gift->email}}</p>

      <p><b>Product(s):</b> @foreach($gift->products as $p) {{$p->name}}, &nbsp; @endforeach </p>

      <p><b>Date to receive:</b> @if($gift->when == 'immediately') {{\Carbon\Carbon::now()->format('Y/m/d')}} @else {{$gift->date->format('Y/m/d')}} @endif</p>

      <hr class="line_full my-4">
    </div>
  </div>
    <div class="row pad_top mob_small_pad">
      <div class="col-sm-12 text-left">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <h4 class="mob_smaller">Order Details</h4>
            <p class="noprint mb-0">You can <span id="print_page" class="text-primary">print and keep this page</span> or keep a note of the payment reference.</p>
          </div>
       </div>
       <hr class="line_full my-4">
       <div class="row">

        <div class="col-md-8">

          <p class="mb-1"><b>Ref:</b> {{$invoice->id}}</p>
          <p class="bold"><b>Order made:</b> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($invoice->created_at))->format('g:i a - d/m/Y') }}</p>

        </div>

        <div class="col-md-4 text-right your_details_success">
          <h4 class="red_text ref_no mob_smaller">Your Details</h4>
          <p>{{$currentUser->first_name}} {{$currentUser->last_name}}<br>{{$currentUser->email}}</p>
          @if($invoice->shipping)
            @foreach($currentUser->addresses as $a)
              @if($a->type == 'shipping')
                <p class="mb-0"><b>Shipping Address</b></p>
                <p>@if($a->company){{$a->company}}<br/>@endif {{$a->streetAddress}}, @if($a->extendedAddress){{$a->extendedAddress}},@endif {{$a->city}}, {{$a->postalCode}}, <br/>  {{$a->region}}, {{$a->country}}</p>
              @endif
            @endforeach
          @endif
        </div>
      </div>
      @if(count($invoice->bundles))
        <hr class="line_full">
        <div class="row">
          <div class="col-sm-12 pad_small_top">
            <p class="text-title text-large mt-4 mb-3">Bundle(s)</p>
            <div class="row">
              @foreach($invoice->bundles as $key => $bundle)
              <div class="col-8">
                <p class="scrollFade text-larger" data-fade="fadeIn">{{$bundle->title}}</p>
              </div>
              <div class="col-4">
                <p class="scrollFade text-larger text-right" data-fade="fadeIn">@if($bundle->pivot->price < $bundle->price)<s class="text-primary text-small">£{{$bundle->price}}</s> - @endif£{{$bundle->pivot->price}}</p>
              </div>
              @endforeach 
              <div class="col-md-12">
                <hr class="mb-5">
              </div>
            </div>
          </div>
        </div>
        @endif
      @if(count($invoice->products))
      <hr class="line_full my-4">
      <div class="row">
        <div class="col-sm-12 pad_small_top">
          <h4 class="mb-4">Your Products</h4>
          <div class="row">
            @foreach($invoice->products as $key => $product)
            <div class="col-md-6">
              <p class="scrollFade large_p" data-fade="fadeIn"><b>{{$product->name}}</b></p>
            </div>
            <div class="col-md-6">
              <p class="scrollFade large_p text-right" data-fade="fadeIn"><b>£{{$product->pivot->price}}</b></p>
            </div>
            @endforeach 
            <div class="col-md-12">
              <hr class="mb-5">
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-sm-12 pad_small_top text-right">
          @if($invoice->shipping)
          <p class="mb-1"><b>Sub Total:</b> £{{number_format($invoice->price, 2)}}</p>
          <p><b>Shipping:</b>&nbsp; &pound;{{number_format($invoice->shipping, 2)}}</p>
          <h4 class="mb-3">Total:&nbsp; £{{number_format($invoice->shipping, 2) + number_format($invoice->price, 2)}}</h4>
          @else
          <h4 class="mb-3">Total:&nbsp; £{{number_format($invoice->price, 2)}}</h4>
          @endif
        </div>
      </div>
      <hr class="line_full mb-5">
      <div class="row">
        <div class="col-sm-12 pad_small_top text-right">
          <p class="text-right">To download your purchase's please login to <a href="/login">your account</a>.</p>
          <a href="/login">
            <div class="btn btn-primary float-right">Login</div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
 document.getElementById("print_page").addEventListener("click", function() {
    window.print();
});
</script>
@endsection