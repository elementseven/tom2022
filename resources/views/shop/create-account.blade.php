@php
$page = 'Checkout';
$pagename = 'Shop';
$pagetitle = 'Checkout | Purchase Tom Morrison Products and Seminar Tickets';
$meta_description = 'View your basket and make sure you have all the right products and tickets before completing your purchase.';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])

@section('head_section')
<style>
	.make-me-sticky {
	  position: -webkit-sticky;
		position: sticky;
    top: 5px;
	}
</style>
@endsection

@section('header')
<header class="container mt-5 mob-mt-0">
	<div class="row">
		<div class="col-12">
			<h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Basket</span><span class="checkout-title-line "></span><span class="text-primary">Checkout</span><span class="checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line mt-4">
			<order-status :stage="1"></order-status>
			<p><b>Already have an account? <a href="/login?redirect=checkout">Click here to login</a>.</b></p>
		</div>
	</div>
</header>
@endsection

@php 
	$hasAddress = false;
	$sameAddress = false;
	if(Auth::check()){
		if(count($currentUser->addresses)){
			$hasAddress = true;
			foreach($currentUser->addresses as $a){
				if($a->type == 'billing'){
					$billingAddress = $a;
				}else if($a->type == 'shipping'){
					$shippingAddress = $a;
				}
			}
			if($billingAddress->streetAddress == $shippingAddress->streetAddress){
				$sameAddress = true;
			}
		}
	}
	$hasMerch = false;
	foreach(Cart::content() as $cc){
		if($cc->options->type == 'merch'){
			$hasMerch = true;
		}
	}
@endphp

@section('content')
<div class="container mt-3 mb-5">
	<div class="row">
		<div class="col-lg-8">
			<div class="card border-0 shadow p-4">
				<form id="create-account-form" class="row half_row" action="/create-account" method="POST">
					{{ csrf_field() }}
					<div class="col-lg-12 half_col">
						<p class="mb-2"><b>Your Details</b></p>
					</div>
					@if(!Auth::check())
					<div class="col-6 half_col mb-3">
						<label class="mb-0 text-small" for="first_name"><b>First Name*</b></label>
						<input id="first_name" type="text" name="first_name" class="form-control mb-0" placeholder="First Name*" value="{{ old('first_name') }}" required/> 
						@if ($errors->has('first_name'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('first_name') }}</strong>
						</span>
						@endif
					</div>
					<div class="col-6 half_col mb-3">
						<label class="mb-0 text-small" for="first_name"><b>Last Name*</b></label>
						<input id="last_name" type="text" name="last_name" class="form-control mb-0" placeholder="Last Name*" value="{{ old('last_name') }}" required/> 
						@if ($errors->has('last_name'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('last_name') }}</strong>
						</span>
						@endif
					</div>
					<div class="col-sm-6 half_col mb-3">
						<label class="mb-0 text-small" for="email"><b>Email Address*</b></label>
						<input id="email" type="text" name="email" class="form-control mb-0" placeholder="Email Address*" value="{{ old('email') }}" required/> 
						@if ($errors->has('email'))
						<span class="help-block text-danger">
							<strong><a href="/login?redirect=checkout">{{ $errors->first('email') }} Please click here to login.</a></strong>
						</span>
						@endif
					</div>
					<div class="col-sm-6 half_col mb-3">
						<label class="mb-0 text-small" for="email-confirm"><b>Confirm Email Address*</b></label>
						<input id="email-confirm" type="text" name="email_confirmation" class="form-control mb-0" placeholder="Email Address*" value="{{ old('email-confirm') }}" required/> 
						@if ($errors->has('email-confirm'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('email-confirm') }}</strong>
						</span>
						@endif
					</div>
					<div class="col-lg-12 half_col">
						<hr class="my-4"/>
						<p class="mb-2"><b>Create your password	</b></p>
					</div>
					<div class="col-12 col-lg-6 half_col mb-3">
						<label class="mb-0 text-small" for="first_name"><b>Password*</b></label>
						<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
            @if ($errors->has('password'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
					</div>
					<div class="col-12 col-lg-6 half_col mb-3">
						<label class="mb-0 text-small" for="password-confirm"><b>Confirm Password*</b></label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
					</div>
					@else
					<div class="col-12 half_col">
						<p class="mb-0">You are logged in as <b>{{$currentUser->full_name}}</b>.<br>Products will be added to your account.</p>
						<p class=""><a href="/not-you">Not you? Click here to log out.</a></p>
					</div>
					@endif
					<div class="col-12 half_col">
						<hr class="my-4"/>
					</div>
					<div class="col-lg-12 half_col">
						<div class="row half_row">
							<div class="col-12 half_col">
								<p class="mb-2"><b>Billing Address</b></p>
							</div>
							<div class="col-12 half_col mb-3">
								<label class="mb-0 text-small" for="streetAddress"><b>Street Address*</b></label>
								<input id="streetAddress" type="text" name="streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ old('streetAddress') }}"required/> 
								@if ($errors->has('streetAddress'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('streetAddress') }}</strong>
								</span>
								@endif
							</div>
							<div class="col-sm-6 half_col col-lg-4 mb-3">
								<label class="mb-0 text-small" for="extendedAddress"><b>Apartment / Suite number</b></label>
								<input id="extendedAddress" type="text" name="extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ old('extendedAddress') }}"/> 
								@if ($errors->has('extendedAddress'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('extendedAddress') }}</strong>
								</span>
								@endif
							</div>
							<div class="col-6 half_col col-lg-4 mb-3">
								<label class="mb-0 text-small" for="city"><b>City*</b></label>
								<input id="city" type="text" name="city" class="form-control mb-0" placeholder="City*" value="{{ old('city') }}" required /> 
								@if ($errors->has('city'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('city') }}</strong>
								</span>
								@endif
							</div>
							<div class="col-6 half_col col-lg-4 mb-3">
								<label class="mb-0 text-small" for="postalCode"><b>Post Code*</b></label>
								<input id="postalCode" type="text" name="postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ old('postalCode') }}" required /> 
								@if ($errors->has('postalCode'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('postalCode') }}</strong>
								</span>
								@endif
							</div>
							<div class="half_col col-lg-4 mb-3">
								<label class="mb-0 text-small" for="country"><b>Country*</b></label>
								<countries :setcountry="'GB'" :inid="'country'"></countries>
								@if ($errors->has('country'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('country') }}</strong>
								</span>
								@endif
							</div>
							<div id="billing-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
								<label class="mb-0 text-small" for="region"><b>State / Region*</b></label>
								<input id="region" type="text" name="region" class="form-control mb-0" placeholder="State / Region" value="{{ old('region') }}"/> 
								@if ($errors->has('region'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('region') }}</strong>
								</span>
								@endif
							</div>
							<div class="col-sm-6 half_col col-lg-4 mb-3">
								<label class="mb-0 text-small" for="company"><b>Company</b></label>
								<input id="company" type="text" name="company" class="form-control mb-0" placeholder="Company" value="{{ old('company') }}"/> 
								@if ($errors->has('company'))
								<span class="help-block text-danger">
									<strong>{{ $errors->first('company') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>
					<div class="col-12 half_col">
						<hr class="my-4"/>
					</div>
					<div class="col-lg-12 half_col">
						<div class="row half_row">
							<div class="col-lg-12 half_col">
								<p class="mb-2"><b>Shipping Address</b></p>
							</div>
							<div class="col-lg-12 half_col">
								<div class="form-check">
								  <input style="margin-top:0.55rem;" id="same" type="checkbox" name="same" class="form-check-input" @if($hasAddress == false || $sameAddress == true) checked="checked" @endif>
								  <label class="form-check-label text-small" for="same">
								    Same as billing address
								  </label>
								</div>
							</div>
							<div id="shipping-address" class="container half_col d-none">
								<div class="row half_row">
									<div class="col-12 half_col mb-3 mt-4">
										<label class="mb-0 text-small" for="shipping-streetAddress"><b>Street Address*</b></label>
										<input id="shipping-streetAddress" type="text" name="shipping-streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ old('shipping-streetAddress') }}" /> 
										@if ($errors->has('shipping-streetAddress'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-streetAddress') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-sm-6 half_col col-lg-4 mb-3">
										<label class="mb-0 text-small" for="shipping-extendedAddress"><b>Apartment / Suite number</b></label>
										<input id="shipping-extendedAddress" type="text" name="shipping-extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ old('shipping-extendedAddress') }}"/> 
										@if ($errors->has('shipping-extendedAddress'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-extendedAddress') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-6 half_col col-lg-4 mb-3">
										<label class="mb-0 text-small" for="shipping-city"><b>City*</b></label>
										<input id="shipping-city" type="text" name="shipping-city" class="form-control mb-0" placeholder="City*"value="{{ old('shipping-city') }}" /> 
										@if ($errors->has('shipping-city'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-city') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-6 half_col col-lg-4 mb-3">
										<label class="mb-0 text-small" for="shipping-postalCode"><b>Post Code*</b></label>
										<input id="shipping-postalCode" type="text" name="shipping-postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ old('shipping-postalCode') }}" /> 
										@if ($errors->has('shipping-postalCode'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-postalCode') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-sm-6 half_col col-lg-4 mb-3">
										<label class="mb-0 text-small" for="shipping-country"><b>Country*</b></label>
										<countries :setcountry="'GB'" :inid="'shipping-country'"></countries>
										@if ($errors->has('shipping-country'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-country') }}</strong>
										</span>
										@endif
									</div>
									<div id="shipping-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
										<label class="mb-0 text-small" for="shipping-region"><b>State / Region*</b></label>
										<input id="shipping-region" type="text" name="shipping-region" class="form-control mb-0" placeholder="State / Region" value="{{ old('shipping-region') }}"/> 
										@if ($errors->has('shipping-region'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-region') }}</strong>
										</span>
										@endif
									</div>
									<div class="col-sm-6 half_col col-lg-4 mb-3">
										<label class="mb-0 text-small" for="shipping-company"><b>Company</b></label>
										<input id="shipping-company" type="text" name="shipping-company" class="form-control mb-0" placeholder="Company" value="{{ old('shipping-company') }}"/> 
										@if ($errors->has('shipping-company'))
										<span class="help-block text-danger">
											<strong>{{ $errors->first('shipping-company') }}</strong>
										</span>
										@endif

										<input id="shippingPrice" type="text" name="shipping-rate" class="d-none" value="{{ old('shipping-rate') }}"/> 

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 half_col">
						<hr class="my-4"/>
						<div class="row half_row">
							<div class="col-12 half_col">
								<div id="mailinglist_agree" class="d-block mb-3">
									<div class="form-check">
										<input id="mailinglist" type="checkbox" class="form-check-input" name="mailinglist"/>
										<label class="form-check-label text-small" for="mailinglist">Join our mailing list to get great mobility tips straight to your inbox and be the first to find out about exclusive offers!
										</label>
									</div>
								</div>
								<div id="agree_holder" class="d-block">
									<div class="form-check">
										<input id="agree" type="checkbox" class="form-check-input" name="agree"/>
										<label class="form-check-label text-small" for="agree">
											By ticking this box you give your consent for the personal information entered on this form to be stored in a secure database. Your information will not be shared or used for marketing and is only stored so you may log in and continue to use the features of this application. You may remove your information from our database by contacting me at any time. You also agree to the <a href="{{route('tandcs')}}" target="_blank">Terms &amp; conditions</a> and <a href="{{route('privacy-policy')}}">Privacy Policy</a>.
										</label>
									</div>
									@if ($errors->has('agree'))
									<span class="help-block text-danger">
										<strong>{{ $errors->first('agree') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="col-12 mt-4 mb-2 text-right">
							  <button id="submit-button" type="submit" class="btn btn-primary d-inline-block">Create Account</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<p class="text-danger">{!!session('payment_error')!!}</p>
			@php session()->forget('payment_error'); @endphp
			
		</div>
		<div class="col-lg-4 d-none d-lg-block">
			@php 
				$totalproducts = 0;
				foreach(Cart::content() as $cc){
					if($cc->options->type != 'merch'){
						$totalproducts++;
					}
				}
			@endphp
			<div class="card shadow border-0 mob-mt-5 make-me-sticky">
				<div class="container p-4">
					<p class="text-large"><b>Basket</b></p>
					@foreach(Cart::content() as $i)
					<div class="row mb-3">
						<div class="col-8">
							<p class="mb-0">{{$i->name}}</p>

							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							@else
							@if($i->options->type == 'merch')
							<p class="small mb-0 text-light-grey">Quantity - {{$i->qty}} Size - {{$i->options->size}}</p>
							@else
							@endif
							@endif
							@if($i->options->type == 'merch')
							<p class="mob-mb-0 small"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@else
							<p class="mob-mb-0 small"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@endif
						</div>
						<div class="col-4 text-right">
							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							<p class="mb-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
							@else
							<p class="mb-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</p>
							@endif
						</div>
						<div class="col-12 text-right">
							<hr class="my-2" />
						</div>
					</div>
					@endforeach
					<p class="text-right mb-0"><b><span class="text-primary">Total: </span> £<span id="total">{{Cart::total()}}</span></b></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="mob-basket" class="d-lg-none">
	<div class="container">
		<div class="row">
			<div class="col-12 px-0">
				<div class="card shadow border-0 p-3">
					<a data-toggle="collapse" href="#basketCollapse" role="button" aria-expanded="false" aria-controls="basketCollapse">
						<p class="mb-0 text-primary text-center collapsed"><b>View Basket <i class="fa fa-angle-up"></i></b></p>
						<p class="mb-0 text-primary text-center expanded"><b>Close Basket <i class="fa fa-angle-down"></i></b></p>
					</a>
					<div class="collapse" id="basketCollapse">
						@foreach(Cart::content() as $i)
						<div class="row mb-3 pt-4">
							<div class="col-8">
								<p class="mb-0">{{$i->name}}</p>

								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								@else
								@if($i->options->type == 'merch')
								<p class="small mb-0 text-light-grey">Quantity - {{$i->qty}} Size - {{$i->options->size}}</p>
								@else
								@endif
								@endif
								@if($i->options->type == 'merch')
								<p class="mob-mb-0 small"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
								@else
								<p class="mob-mb-0 small"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
								@endif
							</div>
							<div class="col-4 text-right">
								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								<p class="mb-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
								@else
								<p class="mb-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</p>
								@endif
							</div>
							<div class="col-12 text-right">
								<hr class="my-2" />
							</div>
						</div>
						@endforeach
						<p class="text-right mb-0"><b><span class="text-primary">Total: </span> £<span id="total">{{Cart::total()}}</span></b></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        // Function to check form inputs
        function checkInputs() {
            if ($('#first_name').val() != "" &&
                $('#last_name').val() != "" &&
                $('#email').val() != "" &&
                $('#agree').prop('checked') == true
            ) {
                $('#form-warning').hide();
                $('#submit-button').prop('disabled', false); // Enable the button
            } else {
                $('#submit-button').prop('disabled', true); // Disable the button
                $('#form-warning').show();
            }
        }

        // Event listeners for form inputs
        $('#first_name').keyup(checkInputs);
        $('#last_name').keyup(checkInputs);
        $('#email').keyup(checkInputs);
        $('#agree_holder').click(checkInputs);

        // Initially check form inputs
        checkInputs();

        // Other event listeners for form functionality
        $('#country').change(function() {
            var cou = $('#country').val();
            if (cou != 'GB' && cou != 'IE') {
                $('#billing-state').show();
                $('#region').prop('required', true);
            } else {
                $('#billing-state').hide();
                $('#region').prop('required', false);
            }
        });

        $('#shipping-country').change(function() {
            var shcou = $('#shipping-country').val();
            if (shcou != 'GB' && shcou != 'IE') {
                $('#shipping-state').show();
                $('#shipping-region').prop('required', true);
            } else {
                $('#shipping-state').hide();
                $('#shipping-region').prop('required', false);
            }
        });

        $('#same').click(function() {
            var isChecked = $('#same:checked').length > 0;
            if (isChecked == true) {
                $('#shipping-address').addClass('d-none');
            } else {
                $('#shipping-address').removeClass('d-none');
            }
        });

        // Form submission handling
        const form = document.getElementById('create-account-form');
        form.addEventListener('submit', async function(event) {
            document.getElementById('loader').classList.add('on');
            document.getElementById('loader-message').innerHTML = 'Creating Account';
        });
    });
</script>

@endsection
