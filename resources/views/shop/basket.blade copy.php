@php
$page = 'Basket';
$pagename = 'Shop';
$pagetitle = 'Basket | Purchase Tom Morrison Products and Seminar Tickets';
$meta_description = 'View your basket and make sure you have all the right products and tickets before completing your purchase.';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('header')
<header class="container">
	<div class="row">
		<div class="col-12">

			@if(session('duplicate-product'))
			<p class="text-danger d-sm-none">You already have {{session('duplicate-product')}} in your basket.</p>
			@endif
			@if(session('non-gift-still-in-basket'))
			<p class="text-danger d-sm-none">You still have items in your basket. Before you can purchase something as a gift you must either complete your purchase or <u><a href="/empty-basket">empty your basket</a></u>.</p>
			@endif
			@if(session('already-purchased'))
			<p class="text-danger d-sm-none">You already have already purchased {{session('already-purchased')}}. <a href="/login">Login to the members area</a> to download!</p>
			@endif

			@if(session('error'))
		    <div class="alert alert-danger alert-dismissible fade show" role="alert">{{ session('error') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button></div>
			@endif
			@if(isset($messages) && count($messages))
				@foreach($messages as $m)
			  <div class="alert alert-success alert-dismissible fade show" role="alert">{{$m}}<button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button></div>
			  @endforeach
			@endif

			@if(count(Cart::content()))
			<h1 class="text-primary checkout-title mob-mt-3 pt-5 pb-2 mob-mb-0 mob-pb-0">Basket<span class="checkout-title-line"></span><span class="text-light-grey">Checkout</span><span class="checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line my-4">
			@else
			<h1 class="page-title pt-5 pb-3">Basket Empty</h1>
			<p class="">Oh no! You have nothing in your basket! Why not head back to the <a href="/shop">shop</a> and pick yourself out something nice...</p>
			<a href="/shop">
				<div class="btn btn-primary mb-5">Shop</div>
			</a>
			@endif

		</div>
	</div>
</header>
@endsection

@section('content')
@if(session('non-gift-still-in-basket'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<p class="text-danger">You still have items in your basket. Before you can purchase something as a gift you must either complete your purchase or <u><a href="/empty-basket">empty your basket</a></u>.</p>
			@php session()->forget('non-gift-still-in-basket'); @endphp
		</div>
	</div>
</div>
@endif
@if(session('duplicate-product'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row">
		<div class="col-12">
			<p class="text-danger">You already have {{session('duplicate-product')}} in your basket.</p>
			@php session()->forget('duplicate-product'); @endphp
		</div>
	</div>
</div>
@endif
@if(session('already-purchased'))
<div class="container pad_top text-center d-none d-sm-block">
	<div class="row">
		<div class="col-12">
			<p class="text-danger">You already have already purchased {{session('already-purchased')}}. <a href="/login">Login to the members area</a> to download!</p>
			@php session()->forget('already-purchased'); @endphp
		</div>
	</div>
</div>
@endif
@if(count(Cart::content()))
<div class="container pb-5 pt-2">
	<div class="row">
		<div class="col-12">
			@php 
				$totalproducts = 0;
				foreach(Cart::content() as $cc){
					if($cc->options->type != 'merch'){
						$totalproducts++;
					}
				}
			@endphp
			@foreach(Cart::content() as $i)
			<div class="row half_row mb-3">
				<div class="col-md-2 col-3 half_col">
					@if($i->options->type == 'merch')
					<img src='{{$i->options->image}}' class="w-100" alt="{{$i->name}} picture"/>
					@else
					<div class="square bg" style="background-image: url('{{$i->model->getFirstMediaUrl('products','thumbnail')}}');"></div>
					@endif
				</div>
				<div class="col-md-5 col-9 half_col">
					<h3 class="mt-4 mob-mt-0 mob-smaller-title mob-mb-0">{{$i->name}}</h3>
					<div class="row">
						<div class="col-6 col-lg-12">
							@if($i->options->type == 'merch')
							<p class="mob-mb-0"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@else
							<p class="mob-mb-0"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@endif
						</div>
						<div class="col-6 col-lg-12 text-right d-lg-none">
							@if($totalproducts > 1 && $bundle->status == "Active" && $i->options->type != 'merch')
							<p class="mb-0 mob-mt-0"><s class="caption text-danger ">£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> £{{number_format((float)$i->price, 2, '.', '')}}</p>
							@else
								@if($i->options->original_price != NULL && $i->options->type != 'merch')
								<p class="mb-0 mob-mt-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> &nbsp; <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
								@else
									@if($i->options->type == 'merch')
									<p class="mb-0 mob-mt-0">£{{number_format((float)($i->price * $i->qty), 2, '.', '')}}</p>
									<p class="small mt-1 mb-0 text-grey">Quantity - {{$i->qty}}</p>
									<p class="small mb-0 text-grey">Size - {{$i->options->size}}</p>
									@else
									<p class="mb-0 mob-mt-0">£{{number_format((float)$i->price, 2, '.', '')}}</p>
									@endif
								@endif
							@endif
						</div>
					</div>
				</div>
				<div class="col-md-5 col-4 text-right half_col d-none d-md-block">
					@if($totalproducts > 1 && $bundle->status == "Active" && $i->options->type != 'merch')
					<h3 class="mb-0 mt-4 mob-mt-0"><s class="caption text-danger ">£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> £{{number_format((float)$i->price, 2, '.', '')}}</h3>
					@else
						@if($i->options->original_price != NULL && $i->options->type != 'merch')
						<h3 class="mb-0 mt-4 mob-mt-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> &nbsp; <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></h3>
						@else
							@if($i->options->type == 'merch')
							<h3 class="mb-0 mt-4 mob-mt-0 ">£{{number_format((float)($i->price * $i->qty), 2, '.', '')}}</h3>
							<p class="small mt-1 mb-0 text-grey">Quantity - {{$i->qty}}</p>
							<p class="small mb-0 text-grey">Size - {{$i->options->size}}</p>
							@else
							<h3 class="mb-0 mt-4 mob-mt-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</h3>
							@endif
						@endif
					@endif
				</div>
			</div>
			@endforeach
		</div>
		<div class="col-md-12 text-right">
			@if($totalproducts > 1 && $bundle->status == "Active")
			<p class="text-primary mb-0"><b>Your {{$bundle->value}}% bundle discount has been applied!</b></p>
			@endif
			@if(count(Cart::content()) > 1)
			<a href="/empty-basket" class="grey-link"><i class="fa fa-trash-o"></i> Empty Basket</a>
			@endif
			<hr class="grey-line">
		</div>
	</div>
	<div class="row mt-5 mob-mt-0">
		<div class="col-lg-6">
			<div class="row">
				<div class="container mb-3 border-1 d-none d-lg-block">
					<div class="row half_row">
						<div class="col-2 text-center bg-grey pr-0 py-2 half_col">
							<img src="/img/graphics/currencies.svg" alt="we accept your currency" width="80" height="58" class="h-auto pt-2" />
						</div>
						<div class="col-10 col-lg-8 bg-grey py-2 half_col mob-px-3">
							<p class="mb-0"><b>We accept your currency!</b></p>
							<p class="mb-0 text-light-grey">We charge in GBP (£), your payment will be converted at the current rate by your bank.</p>
						</div>
					</div>
				</div>
				<div class="container mb-3 border-1 d-none d-lg-block">
					<div class="row half_row">
						<div class="col-2 text-center bg-grey pr-0 py-2 half_col">
							<img src="/img/graphics/subscription.svg" alt="All our programs are one-time payments for lifetime access."  width="80" height="58" class="h-auto pt-2" />
						</div>
						<div class="col-10 col-lg-8 bg-grey py-2 half_col mob-px-3">
							<p class="mb-0"><b>No Subscription!</b></p>
							<p class="mb-0 text-light-grey">All our programs are one-time payments for lifetime access.</p>
						</div>
					</div>
				</div>
				@if(
					session('voucher_notification') == 'Sorry this voucher has expired.' ||
					session('voucher_notification') == 'Voucher not found.' ||
					!session('voucher_notification') && $invoice && count($invoice->vouchers) == 0
				)
				@if($showvoucher > 0)
				<div class="container mob-pb-3">
					<label class="mb-1" for="voucher"><b>Got a voucher code?</b></label>
					<form action="{{route('apply-voucher')}}" method="post" class="row  half_row">
						{{csrf_field()}}
						<div class="col-6 half_col">
							<input class="form-control" type="text" id="voucher" name="voucher" placeholder="Enter Voucher Code"/>
						</div>
						<div class="col-4 half_col">
							<button type="submit" class="btn btn-grey">Apply</button>
						</div>
						@if(session('voucher_notification'))
						<div class="col-12 half_col">
							<p class="text-primary caption mt-2">{{session('voucher_notification')}}</p>
						</div>
						@endif
					</form>
				</div>
				@endif
				@elseif($invoice && count($invoice->vouchers))
				<div class="container">
					<p class="text-primary text-center text-md-left"><b>{{$invoice->vouchers[0]->code}} Voucher Applied</b></p>
				</div>
				@endif
				@php session()->forget('voucher_notification'); @endphp
			</div>
		</div> 
		
		<div class="col-12 col-lg-6 text-right mob-text-left mob-mt-3">
			@if($totalproducts > 1 && $bundle->status == "Active")
			@php 
			$original_total = 0;
			foreach(Cart::content() as $i){
				$original_total = $original_total + ($i->options->original_price * $i->qty);
			}
			@endphp
			<p class="mb-0 text-center text-md-right"><s class="text-danger">£{{number_format((float)$original_total, 2, '.', '')}}</s></p>
			@endif
			<p class="text-center text-even-larger text-md-right title mb-0" style="font-weight: 300;">Total: £{{Cart::total()}}</p>
			<div class="d-block text-center text-lg-right w-100">
				<a href="{{route('checkout')}}">
					<div class="btn btn-primary mt-2 mx-auto d-inline-block mb-2">Proceed to Checkout</div>
				</a>
			</div>
			<div class="d-block w-100 mb-2 text-center text-lg-right">
				<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="60" class="pp-logo d-inline mr-2"/>
				<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="30" class="m-logo d-inline mr-2"/>
				<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="40" class="v-logo d-inline mr-2"/>
				<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="20" class="am-logo d-inline"/>
			</div>
			@if(!Auth::check())
			<p class="text-center text-md-right text-light-grey text-small mob-px-3">If you already own any products, please <a href="/login?redirect=basket" class="text-light-grey"><u>sign in</u></a> before you checkout</p>
			@else
			<p class="text-center text-md-right mb-0">You’re logged in as  <b class="text-capitalize">{{$currentUser->full_name}}</b>.<br/>Products will be added to your account.</p>
			<p class="text-center text-md-right"><a href="/not-you" class="text-small">Not you? Click here to log out.</a></p>
			@endif

			<div class="container mb-3 border-1 d-lg-none">
				<div class="row">
					<div class="col-3 text-center bg-grey pr-0 py-2">
						<img src="/img/graphics/currencies.svg" alt="we accept your currency" width="80" height="58" class="h-auto pt-2" />
					</div>
					<div class="col-9 col-lg-8 bg-grey py-2 pl-2">
						<p class="mb-0 mt-2 font-16-14"><b>We accept your currency!</b></p>
						<p class="mb-0 text-light-grey font-16-14" style="line-height: 1.4;">We charge in GBP (£), but your payment will be converted at the current rate by your bank.</p>
					</div>
				</div>
			</div>
			<div class="container mb-3 border-1 d-lg-none">
				<div class="row">
					<div class="col-3 text-center bg-grey pr-0 py-2">
						<img src="/img/graphics/subscription.svg" alt="All our programs are one-time payments for lifetime access."  width="80" height="58" class="h-auto pt-2" />
					</div>
					<div class="col-9 col-lg-8 bg-grey py-2 pl-2">
						<p class="mb-0 mt-2 font-16-14"><b>No Subscription!</b></p>
						<p class="mb-0 text-light-grey font-16-14" style="line-height: 1.4;">All our programs are one-time payments for lifetime access.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@if(count(Cart::content()))
<div class="container mt-5">
@else
<div class="container py-5 mb-5">
@endif
	@foreach(Cart::content() as $i)
	@if($i->id == 1 && $i->options->type != 'merch')
	<div class="row mb-5">
		<div class="col-lg-10">
			<h3 class="mb-3">Frequently asked questions about SMM</h3>
			<p class="mb-4 caption">*Click a question to reveal the answer</p>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1"><b>Is SMM really worth it?</b></a></p>
            <div id="answer_1" class="collapse">
                <p>Straight to it, eh?? Ok, as an example, when I hurt my knee I spent 6 weeks of physio on it - that cost me about £300. When I hurt my shoulder that was 3 sessions; £150.</p>
				<p>When I badly injured my back I pretty much went every week for 6 months straight just to get some relief... I don’t even want to calculate that.</p>
				<p>The Simplistic Mobility Method is £{{$i->price}} once. For a lifetime of benefits.</p>
				<p>You will learn about your body, how to spot injuries before they happen, it is a tried and tested method with real athletes. After people have bought SMM they have found that they no longer need to go to Physio as often, or even at all.</p>
				<p>Why waste time when you have to rest or rehab after an injury, when you could just prevent it in the first place?</p>
				<p>Plus… as an added bonus you will receive lifetime access to your online account where you access SMM, and automatic updates and upgrades of any improvements we make to the program in future. It really is a lifetime of awesomeness.</p>
				<p>PLUS! As an extra added bonus, you’ll become a part of a global community of SMMers via our facebook group, where you can ask questions directly to people who’ve been using the method for years, and directly to Tom & Jenni who are very active in the SMM community.</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2"><b>Do I really need SMM?</b></a></p>
            <div id="answer_2" class="collapse">
                <p>After working with so many people over the years, there are always things we miss about training, mobility, and recovery – no matter how much research and practice you do. You will pick up imbalances and compensations over years of small mistakes, misinterpretations and even daily habits.</p>
				<p>The Simplistic Mobility Method allows you to learn what your body can and can’t do right now - the best injury is the one that never happened. And you can use the tests & methodology to keep maintaining and checking your body throughout your entire lifetime.</p>
				<p>SMM is designed to make your joints sit and work correctly and teaches your body to move as one unit with great muscle activation.</p>
				<p>It doesn’t matter what you like to do for physical activity, everyone has the same joints and muscles and they need to have a certain baseline mobility & stability to function well; SMM is that baseline. </p>
				<p>Your body won’t be sore all the time if it moves well.</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_3" aria-expanded="false" aria-controls="answer_3"><b>What happens after I buy SMM? Where do I access the program?</b></a></p>
            <div id="answer_3" class="collapse">
                <p>After you buy, you’ll receive an email with your login details and a link to sign in to your new dashboard!</p>
				<p>If you don’t receive an email, make sure to check your spam/junk folder. If it’s not there either, simply go to https://tommorrison.uk/login and click “Forgot Your Password?” to generate a new sign in email for you. Some email hosts don’t like automatic emails and block them to protect you from spam!</p>
				<p>Once you’re signed in, you will see the Simplistic Mobility Method on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily.</p>
				<p>Everything is stored online for you, and any updates we make to the program will automatically appear for you.</p>
				<p>If you ever forget your password, don’t worry! The “Forgot Your Password” feature is always there! You won’t lose your account.</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_4" aria-expanded="false" aria-controls="answer_4"><b>How long will it take for me to see results?</b></a></p>
            <div id="answer_4" class="collapse">
                <p>After your first session. Yeah, really!</p>
				<p>In your first session you run through a series of tests before and after. Some people notice visual improvements right away, others just feel better, taller or that they are moving easier. Surprisingly, many are shocked at how much they struggled with.</p>
				<p>After that, as you’d expect, the more you use the program the faster you see results. Most people tell us it takes about 8 sessions to make a real difference to their body and their training. So if you can manage 4 times per week - that’s only 2 weeks to awesome.</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_5" aria-expanded="false" aria-controls="answer_5"><b>How long does it take to do SMM? Will I actually stick with it?</b></a></p>
            <div id="answer_5" class="collapse">
                <p>An SMM session will take about 20 minutes, so it is pretty easy to slot into even a busy timetable. We’d recommend between 2-5 sessions per week, depending on how much time you have available.</p>
				<p>The better you get at SMM the less frequently you’ll need to go through the whole routine. You can pick the movements that benefit you the most and throw them into warm ups & cool downs. We also go through a way you can condense your mobility practice into a 5 minute-a-day routine!</p>
				<p>Your very first session will take longer, as you’ll go through a full body assessment and you’ll want to watch the videos to check the technique, tips and progressions. You’ve also got optional checklists and note sheets that you can use to mark your progress.</p>
				<p>After that, 20 minute sessions! 4 times a week if possible but if you can only manage 2 times that’s still more than none.</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_6" aria-expanded="false" aria-controls="answer_6"><b>I’m not very flexible, is SMM suitable for total beginners?</b></a></p>
            <div id="answer_6" class="collapse">
                <p>Don’t worry, trying to get flexible before starting a mobility program is like trying to learn to drive before your first lesson.</p>
				<p>The Simplistic Mobility Method is what you need to increase your flexibility in a safe way! Not just stretching for no reason, but intentional, directed exercises to build both mobility and stability.</p>
				<p>The exercises are laid out in such a way that show you how to regress them no matter where you are at! And on the flip side of that! If you are too flexible then the movements are perfect for you to develop control over to add more stability and confidence to your body!</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_7" aria-expanded="false" aria-controls="answer_7"><b>I do/play a sport will SMM help me?</b></a></p>
            <div id="answer_7" class="collapse">
                <p>Yup, the Simplsitic Mobility Method is designed to help the human body – regardless of what sport that body does.</p>
				<p>In our SMM community group we have footballers, crossfitters, kettlebell athletes, powerlifters, weightlifters, rugby players, runners, normal gym goers, bodybuilders, coaches, physiotherapists, bodyweight enthusiasts, pole dancers, even people that aren’t interested in fitness and just want to feel good in their day to day life!</p>
				<p>We all have a body, sports and activities is all the fun stuff after your moves well. SMM is like the background operating systems of your phone to keep it running smoothly. If there’s a ‘bug’ in a specific area, your body will not perform the way it should!</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_8" aria-expanded="false" aria-controls="answer_8"><b>Can I do SMM alongside my own training?</b></a></p>
            <div id="answer_8" class="collapse">
                <p>Yep! We would argue that SMM is an essential part of your training. It runs through everything you need for good performance.</p>
				<p>No more wasting time trying to figure out what’s causing that niggle, rolling out tightness, or getting stuck as a plateau, SMM gives you the tools to actively support and progress your training.<br>It is an indispensable extra to your training!</p>
				<p>However, if you were really in a bad way for a long time and your test show that you have a lot to work on, then I would focus solely on SMM for a few weeks and then return to training when you have the movements at a good level!</p>
            </div>
            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_9" aria-expanded="false" aria-controls="answer_9"><b>Do I need equipment to do SMM?</b></a></p>
            <div id="answer_9" class="collapse">
                <p>Nope, not at all! Every exercise can be done at home, at the gym, while travelling... any where!</p>
				<p>Every movement we use is bodyweight. The aim is to teach you how to move using your own body as the main tool, rather than relying on external forces!</p>
				<p class="mb-0">So,</p>
				<ul>
					<li><p class="mb-0">It educates you!</p></li>
					<li><p class="mb-0">It's tried & tested!</p></li>
					<li><p class="mb-0">Makes you feel awesome!</p></li>
					<li><p class="mb-0">Helps treat & prevent injuries!</p></li>
					<li><p class="mb-0">No equipment needed!</p></li>
					<li><p class="mb-0">It’s a one off payment!</p></li>
					<li><p class="mb-0">It has lifetime benefits!</p></li>
					<li><p class="mb-0">There's an active, global community group!</p></li>
					<li><p class="mb-0">You get direct contact with Tom & Jenni</p></li>
				</ul>
				<p>What are you waiting for!?</p>
            </div>
        </div>
    </div>
    @endif
    @endforeach
	<div class="row">
		<div class="col-12">
			@if(count(Cart::content()) > 1 && $bundle->status == "Active")
			<h3 class="text-primary mob-text-left mb-3">People also bought...</h3>
			@elseif(count(Cart::content()) == 1 && $bundle->status == "Active")
			<h3 class="text-primary mob-text-left">PSSST! Want an extra {{$bundle->value}}% off?</h3>
			<p>Add a second product and get an <b>extra {{$bundle->value}}% off your digital products!</b></p>
			@elseif(count(Cart::content()) == 0)
			<h3 class="text-primary mob-text-left">Why not one of these?</h3>
			<p>Add a product to your basket and get started on your way to better mobility &amp; strength!</b></p>
			@endif
		</div>
		@foreach($products as $p)
		<div class="col-md col-6 add-on-product">
			<a href="/basket/add/{{$p->id}}">
				<div class="square bg" style="background-image: url('/storage/{{$p->image}}');"></div>
			</a> 
			<p class="title mt-2 mb-0"><a href="/basket/add/{{$p->id}}" class="text-dark">{{$p->name}}</a></p>
			@if($p->sale_price == NULL)
			<p><a href="/basket/add/{{$p->id}}" class="text-primary">&pound;{{$p->price}}</a></p>
			@else
			<p><a href="/basket/add/{{$p->id}}" class="text-primary">&pound;{{$p->sale_price}}</a></p>
			@endif
		</div>
		@endforeach
	</div>
</div>
@if(count(Cart::content()))
<div class="container pb-5">
	<div class="row pt-5">
		<div class="col-12">
			<hr class="grey-line"/>
			<a href="{{route('checkout')}}">
				<div class="btn btn-primary mt-2 mx-auto mb-3 float-right mob-no-float">Proceed to Checkout</div>
			</a>
		</div>
		<div class="col-12 text-center text-lg-right">
			<div class="d-block mb-2">
				<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="70" class="pp-logo d-inline mr-2"/>
				<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="35" class="m-logo d-inline mr-2"/>
				<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="50" class="v-logo d-inline mr-2"/>
				<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="25" class="am-logo d-inline"/>
			</div>
		</div>
		@if(!Auth::check())
		<div class="col-12">
			<p class="text-center text-md-right">If you already own any products, please <a href="/login?redirect=basket"><b>sign in</b></a> before you checkout</p>
		</div>
		@else
		<div class="col-12">
			<p class="text-center text-md-right mb-0">You’re logged in as  <b class="text-capitalize">{{$currentUser->full_name}}</b>, products will be added to your account.</p>
			<p class="text-center text-md-right"><a href="/not-you">Not you? Click here to log out.</a></p>
		</div>
		@endif
	</div>
</div>
@endif
@endsection

@section('scripts')

@endsection
