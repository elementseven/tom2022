@php
$page = 'Checkout Error';
$pagename = 'Shop';
$pagetitle = 'Checkout Error | Something went wrong';
$meta_description = 'Something went wrong with your payment';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])

@section('head_section')
<script src="https://js.stripe.com/v3/"></script>
<style>
	.make-me-sticky {
	  position: -webkit-sticky;
		position: sticky;
    top: 5px;
	}
</style>
@endsection

@section('header')
<header class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0">Something went wrong!</h1>
			<hr class="dark-line my-4">
		</div>
	</div>
</header>
@endsection

@section('content')
<div class="container mt-3 mb-5 pb-5">
	<div class="row">
		<div class="col-12">
			<div class="card border-0 shadow p-4">
				<p><b>There was an error when processing your payment.</b></p>

				@if(count($errors))
				<p>The error returned from the payment processor is detailed below:</p>
				@foreach ($errors->all() as $error)

				  <p class="text-primary">{{ $error }}</p>
				  <hr/>
				@endforeach

				@endif

				<p>Please return to your basket and try another payment method in the checkout.</p>

				<a href="/basket">
					<button type="button" class="btn btn-primary">Back to basket</button>
				</a>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')

@endsection
