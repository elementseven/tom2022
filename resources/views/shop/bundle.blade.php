@php
$page = "light";
$pagename = 'Shop';
$pagetitle = $bundle->title . " - Movement | Mobility | Strength - Tom Morrison";
$meta_description = $bundle->exerpt;
$og_image = "https://tommorrison.uk/storage/" . $bundle->image;
if($bundle->sale_price){
	$price = $bundle->sale_price;
}else{
	$price = $bundle->price;
}
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('head_section')
<script>
	dataLayer.push({ ecommerce: null });
	dataLayer.push({
		event: "view_item",
	  ecommerce: {
	  	currency: "GBP",
	  	value: {{$price}},
	    items: [{
	      item_name: "{{$bundle->title}}",       
	      item_id: "{{$bundle->id}}",
	      price: {{number_format($price, 2)}},
	      affiliation: 'Tom Morrison',
	      item_category: "Bundles"
	     }]
	   }
	});
</script>
<style type="text/css">
	body h1 .tm {
		top: -1.2rem;
	}
</style>
@endsection
@section('header')
<header class="container py-5 mob-pt-0">
	<div class="row">
		<div class="col-lg-10 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">{{$bundle->title}}</h1>
			@if(count($reviews))
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
			@endif
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<picture> 
				<source srcset="{{$bundle->getFirstMediaUrl('bundles', 'normal-webp')}}" type="image/webp"/> 
				<source srcset="{{$bundle->getFirstMediaUrl('bundles', 'normal')}}" type="{{$bundle->getFirstMedia('bundles')->mime_type}}"/>
				<img src="{{$bundle->getFirstMediaUrl('bundles', 'normal')}}" type="{{$bundle->getFirstMedia('bundles')->mime_type}}" width="730" height="326" class="w-100 h-auto" alt="{{$bundle->title}} - Tom Morrison" />
			</picture>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($bundle->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$bundle->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$bundle->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$bundle->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Save {{number_format((($bundle->price - $bundle->sale_price)*100) /$bundle->price, 0)}}%!</span></p>
						<p class="mob-mb-0">
							<a href="/basket/bundles/add/{{$bundle->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/bundles/add/{{$bundle->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About the {{$bundle->title}}</p>
				<p>{{$bundle->excerpt}}</p>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 my-5">
		<div class="row pt-4">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Included In the Bundle:</p>
							<ul class="check-graphics-green">
								@foreach($bundle->products as $product)
								<li>{{$product->name}} (worth £{{$product->price}})</li>
								@endforeach
							</ul>
							<p class="mb-0 ">Together worth £{{$bundle->price}}, get them in the {{$bundle->title}} for only £{{number_format((float)$bundle->sale_price, 2, '.', '')}} with lifetime access - <b>Save {{number_format((($bundle->price - $bundle->sale_price)*100) /$bundle->price, 0)}}%!</b></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-5 mb-5">
		<div class="row">
			<div class="col-md-10">
				{!!$bundle->description!!}
				@if($bundle->sale_price != NULL)
				<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$bundle->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$bundle->sale_price, 2, '.', '')}}</span></p>
				@else
				<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$bundle->price}}</p>
				@endif
				<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Save {{number_format((($bundle->price - $bundle->sale_price)*100) /$bundle->price, 0)}}%!</span></p>
				<p class="mob-mb-0">
					<a href="/basket/bundles/add/{{$bundle->id}}">
						<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
					</a>
					<a href="/gift-basket/bundles/add/{{$bundle->id}}">
						<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
					</a>
				</p>
				<div class="">
					<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
					<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
					<div class="d-block mb-2">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	@if(count($reviews))
	<div id="reviews" class="container my-5 mob-pt-0">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews</p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Simplistic mobility method (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($bundle->sale_price != NULL)
					{{-- <p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$bundle->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$bundle->sale_price, 2, '.', '')}}!</span> TODAY!</p> --}}
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$bundle->price}} TODAY!</p>
					@endif
					<a href="/basket/bundles/add/{{$bundle->id}}">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	<div class="container-fluid bg banner-bg2 text-center">
		<div class="row justify-content-center py-5">
			<div class="col-lg-8 py-5">
				<p class="text-white text-large"><b>Never let yourself be set back again by unnecessary injuries or aches & pains</b></p> 
				<p class="text-white text-large mb-0"><b>Build complete strength and figure out what you have been missing with the this Bundle</b></p>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">All Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div class="mw-100 overflow-hidden">
		<product-bundles :bs="['beginners-bundle','intermediate-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/bundles/add/{{$bundle->id}}" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection
@section('scripts')
<script>
	function addToBasket(id, name, category, price){
		dataLayer.push({
		  'event': 'addToCart',
		  'ecommerce': {
		    'currencyCode': 'GBP',
		    'add': {                                
		      'products': [{                      
		        'name': name,
		        'id': id,
		        'price': price,
		        'brand': "Tom Morrison",
		        'category': category,
		        'quantity': 1
		       }]
		    }
		  }
		});
	}
	document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('pre').forEach(function(element) {
        var textContent = element.textContent;
        if (textContent.includes('youtubeid=')) {
            var ytid = textContent.split('=')[1];
            var vidHtml = '<div class="embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + ytid + '?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>';
            element.innerHTML = vidHtml;
        }
    });
});

</script>
@endsection