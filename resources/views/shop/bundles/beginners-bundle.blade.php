@php
$page = "BeginnersBundle";
$pagename = "Beginner's Bundle";
$pagetitle = "Beginner's Bundle - Tom Morrison";
$meta_description = "Want to kickstart your mobility journey in the best way possible? We’ve combined our top 3 products that are best suited to beginners into a discounted bundle so you can hit the ground running!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Beginner's Bundle<span class="tm">®</span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="/simplistic-mobility-method-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<beginners-bundle-slider></beginners-bundle-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($bundle->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$bundle->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$bundle->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$bundle->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Save {{number_format((($bundle->price - $bundle->sale_price)*100) /$bundle->price, 0)}}%!</span></p>
						<p class="mob-mb-0">
							<a href="/basket/bundles/add/{{$bundle->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/bundles/add/{{$bundle->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About the Beginner's Bundle</p>
				<p>Want to kickstart your mobility journey in the best way possible? </p>
				<p>We’ve combined all of Level 1 (our top 3 most popular programs!) that are best suited to beginners into a discounted bundle so you can hit the ground running!</p>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-4">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Included In the Bundle:</p>
							<ul class="check-graphics-green">
								<li>The Simplistic Mobility Method® (worth £69)</li>
								<li>Ultimate Core (worth £27.99)</li>
								<li>Where I Went Wrong (worth £6.99)</li>
							</ul>
							<p class="mb-0 ">Together worth £103.98, get them in the Beginner's <br class="d-none d-lg-inline" />Bundle for only £{{number_format((float)$bundle->sale_price, 2, '.', '')}} with lifetime access - <b>Save {{number_format((($bundle->price - $bundle->sale_price)*100) /$bundle->price, 0)}}%!</b></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4 mt-4 mob-mt-0">
		<div class="row">
			<div class="col-12">
				<div class="card border-0 shadow pt-5 pb-4 px-5 mob-px-3 mob-py-4">
					<div class="row">
						<div class="col-lg-6 pr-5 mob-px-3">
							<p class="mimic-h3 mb-4">The Simplistic Mobility Method<span class="tm">®</span></p>
							<p>The Simplistic Mobility Method<span class="tm">®</span> is your foundational mobility program, used by thousands of people all around the world to figure out & improve their mobility issues.</p>
							<p>Using simple exercises, it shows you how to easily make progress by being consistent and assessing your body as a whole. No equipment needed. Nothing fancy. No big words. Just great drills with simple explanations.</p>
							<p><i>Want to find out more about the Simplistic Mobility Method? <a href="/products/the-simplistic-mobility-method">Click here!</a></i></p>
							</div>
						<div class="col-lg-6 mob-mt-3">
							<picture> 
								<source srcset="/img/programs/beginners-bundle/SMM-Image.webp?v=2023-07-24" type="image/webp"/> 
								<source srcset="/img/programs/beginners-bundle/SMM-Image.jpg?v=2023-07-24" type="image/jpeg"/>
								<img src="/img/programs/beginners-bundle/SMM-Image.jpg?v=2023-07-24" width="585" height="468" class="w-100 h-auto" alt="Tom Morrison - The Simplistic Mobility Method" />
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-4 pb-4 mob-mt-0">
		<div class="row">
			<div class="col-12">
				<div class="card border-0 shadow pt-5 pb-4 px-5 mob-px-3 mob-py-4">
					<div class="row">
						<div class="col-lg-6 pr-5 mob-px-3">
							<p class="mimic-h3 mb-4">Ultimate Core</p>
							<p>Ultimate Core is an educational series which will show you how to have complete core strength – not just sit ups and planks.</p>
							<p>Ultimate Core will change what you think a “strong core” is and will show you how to easily include all 4 principles of core strength in your training. Perfect for coaches and athletes, advanced level, or beginners - especially if you have recurring back pain, you feel dominant on one side, just feel a bit “off” or struggle to engage your core properly.</p>
							<p><i>Want to find out more about Ultimate Core? <a href="/products/ultimate-core-seminar">Click here!</a></i></p>
							</div>
						<div class="col-lg-6 mob-mt-3">
							<picture> 
								<source srcset="/img/programs/beginners-bundle/Ultimate-Core-Image.webp" type="image/webp"/> 
								<source srcset="/img/programs/beginners-bundle/Ultimate-Core-Image.jpg" type="image/jpeg"/>
								<img src="/img/programs/beginners-bundle/Ultimate-Core-Image.jpg" width="585" height="468" class="w-100 h-auto" alt="Tom Morrison - Ultimate Core" />
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-4 pb-4 mob-mt-0">
		<div class="row">
			<div class="col-12">
				<div class="card border-0 shadow pt-5 pb-4 px-5 mob-px-3 mob-py-4">
					<div class="row">
						<div class="col-lg-6 pr-5 mob-px-3">
							<p class="mimic-h3 mb-4">Where I Went Wrong</p>
							<p>Finally, you get Where I Went Wrong, which takes you through Tom’s own journey of discovering fitness, becoming a walking injury, making loads of gains before being taken out by a serious spinal injury.</p>
							<p>You’ll see how he recovered and get back to training CrossFit, weightlifting, calisthenics, etc. and see that you can go from a complete mess to figuring out what your body needs, and feeling better than you ever have before.</p>
							<p><i>Want to find out more about Where I Went Wrong? <a href="/products/where-i-went-wrong-e-book">Click here!</a></i></p>
							</div>
						<div class="col-lg-6 mob-mt-3">
							<picture> 
								<source srcset="/img/programs/beginners-bundle/WIWW-Image.webp" type="image/webp"/> 
								<source srcset="/img/programs/beginners-bundle/WIWW-Image.jpg" type="image/jpeg"/>
								<img src="/img/programs/beginners-bundle/WIWW-Image.jpg" width="585" height="468" class="w-100 h-auto" alt="Tom Morrison - WIWW" />
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-5 pt-5 mob-pt-0">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for the Beginner's Bundle</p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Simplistic mobility method (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($bundle->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$bundle->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$bundle->sale_price, 2, '.', '')}}!</span> TODAY!</p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$bundle->price}} TODAY!</p>
					@endif
					<a href="/basket/bundles/add/{{$bundle->id}}">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg2 text-center">
		<div class="row justify-content-center">
			<div class="col-lg-8 py-5">
				<p class="text-white text-large"><b>Never let yourself be set back again by unnecessary injuries or aches & pains</b></p> 
				<p class="text-white text-large mb-0"><b>Build complete strength and figure out what you have been missing with the Beginner's Bundle</b></p>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">All Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div class="mw-100 overflow-hidden">
		<product-bundles :bs="['beginners-bundle','intermediate-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/bundles/add/{{$bundle->id}}" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection