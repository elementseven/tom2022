
@php
$page = 'Gift Checkout';
$pagename = 'Shop';
$pagetitle = 'Gift Checkout | Purchase Tom Morrison Products';
$meta_description = 'View your gift basket and make sure you have all the right products before completing your purchase.';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])

@section('head_section')
<script src="https://js.stripe.com/v3/"></script>
<style>
	.make-me-sticky {
	  position: -webkit-sticky;
		position: sticky;
    top: 5px;
	}
</style>
@endsection

@section('header')
<header class="container pt-5">
	<div class="row">
		<div class="col-12">
			<h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Gift Basket</span><span class="checkout-title-line gift-checkout-title-line"></span><span class="text-primary">Checkout</span><span class="checkout-title-line gift-checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line my-4">
			<order-status :stage="2"></order-status>
		</div>
	</div>
</header>
@endsection

@php 
	$hasAddress = false;
	$sameAddress = false;
	if(Auth::check()){
		if(count($currentUser->addresses)){
			$hasAddress = true;
			foreach($currentUser->addresses as $a){
				if($a->type == 'billing'){
					$billingAddress = $a;
				}else if($a->type == 'shipping'){
					$shippingAddress = $a;
				}
			}
			if($billingAddress->streetAddress == $shippingAddress->streetAddress){
				$sameAddress = true;
			}
		}
	}
	$hasMerch = false;
	foreach(Cart::content() as $cc){
		if($cc->options->type == 'merch'){
			$hasMerch = true;
		}
	}
@endphp

@section('content')
<div class="container mt-4 mb-5">
	<div class="row">
		<div class="col-lg-8">
			<div class="card border-0 shadow p-4">
				<gift-checkout :stripekey="'{{env('STRIPE_KEY')}}'" :clientsecret="'{{$intent->client_secret}}'" :paymentmethod="'{{$intent->payment_method}}'" :total="{{Cart::total()}}" :returnurl="'{{env("APP_URL")}}/gift-payment-status'"></gift-checkout>
				<p class="text-danger">{!!session('payment_error')!!}</p>
				@php session()->forget('payment_error'); @endphp
			</div>
		</div>
		<div class="col-lg-4">
			@php 
			$totalproducts = 0;
			foreach(Cart::content() as $cc){
				if($cc->options->type != 'merch'){
					$totalproducts++;
				}
			}
			@endphp
			<div class="card shadow border-0 mob-mt-5 make-me-sticky">
				<div class="container p-4">
					<p class="text-large"><b>Gift Basket</b></p>
					@foreach(Cart::content() as $i)
					<div class="row mb-3">
						<div class="col-8">
							<p class="mb-0">{{$i->name}}</p>

							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							@else
							@if($i->options->type == 'merch')
							<p class="small mb-0 text-light-grey">Quantity - {{$i->qty}} Size - {{$i->options->size}}</p>
							@else
							@endif
							@endif
							@if($i->options->type == 'merch')
							<p class="mob-mb-0 small"><a href="{{route('remove-merch',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@else
							<p class="mob-mb-0 small"><a href="{{route('remove',['rowId' => $i->rowId, 'id' => $i->id])}}" class="grey-link"><i class="fa fa-trash"></i> Remove</a></p>
							@endif
						</div>
						<div class="col-4 text-right">

							@if($i->options->original_price != NULL && $i->options->type != 'merch')
							<p class="mb-0"><s>£{{number_format((float)$i->options->original_price, 2, '.', '')}}</s> &nbsp; <span class="text-primary">£{{number_format((float)$i->price, 2, '.', '')}}</span></p>
							@else
							<p class="mb-0 ">£{{number_format((float)$i->price, 2, '.', '')}}</p>
							@endif
						</div>
						<div class="col-12 text-right">
							<hr class="my-2" />
						</div>
					</div>
					@endforeach
					
					<p class="text-right mb-0"><b><span class="text-primary">Total: </span> £<span id="total">{{Cart::total()}}</span></b></p>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AVZYQtB-Sdv3bpKwyP5XMtAdN5Xq7bPSGjrgxoU3pR7KfScqekTu7sGr3PuUf5unet-BZ_ZpUjLqEwcG&currency=GBP"></script>
@endsection