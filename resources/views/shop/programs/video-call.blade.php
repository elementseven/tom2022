@php
$page = 'Video Call';
$pagename = 'Video Call';
$pagetitle = "Video Call - Tom Morrison";
$meta_description = "Don't live close enough to have a face-to-face session? Let's video call instead! Talk to Tom one to one for 45 minutes and ask anything you like!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Video Call<span class="tm"></span></h1>
{{-- 			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
			</p> --}}
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<picture> 
				<source srcset="/img/programs/video-call/video-call.webp" type="image/webp"/> 
				<source srcset="/img/programs/video-call/video-call.jpg" type="image/jpeg"/>
				<img src="/img/programs/video-call/video-call.jpg" width="634" height="357" class="w-100 h-auto" alt="Tom Morrison - Video Call screenshot 1" />
			</picture>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About A Video Call<span class="tm"></span></p>
				<p><b>Talk to Tom one to one for 45 minutes and ask anything you like!</b></p>
				<p>Having things explained to you personally based on your own injury & training history will really help you to understand the best way to alter your training or mobility work so you can get the most out of your time.</p>
				<p>Talking directly is far more useful than via emails or messages as it helps to avoid any misinterpretation, misunderstandings and helps us solve problems quickly with Tom giving you real-time feedback!</p>
				<div class="card border-0 shadow p-3 my-3">
			<div class="row">
				<div class="col-8 mt-1 mob-mt-0">
					<p class="mb-0 mt-2 mob-mt-0" style="color:#4A8CFF;"><b>We use Zoom for a high quality online call!</b></p>
				</div>
				<div class="col-4">
					<img src="/img/programs/video-call/zoom.svg" alt="zoom logo" width="105" height="36" class="float-right mob-mt-2 h-auto"/>
				</div>
			</div>
		</div>
			</div>
		</div>
		
	</div>
	<div class="container px-4 mb-minus-3rem">
		<div class="row justify-content-center">
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/video-call/video-call-grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/video-call/video-call-grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/video-call/video-call-grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Video Call image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/video-call/video-call-grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/video-call/video-call-grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/video-call/video-call-grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Video Call image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/video-call/video-call-grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/video-call/video-call-grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/video-call/video-call-grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Video Call image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/video-call/video-call-grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/video-call/video-call-grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/video-call/video-call-grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Video Call image 4" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">With a Video Call you get:</p>
							<ul class="check-graphics-green mb-0">
								<li>Personalised advice for your areas of tightness, pain or weakness</li>
								<li>Custom exercises that will target issues you're having</li>
								<li>Expert consultation from the World's Sexiest Man 2019, 2020, 2021, 2022, 2023 & 2024</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Video Call<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/video-call/Tom-Mac-Mock.webp" type="image/webp"/> 
							<source srcset="/img/programs/video-call/Tom-Mac-Mock.jpg" type="image/jpeg"/>
							<img src="/img/programs/video-call/Tom-Mac-Mock.jpg" width="460" height="259" class="w-100 h-auto" alt="Video Call Tom-Mac-Mock" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/video-call/1.svg" alt="Tom Morrison 1 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Once purchased, you'll receive short questionnaire to enter details like your timezone, preferred day/time and an overview of what you want to talk about</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/video-call/2.svg" alt="Tom Morrison 2 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Tom will then get in touch to confirm a day & time that suits the both of you</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/video-call/3.svg" alt="Tom Morrison 3 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Tom will send you a Zoom link on the day that you can use on your phone, laptop or tablet</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/video-call/4.svg" alt="Tom Morrison 4 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>After the call, Tom will send you any links, videos or resources you'll need!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block mt-5">
				<picture> 
					<source srcset="/img/programs/video-call/Tom-Mac-Mock.webp" type="image/webp"/> 
					<source srcset="/img/programs/video-call/Tom-Mac-Mock.jpg" type="image/jpeg"/>
					<img src="/img/programs/video-call/Tom-Mac-Mock.jpg" width="460" height="259" class="w-100 h-auto" alt="Video Call Tom-Mac-Mock" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">What To Wear / Prepare</p>
					</div>
					<div class="col-lg-10">
						<ul class="check-graphics-green mb-0">
							<li>Wear comfortable clothes that you can move & stretch in</li>
							<li>If possible, have some floor space for moving and a bare bit of wall</li>
							<li>Have a think about triggers, training & injury history, and anything that may be relevant to your questions with Tom</li>
							<li>Make sure to have some way of taking notes (either your phone or a pen & paper) as there'll be lots of great info!</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/5">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="/basket/add/5" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection