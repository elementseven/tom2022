@php
$page = 'SMM';
$pagename = 'The Simplistic Mobility Method®';
$pagetitle = "The Simplistic Mobility Method® - Tom Morrison";
$meta_description = "Start Here! A daily mobility guide to correct imbalances with simple drills.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">The Simplistic Mobility Method<span class="tm">®</span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="/simplistic-mobility-method-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<smm-slider></smm-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Buy as a Bundle & save up to 23%!</span><a href="#view-smm-bundles" class="px-2 py-1">Shop Now</a></p>
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block mr-auto mb-2 ml-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About the Simplistic Mobility Method<span class="tm">®</span></p>
				<p>The essential, non-negotiable movements to assess yourself, move well and avoid injury - keeping your joints healthy & core strength in check.</p>
				<p>The Simplistic Mobility Method ebook & supporting videos contains a tried & tested program for you to follow to feel better than you ever have before.</p>
				<p class="mb-0">🌎 Used by over 50,000 people worldwide!</p>
				<p class="mb-0">🤗 Beginner Friendly!</p>
				<p>🏠 Easy to do at Home!</p>
				<p>No equipment needed. Nothing fancy. No big words. Just great drills with simple explanations.</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/grid1_zenith.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/grid1_zenith.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/grid1_zenith.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/smm2.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/smm2.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/smm2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/grid4_couch.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/grid4_couch.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/grid4_couch.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/smm3.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/smm3.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/smm3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/grid5_9090.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/grid5_9090.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/grid5_9090.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/smm/smm6.webp" type="image/webp"/> 
					<source srcset="/img/programs/smm/smm6.jpg" type="image/jpeg"/>
					<img src="/img/programs/smm/smm6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">SMM is Designed To:</p>
							<ul class="check-graphics-green mb-0">
								<li>Assess your entire body's flexibility & stability</li>
								<li>Check for any imbalances</li>
								<li>Teach awareness of your own twists & turns</li>
								<li>How to fix them in 30 mins, 3-4 times per week</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pt-5">
			<video-testimonials :amount="20"></video-testimonials>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With SMM</p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/smm/top4_devices.webp?v=2023-7-11" type="image/webp"/> 
							<source srcset="/img/programs/smm/top4_devices.jpg?v=2023-7-11" type="image/jpeg"/>
							<img src="/img/programs/smm/top4_devices.jpg?v=2023-7-11" width="460" height="259" class="w-100 h-auto" alt="Simplistic mobility method devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/smm/chart.svg" alt="Tom Morrison SMM Chart icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>The Simplistic Mobility Method ebook contains the unique program, PLUS the tests that you'll be use to measure your progress</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/smm/video.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Video Follow Alongs with Tom and Jenni at full level & at complete beginner level for you to easily learn & avoid common mistakes</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/smm/info.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Expert instructions, breakdowns & demos of how to use the Method plus extra information, regressions & reasoning behind the exercises</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/smm/lightbulb.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A Cheat Sheet showing all the exercises in extra detail, with exactly which muscles are targeted + extra tips and things to look out for!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-5 pt-5 mob-mt-0 d-none d-lg-block">
				<picture> 
					<source srcset="/img/programs/smm/top4_devices.webp?v=2023-7-11" type="image/webp"/> 
					<source srcset="/img/programs/smm/top4_devices.jpg?v=2023-7-11" type="image/jpeg"/>
					<img src="/img/programs/smm/top4_devices.jpg?v=2023-7-11" width="460" height="259" class="w-100 h-auto" alt="Simplistic mobility method devices" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg pt-5 pb-3">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 text-white mb-4">PLUS! These Awesome Extras:</p>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Head To Toe Mobility</b></p>
						<picture> 
							<source srcset="/img/programs/smm/bonus1_head.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/bonus1_head.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/bonus1_head.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method head to toe" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Lifetime Updates</b></p>
						<picture> 
							<source srcset="/img/programs/smm/setbacks.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/setbacks.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/setbacks.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method setbacks" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Bonus Exercises</b></p>
						<picture> 
							<source srcset="/img/programs/smm/bonus3_exercises.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/bonus3_exercises.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/bonus3_exercisess.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method bonus-exercises" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Regressions & adaptations suitable for all levels</b></p>
						<picture> 
							<source srcset="/img/programs/smm/bonus4_regressions.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/bonus4_regressions.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/bonus4_regressions.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method bonus-regressions" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Access To The SMM Community</b></p>
						<picture> 
							<source srcset="/img/programs/smm/community.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/community.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/community.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method community" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Rewards & Challenges for Motivation!</b></p>
						<picture> 
							<source srcset="/img/programs/smm/bonus6_badges.webp" type="image/webp"/> 
							<source srcset="/img/programs/smm/bonus6_badges.jpg" type="image/jpeg"/>
							<img src="/img/programs/smm/bonus6_badges.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Simplistic Mobility Method badges" lazy/>
						</picture>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is SMM a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, The Simplistic Mobility Method® is a one-time purchase of @if($product->sale_price)<s>£69</s> £{{$product->sale_price}}. @else £69 @endif and you get lifetime access (with lifetime updates too!)</p>      
								<p>To put this in perspective, when I hurt my knee, I spent 6 weeks of physio on it - that cost me about £300. When I hurt my shoulder that was 3 sessions: £150.</p>
								<p>When I badly injured my back, I pretty much went every week for 6 months straight just to get some relief... I don’t even want to calculate that.</p>
								<p>The Simplistic Mobility Method® is @if($product->sale_price)<s>£69</s> £{{$product->sale_price}} @else £69 @endif once. For a lifetime of benefits.</p>
								<p>You’ll learn about your body, how to spot injuries before they happen, it is a tried and tested method with real athletes. After people use SMM they have found that they no longer need to go to Physio as often, or even at all.</p>
								<p>PLUS! As an extra added bonus, you’ll become a part of a global community of SMMers via our <a href="https://www.facebook.com/groups/simplisticmobilitymethod/" target="_blank">Facebook group</a>, where you can ask questions directly to people who’ve been using the method for years, and directly to Tom & Jenni who are very active in the SMM community.</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">Will SMM work for me? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>After working with so many people over the years, there are always things we miss about training, mobility, and recovery – no matter how much research and practice you do. You will pick up imbalances and compensations over years of small mistakes, misinterpretations, and even daily habits.</p>
								<p>The Simplistic Mobility Method® allows you to learn what your body can and can’t do right now and you can use the tests & methodology to keep maintaining and checking your body throughout your entire lifetime.</p>
								<p>SMM is designed to make your joints sit and work correctly and teaches your body to move as one unit with great muscle activation.</p>
								<p>The best injury is the one that never happened!</p>
								<p>It doesn’t matter what you like to do for physical activity or training, everyone has the same joints and muscles, and they need to have a certain baseline mobility & stability to function well; SMM is that baseline.</p>
								<p>Your body won’t be sore all the time if it moves well.</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">What happens after I buy SMM? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
								<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>    
								<p>If you don’t receive an email, make sure to check your spam/junk folder.</p>
								<p>If it’s not there either, simply go to <a href="https://tommorrison.uk/login" target="_blank">https://tommorrison.uk/login</a> and click “Forgot Your Password?” to generate a new sign in email for you. Some email hosts don’t like automatic emails and block them to protect you from spam!</p>
								<p>Once you’re signed in, you will see the Simplistic Mobility Method® on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily.</p>
								<p>Everything is stored online for you, and any updates we make to the program will automatically appear for you.</p>
								<p>If you ever forget your password, don’t worry! The “Forgot Your Password” feature is always there! You won’t lose your account.    </p>      
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">How long will it take for me to see results? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>After your first session. Yeah, really!</p>
								<p>In your first session you run through a series of tests before and after. Some people notice visual improvements right away, others just feel better, taller or that they are moving easier. Many are shocked at how much they struggled with (but this is a good thing, means there’s loads of gains to be had!)</p>
								<p>After that, as you’d expect, the more you use the program the faster you see results. You start to really see differences after 8 sessions. So, if you can manage 4 times per week - that’s only 2 weeks to awesome!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">How long does it take to do SMM? Is it easy to stick to? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>An SMM session will take about 30 minutes, so it is pretty easy to slot into even a busy timetable. We’d recommend between 2-5 sessions per week, depending on how much time you have available.</p>
								<p>The better you get at SMM the less frequently you’ll need to do it. Plus, we have consistency tips like the “1-Rep Method” inside the ebook for anyone short on time, alongside a way you can condense your mobility practice into a 5 minute-a-day routine!</p>
								<p>Your very first session will take longer, often around an hour because you’ll go through a full body assessment, and you’ll want to watch the videos to check the technique, tips and progressions. You’ve also got optional checklists and note sheets that you can use to mark your progress.</p>
								<p>Even if you can only manage one session a week it’s better than none!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">I’m not very flexible, is SMM suitable for total beginners? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>Yes! Don’t worry, trying to get flexible before starting a mobility program is like trying to learn to drive before your first driving lesson!</p>
								<p>The Simplistic Mobility Method® is what you need to increase your flexibility in a safe way. Not just stretching for no reason, but intentional, directed exercises to build both mobility and stability.</p>
								<p>All the exercises have different levels of regressions/progressions so you can do SMM no matter what level you’re at.</p>
								<p>And on the flip side, if you are too flexible then the movements are also designed to develop control, add more stability, and build confidence in your body!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">I have an injury/pain, can I still do SMM? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>If it is a fresh injury, it’s always best to have an in-person assessment by a Physiotherapist, preferably one with an active/sports background who understands the type of training that you do. If it is intense, sudden sharp pain or nerve pain then I’d recommend seeing your doctor and make sure you get cleared to do mobility work. </p>
								<p>However, if you have an old injury that’s still giving you niggles, recurring aches/pains, or you’ve lived with things like disc injuries for a long time and you’ve already been through all the proper channels then you will be ok to use SMM and try just using the regression examples with relaxed breathing. </p>
								<p>Technique is key so make sure to film yourself as you are doing the SMM movements - a lot of times what you think you are doing and what you are actually doing can be very different, so it’s always good to double check everything!</p>     
								<p>If you feel stuck or scared that you’ll hurt yourself further, it would be good to <a href="/products/video-call" target="_blank">book a video call with Tom</a>to get more guidance specific to your own training and injury history.</p>  
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">I do/play a sport will SMM help me? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-8" data-parent="#faq-accordion">
								<p>Yup, the Simplistic Mobility Method® is designed to help the human body – regardless of what sport that body does.</p>
								<p>In our SMM community group we have footballers, crossfitters, kettlebell athletes, powerlifters, weightlifters, rugby players, runners, normal gym goers, bodybuilders, coaches, physiotherapists, bodyweight enthusiasts, pole dancers, even people that aren’t interested in fitness and just want to feel good in their day-to-day life!</p>
								<p>We all have a body, sports and activities is all the fun stuff you can do after your body moves well. SMM is like the background operating systems of your phone to keep it running smoothly. If there’s a ‘bug’ in a specific area, your body will not perform the way it should!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-9" role="button" aria-expanded="false" aria-controls="collapse-9">Can I do SMM alongside my own training? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-9" data-parent="#faq-accordion">
								<p>Yep! We would argue that SMM is an essential part of your training. It runs through everything you need for good performance.</p>
								<p>No more wasting time trying to figure out what’s causing that niggle, rolling out tightness, or getting stuck as a plateau, SMM gives you the tools to actively support and progress your training.</p>
								<p>However, if you’re really in a bad way with a current or longstanding pain and your tests show that you have a lot to work on, then we recommend focusing solely on SMM for a few weeks and then return to training when you’ve improved your movements.</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-10" role="button" aria-expanded="false" aria-controls="collapse-10">Do I need equipment to do SMM? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-10" data-parent="#faq-accordion">
								<p>Nope, not at all! Every exercise can be done at home, at the gym, while travelling... any where!</p>
								<p>Every movement we use is bodyweight. The aim is to teach you how to move using your own body as the main tool, rather than relying on external forces!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-11" role="button" aria-expanded="false" aria-controls="collapse-11">What is the price of SMM in Dollars? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-11" data-parent="#faq-accordion">
								<p>As a UK company, we charge in GBP. But we don’t add on any conversion fees! So, you can use Google to check what the current conversion rate is for SMM! There may be fees added by your own bank, but you would need to check with them.</p>              
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for the Simplistic Mobility Method<span class="tm">®</span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Simplistic mobility method (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/1">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg2 text-center">
		<div class="row justify-content-center">
			<div class="col-lg-8 py-5">
				<p class="text-white text-large"><b>Never let yourself be set back again by unnecessary injuries or aches & pains</b></p> 
				<p class="text-white text-large mb-0"><b>Build complete strength and figure out what you have been missing with the Simplistic Mobility Method®</b></p>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy The Simplistic Mobility Method® as part of a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div class="mw-100 overflow-hidden">
		<product-bundles :bs="['beginners-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/add/1" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection