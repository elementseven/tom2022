@php
$page = 'Splits & Hips';
$pagename = 'Splits & Hips';
$pagetitle = "Splits & Hips - Tom Morrison";
$meta_description = "Want to learn the splits in less than 30 minutes a day with no equipment necessary? This is the program for you.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Splits & Hips<span class="tm"></span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<splits-hips-slider></splits-hips-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Splits & Hips<span class="tm"></span></p>
				<p>Easy to follow program combining multiple techniques to improve both your flexibility and strength, bringing about true mobility changes which your body learns to use and control over time.</p>
				<p>But, the most important part of achieving the splits is consistency. So, we’ve made each day as compact as possible, taking no more than 30 minutes to complete - you don’t need to spend hours stretching every day!</p>
				<p>You can use each day’s program as a cool down after your workout, as a 30 min chill session while watching some TV, or as a standalone session itself in the gym – however you fit it in, just make sure it’s frequent. Your body is constantly changing and adapting, make sure you’re giving it something cool to do.</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid5.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid5.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid5.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/splits-and-hips/grid6.webp" type="image/webp"/> 
					<source srcset="/img/programs/splits-and-hips/grid6.jpg" type="image/jpeg"/>
					<img src="/img/programs/splits-and-hips/grid6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Splits & Hips image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Splits & Hips is Designed To:</p>
							<ul class="check-graphics-green mb-0">
								<li>Help you learn the front splits in less than 30 minute sessions</li>
								<li>Increase your lower body flexibility and strength</li>
								<li>Be simple and easy to slot into your schedule</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Splits & Hips<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/splits-and-hips/devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/splits-and-hips/devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/splits-and-hips/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Splits & Hips devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/video.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>11 high quality workout follow along videos, from 10-30 minutes long that you can do at home or in the gym</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/lightbulb.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A PDF workbook for notes to discover weaknesses & eliminate them forever!</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/info.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Individual PDFs per workout breaking down the movements, regressions & benefits.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-3 px-0 mt-4 d-none d-lg-block">
				<picture> 
							<source srcset="/img/programs/splits-and-hips/devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/splits-and-hips/devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/splits-and-hips/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Splits & Hips devices" lazy/>
						</picture>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is Splits & Hips a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, Splits & Hips is a one-time purchase for lifetime access (with lifetime updates too!)</p>      
								<p>To put this in perspective, when I hurt my knee, I spent 6 weeks of physio on it - that cost me about £300. When I hurt my shoulder that was 3 sessions: £150.</p>
								<p>When I badly injured my back, I pretty much went every week for 6 months straight just to get some relief... I don’t even want to calculate that.</p>
								<p>Splits & Hips is @if($product->sale_price)<s>£67</s> £{{$product->sale_price}} @else £67 @endif once. For a lifetime of benefits.</p>
								<p>You’ll learn about your body, how to spot injuries before they happen, it is a tried and tested method with real athletes. After people use Splits & Hips they have found that they no longer need to go to Physio as often, or even at all!</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">What happens after I buy Splits & Hips? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>
								<p>(If you don’t receive an email, make sure to check your spam/junk folder)</p>
								<p>Once you’re signed in, you will see Splits & Hips on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily!</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">How long are the sessions? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
								<p>Each session is between 20-25 minutes long, and you aim for 4 sessions per week!</p>     
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">Are the sessions suitable for complete beginners? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>Each exercise can be scaled to your level, holding each stretch for less time, doing fewer reps or simply not going as deep.</p>          
								<p>Even if you’re no way near the splits, getting your lower body moving in the ways each session guides you is so beneficial for your legs, hips and back!</p>  
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">How can I fit Splits & Hips into my schedule? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>Splits & Hips is designed to take 20-25 mins, 4 times per week, so it can be added as a cool down after training or a specific mobility session in your day.</p>
								<p>If you’re short for time, you can even get benefits from 2 sessions per week!</p>               
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">What equipment do I need? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>You probably already have everything you need!</p>
								<p>Ideally you want:</p>
								<ul>
									<li>Something you can use as supports under your legs or in your hands, e.g. yoga blocks, cushions or books</li>
									<li>Something to use as a strap to help encourage range of motion, e.g. a yoga strap, a belt or towel</li>
									<li>Something to put your foot on, e.g. a chair or sofa!</li>
								</ul>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">Can I do the sessions at home? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>Yes! They’re designed to use minimal equipment and minimal space so you can do them in one spot in your house!</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">How long will it take me to achieve the front splits? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-8" data-parent="#faq-accordion">
								<p>There’s a lot of factors that will come into play here, your current flexibility levels, your training history, even your daily activity!</p>
								<p>For some it could be a matter of weeks, for others it can be months or a year. But no matter how long it takes you will be continuously improving your hip and lower body health. The longer the journey, the greater the rewards!</p>          
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="program-reviews" class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for Splits & Hips<span class="tm"></span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Splits & Hips (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Splits & Hips review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4 d-none">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/7">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="/basket/add/7" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection