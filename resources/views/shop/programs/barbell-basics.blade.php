@php
$page = 'Barbell Basics';
$pagename = 'Barbell Basics';
$pagetitle = "Barbell Basics - Tom Morrison";
$meta_description = "In this incredible educational video series you are going to be shown all of the elements of the barbell and how to wield it.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Barbell Basics<span class="tm"></span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<barbell-basics-slider></barbell-basics-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Barbell Basics<span class="tm"></span></p>
				<p>In this incredible educational video series you are going to be shown all of the elements of the barbell and how to wield it.</p>
				<p>Barbell Basics highlights important technique points and everything to look out for in your own practice. This is perfect for the complete beginner right up to an advanced lifter and even coaches. </p>
				<p>Years of working with beginners and more advanced athletes has shown me that people miss the simplest things. Barbell Basics is inspired by the eureka moments that both me & my clients have had during one on one coaching.</p>
				<p>You'll come across things you have never thought of before plus subtle refinements that could be that extra push you need to make the progress you crave!</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid5.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid5.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid5.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/barbell-basics/grid6.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/grid6.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/grid6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Barbell Basics image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Barbell Basics is Designed To:</p>
							<ul class="check-graphics-green mb-0">
								<li>Teach you how to warm up effectively for weightlifting</li>
								<li>Simply explain bracing and abdominal pressure for lifting</li>
								<li>Expertly guide you through 10 barbell movements</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Barbell Basics<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.webp" type="image/webp"/> 
							<source srcset="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.jpg" type="image/jpeg"/>
							<img src="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.jpg" width="460" height="259" class="w-100 h-auto" alt="Barbell Basics devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/video.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Thorough video guides explaining:</p>
						<ul>
							<li>Warming Up </li>
							<li>Bracing & Relatable Core Strength </li> 
							<li>Deadlifts</li>
							<li>Back Squats</li>
							<li>Front Squats</li>
							<li>Overhead Press </li>
							<li>Bench Press </li>
							<li>Overhead Squat </li>
							<li>Clean + all variations </li>
							<li>Snatch + all variations </li>
							<li>Jerk + all variations </li>
							<li>Barbell complexes</li>
						</ul>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/lightbulb.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A PDF guide to Strength Programming. Including rep schemes, training protocols and advice</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/splits-and-hips/info.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p><b>PLUS!</b> A bonus video including how to use the barbell in unique ways for advanced core training!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block mt-5">
				<picture> 
					<source srcset="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.webp" type="image/webp"/> 
					<source srcset="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.jpg" type="image/jpeg"/>
					<img src="/img/programs/barbell-basics/Barbell-Basics-Screenshot-Compilation.jpg" width="460" height="259" class="w-100 h-auto" alt="Barbell Basics devices" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is Barbell Basics a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, Barbell Basics is a one-time purchase for lifetime access (with lifetime updates too!)</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">What happens after I buy Barbell Basics? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>
								<p>(If you don’t receive an email, make sure to check your spam/junk folder)</p>
								<p>Once you’re signed in, you will see Barbell Basics on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily!</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">Is Barbell Basics suitable for complete beginners? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>Yes, all of the instructional videos are taught from complete beginner level, we show what the body positions should look like and how you should feel as you do the movements and how to break each exercise down, perfect if you cannot afford multiple one-to-one sessions with a coach!</p>  
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">How can I use Barbell Basics in the gym? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>Barbell Basics is an educational series, so it is best to watch the content at home, make notes or plans, then film yourself doing the exercises in the gym. Compare your form with the instructional videos and check to see if you hit your target positions and you’ll be on the right path!</p>               
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">What equipment do I need? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>You’ll need access to a barbell and plates (ideally bumper plates!)</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">How long will it take me to get a new PR?! <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>Barbell Basics teaches you how to improve or change your technique, so by taking the information and applying your lifts you’ll be safely hitting PR’s within weeks as long as you are consistent with your training! Having a solid foundation is key to your long term success.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="program-reviews" class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for Barbell Basics<span class="tm"></span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Barbell Basics (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Barbell Basics review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4 d-none">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/8">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="/basket/add/8" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection