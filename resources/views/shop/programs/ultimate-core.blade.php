@php
$page = 'Ultimate Core';
$pagename = 'Ultimate Core';
$pagetitle = "Ultimate Core - Tom Morrison";
$meta_description = "Ultimate Core is an education series which covers complete core strength, and how to easily fit it into your training. The most efficient way to structure your core training.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Ultimate Core<span class="tm"></span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<ultimate-core-slider></ultimate-core-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Buy as a Bundle & save up to 23%!</span><a href="#program-bundles" class="px-2 py-1">Shop Now</a></p>
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Ultimate Core<span class="tm"></span></p>
				<p>Ultimate Core is an education series which covers complete core strength, and how to easily fit it into your training.</p>
				<p>Have you ever wondered if you’re doing the right core exercises?</p>
				<p>Or maybe you're doing so much… but not seeing any improvements in your core strength or back pain?</p>
				<p>The amount of information out there and all the millions of core exercises you “should” be doing can be exhausting!</p>
				<p>That’s why we developed Ultimate Core.</p>
				<p>We’ve broken down core training into 4 foundational principles, so you KNOW you’re not leaving any gaps in your training - and have more confidence in your overall strength!</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid5.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid5.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid5.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/ultimate-core/grid6.webp" type="image/webp"/> 
					<source srcset="/img/programs/ultimate-core/grid6.jpg" type="image/jpeg"/>
					<img src="/img/programs/ultimate-core/grid6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Ultimate Core covers the 4 Types of Core Strength:</p>
							<ul class="check-graphics-green mb-0">
								<li><b>Isometric:</b> Bracing, creating tension, and making yourself FEEL stronger</li>
								<li><b>Rotational:</b> Every way the spine moves - and how not to be scared of movement</li>
								<li><b>Anti-Rotational:</b> Being able to resist movement and avoid imbalances (most commonly missed!)</li>
								<li><b>Reactive:</b> Coordination, proprioception, balance & body intelligence - build a confident body!</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Ultimate Core<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/ultimate-core/top4_devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/top4_devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/top4_devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Ultimate Core devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/ultimate-core/video.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Video series breaking down each type of core strength in a clear, easy to follow way</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/ultimate-core/strength.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A PDF cheat sheet with exercises for each type of core strength</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/ultimate-core/info.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>'Workout Builders' to help you put exercises together for the most efficient & effective session!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block">
				<picture> 
							<source srcset="/img/programs/ultimate-core/top4_devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/top4_devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/top4_devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Ultimate Core devices" lazy/>
						</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg pt-5 pb-3">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 text-white mb-4">PLUS! These Awesome Extras:</p>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Follow Along Workouts</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus0_followalongs.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus0_followalongs.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus0_followalongs.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core follow allongs" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>'Putting It All Together'</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus1_together.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus1_together.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus1_together.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core together" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Lower Back Pain Fix</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus2_back.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus2_back.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus2_back.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core back" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Live Core Seminar</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus3_live.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus3_live.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus3_live.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core live" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Lifetime Updates</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus4_updates.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus4_updates.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus4_updates.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core bonus updates" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Access To The Community</b></p>
						<picture> 
							<source srcset="/img/programs/ultimate-core/bonus5_community.webp" type="image/webp"/> 
							<source srcset="/img/programs/ultimate-core/bonus5_community.jpg" type="image/jpeg"/>
							<img src="/img/programs/ultimate-core/bonus5_community.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Ultimate Core community" lazy/>
						</picture>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is Ultimate Core a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, Ultimate Core is a one-time purchase, and you get lifetime access (with lifetime updates too!)</p>
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">What happens after I buy Ultimate Core? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>
								<p>If you don’t receive an email, make sure to check your spam/junk folder.</p>
								<p>If it’s not there either, simply go to <a href="mailto:https://tommorrison.uk/login">https://tommorrison.uk/login</a> and click “Forgot Your Password” to create a new password.</p>
								<p>Once you’re signed in, you will see Ultimate Core on your dashboard, and from there you can click into it and easily navigate through all the videos & PDFs.</p>
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">Is Ultimate Core suitable for lower back pain? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
								<p>Yes! Ultimate core will teach you complete strength to support your spine.</p>
								<p>Lower back pain can be caused by missing some basic foundational principles, so Ultimate Core gives you the right way to approach it, the best frame of mind and the best structure for having a strong core. </p>
								<p>Included is a specific bonus Lower Back video that covers some of the techniques and myths around back pain. Plus, what to do if you have recurring flare ups and even if you have disc injuries that haven’t required surgery (like myself).</p>     
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">How can I fit Ultimate Core into my schedule? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>You can use Ultimate Core in two ways:</p>
								<p><b>1.</b> Use the principles we teach you to improve the core training you’re already doing</p>
								<p><b>2.</b> Use the workout builders to create complete core workouts that can take 4 minutes or less to tag on to your training sessions or as standalone core sessions!</p>
								<p>The key is getting you to hit all the right things, the right way, more frequently!</p>             
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">What equipment do I need? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>Most of Ultimate Core teaches you how to use your bodyweight and create tension better.</p>
								<p>You’ll also need a light resistance band or TheraBand (which everyone should have anyway, they’re great!) and something you can use as a weight, like a dumbbell or even a water bottle!</p>      
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">Can I do the exercises at home? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>Absolutely!</p>
								<p>It’s great to have routines you can do at home, and we’re all about helping you remove obstacles! The more you practice these things at home, the better your training will be and the things you like to do for fun won’t be hindered!</p>         
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">Will Ultimate Core give me a 6-pack?! <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>Ultimate Core is not about aesthetics. It teaches deep core work to protect your spine alongside how to brace and move properly so that you’re supporting your training in the best way possible.</p>
								<p>A better functioning body means fewer injuries, which leads to consistency, which improves any health and fitness goals you have! Ultimate Core is a great investment to reframe your mind to what core training is.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="program-reviews" class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for Ultimate Core<span class="tm"></span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Ultimate Core (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Ultimate Core review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4 d-none">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/3">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy Ultimate Core as part of a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div id="program-bundles" class="mw-100 overflow-hidden">
		<product-bundles :bs="['beginners-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/add/3" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection