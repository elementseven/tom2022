@php
$page = 'End Range Training';
$pagename = 'End Range Training';
$pagetitle = "End Range Training - Tom Morrison";
$meta_description = "End Range Training is an education series which covers complete core strength, and how to easily fit it into your training. The most efficient way to structure your core training.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
	ul.check-graphics-endreange{
	padding-left: 30px;
	}
	ul.check-graphics-endreange li{
		position: relative;
    text-align: left !important;
    padding: 10px 0px 10px 15px;
    list-style: none;
    margin: 0;
	}
	ul.check-graphics-endreange li:before{
    content: "";
    background-image: url('/img/programs/end-range-training/check.svg');
    background-size: contain;
    position: absolute;
    left: -30px;
    width: 30px;
    height: 26px;
    border-radius: 100%;
    top: 15px;
  }
  p.list-check-endrange{
		position: relative;
		padding-left: 40px;
	}
	p.list-check-endrange:before{
		content: "";
		background-image: url('/img/programs/end-range-training/check-2.svg');
		background-size: contain;
		background-repeat: no-repeat;
		width: 33px;
		height: 25px;
		position: absolute;
		left:0;
		top:4px;
	}
}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">End Range Training<span class="tm"></span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<end-range-slider></end-range-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Buy as a Bundle & save up to 23%!</span><a href="#program-bundles" class="px-2 py-1">Shop Now</a></p>
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About End Range Training<span class="tm"></span></p>
				<p>A complete training program for strength & stability. End Range Training is the perfect balance between what your body needs to function properly and fun challenges that keep you coming back for more.</p>
				<p>You get a library of workouts, exercises, follow along classes and a training plan so you can throw yourself fully into End Range, or work it into your schedule.</p>
				<p>All the techniques, exercises & workouts are scalable to suit everyone from beginners to advanced athletes.</p>
				<p>Each workout has instructional videos and PDF’s, with clear, easy to follow instructions & workout trackers and goals to keep you motivated.</p>
			</div>
		</div>
		<p class="mimic-h3 mt-5" style="color:#27BEC1;">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid1-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid1-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid1-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid2-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid2-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid2-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid3-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid3-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid3-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid4-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid4-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid4-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid5-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid5-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid5-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/end-range-training/er-grid6-laptop.webp" type="image/webp"/> 
					<source srcset="/img/programs/end-range-training/er-grid6-laptop.jpg" type="image/jpeg"/>
					<img src="/img/programs/end-range-training/er-grid6-laptop.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid py-5 mob-py-4" style="background-color: #3E4146;">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3" style="background-color:#27BEC1; color: #fff; border-radius:0;">
							<p class="mimic-h3">End Range is Designed to:</p>
							<ul class="check-graphics-endreange mb-0">
								<li>Fill in the gaps in your training, allowing you to get strong & fit in the best way possible</li>
								<li>Help you avoid injuries and prevent imbalances</li>
								<li>Smash through training plateaus, building you a balanced & capable body</li>
								<li>Break the injury cycle allowing you to train consistently & make long term progress</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With End Range Training<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/end-range-training/end-range-what-you-get.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/end-range-what-you-get.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/end-range-what-you-get.jpg" width="460" height="259" class="w-100 h-auto" alt="End Range Training devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/end-range-training/strength.svg" alt="Tom Morrison strength icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Complete, fun & balanced workouts with detailed demonstration videos</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/end-range-training/play.svg" alt="Tom Morrison play icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Comprehensive exercise library that you can search for specific areas, injuries or training</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/end-range-training/checkboard.svg" alt="Tom Morrison checkboard icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Step by Step Guides PDFs for each workout AND Workout Trackers to monitor your progress</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/end-range-training/lightbulb.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A training plan to help you fit End Range into your schedule, or fully dive into the full program</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block">
				<picture> 
							<source srcset="/img/programs/end-range-training/end-range-what-you-get.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/end-range-what-you-get.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/end-range-what-you-get.jpg" width="460" height="259" class="w-100 h-auto" alt="End Range Training devices" lazy/>
						</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg pt-5 pb-3">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 text-white mb-4">PLUS! These Awesome Extras:</p>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-endrange text-white text-larger"><b>Goals & Reward Badges</b></p>
						<picture> 
							<source srcset="/img/programs/end-range-training/bonus1-badgers.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/bonus1-badgers.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/bonus1-badgers.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training together" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-endrange text-white text-larger"><b>Extra Online Classes</b></p>
						<picture> 
							<source srcset="/img/programs/end-range-training/bonus2-classes.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/bonus2-classes.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/bonus2-classes.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training back" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-endrange text-white text-larger"><b>Save Your Favourites</b></p>
						<picture> 
							<source srcset="/img/programs/end-range-training/bonus3-favourites.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/bonus3-favourites.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/bonus3-favourites.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training live" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-endrange text-white text-larger"><b>Bonus Workouts</b></p>
						<picture> 
							<source srcset="/img/programs/end-range-training/bonus4-workouts.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/bonus4-workouts.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/bonus4-workouts.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training bonus updates" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-endrange text-white text-larger"><b>Access To Our Online Community</b></p>
						<picture> 
							<source srcset="/img/programs/end-range-training/bonus5-group.webp" type="image/webp"/> 
							<source srcset="/img/programs/end-range-training/bonus5-group.jpg" type="image/jpeg"/>
							<img src="/img/programs/end-range-training/bonus5-group.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - End Range Training community" lazy/>
						</picture>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is End Range Training a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, End Range Training is a one-time purchase for lifetime access (with lifetime updates too!)</p>
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">What happens after I buy End Range Training? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>
								<p>If you don’t receive an email, make sure to check your spam/junk folder.</p>
								<p>If it’s not there either, simply go to <a href="mailto:https://tommorrison.uk/login">https://tommorrison.uk/login</a> and click “Forgot Your Password” to create a new password.</p>
								<p>Once you’re signed in, you will see End Range Training on your dashboard, and from there you can click into it and easily navigate through all the videos & PDFs.</p>
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">How long are the workouts? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
								<p>Around an hour, but they can vary depending on any progressions or adaptations you’re using.</p>
								<p>As you get more familiar with the programming & exercises, we also encourage you to adapt the workouts which suit your timescales and your training needs.</p>     
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">Do I need equipment to do End Range? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>Yes, ideally you'll need a barbell with weight plates, TRX or rings, a pullup bar, dumbbells and/or kettlebells, a box/high step, and light resistance bands.</p>
								<p>But if you’re more limited with equipment you can do many of the exercises with any weights & a pull up bar - so you can still get stuck in with what you have!</p>             
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">Do I have to follow the training program? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>Nope, the workouts we provide can simply be a starting point.</p>
								<p>End Range Training aims to help you learn which movements benefit you the most, discover areas of strength you're lacking and start to build workouts around your weaknesses.</p>
								<p>Many End Rangers use the workouts to correct imbalances & compensations, and to fill in the gaps they may be missing from their normal programming.</p>
								<p>You can use the library to pick and choose the exercises which work best for you, plus use the Online Classes for tips and inspiration!</p>     
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">Can I keep doing my other training alongside End Range? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>Absolutely! We provide a training template of how to fit in End Range from anything from 2 days per week to 4 days!!</p>
								<p>You could use End Range specifically to build strength in a certain area, or to get a muscle up, or spend time correcting muscle imbalances, etc. and as your goals change you can use different areas of End Range to help get you there!</p>         
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">Is End Range suitable for complete beginners? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>Totally! We teach progressions & regressions in the videos, so you'll be able to get stuck in straight away and build yourself up over time.</p>
								<p>The main thing to remember is there's no rush! The best part about starting this type of training as a beginner is you'll build symmetrical & functional strength right from the start so you are less likely to develop any bad habits!</p>
							</div>
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">I have an injury/pain, can I still do End Range Training? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-8" data-parent="#faq-accordion">
								<p>If it is a recurring niggle, ache or minor pain then End Range Training will help you to build more rounded movement, strength & mobility to keep your joints happy and your body moving the way it should!</p>
								<p>At first some movements may need to be performed with a light weight, or no weight at all, but over time you will feel stronger and more robust!</p>
								<p>If it is a recent, sharp or acute injury it is best to have in person treatment and save End Range until you are recovered.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="program-reviews" class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for End Range Training<span class="tm"></span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, End Range Training (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="End Range Training review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4 d-none">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/6">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg2 text-center">
		<div class="row justify-content-center">
			<div class="col-lg-8 py-5">
				<p class="text-white text-large mb-0"><b>Stop wasting time in the gym and get the most out of your training. Get a lifetime of knowledge & strength with End Range Training</b></p>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy End Range Training as part of a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div id="program-bundles" class="mw-100 overflow-hidden">
		<product-bundles :bs="['intermediate-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/add/6" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection