@php
$page = 'Online Personal Coaching';
$pagename = 'Online Personal Coaching';
$pagetitle = "Online Personal Coaching - Tom Morrison";
$meta_description = "Work one-on-one with Tom online for 4 weeks to fix an injury, learn more about your body or simply to make some gains!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Online Personal Coaching<span class="tm"></span></h1>
	{{-- 		<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
			</p> --}}
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<picture> 
				<source srcset="/img/programs/online-personal-coaching/top-online-coaching.webp" type="image/webp"/> 
				<source srcset="/img/programs/online-personal-coaching/top-online-coaching.jpg" type="image/jpeg"/>
				<img src="/img/programs/online-personal-coaching/top-online-coaching.jpg" width="634" height="357" class="w-100 h-auto" alt="Tom Morrison - Online Personal Coaching screenshot 1" />
			</picture>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Online Personal Coaching<span class="tm"></span></p>
				<p><b>Work one-on-one with Tom online for 4 weeks to fix an injury, learn more about your body or simply to make some gains!</b></p>
				<p>Sometimes just working by yourself isn't enough, sometimes you need an extra pair of expert eyes to see things you hadn't noticed before and to pin point what's causing your lack of progress.</p>
				<p>Tom has years of experience working with people from many different backgrounds at a variety of different training levels with a wide range of goals: no matter who you are, Tom will give you everything at his disposal to help you become pain free and smash your goals.</p>
			</div>
		</div>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid5.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid5.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid5.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid6.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/coaching-grid6.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/coaching-grid6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Online personal coaching image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Online Coaching Gives You:</p>
							<ul class="check-graphics-green mb-0">
								<li>In-depth personalised assessment with Tom identifying areas needing most improvement </li>
								<li>Customised exercises to tackle weakness & imbalances</li>
								<li>Regular feedback & support for progress and exercise updates</li>
								<li><b>PLUS:</b> Learn what YOU need to focus on most, with guidance on how to fit it into your regular training or routine</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">When You Purchase<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/online-personal-coaching/Tom-Mac-Mock.webp" type="image/webp"/> 
							<source srcset="/img/programs/online-personal-coaching/Tom-Mac-Mock.jpg" type="image/jpeg"/>
							<img src="/img/programs/online-personal-coaching/Tom-Mac-Mock.jpg" width="460" height="259" class="w-100 h-auto" alt="Online Personal Coaching Tom-Mac-Mock" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/online-personal-coaching/1.svg" alt="Tom Morrison 1 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Once purchased, you'll receive a questionnaire to enter details like your training & injury history, your goals and overview of what you want to achieve with the coaching</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/online-personal-coaching/2.svg" alt="Tom Morrison 2 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Tom will be in touch with you for what he needs to assess your movement and anything extra based on your questionnaire answers</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/online-personal-coaching/3.svg" alt="Tom Morrison 3 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>In the first week will figure out which exercises work best for you, testing rep schemes and intensity for the most efficient progress</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/online-personal-coaching/4.svg" alt="Tom Morrison 4 icon" width="27" height="27" class="float-right" />
					</div> 
					<div class="col-lg-11 col-9 mb-3">
						<p>Over the next weeks you'll be communicating with Tom, sending videos, getting feedback, with your program being tweaked and updated, setting up you for time-efficient long term progress</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block mt-5">
				<picture> 
					<source srcset="/img/programs/online-personal-coaching/Tom-Mac-Mock.webp" type="image/webp"/> 
					<source srcset="/img/programs/online-personal-coaching/Tom-Mac-Mock.jpg" type="image/jpeg"/>
					<img src="/img/programs/online-personal-coaching/Tom-Mac-Mock.jpg" width="460" height="259" class="w-100 h-auto" alt="Online Personal Coaching Tom-Mac-Mock" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">How Does It Work?</p>
					</div>
					<div class="col-lg-10">
						<ul class="check-graphics-green mb-0">
							<li>We run through a series of assessments to find your strengths & weaknesses</li>
							<li>Identify the movements & exercises you need to reach your goals and eliminate gaps</li>
							<li>Regular feedback and reassessment allows for continued improvement and adaptation of your exercises</li>
							<li>A plan for long term, continued progression is also created for you to continue after the 4 weeks is complete</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Get back to doing what you love. Work with Tom for <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5">Get back to doing what you love. Work with Tom for <span class="text-primary">&pound;{{$product->price}}!</span></p>
					@endif
					<a href="/basket/add/4">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="/basket/add/4" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection