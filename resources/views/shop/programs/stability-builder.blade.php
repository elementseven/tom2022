@php
$page = 'Stability Builder Program';
$pagename = 'Stability Builder™ Program';
$pagetitle = "Stability Builder™ - Tom Morrison";
$meta_description = "Unique, high-quality, follow along video workouts to build an unbreakable body";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Stability Builder<span class="tm">™</span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
					<a href="#program-reviews"><u style="color:#B3B3B3;">Read Reviews</u></a>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<stability-builder-slider></stability-builder-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Buy as a Bundle & save up to 23%!</span><a href="#program-bundles" class="px-2 py-1">Shop Now</a></p>
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Stability Builder<span class="tm">™</span></p>
				<p>Unique, high-quality, follow along video workouts to build an unbreakable body.</p>
				<p>The most efficient stability accessory program you'll use for life.</p>
				<p>Stability Builder contains the best combination of exercises that I use myself, and we’ve used for years with our clients, allowing us all to get the most out of our training in the most efficient way possible - without having to search through hundreds of exercises and spend hours a week on boring prehab/rehab sessions!</p>
				<p>Every workout is in a follow along format, so you will do the reps alongside me and Jenni, meaning you won’t miss anything (and you’ll actually do them!)</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
		<p><b>No ongoing subscription, you get exactly what you need right now in the most efficient way possible!</b></p>
	</div>
	<div class="container-fluid px-4 mb-minus-3rem">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 4" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid5.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid5.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid5.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 5" lazy/>
				</picture>
			</div>
			<div class="col-lg-2 col-md-4 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/stability-builder/grid6.webp" type="image/webp"/> 
					<source srcset="/img/programs/stability-builder/grid6.jpg" type="image/jpeg"/>
					<img src="/img/programs/stability-builder/grid6.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder image 6" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-7">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Stability Builder is Designed To:</p>
							<ul class="check-graphics-green mb-0">
								<li>Build complete joint strength in fun & engaging workouts</li>
								<li>Give you the best accessory exercises in the most efficient way</li>
								<li>Break the injury cycle & build resilience</li>
								<li>Bolt onto your current training as the ultimate accessory program</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Stability Builder<span class="tm">™</span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/stability-builder/devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Stability Builder devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/stability-builder/video.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>11 high quality workout follow along videos, from 10-30 minutes long that you can do at home or in the gym</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/stability-builder/info.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A PDF workbook for notes to discover weaknesses & eliminate them forever!</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/stability-builder/strength.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>Individual PDFs per workout breaking down the movements, regressions & benefits.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 mt-4 d-none d-lg-block">
				<picture> 
							<source srcset="/img/programs/stability-builder/devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Stability Builder devices" lazy/>
						</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg pt-5 pb-3">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 text-white mb-4">PLUS! These Awesome Extras:</p>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Common Mistakes Guide</b></p>
						<picture> 
							<source srcset="/img/programs/stability-builder/bonus1_mistakes.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/bonus1_mistakes.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/bonus1_mistakes.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder mistakes" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Printable Worksheets</b></p>
						<picture> 
							<source srcset="/img/programs/stability-builder/bonus2_worksheets.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/bonus2_worksheets.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/bonus2_worksheets.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder worksheets" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Lifetime Updates</b></p>
						<picture> 
							<source srcset="/img/programs/stability-builder/bonus3_updates.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/bonus3_updates.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/bonus3_updates.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder updates" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Regressions & Adaptations</b></p>
						<picture> 
							<source srcset="/img/programs/stability-builder/bonus4_regressions.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/bonus4_regressions.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/bonus4_regressions.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder bonus regressions" lazy/>
						</picture>
					</div>
					<div class="col-lg-4 mb-5">
						<p class="list-check-green text-white text-larger"><b>Access To The Community</b></p>
						<picture> 
							<source srcset="/img/programs/stability-builder/bonus5_community.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/bonus5_community.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/bonus5_community.jpg" width="356" height="201" class="w-100 h-auto shadow" alt="Tom Morrison - Stability Builder community" lazy/>
						</picture>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="workout-accordion" class="container py-5">
		<h3 class="text-primary mb-4 pb-3 mob-mb-0 mob-pb-0">What Are the Workouts?</h3>
		<p class="d-lg-none"><i>Click a workout to reveal more info!</i></p>
		<div class="row">
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-1" role="button" aria-expanded="false" aria-controls="workout-1"><b>Isometric Tension</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-isometric-tension.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-isometric-tension.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-isometric-tension.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-isometric-tension" lazy/>
					</picture>
					<div class="collapse" id="workout-1" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-isometric-tension.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-isometric-tension.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-isometric-tension.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-isometric-tension" lazy/>
						</picture>
						<p class="mt-3">You won’t have seen a workout like this before. Isometric tension shows you how to progressively ramp up tension to improve your mind/muscle awareness so that when it comes to normal training, your body feels stable and can fire the right muscles effectively!</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-1" role="button" aria-expanded="false" aria-controls="workout-1">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-2" role="button" aria-expanded="false" aria-controls="workout-2"><b>Rotation & Balance</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-rotation-balance.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-rotation-balance.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-rotation-balance.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-rotation-balance" lazy/>
					</picture>
					<div class="collapse" id="workout-2" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-rotation-balance.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-rotation-balance.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-rotation-balance.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-isometric-tension" lazy/>
						</picture>
						<p class="mt-3">Rotation and Balance covers all the ways your spine needs to move so stay strong & healthy, plus how to breathe properly when working on your spine mechanics. This is combined with balance work - the biggest secret to better back, hip and core strength is balance!</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-2" role="button" aria-expanded="false" aria-controls="workout-2">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-3" role="button" aria-expanded="false" aria-controls="workout-3"><b>Toebility & Ankles</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-toebility.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-toebility.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-toebility.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-toebility" lazy/>
					</picture>
					<div class="collapse" id="workout-3" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-toebility.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-toebility.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-toebility.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-toebility" lazy/>
						</picture>
						<p class="mt-3">Your feet are your foundation. Whether you’re a runner or a lifter it doesn’t matter, you NEED strong feet and mobile ankles to avoid common issues like plantar fasciitis, sprains, or tears. Not only will this workout improve your ankle dorsiflexion, but it will also build up other, less common ranges.</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-3" role="button" aria-expanded="false" aria-controls="workout-3">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-4" role="button" aria-expanded="false" aria-controls="workout-4"><b>Coordination & Plyometrics</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-coord-plyo.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-coord-plyo.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-coord-plyo.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-coord-plyo" lazy/>
					</picture>
					<div class="collapse" id="workout-4" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-coord-plyo.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-coord-plyo.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-coord-plyo.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-coord-plyo" lazy/>
						</picture>
						<p class="mt-3">Jumping, landing, and being coordinated are all things to develop for true athleticism, or if you just want to feel like you have a spring in your step! Learning how to absorb your own force and land without rattling your own bones is crucial to powerful plyometric strength.</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-4" role="button" aria-expanded="false" aria-controls="workout-4">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-5" role="button" aria-expanded="false" aria-controls="workout-5"><b>Freestanding Resistance Band</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-free-band.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-free-band.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-free-band.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-free-band" lazy/>
					</picture>
					<div class="collapse" id="workout-5" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-free-band.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-free-band.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-free-band.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-free-band" lazy/>
						</picture>
						<p class="mt-3">Resistance bands are an essential tool to improve your stability and muscular endurance. This workout is how to use them properly (not just curls & squats like its leaflet demonstrates). High rep resistance band work through large ranges of motion is one of the best ways to avoid muscle tightness.</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-5" role="button" aria-expanded="false" aria-controls="workout-5">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-6" role="button" aria-expanded="false" aria-controls="workout-6"><b>Attached Resistance Band</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-attached-band.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-attached-band.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-attached-band.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-attached-band" lazy/>
					</picture>
					<div class="collapse" id="workout-6" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-attached-band.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-attached-band.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-attached-band.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-attached-band" lazy/>
						</picture>
						<p class="mt-3">Not only can you do amazing things with just the band, when you anchor it properly you have the most ultimate full body & core activation device in the world! You will be hard-pressed to find these techniques combined in this way anywhere else!</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-6" role="button" aria-expanded="false" aria-controls="workout-6">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-7" role="button" aria-expanded="false" aria-controls="workout-7"><b>Unilateral Stability</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-uni-stab.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-uni-stab.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-uni-stab.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-uni-stab" lazy/>
					</picture>
					<div class="collapse" id="workout-7" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-uni-stab.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-uni-stab.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-uni-stab.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-uni-stab" lazy/>
						</picture>
						<p class="mt-3">If you predominantly use barbells, they can mask imbalances you may have. Unilateral Stability is not only incredibly fun, but it also assesses you during the workout, quickly highlighting where you fatigue fastest or struggle the most. Your body can’t cheat or compensate in this workout!</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-7" role="button" aria-expanded="false" aria-controls="workout-7">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-8" role="button" aria-expanded="false" aria-controls="workout-8"><b>Super Duper Sets</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-super-duper.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-super-duper.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-super-duper.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-super-duper" lazy/>
					</picture>
					<div class="collapse" id="workout-8" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-super-duper.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-super-duper.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-super-duper.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-super-duper" lazy/>
						</picture>
						<p class="mt-3">Tom’s unique way of combining mobility into workouts, incorporating both weighted & unweighted exercises. Super Sets combine multiple complementary exercises to increase your workload, Drop Sets use the same movement/muscle but drop weight to achieve more reps… Super Duper Sets do both! This workout can be challenging at first but when you do it regularly, you’ll find your technique and strength in other training soar.</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-8" role="button" aria-expanded="false" aria-controls="workout-8">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="workout-card pt-2 px-3">
					<p class="text-primary text-center mb-2"><a data-toggle="collapse" href="#workout-9" role="button" aria-expanded="false" aria-controls="workout-9"><b>Lower Back & Core Builder</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-lower-back.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-lower-back.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-lower-back.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-lower-back" lazy/>
					</picture>
					<div class="collapse" id="workout-9" data-parent="#workout-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-lower-back.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-lower-back.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-lower-back.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-lower-back" lazy/>
						</picture>
						<p class="mt-3">To make your lower back feel better, you should address your hips, core & balance – which is exactly what this workout does. If you are prone to recurring lower back issues, this workout can even be done during a flare up and will help you to overcome it faster. The lower back is a complex fellow and needs a lot of support from everything surrounding it to make real improvement.</p>
					</div>
					<p class="text-center mt-2 d-none d-lg-block"><a class="text-dark" data-toggle="collapse" href="#workout-9" role="button" aria-expanded="false" aria-controls="workout-9">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
		</div>
	</div>
	<div id="bonus-accordion" class="container pb-5">
		<h3 class="text-dark mb-4 pb-3 mob-mb-0">PLUS! Included for free:</h3>
		<div class="row">
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="bonus-card pt-2 px-3 bg-dark">
					<p class="text-center mb-0 pb-2"><a data-toggle="collapse" href="#bonus-1" role="button" aria-expanded="false" aria-controls="bonus-1" class="text-white"><b>A Full Body Warm Up</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-warm-up.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-warm-up.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-warm-up.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-warm-up" lazy/>
					</picture>
					<div class="collapse" id="bonus-1" data-parent="#bonus-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-warm-up.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-warm-up.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-warm-up.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-warm-up" lazy/>
						</picture>
						<p class="mt-3 text-white mob-pb-3">This is based on my own warm ups and morning routines teaching you how to prepare your body and work through loads of joint movement in under 10 minutes!</p>
					</div>
					<p class="text-center text-white mt-2 pb-2 mb-0 d-none d-lg-block"><a class="text-white" data-toggle="collapse" href="#bonus-1" role="button" aria-expanded="false" aria-controls="bonus-1">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
			<div class="col-lg-4 mb-4 mob-mb-3">
				<div class="bonus-card pt-2 px-3 bg-dark">
					<p class="text-center mb-0 pb-2"><a data-toggle="collapse" href="#bonus-2" role="button" aria-expanded="false" aria-controls="bonus-2" class="text-white"><b>TRX/Rings Follow Along</b></a></p>
					<picture> 
						<source srcset="/img/programs/stability-builder/workout-rings.webp" type="image/webp"/> 
						<source srcset="/img/programs/stability-builder/workout-rings.jpg" type="image/jpeg"/>
						<img src="/img/programs/stability-builder/workout-rings.jpg" width="330" height="186" class="w-100 h-auto d-none d-lg-block" alt="Tom Morrison - Stability Builder workout-rings" lazy/>
					</picture>
					<div class="collapse" id="bonus-2" data-parent="#bonus-accordion">
						<picture> 
							<source srcset="/img/programs/stability-builder/workout-rings.webp" type="image/webp"/> 
							<source srcset="/img/programs/stability-builder/workout-rings.jpg" type="image/jpeg"/>
							<img src="/img/programs/stability-builder/workout-rings.jpg" width="330" height="186" class="w-100 h-auto d-lg-none" alt="Tom Morrison - Stability Builder workout-rings" lazy/>
						</picture>
						<p class="mt-3 text-white mob-pb-3">If you have access to these amazing pieces of equipment, then hold on to your pants because we’re going to show some incredible things for your hips and shoulders! There is SO MUCH MORE than dips and rows!</p>
					</div>
					<p class="text-center text-white mt-2 pb-2 mb-0 d-none d-lg-block"><a class="text-white" data-toggle="collapse" href="#bonus-2" role="button" aria-expanded="false" aria-controls="bonus-2">Read <span class="collapsed">More</span><span class="expanded">Less</span> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
				</div>
			</div>
		</div>
	</div>
	<div id="faqs" class="container-fluid bg-light">
		<div class="row">
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<p class="mimic-h3 mb-4">Frequently Asked Questions</p>
					</div>
					<div class="col-lg-10">
						<div id="faq-accordion">
							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is Stability Builder a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
								<p>No, Stability Builder™ is a one-time purchase of @if($product->sale_price)<s>£75</s> £{{$product->sale_price}}. @else £75 @endif and you get lifetime access (with lifetime updates too!)</p>      
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">What equipment do I need? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
								<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
								<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
								<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>            
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">Do I need to do these workouts at the gym? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
								<p>Not at all! You can if you want to, but some space in your living room or bedroom is plenty.</p>
								<p>Any gym equipment we use has an easy home substitute, for example:</p>
								<ul>
									<li><p>Weight plates to stand on, could easily be a thick book, your fireplace, or bottom step of your stairs</p></li>
									<li><p>Rig post to attach a band to, could easily be your banister or a weighed-down table leg</p></li>
								</ul>
								<p>Stability Builder is designed to be as accessible as possible, so you can do the workouts whenever & wherever suits you best!</p>     
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">Do I need to watch the videos every time I do the workouts? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
								<p>No, you don’t need to, it all depends on how you like to train! </p>
								<p>Once you’ve followed along once, or a couple of times, learned the technique and listened to all the useful hints and tips from Tom it is beneficial to try them at your own pace so you can really focus on how you feel, rather than trying to keep up with us. </p>
								<p>The PDFs in your dashboard have the workouts fully written out with screenshots so you can just use these as reminders as you go through.</p>
								<p>So, always follow along or work at your own pace – the choice is yours!</p>              
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">Do I need to be fit to do the workouts? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
								<p>Not at all! Stability Builder is designed for movement quality so whether you’re super fit or a complete beginner, you’ll probably be working at a very similar pace. </p>
								<p>Despite calling them “workouts” there are only a couple that will actually leave you out of breath (I’m looking at your Super Duper Sets!), for the most part, your goal is to get progressively better at technique & control rather than speed or weight.</p>
								<p>Plus, bear in mind that more you find hard at the start the more you have to gain! The things you find challenging are the things you need the most.</p>
								<p>You adjust the rep schemes to your level if you struggle, e.g. doing half the reps we do, and still get loads of benefits. If you found everything super easy straight away where’s the fun in that?!</p>         
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">I have an injury/pain, can I still do Stability Builder? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
								<p>If you haven’t done <a href="/products/the-simplistic-mobility-method">The Simplistic Mobility Method®</a> yet we would recommend you start there, it will build a solid movement foundation for you which you can build from safely.</p>
								<p>That being said! Stability Builder is amazing for old recurring injuries like ankle or knee or lower back pain that comes and goes. It will target exactly what you need and highlight where any weaknesses or compensations have built up (and probably causing your injuries to come back). </p>
								<p>Some movements may need adapting at the start, which we can help you with in the support group, but once you’ve completed the workouts a few times and realise how good they make you feel you’ll wonder what you ever did without them… and why everyone isn’t doing this stuff!</p>          
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">Do I have to do all 11 workouts in a specific order? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
								<p>No, not really, they’re all beneficial by themselves.</p>
								<p>We recommend running through them all at least once, making notes as you go, then you can either start from the beginning again or pick and choose the workouts you feel give you the most benefits.</p>
								<p>Make sure you keep track of which ones you’re doing though! You don’t want to avoid the ones you hate and only do the workouts you find easy!  Each workout has specific principles in mind that develop different areas of your body/movement.</p>
								<p>Best thing to do is schedule in two Stability Builder workouts per week that are non-negotiable, and you just roll through them without even checking what’s coming in the next session, just nod and say YES COACH! Or you can call us nasty names if you like, we’ll not take offence.</p>
							</div>

							<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">Can I do Stability Builder alongside my normal training? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
							<hr class="grey-line" />
							<div class="collapse" id="collapse-8" data-parent="#faq-accordion">
								<p>Yes! Stability Builder is the ultimate bolt-on to your existing training schedule! We recommend slotting in 2 workouts per week (they’re between 10 – 30 mins long), which can be used as warmups, cool downs, or standalone active recovery sessions.</p>
								<p>Or, if you’re currently on a deload week, returning from injury, or maybe a beginner who’s just figuring out your training you can use Stability Builder as a standalone programme, scheduling 4 per week if you like!</p>          
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="program-reviews" class="container my-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<p class="mimic-h3">Reviews for Stability Builder<span class="tm">™</span></p>
				<div class="card-columns">
					@foreach($reviews as $review)
					<div class="card mt-5 mb-3">
						<div class="review-box bg-light p-3">
							<div class="review-avatar smaller">
								<picture> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
									<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
									<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Stability Builder (SMM) review" lazy />
								</picture>
							</div>
							<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Stability Builder review star"/>@endfor</h3>
							<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
								{!!$review->content!!}
							</div>
							<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center mt-4 d-none">
					<a href="/simplistic-mobility-method-reviews">
						<button class="btn btn-outline d-inline-block	">See more reviews ></button>
					</a>
				</div>
			</div>
			<div class="col-lg-4 mt-5 pl-5 pr-0">
				<p class="list-check-green-dark text-larger"><b>One-Off Payment</b></p>
				<p class="list-check-green-dark text-larger"><b>No Recurring Payments</b></p>
				<p class="list-check-green-dark text-larger"><b>Lifetime Automatic Updates</b></p>
				<p class="list-check-green-dark text-larger"><b>Unlimited Online Support</b></p>
				<p class="list-check-green-dark text-larger mb-0"><b>Accessible Anywhere</b></p>
			</div>
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3 mt-5">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3 mt-5"><span class="text-primary">Only</span> &pound;{{$product->price}} TODAY!</p>
					@endif
					<a href="/basket/add/17">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg2 text-center">
		<div class="row justify-content-center">
			<div class="col-lg-8 py-5">
				<p class="text-white text-large"><b>Fill in the VITAL gaps today so you can train confidently to your best, without getting stuck with injuries.</b></p> 
				<p class="text-white text-large mb-0"><b>Stability Builder™ is your path to more complete mobility & stability work for life!</b></p>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy Stability Builder™ as part of a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div id="program-bundles" class="mw-100 overflow-hidden">
		<product-bundles :bs="['intermediate-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/add/17" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection