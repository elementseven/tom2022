@php
$page = 'Where I Went Wrong';
$pagename = 'Where I Went Wrong';
$pagetitle = "Where I Went Wrong - Tom Morrison";
$meta_description = "The book I’d love to go back in time and give myself when I started training. An original, honest look at mobility from a different perspective.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.owl-carousel .owl-stage-outer{
		overflow: visible !important;
	}
	.owl-dots{
  	margin-left: 100px;
	}
	#smm-bundles .text-grey{
		color: #9c9d9f !important;
	}
	.workout-card{
		border: 2px solid #000000;
	}
	.shadow{
		box-shadow: 0 .5rem 1rem 0 rgba(0,0,0,0.2)!important;
	}
	@media only screen and (max-width: 767px){
		body h1.lp-title {
		    font-size: 11vw;
		}
	}
		
</style>
@endsection
@section('header')
<header class="container pt-5 mob-pt-0">
	<div class="row">
		<div class="col-12 pt-5 ipad-pt-0 mb-4">
			<h1 class="lp-title text-dark mb-3 mob-mb-0">Where I Went Wrong<span class="tm"></span></h1>
			<p class="mb-0 larger" style="color:#F8D21B;">
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star"></i>
					<i class="fa fa-star mr-2"></i>
				</p>
		</div>
		<div class="col-lg-6 mob-px-0 mob-mb-3" style="min-height:281px;max-height: 352px; overflow: hidden;">
			<e-book-slider></e-book-slider>
		</div>
		<div class="col-lg-6">
			<div class="product-intro-card card border-0 shadow p-4 mob-px-2">
				<div class="px-2"> 
						@if($product->sale_price != NULL)
						<p class="mimic-h3 mb-3 mt-2">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}</span></p>
						@else
						<p class="mimic-h3 mb-3 mt-2"><span class="text-primary">Only</span> &pound;{{$product->price}}</p>
						@endif
						<p class="bundle-green-p mob-text-small"><span class="px-2 py-1">Buy as a Bundle & save up to 23%!</span><a href="#program-bundles" class="px-2 py-1">Shop Now</a></p>
						<p class="mob-mb-0">
							<a href="/basket/add/{{$product->id}}">
								<button type="button" class="btn btn-primary d-inline-block mx-auto mb-2">Add To Basket</button>
							</a>
							<a href="/gift-basket/add/{{$product->id}}">
								<button type="button" class="btn btn-outline d-inline-block ml-2 mb-2 gift-btn"><i class="fa fa-gift text-primary"></i>&nbsp; Gift It</button>
							</a>
						</p>
						<div class="">
							
							<p class="mb-0 text-small"><i><b>All currencies accepted</b></i></p>
							<p class="mb-3 text-small text-light-grey">Your payment will be converted at the current rate.</p>
							<div class="d-block mb-2">
								<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
								<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
								<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
								<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<main>
	<div class="container pt-5 pb-4">
		<div class="row">
			<div class="col-lg-8">
				<p class="mimic-h3">About Where I Went Wrong<span class="tm"></span></p>
				<p>An original, honest look at mobility from a different perspective.</p>
				<p>Rather than another “Here’s the best way to do things because I say so” manual, I decided to share all of the mistakes and places I completely went wrong. These mistakes led me to teach the way I do and are the reason why I readily share so much information for free.</p>
				<p>Where I Went Wrong is the book I would love to go back in time and give to myself when I started training. If I can spread the idea of always striving for better movement, no matter where you are on your fitness journey, then every second will have been worth it.</p>
			</div>
		</div>
		<p class="mimic-h3 text-primary mt-5">One-Time Purchase, <br class="d-md-none" />Lifetime Access</p>
	</div>
	<div class="container px-4 mb-minus-3rem">
		<div class="row justify-content-center">
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/wiww/grid1.webp" type="image/webp"/> 
					<source srcset="/img/programs/wiww/grid1.jpg" type="image/jpeg"/>
					<img src="/img/programs/wiww/grid1.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Where I Went Wrong image 1" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/wiww/grid2.webp" type="image/webp"/> 
					<source srcset="/img/programs/wiww/grid2.jpg" type="image/jpeg"/>
					<img src="/img/programs/wiww/grid2.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Where I Went Wrong image 2" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/wiww/grid3.webp" type="image/webp"/> 
					<source srcset="/img/programs/wiww/grid3.jpg" type="image/jpeg"/>
					<img src="/img/programs/wiww/grid3.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Where I Went Wrong image 3" lazy/>
				</picture>
			</div>
			<div class="col-lg-3 col-md-3 col-6 mb-4">
				<picture> 
					<source srcset="/img/programs/wiww/grid4.webp" type="image/webp"/> 
					<source srcset="/img/programs/wiww/grid4.jpg" type="image/jpeg"/>
					<img src="/img/programs/wiww/grid4.jpg" width="202" height="202" class="w-100 h-auto shadow" alt="Tom Morrison - Where I Went Wrong image 4" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-primary py-5 mob-py-4">
		<div class="row pt-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-9">
						<div class="card bg-white shadow border-0 px-5 py-4 mb-4 mob-px-3 mob-py-3">
							<p class="mimic-h3">Inside this ebook find out:</p>
							<ul class="check-graphics-green mb-0">
								<li>That being “clumsy” or uncoordinated is just a result of training history and can be completely retrained</li>
								<li>How some people are not ready to workout and can get hurt easily through no fault of their own or coach</li>
								<li>Why I went from an obsessed foam rolling guru to being completely against it</li>
								<li>When a serious back injury was actually one of the best things to ever happen to me and has not stopped my physical freedom or strength gains</li>
								<li>How changing your daily habits is actually key to creating good mobility</li>
								<li>That even with no sporting background, I have now ended up influencing how trainers and therapists work with people all over the world</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5">
		<div class="row">
			<div class="col-lg-7">
				<p class="mimic-h3 mb-4">What You Get With Where I Went Wrong<span class="tm"></span></p>
				<div class="row">
					<div class="col-12 d-lg-none mb-4">
						<picture> 
							<source srcset="/img/programs/wiww/devices.webp" type="image/webp"/> 
							<source srcset="/img/programs/wiww/devices.jpg" type="image/jpeg"/>
							<img src="/img/programs/wiww/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Where I Went Wrong devices" lazy/>
						</picture>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/wiww/lightbulb.svg" alt="Tom Morrison video icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>An awesome ebook taking you through Tom's journey, experiences and lessons</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/wiww/info.svg" alt="Tom Morrison lightbulb icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p>A bonus daily mobility routine you can start right away</p>
					</div>
					<div class="col-lg-1 col-2 pr-0 mb-3">
						<img src="/img/programs/wiww/strength.svg" alt="Tom Morrison info icon" width="50" height="50" class="w-100 h-auto" />
					</div> 
					<div class="col-lg-11 col-10 mb-3">
						<p><b>Plus FREE:</b> The Non-Negotiables & Super Tricks chapter for training & mobility</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 px-0 d-none d-lg-block">
				<picture> 
					<source srcset="/img/programs/wiww/devices.webp" type="image/webp"/> 
					<source srcset="/img/programs/wiww/devices.jpg" type="image/jpeg"/>
					<img src="/img/programs/wiww/devices.jpg" width="460" height="259" class="w-100 h-auto" alt="Where I Went Wrong devices" lazy/>
				</picture>
			</div>
		</div>
	</div>
	<div class="container mb-5">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="text-center">
					@if($product->sale_price != NULL)
					<p class="mimic-h3">Only <s>£{{number_format((float)$product->price, 2, '.', '')}}</s> <span class="text-primary">£{{number_format((float)$product->sale_price, 2, '.', '')}}!</span></p>
					@else
					<p class="mimic-h3"><span class="text-primary">Only</span> &pound;{{$product->price}}!</p>
					@endif
					<a href="/basket/add/2">
						<button class="btn btn-primary d-inline-block	"><i class="fa fa-cart-add"></i> Add To Basket</button>
					</a>
					<div class="d-block mb-2 mt-4">
						<img src="/img/logos/paypal.svg" alt="Tom Morrison accepts PayPal - PayPal logo" width="80" class="pp-logo d-inline mr-3" lazy/>
						<img src="/img/logos/mastercard.svg" alt="Tom Morrison accepts mastercard - mastercard logo" width="40" class="m-logo d-inline mr-3" lazy/>
						<img src="/img/logos/visa.svg" alt="Tom Morrison accepts visa - visa logo" width="60" class="v-logo d-inline mr-3" lazy/>
						<img src="/img/logos/amex.svg" alt="Tom Morrison accepts amex - amex logo" width="30" class="am-logo d-inline" lazy/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="view-smm-bundles" class="container mt-5">
		<div class="row">
			<div class="col-12">
				<p class="mimic-h2">Discounted Bundles</p>
				<p class="bundle-green-p mb-4 px-2 py-1"><span class=""><b>Save up to 23%</b> when you buy Where I Went Wrong as part of a Bundle!</span></p>
			</div>
		</div>
	</div>
	<div id="program-bundles" class="mw-100 overflow-hidden">
		<product-bundles :bs="['beginners-bundle','complete-bundle']"></product-bundles>
	</div>
	<a href="/basket/add/2" class="add-to-basket-follow">
		<i class="fa fa-cart-plus"></i>
	</a>
</main>
@endsection