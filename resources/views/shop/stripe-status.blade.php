@php
$page = 'Success';
$pagename = 'Shop';
$pagetitle = 'Success | Your purchase was successful';
$meta_description = 'You have successfully purchased the awesome Tom Morrison product(s) you wanted!';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('head_section')
@endsection
@section('header')
<header class="container mb-5">
  <div class="row">
    <div class="col-lg-10 text-left">
      <h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Basket</span><span class="checkout-title-line "></span><span class="text-dark">Checkout</span><span class="checkout-title-line"></span><span class="text-primary">Success!</span></h1>
      <hr class="dark-line my-4">
      <h3 class="mb-5">Thank you for your payment</h4>
      <p>You will receive an email to <span class="text-primary">{{$currentUser->email}}</span> containing your password and confirming your order details shortly. Please check your inbox and your spam folder.</p>
      <p class="">Please remember to download your purchases from <a href="/login">your account</a> on the Tom Morrison website.</p>
      <a href="/login">
        <div class="btn btn-primary">Login</div>
      </a>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5">
  <div class="row">
    <div class="col-12">
      <p id="message"></p>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  // Initialize Stripe.js using your publishable key
  const stripe = Stripe("{{env('STRIPE_KEY')}}");

  // Retrieve the "payment_intent_client_secret" query parameter appended to
  // your return_url by Stripe.js
  const clientSecret = new URLSearchParams(window.location.search).get(
    'payment_intent_client_secret'
  );

  // Retrieve the PaymentIntent
  stripe.retrievePaymentIntent(clientSecret).then(({paymentIntent}) => {
    const message = document.querySelector('#message')

    // Inspect the PaymentIntent `status` to indicate the status of the payment
    // to your customer.
    //
    // Some payment methods will [immediately succeed or fail][0] upon
    // confirmation, while others will first enter a `processing` state.
    //
    // [0]: https://stripe.com/docs/payments/payment-methods#payment-notification
    switch (paymentIntent.status) {
      case 'succeeded':
        message.innerText = 'Success! Payment received.';
        break;

      case 'processing':
        message.innerText = "Payment processing. We'll update you when payment is received.";
        break;

      case 'requires_payment_method':
        message.innerText = 'Payment failed. Please try another payment method.';
        // Redirect your user back to your payment page to attempt collecting
        // payment again
        break;

      default:
        message.innerText = 'Something went wrong.';
        break;
    }
  });
</script>
@endsection