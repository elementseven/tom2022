@php
$page = 'Success';
$pagename = 'Shop';
$pagetitle = 'Success | Your purchase was successful';
$meta_description = 'You have successfully purchased the awesome Tom Morrison product(s) you wanted!';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('head_section')
<script>
  dataLayer.push({ ecommerce: null });
  dataLayer.push({
    event: "purchase",
    ecommerce: {
      transaction_id: "{{$invoice->transaction_id}}",
      value: {{number_format($invoice->price, 2)}},
      tax: 0,
      shipping: 0,
      currency: "GBP",
      payment_type: "{{$invoice->payment_method}}",
      items: [
        @foreach($invoice->bundles as $key => $bundle)
        @foreach($bundle->products as $product)
        {
          item_id: "{{$product->id}}",
          item_name: "{{$product->name}}",
          affiliation: "Tom Morrison",
          item_category: "{{$product->category->name}}",
          price: {{number_format($product->pivot->price, 2)}}, 
          quantity: 1
        }@if($key != count($bundle->products) - 1) , @endif
        @endforeach
        @endforeach
        @foreach($invoice->products as $key => $product)
        {
          item_id: "{{$product->id}}",
          item_name: "{{$product->name}}",
          affiliation: "Tom Morrison",
          item_category: "{{$product->category->name}}",
          price: {{number_format($product->pivot->price, 2)}}, 
          quantity: 1
        }@if($key != count($invoice->products) - 1) , @endif
        @endforeach
        @if(count($invoice->products) && count($invoice->variants)),@endif
        @foreach($invoice->variants as $key => $variant)
        {
          item_id: "{{$variant->id}}",
          item_name: "{{$variant->merch->name}} - {{$variant->size}}",
          affiliation: "Tom Morrison",
          item_category: "Merch",
          price: {{number_format($variant->pivot->price, 2)}}, 
          quantity: 1
        }@if($key != count($invoice->variants) - 1) , @endif
        @endforeach
      ]
    }
  });
</script>
@php
Cart::destroy();
@endphp
@endsection
@section('header')
<header class="container mb-5 pt-5 mob-pt-0">
  <div class="row">
    <div class="col-lg-12 text-left">

      <h1 class="checkout-title mob-mt-3 pb-2 mob-mb-0 mob-pb-0"><span class="text-dark">Basket</span><span class="checkout-title-line "></span><span class="text-dark">Checkout</span><span class="checkout-title-line"></span><span class="text-primary">Success!</span></h1>
      <hr class="dark-line my-4">
      <order-status :stage="3"></order-status>
      <h3 class="mb-2 text-primary">Thanks for your Order!</h4>
      <p class="text-large"><b>Your payment has been processed successfully</b></p>
      <div class="bg-light p-3 mb-4" style="border-radius: 5px;">
        <p>An order confirmation has been sent to <span class="text-primary">{{$invoice->user->email}}</span></p>
        <p class="mb-2"><b>To access your new products, <a href="/login">login to your account</a>:</b></p>
        <a href="/login">
          <div class="btn btn-primary mb-2">Login</div>
        </a>
      </div>
      
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5">
    <div class="row pad_top mob_small_pad">
      <div class="col-sm-12 text-left">
        <div class="card shadow p-4 border-0">
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <h4 class="mob_smaller">Order Details</h4>
              <p class="noprint">You can <span id="print_page" class="text-primary">print and keep this page</span> or keep a note of the payment reference.</p>
            </div>
         </div>
         <div class="row">
          <div class="col-12">
            <p class="mb-1"><b>Ref:</b> {{$invoice->id}}</p>
            <p class="mb-4"><b>Order placed:</b> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($invoice->created_at))->format('g:ia - dS M Y') }}</p>
            <p class="mb-1"><b>Name:</b> {{$invoice->user->first_name}} {{$invoice->user->last_name}}</p>
            <p class="mb-1"><b>Email:</b> {{$invoice->user->email}}</p>
            <p class="mb-3"><b>Payment Method:</b> {{$invoice->payment_method}}</p>
            @if($invoice->shipping)
              @foreach($invoice->user->addresses as $a)
                @if($a->type == 'shipping')
                  <p class="mb-0"><b>Shipping Address</b></p>
                  <p>@if($a->company){{$a->company}}<br/>@endif {{$a->streetAddress}}, @if($a->extendedAddress){{$a->extendedAddress}},@endif {{$a->city}}, {{$a->postalCode}}, <br/>  {{$a->region}}, {{$a->country}}</p>
                @endif
              @endforeach
            @endif
          </div>
        </div>
        @if(count($invoice->variants))
        <hr class="line_full">
        <div class="row">
          <div class="col-sm-12 pad_small_top">
            <p class="text-title text-large mt-4 mb-3">Merchandise</p>
            <div class="row">
              @foreach($invoice->variants as $key => $v)
              <div class="col-8">
                <p class="scrollFade text-larger" data-fade="fadeIn">{{$v->merch->name}} - {{$v->size}}</p>
              </div>
              <div class="col-4 text-right">
                <p class="scrollFade text-larger" data-fade="fadeIn">@if($v->pivot->price < $v->price)<s class="text-primary text-small">£{{$v->price}}</s> - @endif£{{$v->price}}</p>
              </div>
              @endforeach 
            </div>
          </div>
        </div>
        @endif
        @if(count($invoice->bundles))
        <hr class="line_full">
        <div class="row">
          <div class="col-sm-12 pad_small_top">
            <p class="text-title text-large mt-4 mb-3">Bundle(s)</p>
            <div class="row">
              @foreach($invoice->bundles as $key => $bundle)
              <div class="col-8">
                <p class="scrollFade text-larger" data-fade="fadeIn">{{$bundle->title}}</p>
              </div>
              <div class="col-4">
                <p class="scrollFade text-larger text-right" data-fade="fadeIn">@if($bundle->pivot->price < $bundle->price)<s class="text-primary text-small">£{{$bundle->price}}</s> - @endif£{{$bundle->pivot->price}}</p>
              </div>
              @endforeach 
              <div class="col-md-12">
                <hr class="mb-5">
              </div>
            </div>
          </div>
        </div>
        @endif
        @if(count($invoice->upgrades))
        <hr class="line_full">
        <div class="row">
          <div class="col-sm-12 pad_small_top">
            <p class="text-title text-large mt-4 mb-3">Upgrade(s)</p>
            <div class="row">
              @foreach($invoice->upgrades as $key => $u)
              <div class="col-8">
                <p class="scrollFade text-larger" data-fade="fadeIn">{{$u->title}}</p>
              </div>
              <div class="col-4">
                <p class="scrollFade text-larger text-right" data-fade="fadeIn">@if($u->pivot->price < $u->price)<s class="text-primary text-small">£{{$u->price}}</s> - @endif£{{$u->pivot->price}}</p>
              </div>
              @endforeach 
              <div class="col-md-12">
                <hr class="mb-5">
              </div>
            </div>
          </div>
        </div>
        @endif
        @if(count($invoice->products))
        <hr class="line_full">
        <div class="row">
          <div class="col-sm-12 pad_small_top">
            <p class="text-title text-large mt-4 mb-3">Product(s)</p>
            <div class="row">
              @foreach($invoice->products as $key => $product)
              <div class="col-8">
                <p class="scrollFade text-larger" data-fade="fadeIn">{{$product->name}}</p>
              </div>
              <div class="col-4">
                <p class="scrollFade text-larger text-right" data-fade="fadeIn">@if($product->pivot->price < $product->price)<s class="text-primary text-small">£{{$product->price}}</s> - @endif£{{$product->pivot->price}}</p>
              </div>
              @endforeach 
              <div class="col-md-12">
                <hr class="mb-5">
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="row">
          <div class="col-sm-12 pad_small_top text-right">
            @if($invoice->shipping)
            <p class="mb-1"><b>Sub Total:</b> £{{$invoice->price}}</p>
            <p><b>Shipping:</b>&nbsp; &pound;{{number_format($invoice->shipping, 2)}}</p>
            <p class="mb-0 text-title text-large">Total:&nbsp; £{{number_format($invoice->shipping, 2) + number_format($invoice->price, 2)}}</p>
            @else
            <p class="mb-0 text-title text-large">Total:&nbsp; £{{number_format($invoice->price, 2)}}</p>
            @endif
          </div>
        </div>
      </div>
    </div>
    @if($invoice->hasProduct(5))
    <div class="col-12 mt-4">
      <div class="card shadow p-4 border-0 mb-4">
        <h4>Book your video call</h4>
        <p>We use Calendly to schedule video calls, please click the button below to book your time slot.</p>
        <a href="https://calendly.com/tom-morrison/45min?month={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m')}}&date={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m-d')}}" target="_blank">
          <button class="btn btn-primary d-inline-block">Book time slot</button>
        </a>
      </div>
    </div>
    @endif
    <div class="col-12 mt-5">
      <p class="text-right"><b>To access your new products, <a href="/login">login to your account</a>:</b></p>
      <a href="/login">
        <div class="btn btn-primary float-right">Login</div>
      </a>
    </div>
  </div>
</div>
@endsection
@section('modals')
@if($upgrade != null)
<div id="upsellModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered px-5 mob-px-0" role="document">
    <div class="modal-content border-0 text-center">
      <div class="modal-header bg-light border-0">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12">
              <img src="/img/smm/smm-circle.png" alt="smm purchased successfully" width="134" height="134" class="smm-upsell-circle mb-3"/>
              @if($upgrade->product)
              <h5 class="mob-mb-2 upsell-title mb-1">{{$upgrade->product->name}}</h5>
              @else
              <h5 class="mob-mb-2 upsell-title mb-1">{{$upgrade->bundle->title}}</h5>
              @endif
              <p class="text-light-grey font-16-14 mb-1">Successfully purchased</p>
              <img src="/img/icons/check-green.svg" alt="success!" width="27" height="27"/>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body border-0"> 
        <div class="container mob-px-0">
          <div class="row justify-content-center mb-4">
            <div class="col-lg-11 px-3 mob-px-3">
              
              <h5 class="modal-title text-primary">Special Bonus Offer!</h5>
              <p id="clockdiv" class="text-light-grey font-16-14 mb-1">Valid for <span class="minutes">08</span>:<span class="seconds">43</span></p>
              <div class="smm-upsell-desc"><b><span class="text-primary">This Page Only:</span> {{$upgrade->textone}}</b></div>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-3 col-5">
              <picture> 
                <source srcset="{{$upgrade->getFirstMediaUrl('upgrades', 'normal-webp')}}" type="image/webp"/> 
                <source srcset="{{$upgrade->getFirstMediaUrl('upgrades', 'normal')}}" type="{{$upgrade->getFirstMedia('upgrades')->mime_type}}"/>
                <img src="{{$upgrade->getFirstMediaUrl('upgrades', 'normal')}}" class="mw-100 h-auto smm-upsell-product-image mob-w-100" alt="{{$upgrade->name}} - Special offer" width="116" height="130"/>
              </picture>
            </div>
            <div class="col-lg-7 col-7 pl-0 mob-px-3 text-left">
              <p class="larger mob-font-16 mb-2 mob-mb-3">{{$upgrade->texttwo}}</p>
              <p class="upsell-prices"><s class="text-light-grey">£{{$upgrade->price}}</s> &nbsp;<span class="text-primary">£{{number_format((float)$upgrade->sale_price, 2, '.', '')}}</span></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer border-0 text-center">
        <div class="container px-0">
          <div class="row">
            <div class="col-12 mb-3">
              <a href="/basket/upgrades/add/{{$upgrade->id}}/{{$upgrade->slug}}">
                <button type="button" class="btn btn-primary d-inline-block w-100 mw-300 mob-no-mw text-capitalize border-radius-0">Yes please! Upgrade Now</button>
              </a>
            </div>
            <div class="col-12">
              <p data-dismiss="modal" aria-label="Close" class="cursor-pointer text-small mb-0">No thanks, I'm happy to lose this offer</p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
@endif
@if($invoice->hasProduct(5))
<div id="videoCallModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered px-5 mob-px-0" role="document">
    <div class="modal-content border-0 text-center">
      <div class="modal-header bg-light border-0">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12">
              <img src="/img/icons/check-green.svg" alt="success!" width="36" height="36" class="mb-2" />
              <h5 class="mob-mb-2 upsell-title mb-1">Video Call</h5>
              <p class="text-light-grey font-16-14 mb-1">Successfully purchased</p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body border-0 pb-0 pt-4"> 
        <div class="container mob-px-0">
          <div class="row justify-content-center">
            <div class="col-lg-11 px-3 mob-px-3">
              <h5 class="modal-title text-primary">Book your video call</h5>
              <div class="smm-upsell-desc"><b>We use Calendly to schedule video calls, please click the button below to book your time slot.</b></div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer border-0 pb-4 text-center">
        <div class="container px-0">
          <div class="row">
            <div class="col-12 mb-3 text-center">
              <a href="https://calendly.com/tom-morrison/45min?month={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m')}}&date={{Carbon\Carbon::parse(strtotime('next Friday'))->format('Y-m-d')}}" target="_blank">
                <button class="btn btn-primary d-inline-block">Book time slot</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('scripts')
<script>
  document.getElementById("print_page").addEventListener("click", function(){
    window.print();
  });

  @if($invoice->hasProduct(5))
  document.getElementById('videoCallModal').style.display = 'block';
  @endif
  @if($upgrade != null)

  document.getElementById('upsellModal').style.display = 'block';

  document.addEventListener('DOMContentLoaded', function() {
    var closeButton = document.querySelector('[data-dismiss="modal"]');
    if (closeButton) {
      closeButton.addEventListener('click', function() {
        document.getElementById('upsellModal').style.display = 'none';
      });
    } else {
      console.log('Dismiss button not found');
    }
  });
    
  function getTimeRemaining(endtime) {

    const total = Date.parse(endtime) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    
    return {
      total,
      minutes,
      seconds
    };
  }

  function initializeClock(id, endtime) {
    const clock = document.getElementById(id);
    const minutesSpan = clock.querySelector('.minutes');
    const secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
      const t = getTimeRemaining(endtime);

      minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
      secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

      if (t.total <= 0) {
        clearInterval(timeinterval);
      }
    }

    updateClock();
    const timeinterval = setInterval(updateClock, 1000);
  }
  const timeInMinutes = 10;
  const currentTime = Date.parse(new Date());
  const deadline = new Date(currentTime + timeInMinutes*60*1000);

  document.addEventListener('DOMContentLoaded', function() {
    initializeClock('clockdiv', deadline);
  });
  @endif
</script>
@endsection