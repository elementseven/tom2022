@php
$page = 'Claim Your Gift';
$pagename = 'Claim Your Gift';
$pagetitle = 'Claim Your Gift | Tom Morrison';
$meta_description = 'Claim your gift';
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('header')
<header class="container mt-5">
  <div class="row">
    <div class="col-lg-12 text-left">
      <h1 class="text-primary smaller-title">Claim Your Gift</h1>
      <hr class="dark-line my-4">
      <h3 class="mb-2 mt-5">Hi {{$gift->first_name}}, {{$gift->invoice->user->full_name}} has sent you a gift!</h4>
      <p class="mb-0">Now you can truly become Flong & Sexible!</p>
      <p class="mb-0">Once you've created your account (below) you'll have full access to:</p>
    </div>
    @foreach($gift->products as $product)
    <div class="col-lg-6 mt-4">
      <div class="card">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 col-4 pl-0">
              <div class="bg" style="background-image: url('/storage/{{$product->image}}'); width: 100%; min-height: 140px; height: 100%;"></div>
            </div>
            <div class="col-lg-7 col-8 pl-0">
              <h5 class="mb-2 mt-3 scroll">{{$product->name}}</h5>
              <p class="mb-3 text-small">{{$product->exerpt}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</header>
@endsection
@section('content')
<div class="container mb-5 py-5">
  <div class="row">
    @if(Auth::check())
    <div class="col-12 mt-5">
      <h3 class="mb-3 text-primary">Accept your gift</h3>
      <p>You are currently logged in as {{Auth::user()->full_name}}, do you want to add this gift to your account?<br/>If not you can <a href="/logout">logout</a> and create a new account with a different email address.</p>
      @if(session('already-purchased'))
      <p class="text-danger">You already have already purchased {{session('already-purchased')}}. Please contact <a href="mailto:support@tommorrison.uk">support@tommorrison.uk</a>.</p>
      @php session()->forget('already-purchased'); @endphp
      @else
      <a href="/accept-gift/{{$gift->id}}/{{$gift->code}}">
        <button type="button" class="btn btn-primary">Accept Gift</button>
      </a>
      @endif
    </div>
    @else
    <div class="col-12 mt-5">
      <h3 class="mb-3 text-primary">Create your account</h3>
      <p>In order to claim your gift & access your new programme(s), you'll need to set up an account to login. <br/>Already have an account? <a href="/login?redirect={{ urlencode(request()->fullUrl()) }}">Please login first!</a>.<br/>Please fill in your details below:</p>
       <form id="create-account-form" class="row" action="/gift-create-account" method="POST">
        {{ csrf_field() }}
        <input type="hidden" value="{{$gift->id}}" name="id"/>
        <input type="hidden" value="{{$gift->code}}" name="code"/>
        <div class="col-lg-4 mb-3">
          <label class="mb-0" for="first_name"><b>First Name*</b></label>
          <input id="first_name" type="text" name="first_name" class="form-control mb-0" placeholder="First Name*" value="{{$gift->first_name}}" required/> 
          @if ($errors->has('first_name'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('first_name') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-lg-4 mb-3">
          <label class="mb-0" for="first_name"><b>Last Name*</b></label>
          <input id="last_name" type="text" name="last_name" class="form-control mb-0" placeholder="Last Name*" value="{{$gift->last_name}}" required/> 
          @if ($errors->has('last_name'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('last_name') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-lg-4 mb-3">
          <label class="mb-0" for="country"><b>Country*</b></label>
          <countries :setcountry="'GB'" :inid="'country'"></countries>
          @if ($errors->has('country'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('country') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-sm-6 mb-3">
          <label class="mb-0" for="email"><b>Email Address*</b></label>
          <input id="email" type="text" name="email" autocomplete="off" class="form-control mb-0" placeholder="Email Address*" value="{{ old('email') }}" required/> 
          @if ($errors->has('email'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-sm-6 mb-3">
          <label class="mb-0" for="email-confirm"><b>Confirm Email Address*</b></label>
          <input id="email-confirm" type="text" autocomplete="off" name="email_confirmation" class="form-control mb-0" placeholder="Email Address*" value="{{ old('email-confirm') }}" required/> 
          @if ($errors->has('email-confirm'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('email-confirm') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-sm-6 mb-3">
          <label class="mb-0" for="password"><b>Choose a password*</b></label>
          <input id="password" type="password" name="password" autocomplete="new-password" class="form-control mb-0" placeholder="Password" value="" required/> 
          @if ($errors->has('password'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-sm-6 mb-3">
          <label class="mb-0" for="password-confirm"><b>Confirm password*</b></label>
          <input id="password-confirm" type="password" autocomplete="new-password" name="password_confirmation" class="form-control mb-0" placeholder="Password confirmation" value="" required/> 
          @if ($errors->has('password-confirm'))
          <span class="help-block text-danger">
            <strong>{{ $errors->first('password-confirm') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-12 mt-3">
          <div id="consent_holder" class="d-block">
            <label class="mb-0" for="consent" class="text-small">
              <input id="consent" class="mr-1" type="checkbox" name="consent"/>
              By ticking this box you give your consent for the personal information entered on this form to be stored in a secure database. Your information will not be shared or used for marketing and is only stored so you may log in and continue to use the features of this application. You may remove your information from our database by contacting me at any time. You also consent to the <a href="{{route('tandcs')}}" target="_blank">Terms &amp; conditions</a> and <a href="{{route('privacy-policy')}}">Privacy Policy</a>.
            </label>
            @if ($errors->has('consent'))
            <span class="help-block text-danger">
              <strong>{{ $errors->first('consent') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="col-12 text-center text-lg-right">
          <hr class="mt-3 mb-4" />
          <button type="submit" class="btn btn-primary d-inline-block">Create Account</button>
        </div>
      </form>
    </div>
    @endif
  </div>
</div>
@endsection
@section('scripts')
@endsection