@php
$page = "Shop";
$pagename = 'Shop';
$pagetitle = "Shop - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison’s mobility programs and strength programs such as the Simplistic Mobility Method. They’re designed to balance your body, improve flexibility, build core strength, prevent injuries, aid rehab and improve lifting techniques";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('head_section')
<link rel="prefetch" url="/js/ShopIndex.js" />
<link rel="prefetch" url="/js/ShopLevelOne.js" />
<link rel="prefetch" url="/js/ShopLevelTwo.js" />
<link rel="prefetch" url="/js/ShopLevelThree.js" />
<script>
	dataLayer.push({ ecommerce: null });
	dataLayer.push({
		event: "view_item_list",
	  ecommerce: {
	  	item_list_id: "shop_page",
    	item_list_name: "Shop Page",
	    currency: 'GBP',
	    items: [
		    @foreach($products as $key => $product)
		   {
		     item_id: "{{$product->id}}",
         item_name: "{{$product->name}}",
		     affiliation: 'Tom Morrison',
		     item_category: "{{$product->category->name}}",
		     price: {{number_format($product->price, 2)}},
		     index: {{$key + 1}}
		   } @if($key != count($products) - 1) , @endif
		   @endforeach
		]
  }
});
</script>
@endsection
@section('styles')
@endsection
@section('content')
<div class="container pt-lg-5 pt-3 mb-4 mob-mb-0 ">
	<div class="row">
		<div class="col-12">
			<h1 class="top-title">Shop</h1>
			<!-- <picture>
				<source media="(min-width: 769px)" srcset="/img/sales/valentines/shop-banner.webp" type="image/webp"/>
				<source media="(min-width: 769px)" srcset="/img/sales/valentines/shop-banner.jpg" type="image/jpeg"/>
				<source srcset="/img/sales/valentines/shop-banner-mob.webp" type="image/webp"/>
				<source srcset="/img/sales/valentines/shop-banner-mob.jpg" type="image/jpeg"/>
				<img src="/img/sales/valentines/shop-banner-mob.jpg" width="730" height="326" class="w-100 h-auto" alt="Tom morrison Valentine's Sale Banner." />
			</picture> -->
		</div>
	</div>
</div>
<shop-index></shop-index>
<div class="container py-5">
	<div class="row">
		<div class="col-12">
			<p class="text-large mb-0"><b>Not sure which is the best option for you? Check out: <a href="/blog/which-program-should-i-choose">'Which Program Should I Buy'</a></b></p>
		</div>
	</div>
</div>
@endsection