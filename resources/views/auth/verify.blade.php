@php
$pagename = 'Login';
$page = 'Verify Email';
$pagetitle = "Verify Email - Tom Morrison user area";
$meta_description = 'Verify your email address to access your Tom Morrison account to download your purchases and watch exclusive videos!';
$og_image = "https://tommorrison.uk/img/home/tom-morrison-homepage2.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])

@section('content')
<header class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="d-table w-100 top-section">
                <div class="d-table-cell align-middle top-section">
                    <h1 class="page-title mb-4">Verify Email</h1>
                    <p class="mt-3">Thanks for purchasing your Tom Morrison products! We just need you to verify your email address ({{$currentUser->email}}). Check your inbox and junk folder for an email from tommorrison.com and click the verifiction link in the email.</p>
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <picture> 
                <source srcset="/img/shop/tom-morrison-basket.webp" type="image/webp"/> 
                <source srcset="/img/shop/tom-morrison-basket.jpg" type="image/jpeg"/>
                <img src="/img/shop/tom-morrison-basket.jpg" class="basket-top-image img-fluid" alt="Tom morrison stretching a band in his shopping basket." />
            </picture>
        </div>
    </div>
</header>
@endsection
