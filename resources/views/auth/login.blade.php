@php
$page = 'Login';
$pagename = 'Login';
$pagetitle = "Login - Tom Morrison user area";
$meta_description = 'Log in to your Tom Morrison account to download your purchases and watch exclusive videos!';
$og_image = "https://tommorrison.uk/img/home/tom-morrison-homepage2.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'nofooter' => true])
@section('styles')
<style>
    #content{
        padding-top: 0 !important;
    }
.no-webp .smm-login-bg::before{
    background-image: url(/img/bg/login.jpg);
}
.webp .smm-login-bg::before{
    background-image: url(/img/bg/login.webp);
}
.smm-login-bg{
    position: relative;
}
.smm-login-bg:before{
    content: "";
    width: 50%;
    height: 100%;
    position: absolute;
    right: 0;
    top: 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;

}
p.prompt{
    position: absolute;
    bottom: 1rem;
    left: 6rem;
}
p.login-p{
    font-size: 0.9rem;
    line-height: 1.5;
}
label.title{
    color: #000;
    font-size: 0.9rem;
    margin-bottom: 5px;
    font-family: Oswald !important;
}
.d-table{
    height: 100vh;
}
h1.login-title{
    font-size: 55px;
    font-weight: 600;
    color: #D82737 !important;
    font-family: 'Oswald', sans-serif;
}
input.form-control{
    height: 40px;
    display: block;
    width: 100%;
    padding: 0.375rem 0.75rem;
    font-size: .9rem;
    font-weight: 400;
    line-height: 1.6;
    color: #212529;
    background-color: #f8fafc;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    appearance: none;
    border-radius: 0.375rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait) {
    h1.login-title {
        font-size: 35px;
    }
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape) {
    h1.login-title {
        font-size: 35px;
    }
    .col-lg-9{
        flex: 0 0 100%;
        max-width: 100% !important;
    }
}
@media only screen and (max-width : 767px){
    body p.prompt{
        position: absolute;
        font-size: 3vw !important;
        bottom: 0;
        padding-left: 0;
        left: 0;
        width: 100%;
    }
    .d-table{
        height: auto;
        padding-top: 30px;
    }
    h1.login-title{
        font-size: 9vw;
    }
    .smm-login-bg{
        position: relative;
        
    }
    .no-webp .smm-login-bg::before{
        display: none;
    }
    .webp .smm-login-bg::before{
        display: none;
    }
    .mob-min-100{
        min-height: 100vh;
    }
}
</style>
@endsection
@section('content')
<div class="container-fluid bg-white smm-login-bg">
    <div class="container container-wide px-0 position-relative mob-min-100">
        <div class="row">
            <div class="col-lg-5 col-md-8 pl-5 mob-px-3">
                <div class="d-table w-100 pl-5 mob-px-0">
                    <div class="d-table-cell align-middle w-100 h-100">
                        <div class="card border-0">
                            <div class="card-body px-2 pt-5">
                                <h1 class="login-title text-primary mb-3 pt-5">Welcome Back!</h1>
                                <p class="mb-3 pb-3 login-p">Login using the details from your 'Account Created' email.<br/><b>Didn't get an email, or forgotten your password? <a href="/password/reset">Click here to reset it!</a></b></p>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="row mb-3">
                                        <div class="col-12 mb-3 col-lg-9 col-xl-9">
                                            <label for="email" class="text-md-end title"><b>{{ __('Email') }}</b></label>
                                            <input id="email" type="email" class="title form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 mb-3 col-lg-9 col-xl-9">
                                            <label for="password" class="text-md-end title"><b>{{ __('Password') }}</b></label>
                                            <input id="password" type="password" class="title form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-lg-9 col-xl-9 mb-0">
                                            <div class="row">
                                                <div class="col-12 col-lg-5 mb-3">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label text-small" for="remember">
                                                            {{ __('Remember Me') }}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-7 mb- text-lg-right">
                                                    @if (Route::has('password.request'))
                                                        <p style="font-size:0.9rem;"><b><a href="/password/reset">
                                                            {{ __('Forgot My Password') }}
                                                        </a></b></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <input type="hidden" name="redirect" value="{{ request()->input('redirect') }}">
                                            <button type="submit" style="max-width:100% !important;" class="btn btn-primary mb-0 w-100">{{ __('Login') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="prompt text-center text-lg-left mb-0"><i>Don't have a program yet? <a href="https://tommorrison.uk/shop">Check out our shop!</a></i></p>
    </div>
</div>
@endsection