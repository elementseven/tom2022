@php
$pagename = 'Login';
$page = 'Reset Password';
$pagetitle = "Reset Password - Tom Morrison user area";
$meta_description = 'Reset your password to access your Tom Morrison account to download your purchases and watch exclusive videos!';
$og_image = "https://tommorrison.uk/img/home/tom-morrison-homepage2.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])

@section('content')
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-5">
                <div class="card-header p-3 mb-5 text-center" style="background-color:#f2f2f2;"><h4 class="mb-0">{{ __('Reset Password') }}</h4></div>

                <div class="card-body px-3 pb-5">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0 text-center">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary d-inline-block">
                                    {{ __('Send Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
