<div>
    <span 
        id="copy-link-{{ $id }}" 
        class="copy-link" 
        onclick="
            // Create a temporary input element
            const input = document.createElement('input');
            input.setAttribute('value', '{{ $link }}');
            document.body.appendChild(input);
            input.select();
            document.execCommand('copy');
            document.body.removeChild(input);
            alert('Link copied to clipboard');
        "
    >{{ $link }}</span>
</div>

