@php
$page = 'Seminars';
$pagename = 'Seminars';
$pagetitle = "Seminars - Reserve a place on one of Tom Morrison's seminars";
$meta_description = "Gain in depth knowledge of mobility exercises, prehab techniques and rehab techniques at Tom Morrison’s Movement and Mobility Seminar. Does seminars and workshops in UK, Ireland, Europe and anywhere people want to improve their flexibility, movement and core strength.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row">
   <div class="col-md-6">
     <div class="d-table w-100 top-section">
       <div class="d-table-cell align-middle top-section">
         <h1 class="page-title mb-3 mob-mt-3 mob-pb-0">SEMINARS &amp; WORKSHOPS</h1>
         <p>Join Tom & Jenni in person to learn about your body and help you fix imbalances & weaknesses.</p>
         {{-- <div class="btn btn-primary mob-mb-5" data-toggle="modal" data-target="#request_seminar_modal">Request A Seminar</div> --}}
<!--
         <div class="bg-grey my-3 p-3 border-1">
            <p class="mb-1"><b>Not accepting new Seminar/Workshop requests</b></p>
            <p class="mb-0">Due to the uncertainty & disruption caused by Covid-19, we’re currently not accepting any new seminar requests until we believe it’s safe to do so. Any that are booked we are rescheduling where possible and updating when new information is announced. If you have any questions or concerns, please contact us at <a href="mailto:support@tommorrison.uk">support@tommorrison.uk</a></p>
        </div>
-->
       </div>
     </div>
   </div>
   <div class="col-md-6">
     <picture> 
       <source srcset="/img/seminars/tom-morrison-seminars.webp" type="image/webp"/> 
       <source srcset="/img/seminars/tom-morrison-seminars.jpg" type="image/jpeg"/>
       <img src="/img/seminars/tom-morrison-seminars.jpg" class="seminars-top-image img-fluid" alt="Tom morrison and Jenni Sanders standing back to back at a seminar." />
     </picture>
   </div>
 </div>
</header>
@endsection
@section('content')
<seminars></seminars>
<div class="container">
 <div class="row">
  <div class="col-md-6 mob-mb-5">
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/1JT5Z2RQQAM?rel=0" allowfullscreen></iframe>
    </div>
  </div>
  <div class="col-md-6">
    <p class="scroll_fade" data-fade="fadeIn"><b>Movement & Mobility</b> is our main seminar, we've travelled from CrossFit boxes to Calisthenics gyms to help spread the message that it's not ok to live or train in pain and that imbalances, weaknesses and niggles can be fixed.</p>
    <p>It is an all day seminar teaching you how to get the most out of your training, how to avoid injury and how to teach your body to progressively adapt and evolve. We'll give you techniques that actually work and that will last you a lifetime.
      <p class="text-primary cursor-pointer" data-toggle="modal" data-target="#request_seminar_modal"><b><u>Request Movement & Mobility at your gym</u></b></p>
<!--       <p class="text-grey cursor-pointer"><b><u>Request Movement & Mobility at your gym</u></b></p> -->
    </div>
  </div>
  <div class="row my-5">
    <div class="col-md-6 mb-5 d-md-none">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rVFeVx50yok?rel=0" allowfullscreen></iframe>
      </div>
    </div>
    <div class="col-md-6">
      <p>We also run shorter <b>workshops</b>, which are a few hours long rather than a full day.</p>
      <p>Each workshop will have a more specific focus, such as hips, shoulders or core. They're a great way to have a deep dive into a particular problem area - though you'd be surprised how working on one area can have positive repercussions throughout your entire body.</p>
      <p>We can run more than 1 workshop per day, allowing us to tailor the content for beginners / intermediate / advanced to make sure everyone gets the most benefit at the level which suits them best.</p>
      <p class="text-primary cursor-pointer" data-toggle="modal" data-target="#request_seminar_modal"><b><u>Request a Workshop(s) at your gym</u></b></p>
<!-- 	<p class="text-grey cursor-pointer"><b><u>Request a Workshop(s) at your gym</u></b></p> -->
    </div>
    <div class="col-md-6 d-none d-md-block">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rVFeVx50yok?rel=0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="row justify-content-center my-5">
    <div class="col-md-6">
      <seminars-slider></seminars-slider>
    </div>
  </div>
</div>
<div id="faqs" class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h3 class="mb-5">FAQs about our Seminars & Workshops</h3>
    </div>
    <div class="col-12 mb-5">
      <h3 class="text-primary">For Attendees</h3>
      <p class="mb-4 caption">*Click a question to reveal the answer</p>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1"><b>I'm very inflexible, will it suitable for me?</b></a></p>
      <div id="answer_1" class="collapse">
        <p>Absolutely!! We make sure that everyone leaves our seminars knowing exactly where THEY need to start and what they need to improve on – not just give generic advice for you to follow after you become flexible!</p>
        <p>We’ll show you any adaptions you might need now, but also explain different scenarios you might encounter long term so you don’t end up stuck again after a few months’ time, we genuinely want to give you ways you can progress for life.</p>
        <p>To be honest, you’re probably not as bad as you think you are, constantly telling yourself you’re not very flexible might be holding you back. So, come along and not only can you assess your flexibility, but immediately start to work on it!</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2"><b>Are your seminars/workshops suitable for trainers/PTs/coaches?</b></a></p>
      <div id="answer_2" class="collapse">
        <p>Absolutely! Our seminars are attended by coaches of all different disciplines, physiotherapists, chiropractors, personal trainers, yoga teachers - you name it!! Not only is the information great to apply to yourself, but you will learn how to spot imbalances and weaknesses in other people too.</p>
        <p>We have a unique way of teaching how the body works, focusing on what everyone needs to have a pain-free and well-functioning body no matter what their discipline. We have different tests and exercises to help make people aware of what they’re missing in their overall programming relatable to their sport or training style.</p>
        <p>We like to put everyone in together - trainers and people that just enjoy training as the language we use we believe should be understandable to any coach and the complete beginner with no training history. There is too much confusion in the flexibility and stability world without adding big words to appear smarter.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_3" aria-expanded="false" aria-controls="answer_3"><b>I'm currently injured - can I still attend a seminar/workshop?</b></a></p>
      <div id="answer_3" class="collapse">
        <p>Totally! If you want, you can <a href="mailto:hello@tommorrison.uk">send us an email</a> beforehand just to double check, but if it is an ongoing injury or still in the healing process then it will be incredibly beneficial for you to attend.</p>
        <p>Injuries can be accidents, but often they’re a result of long-term compensations. We can help you find out why it probably happened and how you can rebuild strength & range and prevent it from becoming an ongoing issue.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_4" aria-expanded="false" aria-controls="answer_4"><b>I don't do weightlifting/CrossFit - will it be relevant for me?</b></a></p>
      <div id="answer_4" class="collapse">
        <p>Yes! We teach how the body should move regardless of your sport/activities; we just both happen to train CrossFit and feel like good movement is something CrossFit promotes quite well. We cover quite a few systems ranging from running to martial arts to yoga. When your body moves like a body, your options become unlimited.</p>
        <p>We want to help people realise that it’s ok to simply feel good some sessions, you don’t always need to be constantly burning fat or building big muscles. When you physically feel good you’re able to be consistent, which is beneficial no matter what you do.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_5" aria-expanded="false" aria-controls="answer_5"><b>Will it be very physical? Or more just talking?</b></a></p>
      <div id="answer_5" class="collapse">
        <p>We have an introduction talking section at the start but after that there’s a lot of joint movement, exercises and tests, however nothing is mandatory, and you can do/try as much or as little as you want!</p>
        <p>You’ll be activating things like you never have before! But you won’t be getting out of breath or sweaty!</p>
        <p>Everything is demonstrated from a basic level and then shown how to progress over time so you’re always making yourself stronger instead of just maintaining your current level.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_6" aria-expanded="false" aria-controls="answer_6"><b>What If I can't do some of the movements?</b></a></p>
      <div id="answer_6" class="collapse">
        <p>You’d be surprised at how many accomplished athletes have struggled with some of the exercises we teach so you will not be alone! But where would the fun be if you showed up and could just already do everything?</p>
        <p>The day is about developing a new understanding of what real flexibility and stability is - embrace the things you struggle with and turn them into a new goal.</p>
        <p>Anything you struggle with we’ll help to give you an insight into why and how you can work on any weaknesses in the best way possible.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_7" aria-expanded="false" aria-controls="answer_7"><b>What should I wear / bring with me?</b></a></p>
      <div id="answer_7" class="collapse">
        <p>Just whatever you would wear to the gym. Quirky t-shirts with funny phrases are highly recommended and if you show up in a ‘Tom Morrison’ t-shirt be prepared for an epic high five.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_8" aria-expanded="false" aria-controls="answer_8"><b>I can't attend any of your seminars/workshops, how else can I get personalised info off you guys?</b></a></p>
      <div id="answer_8" class="collapse">
        <p>You can check out our <a href="{{route('shop')}}">online programs</a> where many of the techniques we cover are presented in a structured format. If you’d like something more tailored to you, check out our <a href="{{route('product','online-personal-coaching')}}">one-on-one coaching</a> with either one of us, where we can look at your technique/posture/issues/injuries and create a program based on your current needs.</p>
      </div>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-12">
      <h3 class="text-primary">For Hosts</h3>
      <p class="mb-4 caption">*Click a question to reveal the answer</p>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_9" aria-expanded="false" aria-controls="answer_9"><b>How much do you charge for a seminar/workshop?</b></a></p>
      <div id="answer_9" class="collapse">
        <p>We don’t charge the gym/venue a fixed amount, instead we simply charge a per-person fee and take the money from ticket sales.</p>
        <p>Depending on the amount of travel required, we will require a minimum number of people guaranteed to attend to cover our costs. This will vary for different locations, so we can discuss this when arranging the seminar/workshop.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_10" aria-expanded="false" aria-controls="answer_10"><b>Do you require a minimum number of people to attend?</b></a></p>
      <div id="answer_10" class="collapse">
        <p>This often depends on how far we’re travelling (i.e. we’ll need to cover bigger costs the further we need to travel!) but a rule of thumb is minimum of 10 people.</p>
        <p>You don’t need to guarantee 10 people before we book the seminar with you but bear in mind that it may be cancelled if we can’t get the minimum number of people.</p>
        <p>We also offer 2 spots for the host/host’s coaches to attend free of charge!</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_11" aria-expanded="false" aria-controls="answer_11"><b>Who will advertise / marketing the event?</b></a></p>
      <div id="answer_11" class="collapse">
        <p>We will advertise the seminar/workshop, create posters and do some paid marketing to gain interest and spread the word.</p>
        <p>Nevertheless, any advertising you can do in your local area, amongst your gym members or on your social media would be great!</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_12" aria-expanded="false" aria-controls="answer_12"><b>Do you take care of ticket sales?</b></a></p>
      <div id="answer_12" class="collapse">
        <p>Yes, we use Eventbrite for our tickets, so you don’t need to worry!</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_13" aria-expanded="false" aria-controls="answer_13"><b>Which countries do you do seminars/workshops in?</b></a></p>
      <div id="answer_13" class="collapse">
        <p>We are up for travelling anywhere in the world! So, if you would like to have us just at your gym don’t hesitate to message and we will check out the flights & hotels for your area and let you know when would suit best!</p>
        <p>The more notice the better, especially if you’re outside of Europe.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_14" aria-expanded="false" aria-controls="answer_14"><b>How much space do you need for a seminar/workshop?</b></a></p>
      <div id="answer_14" class="collapse">
        <p>This is dependant on how many people attend the seminar/workshop, but we usually have between 10-30 people, so enough for enough people to have their own spot for stretching & moving – or at the least enough room for people to work in pairs.</p>
      </div>
      <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_15" aria-expanded="false" aria-controls="answer_15"><b>Do you need specific equipment for a seminar/workshop?</b></a></p>
      <div id="answer_15" class="collapse">
        <p>A good amount of floor space is most important, and ideally:</p>
        <ul>
          <li><p class="mb-0">Resistance bands</p></li>
          <li><p class="mb-0">Pull Up Bars / a Rig</p></li>
          <li><p class="mb-0">Kettlebells</p></li>
          <li><p class="mb-0">Gymnastics Rings</p></li>
          <li><p class="mb-0">Dumbbells</p></li>
          <li><p class="mb-0">Fractional Plates</p></li>
        </ul>
        <p>But, if you don’t have any of these things it’s no problem, we can adapt the exercises to work around what you have.</p>
      </div>
    </div>
  </div>
 {{--  <div class="row text-center">
    <div class="col-12 py-5 mb-5">
      <h4 class="mb-4">No location that suits?<br>Request a seminar at your gym!</h4>
      <div class="btn btn-primary mx-auto" data-toggle="modal" data-target="#request_seminar_modal">Request A Seminar</div>
    </div>
  </div> --}}
</div>
@endsection
@section('scripts')
<script src='https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit'async defer></script>
<script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>
@endsection
@section('modals')
<request-seminar :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></request-seminar>
@endsection