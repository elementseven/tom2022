@extends('layouts.users', ['page' => "Dashboard", 'pagetitle' => "Dashboard - Support", 'padfooter' => true])

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-6">
            <p class="mb-0 text-small text-light-grey"><b>Support</b></p>
        </div>
        <div class="col-6 text-right">
            <p class="mb-0 text-small text-light-grey">Account Active<i class="fa fa-circle text-success ml-2" aria-hidden="true"></i></p>
        </div>
        <div class="col-12">
            <hr class="mt-1 mb-4" />
        </div>
        <div class="col-md-9">
            <h1 class="text-primary">Support</h1>
            <p class="mb-0">Just send us a message using the form below and we will get back to you as soon as possible!</p>
        </div>
        <div class="col-12">
            <support-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :user="{{$user}}"></support-form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection