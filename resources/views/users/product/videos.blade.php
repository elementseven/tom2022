@extends('layouts.compatible', ['page' => "product", 'pagetitle' => $product->name, 'padfooter' => true])
@section('styles')
<style>
.btn-small{
    width: 105px;
    padding: 0;
    height: 35px;
    font-size: 13px;
    display: inline-block;
}
.btn-small i{
    font-size:  13px;
}
@media only screen and (max-width : 767px){
    .btn-small{
        width: 130px;
    }
}
</style>
@endsection
@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <a href="/dashboard" class="dark">
                <p class="mb-2"><i class="fa fa-angle-left"></i> Back to dashboard</p>
            </a>
            <h1>{{$product->category->name}} //<br class="d-sm-none"> <span  class="text-primary">{{$product->name}}</span></h1>
        </div>
        <div class="col-12 mb-4">
            <hr class="dark-line"/>
        </div>
        <div class="col-lg-3">
            <div class="container product-menu">
                <div class="row">
                    <div class="col-12">
                        <p class="larger mb-1"><b><a href="{{route('dashboard-view-product', ['slug' => $product->slug])}}" class="dark">Introduction</a></b></p>
                    </div>
                    @if(count($product->faqs))
                    <div class="col-12">
                        <p class="larger mb-1"><b><a href="{{route('dashboard-product-faqs', ['slug' => $product->slug])}}" class="dark">FAQs</a></b></p>
                    </div>
                    @endif
                    @if(count($product->videos))
                    <div class="col-12">
                        <hr/>
                        <p class="larger mb-1"><b><a href="{{route('dashboard-product-videos', ['slug' => $product->slug])}}" class="dark active"><i class="fa fa-video-camera text-primary mr-2"></i>Videos</a></b></p>
                        <hr class="mb-0" />
                    </div>
                    @endif
                    @if(count($product->downloads))
                    <div class="col-12">
                        <p class="text-primary mt-3 text-small mb-0">Downloads</p>
                        @foreach($product->downloads as $key => $d)
                        @if($key != 0)
                        <div class="mt-3"></div> 
                        @endif
                        @if($d->show_time != null)
                            @if(Carbon\Carbon::now() >= Carbon\Carbon::parse($d->show_time))
                            <p class="mb-2 larger" style="line-height: 1.1;"><b>{{$d->name}}</b></p>
                            <a href="https://drive.google.com/viewerng/viewer?url=https://tommorrison.uk/storage/{{$d->path}}" target="_blank" class="dark">
                                <button type="button" class="btn btn-primary btn-small w-100 mb-2"><i class="fa fa-file-text-o"></i> Open</button>
                            </a> 
                            <a href="{{route('dashboard-product-downloads', ['slug' => $product->slug, 'download' => $d->path])}}" class="dark">
                                <button type="button" class="btn btn-outline btn-small w-100"><i class="fa fa-cloud-download"></i> Download</button>
                            </a>
                            @endif
                        @else
                            <p class="mb-1 larger"><b>{{$d->name}}</b></p>
                            <a href="https://drive.google.com/viewerng/viewer?url=https://tommorrison.uk/storage/{{$d->path}}" target="_blank" class="dark">
                                <button type="button" class="btn btn-primary btn-small w-100 mb-2"><i class="fa fa-file-text-o"></i> Open</button>
                            </a> 
                            <a href="{{route('dashboard-product-downloads', ['slug' => $product->slug, 'download' => $d->path])}}" class="dark">
                                <button type="button" class="btn btn-outline btn-small w-100"><i class="fa fa-cloud-download"></i> Download</button>
                            </a>
                        @endif
               
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-9 mob-mt-5">
            @php
            use App\Models\Video;
            @endphp
            @if($product->id == 1)
            <div class="row"> 
                <div class="col-12">
                    <p class="text-large"><b>Introduction</b></p>
                    <hr/>
                    @php $video = Video::where('id', 1)->first(); @endphp
                    <div class="row mb-3">
                    <div class="col-3 col-lg-2">
                        <div class="video-holder">
                            <a href="/dashboard/videos/{{$video->id}}#player">
                            <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                          </a>
                        </div>
                      </div>
                    <div class="col-9 col-lg-10 pl-0">
                        <p><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 pt-5  mob-my-0">
                  <p class="text-large"><b>Test Videos</b></p>
                  <hr/>
                  
                  @php $videos = Video::where('category', 'Test Videos')->get(); @endphp
                  @foreach($videos as $key => $video)
                  <div class="row mb-3">
                    <div class="col-3 col-lg-4">
                        <div class="video-holder">
                            <a href="/dashboard/videos/{{$video->id}}#player">
                            <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                          </a>
                        </div>
                      </div>
                    <div class="col-9 col-lg-8 pl-0">
                        <p><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                    </div>
                  </div>
                  @endforeach
                 </div>
                 <div class="col-lg-6">
                  <p class="text-large mt-5"><b>Exercises Videos</b></p>
                  <hr/>
                  @php $videos = Video::where('category', 'Exercises Videos')->get(); @endphp
                  @foreach($videos as $key => $video)
                  <div class="row mb-3">
                    <div class="col-3 col-lg-4">
                        <div class="video-holder">
                            <a href="/dashboard/videos/{{$video->id}}#player">
                            <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                          </a>
                        </div>
                      </div>
                    <div class="col-9 col-lg-8 pl-0">
                        <p><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                    </div>
                  </div>
                  @endforeach
                </div>
                <div class="col-12">
                    <p class="text-large mt-5"><b>Exercise Breakdowns</b></p>
                    <hr/>
                    <div class="row">
                        @php $videos = Video::where('category', 'Exercise Breakdowns')->get(); @endphp
                        @foreach($videos as $key => $video)
                        <div class="col-lg-6">
                            <div class="row mb-3">
                              <div class="col-3 col-lg-4">
                                <div class="video-holder">
                                    <a href="/dashboard/videos/{{$video->id}}#player">
                                        <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                                      </a>
                                    </div>
                                  </div>
                              <div class="col-9 col-lg-8">
                                  <p><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      </div>
                  </div>
                  <div class="col-lg-6">

                    <p class="text-large mt-5"><b>Head To Toe Mobility</b></p>
                    <hr/>
                    @php $videos = Video::where('category', 'Head To Toe')->get(); @endphp
                    @foreach($videos as $key => $video)
                    <div class="row mb-3">
                      <div class="col-3 col-lg-4">
                        <div class="video-holder">
                            <a href="/dashboard/videos/{{$video->id}}#player">
                                <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                              </a>
                            </div>
                          </div>
                      <div class="col-9 col-lg-8">
                          <p class="mb-0"><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                      </div>
                    </div>
                    @endforeach

                  </div>
                  <div class="col-lg-6">

                    <p class="text-large mt-5"><b>Bonus Videos</b></p>
                    <hr/>
                    @php $videos = Video::where([['category', 'Bonus Videos'],['product_id', $product->id]])->get(); @endphp
                    @foreach($videos as $key => $video)
                    <div class="row mb-3">
                      <div class="col-3 col-lg-4">
                        <div class="video-holder">
                            <a href="/dashboard/videos/{{$video->id}}#player">
                                <img loading="lazy" src="/img/thumbs/{{$video->youtube}}.jpg" alt="{{$video->title}}" class="w-100">
                              </a>
                            </div>
                          </div>
                      <div class="col-9 col-lg-8">
                          <p><a href="/dashboard/videos/{{$video->id}}#player" class="text-dark"><i class="fa fa-play-circle text-primary mr-1"></i> {{$video->name}} </a></p>
                      </div>
                    </div>
                    @endforeach
                </div>
            </div>
            </div>
            @else
           <div class="row"> 
            @foreach($product->videos as $key => $video)
                @if($video->show_time != null)
                    @if(Carbon\Carbon::now() >= Carbon\Carbon::parse($video->show_time))
                        <div class="col-lg-6 mb-2">
                            <div class="embed-responsive embed-responsive-16by9 mb-3 video-wrapper" data-video-id="{{ $video->youtube }}">
                                @if($video->thumbnail)
                                    <img class="embed-responsive-item lazy-thumbnail" src="{{ $video->thumbnail }}" alt="{{ $video->name }}">
                                @else
                                    <div class="embed-responsive-item lazy-thumbnail-fallback"></div>
                                @endif
                                <div class="play-button"></div>
                            </div>
                            
                        </div>
                        <div class="col-lg-6 mb-4">
                            <h4>{{ $video->name }}</h4>
                            <p>{{ $video->description }}</p>
                        </div>
                    @endif
                @else
                    <div class="col-lg-6 mb-2">
                        <div class="embed-responsive embed-responsive-16by9 mb-3 video-wrapper" data-video-id="{{ $video->youtube }}">
                            @if($video->thumbnail)
                                <img class="embed-responsive-item lazy-thumbnail" src="{{ $video->thumbnail }}" alt="{{ $video->name }}">
                            @else
                                <div class="embed-responsive-item lazy-thumbnail-fallback"></div>
                            @endif
                            <div class="play-button"></div>
                        </div>
                
                    </div>
                    <div class="col-lg-6 mb-4">
                            <h4>{{ $video->name }}</h4>
                            <p>{{ $video->description }}</p>
                        </div>
                @endif
            @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
@if($product->id == 1)
<style>
.video-holder{
    position:relative;
}
.video-holder .play-btn{
    width:80px;
    height:50px;
    position:absolute;
    top:calc(50% - 25px);
    left:calc(50% - 40px);
}
</style>
@endif
<style>
    .video-wrapper {
        position: relative;
        cursor: pointer;
    }

    .lazy-thumbnail {
        width: 100%;
        height: auto;
    }

    .lazy-thumbnail-fallback {
        width: 100%;
        height: 0;
        padding-bottom: 56.25%; /* 16:9 aspect ratio */
        background-color: #333;
        position: relative;
    }

    .play-button {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 80px;
        height: 80px;
        background: url('/img/icons/play-red.svg') no-repeat center center;
        background-size: contain;
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const videoWrappers = document.querySelectorAll('.video-wrapper');

        videoWrappers.forEach(wrapper => {
            wrapper.addEventListener('click', function() {
                const videoId = this.getAttribute('data-video-id');
                const iframe = document.createElement('iframe');
                iframe.setAttribute('src', `https://player.vimeo.com/video/${videoId}?autoplay=1&rel=0`);
                iframe.setAttribute('allow', 'autoplay; fullscreen');
                iframe.setAttribute('frameborder', '0');
                iframe.setAttribute('allowfullscreen', 'true');
                iframe.classList.add('embed-responsive-item');

                // Clear the thumbnail
                this.innerHTML = '';
                // Append the iframe
                this.appendChild(iframe);
            });
        });
    });

</script>


@endsection