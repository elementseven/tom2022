@extends('layouts.compatible', ['page' => "product", 'pagetitle' => $product->name, 'padfooter' => true])
@section('styles')
<style>
.btn-small{
    width: 105px;
    padding: 0;
    height: 35px;
    font-size: 13px;
    display: inline-block;
}
.btn-small i{
    font-size:  13px;
}
@media only screen and (max-width : 767px){
    .btn-small{
        width: 130px;
    }
}
</style>
@endsection
@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <a href="/dashboard" class="dark">
                <p class="mb-2"><i class="fa fa-angle-left"></i> Back to dashboard</p>
            </a>
            <h1>{{$product->category->name}} //<br class="d-sm-none"> <span  class="text-primary">{{$product->name}}</span></h1>
        </div>
        <div class="col-12 mb-4">
            <hr class="dark-line"/>
        </div>
        <div class="col-lg-3">
            <div class="container product-menu">
                <div class="row">
                    <div class="col-12">
                        <p class="larger mb-1"><b><a href="{{route('dashboard-view-product', ['slug' => $product->slug])}}" class="dark">Introduction</a></b></p>
                    </div>
                    @if(count($product->faqs))
                    <div class="col-12">
                        <p class="larger mb-1"><b><a href="{{route('dashboard-product-faqs', ['slug' => $product->slug])}}" class="dark active">FAQs</a></b></p>
                    </div>
                    @endif
                    @if(count($product->videos))
                    <div class="col-12">
                        <hr/>
                        <p class="larger mb-1"><b><a href="{{route('dashboard-product-videos', ['slug' => $product->slug])}}" class="dark"><i class="fa fa-video-camera text-primary mr-2"></i>Videos</a></b></p>
                        <hr class="mb-0" />
                    </div>
                    @endif
                    @if(count($product->downloads))
                    <div class="col-12">
                        <p class="text-primary mt-3 text-small mb-0">Downloads</p>
                        @foreach($product->downloads as $key => $d)
                        @if($key != 0)
                        <div class="mt-3"></div> 
                        @endif
                        @if($d->show_time != null)
                            @if(Carbon\Carbon::now() >= Carbon\Carbon::parse($d->show_time))
                            <p class="mb-2 larger" style="line-height: 1.1;"><b>{{$d->name}}</b></p>
                            <a href="https://drive.google.com/viewerng/viewer?url=https://tommorrison.uk/storage/{{$d->path}}" target="_blank" class="dark">
                                <button type="button" class="btn btn-primary btn-small w-100 mb-2"><i class="fa fa-file-text-o"></i> Open</button>
                            </a> 
                            <a href="{{route('dashboard-product-downloads', ['slug' => $product->slug, 'download' => $d->path])}}" class="dark">
                                <button type="button" class="btn btn-outline btn-small w-100"><i class="fa fa-cloud-download"></i> Download</button>
                            </a>
                            @endif
                        @else
                            <p class="mb-1 larger"><b>{{$d->name}}</b></p>
                            <a href="https://drive.google.com/viewerng/viewer?url=https://tommorrison.uk/storage/{{$d->path}}" target="_blank" class="dark">
                                <button type="button" class="btn btn-primary btn-small w-100 mb-2"><i class="fa fa-file-text-o"></i> Open</button>
                            </a> 
                            <a href="{{route('dashboard-product-downloads', ['slug' => $product->slug, 'download' => $d->path])}}" class="dark">
                                <button type="button" class="btn btn-outline btn-small w-100"><i class="fa fa-cloud-download"></i> Download</button>
                            </a>
                        @endif
               
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-9 mob-mt-5">
	        <p class="mb-3"><i>Click the question to see the answer</i></p>
            @foreach($product->faqs as $faq)
            <p><a class="dark" data-toggle="collapse" href="#answer_{{$faq->id}}" aria-expanded="false" aria-controls="answer_{{$faq->id}}"><b>{{$faq->question}}</b></a></p>
            <div id="answer_{{$faq->id}}" class="collapse">
                <p class="mb-0">{!!$faq->answer!!}</p>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection