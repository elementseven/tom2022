@extends('layouts.compatible', ['page' => "Dashboard - Orders", 'pagetitle' => "Dashboard - Orders", 'padfooter' => true])

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-6">
            <p class="mb-0 text-small text-light-grey"><b>Your Dashboard</b></p>
        </div>
        <div class="col-6 text-right">
            <p class="mb-0 text-small text-light-grey">Merchandise Orders<i class="fa fa-circle text-success ml-2" aria-hidden="true"></i></p>
        </div>
        <div class="col-12">
            <hr class="mt-1 mb-4" />
        </div>
        <div class="col-md-12 mb-5">
            <h1 class="text-primary">Your Merchandise Orders</h1>
            <p class="mb-0">Check the status of your merchandise orders below. </p>
            <ul class="pl-3" style="font-size: 12px;">
                <li><b>Failed</b> - order was submitted for fulfillment but was not accepted because of an error (problem with address, printfiles, charging, etc.)</li>
                <li><b>Pending</b> - order has been submitted for fulfillment</li>
                <li><b>Canceled</b> - order is canceled</li>
                <li><b>Onhold</b> - order has encountered a problem during the fulfillment that needs to be resolved together with the Printful customer service</li>
                <li><b>Inprocess</b> - order is being fulfilled and is no longer cancellable</li>
                <li><b>Partial</b> - order is partially fulfilled (some items are shipped already, the rest will follow)</li>
                <li><b>Fulfilled</b> - all items are shipped</li>
            </ul>
        </div>
        @foreach($currentUser->orders as $order)
        <div class="col-lg-4 col-md-6 my-3">
            <div class="card px-3">
                <div class="row">
                    <div class="col-12 py-3">
                        <img src="{{$order->thumbnail_url}}" class="w-100" alt="{{$order->variant->merch->name}} image"/>
                        <hr class="mb-0" />
                    </div>
                    <div class="col-12 py-3">
                        <div class="row">
                            <div class="col-12">
                                <p class="larger"><b>{{$order->variant->merch->name}}</b></p>
                            </div>
                            <div class="col-5">
                                <p class="text-capitalize mb-0" style="font-size: 0.9rem;"><b>Size:</b> {{$order->variant->size}}</p>
                                <p class="text-capitalize mb-0" style="font-size: 0.9rem;"><b>Quantity:</b> {{$order->quantity}}</p>
                            </div>
                            <div class="col-7">
                                <p class="text-capitalize mb-0" style="font-size: 0.9rem;"><b>Ordered:</b> {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->format('d/m/Y')}}</p>
                                <p class="text-capitalize mb-0" style="font-size: 0.9rem;"><b>Status: <span class="text-primary">{{$order->status}}</span></b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-lg-4 col-md-6 my-3">
            <div class="card px-3">
                <div class="row">
                    <div class="col-12 py-3">
                        <img src="/img/shop/bag.jpg" class="w-100" alt="Merchandise store image"/>
                        <hr class="mb-0" />
                    </div>
                    <div class="col-lg-12 py-3">
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="larger"><b>Shop for more</b></p>
                                <a href="{{route('merch')}}">
                                    <div class="btn btn-primary d-inline-block mx-auto mb-4">Shop Now</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row mt-5 mob-mt-0">
        <div class="col-lg-6">
            <div class="bg-primary p-5 text-left mob-mt-5 mob-p-3">
                <h3 class="text-white mb-3">Got a second?</h3>
                <p class="text-white">If you love the stuff we create, it'd really help us out if you could take a moment to leave us a quick review on Google!</p>
                <p class="text-white">We want to keep making awesome products for you, and for that we need your support to help spread the word.</p>
                <p class="text-white">Thanks in advance :)<br>Tom & Jenni</p>
                <a href="https://www.google.co.uk/search?q=Tom+Morrison+Ltd&ludocid=581983343819244963#lrd=0x4861054a7fdb59f9:0x8139ec4898a45a3,3" target="_blank">
                    <div class="btn btn-white mb-2">Leave Review &nbsp;<i class="fa fa-angle-right"></i></div>
                </a>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="bg-white border-2 p-5 text-left mob-mt-3 mob-p-3">
                <h3 class="mb-3">Done something awesome?</h3>
                <p>We really love to see what you guys are up to, the progress you make and the things you achieve!</p>
                <p>If you pass your deep lunge test for the first time, have some great Before/After comparisons, get a new PB, or just do something cool - we love to hear about it!!</p>
                <p>Tag us on <a href="https://www.instagram.com/max_emom_tom_morrison/" target="_blank">Instagram</a>, on <a href="https://www.facebook.com/Movementandmotion" target="_blank">Facebook</a> or drop us <a href="mailto:hello@tommorrison.uk">an email</a></p>
                <p class="mb-0">What's the point in being awesome if you can't show off a little bit? ;)</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var wantsomethingextra = document.getElementById('wantsomethingextra');
    var wantcollapse = document.getElementById('wantcollapse');
    wantsomethingextra.addEventListener('hide.bs.collapse', function () {
        wantcollapse.innerHTML = 'Want something extra? <i class="fa fa-angle-down float-right"></i>';
    });
    wantsomethingextra.addEventListener('show.bs.collapse', function () {
        wantcollapse.innerHTML = 'Want something extra? <i class="fa fa-angle-up float-right"></i>';
    });
</script>
@endsection