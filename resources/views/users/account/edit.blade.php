@extends('layouts.compatible', ['page' => "Dashboard", 'pagetitle' => "Dashboard - Account Settings", 'padfooter' => true])

@section('content')
<div class="container">
    <div class="row">
    <div class="col-6">
            <p class="mb-0 text-small text-light-grey"><b>Account Settings</b></p>
        </div>
        <div class="col-6 text-right">
            <p class="mb-0 text-small text-light-grey">Account Active<i class="fa fa-circle text-success ml-2" aria-hidden="true"></i></p>
        </div>
        <div class="col-12">
            <hr class="mt-1 mb-4" />
        </div>
    </div> 
    @if($currentUser->products()->where('id', 1)->exists())
    <form action="{{route('toggle-follow-up')}}" method="post" class="row mb-5">
                {{ csrf_field() }}  
        <div class="col-12 pt-5"><h3 class="">SMM Follow Up Emails</h3><hr/></div>
        <div class="col-lg-9 pt-3">
            @if($currentUser->follow_up)
            <p><b>You are currently set to receive SMM follow up emails, you can opt out at any time.</b></p>
            @else
            <p><b>You are currently not set to receive SMM follow up emails, you can opt in any time.</b></p>
            @endif
        </div>
        <div class="col-lg-3">
            @if($currentUser->follow_up)
            <button type="submit" class="btn btn-primary">Don't Send me emails</button>
            @else
            <button type="submit" class="btn btn-primary">Send me emails</button>
            @endif
        </div>
    </form>
    @endif
    <div class="row py-5">
        <div class="col-md-12">
            <h3 class="">Your Details</h3>
            <hr class="primary_line">
        </div>
        <div class="col-md-8">
            <p class=" mb-3">Only the information you provided at the point of purchase is stored. Your details are kept in a secure database so you may log in and use this system again in the future. If you wish to have your details removed from our system please contact me.</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Name: </b>{{$currentUser->first_name}} {{$currentUser->last_name}}</p>
            <p class="marg_top_20"><b>Email: </b>{{$currentUser->email}}</p>
        </div>
        <div class="col-sm-6">
            <p class="marg_top_20"><b>Registered since: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->created_at))->format('d/m/Y') }}</p>
            <p class="marg_top_20"><b>Details last updated: </b>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($currentUser->updated_at))->format('d/m/Y') }}</p>
        </div>
        <div class="col-12 mt-5">
            <hr/>
            <p class="large"><b>Want to update your address details?</b></p>
            <a href="/dashboard/address">
                <button type="button" class="btn btn-primary">Update Address</button>
            </a>
        </div>
    </div>
    
    <div class="row py-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Additional Information</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Let us know some more information about you and why you chose our products. Information provided here will help improve what we can offer you in the future.</p>
                </div>
            </div>
            @if(!$currentUser->info)
            <form action="{{route('add-info')}}" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-6 half_col mb-4">
                        <label for="gender"><b>Gender</b></label>
                        <select name="gender" class="form-control">
                            <option selected disabled>Please Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4">
                        <label for="dob"><b>Date of birth</b></label>
                        <input name="dob" type="date" class="form-control" placeholder="Date of birth"/>
                        @if ($errors->has('dob'))
                            <span class="help-block">
                                <strong>{{ $errors->first('dob') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        <label for="job"><b>Type of work:</b></label>
                        <select name="job" class="form-control">
                            <option selected disabled>Please Select</option>
                            <option value="Coach/Personal Trainer">Coach/Personal Trainer</option>
                            <option value="Emergency Services">Emergency Services</option>
                            <option value="Military">Military</option>
                            <option value="Health care">Health care</option>
                            <option value="Professional/Financial Services">Professional/Financial Services</option>
                            <option value="Design/Marketing">Design/Marketing</option>
                            <option value="Construction/Manufacturing">Construction/Manufacturing</option>
                            <option value="Education">Education</option>
                            <option value="Other">Other</option>
                        </select>
                        <input id="jobother" type="text" class="form-control mt-3 d-none" name="jobother" placeholder="Tell us what type of work you do" /> 
                        @if ($errors->has('job'))
                            <span class="help-block">
                                <strong>{{ $errors->first('job') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        <label for="sport"><b>What's your main Sport/Training?</b></label>
                        <select name="sport" class="form-control">
                            <option selected disabled>Please Select</option>
                            <option value="Crossfit">Crossfit</option>
                            <option value="Weightlifting">Weightlifting</option>
                            <option value="Powerlifting">Powerlifting</option>
                            <option value="Gymnastics">Gymnastics</option>
                            <option value="Calisthenics">Calisthenics</option>
                            <option value="Bodybuilding">Bodybuilding</option>
                            <option value="Kettlebell Sport">Kettlebell Sport</option>
                            <option value="Running/Hiking">Running/Hiking</option>
                            <option value="Cycling">Cycling</option>
                            <option value="Racket Sport">Racket Sport</option>
                            <option value="Team Sport">Team Sport</option>
                            <option value="Other">Other</option>
                        </select>
                        <input id="sportother" type="text" class="form-control mt-3 d-none" name="sportother" placeholder="Tell us what sport you do" /> 
                        @if ($errors->has('sport'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sport') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        <label for="hear"><b>How did you hear about us?</b></label>
                        <select name="hear" class="form-control">
                            <option selected disabled>Please Select</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Youtube">Youtube</option>
                            <option value="Online magazine">Online magazine (T-Nation/Boxrox/Breaking Muscle etc.)</option>
                            <option value="Google search">Google search</option>
                            <option value="From a friend">From a friend</option>
                            <option value="Other">Other</option>
                        </select>
                        <input id="hearother" type="text" class="form-control mt-3 d-none" name="hearother" placeholder="Tell us how you heard about us" /> 
                        @if ($errors->has('hear'))
                            <span class="help-block">
                                <strong>{{ $errors->first('hear') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4">
                        <label for="injuries"><b>Any major past or current injuries?</b></label>
                        <textarea id="injuries" rows="5" name="injuries" type="text" class="form-control" placeholder="Major Injuries" value=" @if($currentUser->info) {{$currentUser->info->injuries}} @endif "></textarea>
                        @if ($errors->has('injuries'))
                            <span class="help-block">
                                <strong>{{ $errors->first('injuries') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-4">
                        <label><b>Why do you want SMM?</b></label>
                        <div>
                            <label for="mobility"><input id="mobility" type="checkbox" name="why[]" value="Improve mobility"/> Improve mobility &nbsp; &nbsp; </label>
                            <label for="stability"><input id="stability" type="checkbox" name="why[]" value="Improve stability"/> Improve stability &nbsp; &nbsp; </label>
                            <label for="corestrength"><input id="corestrength" type="checkbox" name="why[]" value="Improve core strength"/> Improve core strength &nbsp; &nbsp; </label>
                            <label for="rehab"><input id="rehab" type="checkbox" name="why[]" value="Rehab from recent injury"/> Rehab from recent injury &nbsp; &nbsp; </label>
                            <label for="future"><input id="future" type="checkbox" name="why[]" value="Prevent future injuries"/> Prevent future injuries &nbsp; &nbsp; </label>
                            <label for="enhance"><input id="enhance" type="checkbox" name="why[]" value="Enhance sporting/training performance"/> Enhance sporting/training performance &nbsp;  &nbsp; </label>
                            <label for="other"><input id="other" type="checkbox" name="why[]" value="other"/> Other</label>
                        </div>
                        @if ($errors->has('why'))
                            <span class="help-block">
                                <strong>{{ $errors->first('why') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-3">
                        <button type="submit" class="btn btn-primary float-right mt-0 px-5">Confirm</button>
                    </div>
                </div>
            </form>
            @else
            <form action="{{route('add-info')}}" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-6 half_col mb-4">
                        <label for="gender"><b>Gender</b></label>
                        <select name="gender" class="form-control">
                            <option @if($currentUser->info->gender == NULL) selected @endif disabled>Please Select</option>
                            <option @if($currentUser->info->gender == "Male") selected @endif value="Male">Male</option>
                            <option @if($currentUser->info->gender == "Female") selected @endif value="Female">Female</option>
                            <option @if($currentUser->info->gender == "Other") selected @endif value="Other">Other</option>
                        </select>
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4">
                        <label for="dob"><b>Date of birth</b></label>
                        <input name="dob" type="date" class="form-control" placeholder="Date of birth" value="@if($currentUser->info->dob){{Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$currentUser->info->dob)->format('Y-m-d')}}@endif"/>
                        @if ($errors->has('dob'))
                            <span class="help-block">
                                <strong>{{ $errors->first('dob') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        @php 
                        $jobs = array("Coach/Personal Trainer", "Emergency Services", "Military", "Health care","Professional/Financial Services","Design/Marketing","Construction/Manufacturing","Education");
                        @endphp
                        <label for="job"><b>Type of work:</b></label>
                        <select id="type_of_work" name="job" class="form-control">
                            <option @if($currentUser->info->job == NULL) selected @endif disabled>Please Select</option>
                            <option @if($currentUser->info->job == "Coach/Personal Trainer" && in_array($currentUser->info->job, $jobs)) selected @endif value="Coach/Personal Trainer">Coach/Personal Trainer</option>
                            <option @if($currentUser->info->job == "Emergency Services" && in_array($currentUser->info->job, $jobs)) selected @endif value="Emergency Services">Emergency Services</option>
                            <option @if($currentUser->info->job == "Military" && in_array($currentUser->info->job, $jobs)) selected @endif value="Military">Military</option>
                            <option @if($currentUser->info->job == "Health care" && in_array($currentUser->info->job, $jobs)) selected @endif value="Health care">Health care</option>
                            <option @if($currentUser->info->job == "Professional/Financial Services" && in_array($currentUser->info->job, $jobs)) selected @endif value="Professional/Financial Services">Professional/Financial Services</option>
                            <option @if($currentUser->info->job == "Design/Marketing" && in_array($currentUser->info->job, $jobs)) selected @endif value="Design/Marketing">Design/Marketing</option>
                            <option @if($currentUser->info->job == "Construction/Manufacturing" && in_array($currentUser->info->job, $jobs)) selected @endif value="Construction/Manufacturing">Construction/Manufacturing</option>
                            <option @if($currentUser->info->job == "Education" && in_array($currentUser->info->job, $jobs)) selected @endif value="Education">Education</option>
                            <option @if($currentUser->info->job != NULL && !in_array($currentUser->info->job, $jobs)) selected @endif value="Other">Other</option>
                        </select>
                        <input id="jobother" type="text" class="form-control mt-3 @if($currentUser->info->job != NULL && in_array($currentUser->info->job, $jobs)) d-none @endif" name="jobother" placeholder="Tell us what type of work you do" @if($currentUser->info->job != NULL && !in_array($currentUser->info->job, $jobs)) value="{{$currentUser->info->job}}" @endif /> 
                        @if ($errors->has('job'))
                            <span class="help-block">
                                <strong>{{ $errors->first('job') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        @php 
                        $sports = array("Crossfit", "Weightlifting", "Powerlifting", "Gymnastics","Calisthenics","Bodybuilding","Kettlebell Sport","Running/Hiking","Cycling","Racket Sport","Team Sport");
                        @endphp
                        <label for="sport"><b>What's your main Sport/Training?</b></label>
                        <select name="sport" class="form-control">
                            <option @if($currentUser->info->sport == NULL) selected @endif disabled>Please Select</option>
                            <option @if($currentUser->info->sport == "Crossfit" && in_array($currentUser->info->sport, $sports)) selected @endif value="Crossfit">Crossfit</option>
                            <option @if($currentUser->info->sport == "Weightlifting" && in_array($currentUser->info->sport, $sports)) selected @endif value="Weightlifting">Weightlifting</option>
                            <option @if($currentUser->info->sport == "Powerlifting" && in_array($currentUser->info->sport, $sports)) selected @endif value="Powerlifting">Powerlifting</option>
                            <option @if($currentUser->info->sport == "Gymnastics" && in_array($currentUser->info->sport, $sports)) selected @endif value="Gymnastics">Gymnastics</option>
                            <option @if($currentUser->info->sport == "Calisthenics" && in_array($currentUser->info->sport, $sports)) selected @endif value="Calisthenics">Calisthenics</option>
                            <option @if($currentUser->info->sport == "Bodybuilding" && in_array($currentUser->info->sport, $sports)) selected @endif value="Bodybuilding">Bodybuilding</option>
                            <option @if($currentUser->info->sport == "Kettlebell Sport" && in_array($currentUser->info->sport, $sports)) selected @endif value="Kettlebell Sport">Kettlebell Sport</option>
                            <option @if($currentUser->info->sport == "Running/Hiking" && in_array($currentUser->info->sport, $sports)) selected @endif value="Running/Hiking">Running/Hiking</option>
                            <option @if($currentUser->info->sport == "Cycling" && in_array($currentUser->info->sport, $sports)) selected @endif value="Cycling">Cycling</option>
                            <option @if($currentUser->info->sport == "Racket Sport" && in_array($currentUser->info->sport, $sports)) selected @endif value="Racket Sport">Racket Sport (such as tennis, badminton, squash etc.)</option>
                            <option @if($currentUser->info->sport == "Team Sport" && in_array($currentUser->info->sport, $sports)) selected @endif value="Team Sport">Team Sport (such as rugby, football, hockey, volleyball etc.)</option>
                            <option @if($currentUser->info->sport != NULL && !in_array($currentUser->info->sport, $sports)) selected @endif value="Other">Other</option>
                        </select>
                        <input id="sportother" type="text" class="form-control mt-3 @if($currentUser->info->sport != NULL && in_array($currentUser->info->sport, $sports)) d-none @endif" name="sportother" placeholder="Tell us what sport you do" @if($currentUser->info->sport != NULL && !in_array($currentUser->info->sport, $sports)) value="{{$currentUser->info->sport}}" @endif /> 
                        @if ($errors->has('sport'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sport') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4 possible-other">
                        @php 
                        $hears = array("Facebook", "Instagram", "Youtube", "Online magazine","Google search","From a friend");
                        @endphp
                        <label for="hear"><b>How did you hear about us?</b></label>
                        <select name="hear" class="form-control">
                            <option @if($currentUser->info->hear == NULL) selected @endif disabled>Please Select</option>
                            <option @if($currentUser->info->hear == "Facebook" && in_array($currentUser->info->hear, $hears)) selected @endif value="Facebook">Facebook</option>
                            <option @if($currentUser->info->hear == "Instagram" && in_array($currentUser->info->hear, $hears)) selected @endif value="Instagram">Instagram</option>
                            <option @if($currentUser->info->hear == "Youtube" && in_array($currentUser->info->hear, $hears)) selected @endif value="Youtube">Youtube</option>
                            <option @if($currentUser->info->hear == "Online magazine" && in_array($currentUser->info->hear, $hears)) selected @endif value="Online magazine">Online magazine (T-Nation/Boxrox/Breaking Muscle etc.)</option>
                            <option @if($currentUser->info->hear == "Google search" && in_array($currentUser->info->hear, $hears)) selected @endif value="Google search">Google search</option>
                            <option @if($currentUser->info->hear == "From a friend" && in_array($currentUser->info->hear, $hears)) selected @endif value="From a friend">From a friend</option>
                            <option @if($currentUser->info->hear != NULL && !in_array($currentUser->info->hear, $hears)) selected @endif value="Other">Other</option>
                        </select>
                        <input id="hearother" type="text" class="form-control mt-3 @if($currentUser->info->hear != NULL && in_array($currentUser->info->hear, $hears)) d-none @endif" name="hearother" placeholder="Tell us how you heard about us" @if($currentUser->info->hear != NULL && !in_array($currentUser->info->hear, $hears)) value="{{$currentUser->info->hear}}" @endif /> 
                        @if ($errors->has('hear'))
                            <span class="help-block">
                                <strong>{{ $errors->first('hear') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-4">
                        <label for="injuries"><b>Any major past or current injuries?</b></label>
                        <textarea id="injuries" rows="5" name="injuries" type="text" class="form-control" placeholder="Major Injuries">{{$currentUser->info->injuries}}</textarea>
                        @if ($errors->has('injuries'))
                            <span class="help-block">
                                <strong>{{ $errors->first('injuries') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-4">
                        <label><b>Why do you want SMM?</b></label>
                        <div>
                            <label for="mobility"><input @if($currentUser->info->why && in_array("Improve mobility", json_decode($currentUser->info->why))) checked @endif id="mobility" type="checkbox" name="why[]" value="Improve mobility"/> Improve mobility &nbsp; &nbsp; </label>
                            <label for="stability"><input @if($currentUser->info->why && in_array("Improve stability", json_decode($currentUser->info->why))) checked @endif id="stability" type="checkbox" name="why[]" value="Improve stability"/> Improve stability &nbsp; &nbsp; </label>
                            <label for="corestrength"><input @if($currentUser->info->why && in_array("Improve core strength", json_decode($currentUser->info->why))) checked @endif id="corestrength" type="checkbox" name="why[]" value="Improve core strength"/> Improve core strength &nbsp; &nbsp; </label>
                            <label for="rehab"><input @if($currentUser->info->why && in_array("Rehab from recent injury", json_decode($currentUser->info->why))) checked @endif id="rehab" type="checkbox" name="why[]" value="Rehab from recent injury"/> Rehab from recent injury &nbsp; &nbsp; </label>
                            <label for="future"><input @if($currentUser->info->why && in_array("Prevent future injuries", json_decode($currentUser->info->why))) checked @endif id="future" type="checkbox" name="why[]" value="Prevent future injuries"/> Prevent future injuries &nbsp; &nbsp; </label>
                            <label for="enhance"><input @if($currentUser->info->why && in_array("Enhance sporting/training performance", json_decode($currentUser->info->why))) checked @endif id="enhance" type="checkbox" name="why[]" value="Enhance sporting/training performance"/> Enhance sporting/training performance &nbsp;  &nbsp; </label>
                            <label for="other"><input @if($currentUser->info->why && in_array("other", json_decode($currentUser->info->why))) checked @endif id="other" type="checkbox" name="why[]" value="other"/> Other</label>
                        </div>
                        @if ($errors->has('why'))
                            <span class="help-block">
                                <strong>{{ $errors->first('why') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-12 half_col mb-3">
                        <button type="submit" class="btn btn-primary float-right mt-0 px-5">Confirm</button>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Details</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your name or email address using the form below. Note that after saving the changes, these will be the details I use to contact you.</p>
                </div>
            </div>
            <form action="{{route('update-account')}}" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-6 half_col mb-3">
	                    <label class="mb-0" for="first_name"><b>First Name*</b></label>
                        <input name="first_name" type="text" class="form-control" placeholder="First Name" value="{{$currentUser->first_name}}"/>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-3">
	                    <label class="mb-0" for="last_name"><b>Last Name*</b></label>
                        <input name="last_name" type="text" class="form-control" placeholder="Last Name" value="{{$currentUser->last_name}}"/>
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-3">
	                    <label class="mb-0" for="email"><b>Email address*</b></label>
                        <input name="email" type="text" class="form-control" placeholder="New Email" value="{{$currentUser->email}}"/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6 half_col mb-3">
						<label class="mb-0" for="country"><b>Country*</b></label>
						<countries @if($currentUser->country) :setcountry="'{{$currentUser->country}}'" @else  :setcountry="'GB'" @endif :inid="'country'"></countries>
						@if ($errors->has('country'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('country') }}</strong>
						</span>
						@endif
					</div>
                    <div class="col-12 half_col mb-3">
						<button type="submit" class="btn btn-primary float-right mt-0 px-5">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Change Password</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">Change your password using the form below. Note that after saving your new password, you will need to use it to log in afterwards.</p>
                </div>
            </div>
            <form action="/change-password" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
	                    <label class="mb-0" for="current_password"><b>Current Password*</b></label>
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
	                    <label class="mb-0" for="password"><b>New Password*</b></label>
                        <input id="password" type="password" class="form-control" name="password" placeholder="New Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4 half_col mb-3">
	                    <label class="mb-0" for="password-confirm"><b>Confirm New Password*</b></label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm New Password" required>
                    </div>
                    <div class="col-md-12 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="">Remove Account</h3>
                    <hr class="primary_line">
                </div>
                <div class="col-md-8">
                    <p class="mb-3">You can remove your account and all of your information from this web app. This action is irreversible and you will lose access to all the content that you have paid for. You can pay for a new account afterwards at any time.</p>
                </div>
            </div>
            <form action="/remove-account" method="post">
                {{ csrf_field() }}  
                <div class="row half_row">
                    <div class="col-md-4 half_col mb-3">
                        <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
                        @if ($errors->has('current_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-8 half_col">
                        <button type="submit" class="btn btn-primary float-right mt-0">Remove Account</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.possible-other').each(function(){
        var select = $(this).find('select');
        var input = $(this).find('input');
        $(select).on('change',function(){
            if($(select).val() == 'Other'){
                $(input).removeClass('d-none');
                $(input).focus();
            }
            else{
                $(input).val('');
                $(input).addClass('d-none');
            }
        })
    });
    $(document).ready(function(){
	    $('#country option[value={{$currentUser->country}}]').attr('selected','selected');
    });	
</script>
@endsection