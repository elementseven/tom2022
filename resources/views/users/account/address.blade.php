@extends('layouts.compatible', ['page' => "Dashboard", 'pagetitle' => "Dashboard - Address", 'padfooter' => true])
@php 
    $hasAddress = false;
    $sameAddress = false;
    $billingAddress = null;
    $shippingAddress = null;
    if(Auth::check()){
        if(count($currentUser->addresses)){
            $hasAddress = true;
            foreach($currentUser->addresses as $a){
                if($a->type == 'billing'){
                    $billingAddress = $a;
                }else if($a->type == 'shipping'){
                    $shippingAddress = $a;
                }
            }
            if($billingAddress->streetAddress == $shippingAddress->streetAddress){
                $sameAddress = true;
            }
        }
    }
    $hasMerch = false;
    foreach(Cart::content() as $cc){
        if($cc->options->type == 'merch'){
            $hasMerch = true;
        }
    }
@endphp
@section('content')
<div class="container mb-5">
    <div class="row">
        <div class="col-6">
            <p class="mb-0 text-small text-light-grey"><b>Your Address</b></p>
        </div>
        <div class="col-6 text-right">
            <p class="mb-0 text-small text-light-grey">Account Active<i class="fa fa-circle text-success ml-2" aria-hidden="true"></i></p>
        </div>
        <div class="col-12">
            <hr class="mt-1 mb-4" />
        </div>
    </div> 
    @if(session('address-created'))
    <div class="alert alert-success border-0 alert-dismissible fade show mt-4" role="alert">
      <strong>Address Added Successfully!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <p>Looking for the checkout? <a href="/checkout"><u>Click here</u></a></p>
    @php session()->forget('address-created'); @endphp
    @endif
    @if(session('address-updated'))
    <div class="alert alert-success border-0 alert-dismissible fade show mt-4" role="alert">
      <strong>Address Updated Successfully!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <p>Looking for the checkout? <a href="/checkout"><u>Click here</u></a></p>
    @php session()->forget('address-updated'); @endphp
    @endif



    @if($billingAddress && $shippingAddress)
    <form id="edit-address-form" class="row half_row" action="/edit-address" method="POST">
        {{ csrf_field() }}
        <div class="col-lg-12 half_col">
            <div class="row half_row">
                <div class="col-12 half_col">
                    <p class="mb-2 text-primary"><b>Billing Address</b></p>
                </div>
                <div class="col-12 half_col mb-3">
                    <label class="mb-0 text-small" for="streetAddress"><b>Street Address*</b></label>
                    <input id="streetAddress" type="text" name="streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ $billingAddress->streetAddress }}"required/> 
                    @if ($errors->has('streetAddress'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('streetAddress') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-sm-6 half_col col-lg-4 mb-3">
                    <label class="mb-0 text-small" for="extendedAddress"><b>Apartment / Suite number</b></label>
                    <input id="extendedAddress" type="text" name="extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ $billingAddress->extendedAddress }}"/> 
                    @if ($errors->has('extendedAddress'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('extendedAddress') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-6 half_col col-lg-4 mb-3">
                    <label class="mb-0 text-small" for="city"><b>City*</b></label>
                    <input id="city" type="text" name="city" class="form-control mb-0" placeholder="City*" value="{{ $billingAddress->city }}" required /> 
                    @if ($errors->has('city'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-6 half_col col-lg-4 mb-3">
                    <label class="mb-0 text-small" for="postalCode"><b>Post Code*</b></label>
                    <input id="postalCode" type="text" name="postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ $billingAddress->postalCode }}" required /> 
                    @if ($errors->has('postalCode'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('postalCode') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="half_col col-lg-4 mb-3">
                    <label class="mb-0 text-small" for="country"><b>Country*</b></label>
                    <countries :setcountry="'{{$billingAddress->country}}'" :inid="'country'"></countries>
                    @if ($errors->has('country'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('country') }}</strong>
                    </span>
                    @endif
                </div>
                <div id="billing-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
                    <label class="mb-0 text-small" for="region"><b>State / Region*</b></label>
                    <input id="region" type="text" name="region" class="form-control mb-0" placeholder="State / Region" value="{{ $billingAddress->region }}"/> 
                    @if ($errors->has('region'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('region') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-sm-6 half_col col-lg-4 mb-3">
                    <label class="mb-0 text-small" for="company"><b>Company</b></label>
                    <input id="company" type="text" name="company" class="form-control mb-0" placeholder="Company" value="{{ $billingAddress->company }}"/> 
                    @if ($errors->has('company'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('company') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-12 half_col">
            <hr class="my-4"/>
        </div>
        <div class="col-lg-12 half_col">
            <div class="row half_row">
                <div class="col-lg-12 half_col">
                    <p class="mb-2 text-primary"><b>Shipping Address</b></p>
                </div>
               
                <div id="shipping-address" class="container half_col">
                    <div class="row half_row">
                        <div class="col-12 half_col mb-3">
                            <label class="mb-0 text-small" for="shipping-streetAddress"><b>Street Address*</b></label>
                            <input id="shipping-streetAddress" type="text" name="shipping-streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ $shippingAddress->streetAddress }}" /> 
                            @if ($errors->has('shipping-streetAddress'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-streetAddress') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6 half_col col-lg-4 mb-3">
                            <label class="mb-0 text-small" for="shipping-extendedAddress"><b>Apartment / Suite number</b></label>
                            <input id="shipping-extendedAddress" type="text" name="shipping-extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ $shippingAddress->extendedAddress }}"/> 
                            @if ($errors->has('shipping-extendedAddress'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-extendedAddress') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-6 half_col col-lg-4 mb-3">
                            <label class="mb-0 text-small" for="shipping-city"><b>City*</b></label>
                            <input id="shipping-city" type="text" name="shipping-city" class="form-control mb-0" placeholder="City*"value="{{ $shippingAddress->city }}" /> 
                            @if ($errors->has('shipping-city'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-city') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-6 half_col col-lg-4 mb-3">
                            <label class="mb-0 text-small" for="shipping-postalCode"><b>Post Code*</b></label>
                            <input id="shipping-postalCode" type="text" name="shipping-postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ $shippingAddress->postalCode }}" /> 
                            @if ($errors->has('shipping-postalCode'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-postalCode') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6 half_col col-lg-4 mb-3">
                            <label class="mb-0 text-small" for="shipping-country"><b>Country*</b></label>
                            <countries :setcountry="'{{ $shippingAddress->country }}'" :inid="'shipping-country'"></countries>
                            @if ($errors->has('shipping-country'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-country') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div id="shipping-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
                            <label class="mb-0 text-small" for="shipping-region"><b>State / Region*</b></label>
                            <input id="shipping-region" type="text" name="shipping-region" class="form-control mb-0" placeholder="State / Region" value="{{ $shippingAddress->region }}"/> 
                            @if ($errors->has('shipping-region'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-region') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6 half_col col-lg-4 mb-3">
                            <label class="mb-0 text-small" for="shipping-company"><b>Company</b></label>
                            <input id="shipping-company" type="text" name="shipping-company" class="form-control mb-0" placeholder="Company" value="{{ $shippingAddress->company }}"/> 
                            @if ($errors->has('shipping-company'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('shipping-company') }}</strong>
                            </span>
                            @endif

                        </div>
                    </div>
                </div>
             </div>
            <div class="col-12 half_col">
                <hr class="my-4"/>
                <div class="row half_row">
                    <div class="col-12 half_col">
                        <p class="mb-2 text-primary"><b>Consent</b></p>
                    </div>
                    <div class="col-12 half_col">
                        
                        <div id="agree_holder" class="d-block mb-3">
                            <div class="form-check">
                                <input id="agree" type="checkbox" class="form-check-input" name="agree"/>
                                <label class="form-check-label text-small" for="agree">
                                    By ticking this box you give your consent for the personal information entered on this form to be stored in a secure database. Your information will not be shared or used for marketing and is only stored so you may log in and continue to use the features of this application. You may remove your information from our database by contacting us at any time. You also agree to the <a href="{{route('tandcs')}}" target="_blank">Terms &amp; conditions</a> and <a href="{{route('privacy-policy')}}">Privacy Policy</a>.
                                </label>
                            </div>
                            @if ($errors->has('agree'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('agree') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div id="mailinglist_agree" class="d-block mb-3">
                            <div class="form-check">
                                <input id="mailinglist" type="checkbox" class="form-check-input" name="mailinglist"/>
                                <label class="form-check-label text-small" for="mailinglist">Join our mailing list to get great mobility tips straight to your inbox and be the first to find out about exclusive offers!
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-4 mb-2 text-right">
                      <button id="submit-button" type="submit" class="btn btn-primary d-inline-block" disabled>Save</button>
                    </div>
                </div>
            </div>
        </form>
        @else
        <form id="add-address-form" class="row half_row" action="/add-address" method="POST">
            {{ csrf_field() }}
            <div class="col-lg-12 half_col">
                <div class="row half_row">
                    <div class="col-12 half_col">
                        <p class="mb-2 text-primary"><b>Billing Address</b></p>
                    </div>
                    <div class="col-12 half_col mb-3">
                        <label class="mb-0 text-small" for="streetAddress"><b>Street Address*</b></label>
                        <input id="streetAddress" type="text" name="streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ old('streetAddress') }}"required/> 
                        @if ($errors->has('streetAddress'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('streetAddress') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-sm-6 half_col col-lg-4 mb-3">
                        <label class="mb-0 text-small" for="extendedAddress"><b>Apartment / Suite number</b></label>
                        <input id="extendedAddress" type="text" name="extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ old('extendedAddress') }}"/> 
                        @if ($errors->has('extendedAddress'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('extendedAddress') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-6 half_col col-lg-4 mb-3">
                        <label class="mb-0 text-small" for="city"><b>City*</b></label>
                        <input id="city" type="text" name="city" class="form-control mb-0" placeholder="City*" value="{{ old('city') }}" required /> 
                        @if ($errors->has('city'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-6 half_col col-lg-4 mb-3">
                        <label class="mb-0 text-small" for="postalCode"><b>Post Code*</b></label>
                        <input id="postalCode" type="text" name="postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ old('postalCode') }}" required /> 
                        @if ($errors->has('postalCode'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('postalCode') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="half_col col-lg-4 mb-3">
                        <label class="mb-0 text-small" for="country"><b>Country*</b></label>
                        <countries :setcountry="'GB'" :inid="'country'"></countries>
                        @if ($errors->has('country'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div id="billing-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
                        <label class="mb-0 text-small" for="region"><b>State / Region*</b></label>
                        <input id="region" type="text" name="region" class="form-control mb-0" placeholder="State / Region" value="{{ old('region') }}"/> 
                        @if ($errors->has('region'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('region') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-sm-6 half_col col-lg-4 mb-3">
                        <label class="mb-0 text-small" for="company"><b>Company</b></label>
                        <input id="company" type="text" name="company" class="form-control mb-0" placeholder="Company" value="{{ old('company') }}"/> 
                        @if ($errors->has('company'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('company') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12 half_col">
                <hr class="my-4"/>
            </div>
            <div class="col-lg-12 half_col">
                <div class="row half_row">
                    <div class="col-lg-12 half_col">
                        <p class="mb-2 text-primary"><b>Shipping Address</b></p>
                    </div>
                    <div id="shipping-address" class="container half_col">
                        <div class="row half_row">
                            <div class="col-12 half_col mb-3">
                                <label class="mb-0 text-small" for="shipping-streetAddress"><b>Street Address*</b></label>
                                <input id="shipping-streetAddress" type="text" name="shipping-streetAddress" class="form-control mb-0" placeholder="Street Address*" value="{{ old('shipping-streetAddress') }}" /> 
                                @if ($errors->has('shipping-streetAddress'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-streetAddress') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-6 half_col col-lg-4 mb-3">
                                <label class="mb-0 text-small" for="shipping-extendedAddress"><b>Apartment / Suite number</b></label>
                                <input id="shipping-extendedAddress" type="text" name="shipping-extendedAddress" class="form-control mb-0" placeholder="Apartment / Suite number" value="{{ old('shipping-extendedAddress') }}"/> 
                                @if ($errors->has('shipping-extendedAddress'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-extendedAddress') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-6 half_col col-lg-4 mb-3">
                                <label class="mb-0 text-small" for="shipping-city"><b>City*</b></label>
                                <input id="shipping-city" type="text" name="shipping-city" class="form-control mb-0" placeholder="City*"value="{{ old('shipping-city') }}" /> 
                                @if ($errors->has('shipping-city'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-city') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-6 half_col col-lg-4 mb-3">
                                <label class="mb-0 text-small" for="shipping-postalCode"><b>Post Code*</b></label>
                                <input id="shipping-postalCode" type="text" name="shipping-postalCode" class="form-control mb-0" placeholder="Post Code*" value="{{ old('shipping-postalCode') }}" /> 
                                @if ($errors->has('shipping-postalCode'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-postalCode') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-6 half_col col-lg-4 mb-3">
                                <label class="mb-0 text-small" for="shipping-country"><b>Country*</b></label>
                                <countries :setcountry="'GB'" :inid="'shipping-country'"></countries>
                                @if ($errors->has('shipping-country'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-country') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div id="shipping-state" class="col-sm-6 half_col col-lg-4 mb-3" style="display: none;">
                                <label class="mb-0 text-small" for="shipping-region"><b>State / Region*</b></label>
                                <input id="shipping-region" type="text" name="shipping-region" class="form-control mb-0" placeholder="State / Region" value="{{ old('shipping-region') }}"/> 
                                @if ($errors->has('shipping-region'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-region') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-sm-6 half_col col-lg-4 mb-3">
                                <label class="mb-0 text-small" for="shipping-company"><b>Company</b></label>
                                <input id="shipping-company" type="text" name="shipping-company" class="form-control mb-0" placeholder="Company" value="{{ old('shipping-company') }}"/> 
                                @if ($errors->has('shipping-company'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('shipping-company') }}</strong>
                                </span>
                                @endif

                                <input id="shippingPrice" type="text" name="shipping-rate" class="d-none" value="{{ old('shipping-rate') }}"/> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 half_col">
                <hr class="my-4"/>
                <div class="row half_row">
                    <div class="col-12 half_col">
                        <p class="mb-2 text-primary"><b>Consent</b></p>
                    </div>
                    <div class="col-12 half_col">
                        
                        <div id="agree_holder" class="d-block mb-3">
                            <div class="form-check">
                                <input id="agree" type="checkbox" class="form-check-input" name="agree"/>
                                <label class="form-check-label text-small" for="agree">
                                    By ticking this box you give your consent for the personal information entered on this form to be stored in a secure database. Your information will not be shared or used for marketing and is only stored so you may log in and continue to use the features of this application. You may remove your information from our database by contacting us at any time. You also agree to the <a href="{{route('tandcs')}}" target="_blank">Terms &amp; conditions</a> and <a href="{{route('privacy-policy')}}">Privacy Policy</a>.
                                </label>
                            </div>
                            @if ($errors->has('agree'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('agree') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div id="mailinglist_agree" class="d-block mb-3">
                            <div class="form-check">
                                <input id="mailinglist" type="checkbox" class="form-check-input" name="mailinglist"/>
                                <label class="form-check-label text-small" for="mailinglist">Join our mailing list to get great mobility tips straight to your inbox and be the first to find out about exclusive offers!
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-4 mb-2 text-right">
                      <button id="submit-button" type="submit" class="btn btn-primary d-inline-block" disabled>Save</button>
                    </div>
                </div>
            </div>
        </form>
        @endif
    </div>
</div>
</div>
@endsection
@section('scripts')
<script>
    $(window).on('load', function () {

        // If billing country is filled
        var cou = $('#country').val();
        if( cou != 'GB' && cou != 'IE'){
            $('#billing-state').show();
            $('#region').prop('required',true);
        }else{
            $('#billing-state').hide();
            $('#region').prop('required',false);
        }

        // If shipping country is filled
        var shcou = $('#shipping-country').val();
        if( shcou != 'GB' && shcou != 'IE'){
            $('#shipping-state').show();
            $('#shipping-region').prop('required',true);
        }else{
            $('#shipping-state').hide();
            $('#shipping-region').prop('required',false);
        }


        $('#country').change(function(){
            var cou = $('#country').val();
            if( cou != 'GB' && cou != 'IE'){
                $('#billing-state').show();
                $('#region').prop('required',true);
            }else{
                $('#billing-state').hide();
                $('#region').prop('required',false);
            }
        });
        $('#shipping-country').change(function(){
            var shcou = $('#shipping-country').val();
            if( shcou != 'GB' && shcou != 'IE'){
                $('#shipping-state').show();
                $('#shipping-region').prop('required',true);
            }else{
                $('#shipping-state').hide();
                $('#shipping-region').prop('required',false);
            }
        });
    });
    
    function checkInputs(){
        var billpass = true;
        var billcou = $('#country').val();
        var billst = $('#region').val();

        var shpass = true;
        var shcou = $('#shipping-country').val();
        var shst = $('#shipping-region').val();
        if( billcou != 'GB' && billcou != 'IE' && billst == ''){
            billpass =false
        }
        if( shcou != 'GB' && shcou != 'IE' && shst == ''){
            shpass =false
        }

        if( $('#first_name').val() != "" &&  $('#last_name').val() != "" && $('#email').val() != "" && $('#agree').prop('checked') == true && billpass == true && shpass == true){
            $('#form-warning').hide();
            document.getElementById("submit-button").removeAttribute('disabled');
        }else{
            $("#submit-button").attr("disabled", true);
            $('#form-warning').show();
        }
    }
    $('#first_name').keyup(function() {
        checkInputs();
    });
    $('#last_name').keyup(function() {
        checkInputs();
    });
    $('#email').keyup(function() {
        checkInputs();
    });
    $('#agree_holder').click(function() {
        checkInputs();
    });

    $('#region').keyup(function() {
        checkInputs();
    });

    $('#shipping-region').keyup(function() {
        checkInputs();
    });

    const form = document.getElementById('create-account-form');

    form.addEventListener('submit', async (event) => {
        document.getElementById("loader").classList.add("on");
        document.getElementById("loader-message").innerHTML = "Updating Address";
    });
</script>
@endsection