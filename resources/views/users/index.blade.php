@extends('layouts.compatible', ['page' => "Dashboard", 'pagetitle' => "Dashboard", 'padfooter' => true])
@section('styles')
<style>
    .sale-card{
        background-image: url('/img/sales/valentines/dash-bg.jpg');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        background-color: #85BED1;
    }
    .sale-text{
        font-weight:500;
        line-height: 50px;
        color: #000000;
    }
    .sale-text img{
        margin-top: -7px;
    }
    .sale-text span.sale-text-info{
        padding-left: 2rem !important;
    }
    .sale-card h3{
        font-size:1.7rem;
    }
    @media only screen and (max-width : 767px){
        .sale-card{
            background-position: 100% 100%;
        }
        .sale-text{
            font-size: 9vw;
            line-height: 1.3 !important;
        }
        .sale-text span.sale-text-info{
            padding-left: 0rem !important;
        }
    }
</style>
@endsection
@section('content')
<div class="container pb-5 pt-4">
    <div class="row">
        <div class="col-6">
            <p class="mb-0 text-small text-light-grey"><b>Your Dashboard</b></p>
        </div>
        <div class="col-6 text-right">
            <p class="mb-0 text-small text-light-grey">Account Active<i class="fa fa-circle text-success ml-2" aria-hidden="true"></i></p>
        </div>
        <div class="col-12">
            <hr class="mt-1 mb-4" />
            <h1 class="text-primary">Welcome {{$currentUser->first_name}}!</h1>
        </div>
        <div class="col-lg-6">
            <p class="mb-0 mob-mb-3">Hello beautiful! Let’s start creating better movement a little bit, every day. Get curious, experiment, and most of all have fun!</p>
        </div>
        <div class="col-lg-6">
            <div class="card p-3 bg-light-grey border-0 shadow">
                <p class="mb-0"><b>Using a Smart TV or an older device?</b></p>
                <p class="mb-0 mob-text-small"><a href="/dashboard/compatible-links"><u>Click here to use compatible versions of our programs</u></a></p>
            </div>
        </div> 
        <!-- <div class="col-12 mt-5">
	        <div class="card sale-card w-100 border-0 shadow p-3 text-center">
		        <h3 class="mb-0 mob-mb-3 sale-text">
                    <img src="/img/sales/valentines/dash-text.svg" alt="sale" width="280" class="mob-mb-3 mob-pt-4"/>
                    <br class="d-lg-none"/> 
			        <span class="ml-2 pl-5 mob-px-0 mob-mx-0 position-relative sale-text-info">UP TO 10% OFF ALL<br class="d-lg-none"/> PROGRAMS!<br class="d-lg-none"/>
    		        <a href="/shop">
                        <button type="button" class="btn text-capitalize btn-white text-primary d-inline-block ml-5 mob-mt-3 mob-mx-0 float-right mob-no-float border-0" style="background-color:#905125 !important;color: #fff !important; font-size: 22px;padding: 4px 15px;font-weight:400;">Shop Now</button>
                    </a>
		        </h3>
	        </div>
        </div> -->
    </div>
    <div class="row">
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 bg-light-grey border-0">
                <p class="text-small letter-spacing mb-0"><b>Level 1</b></p>
                <h4 class="mb-0">Build Your Foundation</h4>
                <hr class="dark-line"/>
                @foreach($products as $p)
                @if($p->category->slug == "build-your-foundation" && $p->bought == true && $p->id != 15 && $p->id != 16 && $p->slug != "beginners-bundle")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif  @if($p->id == 1 && $p->bought == true) mb-1 @endif">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 py-3 pl-2 pr-0">
                                <p class="dash-product-name text-small mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                @if($p->id ==1)
                                <a href="https://app.tommorrison.uk/dashboard/programs/smm/the-program">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @elseif($p->id ==2)
                                <a href="https://app.tommorrison.uk/dashboard/programs/where-i-went-wrong">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @elseif($p->id ==3)
                                <a href="https://app.tommorrison.uk/dashboard/programs/uc/info">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @else
                                <a href="/dashboard/products/view/{{$p->slug}}">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if($p->id == 1 && $p->bought == true)
                {{-- <p class="text-small"><b>Using a Smart TV or an older device?</b><br/><a href="/dashboard/products/view/the-simplistic-mobility-method"><u>Click here to use a compatible version of SMM.</u></a></p> --}}
                @endif
                @elseif($p->category->slug == "build-your-foundation" && $p->bought != true && $p->status == "Available" && $p->slug != "beginners-bundle")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3 pr-0">
                                <p class="dash-product-name mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="/products/{{$p->slug}}">
                                    <div class="d-table dash-product-btn buy-btn">
                                        <div class="d-table-cell align-middle">View</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 bg-light-grey border-0">
                <p class="text-small letter-spacing mb-0"><b>Level 2</b></p>
                <h4 class="mb-0">The Next Step</h4>
                <hr class="dark-line"/>
                @foreach($products as $p)
                @if($p->category->slug == "the-next-step" && $p->bought == true && $p->id != 15 && $p->id != 16)
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3 px-2">
                                <p class="dash-product-name text-small mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 px-0 position-relative">
                                @if($p->id == 17)
                                <a href="https://app.tommorrison.uk/dashboard/programs/sb/info">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @else
                                <a href="/dashboard/products/view/{{$p->slug}}">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($p->category->slug == "the-next-step" && $p->bought != true && $p->status == "Available")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3">
                                <p class="dash-product-name mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 px-0 position-relative">
                                <a href="/products/{{$p->slug}}">
                                    <div class="d-table dash-product-btn buy-btn">
                                        <div class="d-table-cell align-middle">View</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 bg-light-grey border-0">
                <p class="text-small letter-spacing mb-0"><b>Level 3</b></p>
                <h4 class="mb-0">Taking It Further</h4>
                <hr class="dark-line"/>
                @foreach($products as $p)
                @if($p->category->slug == "taking-it-further" && $p->bought == true)
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 py-3 pl-2 pr-0">
                                <p class="dash-product-name text-small mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="https://app.tommorrison.uk/dashboard/programs/er">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($p->category->slug == "taking-it-further" && $p->bought != true && $p->status == "Available")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3 pr-0">
                                <p class="dash-product-name mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="/products/{{$p->slug}}">
                                    <div class="d-table dash-product-btn buy-btn">
                                        <div class="d-table-cell align-middle">View</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-12 mt-5">
            <hr/>
        </div>
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 bg-light-grey border-0">
                <p class="text-small letter-spacing mb-0"><b>Bonus</b></p>
                <h4 class="mb-0">Extra Awesomeness</h4>
                <hr class="dark-line"/>
                @foreach($products as $p)
                @if($p->category->slug == "extra-awesomeness" && $p->bought == true)
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 py-3 pl-2 pr-0">
                                <p class="dash-product-name text-small mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                @if($p->id == 7)
                                <a href="https://app.tommorrison.uk/dashboard/programs/sh/info">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @elseif($p->id ==8)
                                <a href="https://app.tommorrison.uk/dashboard/programs/bb/info">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @else
                                <a href="/dashboard/products/view/{{$p->slug}}">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($p->category->slug == "extra-awesomeness" && $p->bought != true && $p->status == "Available")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3 pr-0">
                                <p class="dash-product-name mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="/products/{{$p->slug}}">
                                    <div class="d-table dash-product-btn buy-btn">
                                        <div class="d-table-cell align-middle">View</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        @if(count($onlinecamps) || $showcamps)
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 bg-light-grey border-0">
                <h4 class="mb-0">Online Camps</h4>
                <hr class="dark-line"/>
                @foreach($products as $p)
                @if($p->category->slug == "online-camps" && $p->bought == true)
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 py-3 pl-2 pr-0">
                                <p class="dash-product-name text-small mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="https://app.tommorrison.uk/dashboard/camps/{{$p->id}}">
                                    <div class="d-table dash-product-btn view-btn">
                                        <div class="d-table-cell align-middle">Open</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @elseif($p->category->slug == "online-camps" && $p->bought != true && $p->status == "Available")
                <div class="dash-product border-0 mb-3 @if($p->bought == true) purchased @endif ">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-2 py-3 pr-0">
                                <p class="dash-product-name mb-0">{{$p->name}}@if($p->id ==1)&reg;@endif</p>
                            </div>
                            <div class="col-3 pr-0 position-relative">
                                <a href="/products/{{$p->slug}}">
                                    <div class="d-table dash-product-btn buy-btn">
                                        <div class="d-table-cell align-middle">View</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        @endif
        <div class="col-lg-4 mt-5 mob-mt-4">
            <div class="card shadow p-3 border-0">
                <p class="mob-mt-3 mb-0"><u><a id="wantcollapse" data-toggle="collapse" href="#wantsomethingextra" role="button" aria-expanded="false" aria-controls="wantsomethingextra">Want something extra? <i class="fa fa-angle-down float-right"></i></a></u></p>
                <div class="collapse" id="wantsomethingextra">
                    <p class="mt-3"><b>Sometimes you need a pair of expert eyes to discover the cause of pain or plateau.</b></p>

                    <p>We have two options to help you make progress:</p>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-video-camera"></i>
                        </div>
                        <div class="col-10">
                            <p><a href="{{route('product','video-call')}}" target="_blank"><u>Video Call</u></a> with Tom, going through your past & current training and injuries, working out the best way to get you back to 100%.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="col-10">
                            <p><a href="{{route('product','personalised-mobility-program')}}" target="_blank"><u>Personalised Program</u></a> by Tom, is if you're really looking to make some gains. 4 weeks of one-to-one programming to really give you a kick start!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5 justify-content-center pt-5">
        <div class="col-lg-4 mob-mb-5">
            <div class="row">
                <div class="col-3">
                    <img src="/img/icons/google-review.svg" width="54" height="54" class="w-100 h-auto" alt="leave review on google" />
                </div>
                <div class="col-9 pl-0">
                    <h4>Leave Us A Review!</h4>
                    <p>If you love the stuff we create, it'd really help us out if you could take a moment to leave us a quick review on Facebook!</p>
                    <a href="https://www.facebook.com/MovementandMotion/reviews" target="_blank">
                        <div class="btn btn-primary mb-2">Leave Review</div>
                    </a>
                </div>
            </div>

        </div>
        <div class="col-lg-4">
            <div class="row">
                <div class="col-3">
                    <img src="/img/icons/arm.svg" width="54" height="54" class="w-100 h-auto" alt="leave review on google" />
                </div>
                <div class="col-9 pl-0">
                    <h4>Share Your Wins!</h4>
                    <p>We love to see what you guys are up to & the progress you make!</p>
                    <p>Tag us on <a href="https://www.instagram.com/tom.morrison.training/" target="_blank">Instagram</a>, <a href="https://www.facebook.com/Movementandmotion" target="_blank">Facebook</a>, even <a href="https://www.tiktok.com/@tom_morrison" target="_blank">TikTok</a> - or drop us <a href="mailto:hello@tommorrison.uk">an email</a></p>
                </div>
            </div>

        </div>
        <div class="col-12 my-5">
            <hr/>
        </div>
    </div>
    <div class="row">
	    <div class="col-12">
		    <p class="text-small text-center" style="color: #707070; line-height: 1.4;">The information contained in these online programs/products/e-books/camps is presented to improve movement, not treat medical conditions or injuries. This information is not a substitute for medical advice or treatment of medical conditions or injuries. If you feel any pain stop immediately. If in doubt, always seek the advice of your Doctor.</p>
	    </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var wantsomethingextra = document.getElementById('wantsomethingextra');
    var wantcollapse = document.getElementById('wantcollapse');
    wantsomethingextra.addEventListener('hide.bs.collapse', function () {
        wantcollapse.innerHTML = 'Want something extra? <i class="fa fa-angle-down float-right"></i>';
    });
    wantsomethingextra.addEventListener('show.bs.collapse', function () {
        wantcollapse.innerHTML = 'Want something extra? <i class="fa fa-angle-up float-right"></i>';
    });
</script>
@endsection