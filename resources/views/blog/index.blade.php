@php
$pagename = "Blog";
$page = 'Blog';
$pagetitle = "Blog - Stay up to date with the latest Mobility, Strength and Movement tips";
$meta_description = "In depth articles by Tom Morrison about his experience as a coach, his favourite mobility exercises and techniques, common training mistakes to avoid and what habits to build in order to gain strength and flexibility safely.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('content')
<div class="container-fluid page-scroll-to-top">
	<div class="row">
		<blog :count="{{$count}}" :categories="{{$categories}}" :savedsearch="'{{session('saved-search')}}'"></blog>
	</div>
</div>
@endsection