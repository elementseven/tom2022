@php
$page = 'light';
$pagetitle = $post->title . " - Tom Morrison";
$meta_description = $post->exerpt;
$pagename = "Blog";
$og_image = $post->getFirstMediaUrl('posts','large');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
	figure .attachment__caption{
		display: none !important;
	}
</style>
@endsection
@section('header')
<header class="container-fluid blog-post-top position-relative" style="background-image: url('{{$post->getFirstMediaUrl('posts','large')}}');">
	<div class="trans"></div>
	<div class="row">
		<div class="col-12">
			<div class="d-table">
				<div class="d-table-cell align-middle text-center">
					<h1 class="mb-3 display-4 text-white">{{$post->title}}</h1>
					<div class="fb-like" data-href="{{Request::url()}}" data-width="" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container py-5 my-5">
	<div class="row justify-content-center">
		<div class="col-md-10">
			{!!$post->body!!}
			<div class="fb-like" data-href="{{Request::url()}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
			<a href="/blog">
				<button class="btn btn-primary mt-4">Back to blogs page</button>
			</a>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var preElements = document.querySelectorAll('pre');

    preElements.forEach(function(pre) {
        if (pre.textContent.includes('youtubeid=')) {
            var ytid = pre.textContent.split('=')[1];
            var vidhtml = '<div class="embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + ytid + '?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>';
            pre.innerHTML = vidhtml;
        }
    });

    var figcaptions = document.querySelectorAll('figcaption');

    figcaptions.forEach(function(figcaption) {
        figcaption.style.display = 'none';
    });
});

</script>
@stop