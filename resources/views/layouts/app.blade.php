<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="">
  <link rel="canonical" href="https://tommorrison.uk/">
  <meta name="description" content="{{$meta_description}}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Tom Morrison">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$og_image}}">
  <meta property="og:description" content="{{$meta_description}}">
  <title>{{$pagetitle}}</title>
  <!-- Styles -->
  @yield('preloads')
  <script>window.dataLayer = window.dataLayer || [];</script>
  @yield('head_section')
  @vite(['resources/sass/app.scss'])
<!--
  <style>
    .bounce-2 {
        animation-name: bounce-2;
        animation-timing-function: ease;
        transform-origin: bottom;
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    @keyframes bounce-2 {
        0%   { transform: translateY(0); }
        50%  { transform: translateY(-10px); }
        100% { transform: translateY(0); }
    }
  </style>
-->
  <style>
/*#saleModal .modal-content{height:auto}.sale-modal-btn{position:absolute;background-color:#fff!important;color:#53002A!important;bottom:20%;border:0!important;max-width:380px;right:calc(50% - 180px)}#saleModal .modal-content .timer-holder{position:absolute;bottom:calc(16% + 124px);left:calc(50% - 145px);width:150px}#saleModal .modal-content p.close-modal{right:-40%;width:100%;bottom:calc(24% - 75px)}#saleModal .modal-content p{color:#fff}#saleModal .modal-content .timer .timer-box p{bottom:-13px;line-height:1.1rem;color:#fff}#saleModal h3{font-family:Karla;font-size:26px;line-height:1.4rem;color:#fff!important}#saleModal .timer-text{padding-right:0}#saleModal .timer{position:absolute;right:calc(50% - (64px + 127px));top:-11px}#saleModal .timer .timer-box .timer-number{color:#fff;border:2px solid #fff}@media only screen and (min-width :992px) and (max-width :1199px){#saleModal .modal-content p.close-modal{bottom:calc(24% - 98px);left:-15px}.sale-modal-btn{bottom:17%;right:7%}#saleModal .modal-content .timer-holder{bottom:calc(20% + 110px);left:35px!important}}@media only screen and (max-width :991px){#saleModal .modal-content .timer-holder{bottom:calc(20% + 55px);width:100%;left:0}#saleModal h3{font-size:4vw;line-height:4vw}#saleModal .timer-text{padding-right:0}.mob-pl-4{padding-left:1.5rem}.sale-modal-btn{bottom:18%;max-width:260px;left:calc(50% - 130px)}#saleModal .modal-content .timer{width:161px;position:relative;right:auto;top:-8px}#saleModal .modal-content p.close-modal{width:calc(100% - 2rem)!important;bottom:6%}#saleModal .modal-content .timer .timer-box p{bottom:-13px}}@media only screen and (max-width :767px){.sale-banner h3{font-size:7.5vw}#saleModal .modal-content p.close-modal{width:calc(100% - 2rem)!important;bottom:5%;left:1rem!important}#saleModal h3{font-size:6vw;line-height:6vw}.sale-modal-btn{bottom:11%}#saleModal .modal-content .timer-holder{bottom:calc(27% + 8px)}#saleModal .modal-content .timer{top:3px}}*/
</style>
@yield('styles')
</head>
<body>
 <!-- Google Tag Manager -->
@if(env('APP_ENV') == 'production')
<script async>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5K86JZ8');</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K86JZ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
@endif
@php 
$gifttest = false;
if(Cart::count()){
  foreach(Cart::content() as $i){
    if($i->options->gift == 'yes'){
      $gifttest = true;
    }
  }
}
@endphp
  <div id="fb-root"></div>
  <script async crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
  <!-- End Google Tag Manager (noscript) -->
  <div id="app" class="front">
    @if($page == 'Stability Builder')
    <div id="smm-menu" class="d-md-none " style="top: 8rem">
      <div class="smm-menu-btn">menu</div>
      <div class="smm-menu-inner">
        <div class="menu-item">
          <a class="nav-link" href="#what-do-you-get">What Do You Get?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#how-to-fit-it-in">How To Fit It In</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#the-workouts">The Workouts</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#pricing">Pricing</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#what-do-you-need">What Do You Need?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#faqs">FAQs</a>
        </div>
      </div>
    </div>
    @endif
    @if($page == 'The Simplistic Mobility Method' || $page == 'smm-usa' || $page == 'smm-aus')
    <div id="smm-menu" class="d-md-none ">
      <div class="smm-menu-btn">menu</div>
      <div class="smm-menu-inner">
        <div class="menu-item">
          <a class="nav-link" href="#whatissmm">What is SMM?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#pricing">Pricing</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#whatsincluded">What's Included</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#faqs">FAQs</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#testimonials">Testimonials</a>
        </div>
      </div>
    </div>
    @endif
        <div id="page" class="page">
	@if($page == 'smm-usa')
    <img src="/img/flags/flag-usa.svg" width="60" height="35" alt="SMM American Flag" class="smm-lp-flag" />
    @elseif($page == 'smm-aus')
     <img src="/img/flags/flag-aus.svg" width="60" height="35" alt="SMM Australian Flag" class="smm-lp-flag" />
    @endif
		@if(!isset($nomenuatall))
      @if($currentUser)
      <main-menu :user="{{$currentUser}}" :cart='@json(Cart::content())' :page="'{{$page}}'"></main-menu>
      @else
      <main-menu :user="null" :cart='@json(Cart::content())' :page="'{{$page}}'"></main-menu>
      @endif
      @endif
      <main id="content">
      @yield('header')
        @yield('content')
        @if(isset($getstarted))
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-6 col-sm-6  text-center py-5 mob-pt-0">
              <h3 class="mt-5 mb-3">READY TO GET STARTED?</h3>
              <a href="{{route('shop')}}">
                <div class="btn btn-primary mx-auto">View Products</div>
              </a>
            </div>
            <div class="col-xl-4 col-lg-5 col-sm-6 mob-px-0 text-center">
              <picture> 
                <source srcset="/img/tom-morrison-footer-top.webp" type="image/webp"/> 
                <source srcset="/img/tom-morrison-footer-top.png" type="image/png"/>
                <img src="/img/tom-morrison-footer-top.png" srcset="/img/tom-morrison-footer-top.png" id="tom-footer-top" class="img-fluid lazy mob-mb-0" alt="Tom morrison looking inquisitive." style="margin-bottom: -79px; max-width: 350px;" width="350" height="350" />
              </picture>
            </div>
          </div>
        </div>
        @endif
      </main>
      @if(!isset($nofooter))
      <footer class="@if(isset($nomenu) || isset($padfooter)) pt-5 @else mob-pt-0 @endif">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center mb-5">
              <img src="/img/logos/logo-full-white.svg" class="img-fluid footer-logo lazy" width="270" height="130" alt="Tom Morrison logo"/>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <p class="mb-2"><a href="{{route('about')}}">About Us</a></p>
              <p class="mb-2"><a href="{{route('blog')}}">Blog</a></p>
              <p class="mb-2"><a href="{{route('shop')}}">Shop</a></p>
              <p class="mb-2"><a href="{{route('merch')}}">Merchandise</a></p>
              <p class="mb-2"><a href="{{route('seminars')}}">Seminars</a></p>
              <p class="mb-2"><a href="{{route('facts')}}">Tom Morrison Facts</a></p>
              <p class="mb-2"><a href="{{route('contact')}}">Get in Touch</a></p>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <h4 class="text-white mb-3">Useful Links</h4>
              <p class="mb-2"><a href="{{route('product','the-simplistic-mobility-method')}}">The Simplistic Mobility Method</a></p>
              <p class="mb-2"><a href="{{route('simplistic-mobility-method-reviews')}}">Reviews</a></p>
              <p class="mb-2"><a href="{{route('lower-back-reach')}}">Lower Back Pain Help</a></p>
              <p class="mb-4 mob-mb-5"><a href="{{route('stretching-exercises')}}">Stretching Exercises</a></p>
              <h4 class="text-white mb-3">Follow Us:</h4>
              <p class="mob-mb-0">
                <a href="https://www.facebook.com/Movementandmotion" target="_blank" rel="noopener" aria-label="Facebook"><span class="social-circle mr-3 pl-0" style="width: 20px;"><i class="fa fa-facebook"></i></span></a>
                <a href="https://www.tiktok.com/@tom_morrison" target="_blank" rel="noopener" aria-label="TikTok"><span class="social-circle mr-3"><img src="/img/icons/tiktok-brands.svg" width="17" height="17" alt="tom morrison tiktok" class=" mb-1" /></span></a>
                <a href="https://www.instagram.com/tom.morrison.training" target="_blank" rel="noopener" aria-label="Instagram"><span class="social-circle mr-3"><i class="fa fa-instagram"></i></span></a>
                <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank" rel="noopener" aria-label="YouTube"><span class="social-circle mr-3"><i class="fa fa-youtube-play"></i></span></a>
                <a href="https://twitter.com/OMGTomMorrison" target="_blank" rel="noopener" aria-label="Twitter"><span class="social-circle mr-3"><i class="fa fa-twitter"></i></span></a>
              </p>
            </div>
            <div class="col-lg-4">
              {{-- <p class="mb-0"><a href="mailto:hello@tommorrison.uk"><i class="fa fa-envelope"></i> hello@tommorrison.uk</a></p> --}}
              <h4 class="text-white mt-4 mb-3 mob-mt-5">Sign up to our FREE 7 Days of Awesome Mobility Series!</h4>
              <mailing-list></mailing-list>
            </div>
            <div class="col-12 text-center mt-5">
              <p class="mb-0 text-footer-grey mb-3"><a href="{{route('tandcs')}}">Terms &amp; Conditions</a> | <a href="{{route('privacy-policy')}}">Privacy Policy</a> <span class="d-none d-md-inline">|</span><br class="d-md-none" /> <a href="{{route('delivery-and-returns')}}">Delivery &amp; Returns</a></p>
              <img src="/img/logos/powered-by-stripe.svg" data-srcset="/img/logos/powered-by-stripe.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Powered by Stripe Logo" width="110" height="25" />
              <img src="/img/logos/paypal.svg" data-srcset="/img/logos/paypal.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="PayPal Logo" width="86" height="20" />
              <img src="/img/logos/mastercard.svg" data-srcset="/img/logos/mastercard.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Mastercard Logo" width="36" height="27" />
              <img src="/img/logos/visa.svg" data-srcset="/img/logos/visa.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Visa Logo" width="47"  height="15"/>
              <img src="/img/logos/amex.svg" data-srcset="/img/logos/amex.svg" class="img-fluid lazy mob-mb-0" alt="American Express Logo" width="33" height="33" />
            </div>
          </div>
        </div>
      </footer>
      <div class="copyright text-center">
        <p class="mb-0">&copy; Tom Morrison {{Carbon\Carbon::now()->format('Y')}}. <a href="https://elementseven.co" style="color:#8F9195!important;" target="_blank">Website by Element Seven</a></p>
      </div>
      @endif
    </div>
    <div id="loader">
      <p id="loader-close"><i class="fa fa-close"></i></p>
      <div class="vert-mid">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto lazy" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-lg-7 text-center">
                  <p id="loader-second-text" class="mt-1 d-none cursor-pointer larger"><a id="loader-second-link"></a></p>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-4">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('modals')
    <!-- @if($page != 'Shop' && $page != 'Login' && $page != 'Basket' && $page != 'Checkout' && $page != 'Success' &&  Route::currentRouteName() != 'product' && Route::currentRouteName() != 'smm-yt-lp')
    <sale-modal></sale-modal>
    @endif	 -->
    <div class="modal fade" id="videoTestimonialsModal" tabindex="-1" role="dialog" aria-labelledby="videoTestimonialsModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="embed-responsive embed-responsive-9by16">
            <iframe id="videoTestimonialIframe" class="embed-responsive-item" src="" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>  
  </div>
  <img src="/img/icons/close-modal.svg" alt="close modal" width="50" class="close-video-modal"/>
  @if(!isset($nomenu))
  <div id="menu-btn" class="menu_btn d-sm-none">
    <div class="nav-icon">
      <span></span>
      <span></span>
      <span></span>   
    </div> 
  </div>
  <div id="shopping-cart" class="d-md-none">
    @if($gifttest == false)
    <a class="@if($page == 'light') text-white @endif" href="{{route('basket')}}" aria-label="shopping cart"><i class="fa fa-shopping-cart"></i>
      @if(Cart::count())
      <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
      @endif
    </a>
    @else
    <a class="@if($page == 'light') text-white @endif" href="{{route('gift-basket')}}"><i class="fa fa-gift"></i>
      @if(Cart::count())
      <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
      @endif
    </a>
    @endif
  </div>
  @if(!isset($nofooter))
  <div id="menubtntrigger"></div>
  @endif
  @endif
  <style>@import url('https://fonts.googleapis.com/css?family=Karla:400,700|Oswald:500,700&display=swap');</style>
  <!-- Scripts -->
  @yield('prescripts')
  @if($page != 'Homepage')
  @vite('resources/js/app.js')
  @else
  @vite('resources/js/home.js')
  @endif
  @yield('scripts')
  <script>
    window.onload = function () {
      var previewElements = document.querySelectorAll('.vlt-preview');
      previewElements.forEach(function (element) {
        element.setAttribute('href', '#');
        element.classList.add('added');
        element.addEventListener('click', function (e) {
          e.preventDefault(); // Prevent the default link click behavior
        });
      });
    };
    document.querySelectorAll('.close-video-modal').forEach(function(element) {
      element.addEventListener('click', function() {
        document.getElementById('videoTestimonialIframe').src = '';
        document.getElementById('videoTestimonialsModal').classList.remove('show');
        document.getElementById('videoTestimonialsModal').setAttribute('aria-hidden', 'true');
        document.body.classList.remove('modal-open');
        document.body.style.paddingRight = '0px';
      });
    });    
  </script>
</body>
</html>