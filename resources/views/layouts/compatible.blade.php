<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="">
  <link rel="canonical" href="https://tommorrison.uk/">
  <meta name="description" content="">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <meta property="og:type" content="website">
  <meta property="og:title" content="Tom Morrison">
  <meta property="og:url" content="https://tommorrison.uk/">
  <meta property="og:site_name" content="Tom Morrison">
  <meta property="og:locale" content="en_GB">

  <title>{{$pagetitle}}</title>
  <!-- Styles -->
  @vite(['resources/sass/app.scss'])
  @yield('styles')
   <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5K86JZ8');</script>
  <!-- End Google Tag Manager -->
  <style>
    #mobile-menu{
      position: fixed;
      top: 0;
      left: -120vw;
      width: 100%;
      height: 100%;
      background-color: #3e4146;
      color: #fff;
      z-index: 1000;
      -webkit-transition: all 200ms ease;
      -moz-transition: all 200ms ease ;
      -o-transition: all 200ms ease;
      transition: all 200ms ease;
    }
    #mobile-menu.on{
      left: 0;
    }
    #mobile-menu .mobile-menu-logo{
      position: absolute;
      top: 1rem;
      left: 1rem;
      width: 50px;
    }
    #mobile-menu .menu-inner{
      margin-top: 60px;
      padding-top: 30px;
      text-align: center;
    }
    #mobile-menu .menu-inner a{
      color: #fff;
      font-family: 'Oswald',sans-serif;
    }
    #mobile-menu .menu-inner a:hover{
      color: #d82737;
    }
    #mobile-menu .menu-inner a.nav-link{
      display: inline-block;
      font-size: 6vw;
      padding-bottom: 0.3rem;
      padding-left: 0;
      padding-right: 0;
      padding-top: 1rem;
    }
    #mobile-menu .menu-inner a.nav-link.underline{
      border-bottom: 2px solid #d82737;
    }
    #mobile-menu .menu-inner .menu-inner-bottom{
      position: absolute;
      width: 100%;
      left: 0;
      bottom: 0;
      margin-top: 0 -15px 0 -15px;
      padding: 1rem 1rem 0;
      background: #2D2F32;
      box-shadow: 0px -4px 6px -2px rgba(0, 0, 0, 0.1) !important;
    }
    #mobile-menu .menu-inner .menu-inner-bottom .social-circle{
      background-color: transparent;
      color: #fff;
      padding-top: 4px;
      border-radius: 100%;
      width: 35px;
      font-size: 20px;
      height: 35px;
      margin-left: 5px;
      text-align: center;
      display: inline-block;
      -webkit-transition: all 200ms ease;
      -moz-transition: all 200ms ease ;
      -o-transition: all 200ms ease;
      transition: all 200ms ease;
    }
    #mobile-menu .menu-inner .menu-inner-bottom .social-circle:hover{
      background-color: transparent;
      color: #d82737;
    }
  </style>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K86JZ8"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
  <!-- End Google Tag Manager (noscript) -->
  <div id="app" class="front">
    <div id="page" class="page">
      <nav id="menu" class="dash-menu navbar navbar-expand-lg navbar-light bg-transparent @if($page == 'post' || $page == 'facts') position-absolute w-100 @endif">
        <a class="navbar-brand" href="/">
          @if($page == 'post' || $page == 'facts')
          <img id="top-logo" src="/img/logos/logo-white.svg" class="img-fluid mt-3 ml-3 mob-m-0" alt="Tom Morrison logo white"/>
          @else
          <img id="top-logo" src="/img/logos/logo.svg" class="img-fluid mt-3 ml-3 mob-m-0" alt="Tom Morrison logo"/>
          @endif
        </a>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
          <ul class="navbar-nav ml-auto">
	        <li class="nav-item ">
            <a class="nav-link @if($pagetitle == 'Dashboard') underline @endif" href="{{route('dashboard')}}">Dashboard</a>
          </li>
          @if(count($currentUser->orders))
          <li class="nav-item">
            <a class="nav-link @if($pagetitle == 'Dashboard - Orders') underline @endif" href="{{route('orders')}}">Merch Orders</a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link @if($pagetitle == 'Dashboard - Support') underline @endif" href="{{route('support')}}">Support</a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($pagetitle == 'Dashboard - Account Settings') underline @endif" href="https://app.tommorrison.uk/dashboard/account">Account Settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('logout')}}">Logout</a>
          </li>
          
          </ul>
        </div>
      </nav>

      <div id="content" style="padding-top: 0;">
        @if(session('action_status'))
        <div id="notification_stripe" class="container-fluid primary_bg py-3">
            <div class="row">
                <div class="container">
                    <div class="row mob_text_center">
                        <div class="col-md-10">
                            <h3>{{session('action_status')}}</h3>
                            <p class="mb-0">{{session('action_message')}}</p>
                        </div>
                        <div class="col-md-2">  
                            <div id="dismiss" class="btn btn-white btn-200 mt-1 float-right mob_no_float mob_btn_small">Dismiss</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @php 
        session()->forget('action_status');
        session()->forget('action_message');
        @endphp
        @yield('header')
        @yield('content')
      </div>
      <footer class="@if(isset($nomenu) || isset($padfooter)) pt-5 @else mob-pt-0 @endif">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center mb-5">
              <img src="/img/logos/logo-full-white.svg" class="img-fluid footer-logo lazy" width="270" alt="Tom Morrison logo"/>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <p class="mb-0"><a href="{{route('about')}}">About Us</a></p>
              <p class="mb-0"><a href="{{route('blog')}}">Blog</a></p>
              <p class="mb-0"><a href="{{route('shop')}}">Shop</a></p>
              <p class="mb-0"><a href="{{route('merch')}}">Merchandise</a></p>
              <p class="mb-0"><a href="{{route('seminars')}}">Seminars</a></p>
              <p class="mb-0"><a href="{{route('facts')}}">Tom Morrison Facts</a></p>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <h4 class="text-white">Useful Links</h4>
              <p class="mb-0"><a href="{{route('product','the-simplistic-mobility-method')}}">The Simplistic Mobility Method</a></p>
              <p class="mb-0"><a href="{{route('simplistic-mobility-method-reviews')}}">Reviews</a></p>
              <p class="mb-0"><a href="{{route('lower-back-reach')}}">Lower Back Pain Help</a></p>
              <p class="mb-4 mob-mb-5"><a href="{{route('stretching-exercises')}}">Stretching Exercises</a></p>
              
            </div>
            <div class="col-lg-4">
              <p class="mb-0"><a href="{{route('contact')}}">Get in Touch</a></p>
              <p class="mb-0"><a href="mailto:hello@tommorrison.uk"><i class="fa fa-envelope"></i> hello@tommorrison.uk</a></p>
              <h4 class="text-white mb-2 mt-4">Follow Us:</h4>
              <p class="mob-mb-0">
                <a href="https://www.facebook.com/Movementandmotion" target="_blank" rel="noopener" aria-label="Facebook"><span class="social-circle pl-0" style="width: 20px;"><i class="fa fa-facebook"></i></span></a>
                <a href="https://www.tiktok.com/@tom_morrison" target="_blank" rel="noopener" aria-label="TikTok"><span class="social-circle"><img src="/img/icons/tiktok-brands.svg" width="17" height="17" alt="tom morrison tiktok" class=" mb-1" /></span></a>
                <a href="https://www.instagram.com/tom.morrison.training" target="_blank" rel="noopener" aria-label="Instagram"><span class="social-circle"><i class="fa fa-instagram"></i></span></a>
                <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank" rel="noopener" aria-label="YouTube"><span class="social-circle"><i class="fa fa-youtube-play"></i></span></a>
                <a href="https://twitter.com/OMGTomMorrison" target="_blank" rel="noopener" aria-label="Twitter"><span class="social-circle"><i class="fa fa-twitter"></i></span></a>
              </p>
            </div>
            <div class="col-12 text-center mt-5">
              <p class="mb-0 text-footer-grey mb-3"><a href="{{route('tandcs')}}">Terms &amp; Conditions</a> | <a href="{{route('privacy-policy')}}">Privacy Policy</a> <span class="d-none d-md-inline">|</span><br class="d-md-none" /> <a href="{{route('delivery-and-returns')}}">Delivery &amp; Returns</a></p>
              <img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" class=" lazy mob-mb-0 mr-2" alt="PayPal Logo" width="86" />
              <img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" class="lazy mob-mb-0 mr-2" alt="Mastercard Logo" width="36" />
              <img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" class="lazy mob-mb-0 mr-2" alt="Visa Logo" width="47" />
              <img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" class="lazy mob-mb-0" alt="American Express Logo" width="33"/>
            </div>
          </div>
        </div>
      </footer>
      <div class="copyright text-center">
        <p class="mb-0">&copy; Tom Morrison {{Carbon\Carbon::now()->format('Y')}}. <a href="https://elementseven.co" style="color:#8F9195!important;" target="_blank">Website by Element Seven</a></p>
      </div>
    </div>
    <div id="loader">
      <div class="vert-mid">
        <img id="loader-success" src="/img/icons/success.svg" class="d-none mx-auto" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-md-4">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
                <div class="col-md-12 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer"><a id="loader-second-link"></a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('modals')
  </div>

  <div class="menu_btn d-sm-none">
    <div class="nav-icon">
      <span></span>
      <span></span>
      <span></span>   
    </div> 
  </div>

  <div id="mobile-menu">
    <img src="/img/logos/logo-white.svg" class="mobile-menu-logo lazy" alt="Tom Morrison logo white"/>
    <div class="menu-inner">
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagetitle == "Dashboard") underline @endif " href="{{route('dashboard')}}">Dashboard</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagetitle == "Dashboard - Account") underline @endif " href="https://app.tommorrison.uk/dashboard/account">Account</a>
        </p>
      </div>
      @if(count($currentUser->orders))
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagetitle == "Dashboard - Orders") underline @endif " href="{{route('orders')}}">Merch Orders</a>
        </p>
      </div>
      @endif
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagetitle == "Dashboard - Support") underline @endif " href="{{route('support')}}">Support</a>
        </p>
      </div>

      <div class="menu-inner-bottom">
        <div class="row">
          <div class="col-5 text-left">
            <a href="/logout">
              <button class="btn btn-white-line">Logout</button>
            </a>
          </div>
          <div class="col-7">
            <p class="text-right">
              <a href="https://www.facebook.com/Movementandmotion" target="_blank" rel="noopener" aria-label="Facebook"><span class="social-circle"><i class="fa fa-facebook"></i></span></a>
              <a href="https://www.tiktok.com/@tom_morrison" target="_blank" rel="noopener" aria-label="TikTok"><span class="social-circle"><img src="/img/icons/tiktok-brands.svg" width="18" height="18" alt="tom morrison tiktok"/></span></a>
              <a href="https://www.instagram.com/tom.morrison.training" target="_blank" rel="noopener" aria-label="Instagram"><span class="social-circle"><i class="fa fa-instagram"></i></span></a>
              <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank" rel="noopener" aria-label="YouTube"><span class="social-circle"><i class="fa fa-youtube-play"></i></span></a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="orientation_change">
    <p>This website is best experienced in portrait mode, please rotate your device to continue.</p>
    {{-- <img width="200" src="/img/icons/mobile-rotate.png" alt="logo"> --}}
  </div>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css"/>
  <style>@import url('https://fonts.googleapis.com/css?family=Karla:400,700|Oswald:500,700');</style>
  <!-- Scripts -->
  @yield('scripts')
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
<script>
  var menu_count = 0;
document.querySelector('.menu_btn').addEventListener('click', function() {
    this.querySelector('.nav-icon').classList.toggle('nav_open');
    document.getElementById('mobile-menu').classList.toggle('on');
    document.getElementById('shopping-cart').classList.toggle('menu-open');
});
</script>
</body>
</html>