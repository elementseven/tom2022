<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="">
  <link rel="canonical" href="https://tommorrison.uk/">
  <meta name="description" content="{{$meta_description}}">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Tom Morrison">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$og_image}}">
  <meta property="og:description" content="{{$meta_description}}">

  <title>{{$pagetitle}}</title>
  <!-- Styles -->
  <link href="{{ mix('css/app.min.css') }}" rel="stylesheet"/>

  <script>window.dataLayer = window.dataLayer || [];</script>
  @yield('head_section')
  
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5K86JZ8');</script>
  <!-- End Google Tag Manager -->
  
<!--
  <style>
    .bounce-2 {
        animation-name: bounce-2;
        animation-timing-function: ease;
        transform-origin: bottom;
        align-self: flex-end;
        animation-duration: 2s;
        animation-iteration-count: infinite;
    }
    @keyframes bounce-2 {
        0%   { transform: translateY(0); }
        50%  { transform: translateY(-10px); }
        100% { transform: translateY(0); }
    }
  </style>
-->

  <style>
    .cc-window{opacity:1;transition:opacity 1s ease}.cc-window.cc-invisible{opacity:0}.cc-animate.cc-revoke{transition:transform 1s ease}.cc-animate.cc-revoke.cc-top{transform:translateY(-2em)}.cc-animate.cc-revoke.cc-bottom{transform:translateY(2em)}.cc-animate.cc-revoke.cc-active.cc-bottom,.cc-animate.cc-revoke.cc-active.cc-top,.cc-revoke:hover{transform:translateY(0)}.cc-grower{max-height:0;overflow:hidden;transition:max-height 1s}
.cc-link,.cc-revoke:hover{text-decoration:underline}.cc-revoke,.cc-window{position:fixed;overflow:hidden;box-sizing:border-box;font-family:Helvetica,Calibri,Arial,sans-serif;font-size:16px;line-height:1.5em;display:-ms-flexbox;display:flex;-ms-flex-wrap:nowrap;flex-wrap:nowrap;z-index:9999}.cc-window.cc-static{position:static}.cc-window.cc-floating{padding:2em;max-width:24em;-ms-flex-direction:column;flex-direction:column}.cc-window.cc-banner{padding:1em 1.8em;width:100%;-ms-flex-direction:row;flex-direction:row}.cc-revoke{padding:.5em}.cc-header{font-size:18px;font-weight:700}.cc-btn,.cc-close,.cc-link,.cc-revoke{cursor:pointer}.cc-link{opacity:.8;display:inline-block;padding:.2em}.cc-link:hover{opacity:1}.cc-link:active,.cc-link:visited{color:initial}.cc-btn{display:block;padding:.4em .8em;font-size:.9em;font-weight:700;border-width:2px;border-style:solid;text-align:center;white-space:nowrap}.cc-highlight .cc-btn:first-child{background-color:transparent;border-color:transparent}.cc-highlight .cc-btn:first-child:focus,.cc-highlight .cc-btn:first-child:hover{background-color:transparent;text-decoration:underline}.cc-close{display:block;position:absolute;top:.5em;right:.5em;font-size:1.6em;opacity:.9;line-height:.75}.cc-close:focus,.cc-close:hover{opacity:1}
.cc-revoke.cc-top{top:0;left:3em;border-bottom-left-radius:.5em;border-bottom-right-radius:.5em}.cc-revoke.cc-bottom{bottom:0;left:3em;border-top-left-radius:.5em;border-top-right-radius:.5em}.cc-revoke.cc-left{left:3em;right:unset}.cc-revoke.cc-right{right:3em;left:unset}.cc-top{top:1em}.cc-left{left:1em}.cc-right{right:1em}.cc-bottom{bottom:1em}.cc-floating>.cc-link{margin-bottom:1em}.cc-floating .cc-message{display:block;margin-bottom:1em}.cc-window.cc-floating .cc-compliance{-ms-flex:1 0 auto;flex:1 0 auto}.cc-window.cc-banner{-ms-flex-align:center;align-items:center}.cc-banner.cc-top{left:0;right:0;top:0}.cc-banner.cc-bottom{left:0;right:0;bottom:0}.cc-banner .cc-message{display:block;-ms-flex:1 1 auto;flex:1 1 auto;max-width:100%;margin-right:1em}.cc-compliance{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-line-pack:justify;align-content:space-between}.cc-floating .cc-compliance>.cc-btn{-ms-flex:1;flex:1}.cc-btn+.cc-btn{margin-left:.5em}
@media print{.cc-revoke,.cc-window{display:none}}@media screen and (max-width:900px){.cc-btn{white-space:normal}}@media screen and (max-width:414px) and (orientation:portrait),screen and (max-width:736px) and (orientation:landscape){.cc-window.cc-top{top:0}.cc-window.cc-bottom{bottom:0}.cc-window.cc-banner,.cc-window.cc-floating,.cc-window.cc-left,.cc-window.cc-right{left:0;right:0}.cc-window.cc-banner{-ms-flex-direction:column;flex-direction:column}.cc-window.cc-banner .cc-compliance{-ms-flex:1 1 auto;flex:1 1 auto}.cc-window.cc-floating{max-width:none}.cc-window .cc-message{margin-bottom:1em}.cc-window.cc-banner{-ms-flex-align:unset;align-items:unset}.cc-window.cc-banner .cc-message{margin-right:0}}
.cc-floating.cc-theme-classic{padding:1.2em;border-radius:5px}.cc-floating.cc-type-info.cc-theme-classic .cc-compliance{text-align:center;display:inline;-ms-flex:none;flex:none}.cc-theme-classic .cc-btn{border-radius:5px}.cc-theme-classic .cc-btn:last-child{min-width:140px}.cc-floating.cc-type-info.cc-theme-classic .cc-btn{display:inline-block}
.cc-theme-edgeless.cc-window{padding:0}.cc-floating.cc-theme-edgeless .cc-message{margin:2em 2em 1.5em}.cc-banner.cc-theme-edgeless .cc-btn{margin:0;padding:.8em 1.8em;height:100%}.cc-banner.cc-theme-edgeless .cc-message{margin-left:1em}.cc-floating.cc-theme-edgeless .cc-btn+.cc-btn{margin-left:0}
.cc-compliance,.cc-banner .cc-message{
  display: inline-block !important;
  text-align: center;
}
.cc-window{
  text-align: center;
  display: block !important;
}
.cc-window.cc-invisible{
  display: none !important;
}
  #saleModal .modal-content{
    height: auto;
  }
  .xmas-shop-btn{
    position: absolute;
    bottom: 23%;
    border: 0 !important;
    border-radius: 0 !important ;
    max-width: 380px;
    left: calc(50% - 190px);
  }
  #saleModal .modal-content .timer-holder {
    position: absolute;
    bottom: calc(20% + 90px);
    left: 0;
    width: 100%;
  }
  #saleModal .modal-content p.close-modal{
    bottom: calc(24% - 90px);
  }
  #saleModal .modal-content p{
    color: #fff;
  }
  #saleModal .modal-content .timer .timer-box p{
    bottom:  -13px;
    line-height: 1.1rem;
    color: #fff;
  }
  #saleModal h3{
    font-size:  3rem;
    line-height: 3rem;
    color: #fff !important;
  }
  #saleModal .timer-text{
    padding-right: 175px;
  }
  #saleModal .timer{
    position: absolute;
    right: calc(50% - (64px + 90px));
    top: -11px;
  }
  #saleModal .timer .timer-box .timer-number{
    color: #fff;
    border:  2px solid #fff;
  }
@media only screen and (max-width : 991px){
  #saleModal .modal-content .timer-holder {
    bottom: calc(20% + 84px);
    width: 100%;
    left: 0;
  }
  #saleModal h3{
    font-size: 6vw;
    line-height: 6vw;
  }
  #saleModal .timer-text{
    padding-right: 0;
  }
  .mob-pl-4{
    padding-left: 1.5rem;
  }
  .xmas-shop-btn{
    bottom: 21%;
    max-width: 260px;
    left: calc(50% - 130px);
  }
  #saleModal .modal-content .timer {
    width: 161px;
    position: relative;
    right: auto;
    top: 6px;
  }
  #saleModal .modal-content p.close-modal{
    bottom: 11%;
  }
  #saleModal .modal-content .timer .timer-box p {
    bottom: -13px;
  }
}

@media only screen and (max-width : 767px){
  .sale-banner h3{
    font-size: 7.5vw;
  }
}

</style>
</head>

<body>
@php 
$gifttest = false;
if(Cart::count()){
  foreach(Cart::content() as $i){
    if($i->options->gift == 'yes'){
      $gifttest = true;
    }
  }
}
@endphp
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K86JZ8"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
  <!-- End Google Tag Manager (noscript) -->
  <div id="app" class="front">
	  
    @if($page == 'Stability Builder')
    <div id="smm-menu" class="d-md-none " style="top: 8rem">
      <div class="smm-menu-btn">menu</div>
      <div class="smm-menu-inner">
        <div class="menu-item">
          <a class="nav-link" href="#what-do-you-get">What Do You Get?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#how-to-fit-it-in">How To Fit It In</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#the-workouts">The Workouts</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#pricing">Pricing</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#what-do-you-need">What Do You Need?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#faqs">FAQs</a>
        </div>
      </div>
    </div>
    @endif

    @if($page == 'The Simplistic Mobility Method' || $page == 'smm-usa' || $page == 'smm-aus')
    <div id="smm-menu" class="d-md-none ">
      <div class="smm-menu-btn">menu</div>
      <div class="smm-menu-inner">
        <div class="menu-item">
          <a class="nav-link" href="#whatissmm">What is SMM?</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#pricing">Pricing</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#whatsincluded">What's Included</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#faqs">FAQs</a>
        </div>
        <div class="menu-item">
          <a class="nav-link" href="#testimonials">Testimonials</a>
        </div>
      </div>
    </div>
    @endif

        <div id="page" class="page">
{{--     @if(Route::currentRouteName() != 'smm-yt-lp')
   <a  href="{{route('shop')}}">
    <div class="container-fluid text-center px-3 pb-3 pt-2 mob-pt-0 position-relative z-2 birthday-gradient text-white sale-banner">
      <div class="row">
        <div class="col-12 pt-3">
          <h3 class="mb-0 d-inline-block" style="color: #fff;font-weight: 300;">Jenni's Birthday Flash Sale! <span class="d-none d-lg-inline"></span><br class="d-lg-none"/>Ends in <timer ds="2022-04-20T08:00:00+00:00"></timer></h3>
            <button type="button" class="btn text-capitalize btn-white d-none d-lg-inline mt-2 ml-5" style="font-size: 22px;padding: 4px 15px;font-weight:400;background-color: #fff !important; border-color:#fff; color:#000;margin-top: -15px !important;">Shop Now</button>
            <button type="button" class="btn text-capitalize btn-white d-inline-block d-lg-none mt-2 mx-0" style="font-size: 22px;padding: 4px 15px;font-weight:400;background-color: #fff !important; border-color:#fff; color:#000;">Shop Now</button>
        </div>
      </div>
    </div>
	  </a>
    @endif --}}

	@if($page == 'smm-usa')
    <img src="/img/flags/flag-usa.svg" width="60" height="35" alt="SMM American Flag" class="smm-lp-flag" />
    @elseif($page == 'smm-aus')
     <img src="/img/flags/flag-aus.svg" width="60" height="35" alt="SMM Australian Flag" class="smm-lp-flag" />
    @endif
    
		@if(!isset($nomenuatall))
      <nav id="menu" class="navbar navbar-expand-md navbar-light bg-transparent @if($page == 'light') position-absolute w-100 @endif">
        <a class="navbar-brand" href="/">
          @if($page == 'light')
          <img id="top-logo" src="/img/logos/logo-white.svg" class="img-fluid mt-3 ml-3 mob-m-0" alt="Tom Morrison logo white" width="65" height="65" />
          @else
          <img id="top-logo" src="/img/logos/logo.svg" class="img-fluid mt-3 ml-3 mob-m-0" alt="Tom Morrison logo" width="65" height="65"/>
          @endif
        </a>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
          <ul class="navbar-nav ml-auto">
            @if(!isset($nomenu))
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('shop')}}">Shop</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('merch')}}">Merch</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('seminars')}}">Seminars</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('about')}}">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('blog')}}">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('simplistic-mobility-method-reviews')}}">Reviews</a>
            </li>
            <li class="nav-item">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('contact')}}">Get In Touch</a>
            </li>
            @if($gifttest == false)
            <li class="nav-item position-relative">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('basket')}}"><i class="fa fa-shopping-cart"></i>
	            @if(Cart::count())
              <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
              @endif
              </a>
            </li>  
            @else
            <li class="nav-item position-relative">
              <a class="nav-link @if($page == 'light') text-white @endif" href="{{route('gift-basket')}}"><i class="fa fa-gift"></i>
              @if(Cart::count())
              <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
              @endif
              </a>
            </li>  
            @endif             
            <li class="nav-item">
              <a class="nav-link" href="/login"><img src="/img/icons/user-circle-solid.svg" class="img-fluid" width="40" alt="User login button"/></a>
            </li>
            @endif
            @if($page == 'The Simplistic Mobility Method' || $page == 'smm-usa' || $page == 'smm-aus')
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#whatissmm">What is SMM?</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#pricing">Pricing</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#whatsincluded">What's Included</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#faqs">FAQs</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#testimonials">Testimonials</a>
            </li>
            @endif

            @if($page == 'Stability Builder')
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#what-do-you-get">What Do You Get?</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#how-to-fit-it-in">How To Fit It In</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#the-workouts">The Workouts</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#pricing">Pricing</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#what-do-you-need">What Do You Need?</a>
            </li>
            <li class="nav-item pt-4">
              <a class="nav-link @if($page == 'light') text-white @endif" href="#faqs">FAQs</a>
            </li>
            @endif

          </ul>
        </div>
      </nav>
      @endif
      <div id="content">
        @yield('header')
        <main>
          @yield('content')
          @if(isset($getstarted))
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-6 col-sm-6  text-center py-5 mob-pt-0">
                <h3 class="mt-5 mb-3">READY TO GET STARTED?</h3>
                <a href="{{route('shop')}}">
                  <div class="btn btn-primary mx-auto">View Products</div>
                </a>
              </div>
              <div class="col-xl-4 col-lg-5 col-sm-6 mob-px-0 text-center">
                <picture> 
                  <source data-srcset="/img/tom-morrison-footer-top.webp" type="image/webp"/> 
                  <source data-srcset="/img/tom-morrison-footer-top.png" type="image/png"/>
                  <img data-src="/img/tom-morrison-footer-top.png" data-srcset="/img/tom-morrison-footer-top.png" id="tom-footer-top" class="img-fluid lazy mob-mb-0" alt="Tom morrison looking inquisitive." style="margin-bottom: -79px; max-width: 350px;" width="350" height="350" />
                </picture>
              </div>
            </div>
          </div>
          @endif
        </main>
      </div>
      <footer class="@if(isset($nomenu) || isset($padfooter)) pt-5 @else mob-pt-0 @endif">
        <div class="container">
          <div class="row">
            <div class="col-12 text-center mb-5">
              <img data-src="/img/logos/logo-full-white.svg" class="img-fluid footer-logo lazy" width="270" height="130" alt="Tom Morrison logo"/>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <p class="mb-0"><a href="{{route('about')}}">About Us</a></p>
              <p class="mb-0"><a href="{{route('blog')}}">Blog</a></p>
              <p class="mb-0"><a href="{{route('shop')}}">Shop</a></p>
              <p class="mb-0"><a href="{{route('merch')}}">Merchandise</a></p>
              <p class="mb-0"><a href="{{route('seminars')}}">Seminars</a></p>
              <p class="mb-0"><a href="{{route('facts')}}">Tom Morrison Facts</a></p>
            </div>
            <div class="col-lg-4 mob-mb-5">
              <h4 class="text-white">Useful Links</h4>
              <p class="mb-0"><a href="{{route('product','the-simplistic-mobility-method')}}">The Simplistic Mobility Method</a></p>
              <p class="mb-0"><a href="{{route('simplistic-mobility-method-reviews')}}">Reviews</a></p>
              <p class="mb-0"><a href="{{route('lower-back-reach')}}">Lower Back Pain Help</a></p>
              <p class="mb-4 mob-mb-5"><a href="{{route('stretching-exercises')}}">Stretching Exercises</a></p>
              <h4 class="text-white mb-2">Follow Us:</h4>
              <p class="mob-mb-0">
                <a href="https://www.facebook.com/Movementandmotion" target="_blank" rel="noopener" aria-label="Facebook"><span class="social-circle pl-0" style="width: 20px;"><i class="fa fa-facebook"></i></span></a>
                <a href="https://www.tiktok.com/@tom_morrison" target="_blank" rel="noopener" aria-label="TikTok"><span class="social-circle"><img src="/img/icons/tiktok-brands.svg" width="17" height="17" alt="tom morrison tiktok" class=" mb-1" /></span></a>
                <a href="https://www.instagram.com/tom.morrison.training" target="_blank" rel="noopener" aria-label="Instagram"><span class="social-circle"><i class="fa fa-instagram"></i></span></a>
                <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank" rel="noopener" aria-label="YouTube"><span class="social-circle"><i class="fa fa-youtube-play"></i></span></a>
                <a href="https://twitter.com/OMGTomMorrison" target="_blank" rel="noopener" aria-label="Twitter"><span class="social-circle"><i class="fa fa-twitter"></i></span></a>
              </p>
            </div>
            <div class="col-lg-4">
              <p class="mb-0"><a href="{{route('contact')}}">Get in Touch</a></p>
              <p class="mb-0"><a href="mailto:hello@tommorrison.uk"><i class="fa fa-envelope"></i> hello@tommorrison.uk</a></p>
              <h4 class="text-white mt-4 mb-3 mob-mt-5">Sign up to our FREE 7 Days of Awesome Mobility Series!</h4>
              <mailing-list></mailing-list>
            </div>
            <div class="col-12 text-center mt-5">
              <p class="mb-0 text-footer-grey mb-3"><a href="{{route('tandcs')}}">Terms &amp; Conditions</a> | <a href="{{route('privacy-policy')}}">Privacy Policy</a> <span class="d-none d-md-inline">|</span><br class="d-md-none" /> <a href="{{route('delivery-and-returns')}}">Delivery &amp; Returns</a></p>
              <img data-src="/img/logos/powered-by-stripe.svg" data-srcset="/img/logos/powered-by-stripe.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Powered by Stripe Logo" width="110" height="25" />
              <img data-src="/img/logos/paypal.svg" data-srcset="/img/logos/paypal.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="PayPal Logo" width="86" height="20" />
              <img data-src="/img/logos/mastercard.svg" data-srcset="/img/logos/mastercard.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Mastercard Logo" width="36" height="27" />
              <img data-src="/img/logos/visa.svg" data-srcset="/img/logos/visa.svg" class="img-fluid lazy mob-mb-0 mr-2" alt="Visa Logo" width="47"  height="15"/>
              <img data-src="/img/logos/amex.svg" data-srcset="/img/logos/amex.svg" class="img-fluid lazy mob-mb-0" alt="American Express Logo" width="33" height="33" />
            </div>
          </div>
        </div>
      </footer>
      <div class="copyright text-center">
        <p class="mb-0">&copy; Tom Morrison {{Carbon\Carbon::now()->format('Y')}}. <a href="https://elementseven.co" style="color:#8F9195!important;" target="_blank">Website by Element Seven</a></p>
      </div>
    </div>
    <div id="loader">
      <div class="vert-mid">
        <img id="loader-success" data-src="/img/icons/success.svg" class="d-none mx-auto lazy" width="80" alt="Success icon"/>
        <div id="loader-roller" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <div id="loader-message"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-lg-7 text-center">
                  <p id="loader-second-text" class="mt-3 d-none cursor-pointer larger"><a id="loader-second-link"></a></p>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="col-lg-4">
                  @yield('loader-buttons')
                  <a id="loader-link">
                    <div id="loader-btn" class="btn btn-primary mx-auto d-none mt-3"></div>
                  </a>
                  <div id="close-loader-btn" class="btn btn-primary mx-auto d-none mt-3">Close</div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @yield('modals')
    @if($page != 'Shop' && $page != 'Basket' && $page != 'Checkout' && $page != 'Success' &&  Route::currentRouteName() != 'product' && Route::currentRouteName() != 'smm-yt-lp')

{{-- 
    <div class="modal fade show" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content text-center" style="border: 0 !important;">
          <picture> 
            <source srcset="/img/sales/jennis-birthday-sale.webp" type="image/webp"/> 
            <source srcset="/img/sales/jennis-birthday-sale.jpg" type="image/jpeg"/>
            <img src="/img/sales/jennis-birthday-sale.jpg" class="w-100 d-none d-lg-block" alt="Jenni birthday sale." />
          </picture>
          <picture> 
            <source srcset="/img/sales/jennis-birthday-sale-mob.webp" type="image/webp"/> 
            <source srcset="/img/sales/jennis-birthday-sale-mob.jpg" type="image/jpeg"/>
            <img src="/img/sales/jennis-birthday-sale-mob.jpg" class="w-100 d-lg-none" alt="Jenni birthday sale." />
          </picture>
          <div class="timer-holder d-inline-block">
            <h3 class="text-white"><span class="text-center text-lg-left text-white mob-mb-2 timer-text">Ends in</span> <br class="d-md-none" /><timer ds="2022-08-02T09:00:00+00:00"></timer></h3>
          </div>
          <a href="/shop">
            <button class="btn btn-white xmas-shop-btn" style="color:#FE4078;">Shop Now</button>
          </a>
          <p class="close-modal text-center px-5 mob-px-3 " style="left: calc(50% - 175px);width: 350px;"><a class="close w-100" data-dismiss="modal" aria-label="Close" style="color: #ffffff !important; text-shadow: none !important;"><u>No thanks, continue to site</u></a></p>
        </div>
      </div>
    </div> --}}

    @endif	
    <div class="modal fade" id="videoTestimonialsModal" tabindex="-1" role="dialog" aria-labelledby="videoTestimonialsModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="embed-responsive embed-responsive-9by16">
            <iframe id="videoTestimonialIframe" class="embed-responsive-item" src="" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>  
  </div>
  <img src="/img/icons/close-modal.svg" alt="close modal" width="50" class="close-video-modal"/>
  @if(!isset($nomenu))
  <div id="menu-btn" class="menu_btn d-sm-none">
    <div class="nav-icon">
      <span></span>
      <span></span>
      <span></span>   
    </div> 
  </div>
  <div id="shopping-cart" class="d-md-none">
    @if($gifttest == false)
    <a class="@if($page == 'light') text-white @endif" href="{{route('basket')}}"><i class="fa fa-shopping-cart"></i>
      @if(Cart::count())
      <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
      @endif
    </a>
    @else
    <a class="@if($page == 'light') text-white @endif" href="{{route('gift-basket')}}"><i class="fa fa-gift"></i>
      @if(Cart::count())
      <div class="basket-count" style="position: absolute;top:3px;right:-2px;border-radius: 100%;background-color: #D82737;color:#fff;text-align:center;width:18px;height:18px;font-size:12px;cursor:pointer;">{{Cart::count()}}</div>
      @endif
    </a>
    @endif
  </div>
  <div id="menubtntrigger"></div>
  @endif
  
  <div id="mobile-menu">
    <img data-src="/img/logos/logo-white.svg" class="mobile-menu-logo lazy" alt="Tom Morrison logo white"/>
    <div class="menu-inner">
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Homepage") underline @endif " href="/">Home</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Shop") underline @endif " href="{{route('shop')}}">Shop</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Merch") underline @endif " href="{{route('merch')}}">Merch</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Seminars") underline @endif " href="{{route('seminars')}}">Seminars</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Facts") underline @endif " href="{{route('facts')}}">Tom Facts</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "About") underline @endif " href="{{route('about')}}">About Us</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Blog") underline @endif " href="{{route('blog')}}">Blog</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Reviews") underline @endif " href="{{route('simplistic-mobility-method-reviews')}}">Reviews</a>
        </p>
      </div>
      <div class="nav-item">
        <p class="mb-0">
          <a class="nav-link @if($pagename == "Contact") underline @endif " href="{{route('contact')}}">Get In Touch</a>
        </p>
      </div>
      <div class="menu-inner-bottom">
        <div class="row">
          <div class="col-5 text-left">
            <a href="{{route('login')}}">
              <button class="btn btn-white-line">Login</button>
            </a>
          </div>
          <div class="col-7">
            <p class="text-right">
              <a href="https://www.facebook.com/Movementandmotion" target="_blank" rel="noopener" aria-label="Facebook"><span class="social-circle"><i class="fa fa-facebook"></i></span></a>
              <a href="https://www.tiktok.com/@tom_morrison" target="_blank" rel="noopener" aria-label="TikTok"><span class="social-circle"><img src="/img/icons/tiktok-brands.svg" width="18" height="18" alt="tom morrison tiktok"/></span></a>
              <a href="https://www.instagram.com/tom.morrison.training" target="_blank" rel="noopener" aria-label="Instagram"><span class="social-circle"><i class="fa fa-instagram"></i></span></a>
              <a href="https://www.youtube.com/channel/UC1bHlccT8JOMAWm5wMuzG9A" target="_blank" rel="noopener" aria-label="YouTube"><span class="social-circle"><i class="fa fa-youtube-play"></i></span></a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="orientation_change">
    <p>This website is best experienced in portrait mode, please rotate your device to continue.</p>
    {{-- <img width="200" src="/img/icons/mobile-rotate.png" alt="logo"> --}}
  </div>
  <style>@import url('https://fonts.googleapis.com/css?family=Karla:400,700|Oswald:500,700&display=swap');</style>
  <!-- Scripts -->
  <script src="{{ mix('/js/manifest.js') }}"></script>
  <script src="{{ mix('/js/vendor.js') }}"></script>
  <script src="{{ mix('js/app.js') }}"></script>
  @yield('scripts')
  <script>
    window.addEventListener("load", function(){
       window.cookieconsent.initialise({
         "palette": {
           "popup": {
             "background": "#D82737",
             "text": "#ffffff"
           },
           "button": {
               "background": "#ffffff",
               "text": "#D82737",
               "border-radius": "0"
           },
        
        },
        "content": {
          "message": "This website uses cookies to ensure you get the best experience possible!",
          "dismiss": "Got it!",
          "link": "Learn more",
          "href": "http://cookies.insites.com/about-cookies"
        }
    })});
  </script>
  <script>
    $('.close-video-modal').click(function(){
      $('#videoTestimonialIframe').attr('src', '');
      $('#videoTestimonialsModal').modal('hide');
    });
  </script>
{{--    @if($page != 'Shop' && $page != 'Basket' && $page != 'Checkout' && $page != 'Success' && $page != 'Claim Your Gift' && Route::currentRouteName() != 'product' && Route::currentRouteName() != 'smm-yt-lp' && Session::get('val-pop-3') != 'shown')
  @php session(['val-pop-3' => 'shown']); @endphp

  <script>  
    $('#saleModal').modal('show');
  </script>
 
  @endif

   <script>  
    $('#saleModal').modal('show');
  </script>

@if($page == 'smm-usa' || $page == 'smm-aus')

  <script>
    $('#menu-btn').css('top','6rem');
    var inview2 = new Waypoint({
      element: $('#menubtntrigger'),
      handler: function (direction) {
        if(direction === "down"){
          $('.menu_btn').css('top','1rem');
          console.log('down');
        }else{
          $('.menu_btn').css('top','6rem');
          console.log('up');
        }
      },offset: '100%'
    });

    
  </script> 

@endif
 <script>
  $('.close-modal').click(function(){
      $('#saleModal').modal('hide');
    });

  
    $('#menu-btn').css('top','9.4rem');
    $('#shopping-cart').css('top','9rem');
    var inview2 = new Waypoint({
      element: $('#menubtntrigger'),
      handler: function (direction) {
        if(direction === "down"){
          $('#menu-btn').css('top','1rem');
          $('#shopping-cart').css('top','0.75rem');
        }else{
          $('#menu-btn').css('top','8rem');
          $('#shopping-cart').css('top','7.75rem');
        }
      },offset: '100%'
    });

  </script> --}}

  

</body>
</html>