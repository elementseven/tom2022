@php
$page = "Homepage";
$pagename = "Homepage";
$pagetitle = "Tom Morrison - Movement | Mobility | Strength";
$meta_description = "Tom Morrison is the creator of the Simplistic Mobility Method, and specialises in correcting dysfunctional movement. Tom’s online mobility programs improve shoulder mobility, hip mobility, thoracic mobility, ankle mobility, and build core strength while teaching you about your body.";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('styles')
<style>
.no-webp .new-home-smm{background-image:url('/img/home/new-home-smm-2.jpg')}.no-webp .new-home-bb{background-image:url('/img/home/new-home-bb.jpg?v=2024-07-24')}.webp .new-home-smm{background-image:url('/img/home/new-home-smm-2.webp')}.webp .new-home-bb{background-image:url('/img/home/new-home-bb.webp?v=2024-07-24')}#home-header .full-height-minus-menu{height:calc(100vh - 96px)}.home-vimeo .vlt-preview:before{height:0!important;padding-bottom:0!important;display:none!important}#home-header .or{position:absolute;left:-27px;top:calc(50% - 20px);width:54px;height:40px;background-color:#fff;border-radius:3px;color:#000;font-size:23px;line-height:23px;font-weight:700;text-align:center;padding-top:9px}#home-header .special-offer{display:inline-block;text-align:center;width:160px;height:33px;font-weight:900;background-color:#D82737;color:#ffffff;padding-top:3px;border-radius:15px}.owl-carousel .owl-stage-outer{overflow:visible!important}.home-bg-splash{position:relative;background-image:url('/img/sales/valentines/home-bg.jpg');background-size:cover;background-repeat:no-repeat}.webp .home-bg-splash{background-image:url('/img/sales/valentines/home-bg.webp')}@media only screen and (min-device-width :768px) and (max-device-width :1024px) and (orientation :portrait){#home-header .or{top:50%}}@media only screen and (max-width :767px){.no-webp .home-bg-splash{background-image:url('/img/sales/valentines/home-bg-mob.jpg')}.webp .home-bg-splash{background-image:url('/img/sales/valentines/home-bg-mob.webp')}.no-webp .new-home-smm{background-image:url('/img/home/new-home-smm-2-mob.jpg')}.no-webp .new-home-bb{background-image:url('/img/home/new-home-bb-mob.jpg?v=2024-07-24')}.webp .new-home-smm{background-image:url('/img/home/new-home-smm-2-mob.webp')}.webp .new-home-bb{background-image:url('/img/home/new-home-bb-mob.webp?v=2024-07-24')}#home-header .full-height-minus-menu{height:calc(50vh - 48px);max-height:500px}#home-header p.mimic-h2{font-size:8vw;line-height:1.4}#home-header p.mimic-h2 .tm{font-size:5vw;top:-14px}#home-header .or{top:-20px;left:calc(50% - 20px)}}
</style>	
@endsection
@section('preloads')
<link rel="preload" media="(max-width: 767px)" as="image" href="/img/home/new-home-smm-2-mob.webp">
<link rel="preload" media="(max-width: 767px)" as="image" href="/img/home/new-home-bb-mob.webp?v=2024-07-24">
@endsection
@section('header')
<header id="home-header" class="container-fluid text-center">
	<div class="row">
		<div class="col-md-6 full-height-minus-menu new-home-smm bg">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 h-100">
					<p class="text-white mb-0"><i>I want to improve my mobility:</i></p>
					<p class="mimic-h2 text-white">The Simplistic<br class="d-md-none" /> Mobility Method<span class="tm">&reg;</span></p>
					<a href="/products/the-simplistic-mobility-method">
						<button type="button" class="btn btn-white-outline mx-auto">Find out more <i class="fa fa-angle-double-right ml-2"></i></button>
					</a>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-6 full-height-minus-menu home-bg-splash bg position-relative">
			<div class="d-table w-100 h-100 position-relative z-2">
				<div class="d-table-cell align-middle w-100 h-100">
					<div class="special-offer mb-2 mx-auto" style="background-color: #905125 !important;color: #FFF !important;">Save 10%</div>
					<p class="mimic-h1 lh-1" style="color:#FFFFFF !important;text-shadow: rgba(30, 50, 25, 0.5) 0px 2px 4px;">Valentine's Sale Now On!</p>
					<a href="/shop">
						<button type="button" class="btn btn-primary mx-auto border-0" style="background-color:#321E19 !important;color: #FFF !important;">Shop Now <i class="fa fa-angle-double-right ml-2"></i></button>
					</a>
				</div>
			</div>
			<div class="or shadow">OR</div>
		</div> -->
		<div class="col-md-6 full-height-minus-menu new-home-bb bg position-relative">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 h-100">
					<div class="special-offer mb-2 mx-auto">Save 15%</div>
					<p class="text-white mb-0"><i>I want SMM + bonus programs & inspiration:</i></p>
					<p class="mimic-h2 text-white">Beginner's Bundle</p>
					<a href="/products/beginners-bundle">
						<button type="button" class="btn btn-white-outline mx-auto">Find out more <i class="fa fa-angle-double-right ml-2"></i></button>
					</a>
				</div>
			</div>
			<div class="or">OR</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<home-page></home-page>
@endsection