@php
$page = '503';
$pagename = 'Error';
$pagetitle = "503 - Scheduled Maintenance - Tom Morrison";
$meta_description = "Our website is currently undergoing scheduled maintenance, we'll be back shortly.";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])
@section('content')
<main class="container bg-white position-relative">
	<div class="row">
		<div class="col-12">
			<div class="row mt-5">
				<div class="col-md-6">
					<h1 class="page-title title-404 mb-4 mt-5">Down for Maintenance</h1>
					<p class="mb-2	 text-large mob-mb-3"><b>Our website is currently undergoing scheduled maintenance, we'll be back shortly.</b></p>
					<p class="mb-4">You can still use all your programs, just click the button below!</p>
					<a href="https://app.tommorrison.uk/login" target="_blank">
						<div class="btn btn-primary d-block d-md-inline-block mob-mb-3 mr-2">Login</div>
					</a>
				</div>
				<div class="col-md-5">
					<picture> 
						<source srcset="/img/tom-morrison-404.webp" type="image/webp"> 
						<source srcset="/img/tom-morrison-404.jpg" type="image/jpeg">
						<img src="/img/tom-morrison-404.jpg" class="tom-morrison-404 img-fluid" alt="Tom morrison wasn't expecting you" />
					</picture>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection