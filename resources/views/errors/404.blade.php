@php
$page = '404';
$pagename = 'Error';
$pagetitle = "404 - Page not found - Tom Morrison";
$meta_description = "Oh, I wasn't expecting you! Maybe one of these pages will be more helpful.";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename])

@section('content')
<main class="container bg-white position-relative">
	<div class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-md-6">
					<h1 class="page-title title-404 mb-5 mt-5">Oh, I wasn't expecting you!</h1>
					<div class="row">
						<div class="col-md-8">
							<p class="mb-4 text-large mob-mb-3"><b>Maybe one of these pages will be more helpful:</b></p>
						</div>
					</div>
					<a href="{{route('shop')}}">
						<div class="btn btn-primary d-block d-md-inline-block mob-mb-3 mr-2">Shop</div>
					</a>
					<a href="{{route('blog')}}">
						<div class="btn btn-primary d-block d-md-inline-block">Blog</div>
					</a>
				</div>
				<div class="col-md-5">
					<picture> 
		                <source srcset="/img/tom-morrison-404.webp" type="image/webp"> 
		                <source srcset="/img/tom-morrison-404.jpg" type="image/jpeg">
		                <img src="/img/tom-morrison-404.jpg" class="tom-morrison-404 img-fluid" alt="Tom morrison wasn't expecting you" />
		            </picture>
		        </div>
			</div>
        </div>
	</div>
</main>
@endsection