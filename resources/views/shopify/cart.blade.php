@php
$page = "Merch Cart";
$pagename = 'Merch Cart';
$pagetitle = "Cart - Merchandise - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison Merchandise";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('head_section')
{{-- <script>
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'GBP',
    'impressions': [
        @foreach($products as $key => $product)
       {
          "id": "{{$product->id}}",
         'sku': "{{$product->id}}",
         'name': "{{$product->name}}",
         'brand': 'Tom Morrison',
         'category': "{{$product->category->name}}",
         'price': '{{$product->price}}',
         'position': {{$key + 1}}
       } @if($key != count($products) - 1) , @endif
       @endforeach
    ]
  }
});
</script> --}}
@endsection
@section('header')
<header class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="text-primary checkout-title mob-mt-3 pt-lg-5 pt-3 pb-2 mob-mb-0 mob-pb-0">Basket<span class="checkout-title-line"></span><span class="text-light-grey">Checkout</span><span class="checkout-title-line lighter"></span><span class="text-light-grey">Success!</span></h1>
			<hr class="dark-line my-4">
		</div>
	</div>
</header>
 @endsection
@section('content')
<shopify-cart></shopify-cart>
<shopify-other-products></shopify-other-products>
@endsection