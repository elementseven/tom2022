@php
$page = "Merch";
$pagename = 'Merch';
$pagetitle = "Merchandise - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison Merchandise";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('head_section')
{{-- <script>
dataLayer.push({
  'ecommerce': {
    'currencyCode': 'GBP',
    'impressions': [
        @foreach($products as $key => $product)
       {
          "id": "{{$product->id}}",
         'sku': "{{$product->id}}",
         'name': "{{$product->name}}",
         'brand': 'Tom Morrison',
         'category': "{{$product->category->name}}",
         'price': '{{$product->price}}',
         'position': {{$key + 1}}
       } @if($key != count($products) - 1) , @endif
       @endforeach
    ]
  }
});
</script> --}}
@endsection
@section('header')
<div class="container-fluid">
    <div class="row">
      <div class="container top-section top-shop">
        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-md-4">
                <div class="d-table w-100">
                  <div class="d-table-cell align-middle">
                    <h1 class="page-title mb-3 mob-mt-3 pb-0">Merchandise</h1>
                    <p class="mt-3"><b>Check out our range of clothing and other merchandise!<br class="d-none d-lg-inline"/> Looking for our digital programs? <a href="{{route('shop')}}">Shop here!</a></b></p>
               {{--      <div class="bg-grey my-3 p-3 border-1">
                        <p class="mb-1"><b>Please be prepared for delays</b></p>
                        <p class="mb-0">Merch is still being sent out, but our fulfillment & delivery times are currently longer than usual due to disruption caused by Covid-19. (Does not affect our digital products)</p>
                    </div> --}}
                  </div>
                </div>
              </div>
            </div>
            <picture> 
              <source srcset="/img/shop/jenni-merch.webp" type="image/webp"> 
              <source srcset="/img/shop/jenni-merch.jpg" type="image/jpeg">
              <img src="/img/shop/jenni-merch.jpg" class="shop-top-image d-none d-lg-block" alt="Jenni Merch" />
            </picture>
          </div>
        </div>
      </div>
    </div>
  </div>
 @endsection
@section('content')
<shopify-index></shopify-index>
@endsection