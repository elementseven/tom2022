@php
$page = "Merch";
$pagename = 'Merch';
$pagetitle = "Merchandise - Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Buy Tom Morrison Merchandise";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])

@section('content')
  <div id="app">
    <shopify-product :product="{{ json_encode($product) }}"></shopify-product>
  </div>
@endsection

@section('scripts')
  <script src="{{ mix('js/app.js') }}"></script>
@endsection
