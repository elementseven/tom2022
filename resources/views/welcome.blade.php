@php
$page = "Homepage";
$pagename = "Homepage";
$pagetitle = "Movement | Mobility | Strength - Tom Morrison";
$meta_description = "Tom Morrison is the creator of the Simplistic Mobility Method, and specialises in correcting dysfunctional movement. Tom’s online mobility programs improve shoulder mobility, hip mobility, thoracic mobility, ankle mobility, and build core strength while teaching you about your body.";
$og_image = "https://tommorrison.uk/img/og.jpg";
@endphp

@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'getstarted' => true, 'pagename' => $pagename])
@section('header')
<header class="container-fluid">
	<div class="row">
		<div class="container home-top-container">
			<div class="row">
				<div class="col-md-8 mb-3">
					<picture> 
						<source srcset="/img/home/tom-morrison-homepage1.webp" type="image/webp"/> 
						<source srcset="/img/home/tom-morrison-homepage1.jpg" type="image/jpeg"/>
						<img src="/img/home/tom-morrison-homepage1.jpg" width="730" height="326" class="img-fluid home-top-img" alt="Tom morrison looking down over the rest of the website." />
					</picture>
				</div>
				<div class="col-md-6">
					<h1 class="home-top-title text-primary">LEARN<br>TO MOVE<br>BETTER</h1>
				</div>
				<div class="col-md-6 testimonials_slider_holder">
					<div class="testimonials_slider">
						<li style="list-style: none;">
							<div class="container-fluid testimonial_card p-5">
								<div class="row">
									<div class="col-md-8">
										<hr class="testimonial_line mt-0">
									</div>
									<div class="col-12">
										<p class="testimonial_text"><b>“No more endless foam rolling, band distractions or stretches that do nothing for the ROM - only pure quality movement. I would and am recommending Tom to everyone I know!”</b></p>
									</div>
								</div>
								<div class="testimonial_name">Juho Kolehmainen</div>
							</div>
						</li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-1-1 bg-top"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-1-2 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-1-3 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-1-4 bg"></div></li>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pt-5">
	<div class="row py-5 mob-py-0">
		<div class="col-lg-5 mb-5">
			<div class="intro-paragraph">
				<p class="scroll_fade" data-fade="fadeIn">After years of feeling like I was working against my own body, I now have so much fun every day and continue to learn and try as much as I can. It is my goal to help others do the same.</p>
				<p class="scroll_fade" data-fade="fadeIn">My programs are designed to not only bring progress, but to bring consistency. When you enjoy what you do, you actually want to do it - simple, right?</p>
				<p class="mob-pb-5 scroll_fade" data-fade="fadeIn">The <a href="/mobility-program" class="text-dark">mobility</a> &amp; strength exercises I’ve included I’ve tried and tested through years of experience and found them to be the best for keeping your body functioning well so that you can move without restriction and be able to enjoy learning skills and getting stronger every day. I am also incredibly nice to look at, which is a bonus.</p>
			</div>
		</div>
		<div class="col-lg-6 offset-lg-1">
			<div class="want-box bg-primary text-center p-5">
				<h2 class="text-white scroll_fade" data-fade="fadeIn">Want Mobility?</h2>
				<p class="text-white scroll_fade" data-fade="fadeIn">You need the <b><a href="/the-simplistic-mobility-method" class="text-white">Simplistic Mobility Method</a></b></p>
				<a href="/products/the-simplistic-mobility-method">
					<div class="btn btn-white m-auto scroll_fade" data-fade="fadeInUp">Find out more</div>
				</a>
			</div>
			<div class="want-box bg-dark-grey text-center p-5">
				<h2 class="text-white scroll_fade" data-fade="fadeIn">WANT STRENGTH?</h2>
				<p class="text-white scroll_fade" data-fade="fadeIn">You need <b>End Range Training</b></p>
				<a href="/products/end-range-training">
					<div class="btn btn-white m-auto scroll_fade" data-fade="fadeInUp">Find out more</div>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg-light py-5 mob-mt-5">
	<div class="row">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<img data-src="/img/home/tm-logos-desktop.svg" class="d-none d-sm-block img-fluid lazy scroll_fade" data-fade="fadeIn" alt="Verified logos" width="919" height="92">
					<img data-src="/img/home/tm-logos-mobile.svg" class="d-sm-none w-100 lazy scroll_fade" data-fade="fadeIn" alt="Verified logos" width="345" height="167">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid pt-5 mt-5">
	<div class="row">
		<video-testimonials :amount="20"></video-testimonials>
	</div>
</div>
<div class="container py-5 mt-5 mob-mt-0">
	<div class="row">
		<div class="col-md-5 mob-mb-5">
			{{-- <picture> 
				<source srcset="/img/home/tom-morrison-homepage2.webp" type="image/webp"/> 
				<source srcset="/img/home/tom-morrison-homepage2.jpg" type="image/jpeg"/>
				<img src="/img/home/tom-morrison-homepage2.jpg" class="img-fluid home-testimonials-img" alt="Tom morrison holding chin looking to the sky." />
			</picture> --}}
			<div class="testimonials_slider ">
				<li style="list-style: none;">
					<div class="container-fluid testimonial_card bg-primary p-5">
						<div class="row">
							<div class="col-md-8">
								<hr class="testimonial_line mt-0">
							</div>
							<div class="col-12">
								<p class="testimonial_text"><b>“Love Tom’s stuff. Very helpful and highly recommended. He isn’t just speaking from theory but also from experience which is invaluable. Top class.”</b></p>
							</div>
						</div>
						<div class="testimonial_name">Stuart Thompson</div>
					</div>
				</li>
				<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-2-1 bg"></div></li>
				<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-2-2 bg"></div></li>
				<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-2-3 bg"></div></li>
				<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-2-4 bg"></div></li>
			</div>
		</div>
		<div class="col-md-7">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe src="https://player.vimeo.com/video/329333755" width="640" height="360" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg-light py-5 mob-pt-0">
	<div class="row py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-xl-4 mob-mb-5">
					<div class="seminars-intro">
						<p class="scroll_fade" data-fade="fadeIn">The Movement & Mobility seminar covers mobility, stability and how to increase your range of motion with progressions that lead to advanced skills and strength. You’ll learn how to get the most out of your strength training and avoid injury and how to teach your body to adapt with real techniques that will last you a lifetime.</p>
						<div class="btn btn-primary mt-5 scroll_fade" data-fade="fadeInUp" data-toggle="modal" data-target="#request_seminar_modal">Request a Seminar</div>
					</div>
				</div>
				<div class="col-md-7 col-xl-7 offset-xl-1">
					<div class="testimonials_slider">
						<li style="list-style: none;">
							<div class="container-fluid testimonial_card p-5">
								<div class="row">
									<div class="col-md-8">
										<hr class="testimonial_line mt-0">
									</div>
									<div class="col-12">
										<p class="testimonial_text"><b>“I considered myself quite knowledgeable regarding mobility, but the things I learnt at the seminar about daily rituals and mobility fixes have the potential to quite literally change my life. All of this in one afternoon!”</b></p>
									</div>
								</div>
								<div class="testimonial_name">Mark Coyle</div>
							</div>
						</li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-3-1 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-3-2 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-3-3 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-3-4 bg"></div></li>
						<li style="list-style: none;"><div class="home-testimonial-1 home-testimonial-3-5 bg"></div></li>
					</div>
				</div>
			</div>
			<div class="tom-lying-down-spacer"></div>
		</div>
	</div>
</div>
<div class="container z-3 position-relative">
	<div class="row">
		<div class="col-md-12">
			<picture> 
				<source data-srcset="/img/home/tom-morrison-homepage3.webp" type="image/webp"/> 
				<source data-srcset="/img/home/tom-morrison-homepage3.png" type="image/png"/>
				<img data-src="/img/home/tom-morrison-homepage3.png" data-srcset="/img/home/tom-morrison-homepage3.png" id="tom-lying-down" class="img-fluid lazy scroll_fade" data-fade="fadeIn" alt="Tom morrison lying down." width="600" height="367" />
			</picture>
		</div>
	</div>
</div>
<div class="container pb-5 mob-pb-0">
	<div class="row pb-5 justify-content-center">
		<div class="col-md-12 text-center">
			<h2 class="mb-5">RECENT BLOGS</h2>
		</div>
		<div class="col-md-10 mob-px-0">
			<div class="blog-slider">
				@foreach($blogs as $blog)
				<li class="blog-slider-post lazy" data-bg="url('/storage/{{$blog->photo}}')">
					<div class="blog-text">
						<h3 class="mb-3">{{substr($blog->title,0,36)}} @if(strlen($blog->title) > 36) <span class="caption">[...]</span> @endif</h3>
						<p>{{substr($blog->exerpt,0,250)}} […] </p>
						<a href="/blog/{{$blog->slug}}">
							<div class="read-more">Read more</div>
						</a>
					</div>
				</li>
				@endforeach
			</div>	
			<p class="mb-0 text-center"><a href="{{route('blog')}}"><b>View all blogs</b> &nbsp; <i class="fa fa-angle-right"></i></a></p>
		</div>
	</div>
</div>
@endsection
@section('scripts')

@endsection
@section('modals')
<request-seminar :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></request-seminar>
@endsection