@php
$pagename = 'LP';
$page = 'Stability Builder';
$pagetitle = "Stability Builder - Tom Morrison";
$meta_description = "Unique, high-quality, follow along video workouts to build an unbreakable body";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'nomenu' => true, 'pagename' => $pagename])
@section('header')
<header class="container mt-5 mob-mt-3">
	<div class="row justify-content-center text-center">
		<div class="col-md-12 mb-3">
			<img src="/img/lps/stability-builder.svg" alt="Stability Builder Title" width="628" height="99" class="h-auto d-block mx-auto mw-100"/>
		</div>
		<div class="col-12 text-c">
			<p class="mb-3" style="font-size: 1.25rem;"><b>Unique, high-quality, follow along video workouts to build an <span class="text-primary">unbreakable body</span></b></p>	
			<p class="mb-4">The most efficient stability accessory program.<br class="d-none d-lg-inline" />(From the team that brought 35,000+ people The Simplistic Mobility Method&reg;)</p>
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
			<br/>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-10 text-center my-5 pb-4 mob-pb-0">
			<picture>
				<source src="/img/lps/stability-builder/squares-1.webp" type="image/webp"/>
				<source src="/img/lps/stability-builder/squares-1.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-1.jpg" width="898" height="200" class="w-100 h-auto d-none d-lg-block mx-auto mw-100"  alt="Tom Morrison Stability Builder">
			</picture>
			<picture>
				<source src="/img/lps/stability-builder/squares-1-mob.webp" type="image/webp"/>
				<source src="/img/lps/stability-builder/squares-1-mob.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-1-mob.jpg" width="898" height="200" class="w-100 h-auto d-lg-none mx-auto mw-100"  alt="Tom Morrison Stability Builder">
			</picture>
		</div>
		<div class="col-lg-8">
			<p class="larger">Let’s face it, you know you should be doing more stability & mobility exercises, assistance work and the prehab accessory exercises that will fill in the gaps that you KNOW you’re missing in your training.</p>

			<p class="larger">(and are the root cause of the injuries that send your hard work backwards after simply doing normal training exercises)</p>

			<p class="larger">But… often you spend more time researching what to do, wondering if you’re doing it right, or wondering if you’re even doing the right thing.</p>

			<p class="larger mb-4"><b>Stability Builder™</b> gives you everything you need to be doing in clear, follow along workouts so you can just press play and know you’re not missing anything out.</p>

			<h3 class="text-primary">Every basis covered.</h3>
		</div>	

		<div class="col-lg-10 my-5 mob-mb-0 mob-px-0">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BXJ5aWdXmdg" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<div id="what-do-you-get" class="container my-5 pt-5 mob-my-0 mob-pt-0">
	<div class="row justify-content-center mb-5 justify-content-center">
		<div class="col-md-12 text-center">
			<h2 class="mb-5 mob-mb-3 large-title text-capitalize text-center">What Do You Get?</h2>
		</div>
		<div class="col-lg-7 pr-5 mob-px-3 mob-mb-4">
			<div class="row mb-4">
				<div class="col-2 mob-pr-0">
					<img src="/img/lps/icons/play.svg" class="img-fluid lazy" alt="Stability Builder icons play" />
				</div>
				<div class="col-10">
					<p class="pt-2 mob-pt-0">11 high quality workout follow along videos, from 10-30 minutes long that you can do at home or in the gym.</p>
				</div>
			</div>
			<div class="row mb-4">
				<div class="col-2 mob-pr-0">
					<img src="/img/lps/icons/doc.svg" class="img-fluid lazy" alt="Stability Builder icons doc" />
				</div>
				<div class="col-10">
					<p class="pt-2 mob-pt-0">A PDF workbook containing the workouts with space for notes to discover weaknesses & eliminate them forever!</p>
				</div>
			</div>
			<div class="row">
				<div class="col-2 mob-pr-0">
					<img src="/img/lps/icons/lightbulb.svg" class="img-fluid lazy" alt="Stability Builder icons lightbulb" />
				</div>
				<div class="col-10">
					<p class="pt-2 mob-pt-0">Individual PDFs per workout breaking down the movements, regressions & benefits.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-5 bg-light py-5 px-5">
			<h3 class="mb-4 text-primary">Plus FREE Bonus Material:</h3>
			<p><b class="text-primary">BONUS #1:</b> Common Movement Mistakes guide + how to fix them</p>
			<p class="mb-0"><b class="text-primary">BONUS #2:</b> 24/7 access & support from our online support group</p>
		</div>
		<div class="col-12 mt-5 text-center">
			<p><b>All inside an online dashboard that you can access anywhere from any device!</b></p>
		</div>
		<div class="col-lg-7 mt-4 text-center">
			<picture>
				<source src="" type=""/>
				<source src="" type=""/>
				<img src="/img/lps/stability-builder/devices-mock.jpg" alt="Stability Builder Devices" width="608" width="405" class="w-100 h-auto"/>
			</picture>
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block mt-5">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
		</div>
		<div class="col-lg-10 text-left mt-5 mob-pt-4">
			<div class="review-box bg-light pt-4 pb-5 px-5 mob-px-3 mb-3">
				<div class="review-avatar">
					<picture> 
						<source srcset="/img/lps/stability-builder/giorgia-choudhary.webp" type="image/webp"/> 
						<source srcset="/img/lps/stability-builder/giorgia-choudhary.jpg" type="image/jpeg"/>
						<img src="/img/lps/stability-builder/giorgia-choudhary.jpg" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Stability Builder Giorgia Choudhary testimonial" />
					</picture>
				</div>
				<p class="review-name title text-large mob-text-small mb-5 mob-mb-3">Giorgia Choudhary<br/>@for($x = 1; $x<= 5; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</p>
				<div class="review-body"> 
					<p class="text-large mb-0">"I really like the no-nonsense, yet charismatic and energetic approach to teaching of Tom and Jenni. These guys really know what they are talking about!"</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="how-to-fit-it-in" class="container my-5 pt-5 mob-my-0 mob-pt-0">
	<div class="row justify-content-center mb-5 justify-content-center">
		<div class="col-12 text-center">
			<h2 class="mb-4 mob-mb-3 large-title text-capitalize text-center">How To Fit It In</h2>
			<img src="/img/lps/stability-builder/circles.svg" class="mw-100 h-auto" width="419" height="118" alt="Stability Builder Stats"/>
		</div>
		<div class="col-lg-8 mt-5 mob-mt-4">
			<p class="larger">Each workout is less than 30 minutes long – some less than 15 minutes!</p>

			<p class="larger">All you need to do is bolt on 1 or 2 Stability Builder workouts to your training per week, either at home or at the gym, and you’ll KNOW you’re doing enough of the good stuff to just enjoy your training.</p>

			<p class="larger">Every exercise in Stability Builder™ is carefully selected for specific reasons to complement how your joints need to move, while teaching you how to build proper tension from all the angles you need to be strong in.</p>
		</div>

	</div>
</div>
<div id="the-workouts" class="container-fluid bg-light">
	<div class="row">
		<div class="container my-5 mob-mb-0">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<h2 class="text-primary large-title text-capitalize mb-4">What Are The Workouts?</h2>
				</div>
				<div class="col-lg-8">
					<div id="workout-accordion">
						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Isometric Tension <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-1" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-isometric-tension.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-isometric-tension.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-isometric-tension.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder isometric tension" />
							</picture>
							<p class="mt-3">You won’t have seen a workout like this before. Isometric tension shows you how to progressively ramp up tension to improve your mind/muscle awareness so that when it comes to normal training, your body feels stable and can fire the right muscles effectively!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">Rotation & Balance <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-2" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-rotation-balance.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-rotation-balance.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-rotation-balance.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout rotation balance" />
							</picture>
							<p class="mt-3">Rotation and Balance covers all the ways your spine needs to move so stay strong & healthy, plus how to breathe properly when working on your spine mechanics. This is combined with balance work - the biggest secret to better back, hip and core strength is balance!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">Toebility & Ankles <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-3" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-toebility.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-toebility.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-toebility.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout toebility" />
							</picture>
							<p class="mt-3">Your feet are your foundation. Whether you’re a runner or a lifter it doesn’t matter, you NEED strong feet and mobile ankles to avoid common issues like plantar fasciitis, sprains, or tears. Not only will this workout improve your ankle dorsiflexion, but it will also build up other, less common ranges.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">Coordination & Plyometrics <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-4" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-coord-plyo.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-coord-plyo.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-coord-plyo.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Coordination & Plyometrics" />
							</picture>
							<p class="mt-3">Jumping, landing, and being coordinated are all things to develop for true athleticism, or if you just want to feel like you have a spring in your step! Learning how to absorb your own force and land without rattling your own bones is crucial to powerful plyometric strength.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">Freestanding Resistance Band <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-5" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-free-band.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-free-band.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-free-band.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Freestanding Resistance Band" />
							</picture>
							<p class="mt-3">Resistance bands are an essential tool to improve your stability and muscular endurance. This workout is how to use them properly (not just curls & squats like its leaflet demonstrates). High rep resistance band work through large ranges of motion is one of the best ways to avoid muscle tightness.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">Attached Resistance Band <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-6" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-attached-band.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-attached-band.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-attached-band.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Attached Resistance Band" />
							</picture>
							<p class="mt-3">Not only can you do amazing things with just the band, when you anchor it properly you have the most ultimate full body & core activation device in the world! You will be hard-pressed to find these techniques combined in this way anywhere else!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">Unilateral Stability <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-7" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-uni-stab.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-uni-stab.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-uni-stab.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Attached Resistance Band" />
							</picture>
							<p class="mt-3">If you predominantly use barbells, they can mask imbalances you may have. Unilateral Stability is not only incredibly fun, but it also assesses you during the workout, quickly highlighting where you fatigue fastest or struggle the most. Your body can’t cheat or compensate in this workout!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">Super Duper Sets <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-8" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-super-duper.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-super-duper.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-super-duper.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Super Duper Sets" />
							</picture>
							<p class="mt-3">Tom’s unique way of combining mobility into workouts, incorporating both weighted & unweighted exercises. Super Sets combine multiple complementary exercises to increase your workload, Drop Sets use the same movement/muscle but drop weight to achieve more reps… Super Duper Sets do both! This workout can be challenging at first but when you do it regularly, you’ll find your technique and strength in other training soar.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-9" role="button" aria-expanded="false" aria-controls="collapse-9">Lower Back & Core Builder <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-9" data-parent="#workout-accordion">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-lower-back.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-lower-back.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-lower-back.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout Lower Back & Core Builder" />
							</picture>
							<p class="mt-3">To make your lower back feel better, you should address your hips, core & balance – which is exactly what this workout does. If you are prone to recurring lower back issues, this workout can even be done during a flare up and will help you to overcome it faster. The lower back is a complex fellow and needs a lot of support from everything surrounding it to make real improvement.</p>              
						</div>
					</div>
				</div>
				<div class="col-lg-8 mob-px-0">
					<div class="border-2 p-4 mt-4 mob-px-3">
						<p>On top of these 9 incredible workouts you also get:</p>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-10" role="button" aria-expanded="false" aria-controls="collapse-10">A Full Body Warm Up <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-10">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-warm-up.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-warm-up.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-warm-up.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout A Full Body Warm Up" />
							</picture>
							<p class="mt-3">This is based on my own warm ups and morning routines teaching you how to prepare your body and work through loads of joint movement in under 10 minutes!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-11" role="button" aria-expanded="false" aria-controls="collapse-11">BONUS TRX/Rings Follow Along <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-11">
							<picture> 
								<source srcset="/img/lps/stability-builder/workout-rings.webp" type="image/webp"/> 
								<source srcset="/img/lps/stability-builder/workout-rings.jpg" type="image/jpeg"/>
								<img src="/img/lps/stability-builder/workout-rings.jpg" class="w-100 h-100 mt-2 lazy" width="540" height="303" alt="Stability Builder workout BONUS TRX/Rings Follow Along" />
							</picture>
							<p class="mt-3">If you have access to these amazing pieces of equipment, then hold on to your pants because we’re going to show some incredible things for your hips and shoulders! There is SO MUCH MORE than dips and rows!</p>              
						</div>
					</div>
					<div class="text-center mt-5">
						<a href="/basket/add/17">
							<button type="button" class="btn btn-primary d-inline-block">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container my-5 py-4 mob-py-0">
	<div class="row justify-content-center">
		<div class="col-12 text-center mb-5">
			<h2 class="text-primary large-title text-capitalize">Break The Injury Cycle For Good</h2>
			<h3>(Never See Months of Hard Work Deleted in an Instant Ever Again)</h3>
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block d-lg-none mt-3">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
		</div>
		<div class="col-lg-8">
			<p class="larger mb-4">When I first started my journey with strength and fitness there were so many fundamental things that I missed. I started getting stuck in the cycle of train… injury… rehab… train… injury… rehab…</p>
			<p class="larger mb-4">This happened again and again and again and again.</p>
			<p class="larger mb-4">In fact, this same problem was happening to people all around me. When I became a Coach and gained more experience, I started to see the exact same patterns over and over.</p>
			<p class="text-large mb-4"><b>Injuries, aches, pains, and weakness that were <span class="text-primary">totally AVOIDABLE</span> with just a few simple exercises added to your training.</b></p>
			<p class="larger mb-5">(FYI - we've now worked with over 35,000 people who love to exercise to help them move better, progress faster and severely reduce the risk of injury)</p>
		</div>
		<div class="col-12 mb-5 text-center">
			<picture> 
				<source srcset="/img/lps/stability-builder/squares-2.webp" type="image/webp"/> 
				<source srcset="/img/lps/stability-builder/squares-2.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-2.jpg" class="mw-100 h-auto mt-2 lazy d-none d-lg-block mx-auto" width="896" height="200" alt="Stability Builder squares 2" />
			</picture>
			<picture> 
				<source srcset="/img/lps/stability-builder/squares-2-mob.webp" type="image/webp"/> 
				<source srcset="/img/lps/stability-builder/squares-2-mob.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-2-mob.jpg" class="mw-100 h-auto mt-2 lazy d-lg-none" width="896" height="200" alt="Stability Builder squares 2" />
			</picture>
		</div>
		<div class="col-lg-8">
			<p class="larger mb-4">The problem is that how do you know which exercises are the right ones?</p>
			<p class="larger text-primary mb-4"><b>Stability Builder™ removes that problem!</b></p>
			<p class="larger mb-4">It contains the best combination of exercises that I use myself, and we’ve used for years with our clients, allowing us all to get the most out of our training in the most efficient way possible - <u>without having to search through hundreds of exercises and spend hours a week on boring prehab/rehab sessions!</u></p>
			<p class="larger">Every workout is in a follow along format, so you will do the reps alongside me and Jenni at home or in the gym, meaning you won’t miss anything (and you’ll actually do them!)</p>
		</div>
		<div class="col-lg-10 text-left mt-5">
			<div class="review-box bg-light pt-4 pb-5 px-5 mob-px-3 mb-3">
				<div class="review-avatar">
					<picture> 
						<source srcset="/img/lps/stability-builder/chris.webp" type="image/webp"/> 
						<source srcset="/img/lps/stability-builder/chris.jpg" type="image/jpeg"/>
						<img src="/img/lps/stability-builder/chris.jpg" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Stability Builder Chris McBride testimonial" />
					</picture>
				</div>
				<p class="review-name title text-large mob-text-small mb-5 mob-mb-3">Chris McBride<br/>@for($x = 1; $x<= 5; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</p>
				<div class="review-body"> 
					<p class="text-large mb-0">"Tom teaches you to look after yourself, to no longer live in discomfort or pain from previous injuries or inherent lack of movement, to move again pain free and enjoy a higher quality of life, regardless of your age, ability, lifestyle or sporting endeavour."</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="pricing" class="container-fluid bg-primary text-white text-center py-5 mb-5">
	<div class="row my-4 mob-my-0">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-12 px-5 mob-px-3">
					<h2 class="mb-0 mob-px-3 large-title text-capitalize">The Most Time (& Money) Efficient Program For Total Body Stability</h2>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container my-5">
	<div class="row justify-content-center">
		<div class="col-12 mb-4 text-lg-center">
			<p class="text-large"><b>Stability Builder™ isn’t just exercises I like; <span class="text-primary">this is the BEST of the BEST!</span></b></p>
		</div>
		<div class="col-lg-8">
			<p class="larger">Tried & tested strength and mobility drills that I have been using with people all over the world from all different backgrounds for YEARS!</p>
			<p class="larger">This is the most efficient way to do your mobility & stability work and never fall behind.</p>
			<p class="larger">To work with me one to one online is £400 for 4 weeks, but with Stability Builder™ you can have access to my best programming (the exercises I use EVERY week for personal programming) for <b><u>just @if($product->sale_price)<s>£75</s> £{{$product->sale_price}} @else £75 @endif for Life!</u></b></b></p>
		</div>
		<div class="col-12 text-center my-5">
			<img src="/img/lps/stability-builder/list.svg" width="366" height="223" class="mw-100 h-auto"/>
		</div>
		<div class="col-12 mb-2 text-center">
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
		</div>
		<div class="col-12 text-center mb-3">
			<p class="mb-0 text-small mb-3">*payments in other currencies accepted & converted at the current rate</p> 
			<img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" alt="Visa Logo" width="94" class="img-fluid lazy mob-mb-0 mob-mr-3 mr-5 loaded" srcset="/img/logos/visa.svg" src="/img/logos/visa.svg" data-was-processed="true" style="max-width: 12vw;"> 
			<img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" alt="Mastercard Logo" width="79" class="img-fluid lazy mob-mr-3 mob-mb-0 mr-5 loaded" srcset="/img/logos/mastercard.svg" src="/img/logos/mastercard.svg" data-was-processed="true" style="max-width: 10vw;">
			<img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" alt="PayPal Logo" width="137" class="img-fluid lazy mob-mb-0 mr-5 mob-mr-3 loaded" srcset="/img/logos/paypal.svg" src="/img/logos/paypal.svg" data-was-processed="true" style="max-width: 20vw;"> 
			<img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" alt="American Express Logo" width="74" class="img-fluid lazy mob-mb-0 loaded" srcset="/img/logos/amex.svg" src="/img/logos/amex.svg" data-was-processed="true" style="max-width: 9vw;">
		</div>
	</div>
</div>
<div id="what-do-you-need" class="container-fluid bg-light">
	<div class="row">
		<div class="container my-5">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<h2 class="text-primary large-title text-capitalize mb-4">What Do You Need?</h2>
				</div>
				<div class="col-lg-8">
					<p class="larger mb-4">A lot of the workouts inside of Stability Builder™ require no equipment, but, to get the most out of it you will need:</p>
					<p class="larger list-check">A light resistance band or TheraBand</p>
					<p class="larger list-check">A weight e.g. dumbbell, kettlebell or a full 2L bottle</p>
					<p class="larger list-check mb-4">A step, thick book or yoga block you can stand on</p>
					<p class="larger">Printing off the worksheets is totally optional, you can complete the workouts using just your phone, laptop, tablet or TV screen!</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12 my-5 text-center">
			<picture> 
				<source srcset="/img/lps/stability-builder/squares-3.webp" type="image/webp"/> 
				<source srcset="/img/lps/stability-builder/squares-3.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-3.jpg" class="mw-100 h-auto mt-2 lazy d-none d-lg-block mx-auto" width="896" height="200" alt="Stability Builder squares 3" />
			</picture>
			<picture> 
				<source srcset="/img/lps/stability-builder/squares-3-mob.webp" type="image/webp"/> 
				<source srcset="/img/lps/stability-builder/squares-3-mob.jpg" type="image/jpeg"/>
				<img src="/img/lps/stability-builder/squares-3-mob.jpg" class="mw-100 h-auto mt-2 lazy d-lg-none" width="896" height="200" alt="Stability Builder squares 3" />
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid bg-primary text-white text-center py-5 mb-5">
	<div class="row my-4 mob-my-0">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-12 px-5 mob-px-3">
					<h2 class="mb-0 large-title text-capitalize">Fill In The Gaps, Severely Reduce The Risk of Injury & STAY on the Results Train For Longer, For <b><u>just @if($product->sale_price)<s>£75</s> £{{$product->sale_price}} @else £75 @endif  for Life!</u></b></h2>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-12 text-center">
			<h2 class="large-title text-capitalize mb-4">Get Started Now</h2>
		</div>
		<div class="col-lg-8">
			<p class="larger">Every little gem I’ve learned over my years working with people of all different backgrounds is combined here to bring the most affordable route to results, reduced injury and training longevity!</p>

			<p class="larger">Fill in the VITAL gaps today so you can train confidently to your best, without getting stuck with injuries.</p>

			<p class="larger mob-mb-4">Stability Builder™ is your path to more complete mobility & stability work for <b>@if($product->sale_price)<s>£75</s> £{{$product->sale_price}} @else £75 @endif  - for life.</b></p>
		</div>
		<div class="col-12 mb-2 text-center">
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
		</div>
	</div>
</div>
<div id="faqs" class="container-fluid bg-light">
	<div class="row">
		<div id="faqs" class="container mb-5">
			<div class="row mt-5 mob-mb-0 justify-content-center">
				<div class="col-12 mob-my-0">
					<h2 class="mb-5 large-title text-center">Frequently Asked Questions</h2>
					<div id="faq-accordion">
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1"><b>What equipment do I need?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_1" class="collapse" data-parent="#faq-accordion">
							<p>Some of the workouts only require you and your own bodyweight so you can get started right away, some require a resistance band and others required a weight – this can be anything from a kettlebell, dumbbell, a full backpack, or full bottle of water!</p>
							<p>If you have more equipment available, different weights or adjustable dumbbells would be great to progressively increase the weight you’re using, but the focus is stability and full joint strength from all angles so any weight at all will still allow you to get the benefits!</p>
							<p>The Rings Follow Along workout does require rings or a TRX, but this is a simply bonus, so if you don’t have these then you can still get all the awesome benefits of every other workout!</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2"><b>Do I need to do these workouts at the gym?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_2" class="collapse" data-parent="#faq-accordion">
							<p>Not at all! You can if you want to, but some space in your living room or bedroom is plenty.</p>

							<p>Any gym equipment we use has an easy home substitute, for example:</p>
							<ul>
								<li><p>Weight plates to stand on, could easily be a thick book, your fireplace, or bottom step of your stairs</p></li>
								<li><p>Rig post to attach a band to, could easily be your banister or a weighed-down table leg</p></li>
							</ul>
							<p>Stability Builder is designed to be as accessible as possible, so you can do the workouts whenever & wherever suits you best!</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_3" aria-expanded="false" aria-controls="answer_3"><b>Do I need to watch the videos every time I do the workouts?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_3" class="collapse" data-parent="#faq-accordion">
							<p>No, you don’t need to, it all depends on how you like to train! </p>
							<p>Once you’ve followed along once, or a couple of times, learned the technique and listened to all the useful hints and tips from Tom it is beneficial to try them at your own pace so you can really focus on how you feel, rather than trying to keep up with us. </p>
							<p>The PDFs in your dashboard have the workouts fully written out with screenshots so you can just use these as reminders as you go through.</p>
							<p>So, always follow along or work at your own pace – the choice is yours!</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_4" aria-expanded="false" aria-controls="answer_4"><b>Do I need to be fit to do the workouts?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_4" class="collapse" data-parent="#faq-accordion">
							<p>Not at all! Stability Builder is designed for movement quality so whether you’re super fit or a complete beginner, you’ll probably be working at a very similar pace. </p>

							<p>Despite calling them “workouts” there are only a couple that will actually leave you out of breath (I’m looking at your Super Duper Sets!), for the most part, your goal is to get progressively better at technique & control rather than speed or weight.</p>

							<p>Plus, bear in mind that more you find hard at the start the more you have to gain! The things you find challenging are the things you need the most.</p>

							<p>You adjust the rep schemes to your level if you struggle, e.g. doing half the reps we do, and still get loads of benefits. If you found everything super easy straight away where’s the fun in that?!</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_5" aria-expanded="false" aria-controls="answer_5"><b>I have an injury/pain, can I still do Stability Builder?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_5" class="collapse" data-parent="#faq-accordion">
							<p>If you haven’t done <a href="/products/the-simplistic-mobility-method">The Simplistic Mobility Method®</a> yet we would recommend you start there, it will build a solid movement foundation for you which you can build from safely.</p>
							<p>That being said! Stability Builder is amazing for old recurring injuries like ankle or knee or lower back pain that comes and goes. It will target exactly what you need and highlight where any weaknesses or compensations have built up (and probably causing your injuries to come back). </p>
							<p>Some movements may need adapting at the start, which we can help you with in the support group, but once you’ve completed the workouts a few times and realise how good they make you feel you’ll wonder what you ever did without them… and why everyone isn’t doing this stuff!</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_6" aria-expanded="false" aria-controls="answer_6"><b>Do I have to do all 11 workouts in a specific order?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_6" class="collapse" data-parent="#faq-accordion">
							<p>No, not really, they’re all beneficial by themselves.</p>
							<p>We recommend running through them all at least once, making notes as you go, then you can either start from the beginning again or pick and choose the workouts you feel give you the most benefits.</p>
							<p>Make sure you keep track of which ones you’re doing though! You don’t want to avoid the ones you hate and only do the workouts you find easy!  Each workout has specific principles in mind that develop different areas of your body/movement.</p>
							<p>Best thing to do is schedule in two Stability Builder workouts per week that are non-negotiable, and you just roll through them without even checking what’s coming in the next session, just nod and say YES COACH! Or you can call us nasty names if you like, we’ll not take offence.</p>
						</div>
						<p class="text-large mob-text-s"><a class="dark" data-toggle="collapse" href="#answer_7" aria-expanded="false" aria-controls="answer_7"><b>Can I do Stability Builder alongside my normal training?</b> <i class="fa fa-angle-down collapsed"></i><i class="fa fa-angle-up expanded"></i></a></p>
						<div id="answer_7" class="collapse" data-parent="#faq-accordion">
							<p>Yes! Stability Builder is the ultimate bolt-on to your existing training schedule! We recommend slotting in 2 workouts per week (they’re between 10 – 30 mins long), which can be used as warmups, cool downs, or standalone active recovery sessions.</p>
							<p>Or, if you’re currently on a deload week, returning from injury, or maybe a beginner who’s just figuring out your training you can use Stability Builder as a standalone programme, scheduling 4 per week if you like!</p>
						</div>
					</div>
				</div>
				<div class="col-12 mt-3">
					<p>Got a question we didn't answer? Visit our <a href="/help">help page</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container my-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center">
			<p class="text-large"><b>Become the master of your own stability and build superior joint strength with us for <span class="text-primary"><b><u>just @if($product->sale_price)<s>£75</s> £{{$product->sale_price}} @else £75 @endif  for Life!</u></b></p>
			<a href="/basket/add/17">
				<button type="button" class="btn btn-primary d-inline-block">Buy Now <i class="fa fa-angle-double-right text-white"></i></button>
			</a>
		</div>
	</div>
</div>
<div class="container">
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
	$(document).ready(function(){
		if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	    backToTop = function () {
	    	var scrollTop = $(window).scrollTop();
	    	if (scrollTop > scrollTrigger) {
	    		$('#back-to-top').addClass('show');
	    	} else {
	    		$('#back-to-top').removeClass('show');
	    	}
	    };
	    backToTop();
	    $(window).on('scroll', function () {
	    	backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	    	e.preventDefault();
	    	$('html,body').animate({
	    		scrollTop: 0
	    	}, 700);
	    });
	  }
	});

</script>
@endsection