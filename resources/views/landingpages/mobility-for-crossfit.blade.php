@php
$page = 'Reduce Pain, Increase Mobility';
$pagename = 'LP';
$pagetitle = "Mobility For Crossfit | The Simplistic Mobility Method | Tom Morrison";
$meta_description = "The Simplistic Mobility Method. A complete mobility program for crossfit to improve your flexibility & stability. No equipment required!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@section('styles')
<style>
	#content{
		padding-top: 0 !important;
	}
</style>
@endsection
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'nomenuatall' => true, 'pagename' => $pagename])
@section('head_section')
<style>
	.crossfit-slider-container .bx-viewport{
		overflow: visible !important;
	}
</style>
@endsection
@section('header')
<header class="video-container position-relative athlete-header bg h-auto mob-pb-5 border-0">
	<video class="header-video d-none d-md-block z-0" autoplay="true" playsinline muted loop>
		<source src="/vid/athlete.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
		<div class="video-trans"></div>
		<div class="container-fluid text-white pt-4 mob-pt-5 position-relative" style="z-index: 10;">
			<div class="row pt-5 mob-pt-0">
				<div class="container pt-5 mob-pt-0">
					<div class="row justify-content-center">
						<div class="col-lg-10 text-center mob-pt-5">
							<h1 class="mt-5 mob-mt-3 mb-2 lp-title-smaller mob-bigger text-center text-uppercase">Mobility for <br class="d-md-none"/><span class="text-primary">Crossfit<span class="tm">&reg;</span></span></h1>
						</div>
						<div class="col-lg-7 text-center">
							<p class="mb-0 text-large mob-text-smaller">Simple & effective mobility program to enhance your movement and increase your performance.</p>
							<a href="/basket/add/1"><div class="btn btn-primary mt-4 mob-mt-5 h-auto mx-auto d-inline-block"><b>Start Today >></b><br/><div style="font-weight:300;">(Get Instant Access)</div></div></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid mob-pt-3 mt-5 py-5 mob-mb-0 position-relative z-3 d-none d-lg-block" style="min-height: 262px;z-index: 10;">
			<div class="row mt-5">
				<div class="container">
					<div class="row text-center">
						<div class="col-12">
							<p class="text-large text-white">Featured in:</p>
						</div>
						<div class="col-12">
							<img src="/img/lps/mobility-training/logos.svg?v2.0"class="w-100" alt="Tom featured logos"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	@endsection
	@section('content')
	<div class="container-fluid py-4 d-lg-none" style="background-color: #222428;">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<p class="text-large text-white">Featured in:</p>
						<img src="/img/logos/crossfit-featured-logos.svg"class="w-100" alt="Tom featured logos"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-5">
		<div class="row">
			<div class="col-12">
				<p class="text-145"><b>CrossFit is a demanding sport, and without good mobility it can MESS YOU UP!</b></p>
				<p class="text-145">Few years back, I was constantly picking up injuries, and a full barbell snatch was only a dream - I spent more time on a foam roller than actually training…</p>
				<p class="text-145">I kept pushing through these pains & inflexibility because... Well, the CrossFit open was coming up or I was trying to beat my class bestie/rival 😈</p>
				<p class="text-145">Skip forward to now and I am loving life with a strong & healthy body. Snatches, muscle ups, handstands, cleans, pistol squats - and all feeling amazing!</p>
				<p class="text-145">I even enjoy thrusters because I can finally lock my arms overhead and get a mini rest at the top like Matt Fraser 😎</p>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg text-white">
		<div class="row">
			<div class="container">
				<div class="row mob-mb-4">
					<div class="col-lg-6 my-5 mob-mb-0">
						<p class="text-145"><b>So how did I go from this...</b></p>
						<picture>
							<source srcset="/img/lps/mobility-for-crossfit/tom-bad-overhead-squat.jpg" type="image/jpeg"/>
							<source srcset="/img/lps/mobility-for-crossfit/tom-bad-overhead-squat.webp" type="image/webp"/>
							<img src="/img/lps/mobility-for-crossfit/tom-bad-overhead-squat.jpg" type="image/jpeg" class="w-100 lazy" alt="Mobility for Crossfit - Tom bad overhead squat" />
						</picture>
					</div>
					<div class="col-lg-6 my-5 mob-mb-0">
						<p class="text-145"><b>To this...?</b></p>
						<picture>
							<source srcset="/img/lps/mobility-for-crossfit/tom-overhead-squat.jpg" type="image/jpeg"/>
							<source srcset="/img/lps/mobility-for-crossfit/tom-overhead-squat.webp" type="image/webp"/>
							<img src="/img/lps/mobility-for-crossfit/tom-overhead-squat.jpg" type="image/jpeg" class="w-100 lazy" alt="Mobility for Crossfit - Tom overhead squat" />
						</picture>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<p class="text-white text-145">I realised the only way to improve my mobility for CrossFit was to approach it like training:</p>
						<p class="text-white text-145">Get progressively better at the right things and it's that simple!</p>
						<p class="text-white text-145">I could squat & deadlift properly, recover better and train longer without feeling stiff & sore all the time!</p>
						<p class="text-white text-145">When we started applying this Method to clients, everyone was getting the same results! Not only did it work for me, but it has been a success for over 15,000 people worldwide!</p>
					</div>
					<div class="col-12 text-center mb-5">
						<a href="/basket/add/1"><div class="btn btn-primary mt-4 mob-mt-3 h-auto mx-auto d-inline-block"><b>Start Today >></b><br/><div style="font-weight:300;">(Get Instant Access)</div></div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container mb-5 mob-pb-0">
		<div class="row justify-content-center text-left text-lg-center py-5">
			<div class="col-lg-8 mob-px-0">
				<h2 class="text-capitalize mob-text-smaller text-primary text-center mb-4">The Simplistic Mobility Method<span class="tm">&reg;</span></h2>
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zMvnI02wPSE?rel=0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-lg-11 mt-5">
				<p class="text-145 mb-4"><b>Join the global SMM community full of people who are regaining control of their body.</b></p>
				<p class="text-145">Don't let niggles, aches, pains and injuries stop you from being consistent or hold you <br class="d-none d-lg-inline" />back in your training!</p>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/stretching.svg" height="65" alt="Stretching"/>
				<h3 class="text-capitalize mt-3">Full Body</h3>
				<p class="text-large mob-text-small mb-0">Everything you need in one program</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/track.svg" height="65" alt="Track Progress"/>
				<h3 class="text-capitalize mt-3">Track Progress</h3>
				<p class="text-large mob-text-small mb-0">Built in tests keep you focused</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/nosub.svg" height="65" alt="No Subscription"/>
				<h3 class="text-capitalize mt-3">No Subscription</h3>
				<p class="text-large mob-text-small mb-0">One-time payment, lifetime access</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/alld.svg" height="65" alt="All Devices"/>
				<h3 class="text-capitalize mt-3">All Devices</h3>
				<p class="text-large mob-text-small mb-0">Phone, tablet, laptop, PC or TV. Even offline!</p>
			</div>
			<div class="col-12 text-center my-5 mob-my-0">
				<a href="/basket/add/1"><div class="btn btn-primary mt-4 mob-mt-0 h-auto mx-auto d-inline-block"><b>Start Today >></b><br/><div style="font-weight:300;">(Get Instant Access)</div></div></a>
			</div>
		</div>
	</div>
	<div class="container-fluid overflow-hidden">
		<div class="row">
			<div class="container crossfit-slider-container">
				<div class="row justify-content-center mob-pb-5">
					<div class="col-lg-12 mob-px-0 weird-height-thing">
						<div style="width: 460px;max-width: 100%;display: block;margin: auto;">
							<div class="crossfit-slider">
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after1.webp?v2.0" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after1.jpg?v2.0" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after1.jpg?v2.0" class="w-100" alt="Simplistic Mobility Method Gallery image 1" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after2.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after2.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after2.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 2" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after3.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after3.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after3.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after4.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after4.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after4.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after5.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after5.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after5.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 5" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after6.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after6.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after6.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 6" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after7.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after7.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after7.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 7" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after8.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after8.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after8.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 8" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after9.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after9.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after9.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 9" />
									</picture>
								</li>
								<li style="list-style: none;" class="px-2">
									<picture> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after10.webp" type="image/webp"/> 
										<source srcset="/img/lps/mobility-for-crossfit/before-after10.jpg" type="image/jpeg"/>
										<img src="/img/lps/mobility-for-crossfit/before-after10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 10" />
									</picture>
								</li>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container mb-5 mob-my-0">
		<div class="row">
			<div class="col-lg-6 mb-4">
				<div class="card p-4 border-0 bg-light shadow">
					<div class="row">
						<div class="col-6">
							<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
						</div>
						<div class="col-6 text-right">
							<p class="title" style="font-weight:300;"><b>Nita Newman</b></p>
						</div>
						<div class="col-12">
							<p class="mb-0">This is my exercise plan for life. I've NEVER stuck with any other program. I got so used to pain & stiffness that I forgot what it was like to feel well. LOVE IT  ❤️</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 mb-4">
				<div class="card p-4 border-0 bg-light shadow">
					<div class="row">
						<div class="col-6">
							<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
						</div>
						<div class="col-6 text-right">
							<p class="title" style="font-weight:300;"><b>Ben Ladlow</b></p>
						</div>
						<div class="col-12">
							<p class="mb-0">SMM has been an absolute game changer for me. For the first time I've been able to squat heavy without any niggling pain, and recovery between sets has been easier</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-lg-6 mob-mb-5">
				<h2 class="text-capitalize mob-text-smaller mb-3">Move Better, Train Better</h2>
				<p>SMM is built on foundational movement principles, so it works with every body, no matter what your starting level.</p>
				<p>You complete the 30 minute routine 2-4x a week, so it easily fits into your busy schedule.</p>
				<p>We give you the tools, all you have to do is put the work in. Don't avoid things that are hard or uncomfortable, that's where the results lie.</p>
			</div>
			<div class="col-lg-6">
				<picture> 
					<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
					<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
					<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy mb-4" alt="Simplistic mobility method devices" />
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12 mt-5 mb-5">
						<div class="row justify-content-center">
							<div class="col-lg-12">
								<h2 class="mb-4 text-capitalize">What You Get:</h2>
							</div>
							<div class="col-lg-6">
								<ul class="check-graphics-green mob-mb-0">
									<li>
										<p class="mb-0 larger">In-Depth SMM E-book</p>
									</li>
									<li>
										<p class="mb-0 larger">13 Movement Routines</p>
									</li>
									<li>
										<p class="mb-0 larger">Series of Tests & Assessments</p>
									</li>
									<li>
										<p class="mb-0 larger">Comprehensive Demo Videos</p>
									</li>
									<li>
										<p class="mb-0 larger">Beginner Versions of Every Exercise</p>
									</li>
									<li>
										<p class="mb-0 larger">Progress Tracker & 'Cheat Sheet' PDFs</p>
									</li>
									<li>
										<p class="mb-0 larger">Support From the SMM Community</p>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5">
								<div class="row justify-content-center text-center">
									<div class="col-lg-10">
										<h3 class="text-large mob-text-small mb-3">For a one-off payment of <span class="text-primary">@if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif</span></h3>
										<p class="text-small">(Other currencies accepted! Payments are converted at the current rate)</p>
										<a href="/basket/add/1"><div class="btn btn-primary mt-2 mb-3 mx-auto d-inline-block">Buy Now</div></a>
										<div class="col-12 text-center"> <img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" id="tom-footer-top" alt="PayPal Logo" width="86" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/paypal.svg" src="/img/logos/paypal.svg" data-was-processed="true"> <img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" id="tom-footer-top" alt="Mastercard Logo" width="36" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/mastercard.svg" src="/img/logos/mastercard.svg" data-was-processed="true"> <img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" id="tom-footer-top" alt="Visa Logo" width="47" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/visa.svg" src="/img/logos/visa.svg" data-was-processed="true"> <img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" id="tom-footer-top" alt="American Express Logo" width="33" class="img-fluid lazy mob-mb-0 loaded" srcset="/img/logos/amex.svg" src="/img/logos/amex.svg" data-was-processed="true"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-5">
		<h2 class="text-primary text-capitalize">What people say about the Simplistic Mobility Method<span class="tm">&reg;</span></h2>
		<div class="card-columns">
			@foreach($reviews as $review)
			<div class="card mt-5 mb-3">
				<div class="review-box bg-light p-3">
					<div class="review-avatar smaller">
						<img src="{{$review->avatar}}" alt="{{$review->name}}, Simplistic mobility method (SMM) review">
					</div>
					<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
					<div class="review-body"> 
						{!!$review->content!!}
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="row mt-5">
			<div class="col-12 text-center">
				<a href="/simplistic-mobility-method-reviews">
					<div class="btn btn-outline d-inline-block">See more reviews</div>
				</a>
			</div>
		</div>
	</div>
	<div class="container-fluid bg banner-bg2 text-center">
	<div class="row">
		<div class="col-12 py-5">
			<p class="text-white">Never let yourself be set back again by unnecessary injuries or aches & pains<br/>Build complete strength and figure out what you have been missing with the Simplistic Mobility Method®</p>
			<h2 class="text-white mb-4 text-transform-none">Only @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif TODAY!</h2>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Take control of your mobility >></button></a>
		</div>
	</div>
</div>
	@endsection
	@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".crossfit-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
			infiniteLoop: true,
			touchEnabled:true
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
	<script>
		function addToBasket(id, name, category, price){
			dataLayer.push({
				'event': 'addToCart',
				'ecommerce': {
					'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
	'name': name,
	'id': id,
	'price': price,
	'brand': "Tom Morrison",
	'category': category,
	'quantity': 1
}]
}
}
});
			fbq('track', 'AddToCart', {
				value: {{$product->price}},
				currency: 'GBP'
			});
			console.log('done');
		}
		$(document).ready(function(){
			if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	    backToTop = function () {
	    	var scrollTop = $(window).scrollTop();
	    	if (scrollTop > scrollTrigger) {
	    		$('#back-to-top').addClass('show');
	    	} else {
	    		$('#back-to-top').removeClass('show');
	    	}
	    };
	    backToTop();
	    $(window).on('scroll', function () {
	    	backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	    	e.preventDefault();
	    	$('html,body').animate({
	    		scrollTop: 0
	    	}, 700);
	    });
	  }
	});

	</script>
	@endsection