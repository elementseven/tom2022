@php
$page = 'Lower Back Pain';
$pagename = 'LP';
$pagetitle = "Lower Back Pain - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great mobility drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row justify-content-center">
		<div class="col-lg-6">
			<h1 class="mt-5 mob-mt-3 mb-5 pb-4 mob-pb-0 lp-title-smaller text-primary text-center">The best advice for lower back pain</h1>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-7">
			<h3 style="line-height: 2.4rem; font-weight: 400;">If you need relief from lower back pain, you’re not alone!</h3>
			<p class="text-large">We want to give you the most important lower back exercises you can use straight away to start getting yourself back to 100% again! </p>
			<picture> 
				<source srcset="/img/lps/lower-back-reach/lower-back-stretch.webp" type="image/webp"/> 
				<source srcset="/img/lps/lower-back-reach/lower-back-stretch.jpg" type="image/jpeg"/>
				<img src="/img/lps/lower-back-reach/lower-back-stretch.jpg" class="img-fluid lazy d-lg-none mt-5 mb-2" alt="Lower back stretch exercise" />
			</picture>
			<p class="mb-5 mt-2 text-center d-lg-none" style="color: #868686;">**not a real exercise, don't try 😂</p>
			<p><b>Many people get stuck stretching their lower back all the time</b>, temporarily relieving the painful symptoms, but constant stretching will mean you’ll be back to Google again and again searching “lower back pain” forever and never getting long term relief.</p>
			<p>We’re going to give you the exercises you need to not only relieve your back pain, <b>but also stop it from coming back!</b></p>
			<p>The trick is <b>understanding why your lower back is painful in the first place</b>. A lot of people don’t realise it’s largely to do with your hip flexibility: if your hips are tight, your lower back must compensate for their lack of movement. <b>Your lumbar spine is designed to be stable, while your hips should be mobile</b>.</p>
			<p>I don’t mean you need to able to do the splits, but if you’re missing the standard level of normal hip flexibility the unwanted lower back movement will make it feel unstable and weak - and <b>when something in your body feels like that it gives you pain as a warning signal</b>.</p>
			<p>Even issues such as bulging or herniated discs can be completely pain-free when you improve your hip function.</p>
			<p>So let's get stuck in!</p>
		</div>
		<div class="col-lg-5 d-none d-lg-block pl-5">
			<picture> 
				<source srcset="/img/lps/lower-back-reach/lower-back-stretch.webp" type="image/webp"/> 
				<source srcset="/img/lps/lower-back-reach/lower-back-stretch.jpg" type="image/jpeg"/>
				<img src="/img/lps/lower-back-reach/lower-back-stretch.jpg" class="img-fluid lazy" alt="Lower back stretch exercise" />
			</picture>
			<p class="mb-5 mt-2 text-center" style="color: #868686;">**not a real exercise, don't try 😂</p>
			<picture> 
				<source srcset="/img/lps/lower-back-reach/lower-back-pain.webp" type="image/webp"/> 
				<source srcset="/img/lps/lower-back-reach/lower-back-pain.jpg" type="image/jpeg"/>
				<img src="/img/lps/lower-back-reach/lower-back-pain.jpg" class="img-fluid lazy" alt="Lower back pain exercise" />
			</picture>
		</div>
		<div class="col-lg-12 my-5">
			<div class="card border-0 shadow container p-5 mob-px-3 mob-py-4">
				<div class="row">
					<div class="col-12 text-center mb-4">
						<h2 class="text-capitalize">Clockwork Hip Drill</h2>
					</div>
					<div class="col-lg-6">
						<p>Let’s start off with one of the easiest exercises you can do right now. Working on your hips like this is much safer than starting to crank on the lower back, and probably will give you more benefits. The great thing about this drill is that even if you are in pain right now, you will still be able to do it, just with less depth.</p>
						<p>If you don't have a box, you can use anything to elevate your foot! A chair, sofa, even your stairs!</p>
						<div class="embed-responsive embed-responsive-16by9 d-lg-none my-4">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/pb7HEvHC1Ak?rel=0" allowfullscreen></iframe>
						</div>
						<p><b>Spend 5-10 minutes doing this</b>, making sure you spend equal amount of time per leg. Notice if your hips feel different from side to side – especially if you feel your back pain on one side.</p>
						<p>Use the clockwork hip drill every day until it starts getting easier. Yes, it may feel uncomfortable for the first few times, but this is because your hips have been stiff for so long. Not only does this drill build flexibility, but simultaneously builds strength in your new ranges! This is the best remedy for your pain: flexibility & stability combined.</p>
					</div>
					<div class="col-lg-6 d-none d-lg-block">
						<div class="embed-responsive embed-responsive-16by9">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/pb7HEvHC1Ak?rel=0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="whatsincluded" class="container-fluid bg-primary text-white py-5 mb-5">
	<div class="row my-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 text-center">
					<h2 class="large-title text-nocase mb-3">Remember!</h2>
					<p class="">If you’ve been in pain for a long time it can take a few weeks to really rewrite that “nervous” feeling in your body. The only way to do that is to move; resting will never get rid of it! Resting will only make you weaker and should be used only if you’ve had a serious impact injury… and in that case you shouldn’t be online, you should be seeing your Doctor!! </p>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-12 my-5">
			<div class="card border-0 shadow container p-5 mob-px-3 mob-py-4">
				<div class="row">
					<div class="col-12 text-center mb-4">
						<h2 class="text-capitalize">Lower Back Stretches</h2>
					</div>
					<div class="col-lg-6">
						<p>If you've been doing the Clockwork drill for few days and your lower back still feels stiff, then you can start to add some lower back specific stretching. But make sure to watch the full video; it not only shows you 2 of our favourite stretches for instant pain relief but also goes into some reasons why it may hurt in the first place, with some bonus tips for reducing pain long term!</p>
					</div>
					<div class="col-lg-6 mob-mt-3">
						<div class="embed-responsive embed-responsive-16by9">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lPQx4TdW6Og?rel=0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 my-5">
			<div class="card border-0 shadow container p-5 mob-px-3 mob-py-4">
				<div class="row">
					<div class="col-12 text-center mb-4">
						<h2 class="text-capitalize">Core Strength</h2>
					</div>
					<div class="col-lg-6">
						<p>Lastly your core strength is a massive factor in eliminating your lower back pain for good! Your core protects your spine, so when it’s weak your body reacts (giving you pain is a good way to get you to stop what you’re doing to prevent a spinal injury).</p>
						<p>We teach 4 types of core strength and they are all equally important! Often “core strength” for lower back pain is simply a plank and this is NOT ENOUGH! Your core is so much more sophisticated than that.</p>
					</div>
					<div class="col-lg-6 mob-mt-3 d-none d-lg-block">
						<div class="embed-responsive embed-responsive-16by9">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tQdUIHmEfR4?rel=0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-12 mb-3">
						<p>The four types of core strength you should work on are:</p>
					</div>
					<div class="col-lg-4">
						<h3 class="text-primary mob-mb-0">Isometric</h3>
						<p><b>Holding strong positions</b></p>
						<h3 class="text-primary mob-mb-0">Rotational</h3>
						<p><b>Being able to move with control</b></p>
					</div>
					<div class="col-lg-8">
						<h3 class="text-primary mob-mb-0">Anti-rotational</h3>
						<p><b>Being able to resist something pulling/pushing you</b></p>
						<h3 class="text-primary mob-mb-0">Reactive</h3>
						<p><b>Balance & stability</b></p>
					</div>
					<div class="col-12 mt-3">
						<p>You don’t have to obsess over them, but you need to have even strength in each, without missing any out.</p>
						<p class="d-lg-none">Here’s some examples of how to train your core:</p>
						<div class="embed-responsive embed-responsive-16by9 d-lg-none mb-4">
						  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tQdUIHmEfR4?rel=0" allowfullscreen></iframe>
						</div>
						<p>If you regularly use all four of them in some way your body will have complete baseline strength which will make it feel more confident and take away the reasons for your pain! EVEN if you have an injury!</p>
						<p>I personally have L4/L5/S1 protrusions plus an impinged nerve and with these techniques I have not had any pain for YEARS!</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9 text-center my-4">
			<p class="text-large">I understand how hard it is when you’re constantly struggling to do day-to-day tasks - but you will get through it if you take your movement into your own hands instead of waiting for it to go away!</p>
			<p class="text-large mb-4"><b>Save this link, share it with someone that needs it and if you have any questions at all feel free to <a href="mailto:support@tommorrison.uk"><u>email us</u></a> or check out <a href="https://tommorrison.uk/products/the-simplistic-mobility-method" target="_blank"><u>The Simplistic Mobility Method</u></a> if you want to really improve your entire body!</b></p>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<hr style="border-top: 1px solid #000;" />
		</div>
		<div class="col-12 pt-5">
			<h2 class="text-center text-capitalize mb-4">Who Are We?</h2>
			<p class="mb-4">Tom & Jenni make complicated information about training and injury prevention fun and easy to understand.</p>
			<p class="mb-4"><b>Tom Morrison</b> came into sports and fitness quite late compared to most, and his lack of sporting history led him to pick up multiple injuries including a major back injury. His journey to recovery has made him one of the top names for flexibility and stability information. He is the creator of the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method" target="_blank"><u>Simplistic Mobility Method</u></a>; helping people all over the world to improve their bodies; and contributes to many training magazines including Testosterone Nation, BOXROX and Breaking Muscle.</p>
			<p class="mb-4">Jenni has been into physical training for most of her life, and where Tom had zero flexibility Jenni had far too much. She has hypermobility syndrome, which means that her joints don’t have the natural stability they should, so she had to develop it through her own training. She never let the condition hold her back and successfully trains a wide range of disciplines, from Weightlifting to Hand Balancing.</p>
			<p class="mb-4">Together they <a href="https://tommorrison.uk/blog" target="_blank"><u>create and share content online</u></a> to help people deal with their own mobility/stability issues and travel all over to perform <a href="https://tommorrison.uk/seminars" target="_blank"><u>seminars & workshops</u></a> which are attended by all ages, abilities and interests - from regular gym go-ers to physiotherapists.</p>
			<p>They want everyone to know that their body is adaptable. <b>Pain is not normal</b> and issues that people just accept can be changed.</p>
			<picture> 
				<source srcset="/img/lps/tomjenniflexing.webp" type="image/webp"/> 
				<source srcset="/img/home/tomjenniflexing.jpg" type="image/jpeg"/>
				<img src="/img/home/tomjenniflexing.jpg" class="w-100 lazy" alt="Tom Morrison and Jenni flexing and smiling" />
			</picture>
		</div>
	</div>
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){

dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	$('.play-btn').click(function(){
		$(this).fadeOut();
		$('#bottom-video')[0].play();
	});
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection