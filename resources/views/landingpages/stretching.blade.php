@php
$pagename = 'LP';
$page = 'Stretching Program';
$pagetitle = "Stretching Program - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great mobility drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container mob-mb-5">
	<div class="row justify-content-center text-center">
		<div class="col-md-12">
			<div class="d-table w-100 full-height">
				<div class="d-table-cell align-middle w-100 h-100 full-height">
					<div class="row justify-content-center">
						<div class="col-lg-7">
							<h1 class="mob-mt-3 mb-3 lp-title text-dark mb-5">What do you want to stretch?</h1>
						</div>
					</div>
					<div class="row text-center mb-5 mob-mb-0  pb-5 mob-pb-0">
						<div class="col-lg-3 col-md-6">
							<a href="#upperback">
								<div class="btn btn-primary d-inline-block no_max mb-5 mob-mb-3">Upper Back</div>
							</a>
							<a href="#lowerback">
								<div class="btn btn-primary d-inline-block no_max mb-5 mob-mb-3">Lower Back</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-6">
							<a href="#hips">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Hips</div>
							</a>
							<a href="#shoulders">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Shoulders</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-6">
							<a href="#ankles">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Ankles</div>
							</a>
							<a href="#neck">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Neck</div>
							</a>
						</div>
						<div class="col-lg-3 col-md-6">
							<a href="#knees">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Knees</div>
							</a>
							<a href="#wholebody">
								<div class="btn btn-primary d-inline-block mb-5 no_max mob-mb-3">Whole body</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div id="upperback" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Upper Back</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BFaIxWSXoec?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Zenith Rotations</h3>
			<p class="mb-4"><b>We spend so much of our time hunched over desks & phones, it's so common to get a tight upper back. This quick thoracic rotation stretch will help you feel more open!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Go into an all 4s position on the floor</p></li>
				<li><p class="mb-0">Sit your hips back towards your heels</p></li>
				<li><p class="mb-0">Lift one arm out to the side at 90 degrees, keeping the other hand on the floor</p></li>
				<li><p class="mb-0">Rotate as much as you can in the direction of your lifted arm</p></li>
				<li><p class="mb-0">Take a deep breath in, then exhale as you twist a bit further</p></li>
				<li><p class="mb-0">Without coming out of the twist, take another deep breath in, then exhale and twist further</p></li>
				<li><p class="mb-0">Do this 3-5 times on this side, trying to get further each time</p></li>
				<li><p class="mb-0">Switch sides, and repeat!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Twist from your upper back, not your lower back</p></li>
					<li><p>Use the hand on the floor to help push yourself round</p></li>
					<li><p>Use your exhales to "relax" into the new range</p></li>
				</ul>
			</div>
			<p>Most shoulder problems are actually caused by tight thoracic spines (your upper back, or "T-Spine"), and working on one side at a time like this allows you to increase your range far more effectively than just leaning over a roller!</p>
			<p><b>If you'd like to take your back stretching a bit further, why not <a href="https://www.youtube.com/watch?v=-wOJZL-TthI" target="_blank"><u>check out our video on back bending</u></a>!</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Go into an all 4s position on the floor</p></li>
				<li><p>Sit your hips back towards your heels</p></li>
				<li><p>Lift one arm out to the side at 90 degrees, keeping the other hand on the floor</p></li>
				<li><p>Rotate as much as you can in the direction of your lifted arm</p></li>
				<li><p>Take a deep breath in, then exhale as you twist a bit further</p></li>
				<li><p>Without coming out of the twist, take another deep breath in, then exhale and twist further</p></li>
				<li><p>Do this 3-5 times on this side, trying to get further each time</p></li>
				<li><p>Switch sides, and repeat!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="lowerback" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Lower Back</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lPQx4TdW6Og?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Elbow Reaches</h3>
			<p class="mb-4"><b>Feel free to watch the whole video, or skip to <span class="text-primary">2:39</span> for one of our favourite lower back stretches! It hits the, lower back, hamstrings and the QL all at the same time!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Start stood with your feet hip width apart and bend your knees slightly</p></li>
				<li><p class="mb-0">Fold forwards & grab opposite elbows with your hands</p></li>
				<li><p class="mb-0">Make your torso nice and long, try and feel a stretch in your lower back straight away</p></li>
				<li><p class="mb-0">Then reach one elbow towards the floor between your feet</p></li>
				<li><p class="mb-0">Without lifting back up, reach your other elbow towards the floor</p></li>
				<li><p class="mb-0">Alternating sides for 4 total reps</p></li>
				<li><p class="mb-0">Without lifting your torso, try and straighten your knees</p></li>
				<li><p class="mb-0"> Then re-bend and repeat your reaches for another 4 reps
				<li><p class="mb-0"> Repeat this as many times as you'd like!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Bend your knees as much as you need!</p></li>
					<li><p>Really reach towards the floor, rounding your back and elevating your shoulders as much as possible</p></li>
					<li><p>Move slowly and deliberately</p></li>
				</ul>
			</div>
			<p>The lower back is a tricky subject. Often it's tight for a reason - usually there's something else it's having to compensate for.</p>
			<p><b>If you're always getting a tight lower back, <a href="https://www.youtube.com/watch?v=v8VHz8ChwLM" target="_blank"><u>check out this video for some treatment & prevention tips!</u></a></b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Start stood with your feet hip width apart and bend your knees slightly</p></li>
				<li><p>Fold forwards & grab opposite elbows with your hands</p></li>
				<li><p>Make your torso nice and long, try and feel a stretch in your lower back straight away</p></li>
				<li><p>Then reach one elbow towards the floor between your feet</p></li>
				<li><p>Without lifting back up, reach your other elbow towards the floor</p></li>
				<li><p>Alternating sides for 4 total reps</p></li>
				<li><p>Without lifting your torso, try and straighten your knees</p></li>
				<li><p> Then re-bend and repeat your reaches for another 4 reps
				<li><p> Repeat this as many times as you'd like!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="hips" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Hips</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/4pCy-BxTDnE?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">The Deep Lunge</h3>
			<p class="mb-4"><b>This exercise is so good for stretching your hips, we turned it into a test to assess your hip health! It hits so many areas at once! Give it a go - do you pass?</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Start in a push up position</p></li>
				<li><p class="mb-0">Bring one foot up to outside the same side hand</p></li>
				<li><p class="mb-0">Keeping your shin vertical, aim to replace your hand with your elbow</p></li>
				<li><p class="mb-0">You may or not be able to reach the floor, but make a note of wherever you get to</p></li>
				<li><p class="mb-0">Take a deep breath in as you lift your arm up to the sky, twisting your torso upwards</p></li>
				<li><p class="mb-0">Exhale as you bring your elbow back down to as close to the floor as possible</p></li>
				<li><p class="mb-0">Repeat for 3-5 reps</p></li>
				<li><p class="mb-0">Switch and repeat on the other side - are both sides the same?</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Keep your front shin vertical!!</p></li>
					<li><p>Try to stretch from the hip, rather than rounding the back</p></li>
					<li><p>Keep your back leg straight and off the ground for an extra hip flexor stretch!</p></li>
				</ul>
			</div>
			<p>This is our go-to warm up exercise for the lower body, it hits everything: hamstrings, adductors, hip flexors - even your upper back when you twist!</p>
			<p><b>Find out more about the Deep Lunge Test and Hip Flexion <a href="https://tommorrison.uk/blog/2019-09-29/hows-your-hip-flexion" target="_blank"><u>in this blog</u></a>.</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Start in a push up position</p></li>
				<li><p>Bring one foot up to outside the same side hand</p></li>
				<li><p>Keeping your shin vertical, aim to replace your hand with your elbow</p></li>
				<li><p>You may or not be able to reach the floor, but make a note of wherever you get to</p></li>
				<li><p>Take a deep breath in as you lift your arm up to the sky, twisting your torso upwards</p></li>
				<li><p>Exhale as you bring your elbow back down to as close to the floor as possible</p></li>
				<li><p>Repeat for 3-5 reps</p></li>
				<li><p>Switch and repeat on the other side - are both sides the same?</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="shoulders" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Shoulders</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7uMPn9T-fw4?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Wall Stretch</h3>
			<p class="mb-4"><b>A lovely shoulder stretch with a sneaky bit of upper back and hamstrings too! Once you've held this for 30s or so, do a few rotations with your shoulders to keep them healthy!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Stand in front of a wall and place you hands on it just below shoulder height</p></li>
				<li><p class="mb-0">Bend down so that your chest is parallel to the floor, sticking your bum out behind you</p></li>
				<li><p class="mb-0">Tuck your pelvis slightly to make sure you're not arching your lower back, and keep pushing down for 30 seconds</p></li>
				<li><p class="mb-0">Come up and repeat 2 more times!</p></li>
				<li><p class="mb-0"> Try playing with your hand position - placing them higher or lower, each position will give you a slightly different stretch!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Keep your elbows straight and push into the wall</p></li>
					<li><p>Push your chest forwards & down as you push your hips back & up</p></li>
					<li><p>Keep your legs straight for a bonus hamstring stretch</p></li>
				</ul>
			</div>
			<p>Shoulders are so complex, there's so much you can do with them - and so much that can go wrong! As well as adding range of motion, don't forget to keep them strong and stable too!</p>
			<p><b><a href="https://www.youtube.com/watch?v=chmwOU8dhrc" target="_blank"><u>Watch this video</u></a> for our favourite shoulder stability exercise!.</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Stand in front of a wall and place you hands on it just below shoulder height</p></li>
				<li><p>Bend down so that your chest is parallel to the floor, sticking your bum out behind you</p></li>
				<li><p>Tuck your pelvis slightly to make sure you're not arching your lower back, and keep pushing down for 30 seconds</p></li>
				<li><p>Come up and repeat 2 more times!</p></li>
				<li><p> Try playing with your hand position - placing them higher or lower, each position will give you a slightly different stretch!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="ankles" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Ankles</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/y6kLhEgk4cA?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Rig Stretch</h3>
			<p class="mb-4"><b>Don't worry if you don't have a rig or squat rack you can use - you can also do this one with your door frame! You just need something to anchor yourself on to!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Place one foot up on a rig / post / doorframe as high as you can - make sure at least the ball of your foot is up so you're not just stretching your toes</p></li>
				<li><p class="mb-0">Use your arms to pull yourself more vertical - extend your hips as much as possible!</p></li>
				<li><p class="mb-0">If your setup allows it, wrap your non-stretching leg around your support and use it to pull yourself even closer, increasing the stretch in your calf and ankle even more!</p></li>
				<li><p class="mb-0">Either move forwards and backwards for a few reps, or hold for 20-30 seconds</p></li>
				<li><p class="mb-0">Swap feet, and repeat on the other side!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Bring your hips forwards as much as possible</p></li>
					<li><p>Make sure as much of your foot is on the post / doorframe as possible</p></li>
					<li><p>Try moving left and right as well to see how it affects the stretch!</p></li>
				</ul>
			</div>
			<p>Your ankles and feet get so much wear and tear every day, keep on top of them can be a constant battle!</p>
			<p><b><a href="https://www.youtube.com/watch?v=risrkOxeIw0" target="_blank"><u>Here's some more</u></a>  of our favourite ankle stretching exercises!</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Place one foot up on a rig / post / doorframe as high as you can - make sure at least the ball of your foot is up so you're not just stretching your toes</p></li>
				<li><p>Use your arms to pull yourself more vertical - extend your hips as much as possible!</p></li>
				<li><p>If your setup allows it, wrap your non-stretching leg around your support and use it to pull yourself even closer, increasing the stretch in your calf and ankle even more!</p></li>
				<li><p>Either move forwards and backwards for a few reps, or hold for 20-30 seconds</p></li>
				<li><p>Swap feet, and repeat on the other side!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="neck" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Neck</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VY_1XuT57Zw?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Lateral Neck Stretches</h3>
			<p class="mb-4"><b>These. Feel. Ah. Mazing. You're welcome. Necks get so tight after working and slouching a lot, but these quick exercises will sort you right out! Try them right now!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Drop your shoulders down as much as possible, stretching your traps</p></li>
				<li><p class="mb-0">Tip your head over to one side - make your shoulders don't creep back up!</p></li>
				<li><p class="mb-0">Keep the tilt and move your head up and down, as if you're saying 'yes'</p></li>
				<li><p class="mb-0">After 5-10 reps, (keeping the tilt!) start turning your head left and right, as if you're saying 'no'</p></li>
				<li><p class="mb-0">Repeat for 5-10 reps</p></li>
				<li><p class="mb-0">Then, tilt your head to the other side and repeat the two movements!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Keep your shoulders down as much as you can throughout</p></li>
					<li><p>Keep the head tilt as you move left / right / up / down</p></li>
					<li><p>If one side is tighter than the other, do a few more reps on that side!</p></li>
				</ul>
			</div>
			<p>A tight neck is often caused by tight traps, and one of the best ways to relax your traps is to get your lats working better!</p>
			<p><b><a href="https://www.youtube.com/watch?v=uRLKIqHMNDM" target="_blank"><u>Try out this simple lat activation drill</u></a> and see if your neck feels better afterwards!</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Drop your shoulders down as much as possible, stretching your traps</p></li>
				<li><p>Tip your head over to one side - make your shoulders don't creep back up!</p></li>
				<li><p>Keep the tilt and move your head up and down, as if you're saying 'yes'</p></li>
				<li><p>After 5-10 reps, (keeping the tilt!) start turning your head left and right, as if you're saying 'no'</p></li>
				<li><p>Repeat for 5-10 reps</p></li>
				<li><p>Then, tilt your head to the other side and repeat the two movements!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="knees" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Knees</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eZP2WQMx-IM?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Heel Sit</h3>
			<p class="mb-4"><b>Feel free to watch the whole video, or skip to <span class="text-primary">2:20</span> for one of the most important knee stretches ever! If you can't do this you're probably going to have knee issues down the line.</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Kneel down on the floor, either with your toes tucked or your feet pointed</p></li>
				<li><p class="mb-0">Lower your bum back (using your hands on the ground if you like) until your bum is as close to your heels as possible</p></li>
				<li><p class="mb-0">Stay there for 30 seconds or so</p></li>
				<li><p class="mb-0">If it's really uncomfortable and you're not quite at your heels yet, put some cushions between your heels and your bum to take off some of the pressure</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Put your hands on the floor to hold your weight if needed</p></li>
					<li><p>Put cushions under your bum to make it more comfortable to hold</p></li>
					<li><p>If you can't do this, keep trying! Watch TV in this position!</p></li>
				</ul>
			</div>
			<p>Knees are a hinge joint and often end up twisting out of position due to tight quads & hips or unstable ankles.</p>
			<p><b>We put some great info for bulletproof knees <a href="https://tommorrison.uk/blog/2019-11-28/3-steps-to-bulletproof-knees" target="_blank"><u>into this blog - check it out!</u></a> Your knees will thank you.</b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Kneel down on the floor, either with your toes tucked or your feet pointed</p></li>
				<li><p>Lower your bum back (using your hands on the ground if you like) until your bum is as close to your heels as possible</p></li>
				<li><p>Stay there for 30 seconds or so</p></li>
				<li><p>If it's really uncomfortable and you're not quite at your heels yet, put some cushions between your heels and your bum to take off some of the pressure</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div id="wholebody" class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center mb-5">
			<h2 class="large-title mb-4 text-capitalize">Whole Body!</h2>
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/iAszWy07yt8?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-lg-6">
			<h3 class="text-primary mb-4">Squat 'n' Reach</h3>
			<p class="mb-4"><b>If you're stuck for time, or just feel like you need a full body stretch - this is the one for you! You just need something to hold onto, a doorframe or a rig work perfectly!</b></p>
			<h3 class="mb-4 d-lg-none">How to do it</h3>
			<ul class="pl-3 mb-4 d-lg-none">
				<li><p class="mb-0">Hold onto a post / door frame and squat down low</p></li>
				<li><p class="mb-0">Lift one are higher on your support and start to rotate your torso to the ceiling - stretching your sides</p></li>
				<li><p class="mb-0">Use your elbows to push your knees out further</p></li>
				<li><p class="mb-0">Move yourself around to stretch your ankles and calves</p></li>
				<li><p class="mb-0">Switch which arm is overhead and repeat!</p></li>
			</ul>
			<div class="bg-primary border border-dark shadow p-4 text-white mb-4">
				<h4>Tips</h4>
				<ul class="pl-3">
					<li><p>Use the post as much as possible to increase your ranges of motion</p></li>
					<li><p>Be creative! Mess around and see how many different body parts you can stretch!</p></li>
					<li><p>Stretch for as long as you want!</p></li>
				</ul>
			</div>
			<p>Of course, this stretch doesn't actually hit the entire body.. that would be some weird position! But we do think it hits quite a lot.</p>
			<p><b>We've got another nice drill if you're still quite inflexible and want to start moving better, <a href="https://tommorrison.uk/blog/2019-11-04/easy-stretches-for-the-inflexible" target="_blank"><u>check it out here!</u></a></b></p>
		</div>
		<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5 d-none d-lg-block">
			<h3 class="mb-4">How to do it</h3>
			<ul class="pl-3">
				<li><p>Hold onto a post / door frame and squat down low</p></li>
				<li><p>Lift one are higher on your support and start to rotate your torso to the ceiling - stretching your sides</p></li>
				<li><p>Use your elbows to push your knees out further</p></li>
				<li><p>Move yourself around to stretch your ankles and calves</p></li>
				<li><p>Switch which arm is overhead and repeat!</p></li>
			</ul>
		</div>
		<div class="col-12"><hr class="my-5" /></div>
	</div>
</div>
<div class="container pb-5">
	<div class="row">
		<div class="col-12">
			<h2 class="text-center text-capitalize mb-4">Why should you stretch?</h2>
			<p>Stretching is one of those things: everyone knows they should do it more often... but usually <b>we only take an interest in stretching when something starts hurting</b> or becomes constantly tight.</p>
			<p>To be honest, stretching can be boring! And <b>if you're inflexible, it can be downright horrible.</b></p>
			<p>I know - coz I was one of those inflexible people! It wasn't until I got <b>seriously injured that I started working on my mobility.</b></p>
			<p>I think one reason why people don't stretch is <b>they don't realise just how good it makes you feel.</b></p>
			<p>No, not in a yoga, "at one with the universe" sort of way, but in a, <b>"wait a minute, I'm not sore, no where aches and I'm not grumpy"</b> sort of way!</p>
			<p>It's not until you become more flexible do you realise how <b>being tight genuinely affects your mood!</b></p>
			<p>Maybe that's why yoga people are so happy all the time...</p>
			<p>On top of a general vigour and spring in your step, you have the added benefits of:</p>
			<p>… and much more! In all my years of coaching, the <b>most common thing I see holding people back is a lack of flexibility.</b></p>
			<ul class="pl-3">
				<li><p>Less likely to get injured</p></li>
				<li><p>Less likely to suffer from random aches & pains</p></li>
				<li><p>Able to recover quicker from workouts</p></li>
				<li><p>Able to take part in more activities & sports without fear of snapping</p></li>
				<li><p>Able to keep up with your kids/nieces/nephews/grandkids</p></li>
				<li><p>Make progress faster</p></li>
			</ul>
			
			<p>And people just accept it!</p>
			<p>“My shoulders are so stiff”<br/>
			“I can’t do that; I have a bad back”<br/>
			“My hamstrings are just too tight for that”</p>
			<p>Etc.</p>
			<p>As if it’s something intrinsic, <b>something that’s just part of them and cannot be changed!</b></p>
			<p>What?!</p>
			<p>If someone told you that you had to grow another 50cm then I’d understand, but we’re talking about <b>something that can easily be worked on and improved!</b></p>
			<p>Let me tell you about THE BEST way to improve your flexibility…</p>
			<h2 class="mt-5 text-center text-capitalize">Do 5 Minutes, Every Day</h2>
		</div>
	</div>
</div>
<div class="container-fluid bg-primary text-white py-5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<p>Yup.</p>
					<p>That's it.</p>
					<p>5 minutes.</p>
					<p>Done.</p>
					<p><b>You don’t need to spend hours stretching</b>, getting all zen or beating yourself up with a foam roller… <b>you need to be consistent.</b></p>
					<p><b>I challenge you for the next 2 weeks</b> to wake up 5 minutes earlier (c’mon, don’t give me that, it’s only 5 minutes 😉), set a timer and “stretch” for 5 minutes.</p>
					<p>You can't do it wrong.</p>
					<p>I’m not going to give you any stretches to do, <b>you know loads already!</b></p>
					<p>If you’re stuck, just do circles! Circle your head, shoulders, elbows, wrists, hips, knees, ankles, etc.</p>
					<p>After 5 minutes, get on with your day and <b>notice how you feel a little better, stand a little taller, and generally feel a little bit more cheerful.</b></p>
					<p><b>Don’t make it complicated, don’t make it a big deal</b> – roll around in bed if you really can’t drag yourself out!</p>
					<p>Just take 5 minutes, run through your body, see what’s tight, see how you feel – did you do any training yesterday? <b>Does your body feel the effects today?</b></p>
					<p>Were you at a meeting all day and <b>your upper back is super tight?</b></p>
					<p><b>Just give yourself a little check for 5 minutes, then carry on.</b></p>
					<p>Trust me.</p>
					<p>If you’d like to find out a bit more about this and other principles we use to help people all over the world improve their flexibility, check out <a class="text-white " href="https://tommorrison.uk/products/the-simplistic-mobility-method" target="_blank"><u><b>The Simplistic Mobility Method</b></u></a>!</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container pt-5">
	<div class="row">
		<div class="col-12">
			<h2 class="text-center text-capitalize mb-4">Who Are We?</h2>
			<p>Tom & Jenni make complicated information about training and injury prevention fun and easy to understand.</p>
			<p><b>Tom Morrison</b> came into sports and fitness quite late compared to most, and his lack of sporting history led him to pick up multiple injuries including a major back injury. His journey to recovery has made him one of the top names for flexibility and stability information. He is the creator of the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method" target="_blank"><u>Simplistic Mobility Method</u></a>; helping people all over the world to improve their bodies; and contributes to many training magazines including Testosterone Nation, BOXROX and Breaking Muscle.</p>
			<p>Jenni has been into physical training for most of her life, and where Tom had zero flexibility Jenni had far too much. She has hypermobility syndrome, which means that her joints don’t have the natural stability they should, so she had to develop it through her own training. She never let the condition hold her back and successfully trains a wide range of disciplines, from Weightlifting to Hand Balancing.</p>
			<p>Together they <a href="https://tommorrison.uk/blog" target="_blank"><u>create and share content online</u></a> to help people deal with their own mobility/stability issues and travel all over to perform <a href="https://tommorrison.uk/seminars" target="_blank"><u>seminars & workshops</u></a> which are attended by all ages, abilities and interests - from regular gym go-ers to physiotherapists.</p>
			<p>They want everyone to know that their body is adaptable. <b>Pain is not normal</b> and issues that people just accept can be changed.</p>
			<picture> 
				<source srcset="/img/lps/tomjenniflexing.webp" type="image/webp"/> 
				<source srcset="/img/home/tomjenniflexing.jpg" type="image/jpeg"/>
				<img src="/img/home/tomjenniflexing.jpg" class="w-100 lazy" alt="Tom Morrison and Jenni flexing and smiling" />
			</picture>
		</div>
	</div>
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>

@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){

dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	$('.play-btn').click(function(){
		$(this).fadeOut();
		$('#bottom-video')[0].play();
	});
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection