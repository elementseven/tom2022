@php
$pagename = 'LP';
$page = 'Stretching Routine';
$pagetitle = "Stretching Routine - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great mobility drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row justify-content-center">
		<div class="col-lg-12 text-left text-lg-center">
			<h1 class="mt-5 mob-mt-3 mb-5 lp-title-smaller text-primary text-center">A simple stretching program to keep your body flexible & pain free</h1>
			<h3 class="mob-smaller-title" style="line-height: 2.4rem;">Tight hamstrings? Tight hips? Tight shoulders? Tight ankles…. Tight everything?!</h3>
			<p class="mb-4"><b>We’ve been there, and we know that stretching can be boring (and painful).</b></p>
			<p class="mb-4">So, we’ve created a simple routine that will increase the mobility in your entire body.</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5  pb-5 mob-py-0">
	<div class="row justify-content-center">
		<div class="col-12 mt-5 mob-my-0 text-center">
			<div class="embed-responsive embed-responsive-16by9">
			  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wDmuyeqsFPk?rel=0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-12 my-4 py-2">
			<p class="mb-0"><b>The Simplistic Mobility Method</b> does more than improve flexibility. It helps to balance your body, eliminate aches & niggles, prevent injuries, and genuinely reduce pain – even from old injuries!</p>
		</div>
		<div class="col-lg-12 mt-5 mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-lg-6 mob-mb-3">
					<div class="smm-slider ">
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-01.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 1">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-08.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 2">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery4.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-04.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-07.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 3">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 5">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-03.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 7">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 9">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-09.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 1">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-06.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 3">
						</li>
					</div>
				</div>
				<div class="col-12 text-center">
					<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block mw-300">Buy Now for @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif</div></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid mob-pt-3 my-5">
	<div class="row">
		<video-testimonials :amount="20"></video-testimonials>
	</div>
</div>
<div class="container-fluid bg-light pb-5 mob-py-0">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 mt-5 pt-5 mob-mt-0">
					<div class="row justify-content-center">
						<div class="col-lg-12">
							<h2 class="mb-4 text-capitalize">The Simplistic Mobility Method covers:</h2>
						</div>
						<div class="col-lg-6">
							<ul class="check-graphics">
								<li>
									<h3 class="text-primary">Upper Back & Posture</h3>
									<p class="">Aligns your shoulders and neck for pain-free movement.</p>
								</li>
								<li>
									<h3 class="text-primary">Bicep Openers & Lat Activation</h3>
									<p class="">To relax overactive muscles and activate lazy ones.</p>
								</li>
								<li>
									<h3 class="text-primary">Hip Flexibility</h3>
									<p class="">Works your hips through flexion, extension, rotation and laterally.</p>
								</li>
								<li>
									<h3 class="text-primary">Deep Knee Bends</h3>
									<p class="mob-mb-0">Learn how to load your hips without knee pain, while loosening & strengthening your quads.</p>
								</li>
							</ul>
						</div>
						<div class="col-lg-6">
							<ul class="check-graphics">
								<li>
									<h3 class="text-primary">Ankle ROM & Balance</h3>
									<p class="">Builds a good foundation - everything starts at the feet!</p>
								</li>
								<li><h3 class="text-primary">Core Activation + Limb Disassociation</h3>
									<p class="">Lock in your range of motion improvements with strength. This step is crucial to maintain your flexibility!</p>
								</li>
								<li><h3 class="text-primary">Body Awareness & Symmetry</h3>
									<p class="">You’ll be made aware of imbalances you may have and know exactly what you need to focus on. We include tests, markers & PDFs for you to keep track of your progress!</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5">
	<div class="row justify-content-center py-5 mob-py-0">
		<div class="col-lg-5">
			<p class="mb-0 mt-5 mob-mt-0 pt-4 mob-pt-0"><b>SMM</b> is so effective because it addresses all this in one 30-minute session. If you just passively stretch and do nothing else, it doesn’t become integrated to how you move.</p>
		</div>
		<div class="col-lg-4 mob-mt-5">
			<img src="/img/lps/sub-sticker_v.png" alt="Simplistic mobility method sticker image" class="w-100 px-5"/>
		</div>
	</div>
</div>
<div id="whatsincluded" class="bg-dark container-fluid bg-primary text-white py-5 mb-5">
	<div class="row py-5 mob-py-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-10">
					<h2 class="large-title text-nocase mb-4">“This all sounds great, but I wasn’t looking to buy a program!”</h2>
					<h3 class="">I hear you say.</h3>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container py-5 mob-py-0">
	<div class="row justify-content-center">
		<div class="col-lg-12">
			<p class="mb-4"><b>You could go back to your Google Search right now</b> and click on a "FREE PROGRAM" or follow a set of stretches in a nice gallery… and you’ll do it once or twice… then stop.</p>
			<p class="mb-4">I’m not being rude, I’m just speaking from experience. There’s no thought process, no reasoning... and often, no results.</p>
			<p class="mb-4">I want something that <b>gives you focus, goals to achieve and keeps you motivated.</b></p>
			<p class="">That’s what the Simplistic Mobility Method has, and that’s why it’s better than free content.</p>
		</div>
		<div class="col-12 text-center mb-5">
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block mw-300">Buy Now for @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif</div></a>
		</div>
		<div class="col-lg-7">
			<picture> 
				<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
				<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
				<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy mb-4" alt="Simplistic mobility method devices" />
			</picture>
		</div>
		<div class="col-lg-5 pt-5 mob-pt-0">
			<picture> 
				<source srcset="/img/lps/testimonial-5.webp" type="image/webp"/> 
				<source srcset="/img/lps/testimonial-5.jpg" type="image/jpeg"/>
				<img src="/img/lps/testimonial-5" class="img-fluid lazy mb-5" alt="Simplistic mobility method testimonial-5" />
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid bg-light py-5 mob-py-0">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="my-5 text-capitalize">More reasons why SMM is awesome</h2>
				</div>
				<div class="col-lg-6">
					<h3 class="text-primary">No Equipment Required</h3>
					<p class="mb-5">You use your own strength to create the flexibility (meaning you aren’t relying on external sources!).</p>
					<h3 class="text-primary">You Have it For Life</h3>
					<p class="mb-5">A one-off payment gives you access to SMM for life - plus automatic updates!</p>
					<h3 class="text-primary">Short & Simple to Follow</h3>
					<p class="mb-5">SMM works on basic principles. Getting really good at core concepts, rather than blasting through 100s of different exercises, is the best way to get results.</p>
					</div>
					<div class="col-lg-6">
					<h3 class="text-primary">Great for Beginners</h3>
					<p class="mb-5">We show you how to get better at the movements progressively so you'll always be able to complete SMM, no matter what your starting point!</p>
					<h3 class="text-primary">Fits Into Your Schedule</h3>
					<p class="mb-5">After your initial tests, SMM only takes around 30 minutes to do - and you don't need to do it every day! We recommend 3-4 times per week.</p>
					<h3 class="text-primary">It Works on All Devices</h3>
					<p class="mb-5">Whether you're tech savvy and hook us up to your Smart TV, or can just about work your phone, SMM will work on whatever device suits you!</p>
					</div>
			</div>
		</div>
	</div>
</div>
<div class="container mt-5 pt-5 mob-py-0">
	<div class="row justify-content-center">
		<div class="col-lg-10 text-center">
			<h3 class="mb-4 midsize text-capitalize">So now that's all out of the way, isn’t it time you got started?</h3>
			<a href="/basket/add/1"><div class="btn btn-primary mx-auto d-inline-block">Buy Now! Only @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif</div></a>
		</div>
	</div>
</div>
<div class="container pt-5">
	<div class="row">
		<div class="col-12">
			<hr style="border-bottom: 2px solid #3E4146 !important;" />
			<h2 class="text-center text-primary text-capitalize mb-4 mt-5">Who Are We?</h2>
			<p class="mb-4">Tom & Jenni make complicated information about training and injury prevention fun and easy to understand.</p>
			<p class="mb-4">Together they <a href="https://www.facebook.com/Movementandmotion" target="_blank"><u>create and share content online</u></a> to help people deal with their own mobility/stability issues and travel to <a href="/seminars" target="_blank"><u>perform seminars & workshops</u></a> which are attended by all ages, abilities and interests - from regular gym go-ers to physiotherapists.</p>
			<p class="mb-4">They want everyone to know that their body is adaptable. Pain is not normal and issues that people just accept can be changed.</p>
			<picture> 
				<source srcset="/img/lps/tomjenniflexing.webp" type="image/webp"/> 
				<source srcset="/img/home/tomjenniflexing.jpg" type="image/jpeg"/>
				<img src="/img/home/tomjenniflexing.jpg" class="w-100 lazy" alt="Tom Morrison and Jenni flexing and smiling" />
			</picture>
		</div>
	</div>
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){

dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	$('.play-btn').click(function(){
		$(this).fadeOut();
		$('#bottom-video')[0].play();
	});
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection