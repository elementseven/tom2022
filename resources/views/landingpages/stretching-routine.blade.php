@php
$pagename = 'LP';
$page = 'Stretching Routine';
$pagetitle = "Stretching Routine - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great mobility drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row justify-content-center">
		<div class="col-lg-12">
			<h1 class="mt-5 mob-mt-3 mb-5 lp-title-smaller text-primary text-center">A simple stretching routine to keep your body loose and pain free</h1>
			<h3 style="line-height: 2.4rem;">Tight hamstrings? Tight hips? Tight shoulders? Tight ankles…. Tight everything?!</h3>
			<p class="mb-4"><b>We’ve been there, and we know that stretching can be boring (and painful).</b></p>
			<p class="">So, we’ve created a simple routine that will help you stretch your entire body in around 20 minutes, without the normal boredom of sitting still and holding a pose for 3,000 years at a time. </p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-12 mt-5 mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-lg-6 mob-mb-3">
					<div class="smm-slider ">
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-01.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 1">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-03.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 3">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-05.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 5">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-07.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 7">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-09.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 9">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-01.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 1">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-03.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 3">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-05.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 5">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-07.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 7">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery2-09.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 9">
						</li>
					</div>
				</div>
				<div class="col-12 mt-5 mob-mb-3">
					<p class="">We all know stretching is good for us, but still it gets pushed to the edges of our gym time. If we’re lucky we’ll do a few reps of this, that or the other, without any intention or thought behind it.
					<p class="">Stretching does more than increase your flexibility. When done right, stretching helps to balance your body, eliminate aches & niggles, prevent injuries, and genuinely reduce pain – even from old injuries!</p>
				</div>
				<div class="col-12 mt-5 mob-my-0 text-center">
					<div class="embed-responsive embed-responsive-16by9">
					  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/wDmuyeqsFPk?rel=0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mt-5 pt-5 mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-lg-12">
					<h2 class="mb-5 large-title">The Simplistic Mobility Method covers:</h2>
				</div>
				<div class="col-lg-6">
					<h3 class="text-primary">Upper Back & Posture</h3>
					<p class="">To align your shoulders and neck for pain free movement</p>
					<h3 class="text-primary mt-5">Bicep Openers and Lat Activation</h3>
					<p class="">To relax overactive muscles and activate the important ones</p>
					<h3 class="text-primary mt-5">Hip Flexibility & Openers</h3>
					<p class="">Working your hips through flexion, extension, rotation and laterally</p>
					<h3 class="text-primary mt-5">Deep Knee Bends</h3>
					<p class="mob-mb-0">To teach your body how to load the hips instead of the knees or back while loosening and strengthen the quads</p>
				</div>
				<div class="col-lg-6">
					

					<h3 class="text-primary mob-mt-5">Ankle Range of Motion & Balance</h3>
					<p class="">Building a good foundation to avoid compensations upstream in the rest of your body - everything starts at the feet!</p>
					<h3 class="text-primary mt-5">Deep Core Activation + Limb Disassociation</h3>
					<p class="">To help to lock in the improvements to your range of motion with strength so that your body is less likely to regress. This step is so important to easily maintain your flexibility!</p>

					<h3 class="text-primary mt-5">Body awareness and Symmetry</h3>
					<p class="">As you do the exercises, you’ll be made aware of every imbalance you may have and know exactly what you need to focus on. We include tests, markers & PDFs for you to keep track of your progress!</p>
				</div>
				<div class="col-12 mt-5">
					<p class="mb-4">The secret that makes SMM so effective is it addresses all these factors in the same 20-minute session. If you just passively stretch and do nothing else, it doesn’t become integrated to how you actually move – unless you’re specialising in something like <a href="https://tommorrison.uk/products/splits-and-hips" target="_blank">the splits</a> it’s actually pretty time wasting to do individual passive mobility drills.</p>
					<p class="mb-4"><b>I went down the foam rolling route, the physio route, the 2 hour long stretching sessions route - and now? I don’t teach any other way.</b></p>
					<p class="">Treat your body as a complete unit that needs to be stretched, strengthened & stabilised and you’ll make so much more progress.</p>
				</div>
				<div class="col-12 mt-4 text-center">
					<a href="javascript:void(0)" onclick="addToBasket(1,'The Simplistic Mobility Method','Foundation',{{$price}});window.location='/basket/add/1'"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
				</div>
			</div>
		</div>

	</div>
	
</div>
<div id="whatsincluded" class="container-fluid bg-primary text-white py-5 mb-5">
	<div class="row my-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<h2 class="large-title text-nocase mb-4">“This all sounds great, but I wasn’t looking to buy a program!”</h2>
					<h3 class="">I hear you say.</h3>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-12">
			<p class="mb-4">To which I respond… there’s so much free content out there. An overwhelming amount. </p>

			<p class="mb-4"><b>You could go back to your Google Search right now</b> and click on another link with a FREE PROGRAM or arbitrary set of stretches listed in a nice gallery… and you’ll do it once or twice… then stop.</p>

			<p class="mb-4">I’m not just being rude; I’m just speaking from experience: <b>You will never build a successful plan by yourself</b>. There’s no thought process, no intention, no reasoning... and often, no results.</p>

			<p class="mb-4"><b>I want something that makes people move better right now and that gives them focus, markers to achieve and keeps them motivated to want to keep getting stronger forever.</b></p>

			<p class="">That’s what the Simplistic Mobility Method has, and that’s why it’s better than the free content out there.</p>

		</div>
		<div class="col-lg-6 my-5">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery1-02.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 2">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery1-04.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery1-06.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 6">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery1-08.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 8">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery1-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 10">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-02.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 2">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-04.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 4">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-06.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 6">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-08.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 8">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 10">
				</li>
			</div>
		</div>
		<div class="col-12">
			<p class="mb-5"><b>The Simplistic Mobility Method takes into account all the obstacles which might prevent you from sticking to a program:</b></p>

			<h3 class="text-primary">The exercises require no equipment.</h3>
			<p class="mb-5">It teaches you how to anchor your body in positions, so you use your own strength to create the flexibility (meaning you aren’t relying on external sources to push your muscles)</p>

			<h3 class="text-primary">It’s short and simple to follow.</h3>
			<p class="">“I fear not the man who has practiced 10,000 kicks once, but I fear the man who has practiced one kick 10,000 times.” - Bruce Lee</p>

			<p class="mb-5">SMM works on basic principles, and getting really good at a handful core concepts, rather than skimming over 100s of different exercises is the way to achieve the best results.</p>

			<h3 class="text-primary">It’s suitable for beginners/inflexible people</h3>
			<p class="mb-5">I was one of the most inflexible injury prone people there ever was. We show you how to get better at the movements progressively so you will always be able to work on all areas of your body from the start!</p>

			<h3 class="text-primary">It’s quick</h3>
			<p class="mb-5">Your first session will take a little bit longer as you figure out all the movements, but after that SMM only takes 20 minutes to do! And you don’t need to do it every day! We recommend about 3-4 times per week</p>

			<h3 class="text-primary">It costs less than 1 physio session</h3>
			<p class="mb-5">People are always looking for a free option, without thinking of the long-term costs. Not working on your movement & mobility now could cost you hundreds in physio/chiro/doctor bills, even buying yourself foam rollers & balls adds up. SMM requires none of that, prevents injuries and promotes physical longevity. Plus, it’s a one-off cost. No monthly subscriptions, no surprise fees – just a onetime purchase</p>
			</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-8">
			<picture> 
				<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
				<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
				<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy" alt="Simplistic mobility method devices" />
			</picture>
		</div>
		<div class="col-lg-10 text-center">
			<h2 class="mb-5 large-title text-center">So now that's all out of the way, isn’t it time you got started?</h2>
			<a href="javascript:void(0)" onclick="addToBasket(1,'The Simplistic Mobility Method','Foundation',{{$price}});window.location='/basket/add/1'"><div class="btn btn-primary mx-auto d-inline-block mb-5">Buy Now! Only £{{$price}}</div></a>
		</div>
	</div>
</div>
<div class="container pt-5">
	<div class="row">
		<div class="col-12">
			<h2 class="text-center text-capitalize mb-4">Who Are We?</h2>
			<p class="mb-4">Tom & Jenni make complicated information about training and injury prevention fun and easy to understand.</p>
			<p class="mb-4"><b>Tom Morrison</b> came into sports and fitness quite late compared to most, and his lack of sporting history led him to pick up multiple injuries including a major back injury. His journey to recovery has made him one of the top names for flexibility and stability information. He is the creator of the <a href="https://tommorrison.uk/products/the-simplistic-mobility-method" target="_blank"><u>Simplistic Mobility Method</u></a>; helping people all over the world to improve their bodies; and contributes to many training magazines including Testosterone Nation, BOXROX and Breaking Muscle.</p>
			<p class="mb-4">Jenni has been into physical training for most of her life, and where Tom had zero flexibility Jenni had far too much. She has hypermobility syndrome, which means that her joints don’t have the natural stability they should, so she had to develop it through her own training. She never let the condition hold her back and successfully trains a wide range of disciplines, from Weightlifting to Hand Balancing.</p>
			<p class="mb-4">Together they <a href="https://tommorrison.uk/blog" target="_blank"><u>create and share content online</u></a> to help people deal with their own mobility/stability issues and travel all over to perform <a href="https://tommorrison.uk/seminars" target="_blank"><u>seminars & workshops</u></a> which are attended by all ages, abilities and interests - from regular gym go-ers to physiotherapists.</p>
			<p>They want everyone to know that their body is adaptable. <b>Pain is not normal</b> and issues that people just accept can be changed.</p>
			<picture> 
				<source srcset="/img/lps/tomjenniflexing.webp" type="image/webp"/> 
				<source srcset="/img/home/tomjenniflexing.jpg" type="image/jpeg"/>
				<img src="/img/home/tomjenniflexing.jpg" class="w-100 lazy" alt="Tom Morrison and Jenni flexing and smiling" />
			</picture>
		</div>
	</div>
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){

dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	$('.play-btn').click(function(){
		$(this).fadeOut();
		$('#bottom-video')[0].play();
	});
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection