@php
$pagename = 'LP';
$page = 'Share your journey | The Simplistic Mobility Method - Tom Morrison';
$pagetitle = "Share your journey | The Simplistic Mobility Method - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<h1 class="mt-5 mob-mt-3 mb-5 lp-title text-primary text-center">Share Your Journey!</h1>
			<p class="text-large mob-text-smaller">We love to show off the progress people make, from being able to put their socks on again to getting a new PB - it's all awesome to see!</p>
			<p class="text-large mob-text-smaller">So, as you're at the start of your SMM journey, you're in a great place to record your experience right from the beginning.</p>
			<p class="text-large mob-text-smaller"><b>If you're interested, just submit your email address here and Tom will get in touch with you to run through what you need to do:</b></p>
		</div>
		<div class="col-lg-8 mt-4">
			@if($currentUser != null)
			<share-journey :user="{{$currentUser}}"></share-journey>
			@else
			<share-journey :user="null"></share-journey>
			@endif
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container py-5">
	<div class="row">
		<div class="col-12 mb-5">
			<p class="text-large mob-text-smaller">To give you a heads up so you know what to expect, here's what we'll ask you for:</p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7 mob-px-3 pr-5">
			<h3 class="mb-3 text-primary">Week 1:</h3>
			<h4 class="mb-3 larger">1. A few videos of your Tests</h4>
			<p class="text-large mob-text-smaller">Quick videos of a couple of Tests, ideally from your very first go through them! You don't need to video them all, just a couple such as the Overhead Squat, Deep Lunge Test & Overhead Reach, for example.</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week1-exercises1.jpg" alt="tom morrison week1-exercise" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-exercises2.jpg" alt="tom morrison week1-exercise" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-exercises3.jpg" alt="tom morrison week1-exercise" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-3 mob-mt-5">
		<div class="col-lg-7 pr-5 mob-px-3 pt-3">
			<h4 class="mb-3 larger">2. A few videos of some Exercises</h4>
			<p class="text-large mob-text-smaller">Again, just a few quick clips of you doing some of the exercises. Even pick the ones you're struggling with to really help see your progress!</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week1-tests1.jpg" alt="tom morrison week1-tests1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-tests2.jpg" alt="tom morrison week1-tests2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-tests3.jpg" alt="tom morrison week1-tests-3" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-lg-7 pr-5 mob-px-3 pt-3">
			<h4 class="mb-3 larger">3. Around a 1 min video of why you got SMM & initial thoughts</h4>
			<p class="text-large mob-text-smaller">Do you have pain? Are you recovering from an injury? Hit a plateau in your training? Or just full of niggles? Have you discovered any immediate limitations or imbalances from your first session?</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week1-face1.jpg" alt="tom morrison week1-face1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-face2.jpg" alt="tom morrison week1-face2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week1-face3.jpg" alt="tom morrison week1-face3" class="w-100">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 mt-5">
	<div class="row">
		<div class="col-lg-7 mob-px-3 pr-5">
			<h3 class="mb-3 text-primary">Week 4:</h3>
			<h4 class="mb-3 larger">1. A few videos of your Retests</h4>
			<p class="text-large mob-text-smaller">Quick videos of the same tests you recorded on week 1 from the same angles to show your progress!</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week4-exercises1.jpg" alt="tom morrison week4-exercise" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-exercises2.jpg" alt="tom morrison week4-exercise" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-exercises3.jpg" alt="tom morrison week4-exercise" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-3 mob-mt-5">
		<div class="col-lg-7 pr-5 mob-px-3 pt-3">
			<h4 class="mb-3 larger">2. A few videos of some Exercises</h4>
			<p class="text-large mob-text-smaller">And again, just a few quick clips of the same exercises you did in week 1 to see your improvements!</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week4-tests1.jpg" alt="tom morrison week4-tests1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-tests2.jpg" alt="tom morrison week4-tests2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-tests3.jpg" alt="tom morrison week4-tests-3" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-lg-7 pr-5 mob-px-3 pt-3">
			<h4 class="mb-3 larger">3. Around a 1 min video of how it's been and anything you've noticed</h4>
			<p class="text-large mob-text-smaller">Have you felt an improvement? Have your pains been reduced or performance increased? How did you find the program overall?</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week4-face1.jpg" alt="tom morrison week4-face1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-face2.jpg" alt="tom morrison week4-face2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week4-face3.jpg" alt="tom morrison week4-face3" class="w-100">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 mt-5">
	<div class="row">
		<div class="col-lg-7 mob-px-3 pr-5">
			<h3 class="mb-3 text-primary">Week 8:</h3>
			<h4 class="mb-3 larger">1. Around a 1 min progress update</h4>
			<p class="text-large mob-text-smaller">How has SMM helped you after 8 weeks, still working on stuff or feel like you're reborn?!</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week8-update1.jpg" alt="tom morrison week8-update1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week8-update2.jpg" alt="tom morrison week8-update2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week8-update3.jpg" alt="tom morrison week8-update3" class="w-100">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-3 mob-mt-5">
		<div class="col-lg-7 pr-5 mob-px-3 pt-3">
			<h4 class="mb-3 larger">2. Training clips/new skills/etc!</h4>
			<p class="text-large mob-text-smaller">Show us what you're up to! Any new PBs? Learnt any new skills? Able to go back to an old hobby? Playing with the kids with ease? Show us what you're up to!</p>
		</div>
		<div class="col-lg-5">
			<div class="row">
				<div class="col-4">
					<img src="/img/lps/share/week8-skills1.jpg" alt="tom morrison week8-skills1" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week8-skills2.jpg" alt="tom morrison week8-skills2" class="w-100">
				</div>
				<div class="col-4">
					<img src="/img/lps/share/week8-skills3.jpg" alt="tom morrison week8-skills3" class="w-100">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 mb-5">
	<div class="row justify-content-center">
		<div class="col-12 text-center mb-5 mob-mb-0">
			<h3 class="text-primary mb-5 bigger">And that's it!</h3>
			<p class="text-large mob-text-smaller">You'll be sending these videos directly to Tom, so you'll be able to chat with him the whole time about how you're getting on and if you have any problems.</p>
			<p class="text-large mob-text-smaller">Got questions? Just send Tom an email at <a href="mailto:support@tommorrison.uk">support@tommorrison.uk</a></p>
			<p class="text-large mob-text-smaller"><b>Or if you're ready to get started, submit your email address here:</b></p>
		</div>
		<div class="col-lg-8">
			@if($currentUser != null)
			<share-journey :user="{{$currentUser}}"></share-journey>
			@else
			<share-journey :user="null"></share-journey>
			@endif
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
@endsection