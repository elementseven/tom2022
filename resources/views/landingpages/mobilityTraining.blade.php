@php
$page = 'Reduce Pain, Increase Mobility';
$pagename = 'LP';
$pagetitle = "Reduce Pain, Increase Mobility | The Simplistic Mobility Method | Tom Morrison";
$meta_description = "The Simplistic Mobility Method. A complete program to improve your flexibility & stability. No equipment required!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@section('styles')
<style>
	#content{
		padding-top: 0 !important;
	}
</style>
@endsection
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'nomenuatall' => true, 'pagename' => $pagename])
@section('header')
<header class="video-container video-container-600 position-relative athlete-header bg">
	<video class="header-video d-none d-md-block z-0" autoplay="true" playsinline muted loop>
		<source src="/vid/athlete.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
		<div class="video-trans"></div>
		<div class="container-fluid video-container-content text-white pt-4 mob-pt-5">
			<div class="row pt-5">
				<div class="container pt-5 mob-pt-4">
					<div class="row justify-content-center">
						<div class="col-lg-10 text-center mob-pt-5">
							<h1 class="mt-5 mob-mt-3 mb-2 lp-title-smaller mob-bigger text-center"><span class="text-primary">REDUCE PAIN,</span><br class="d-md-none"/> INCREASE MOBILITY</h1>
						</div>
						<div class="col-lg-7 text-center">
							<p class="mb-0 text-large mob-text-smaller">Simple & effective mobility program to enhance your movement and increase your performance.</p>
							<a href="/basket/add/1"><div class="btn btn-primary mt-4 mob-mt-5 mx-auto d-inline-block">Get Started</div></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	@endsection
	@section('content')
	<div class="container-fluid mob-pt-3 mb-5 py-5 mob-mb-0 bg-primary" style="min-height: 262px;">
		<div class="row">
			<div class="container">
				<div class="row text-center">
					<div class="col-12">
						<p class="text-large text-white">Featured on:</p>
					</div>
					<div class="col-12 d-none d-lg-block">
						<img src="/img/lps/mobility-training/logos.svg?v2.0"class="w-100" alt="Tom featured logos"/>
					</div>
					<div class="col-11 d-lg-none">
						<img src="/img/lps/mobility-training/logos-mob.svg?v2.0"class="w-100" alt="Tom featured logos"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container mb-5 mob-pb-0">
		<div class="row py-5">
			<div class="col-lg-6 mob-mb-5">
				<h2 class="text-capitalize mob-text-smaller mb-3">The Simplistic Mobility Method<span class="tm">&reg;</span></h2>
				<p>Join over 35,000 people worldwide who are regaining control of their body. Niggles, aches, pains and injuries stop you from being consistent with your training and your performance can suffer.</p>
				<p>Using SMM, you can fix imbalances, increase flexibility, build stability and prevent injuries, allowing you to stay consistent and get back to the fun stuff: getting stronger, fitter & faster.</p>
			</div>
			<div class="col-lg-6 mob-px-0">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/y5NnV84IsKE?rel=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div class="row text-center mt-5">
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/stretching.svg" height="65" alt="Stretching"/>
				<h3 class="text-capitalize mt-3">Full Body</h3>
				<p class="text-large mob-text-small mb-0">Everything you need in one program</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/track.svg" height="65" alt="Track Progress"/>
				<h3 class="text-capitalize mt-3">Track Progress</h3>
				<p class="text-large mob-text-small mb-0">Built in tests keep you focused</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/nosub.svg" height="65" alt="No Subscription"/>
				<h3 class="text-capitalize mt-3">No Subscription</h3>
				<p class="text-large mob-text-small mb-0">One-time payment, lifetime access</p>
			</div>
			<div class="col-lg-3 col-6 mob-mb-5 scroll_fade" data-fade="fadeInUp">
				<img src="/img/lps/athlete/alld.svg" height="65" alt="All Devices"/>
				<h3 class="text-capitalize mt-3">All Devices</h3>
				<p class="text-large mob-text-small mb-0">Phone, tablet, laptop, PC or TV. Even offline!</p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row justify-content-center py-5 mob-py-0">
			<div class="col-lg-7 mob-px-0 weird-height-thing">
				<div class="smm-slider-nav ">
					<li style="list-style: none;">
						<picture> 
							<source srcset="/img/lps/athlete/athlete-gallery1.webp" type="image/webp"/> 
							<source srcset="/img/lps/athlete/athlete-gallery1.jpg" type="image/jpeg"/>
							<img src="/img/lps/athlete/athlete-gallery1.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 1" />
						</picture>
					</li>
					<li style="list-style: none;">
						<picture> 
							<source srcset="/img/lps/athlete/athlete-gallery2.webp" type="image/webp"/> 
							<source srcset="/img/lps/athlete/athlete-gallery2.jpg" type="image/jpeg"/>
							<img src="/img/lps/athlete/athlete-gallery2.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 2" />
						</picture>
					</li>
					<li style="list-style: none;">
						<picture> 
							<source srcset="/img/lps/athlete/athlete-gallery3.webp" type="image/webp"/> 
							<source srcset="/img/lps/athlete/athlete-gallery3.jpg" type="image/jpeg"/>
							<img src="/img/lps/athlete/athlete-gallery3.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 3" />
						</picture>
					</li>
				</div>
			</div>
		</div>
	</div>
	<div id="mobility-stats-counters" class="container py-5">
		<div class="row text-center justify-content-center">
			<div class="col-lg-9">
				<div class="row text-center">
					<div class="col-lg-3 col-6 mob-mb-3">
						<div class="circle-counter counter col_fourth">
							<h2><span class="timer count-title count-number" data-to="35000" data-speed="1500"></span>+</h2>
							<p class="count-text ">Happy Users</p>
						</div>
					</div>
					<div class="col-lg-3 col-6 mob-mb-3">
						<div class="circle-counter counter col_fourth">
							<h2 class="timer count-title count-number" data-to="98" data-speed="1500"></h2>
							<p class="count-text ">Countries</p>
						</div>
					</div>
					<div class="col-lg-3 col-6 mob-mb-3">
						<div class="circle-counter counter col_fourth">
							<h2><span class="timer count-title count-number" data-to="5" data-speed="1500"></span> <img src="/img/icons/star-white.svg" alt="star" width="30" height="30" class="counters-star" style="margin-top: -10px;" /></h2>
							<p class="count-text ">Trusted<br/>Reviews</p>
						</div>
					</div>
					<div class="col-lg-3 col-6 mob-mb-3">
						<div class="circle-counter counter col_fourth end">
							<h2 class="timer count-title count-number" data-to="30" data-speed="1500"></h2>
							<p class="count-text ">Minute<br/>Routine</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container my-5 mob-my-0">
		<div class="row">
			<div class="col-lg-6 mob-mb-5">
				<h2 class="text-capitalize mob-text-smaller mb-0">Move Better, Train Better</h2>
				<p>SMM is built on foundational movement principles, so it works with every body, no matter what your starting level.</p>
				<p>You complete the 30 minute routine 2-4x a week, so it easily fits into your busy schedule.</p>
				<p>We give you the tools, all you have to do is put the work in. Don't avoid things that are hard or uncomfortable, that's where the results lie.</p>
			</div>
			<div class="col-lg-6">
				<picture> 
					<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
					<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
					<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy mb-4" alt="Simplistic mobility method devices" />
				</picture>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-12 mt-5 mb-5">
						<div class="row justify-content-center">
							<div class="col-lg-12">
								<h2 class="mb-4 text-capitalize">What You Get:</h2>
							</div>
							<div class="col-lg-6">
								<ul class="check-graphics mob-mb-0">
									<li>
										<p class="mb-0 larger">In-Depth SMM E-book</p>
									</li>
									<li>
										<p class="mb-0 larger">13 Movement Routines</p>
									</li>
									<li>
										<p class="mb-0 larger">Series of Tests & Assessments</p>
									</li>
									<li>
										<p class="mb-0 larger">Comprehensive Demo Videos</p>
									</li>
									<li>
										<p class="mb-0 larger">Beginner Versions of Every Exercise</p>
									</li>
									<li>
										<p class="mb-0 larger">Progress Tracker & 'Cheat Sheet' PDFs</p>
									</li>
									<li>
										<p class="mb-0 larger">Support From the SMM Community</p>
									</li>
								</ul>
							</div>
							<div class="col-lg-6 pl-5 mob-px-3 mob-mt-5">
								<div class="row justify-content-center text-center">
									<div class="col-lg-10">
										<h3 class="text-large mob-text-small mb-3">For a one-off payment of <span class="text-primary">£{{$product->price}}</span></h3>
										<p class="text-small">(Other currencies accepted! Payments are converted at the current rate)</p>
										<a href="/basket/add/1"><div class="btn btn-primary mt-2 mb-3 mx-auto d-inline-block">Buy Now</div></a>
										<div class="col-12 text-center"> <img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" id="tom-footer-top" alt="PayPal Logo" width="86" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/paypal.svg" src="/img/logos/paypal.svg" data-was-processed="true"> <img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" id="tom-footer-top" alt="Mastercard Logo" width="36" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/mastercard.svg" src="/img/logos/mastercard.svg" data-was-processed="true"> <img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" id="tom-footer-top" alt="Visa Logo" width="47" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/visa.svg" src="/img/logos/visa.svg" data-was-processed="true"> <img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" id="tom-footer-top" alt="American Express Logo" width="33" class="img-fluid lazy mob-mb-0 loaded" srcset="/img/logos/amex.svg" src="/img/logos/amex.svg" data-was-processed="true"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container py-5">
		<h2 class="text-primary text-capitalize">Reviews for the Simplistic Mobility Method<span class="tm">&reg;</span></h2>
		<div class="card-columns">
			@foreach($reviews as $review)
			<div class="card mt-5 mb-3">
				<div class="review-box bg-light p-3">
					<div class="review-avatar smaller">
						<img src="{{$review->avatar}}" alt="{{$review->name}}, Simplistic mobility method (SMM) review">
					</div>
					<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
					<div class="review-body"> 
						{!!$review->content!!}
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="row mt-5">
			<div class="col-12 text-center">
				<a href="/simplistic-mobility-method-reviews">
					<div class="btn btn-outline d-inline-block">See more reviews</div>
				</a>
			</div>
		</div>
	</div>
	@endsection
	@section('scripts')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
	<script>
		function addToBasket(id, name, category, price){
			dataLayer.push({
				'event': 'addToCart',
				'ecommerce': {
					'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
	'name': name,
	'id': id,
	'price': price,
	'brand': "Tom Morrison",
	'category': category,
	'quantity': 1
}]
}
}
});
			fbq('track', 'AddToCart', {
				value: {{$product->price}},
				currency: 'GBP'
			});
			console.log('done');
		}
		$(document).ready(function(){
			if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	    backToTop = function () {
	    	var scrollTop = $(window).scrollTop();
	    	if (scrollTop > scrollTrigger) {
	    		$('#back-to-top').addClass('show');
	    	} else {
	    		$('#back-to-top').removeClass('show');
	    	}
	    };
	    backToTop();
	    $(window).on('scroll', function () {
	    	backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	    	e.preventDefault();
	    	$('html,body').animate({
	    		scrollTop: 0
	    	}, 700);
	    });
	  }
	});
		window.addEventListener('load', (event) => {
		var inview = new Waypoint({
			element: $('#mobility-stats-counters'),
			handler: function (direction) {
				(function ($) {
					$.fn.countTo = function (options) {
						options = options || {};

						return $(this).each(function () {
					// set options for current element
					var settings = $.extend({}, $.fn.countTo.defaults, {
						from:            $(this).data('from'),
						to:              $(this).data('to'),
						speed:           $(this).data('speed'),
						refreshInterval: $(this).data('refresh-interval'),
						decimals:        $(this).data('decimals')
					}, options);
					
					// how many times to update the value, and how much to increment the value on each update
					var loops = Math.ceil(settings.speed / settings.refreshInterval),
					increment = (settings.to - settings.from) / loops;
					
					// references & variables that will change with each update
					var self = this,
					$self = $(this),
					loopCount = 0,
					value = settings.from,
					data = $self.data('countTo') || {};
					
					$self.data('countTo', data);
					
					// if an existing interval can be found, clear it first
					if (data.interval) {
						clearInterval(data.interval);
					}
					data.interval = setInterval(updateTimer, settings.refreshInterval);
					
					// initialize the element with the starting value
					render(value);
					
					function updateTimer() {
						value += increment;
						loopCount++;
						
						render(value);
						
						if (typeof(settings.onUpdate) == 'function') {
							settings.onUpdate.call(self, value);
						}
						
						if (loopCount >= loops) {
							// remove the interval
							$self.removeData('countTo');
							clearInterval(data.interval);
							value = settings.to;
							
							if (typeof(settings.onComplete) == 'function') {
								settings.onComplete.call(self, value);
							}
						}
					}
					
					function render(value) {
						var formattedValue = settings.formatter.call(self, value, settings);
						$self.html(formattedValue);
					}
				});
					};

					$.fn.countTo.defaults = {
				from: 0,               // the number the element should start at
				to: 0,                 // the number the element should end at
				speed: 1000,           // how long it should take to count between the target numbers
				refreshInterval: 100,  // how often the element should be updated
				decimals: 0,           // the number of decimal places to show
				formatter: formatter,  // handler for formatting the value before rendering
				onUpdate: null,        // callback method for every time the element is updated
				onComplete: null       // callback method for when the element finishes updating
			};
			
			function formatter(value, settings) {
				return value.toFixed(settings.decimals);
			}
		}(jQuery));

				jQuery(function ($) {
		  // custom formatting example
		  $('.count-number').data('countToOptions', {
		  	formatter: function (value, options) {
		  		return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
		  	}
		  });
		  
		  // start all the timers
		  $('.timer').each(count);  
		  
		  function count(options) {
		  	var $this = $(this);
		  	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		  	$this.countTo(options);
		  }
		});
			},offset: '90%'
		});
});

	</script>
	@endsection