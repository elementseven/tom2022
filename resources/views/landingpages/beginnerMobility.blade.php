@php
$page = 'Move Better Feel Better';
$pagename = 'LP';
$pagetitle = "Move Better Feel Better | The Simplistic Mobility Method | Tom Morrison";
$meta_description = "The Simplistic Mobility Method. A complete program to improve your flexibility & stability. No equipment required!";
$og_image = 'https://tommorrison.uk/img/og.jpg';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'nomenuatall' => true, 'pagename' => $pagename])
@section('head_section')
<style>
#smm-slider-nav-container .bx-viewport{height: auto !important;}
</style>
@endsection
@section('header')
<header class="video-container video-container-600 position-relative beginner-header bg">
	<video class="header-video d-none d-md-block z-0" autoplay="true" playsinline muted loop>
		<source src="/vid/people.mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video>
	<div class="video-trans"></div>
	<div class="container-fluid video-container-content text-white pt-4">
		<div class="row pt-5">
			<div class="container pt-5 mob-pt-4">
				<div class="row justify-content-center ">
					<div class="col-lg-8 text-center">
						<h1 class="mt-5 mob-mt-3 mb-2 lp-title-smaller mob-bigger text-center"><span class="text-primary">MOVE BETTER,</span><br class="d-md-none"/> FEEL BETTER</h1>
						<p class="mb-0 text-large mob-text-smaller mob-mt-3"><b>The Simplistic Mobility Method.</b></p>
						<p class="mb-0 text-large mob-text-smaller">A complete program to improve your flexibility & stability. No equipment required!</p>
						<a href="/basket/add/1"><div class="btn btn-primary mt-4 mob-mt-5 mx-auto d-inline-block">Get Started</div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="mt-5"></div>
<video-testimonials :amount="20"></video-testimonials>
<div class="container mb-5 mob-pb-0">
	<div class="row">
		<div class="col-lg-6 mob-mb-5">
			<h2 class="text-primary text-capitalize mob-text-smaller mb-0">The Simplistic Mobility Method</h2>
			<p class="text-large mob-text-small"><b>It's not okay to be in pain!</b></p>
			<p>SMM helps you to take back control of your body, getting you moving using simple but effective exercises that are suitable for even the most inflexible people.</p>
			<p>When you move better, you feel better! Reduce pain, relieve injuries, and get back to doing what you love.</p>
		</div>
		<div class="col-lg-6">
			<picture> 
				<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
				<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
				<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy mb-4" alt="Simplistic mobility method devices" />
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid bg-light">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 mt-5 mb-5">
					<div class="row justify-content-center">
						<div class="col-lg-12">
							<h2 class="mb-4 text-capitalize">What You Get:</h2>
						</div>
						<div class="col-lg-4">
							<ul class="check-graphics mob-mb-0">
								<li>
									<p class="mb-0">In-Depth SMM E-book</p>
								</li>
								<li>
									<p class="mb-0">A 13 Movement Routine</p>
								</li>
								<li>
									<p class="mb-0">Series of Tests & Assessments</p>
								</li>
								<li>
									<p class="mb-0">Comprehensive Demo Videos</p>
								</li>
							</ul>
						</div>
						<div class="col-lg-4">
							<ul class="check-graphics">
								<li>
									<p class="mb-0">Beginner Versions of Every Exercise</p>
								</li>
								<li>
									<p class="mb-0">Progress Tracker & 'Cheat Sheet' PDFs</p>
								</li>
								<li>
									<p class="mb-0">Support From the SMM Community</p>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 pl-5 mob-px-3">
							<img src="/img/lps/sub-sticker_v.png" alt="Simplistic mobility method sticker image" class="w-100 px-5"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5">
	<h2 class="text-primary text-capitalize mob-text-smaller">Reviews for the Simplistic Mobility Method<span class="tm">&reg;</span></h2>
	<div class="card-columns">
		@foreach($reviews as $review)
		<div class="card mt-5 mb-3">
			<div class="review-box bg-light p-3">
				<div class="review-avatar smaller">
					<img src="{{$review->avatar}}" alt="{{$review->name}}, Simplistic mobility method (SMM) review">
				</div>
				<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
				<div class="review-body"> 
					{!!$review->content!!}
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="row mt-5">
		<div class="col-12 text-center">
			<a href="/simplistic-mobility-method-reviews">
				<div class="btn btn-outline d-inline-block">See more reviews</div>
			</a>
		</div>
	</div>
</div>
{{-- <div id="smm-slider-nav-container" class="container py-5 mob-py-0">
	<div class="row justify-content-center py-5">
		<div class="col-lg-7">
			<div class="smm-slider-nav">
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery1.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery1.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery1.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 1" />
					</picture>
				</li>
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery2.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery2.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery2.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 2" />
					</picture>
				</li>
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery3.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery3.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery3.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 3" />
					</picture>
				</li>
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery4.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery4.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery4.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 4" />
					</picture>
				</li>
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery5.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery5.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery5.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 5" />
					</picture>
				</li>
				<li style="list-style: none;">
					<picture> 
						<source srcset="/img/lps/beginner/people-gallery6.webp" type="image/webp"/> 
						<source srcset="/img/lps/beginner/people-gallery6.jpg" type="image/jpeg"/>
						<img src="/img/lps/beginner/people-gallery6.jpg" class="w-100 lazy" alt="Simplistic Mobility Method Gallery image 6" />
					</picture>
				</li>
			</div>
		</div>
	</div>
</div> --}}
<div class="container-fluid bg-light py-5 mob-py-0">
	<div class="row py-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 text-left text-lg-center">
					<h2 class="mb-4 text-capitalize">Anyone can do SMM!</h2>
					<p class="larger">The beauty of SMM is that it’s designed for everyone, and can be done anywhere. In your home, the gym - anywhere at all!</p>
					<p class="larger">You don’t need any experience, whether you’re totally new to movement training, or you want to improve your techniques, we have everything you need.</p>
					<p class="larger mb-0">Movement is the most important part of knowing how your body works. By improving how you move, you will improve how you feel. It’s really that simple!</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 my-5 mob-my-0 text-center">
	<div class="row justify-content-center">
		<div class="col-lg-7">
			<h2 class="text-primary text-capitalize">Get the Simplistic Mobility Method</h2>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-5">
			<p class="text-large mob-text-small mb-0"><b>Simplify your mobility training For a one-off payment of @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif</b></p>
		</div>
		<div class="col-lg-12">
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mb-3 mx-auto d-inline-block">Buy Now</div></a>
			<p class="text-small">(Other currencies accepted! Payments are converted at the current rate)</p>
			<div class="col-12 text-center"> <img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" id="tom-footer-top" alt="PayPal Logo" width="86" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/paypal.svg" src="/img/logos/paypal.svg" data-was-processed="true"> <img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" id="tom-footer-top" alt="Mastercard Logo" width="36" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/mastercard.svg" src="/img/logos/mastercard.svg" data-was-processed="true"> <img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" id="tom-footer-top" alt="Visa Logo" width="47" class="img-fluid lazy mob-mb-0 mr-2 loaded" srcset="/img/logos/visa.svg" src="/img/logos/visa.svg" data-was-processed="true"> <img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" id="tom-footer-top" alt="American Express Logo" width="33" class="img-fluid lazy mob-mb-0 loaded" srcset="/img/logos/amex.svg" src="/img/logos/amex.svg" data-was-processed="true"></div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){
dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection