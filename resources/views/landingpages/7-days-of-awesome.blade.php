@php
$page = '7 Days of Awesome';
$pagename = 'LP';
$pagetitle = "7 Days of Awesome! - Seven days of FREE mobility exercises, sign up here - Tom Morrison";
$meta_description = "Seven days of FREE mobility exercises, sign up here.";
$og_image = 'https://tommorrison.uk/img/og.jpg?v=2023-04-04';
$was = "";
$price = number_format(69.00, 2, '.', '');
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('styles')
<style>
	.bx-pager{
	  width: auto;
	  display: table;
	  margin: 10px auto;
	}
	.bx-pager .bx-pager-item,
	.bx-pager .bx-pager-link{
		position: relative;
		margin: 0 2px;
		display: inline-block;
	}
	.bx-pager .bx-pager-item a,
	.bx-pager .bx-pager-link a{
		border: 1px solid #3E4146;
		background-color: transparent;
		border-radius: 100%;
		display: block;
		font-size: 0;
		margin: 0 3px;
		padding: 6px;
		&.active,
		&:hover,
		&:focus{
			background-color: #3E4146;
			border: 1px solid #3E4146;
		}
	}
</style>
@section('header')
<header class="container">
	<div class="row justify-content-center">
		<div class="col-lg-12 text-left text-center">
			<h1 class="mt-5 mob-mt-3 mb-3 lp-title-smaller mob-even-bigger text-primary text-center">7 Days of<br class="d-md-none" /> Awesome!</h1>
			<p class="mb-4 text-large"><b>Seven days of FREE mobility exercises, sign up here:</b></p>
		</div>
		<div class="col-lg-7">
			<seven-days-mailing-list></seven-days-mailing-list>
		</div>
	</header>
	@endsection
@section('content')
<div class="container pt-5">
	<div class="row justify-content-center">
		<div class="col-lg-8 my-4 py-2 text-center">
			<h2 class="mb-4 text-transform-none">See how awesome you can become in 7 days!</h2>
			<p class="mb-4 larger">Join Tom & Jenni for a full week of awesomeness where you will be given one simple mobility drill every day straight to your inbox! </p>
			<p>Or if you’re ready to start our paid programs, <a href="/blog/which-program-should-i-choose">click here to find the best program for you</a></p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row half_row py-4">
		<div class="col-12 half_col d-lg-none">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<img src="/img/lps/7doa-1.jpg?v=2023-04-04" alt="7 days of awesome image one" class="w-100"/>
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/7doa-2.jpg?v=2023-04-04" alt="7 days of awesome image two" class="w-100"/>
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/7doa-3.jpg?v=2023-04-04" alt="7 days of awesome image three" class="w-100"/>
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/7doa-4.jpg?v=2023-04-04" alt="7 days of awesome image four" class="w-100"/>
				</li>
			</div>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-1.jpg?v=2023-04-04" alt="7 days of awesome image one" class="w-100"/>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-2.jpg?v=2023-04-04" alt="7 days of awesome image two" class="w-100"/>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-3.jpg?v=2023-04-04" alt="7 days of awesome image three" class="w-100"/>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-4.jpg?v=2023-04-04" alt="7 days of awesome image four" class="w-100"/>
		</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center py-5">
		<div class="col-lg-8 text-center">
			<h2 class="text-primary mb-4 text-transform-none mob-text-bigger">Fill In The Gaps</h2>
			<p class="larger">You'll see how essential movements can easily be missed, causing your body to feel crabby, and allowing niggles, aches and pains to build up.</p>
			<p class="larger mb-0">Most importantly, you'll see how quickly you get results from these simple exercises!</p>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row half_row py-4 justify-content-center">
		<div class="col-12 half_col d-lg-none">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<img src="/img/lps/7doa-5.jpg?v=2023-04-04" alt="7 days of awesome image five" class="w-100"/>
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/7doa-6.jpg?v=2023-04-04" alt="7 days of awesome image six" class="w-100"/>
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/7doa-7.jpg?v=2023-04-04" alt="7 days of awesome image seven" class="w-100"/>
				</li>
			</div>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-5.jpg?v=2023-04-04" alt="7 days of awesome image five" class="w-100"/>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-6.jpg?v=2023-04-04" alt="7 days of awesome image six" class="w-100"/>
		</div>
		<div class="col-lg-3 half_col d-none d-lg-block">
			<img src="/img/lps/7doa-7.jpg?v=2023-04-04" alt="7 days of awesome image seven" class="w-100"/>
		</div>
	</div>
</div>
<div class="container pb-5 pt-2">
	<div class="row justify-content-center">
		<div class="col-lg-8 text-center">
			<h2 class="text-primary mb-4 text-transform-none mob-text-bigger">Full Body Mobility</h2>

			<p class="larger">Day 1 starts with your neck, and over the week we'll work our way down until your shoulders, back and hips feel incredible! All you need is a couple of minutes each day, no equipment, nothing fancy - and all the exercises are beginner friendly!</p>

		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-5 text-center pt-5 pb-3">
			<p class="larger"><b>Find out how easy mobility work can be and get yourself to 100% Awesome in one week!</b></p>
		</div>
	</div>
	<div class="row justify-content-center mb-5">
		<div class="col-lg-8">
			<h2 class="text-primary text-center mb-4 text-transform-none mob-text-bigger">Sign up now!</h2>
			<seven-days-mailing-list></seven-days-mailing-list>
		</div>
	</div>
</div>

@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js" defer></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
@endsection