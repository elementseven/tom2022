@php
$pagename = "Reviews";
$page = 'Reviews - Simplistic Mobility Method';
$pagetitle = "Reviews - The Simplistic Mobility Method - Tom Morrison";
$meta_description = "Check out some of our reviews and find out why The Simplistic Mobility Method is so popular with all kinds of people! No equipment needed. Just great drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'pagename' => $pagename, 'og_image' => $og_image, 'padfooter' => true, 'pagename' => $pagename])
@section('header')
<header class="container mb-5">
	<div class="row justify-content-center text-center">
		<div class="col-md-12">
			<h1 class="mt-5 mob-mt-3 mb-3 lp-title-small text-primary">Simplistic Mobility Method Reviews</h1>
			<h6 class="text-large"><b>Some feedback from our SMM community! </b></h6>
			<h6 class="text-large text-primary cursor-pointer" data-toggle="modal" data-target="#questionModal"><b>Ask a question</b></h6>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid py-5 my-5">
	<video-testimonials :amount="20"></video-testimonials>
	<p class="text-center text-large"><a href="https://uk.trustpilot.com/review/tommorrison.uk" target="_blank"><span class="position-relative" style="top:10px;">Find us on</span> <img src="/img/logos/trustpilot.svg" alt="Review Tom Morrison on TrustPilot" width="250" height="120" class="ml-2 h-auto"/></a></p>
</div>
<div class="container mb-5">
	<div class="row justify-content-center">
		@foreach($reviews as $review)
		<div class="col-lg-12 mb-5">
			<div class="review-box bg-light pt-4 pb-5 px-5 mob-px-3 mb-5">
				<div class="review-avatar">
					<picture> 
						<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal-webp')}}" type="image/webp"/> 
						<source srcset="{{$review->getFirstMediaUrl('reviews', 'normal')}}" type="{{$review->getFirstMedia('reviews')->mime_type}}"/>
						<img src="{{$review->getFirstMediaUrl('reviews', 'normal')}}" class="img-fluid d-block mx-auto mob-mb-3" alt="{{$review->name}}, Simplistic mobility method (SMM) review" />
					</picture>
				</div>
				<h3 class="review-name mb-5 mob-mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
					<div class="review-body"> 
						{!!$review->content!!}
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
<a href="/products/the-simplistic-mobility-method">
	<div class="get-smm-now" style="z-index: 1000;">
		<h3 class="mb-0">Get SMM Now!</h3>
	</div>
</a>
@endsection
@section('scripts')
<script>
	function addToBasket(id, name, category, price){
		dataLayer.push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': 'GBP',
				'add': {
					'products': [{
						'name': name,
						'id': id,
						'price': price,
						'brand': "Tom Morrison",
						'category': category,
						'quantity': 1
					}]
				}
			}
		});
		fbq('track', 'AddToCart', {
			value: {{$product->price}},
			currency: 'GBP'
		});
		console.log('done');
	}
</script>
@endsection
@section('modals')
<ask-a-question :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></ask-a-question>
@endsection