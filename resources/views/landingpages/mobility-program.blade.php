@php
$page = 'The Simplistic Mobility Method';
$pagename = 'LP';
$pagetitle = "Mobility Program - Tom Morrison";
$meta_description = "No equipment needed. Nothing fancy. No big words. Just great mobility drills with simple explanations.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'nomenu' => true, 'pagename' => $pagename])
@section('header')
<header class="container">
	<div class="row justify-content-center text-center">
		<div class="col-md-12">
			<h1 class="mt-5 mob-mt-3 mb-3 lp-title text-primary">Mobility Program</h1>
			<h2 class="mb-5 large-title text-nocase">The essential mobility program for crossfit, athletes, calisthenics, weightlifting, bodybuilding and more!</h2>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mb-5">
	<div class="row justify-content-center">
		<div class="col-lg-12 mt-5 mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-lg-6 text-center text-lg-left mob-mb-5">
					<p class="text-large">The Simplistic Mobility Method is a mobility program suitable for beginners, athletes, men, women, seniors, youngsters, yoga teachers and people who struggle to touch their knees - never mind their toes!</p>

					<p class="text-large">SMM is a mobility program designed to be easy to follow, and to help you improve your flexibility quickly. The program works on your upper back & thoracic flexibility, your core strength, your hip flexibility, your hamstring flexibility & more!</p>

					<p class="text-large">It really is the ultimate full body mobility routine - suitable for everyone, even if you are struggling with your mobility.</p>

					<p class="text-large">Check out some of our before and after photos! Or if you're heard enough, buy it now!</p>
				</div>
				<div class="col-lg-6 mob-mb-0">
					<div class="smm-slider ">
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-01.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 1">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-02.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 2">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-03.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 3">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-04.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 4">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-05.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 5">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-06.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 6">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-07.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 7">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-08.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 8">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-09.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 9">
						</li>
						<li style="list-style: none;">
							<img src="/img/lps/gallery/gallery1-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery image 10">
						</li>
					</div>
				</div>
				<div class="col-12 mt-4 text-center">
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
		</div>
			</div>
		</div>
		<div id="whatissmm" class="col-12 mt-5 pt-5">
			<div class="row justify-content-center">
				<div class="col-lg-12">
					<h2 class="mb-5 large-title text-center text-nocase">Printable mobility routine, downloadable PDFs, exercises with pictures and a full video library!</h2>
				</div>
				<div class="col-lg-10">
					<div class="row">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/graph.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons graph" />
						</div> 
						<div class="col-10">
							<p class="text-large">SMM has flexibility tests with <b>in-built analysis</b>: you do the tests and record your scores and you know what to improve on</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/time.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons time" />
						</div>
						<div class="col-10">
							<p class="text-large">SMM deliberately takes out all of the unnecessary mobility exercises. <b>You get fast flexibility results with less time invested</b></p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/scales.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons scales" />
						</div>
						<div class="col-10">
							<p class="text-large">SMM includes major core strength components that everyone needs to have to <b>avoid imbalances and injuries</b></p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/spine.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons spine" />
						</div>
						<div class="col-10">
							<p class="text-large">SMM improves your posture, with exercises that <b>everyone needs for great posture</b>: thoracic mobility, shoulder mobility, hip mobility, ankle mobility</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/check.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons check" />
						</div>
						<div class="col-10">
							<p class="text-large">SMM is downloadable and printable so the <b>assessments can be used forever</b>: any time you need to keep an eye on how your body feels you can run through it again and again!</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2 pr-5 mob-pr-3">
							<img src="/img/lps/icons/arm.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons arm" />
						</div>
						<div class="col-10">
							<p class="text-large">SMM is a mobility program & a full body strength program and stability program in one: these mobility exercises actually <b>make you stronger</b>, the drills are compound movements that teach your body how to work in unison</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mt-4 text-center">
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
		</div>
	</div>
	{{-- <div class="row justify-content-center my-5 mob-my-0">
		<div class="col-lg-8 my-5">
			<picture> 
				<source srcset="/img/lps/testimonial.webp" type="image/webp"/> 
				<source srcset="/img/lps/testimonial.jpg" type="image/jpeg"/>
				<img src="/img/lps/testimonial.jpg" class="img-fluid lazy" alt="Simplistic mobility method testimonial" />
			</picture>
		</div>
	</div> --}}
	
</div>
<div id="whatsincluded" class="container-fluid bg-primary text-white text-center py-5 mb-5">
	<div class="row my-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<h2 class="large-title text-nocase">No equipment. Nothing fancy. No big words. Just great drills with simple explanations.</h2>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-8">
			<picture> 
				<source srcset="/img/lps/smm-devices.webp?v2.0" type="image/webp"/> 
				<source srcset="/img/lps/smm-devices.jpg?v2.0" type="image/jpeg"/>
				<img src="/img/lps/smm-devices.jpg?v2.0" class="img-fluid lazy" alt="Simplistic mobility method devices" />
			</picture>
		</div>
		<div class="col-12 mt-5 pt-5 mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<h2 class="mb-5 large-title text-center">What's included in SMM?</h2>
					<div class="row">
						<div class="col-2">
							<img src="/img/lps/icons/play.svg" class="img-fluid lazy" alt="Simplistic mobility method icons play" />
						</div>
						<div class="col-10">
							<p class="text-large">A <b>comprehensive set of videos</b> demonstrating the Tests and Exercises in real time with Tom and Jenni for you to easily learn and avoid common mistakes – no equipment required!</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2">
							<img src="/img/lps/icons/doc.svg" class="img-fluid lazy" alt="Simplistic mobility method icons document" />
						</div>
						<div class="col-10">
							<p class="text-large">A PDF containing the <b>Simplistic Mobility Method program</b> which will help rebalance and retrain your body PLUS the tests that you will be using to measure your progress</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2">
							<img src="/img/lps/icons/info.svg" class="img-fluid lazy" alt="Simplistic mobility method icons info" />
						</div>
						<div class="col-10">
							<p class="text-large">The PDF also contains instructions of how to use it, extra information and the <b>reasoning behind the exercises</b></p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2">
							<img src="/img/lps/icons/lightbulb.svg" class="img-fluid lazy" alt="Simplistic mobility method icons lightbulb" />
						</div>
						<div class="col-10">
							<p class="text-large">A <b>Cheat Sheet</b> which takes you through all the exercises in extra detail, showing you exactly which muscles are targeting with extra tips and things to look out for!</p>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-2">
							<img src="/img/lps/icons/facebook.svg" class="img-fluid lazy" alt="Simplistic mobility method facebook arm" />
						</div>
						<div class="col-10">
							<p class="text-large"><b>Daily access, feedback and support</b> from the SMM Facebook group and Tom Morrison</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mb-5 text-center">
			<h2 class="mt-5 large-title text-nocase text-center text-primary">… And that's not all!!</h2>
			<h2 class="large-title text-nocase text-center mb-5">You also get a FREE BONUS video for extra balance, co-ordination and strength gains!!</h2>
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
		</div>
	</div>
</div>
<div id="pricing" class="container-fluid bg-primary text-white text-center py-5 my-5">
	<div class="row my-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<h2 class="large-title text-nocase">For the price of <u>ONE</u> physio session, you get a <u>LIFETIME</u> of better movement</h2>
				</div>
			</div>
		</div>
	</div>
</div>	
<div id="pricing" class="container my-5 mob-my-0">
	<div class="row justify-content-center mb-5 justify-content-center">
		<div class="col-lg-8 my-5 mob-mb-0">
			<picture> 
				<source srcset="/img/lps/testimonial-5.webp" type="image/webp"/> 
				<source srcset="/img/lps/testimonial-5.jpg" type="image/jpeg"/>
				<img src="/img/lps/testimonial-5.jpg" class="img-fluid lazy" alt="Simplistic mobility method testimonial" />
			</picture>
		</div>
		<div class="col-md-9 mob-mt-5">
			<p class="text-large mb-4">I have lost track of the amount of money I've spent on physiotherapy over the years, and though it all helped at the time, it never fixed the crucial issue: <b>I did not know my own body</b>.</p>

			<p class="text-large mb-4">I didn't know it's imbalances, it's compensations, where I lacked essential strength - <b>I thought I was fine</b>.</p>

			<p class="text-large mb-5">I thought the niggles were fine. I thought the soreness was normal. I thought that the bouncing pain from shoulder to hip, to groin, back to shoulder was the price of lifting heavy and training hard. <b>It was a normal part of the gym life, right</b>?</p>

			<h2 class="large-title text-center mb-5">It is not ok to be sore or in pain all of the time!</h2>
			<p class="text-large mb-4">That is just a road to injury. It might not be today, or tomorrow, or even a year from now.. but you are setting yourself up for failure.</p>

			<p class="text-large mb-4">Instead of paying over and over again for injuries that were completely avoidable, let SMM teach you about your body and you will soon see that you don't need to be in pain, or resign yourself to having "bad shoulders" or "a bad back".</p>

			<p class="text-large mb-4"><b>The Simplistic Mobility Method is only @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif. Less than the cost of the average physiotherapy session, and the results will last a lifetime.</b></p>


		</div>
		<div class="col-12 mb-5 text-center">
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
		</div>
	</div>
</div>
<div class="container-fluid bg-light">
	<div class="row">
		<div id="faqs" class="container mb-5">
			<div class="row justify-content-center my-5 mob-mb-0 justify-content-center">
				<div class="col-md-8 my-5 mob-my-0">
					<h2 class="mb-5 large-title text-center">Got some questions?</h2>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_1" aria-expanded="false" aria-controls="answer_1"><b>Is SMM really worth it?</b></a></p>
		            <div id="answer_1" class="collapse">
		                <p>Straight to it, eh?? Ok, as an example, when I hurt my knee I spent 6 weeks of physio on it - that cost me about £300. When I hurt my shoulder that was 3 sessions; £150.</p>
						<p>When I badly injured my back I pretty much went every week for 6 months straight just to get some relief... I don’t even want to calculate that.</p>
						<p>The Simplistic Mobility Method is @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif once. For a lifetime of benefits.</p>
						<p>You will learn about your body, how to spot injuries before they happen, it is a tried and tested method with real athletes. After people have bought SMM they have found that they no longer need to go to Physio as often, or even at all.</p>
						<p>Why waste time when you have to rest or rehab after an injury, when you could just prevent it in the first place?</p>
						<p>Plus… as an added bonus you will receive lifetime access to your online account where you access SMM, and automatic updates and upgrades of any improvements we make to the program in future. It really is a lifetime of awesomeness.</p>
						<p>PLUS! As an extra added bonus, you’ll become a part of a global community of SMMers via our facebook group, where you can ask questions directly to people who’ve been using the method for years, and directly to Tom & Jenni who are very active in the SMM community.</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_2" aria-expanded="false" aria-controls="answer_2"><b>Do I really need SMM?</b></a></p>
		            <div id="answer_2" class="collapse">
		                <p>After working with so many people over the years, there are always things we miss about training, mobility, and recovery – no matter how much research and practice you do. You will pick up imbalances and compensations over years of small mistakes, misinterpretations and even daily habits.</p>
						<p>The Simplistic Mobility Method allows you to learn what your body can and can’t do right now - the best injury is the one that never happened. And you can use the tests & methodology to keep maintaining and checking your body throughout your entire lifetime.</p>
						<p>SMM is designed to make your joints sit and work correctly and teaches your body to move as one unit with great muscle activation.</p>
						<p>It doesn’t matter what you like to do for physical activity, everyone has the same joints and muscles and they need to have a certain baseline mobility & stability to function well; SMM is that baseline. </p>
						<p>Your body won’t be sore all the time if it moves well.</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_3" aria-expanded="false" aria-controls="answer_3"><b>What happens after I buy SMM? Where do I access the program?</b></a></p>
		            <div id="answer_3" class="collapse">
		                <p>After you buy, you’ll receive an email with your login details and a link to sign in to your new dashboard!</p>
						<p>If you don’t receive an email, make sure to check your spam/junk folder. If it’s not there either, simply go to https://tommorrison.uk/login and click “Forgot Your Password?” to generate a new sign in email for you. Some email hosts don’t like automatic emails and block them to protect you from spam!</p>
						<p>Once you’re signed in, you will see the Simplistic Mobility Method on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily.</p>
						<p>Everything is stored online for you, and any updates we make to the program will automatically appear for you.</p>
						<p>If you ever forget your password, don’t worry! The “Forgot Your Password” feature is always there! You won’t lose your account.</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_4" aria-expanded="false" aria-controls="answer_4"><b>How long will it take for me to see results?</b></a></p>
		            <div id="answer_4" class="collapse">
		                <p>After your first session. Yeah, really!</p>
						<p>In your first session you run through a series of tests before and after. Some people notice visual improvements right away, others just feel better, taller or that they are moving easier. Surprisingly, many are shocked at how much they struggled with.</p>
						<p>After that, as you’d expect, the more you use the program the faster you see results. Most people tell us it takes about 8 sessions to make a real difference to their body and their training. So if you can manage 4 times per week - that’s only 2 weeks to awesome.</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_5" aria-expanded="false" aria-controls="answer_5"><b>How long does it take to do SMM? Will I actually stick with it?</b></a></p>
		            <div id="answer_5" class="collapse">
		                <p>An SMM session will take about 20 minutes, so it is pretty easy to slot into even a busy timetable. We’d recommend between 2-5 sessions per week, depending on how much time you have available.</p>
						<p>The better you get at SMM the less frequently you’ll need to go through the whole routine. You can pick the movements that benefit you the most and throw them into warm ups & cool downs. We also go through a way you can condense your mobility practice into a 5 minute-a-day routine!</p>
						<p>Your very first session will take longer, as you’ll go through a full body assessment and you’ll want to watch the videos to check the technique, tips and progressions. You’ve also got optional checklists and note sheets that you can use to mark your progress.</p>
						<p>After that, 20 minute sessions! 4 times a week if possible but if you can only manage 2 times that’s still more than none.</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_6" aria-expanded="false" aria-controls="answer_6"><b>I’m not very flexible, is SMM suitable for total beginners?</b></a></p>
		            <div id="answer_6" class="collapse">
		                <p>Don’t worry, trying to get flexible before starting a mobility program is like trying to learn to drive before your first lesson.</p>
						<p>The Simplistic Mobility Method is what you need to increase your flexibility in a safe way! Not just stretching for no reason, but intentional, directed exercises to build both mobility and stability.</p>
						<p>The exercises are laid out in such a way that show you how to regress them no matter where you are at! And on the flip side of that! If you are too flexible then the movements are perfect for you to develop control over to add more stability and confidence to your body!</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_7" aria-expanded="false" aria-controls="answer_7"><b>I do/play a sport will SMM help me?</b></a></p>
		            <div id="answer_7" class="collapse">
		                <p>Yup, the Simplistic Mobility Method is designed to help the human body – regardless of what sport that body does.</p>
						<p>In our SMM community group we have footballers, crossfitters, kettlebell athletes, powerlifters, weightlifters, rugby players, runners, normal gym goers, bodybuilders, coaches, physiotherapists, bodyweight enthusiasts, pole dancers, even people that aren’t interested in fitness and just want to feel good in their day to day life!</p>
						<p>We all have a body, sports and activities is all the fun stuff after your moves well. SMM is like the background operating systems of your phone to keep it running smoothly. If there’s a ‘bug’ in a specific area, your body will not perform the way it should!</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_8" aria-expanded="false" aria-controls="answer_8"><b>Can I do SMM alongside my own training?</b></a></p>
		            <div id="answer_8" class="collapse">
		                <p>Yep! We would argue that SMM is an essential part of your training. It runs through everything you need for good performance.</p>
						<p>No more wasting time trying to figure out what’s causing that niggle, rolling out tightness, or getting stuck as a plateau, SMM gives you the tools to actively support and progress your training.<br>It is an indispensable extra to your training!</p>
						<p>However, if you were really in a bad way for a long time and your test show that you have a lot to work on, then I would focus solely on SMM for a few weeks and then return to training when you have the movements at a good level!</p>
		            </div>
		            <p class="text-large"><a class="dark" data-toggle="collapse" href="#answer_9" aria-expanded="false" aria-controls="answer_9"><b>Do I need equipment to do SMM?</b></a></p>
		            <div id="answer_9" class="collapse">
		                <p>Nope, not at all! Every exercise can be done at home, at the gym, while travelling... any where!</p>
						<p>Every movement we use is bodyweight. The aim is to teach you how to move using your own body as the main tool, rather than relying on external forces!</p>
						<p class="mb-0">So,</p>
						<ul>
							<li><p class="mb-0">It educates you!</p></li>
							<li><p class="mb-0">It's tried & tested!</p></li>
							<li><p class="mb-0">Makes you feel awesome!</p></li>
							<li><p class="mb-0">Helps treat & prevent injuries!</p></li>
							<li><p class="mb-0">No equipment needed!</p></li>
							<li><p class="mb-0">It’s a one off payment!</p></li>
							<li><p class="mb-0">It has lifetime benefits!</p></li>
							<li><p class="mb-0">There's an active, global community group!</p></li>
							<li><p class="mb-0">You get direct contact with Tom & Jenni</p></li>
						</ul>
						<p>What are you waiting for!?</p>
		            </div>
				</div>
				<div class="col-12 text-center mob-mt-3">
					<a href="/basket/add/1"><div class="btn btn-primary mx-auto d-inline-block">Buy Now</div></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-7 pt-5 my-5">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-01.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 1">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-02.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 2">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-03.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 3">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-04.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 4">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-05.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 5">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-06.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 6">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-07.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 7">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-08.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 8">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-09.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 9">
				</li>
				<li style="list-style: none;">
					<img src="/img/lps/gallery/gallery2-10.jpg" class="w-100" alt="Simplistic Mobility Method Gallery 2 image 10">
				</li>
			</div>
		</div>
		<div id="testimonials" class="col-12  mob-mt-0">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<h2 class="text-primary large-title mb-5">What do people think of SMM?</h2>
					<div class="row half_row justify-content-center mb-5">
						<div class="col-md-3 col-6 half_col">
							<a href="https://www.google.com/search?ei=Xj4jXaL_JOuGhbIPj4u5yA8&q=tom+morrison&oq=tom+morrison&gs_l=psy-ab.3..0i67j0l9.897.897..1128...0.0..0.202.202.2-1......0....1..gws-wiz.kbvFsZWQmp0#lrd=0x4861054a7fdb59f9:0x8139ec4898a45a3,1,,," target="_blank">
								<picture> 
									<source srcset="/img/lps/google.webp" type="image/webp"/> 
									<source srcset="/img/lps/google.jpg" type="image/jpeg"/>
									<img src="/img/lps/google.jpg" class="img-fluid lazy" alt="Simplistic mobility method google reviews 5 star" />
								</picture>
							</a>
						</div>
						<div class="col-md-3 col-6 half_col">
							<a href="https://www.facebook.com/Movementandmotion/" target="_blank">
								<picture> 
									<source srcset="/img/lps/facebook.webp" type="image/webp"/> 
									<source srcset="/img/lps/facebook.jpg" type="image/jpeg"/>
									<img src="/img/lps/facebook.jpg" class="img-fluid lazy" alt="Simplistic mobility method facebook reviews 5 star" />
								</picture>
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-10 text-center text-md-left">

					<div class="container bg-light p-4 mb-3">
						<div class="row justify-content-center">
							<div class="col-3 col-md-2">
								<picture> 
									<source srcset="/img/lps/simon-loalt.webp" type="image/webp"/> 
									<source srcset="/img/lps/simon-loalt.png" type="image/png"/>
									<img src="/img/lps/simon-loalt.png" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method simon loalt testimonial" />
								</picture>
							</div>
							<div class="col-9 col-md-10">
								
								<p class="text-large text-left testimonial-text"><b>"WHAT IS THIS SORCERY!?!?!? How can one round of those exercises make me feel so much better?!?!?!?! Truly fascinating, already hooked. Thank you so much!"</b></p>
								<p class="mb-0 text-right text-md-left testimonial-name"><img src="/img/lps/icons/stars.svg" width="130" class="d-inline mr-5 mob-mr-0 mob-mb-1"><i class="float-right mob-mb-3">Simon Loalt, Regular Gym Go-er</i></p>
							</div>
						</div>
					</div>
					<div class="container bg-light p-4 mb-3">
						<div class="row justify-content-center">
							<div class="col-3 col-md-2">
								<picture> 
									<source srcset="/img/lps/emma-thompson.webp" type="image/webp"/> 
									<source srcset="/img/lps/emma-thompson.png" type="image/png"/>
									<img src="/img/lps/emma-thompson.png" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method emma thompson testimonial" />
								</picture>
							</div>
							<div class="col-9 col-md-10">
								
								<p class="text-large text-left testimonial-text"><b>"Yesterday was my 3rd day completing SMM, once I was finished I was able to stand up tall, with no back pain, for the first time in 18 months"</b></p>
								<p class="mb-0 text-right text-md-left testimonial-name"><img src="/img/lps/icons/stars.svg" width="130" class="d-inline mr-5 mob-mr-0 mob-mb-1"><i class="float-right mob-mb-3">Emma Thompson, Strength & Conditioning Athlete</i></p>
							</div>
						</div>
					</div>
					<div class="container bg-light p-4 mb-3">
						<div class="row justify-content-center">
							<div class="col-3 col-md-2">
								<picture> 
									<source srcset="/img/lps/matt-rodock.webp" type="image/webp"/> 
									<source srcset="/img/lps/matt-rodock.png" type="image/png"/>
									<img src="/img/lps/matt-rodock.png" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method matt rodock testimonial" />
								</picture>
							</div>
							<div class="col-9 col-md-10">
								
								<p class="text-large text-left testimonial-text"><b>"When I contacted Tom after Nationals last year my squat PR was 337.5kg, now it’s 345, and I think it’s looking a lot more symmetrical - my body has never felt this good in the lead up and in the competition"</b></p>
								<p class="mb-0 text-right text-md-left testimonial-name"><img src="/img/lps/icons/stars.svg" width="130" class="d-inline mr-5 mob-mr-0 mob-mb-1"><i class="float-right mob-mb-3">Matt Rodock, Competitive Powerlifter</i></p>
							</div>
						</div>
					</div>
					<div class="container bg-light p-4 mb-3">
						<div class="row justify-content-center">
							<div class="col-3 col-md-2">
								<picture> 
									<source srcset="/img/lps/anders-anderson.webp" type="image/webp"/> 
									<source srcset="/img/lps/anders-anderson.png" type="image/png"/>
									<img src="/img/lps/anders-anderson.png" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method anders anderson testimonial" />
								</picture>
							</div>
							<div class="col-9 col-md-10">
								
								<p class="text-large text-left testimonial-text"><b>"I honestly think SMM qualifies as at least a flamethrower in the knife-fight of physiotherapy. I have been to osteopaths, chiropractic doctors, physios, manual therapists all of them telling me to stop training … and what they couldn't even alleviate was FIXED FOR GOOD with SMM"</b></p>
								<p class="mb-0 text-right text-md-left testimonial-name"><img src="/img/lps/icons/stars.svg" width="130" class="d-inline mr-5 mob-mr-0 mob-mb-1"><i class="float-right mob-mb-3">Anders Anderson. Regular Gym Go-er</i></p>
							</div>
						</div>
					</div>
					<div class="container bg-light p-4 mb-3">
						<div class="row justify-content-center">
							<div class="col-3 col-md-2">
								<picture> 
									<source srcset="/img/lps/su-ward.webp" type="image/webp"/> 
									<source srcset="/img/lps/su-ward.png" type="image/png"/>
									<img src="/img/lps/su-ward.png" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method su ward testimonial" />
								</picture>
							</div>
							<div class="col-9 col-md-10">
								
								<p class="text-large text-left testimonial-text"><b>"When I discovered Tom I was a very injured, depressed runner on the verge of jacking it in and taking up knitting. 12 months on, using SMM pretty much daily I represented my County in the Intercountry Fell Running Championships! Thank you so much Tom."</b></p>
								<p class="mb-0 text-right text-md-left testimonial-name"><img src="/img/lps/icons/stars.svg" width="130" class="d-inline mr-5 mob-mr-0 mob-mb-1"><i class="float-right mob-mb-3">Su Ward, Competitive Runner</i></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mt-5">
			<picture> 
				<source srcset="/img/lps/compilation.webp" type="image/webp"/> 
				<source srcset="/img/lps/compilation.jpg" type="image/jpeg"/>
				<img src="/img/lps/compilation.jpg" class="img-fluid d-block mx-auto mob-mb-3 lazy" alt="Simplistic mobility method testimonials compilation" />
			</picture>
		</div>
		<div class="col-12 mt-5 text-center">
			<h2 class="large-title mb-4">Want to join them?</h2>
			<a href="/basket/add/1"><div class="btn btn-primary mx-auto d-inline-block">Buy Now</div></a>
		</div>
		<div class="col-lg-9 mt-5">
			<div class="border-2 px-4 overflow-hidden">
				<div class="container">
					<div class="row">
						<div class="col-lg-7 pb-4 text-left">
							<h3 class="pt-5">About Tom</h3>
							<p>Tom is unusual compared to most trainers; he didn't start any kind of physical training until 24.</p>
							<p>But, with no foundation whatsoever, after a few years his newfound enthusiasm caught up with him.</p>
							<p>Everything he'd had missed over the years caused him to badly injure his shoulder, knee and the worst he endured was his back: an L4/L5/S1 disc protrusion causing nerve impingement.</p>
							<p>With his career seemingly over before it had begun, he set out on a path of learning. He realised how important it is to have a basic level of mobility & stability throughout the entire body, and how many people didn't. He realised the approach needed to be different to what is commonly taught.</p>
							<p>Tom wants to help people realised that they are in control of their body, for people to know exactly what they need to work on, and to show the principles of good movement are actually pretty simple.</p>
						</div>
						<div class="col-lg-5 pr-0 position-relative">
							<picture> 
								<source srcset="/img/lps/tom-2.webp" type="image/webp"/> 
								<source srcset="/img/lps/tom-2.jpg" type="image/jpeg"/>
								<img src="/img/lps/tom-2.jpg" class="lazy img-abs-btm-left" alt="Tom Morrison of the Simplistic mobility method" />
							</picture>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 my-5 text-center">
			<h2 class="text-center mb-3">Everything Tom learnt has been stripped down & condensed into the fundamental movements that actually work.</h2>
			<h2 class="text-center text-primary">Don't waste your time, pick up the Method today!</h2>
			<a href="/basket/add/1"><div class="btn btn-primary mt-4 mx-auto d-inline-block">Buy Now</div></a>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-12 position-relative">
			<video id="bottom-video" class="w-100 position-relative d-block">
				<source src="/vid/smm-bottom.mp4" type="video/mp4">
				Your browser does not support the video tag.
			</video>
			<div class="play-btn">
				<img src="/img/lps/icons/play.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons play button" />
			</div>
		</div>
	</div>
	<a href="#" id="back-to-top" title="Back to top"><i class="fa fa-angle-up"></i></a>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
<script>
function addToBasket(id, name, category, price){

dataLayer.push({
'event': 'addToCart',
'ecommerce': {
'currencyCode': 'GBP',
'add': {                                // 'add' actionFieldObject measures.
'products': [{                        //  adding a product to a shopping cart.
'name': name,
'id': id,
'price': price,
'brand': "Tom Morrison",
'category': category,
'quantity': 1
}]
}
}
});
fbq('track', 'AddToCart', {
value: {{$product->price}},
currency: 'GBP'
});
console.log('done');
}
$(document).ready(function(){
	$('.play-btn').click(function(){
		$(this).fadeOut();
		$('#bottom-video')[0].play();
	});
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
});

</script>
@endsection