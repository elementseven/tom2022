@php
$pagename = 'LP';
$page = 'SMM 2022';
$pagetitle = "Simplistic Mobility Method - Tom Morrison";
$meta_description = "The essential, non-negotiable movements to assess yourself, move well and avoid injury - keeping your joints healthy & core strength in check.";
$og_image = 'https://tommorrison.uk/img/og.jpg';
@endphp
@extends('layouts.app', ['page' => $page , 'pagetitle' => $pagetitle , 'meta_description' => $meta_description , 'og_image' => $og_image, 'pagename' => $pagename, 'padfooter' => true])
@section('styles')
<link rel="preload" as="image" href="https://tommorrison.uk/img/lps/smm2022/banner-bg1.webp">
<link rel="preload" as="image" href="https://tommorrison.uk/img/lps/smm2022/banner-bg1.jpg">
@endsection
@section('header')
<header class="container">
	<div class="row justify-content-center text-center">
		<div class="col-md-12">
			<h1 class="mt-5 mob-mt-3 mb-2 lp-title text-dark position-relative">The <u style="text-decoration-color: #d82737;">Simplistic</u> Mobility Method<span class="tm">&reg;</span></h1>
		</div>
		<div class="col-lg-10">
			<p class="mb-5 text-large">The essential, non-negotiable movements to assess yourself, move well and avoid injury - keeping your joints healthy & core strength in check</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container-fluid bg bg-fixed banner-bg1 text-center">
	<div class="row">
		<div class="col-12 py-5">
			<h2 class="text-white mb-4 text-transform-none">No Equipment. No Big Words.<br/> Just essential exercises with simple explanations</h2>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want to master my mobility >><br/>(Get Instant Access)</button></a>
		</div>
	</div>
</div>
<div class="container">
	<div class="row justify-content-center mt-5">
		<div class="col-12 d-none d-lg-block">
			<img src="/img/lps/smm2022/logos.svg?v2.0"class="w-100" alt="Tom featured logos"/>
		</div>
		<div class="col-11 d-lg-none">
			<img src="/img/lps/smm2022/logos-mob.svg?v2.0"class="w-100" alt="Tom featured logos"/>
		</div>
		<div class="col-lg-10 my-5">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/710501413?h=cfc2e236a9&color=d82737&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
		<div class="col-12 mt-3">
			<div class="floating-bg-box scroll_fade" data-fade="show-red">
				<div class="content p-4">
					<p><b>No one cares about their mobility until something hurts.</b></p>
					<p>Then you get stuck trying to fix yourself by focusing only on the area that's hurting, or expect a couple of physio/chiro appointments to completely correct how you've been moving your entire life.</p>
					<p>Imagine accidentally wasting your time & money because you didn’t know that your lack of hip flexibility was causing your recurring back pain, or that your shoulder & knee pain was because of a lack of stability, not stretching.…</p>
					<p>That’s what happened to me.</p>
					<p class="mb-0">After spending years doing the WRONG things, I realised that good, effective full body mobility doesn’t have to be complicated.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div id="mobility-stats-counters" class="container pt-5">
			<div class="row text-center justify-content-center">
				<div class="col-lg-9">
					<div class="row text-center">
						<div class="col-lg-3 col-6 mob-mb-3">
							<div class="circle-counter counter col_fourth">
								<h2><span class="timer count-title count-number" data-to="18000" data-speed="1500"></span>+</h2>
								<p class="count-text ">Happy Users</p>
							</div>
						</div>
						<div class="col-lg-3 col-6 mob-mb-3">
							<div class="circle-counter counter col_fourth">
								<h2 class="timer count-title count-number" data-to="98" data-speed="1500"></h2>
								<p class="count-text ">Countries</p>
							</div>
						</div>
						<div class="col-lg-3 col-6 mob-mb-3">
							<div class="circle-counter counter col_fourth">
								<h2><span class="timer count-title count-number" data-to="5" data-speed="1500"></span> <img src="/img/icons/star-white.svg" alt="star" width="30" height="30" class="counters-star" style="margin-top: -10px;" /></h2>
								<p class="count-text ">Trusted<br/>Reviews</p>
							</div>
						</div>
						<div class="col-lg-3 col-6 mob-mb-3">
							<div class="circle-counter counter col_fourth end">
								<h2 class="timer count-title count-number" data-to="30" data-speed="1500"></h2>
								<p class="count-text ">Minute<br/>Routine</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg banner-bg text-center mt-5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 my-5 mob-mb-0 mob-px-0">
					<picture>
						<source srcset="/img/lps/smm2022/4-squares.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/4-squares.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/4-squares.png" type="image/png" class="w-100 lazy"/>
					</picture>
				</div>
				<div class="col-lg-6 my-5 mob-mt-0 text-left">
					<h2 class="text-white text-transform-none pt-3 mb-4">Be <u style="text-decoration-color: #d82737;">confident</u> in how you move</h2>
					<p class="text-white larger">Feel better than you ever have before!</p>
					<p class="text-white larger">Your age, training background or injury history does not matter - you can ALWAYS improve how you move!</p>
					<p class="text-white larger">We have people in their twenties discovering they don't need to stop the training they love.</p>
					<p class="text-white larger mb-4">And people who only started SMM in their 70’s accomplishing things they never thought they'd be able to do again!</p>
					<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want to fix my mobility now! >></button></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 mob-pt-4 mob-pb-0">
	<div class="row">
		<div class="col-lg-7">
			<h2 class="text-transform-none pt-3 mb-4">You <u style="text-decoration-color: #d82737;">CAN</u> change how you feel!</h2>
			<p class="">In my late twenties I believed I was old and had too many injuries. At this point I'd already had knee surgery, shoulder issues, and other injuries including an L4/L5 disc protrusion and an L5/S1 disc extrusion with nerve impingement…</p>
			<p class="">I'd let myself get to a stage where I couldn’t walk, stand or sit without pain.</p>
			<p class="">I was miserable and ready to give up on my training, fitness... even my career as a coach.</p>
			<p class="">But now, because of a stubbornness and a refusal to give up on myself I feel amazing!</p>
			<p class="">I spent years developing the Simplistic Mobility Method and I can now do whatever I want and train with complete confidence in my body’s ability.</p>
		</div>
		<div class="col-lg-5 mt-5 mob-mt-3">
			<picture>
				<source srcset="/img/lps/smm2022/tom-before-after.jpg" type="image/jpeg"/>
				<source srcset="/img/lps/smm2022/tom-before-after.webp" type="image/webp"/>
				<img src="/img/lps/smm2022/tom-before-after.jpg" type="image/jpeg" class="w-100 lazy"/>
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid bg-dark text-white mt-5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 my-5 text-left">
					<h2 class="text-transform-none pt-3 mb-4">Do you <u style="text-decoration-color: #d82737;">believe</u> any of these things about yourself?</h2>
					<ul class="list-pointer list-style-none pl-0">
						<li class="mb-1">You're just not flexible</li>
						<li class="mb-1">You've had this niggle for ages - it's not going to change</li>
						<li class="mb-1">You feel too old for this stuff</li>
						<li class="mb-1">These injuries/aches/pains are just part of who you are</li>
						<li class="mb-1">You don't have time to work on your mobility</li>
						<li class="mb-1">You're just unlucky, always picking up silly injuries</li>
						<li class="mb-1">You just want a whole new body</li>
						<li>You're already doing all the right things but nothing's worked/is working</li>
					</ul>
					<h2 class="text-transform-none pt-3">… That’s where The <u style="text-decoration-color: #d82737;">Simplistic</u> Mobility Method® comes in</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5">
	<div class="row py-5 mob-py-0">
		<div class="col-lg-5">
			<h2 class="text-transform-none pt-3 mb-4">I developed SMM to:</h2>
			<p class="larger">1. Assess your entire body's flexibility & stability</p>
			<p class="larger">2. Check for any imbalances </p>
			<p class="larger">3. Teach awareness of your own twists & turns</p>
			<p class="larger mb-4">4. How to fix them in 30 mins, 3-4 times per week</p>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want the method now! >></button></a>
		</div>
		<div class="col-lg-7 mob-mt-5 mob-mb-4">
			<picture> 
				<source srcset="/img/lps/smm-devices.webp" type="image/webp"/> 
				<source srcset="/img/lps/smm-devices.jpg" type="image/jpeg"/>
				<img src="/img/lps/smm-devices.jpg" class="img-fluid lazy" alt="Simplistic mobility method devices" />
			</picture>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 mb-4">
			<div class="card p-4 border-0 bg-light shadow">
				<div class="row">
					<div class="col-6">
						<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
					</div>
					<div class="col-6 text-right">
						<p class="title" style="font-weight:300;"><b>Nita Newman</b></p>
					</div>
					<div class="col-12">
						<p class="mb-0">This is my exercise plan for life. I've NEVER stuck with any other program. I got so used to pain & stiffness that I forgot what it was like to feel well. LOVE IT  ❤️</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card p-4 border-0 bg-light shadow">
				<div class="row">
					<div class="col-6">
						<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
					</div>
					<div class="col-6 text-right">
						<p class="title" style="font-weight:300;"><b>Ben Ladlow</b></p>
					</div>
					<div class="col-12">
						<p class="mb-0">SMM has been an absolute game changer for me. For the first time I've been able to squat heavy without any niggling pain, and recovery between sets has been easier</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card p-4 border-0 bg-light shadow">
				<div class="row">
					<div class="col-6">
						<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
					</div>
					<div class="col-6 text-right">
						<p class="title" style="font-weight:300;"><b>Tracy Pearson</b></p>
					</div>
					<div class="col-12">
						<p class="mb-0">Thanks Tom & Jenni you have given me the ability to 'do all the things' I love without pain! I can dance again without pain & my goal is to be able to run again!</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card p-4 border-0 bg-light shadow">
				<div class="row">
					<div class="col-6">
						<img src="/img/lps/smm2022/stars.svg" alt="smm 5 star review" width="140" height="25"/>
					</div>
					<div class="col-6 text-right">
						<p class="title" style="font-weight:300;"><b>Mark Lidster</b></p>
					</div>
					<div class="col-12">
						<p class="mb-0">IT WORKS 😀 Pain has subsided considerably and I can touch my toes instead of halfway down my shins. I'm in, a convert to the cause 😊👍</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mt-5">
			<p>It doesn’t matter if you're inflexible, or even too flexible, SMM works because it is built on how the body is SUPPOSE to move - regardless of who you are!</p>
			<p>The program is built in a specific order, each exercise building on the previous to ensure you lengthen the right things before adding strength or stability - meaning you won't be accidentally strengthening dysfunctions.</p>
			<p>Just give us 30 minutes, 3-4 times per week and you will be shocked at how good you feel (and it's so much easier than other people would lead you to believe)</p>
		</div>
	</div>
</div>
<div class="container-fluid bg banner-bg text-center">
	<div class="row">
		<div class="container">
			<div class="row">
				
				<div class="col-lg-7 my-5 mob-mb-3 text-left">
					<h2 class="text-white text-transform-none pt-3 mb-4">One time Purchase, <u style="text-decoration-color: #d82737;">Lifetime</u> Access</h2>
					<p class="text-white larger">We don’t drip feed you to keep you interested or draw you in to an ongoing subscription, we give you the most effective stuff right now in the most efficient way possible!</p>
					<p class="text-white larger">That way you can ACTUALLY DO IT!</p>
					<p class="text-white larger">And get the benefits immediately!</p>
					<p class="text-white larger mb-4"><b>It’s fully online & you can get access TODAY with a one time purchase (+ lifetime access & updates)!</b></p>
					<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want to start today! >></button></a>
				</div>
				<div class="col-lg-5 my-5 mob-mt-0 mob-mb-3 px-0">
					<img src="/img/lps/smm2022/starz-desktop.svg" type="image/svg" class="w-100 lazy d-none d-lg-block"/>
					<img src="/img/lps/smm2022/starz-mob.svg" type="image/svg" class="w-100 lazy d-lg-none"/>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5">
	<div class="row">
		<div class="col-12 mb-5 mob-mb-4">
			<div class="card px-5 py-4 border-0 mob-px-3" style="background-color: #FFE1E3;">
				<h2 class="text-transform-none pt-3 mb-4">The Simplistic Mobility Method is NOT for you if…</h2>
				<p>❌ &nbsp;You want a quick fix without putting any effort or care into what you're doing</p>
				<p>❌ &nbsp;You want 100’s of random movements that look nice but have no logic, that you will rarely do</p>
				<p>❌ &nbsp;You want super fancy & over complicated demos and explanations that are hard to understand and follow</p>
			</div>
		</div>
		<div class="col-12 mb-5 mob-mb-3">
			<div class="card px-5 py-4 border-0 mob-px-3" style="background-color: #D6F6E7;">
				<h2 class="text-transform-none pt-3 mb-4">The Simplistic Mobility Method is for you if…</h2>
				<p>✅ You're fed up trying everything & want structure with easy ways to measure your progress</p>
				<p>✅ You want control over how your body feels & are willing to practice the movements, focusing on quality</p>
				<p>✅ You want to stop recurring injuries, putting time into the right things to stop paint coming back</p>
				<p>✅ You want a system that you know looks out for you, and that you can always come back to even after a break</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 my-4 mob-mb-5">
			<p><b>With SMM everything is simple.</b></p>
			<p>All you have to do is your tests & practice the exercises and you’ll know you are doing all the right things to see progress!</p>
			<p>Never think that your body is letting you down again!</p>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want SMM! >></button></a>
		</div>
		<div class="col-lg-6">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/simple1-caitriona-shoulders.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/simple1-caitriona-shoulders.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/simple1-caitriona-shoulders.jpg" alt="simple1-caitriona-shoulders" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/simple2-david-life.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/simple2-david-life.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/simple2-david-life.jpg" alt="simple2-david-life" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/simple3-marie-knees.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/simple3-marie-knees.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/simple3-marie-knees.jpg" alt="simple3-marie-knees" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/simple4-sarah-shoulders.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/simple4-sarah-shoulders.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/simple4-sarah-shoulders.jpg" alt="simple4-sarah-shoulders" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/simple5-ray-weakness.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/simple5-ray-weakness.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/simple5-ray-weakness.jpg" alt="simple5-ray-weakness" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg-light py-5">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<h2 class="text-transform-none pt-3 mb-4">What Your First SMM Day Will Look Like</h2>
					<p class="larger text-primary mb-0"><b>1. Signing Up</b></p>
					<p class="mb-5 mob-mb-3">After your one time purchase, you'll instantly be emailed a password to log in to your personal account, where you'll find the Simplistic Mobility Method waiting in your dashboard.</p>
					<picture>
						<source srcset="/img/lps/smm2022/firstday1-dashboard.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/firstday1-dashboard.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/firstday1-dashboard.png" alt="first day on smm" type="image/png" class="w-100 lazy d-lg-none mb-5 shadow"/>
					</picture>
					<p class="larger text-primary mb-0"><b>2. Your Tests</b></p>
					<p class="mb-5 mob-mb-3">First thing is to complete the tests, which aren't scary or hard! They're going to give you your baseline level so that you know exactly where you're starting from.</p>
					<picture>
						<source srcset="/img/lps/smm2022/firstday2-tests1-2.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/firstday2-tests1-2.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/firstday2-tests1-2.png" alt="first day on smm" type="image/png" class="w-100 lazy d-lg-none mb-5 shadow"/>
					</picture>
					<p class="larger text-primary mb-0"><b>3. The Exercises</b></p>
					<p class="mb-5 mob-mb-3">Then, Tom and Jenni take you through the exercises in real time with in-depth explanations + how to get the most benefits from each movement.</p>
					<picture>
						<source srcset="/img/lps/smm2022/firstday3-exercises.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/firstday3-exercises.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/firstday3-exercises.png" alt="first day on smm" type="image/png" class="w-100 lazy d-lg-none mb-5 shadow"/>
					</picture>
					<p class="larger text-primary mb-0"><b>4. Retest</b></p>
					<p class="mb-3">After your first session, you run through a retest to show the difference that can be made just from combining the right exercises in the right order!</p>
					<p class="mb-5 mob-mb-3">From now, you stick with the method, master the exercises and then retest every 4 weeks!</p>
					<picture>
						<source srcset="/img/lps/smm2022/firstday4-smm group.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/firstday4-smm group.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/firstday4-smm group.png" alt="first day on smm" type="image/png" class="w-100 lazy d-lg-none mb-5 shadow"/>
					</picture>
					<p class="larger text-primary mb-0"><b>5. Post in the Group!</b></p>
					<p class="mb-4 mob-mb-3">This step is optional, but highly encouraged! Our SMM community is just amazingly supportive, no matter what your starting point or injury history you'll always find someone who's been through something similar & has come out the other side stronger than ever!</p>
					<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Yes, I want to start today! >></button></a>
				</div>
				<div class="col-lg-4 d-none d-lg-block">
					<picture>
						<source srcset="/img/lps/smm2022/first-day-desktop.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/first-day-desktop.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/first-day-desktop.png" alt="first day on smm" type="image/png" class="w-100 lazy"/>
					</picture>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5 mob-pb-0">
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<h2 class="text-transform-none text-center pt-3 mb-5">What's Included In SMM?</h2>
			<div class="row mb-4">
				<div class="col-2 pr-5 mob-pr-3">
					<img src="/img/lps/icons/graph.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons graph" />
				</div>
				<div class="col-10 pl-0">
					<p class="text-large mob-text-s"><b>The Simplistic Mobility Method ebook</b> contains the unique program, PLUS the tests that you'll be use to measure your progress</p>
				</div>
			</div>
			<div class="row mb-4">
				<div class="col-2 pr-5 mob-pr-3">
					<img src="/img/lps/icons/play.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons play" />
				</div>
				<div class="col-10 pl-0">
					<p class="text-large mob-text-s">Comprehensive <b>video demos</b> containing the Tests & Exercises with Tom and Jenni for you to easily learn & avoid common mistakes</p>
				</div>
			</div>
			<div class="row mb-4">
				<div class="col-2 pr-5 mob-pr-3">
					<img src="/img/lps/icons/info.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons info" />
				</div>
				<div class="col-10 pl-0">
					<p class="text-large mob-text-s">Expert <b>instructions & demos</b> of how to use the Method plus extra information, regressions & reasoning behind the exercises</p>
				</div>
			</div>
			<div class="row mb-4 mob-mb-0">
				<div class="col-2 pr-5 mob-pr-3">
					<img src="/img/lps/icons/lightbulb.svg" class="img-fluid w-100 lazy" alt="Simplistic mobility method icons lightbulb" />
				</div>
				<div class="col-10 pl-0">
					<p class="text-large mob-text-s">A <b>Cheat Sheet</b> showing all the exercises in extra detail, with exactly which muscles are targeted + extra tips and things to look out for!</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid bg banner-bg text-center mt-5 overflow-hidden">
	<div class="row">
		<div class="container">
			<div class="row my-5">
				
				<div class="col-lg-8 text-left text-white">
					<h2 class="text-white text-transform-none pt-3 mb-4">PLUS! <u style="text-decoration-color: #d82737;">Sign Up Today</u> & Get These Awesome Extras:</h2>
					<ul class="list-check-green pl-0 mb-4">
						<li>
							<p class="larger mb-0"><b>Head To Toe Mobility</b></p>
							<p class="mob-mb-0">We also teach how to easily give yourself a quick 5 minute daily routine, this is a life-changing habit!</p>
							<picture>
								<source srcset="/img/lps/smm2022/extras1-head-to-toe.jpg" type="image/jpeg"/>
								<source srcset="/img/lps/smm2022/extras1-head-to-toe.webp" type="image/webp"/>
								<img src="/img/lps/smm2022/extras1-head-to-toe.jpg" alt="first day on smm" type="image/jpeg" class="to-edge lazy d-lg-none mb-5 mt-4 shadow"/>
							</picture>
						</li>
						<li>
							<p class="larger mb-0"><b>Lifetime Updates</b></p>
							<p class="mob-mb-0">We're constantly updating & improving SMM. We recently added "The Setback Protocol" and the "One-Rep Method", giving you even more tools to improve your body & your mindset!</p>
							<picture>
								<source srcset="/img/lps/smm2022/extras2-setbacks.png" type="image/png"/>
								<source srcset="/img/lps/smm2022/extras2-setbacks.webp" type="image/webp"/>
								<img src="/img/lps/smm2022/extras2-setbacks.png" alt="first day on smm" type="image/png" class="to-edge lazy d-lg-none mb-5 mt-4 shadow"/>
							</picture>
						</li>
						<li>
							<p class="larger mb-0"><b>Bonus Exercises</b></p>
							<p class="mob-mb-0">Optional bonus exercises that build on the SMM concepts to further increase your core strength, stability, and even your coordination!</p>
							<picture>
								<source srcset="/img/lps/smm2022/extras3-bonus.jpg" type="image/jpeg"/>
								<source srcset="/img/lps/smm2022/extras3-bonus.webp" type="image/webp"/>
								<img src="/img/lps/smm2022/extras3-bonus.jpg" alt="first day on smm" type="image/jpeg" class="to-edge lazy d-lg-none mb-5 mt-4 shadow"/>
							</picture>
						</li>
						<li>
							<p class="larger mb-0"><b>Access To The SMM Community</b></p>
							<p>You can ask any questions, post pictures & videos, share your wins or your struggles. The members are the most supportive group ever PLUS Tom & Jenni are active in the group daily!</p>
							<p class="mob-mb-0">When you join SMM you aren't alone anymore! You’ll not get care & support like this anywhere else for a one time purchase!</p>
							<picture>
								<source srcset="/img/lps/smm2022/extras4-community.png" type="image/png"/>
								<source srcset="/img/lps/smm2022/extras4-community.webp" type="image/webp"/>
								<img src="/img/lps/smm2022/extras4-community.png" alt="first day on smm" type="image/png" class="to-edge lazy d-lg-none mb-4 mt-4 shadow"/>
							</picture>
						</li>
					</ul>
					<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block ml-5 mob-mx-0">Yes, I'm ready to join SMM! >></button></a>
				</div>
				<div class="col-lg-4 d-none d-lg-block">
					<picture>
						<source srcset="/img/lps/smm2022/extras-desktop.png" type="image/png"/>
						<source srcset="/img/lps/smm2022/extras-desktop.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/extras-desktop.png" alt="extras" type="image/png" class="w-100 lazy"/>
					</picture>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container my-5">
	<div class="row justify-content-center">
		<div class="col-12 mb-4 text-center">
			<h2 class="text-transform-none pt-3 mb-3">All of this for less than the price of one physio appointment!</h2>
			<h2 class="text-transform-none pt-3 mb-4">Don’t miss out on this incredible offer!<br/><span class="text-primary">ONLY @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif TODAY!</span></h2>
		</div>
		<div class="col-12 text-center mb-5">
			<img src="/img/lps/stability-builder/list.svg" width="366" height="223" class="mw-100 h-auto"/>
		</div>
		<div class="col-12 text-center mb-3">
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block ml-5 mob-mx-0 mb-3">YES! I need SMM today! >></button></a>
			<p class="mb-0 text-small mb-3">*payments in other currencies accepted & converted at the current rate</p> 
			<img src="/img/logos/visa.svg" srcset="/img/logos/visa.svg" alt="Visa Logo" width="94" class="img-fluid lazy mob-mb-0 mob-mr-3 mr-5 loaded" srcset="/img/logos/visa.svg" src="/img/logos/visa.svg" data-was-processed="true" style="max-width: 12vw;"> 
			<img src="/img/logos/mastercard.svg" srcset="/img/logos/mastercard.svg" alt="Mastercard Logo" width="79" class="img-fluid lazy mob-mr-3 mob-mb-0 mr-5 loaded" srcset="/img/logos/mastercard.svg" src="/img/logos/mastercard.svg" data-was-processed="true" style="max-width: 10vw;">
			<img src="/img/logos/paypal.svg" srcset="/img/logos/paypal.svg" alt="PayPal Logo" width="137" class="img-fluid lazy mob-mb-0 mr-5 mob-mr-3 loaded" srcset="/img/logos/paypal.svg" src="/img/logos/paypal.svg" data-was-processed="true" style="max-width: 20vw;"> 
			<img src="/img/logos/amex.svg" srcset="/img/logos/amex.svg" alt="American Express Logo" width="74" class="img-fluid lazy mob-mb-0 loaded" srcset="/img/logos/amex.svg" src="/img/logos/amex.svg" data-was-processed="true" style="max-width: 9vw;">
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card-columns">
				@foreach($reviews as $review)
				<div class="card mt-5 mb-3">
					<div class="review-box bg-light p-3">
						<div class="review-avatar smaller">
							<img src="{{$review->avatar}}" alt="{{$review->name}}, Simplistic mobility method (SMM) review"/>
						</div>
						<h3 class="review-name smaller mb-4">{{$review->name}}<br/>@for($x = 1; $x<= $review->rating; $x++)<img src="/img/icons/star.svg" class="review-star" alt="Simplistic Mobility Method review star"/>@endfor</h3>
						<div class="review-body mob-height-auto pb-5" style="max-height: 400px; height: 400px; overflow: hidden;"> 
							{!!$review->content!!}
						</div>
						<p class="mt-3"><a href="/simplistic-mobility-method-reviews" target="_blank">Read More >></a></p>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
<div id="faqs" class="container-fluid bg-light">
	<div class="row">
		<div class="container my-5">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<h2 class="text-primary large-title text-capitalize mb-5 mob-mb-4">Frequently Asked Questions</h2>
				</div>
				<div class="col-lg-8">
					<div id="faq-accordion">
						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="false" aria-controls="collapse-1">Is SMM a subscription? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-1" data-parent="#faq-accordion">
							<p>No, The Simplistic Mobility Method® is a one-time purchase of @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}}. @else £{{$product->price}} @endif and you get lifetime access (with lifetime updates too!)</p>      
							<p>To put this in perspective, when I hurt my knee, I spent 6 weeks of physio on it - that cost me about £300. When I hurt my shoulder that was 3 sessions: £150.</p>
							<p>When I badly injured my back, I pretty much went every week for 6 months straight just to get some relief... I don’t even want to calculate that.</p>
							<p>The Simplistic Mobility Method® is @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif once. For a lifetime of benefits.</p>
							<p>You’ll learn about your body, how to spot injuries before they happen, it is a tried and tested method with real athletes. After people use SMM they have found that they no longer need to go to Physio as often, or even at all.</p>
							<p>PLUS! As an extra added bonus, you’ll become a part of a global community of SMMers via our <a href="https://www.facebook.com/groups/simplisticmobilitymethod/" target="_blank">Facebook group</a>, where you can ask questions directly to people who’ve been using the method for years, and directly to Tom & Jenni who are very active in the SMM community.</p>
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2">Will SMM work for me? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-2" data-parent="#faq-accordion">
							<p>After working with so many people over the years, there are always things we miss about training, mobility, and recovery – no matter how much research and practice you do. You will pick up imbalances and compensations over years of small mistakes, misinterpretations, and even daily habits.</p>
							<p>The Simplistic Mobility Method® allows you to learn what your body can and can’t do right now and you can use the tests & methodology to keep maintaining and checking your body throughout your entire lifetime.</p>
							<p>SMM is designed to make your joints sit and work correctly and teaches your body to move as one unit with great muscle activation.</p>
							<p>The best injury is the one that never happened!</p>
							<p>It doesn’t matter what you like to do for physical activity or training, everyone has the same joints and muscles, and they need to have a certain baseline mobility & stability to function well; SMM is that baseline.</p>
							<p>Your body won’t be sore all the time if it moves well.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3">What happens after I buy SMM? How do I access it? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-3" data-parent="#faq-accordion">
							<p>After you buy, you’ll receive an email with your login details and a link to sign into your new dashboard!</p>    
							<p>If you don’t receive an email, make sure to check your spam/junk folder.</p>
							<p>If it’s not there either, simply go to <a href="https://tommorrison.uk/login" target="_blank">https://tommorrison.uk/login</a> and click “Forgot Your Password?” to generate a new sign in email for you. Some email hosts don’t like automatic emails and block them to protect you from spam!</p>
							<p>Once you’re signed in, you will see the Simplistic Mobility Method® on your dashboard, and from there you can click into it and navigate through all the videos & PDFs easily.</p>
							<p>Everything is stored online for you, and any updates we make to the program will automatically appear for you.</p>
							<p>If you ever forget your password, don’t worry! The “Forgot Your Password” feature is always there! You won’t lose your account.    </p>      
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4">How long will it take for me to see results? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-4" data-parent="#faq-accordion">
							<p>After your first session. Yeah, really!</p>
							<p>In your first session you run through a series of tests before and after. Some people notice visual improvements right away, others just feel better, taller or that they are moving easier. Many are shocked at how much they struggled with (but this is a good thing, means there’s loads of gains to be had!)</p>
							<p>After that, as you’d expect, the more you use the program the faster you see results. You start to really see differences after 8 sessions. So, if you can manage 4 times per week - that’s only 2 weeks to awesome!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-5" role="button" aria-expanded="false" aria-controls="collapse-5">How long does it take to do SMM? Is it easy to stick to? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-5" data-parent="#faq-accordion">
							<p>An SMM session will take about 30 minutes, so it is pretty easy to slot into even a busy timetable. We’d recommend between 2-5 sessions per week, depending on how much time you have available.</p>
							<p>The better you get at SMM the less frequently you’ll need to do it. Plus, we have consistency tips like the “1-Rep Method” inside the ebook for anyone short on time, alongside a way you can condense your mobility practice into a 5 minute-a-day routine!</p>
							<p>Your very first session will take longer, often around an hour because you’ll go through a full body assessment, and you’ll want to watch the videos to check the technique, tips and progressions. You’ve also got optional checklists and note sheets that you can use to mark your progress.</p>
							<p>Even if you can only manage one session a week it’s better than none!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-6" role="button" aria-expanded="false" aria-controls="collapse-6">I’m not very flexible, is SMM suitable for total beginners? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-6" data-parent="#faq-accordion">
							<p>Yes! Don’t worry, trying to get flexible before starting a mobility program is like trying to learn to drive before your first driving lesson!</p>
							<p>The Simplistic Mobility Method® is what you need to increase your flexibility in a safe way. Not just stretching for no reason, but intentional, directed exercises to build both mobility and stability.</p>
							<p>All the exercises have different levels of regressions/progressions so you can do SMM no matter what level you’re at.</p>
							<p>And on the flip side, if you are too flexible then the movements are also designed to develop control, add more stability, and build confidence in your body!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-7" role="button" aria-expanded="false" aria-controls="collapse-7">I have an injury/pain, can I still do SMM? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-7" data-parent="#faq-accordion">
							<p>If it is a fresh injury, it’s always best to have an in-person assessment by a Physiotherapist, preferably one with an active/sports background who understands the type of training that you do. If it is intense, sudden sharp pain or nerve pain then I’d recommend seeing your doctor and make sure you get cleared to do mobility work. </p>
							<p>However, if you have an old injury that’s still giving you niggles, recurring aches/pains, or you’ve lived with things like disc injuries for a long time and you’ve already been through all the proper channels then you will be ok to use SMM and try just using the regression examples with relaxed breathing. </p>
							<p>Technique is key so make sure to film yourself as you are doing the SMM movements - a lot of times what you think you are doing and what you are actually doing can be very different, so it’s always good to double check everything!</p>     
							<p>If you feel stuck or scared that you’ll hurt yourself further, it would be good to <a href="/products/video-call" target="_blank">book a video call with Tom</a>to get more guidance specific to your own training and injury history.</p>  
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-8" role="button" aria-expanded="false" aria-controls="collapse-8">I do/play a sport will SMM help me? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-8" data-parent="#faq-accordion">
							<p>Yup, the Simplistic Mobility Method® is designed to help the human body – regardless of what sport that body does.</p>
							<p>In our SMM community group we have footballers, crossfitters, kettlebell athletes, powerlifters, weightlifters, rugby players, runners, normal gym goers, bodybuilders, coaches, physiotherapists, bodyweight enthusiasts, pole dancers, even people that aren’t interested in fitness and just want to feel good in their day-to-day life!</p>
							<p>We all have a body, sports and activities is all the fun stuff you can do after your body moves well. SMM is like the background operating systems of your phone to keep it running smoothly. If there’s a ‘bug’ in a specific area, your body will not perform the way it should!</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-9" role="button" aria-expanded="false" aria-controls="collapse-9">Can I do SMM alongside my own training? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-9" data-parent="#faq-accordion">
							<p>Yep! We would argue that SMM is an essential part of your training. It runs through everything you need for good performance.</p>
							<p>No more wasting time trying to figure out what’s causing that niggle, rolling out tightness, or getting stuck as a plateau, SMM gives you the tools to actively support and progress your training.</p>
							<p>However, if you’re really in a bad way with a current or longstanding pain and your tests show that you have a lot to work on, then we recommend focusing solely on SMM for a few weeks and then return to training when you’ve improved your movements.</p>              
						</div>

						<p class="mob-mt-3 mb-0 text-large mob-text-small"><b><a class="text-dark" data-toggle="collapse" href="#collapse-10" role="button" aria-expanded="false" aria-controls="collapse-10">Do I need equipment to do SMM? <i class="fa fa-angle-down float-right collapsed"></i><i class="fa fa-angle-up float-right expanded"></i></a></b></p>
						<hr class="grey-line" />
						<div class="collapse" id="collapse-10" data-parent="#faq-accordion">
							<p>Nope, not at all! Every exercise can be done at home, at the gym, while travelling... any where!</p>
							<p>Every movement we use is bodyweight. The aim is to teach you how to move using your own body as the main tool, rather than relying on external forces!</p>              
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container py-5">
	<div class="row">
		<div class="col-12 d-lg-none">
			<div class="smm-slider ">
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/bottom-pics1.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/bottom-pics1.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/bottom-pics1.jpg" alt="bottom pics 1" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/bottom-pics2.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/bottom-pics2.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/bottom-pics2.jpg" alt="bottom pics 2" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
				<li style="list-style: none;">
					<picture>
						<source srcset="/img/lps/smm2022/bottom-pics3.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/bottom-pics3.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/bottom-pics3.jpg" alt="bottom pics 3" type="image/jpeg" class="w-100 lazy"/>
					</picture>
				</li>
			</div>
		</div>
		<div class="col-lg-4 d-none d-lg-block">
			<picture>
				<source srcset="/img/lps/smm2022/bottom-pics1.jpg" type="image/jpeg"/>
				<source srcset="/img/lps/smm2022/bottom-pics1.webp" type="image/webp"/>
				<img src="/img/lps/smm2022/bottom-pics1.jpg" alt="bottom pics 1" type="image/jpeg" class="w-100 lazy"/>
			</picture>
		</div>
		<div class="col-lg-4 d-none d-lg-block">
			<picture>
				<source srcset="/img/lps/smm2022/bottom-pics2.jpg" type="image/jpeg"/>
				<source srcset="/img/lps/smm2022/bottom-pics2.webp" type="image/webp"/>
				<img src="/img/lps/smm2022/bottom-pics2.jpg" alt="bottom pics 2" type="image/jpeg" class="w-100 lazy"/>
			</picture>
		</div>
		<div class="col-lg-4 d-none d-lg-block">
			<picture>
				<source srcset="/img/lps/smm2022/bottom-pics3.jpg" type="image/jpeg"/>
				<source srcset="/img/lps/smm2022/bottom-pics3.webp" type="image/webp"/>
				<img src="/img/lps/smm2022/bottom-pics3.jpg" alt="bottom pics 3" type="image/jpeg" class="w-100 lazy"/>
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid bg banner-bg2 text-center">
	<div class="row">
		<div class="col-12 py-5">
			<p class="text-white">Never let yourself be set back again by unnecessary injuries or aches & pains<br/>Build complete strength and figure out what you have been missing with the Simplistic Mobility Method®</p>
			<h2 class="text-white mb-4 text-transform-none">Only @if($product->sale_price)<s>£{{$product->price}}</s> £{{$product->sale_price}} @else £{{$product->price}} @endif TODAY!</h2>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">Take control of your mobility >></button></a>
		</div>
	</div>
</div>
<div class="container py-5 mob-pb-0">
	<div class="row mt-3">
		<div class="col-lg-7">
			<div class="floating-bg-box scroll_fade" data-fade="show-red">
				<div class="content p-4">
					<h2 class="text-transform-none">About Tom Morrison</h2>
					<p>Tom is unusual compared to most coaches; he didn't start any kind of training until 24.</p>
					<p>With no foundation whatsoever, after a few years his newfound enthusiasm caught up with him.</p>
					<p>Everything he'd had missed over the years caused him to badly injure his shoulder, knee and the worst he endured was his back: an L4/L5/S1 disc protrusion & extrusion + nerve impingement.</p>
					<p>With his career seemingly over before it had begun, he set out on a path of learning. He realised how important it is to have a basic level of mobility & stability throughout the entire body, and how so many people didn't.</p>
					<p>He realised the approach needed to be different to what is commonly taught.</p>
					<p>Tom wants to help people realise that they're in control of their body, for people to know exactly what they need to work on, and to show the principles of good movement are actually pretty simple.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-bottom w-100">
					<picture>
						<source srcset="/img/lps/smm2022/about-tom-morrison.jpg" type="image/jpeg"/>
						<source srcset="/img/lps/smm2022/about-tom-morrison.webp" type="image/webp"/>
						<img src="/img/lps/smm2022/about-tom-morrison.jpg" alt="about-tom-morrison" type="image/jpeg" width="550" class="lazy about-tom-morrison-smm2022"/>
					</picture>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container pt-3 pb-5 mob-pt-5">
	<div class="row">
		<div class="col-12 text-center">
			<p class="mb-0">Everything Tom has learned has been stripped down & condensed into the fundamental movements that actually work.</p>
			<h2 class="mb-4">DON'T WASTE YOUR TIME...</h2>
			<a href="/basket/add/1"><button class="btn btn-primary btn-small-text d-inline-block">PICK UP THE METHOD TODAY! >></button></a>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
$(document).ready(function(){
		var smmslider = $(".smm-slider").bxSlider({
			auto: true,
			autoControls: false,
			controls: false,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
		var smmslider = $(".smm-slider-nav").bxSlider({
			auto: false,
			autoControls: true,
			controls: true,
			speed: 800,
			stopAutoOnClick: true,
			randomStart: false,
			pause: 8000,
		});
	});
</script>
	<script>
		$(document).ready(function(){
			if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	    backToTop = function () {
	    	var scrollTop = $(window).scrollTop();
	    	if (scrollTop > scrollTrigger) {
	    		$('#back-to-top').addClass('show');
	    	} else {
	    		$('#back-to-top').removeClass('show');
	    	}
	    };
	    backToTop();
	    $(window).on('scroll', function () {
	    	backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	    	e.preventDefault();
	    	$('html,body').animate({
	    		scrollTop: 0
	    	}, 700);
	    });
	  }
	});
		window.addEventListener('load', (event) => {
		var inview = new Waypoint({
			element: $('#mobility-stats-counters'),
			handler: function (direction) {
				(function ($) {
					$.fn.countTo = function (options) {
						options = options || {};

						return $(this).each(function () {
					// set options for current element
					var settings = $.extend({}, $.fn.countTo.defaults, {
						from:            $(this).data('from'),
						to:              $(this).data('to'),
						speed:           $(this).data('speed'),
						refreshInterval: $(this).data('refresh-interval'),
						decimals:        $(this).data('decimals')
					}, options);
					
					// how many times to update the value, and how much to increment the value on each update
					var loops = Math.ceil(settings.speed / settings.refreshInterval),
					increment = (settings.to - settings.from) / loops;
					
					// references & variables that will change with each update
					var self = this,
					$self = $(this),
					loopCount = 0,
					value = settings.from,
					data = $self.data('countTo') || {};
					
					$self.data('countTo', data);
					
					// if an existing interval can be found, clear it first
					if (data.interval) {
						clearInterval(data.interval);
					}
					data.interval = setInterval(updateTimer, settings.refreshInterval);
					
					// initialize the element with the starting value
					render(value);
					
					function updateTimer() {
						value += increment;
						loopCount++;
						
						render(value);
						
						if (typeof(settings.onUpdate) == 'function') {
							settings.onUpdate.call(self, value);
						}
						
						if (loopCount >= loops) {
							// remove the interval
							$self.removeData('countTo');
							clearInterval(data.interval);
							value = settings.to;
							
							if (typeof(settings.onComplete) == 'function') {
								settings.onComplete.call(self, value);
							}
						}
					}
					
					function render(value) {
						var formattedValue = settings.formatter.call(self, value, settings);
						$self.html(formattedValue);
					}
				});
					};

					$.fn.countTo.defaults = {
				from: 0,               // the number the element should start at
				to: 0,                 // the number the element should end at
				speed: 1000,           // how long it should take to count between the target numbers
				refreshInterval: 100,  // how often the element should be updated
				decimals: 0,           // the number of decimal places to show
				formatter: formatter,  // handler for formatting the value before rendering
				onUpdate: null,        // callback method for every time the element is updated
				onComplete: null       // callback method for when the element finishes updating
			};
			
			function formatter(value, settings) {
				return value.toFixed(settings.decimals);
			}
		}(jQuery));

				jQuery(function ($) {
		  // custom formatting example
		  $('.count-number').data('countToOptions', {
		  	formatter: function (value, options) {
		  		return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
		  	}
		  });
		  
		  // start all the timers
		  $('.timer').each(count);  
		  
		  function count(options) {
		  	var $this = $(this);
		  	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		  	$this.countTo(options);
		  }
		});
			},offset: '90%'
		});
});

	</script>
	<style>
		.timer{
			display: inline !important;
			width: initial !important;
		}
		.review-body img{
			display: none !important;
		}
		</style>
	@endsection