function fadeEffects(){
	document.querySelectorAll('.scroll_fade').forEach(function(element) {
	    var effect = element.getAttribute("data-fade");
	    var inview = new Waypoint({
	        element: element,
	        handler: function(direction) {
	            if (direction === "down") {
	                if (element.classList.contains("delay")) {
	                    setTimeout(function() {
	                        element.classList.add("animated", effect);
	                        element.style.opacity = '1';
	                    }, 800);
	                } else if (element.classList.contains("delay_half")) {
	                    setTimeout(function() {
	                        element.classList.add("animated", effect);
	                        element.style.opacity = '1';
	                    }, 400);
	                } else if (element.classList.contains("delay_quarter")) {
	                    setTimeout(function() {
	                        element.classList.add("animated", effect);
	                        element.style.opacity = '1';
	                    }, 200);
	                } else {
	                    element.classList.add("animated", effect);
	                    element.style.opacity = '1';
	                }
	            }
	        },
	        offset: '95%'
	    });
	});

}
function square(){
	document.querySelectorAll('.square').forEach(function(element) {
    var theWidth = element.offsetWidth;
    element.style.height = theWidth + "px";
	});

}
document.addEventListener('DOMContentLoaded', function() {
	fadeEffects();
	square();


	
	document.getElementById("loader-close").addEventListener("click", function() {
	  document.getElementById("loader").classList.remove("on");
	  document.getElementById("loader-message").innerHTML = "";
	  document.getElementById("loader-second-text").innerHTML = "";
	  document.getElementById("close-loader-btn").classList.remove("d-block");
	  document.getElementById("close-loader-btn").classList.add("d-none");
	  document.getElementById("loader-roller").classList.remove("d-none");
	  document.getElementById("loader-roller").classList.add("d-block");
	  document.getElementById("loader-success").classList.remove("d-block");
	  document.getElementById("loader-success").classList.add("d-none");
	});
	document.getElementById("close-loader-btn").addEventListener("click", function() {
	  document.getElementById("loader").classList.remove("on");
	  document.getElementById("loader-message").innerHTML = "";
	  document.getElementById("loader-second-text").innerHTML = "";
	  document.getElementById("close-loader-btn").classList.remove("d-block");
	  document.getElementById("close-loader-btn").classList.add("d-none");
	  document.getElementById("loader-roller").classList.remove("d-none");
	  document.getElementById("loader-roller").classList.add("d-block");
	  document.getElementById("loader-success").classList.remove("d-block");
	  document.getElementById("loader-success").classList.add("d-none");
	});

	document.addEventListener("click", function(event) {
	  if (event.target.classList.contains("page-link")) {
	    event.preventDefault();
	    document.querySelector("html, body").animate({
	      scrollTop: document.querySelector(".page-scroll-to-top").offsetTop + 500
	    }, 1000);
		}
	});

		let smmcount = 0;

	document.querySelectorAll('.smm-menu-btn').forEach(function(button) {
	    button.addEventListener('click', function() {
	        const smmMenu = document.getElementById('smm-menu');
	        if (smmcount === 0) {
	            smmMenu.classList.toggle('on');
	            button.textContent = 'close [x]';
	            smmcount = 1;
	        } else {
	            smmMenu.classList.toggle('on');
	            button.textContent = 'menu';
	            smmcount = 0;
	        }
	    });
	});

	document.querySelectorAll('#smm-menu .nav-link').forEach(function(link) {
	    link.addEventListener('click', function() {
	        const smmMenu = document.getElementById('smm-menu');
	        smmMenu.classList.toggle('on');
	        document.querySelector('.smm-menu-btn').textContent = 'menu';
	        smmcount = 0;
	    });
	});

	let menu_count = 0;

	
});