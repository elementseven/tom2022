import './bootstrap';
import * as bootstrap from 'bootstrap';

import {createStorefrontApiClient} from '@shopify/storefront-api-client';
import { createApp } from 'vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';
import 'waypoints/lib/noframework.waypoints.js';
import './plugins/cookieConsent.js';
import './plugins/modernizr-custom.js';

import './a-general.js';
import 'vue3-carousel/dist/carousel.css';

// Components
import MainMenu from './components/menus/MainMenu.vue';
import Loader from './components/Loader.vue';
import MailingList from './components/MailingList.vue';
import HomePage from './components/pages/Home.vue';
import SaleModal from './components/sale/SaleModal.vue';

const app = createApp({});

const client = createStorefrontApiClient({
  storeDomain: import.meta.env.VITE_SHOPIFY_MYSHOPIFY_DOMAIN,
  publicAccessToken: import.meta.env.VITE_SHOPIFY_ACCESS_TOKEN,
  apiVersion: '2025-01', // Use a valid API version
});


// Register global components
app.component('main-menu', MainMenu);
app.component('loader', Loader);
app.component('mailing-list', MailingList);
app.component('home-page', HomePage);
app.component('sale-modal', SaleModal);

app.config.globalProperties.$shopifyClient = client;  // Use $shopifyClient in any component


// Use plugins
app.use(VueReCaptcha, { 
  siteKey: '6Ld4MM0pAAAAAJRwwf9L1pS3nQshC2crKI0HvSkg',
  loaderOptions: {
    autoHideBadge: true  // Automatically hides the badge if you want this behavior globally
  }
})

app.mount('#app');