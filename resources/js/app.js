import './bootstrap';
import * as bootstrap from 'bootstrap';

import { VueReCaptcha } from 'vue-recaptcha-v3';

import { createApp } from 'vue';
import 'waypoints/lib/noframework.waypoints.js';
import './plugins/cookieConsent.js';
import './plugins/modernizr-custom.js';
import {createStorefrontApiClient} from '@shopify/storefront-api-client';

import './a-general.js';
import 'vue3-carousel/dist/carousel.css';

// Components
import MainMenu from './components/menus/MainMenu.vue';
import Loader from './components/Loader.vue';

import MailingList from './components/MailingList.vue';
import SevenDaysMailingList from './components/SevenDaysMailingList.vue';

import Countries from './components/Countries.vue';
import ContactForm from './components/ContactForm.vue';
import SupportForm from './components/SupportForm.vue';
import AskAQuestion from './components/AskAQuestion.vue';
import Questionnaire from './components/Questionnaire.vue';
import VideoQuestionnaire from './components/VideoQuestionnaire.vue';
import VideoTestimonials from './components/VideoTestimonials.vue';
import ShareJourney from './components/ShareJourney.vue';

import ShopIndex from './components/shop/Index.vue';
import Products from './components/Products.vue';
import ProductBundles from './components/shop/ProductBundles.vue';
import Timer from './components/sale/Timer.vue';
import OrderStatus from './components/shop/OrderStatus.vue';

import FreeVideos from './components/FreeVideos.vue';
import Seminars from './components/seminars/Seminars.vue';
import RequestSeminar from './components/seminars/RequestSeminar.vue';
import SeminarsSlider from './components/seminars/SeminarsSlider.vue';
import Facts from './components/Facts.vue';

import Blog from './components/blog/Index.vue';
import PodcastIndex from './components/podcast/Index.vue';

import HomePage from './components/pages/Home.vue';
import Help from './components/help/Help.vue';

import SmmSlider from './components/products/SmmSlider.vue';
import UltimateCoreSlider from './components/products/UltimateCoreSlider.vue';
import EBookSlider from './components/products/EBookSlider.vue';
import StabilityBuilderSlider from './components/products/StabilityBuilderSlider.vue';
import EndRangeSlider from './components/products/EndRangeSlider.vue';
import SplitsHipsSlider from './components/products/SplitsHipsSlider.vue';
import BarbellBasicsSlider from './components/products/BarbellBasicsSlider.vue';
import BeginnersBundleSlider from './components/bundles/BeginnersBundleSlider.vue';
import IntermediateBundleSlider from './components/bundles/IntermediateBundleSlider.vue';
import CompleteBundleSlider from './components/bundles/CompleteBundleSlider.vue';

import StripeCheckout from './components/shop/StripeCheckout.vue';
import GiftCheckout from './components/shop/GiftCheckout.vue';

import SaleModal from './components/sale/SaleModal.vue';
import productCard from './components/shop/ProductCard.vue';
import ShopifyIndex from './components/Shopify/Index.vue';
import ShopifyProduct from './components/Shopify/Show.vue';
import ShopifyOtherProducts from './components/Shopify/OtherProducts.vue';
import ShopifyCart from './components/Shopify/Cart.vue';


const client = createStorefrontApiClient({
  storeDomain: import.meta.env.VITE_SHOPIFY_MYSHOPIFY_DOMAIN,
  publicAccessToken: import.meta.env.VITE_SHOPIFY_ACCESS_TOKEN,
  apiVersion: '2025-01', // Use a valid API version
});

export default client;

const app = createApp({
  components: {
    'main-menu': MainMenu,
    'loader': Loader,

    'mailing-list': MailingList,
    'seven-days-mailing-list': SevenDaysMailingList,

    'countries': Countries,
    'contact-form': ContactForm,
    'support-form': SupportForm,
    'ask-a-question': AskAQuestion,
    'questionnaire': Questionnaire,
    'video-questionnaire': VideoQuestionnaire,
    'video-testimonials': VideoTestimonials,
    'share-journey': ShareJourney,

    'shop-index': ShopIndex,
    'products': Products,
    'product-bundles': ProductBundles,
    'timer': Timer,
    'order-status': OrderStatus,

    'free-videos': FreeVideos,
    'seminars': Seminars,
    'seminars-slider': SeminarsSlider,
    'request-seminar': RequestSeminar,
    'facts': Facts,

    'blog': Blog,

    'home-page': HomePage,
    'help': Help,

    'smm-slider': SmmSlider,
    'ultimate-core-slider': UltimateCoreSlider,
    'e-book-slider': EBookSlider,
    'stability-builder-slider': StabilityBuilderSlider,
    'end-range-slider': EndRangeSlider,
    'splits-hips-slider': SplitsHipsSlider,
    'barbell-basics-slider': BarbellBasicsSlider,
    'beginners-bundle-slider': BeginnersBundleSlider,
    'intermediate-bundle-slider': IntermediateBundleSlider,
    'complete-bundle-slider': CompleteBundleSlider,

    'stripe-checkout': StripeCheckout,
    'gift-checkout': GiftCheckout,

    'sale-modal': SaleModal,

    'podcast-index': PodcastIndex,
    'product-card' : productCard,

    'shopify-index' : ShopifyIndex,
    'shopify-product': ShopifyProduct,
    'shopify-other-products': ShopifyOtherProducts,
    'shopify-cart': ShopifyCart
  },
}); 

app.config.globalProperties.$shopifyClient = client;  // Use $shopifyClient in any component

// Use plugins
app.use(VueReCaptcha, { 
  siteKey: '6Ld4MM0pAAAAAJRwwf9L1pS3nQshC2crKI0HvSkg',
  loaderOptions: {
    autoHideBadge: true  // Automatically hides the badge if you want this behavior globally
  }
})

app.mount('#app');