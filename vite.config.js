import path from 'path';
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue'; 
import purge from '@erbelion/vite-plugin-laravel-purgecss'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
                'resources/js/home.js',
            ],
            refresh: true,
        }),
        vue({ 
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        purge({
            templates: ['blade', 'vue'],
            safelist: ['carousel','carousel__track','carousel__viewport','carousel__sr-only','carousel__prev','carousel__next','carousel--rtl','carousel__pagination','carousel__pagination-button','carousel__slide','carousel__icon']
        })
    ],
    resolve: { 
        alias: {
            '@': path.resolve(__dirname, 'resources/js'),
            '$fonts': path.resolve(__dirname, "public/fonts"),
            '$img': path.resolve(__dirname, "public/img"),
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
});
