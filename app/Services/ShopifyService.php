<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ShopifyService
{
    protected $shopUrl;
    protected $accessToken;

    public function __construct($shopUrl, $accessToken)
    {
        $this->shopUrl = "https://{$shopUrl}/api/2025-01/graphql.json";
        $this->accessToken = $accessToken;
    }

    public function getPaginatedProducts($cursor = null, $limit = 12)
    {
        $collectionId = "gid://shopify/Collection/631557783891";  // GraphQL ID of the collection
        $afterClause = $cursor ? ", after: \"$cursor\"" : "";

        $query = <<<GRAPHQL
        {
          collection(id: "$collectionId") {
            title
            products(first: $limit $afterClause) {
              edges {
                cursor
                node {
                  id
                  title
                  description
                  images(first: 1) {
                    edges {
                      node {
                        src
                      }
                    }
                  }
                  variants(first: 1) {
                    edges {
                      node {
                        priceV2 {
                          amount
                          currencyCode
                        }
                      }
                    }
                  }
                }
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
        }
        GRAPHQL;

        $response = Http::withHeaders([
            'X-Shopify-Storefront-Access-Token' => $this->accessToken,
            'Content-Type' => 'application/json',
        ])->post($this->shopUrl, [
            'query' => $query
        ]);

        if (!$response->successful()) {
            Log::error('Failed to retrieve products from Shopify', [
                'response' => $response->body()
            ]);
            return [];
        }

        return $response->json()['data']['collection']['products'] ?? []; // Return product data within the collection or an empty array
    }


    public function getProductById($id)
    {   
        $query = <<<GRAPHQL
        {
          product(id: "$id") {
            id
            title
            description
            descriptionHtml
            images(first: 10) {
              edges {
                node {
                  src
                }
              }
            }
            variants(first: 10) {
              edges {
                node {
                    id
                  priceV2 {
                    amount
                    currencyCode
                  }
                  title
                }
              }
            }
          }
        }
        GRAPHQL;

        $response = Http::withHeaders([
            'X-Shopify-Storefront-Access-Token' => $this->accessToken,
            'Content-Type' => 'application/json',
        ])->post($this->shopUrl, [
            'query' => $query
        ]);

        if (!$response->successful()) {
            Log::error('Failed to retrieve product from Shopify', [
                'response' => $response->body()
            ]);
            return null;
        }

        return $response->json()['data']['product'] ?? null;
    }

    public function getProductsExcludingCart($cartProductIds, $limit = 10)
    {
        // Ensure cart product IDs is an array of full Shopify global IDs
        $cartProductIdsArray = !empty($cartProductIds) ? explode(',', $cartProductIds) : [];

        // Shopify does not support exclusion filtering, so fetch more products than the limit to filter manually
        $fetchLimit = $limit + count($cartProductIdsArray); // Fetch extra to account for excluded products
        $query = <<<GRAPHQL
        {
          products(first: $fetchLimit) {
            edges {
              node {
                id
                title
                images(first: 1) {
                  edges {
                    node {
                      src
                    }
                  }
                }
                variants(first: 1) {
                  edges {
                    node {
                      priceV2 {
                        amount
                        currencyCode
                      }
                    }
                  }
                }
              }
            }
          }
        }
        GRAPHQL;

        $response = Http::withHeaders([
            'X-Shopify-Storefront-Access-Token' => $this->accessToken,
            'Content-Type' => 'application/json',
        ])->post($this->shopUrl, [
            'query' => $query
        ]);

        if (!$response->successful()) {
            Log::error('Failed to retrieve products from Shopify', [
                'response' => $response->body()
            ]);
            return [];
        }

        // Get the products data
        $products = $response->json()['data']['products']['edges'] ?? [];

        // Manually filter out products that are in the cart
        $filteredProducts = array_filter($products, function($product) use ($cartProductIdsArray) {
            // Compare full Shopify global product ID with cart items
            return !in_array($product['node']['id'], $cartProductIdsArray);
        });

        // Limit the filtered results to the desired number of products
        $filteredProducts = array_slice($filteredProducts, 0, $limit);

        // Log::info('Products: ' . json_encode($filteredProducts));

        
        return json_encode($filteredProducts);
    }




}
