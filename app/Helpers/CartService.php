<?php 

namespace App\Helpers;

use App\Models\Upgrade;
use App\Models\Product;
use App\Models\Bundle;
use App\Models\User;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Log;

class CartService
{
    /**
     * Check if the product or bundle the user is trying to add is already covered by their purchases or other cart items.
     * Return a message explaining the reason for redundancy.
     */
    public function checkPurchaseRedundancy($userId, $itemId, $itemType)
    {
        if ($itemType === 'product') {
            return $this->checkProductRedundancy($userId, $itemId);
        } elseif ($itemType === 'bundle') {
            return $this->checkBundleRedundancy($userId, $itemId);
        }else if ($itemType === 'upgrade') {
            return $this->checkUpgradeRedundancy($userId, $itemId);
        }

        return "Invalid item type specified.";
    }

    /**
     * Check if the user already has the product or if the product is part of a bundle in their cart.
     * Return descriptive text if the addition is redundant.
     */
    private function checkProductRedundancy($userId, $productId)
    {
        // Check if the user already owns the product
        if (User::where('id', $userId)->whereHas('products', function ($query) use ($productId) {
            $query->where('id', $productId);
        })->exists()) {
            return "You have already purchased this product.";
        }

        // Check if a bundle containing the product is in their cart
        if (!Cart::count() == 0 && $this->isProductInBundleInCart($productId)) {
            return "A bundle in your basket already contains this product.";
        }

        // Check if a upgrade containing the product is in their cart
        if (!Cart::count() == 0 && $this->isProductInUpgradeInCart($productId)) {
            return "A upgrade in your basket already contains this product.";
        }

        return "This product can be added to your basket.";
    }


    /**
     * Check if the product is part of a bundle already in the user's cart.
     * Return true if it is, otherwise false.
     */
    private function isProductInBundleInCart($productId)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $item) {
            if ($item->options->type === 'bundle') {
                $bundle = Bundle::with('products')->find($item->id);
                if ($bundle && $bundle->products->contains('id', $productId)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if the product is part of a upgrade already in the user's cart.
     * Return true if it is, otherwise false.
     */
    private function isProductInUpgradeInCart($productId)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $item) {
            if ($item->options->type === 'upgrade') {
                $upgrade = Upgrade::with('products')->find($item->id);
                if ($upgrade && $upgrade->products->contains('id', $productId)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Check if any product in the bundle is not owned by the user and not already in the cart.
     * Return descriptive text if the addition is redundant.
     */
    private function checkBundleRedundancy($userId, $bundleId)
    {
        $bundle = Bundle::with('products')->find($bundleId);
        if (!$bundle) {
            return "Bundle not found.";
        }
        if($userId){
            $productsOwned = User::where('id', $userId)->with('products')->first()->products->pluck('id')->toArray();
        }
        $productsInCart = Cart::content()->filter(function ($item) {
            return $item->options->type === 'product';
        })->pluck('id')->toArray();

        foreach ($bundle->products as $product) {
            if($userId){
                if (in_array($product->id, $productsOwned)) {
                    Log::info("User $userId already owns product $product->id in bundle $bundleId");
                    return "You already own a product in this bundle.";
                }
            }

            if (in_array($product->id, $productsInCart) || $this->isProductInAnyBundleInCart($product->id)) {
                return "A product from this bundle is already in your basket.";
            }
        }

        return "This bundle can be added to your basket.";
    }

    /**
     * Check if any product in the upgrade is not owned by the user and not already in the cart.
     * Return descriptive text if the addition is redundant.
     */
    private function checkUpgradeRedundancy($userId, $upgradeId)
    {
        $upgrade = Upgrade::with('products')->find($upgradeId);
        if (!$upgrade) {
            return "Upgrade not found.";
        }
        if($userId){
            $productsOwned = User::where('id', $userId)->with('products')->first()->products->pluck('id')->toArray();
        }
        $productsInCart = Cart::content()->filter(function ($item) {
            return $item->options->type === 'product';
        })->pluck('id')->toArray();

        foreach ($upgrade->products as $product) {
            if($userId){
                if (in_array($product->id, $productsOwned)) {
                    Log::info("User $userId already owns product $product->id in upgrade $upgradeId");
                    return "You already own a product in this upgrade.";
                }
            }

            if (in_array($product->id, $productsInCart) || $this->isProductInAnyUpgradeInCart($product->id)) {
                return "A product from this upgrade is already in your basket.";
            }
        }

        return "This upgrade can be added to your basket.";
    }



    /**
     * Check if a specific product is in any bundle in the user's cart.
     * Return true if it is, otherwise false.
     */
    private function isProductInAnyBundleInCart($productId)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $item) {
            if ($item->options->type === 'bundle') {
                $bundle = Bundle::with('products')->where('id', $item->id)->first();
                if ($bundle && $bundle->products->contains('id', $productId)) {
                    Log::info("Found product $productId in bundle $item->id in cart.");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if a specific product is in any upgrade in the user's cart.
     * Return true if it is, otherwise false.
     */
    private function isProductInAnyUpgradeInCart($productId)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $item) {
            if ($item->options->type === 'upgrade') {
                $upgrade = Upgrade::with('products')->where('id', $item->id)->first();
                if ($upgrade && $upgrade->products->contains('id', $productId)) {
                    Log::info("Found product $productId in upgrade $item->id in cart.");
                    return true;
                }
            }
        }
        return false;
    }



    public function isItemInCart($itemId, $itemType)
    {
        $cartItems = Cart::content();
        foreach ($cartItems as $item) {
            if ($item->id == $itemId && $item->options->type == $itemType) {
                return true;
            }
        }
        return false;
    }

}
