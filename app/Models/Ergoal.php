<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ergoal extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'gender',
        'zero',
        'one',
        'two',
        'three',
        'img'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $punc = array("?", "!", "'", ",", ":", ";", "/", "|", "(", ")", "&");
            $title = str_replace($punc, '', strtolower($post->title));
            $post->slug = str_replace(' ', '-', $title);
        });
    }
    public function users(){
        return $this->belongsToMany('App\Models\User', 'ergoal_user')->withPivot('user_id','ergoal_id', 'level');
    }


}
