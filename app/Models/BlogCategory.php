<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BlogCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($blogCategory) {
            $blogCategory->slug = Str::slug($blogCategory->name, "-");
        });
    }

    public function posts(){
        return $this->belongsToMany('App\Models\Post', 'blog_category_post')->withPivot('post_id');
    }

}
