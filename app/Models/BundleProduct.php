<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Log;

class BundleProduct extends Pivot
{
    protected $table = 'bundle_product';

    protected $fillable = ['bundle_id', 'product_id', 'price'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($pivot) {
            Log::info('Pivot created', ['pivot' => $pivot]);
            self::updateBundleSalePrice($pivot->bundle_id);
        });

        static::updated(function ($pivot) {
            Log::info('Pivot updated', ['pivot' => $pivot]);
            self::updateBundleSalePrice($pivot->bundle_id);
        });

        static::deleted(function ($pivot) {
            Log::info('Pivot updated', ['pivot' => $pivot]);
            self::updateBundleSalePrice($pivot->bundle_id);
        });
    }

    protected static function updateBundleSalePrice($bundleId)
    {
        $bundle = Bundle::find($bundleId);
        $totalPrice = $bundle->products()->sum('bundle_product.price');
        $bundle->sale_price = $totalPrice;
        $bundle->save();

        Log::info('Updated bundle Sale Price', ['totalPrice' => $totalPrice]);
    }
}

