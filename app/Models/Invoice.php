<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'transaction_id',
        'price',
        'paid',
        'payment_method',
        'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'invoice_product')->withPivot('product_id','price','qty');
    }

    public function variants(){
        return $this->belongsToMany('App\Models\Variant', 'invoice_variant')->withPivot('variant_id','price','qty');
    }

    public function vouchers(){
        return $this->belongsToMany('App\Models\Voucher', 'invoice_voucher')->withPivot('voucher_id');
    }

    public function bundles(){
        return $this->belongsToMany('App\Models\Bundle', 'bundle_invoice')->withPivot('bundle_id','price');
    }

    public function orders(){
        return $this->hasMany('App\Models\Order');
    }
    
    public function gift(){
        return $this->hasOne('App\Models\Gift');
    }

    public function hasProduct($product) {
        return $this->products->contains($product);
    }

    public function upgrades(){
        return $this->belongsToMany('App\Models\Upgrade', 'invoice_upgrade')->withPivot('upgrade_id', 'price');
    }

}