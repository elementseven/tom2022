<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'customer_id', 
        'full_name', 
        'first_name', 
        'last_name', 
        'phone', 
        'email', 
        'follow_up', 
        'email_verified_at',
        'password', 
        'country'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $attributes = [
        'role_id' => 2,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($user) {
            $user->full_name = $user->first_name . ' ' . $user->last_name;
        });
    }

    public function role(){
        return $this->belongsTo('App\Models\Role');
    }

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function info(){
        return $this->hasOne('App\Models\Info');
    }

    public function invoices(){
        return $this->hasMany('App\Models\Invoice');
    }

    public function orders(){
        return $this->hasMany('App\Models\Order');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'product_user')->withPivot('product_id')->withTimestamps();
    }

    public function addresses(){
        return $this->hasMany('App\Models\Address');
    }
    
    public function gifts(){
        return $this->hasMany('App\Models\Gift');
    }

}
