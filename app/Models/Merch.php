<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Merch extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'printful',
        'order',
        'status',
        'photo',
        'stripe_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($merch) {
            $merch->slug = Str::slug($merch->name, "-");
        });
    }

    public function variants(){
        return $this->hasMany('App\Models\Variant');
    }

}
