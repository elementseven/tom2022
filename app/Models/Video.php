<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Video extends Model implements HasMedia
{
    use HasFactory;
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name', 
        'description', 
        'youtube', 
        'product_id',
        'runtime',
        'thumbnail',
        'show_time',
        'category'
    ];

    protected $casts = [
        'show_time' => 'datetime',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->width(300);
        $this->addMediaConversion('normal')->keepOriginalImageFormat();
        $this->addMediaConversion('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('videos')->singleFile();
    }

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'user_video')->withPivot('user_id');
    }

}
