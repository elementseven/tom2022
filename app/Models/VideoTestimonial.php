<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class VideoTestimonial extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name', 
        'vimeo_id', 
        'status', 
        'order', 
        'image'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(200, 200);
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->crop(100, 180);
        $this->addMediaConversion('normal-webp')->crop(100, 180)->format('webp');
    }
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('video-testimonials')->singleFile();
    }

}
