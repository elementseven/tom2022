<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Upgrade extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'textone',
        'texttwo',
        'price',
        'sale_price',
        'status',
        'product_id',
        'bundle_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($resource) {
            $resource->slug = Str::slug($resource->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->width(700);
        $this->addMediaConversion('normal-webp')->width(700)->format('webp');
        $this->addMediaConversion('thumbnail')->crop(400, 400);
        $this->addMediaConversion('featured')->crop(600, 600);
        $this->addMediaConversion('featured-webp')->crop(600, 600)->format('webp');
    }

    public function registerMediaCollections(): void
    {
      $this->addMediaCollection('upgrades')->singleFile();
    }

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function bundle(){
        return $this->belongsTo('App\Models\Bundle');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->using(ProductUpgrade::class)->withPivot('product_id','price');
    }

    public function invoices(){
        return $this->belongsToMany('App\Models\Invoice', 'invoice_upgrade')->withPivot('invoice_id', 'price');
    }


    public function hasProduct($product) {
        return $this->products->contains($product);
    }
}
