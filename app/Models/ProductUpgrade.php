<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Log;

class ProductUpgrade extends Pivot
{
    protected $table = 'product_upgrade';

    protected $fillable = ['upgrade_id', 'product_id', 'price'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($pivot) {
            Log::info('Pivot created', ['pivot' => $pivot]);
            self::updateUpgradeSalePrice($pivot->upgrade_id);
        });

        static::updated(function ($pivot) {
            Log::info('Pivot updated', ['pivot' => $pivot]);
            self::updateUpgradeSalePrice($pivot->upgrade_id);
        });

        static::deleted(function ($pivot) {
            Log::info('Pivot updated', ['pivot' => $pivot]);
            self::updateUpgradeSalePrice($pivot->upgrade_id);
        });
    }

    protected static function updateUpgradeSalePrice($upgradeId)
    {
        $upgrade = Upgrade::find($upgradeId);
        $totalPrice = $upgrade->products()->sum('product_upgrade.price');
        $upgrade->sale_price = $totalPrice;
        $upgrade->save();

        Log::info('Updated Upgrade Sale Price', ['totalPrice' => $totalPrice]);
    }
}
