<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;

    protected $fillable = [
        'gender', 
        'dob', 
        'job', 
        'sport', 
        'why', 
        'injuries', 
        'hear', 
        'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
