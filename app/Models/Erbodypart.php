<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Erbodypart extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $punc = array("?", "!", "'", ",", ":", ";", "/", "|", "(", ")", "&");
            $title = str_replace($punc, '', strtolower($post->title));
            $post->slug = str_replace(' ', '-', $title);
        });
    }
    public function ervideo(){
        return $this->belongsToMany('App\Models\Ervideo', 'erbodypart_ervideo')->withPivot('ervideo_id','erbodypart_id');
    }
    public function ervideos(){
        return $this->belongsToMany('App\Models\Ervideo', 'erbodypart_ervideo')->withPivot('ervideo_id','erbodypart_id');
    }


}
