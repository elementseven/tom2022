<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Podcast extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($podcast) {
            $podcast->slug = Str::slug($podcast->name, "-");
        });
    }

    protected $fillable = [
        'name',
        'slug',
        'excerpt',
        'episode',
        'date',
        'description',
        'youtube',
        'status',
        'image'
    ];

    
    
    protected $casts = [
        'created_at' => 'datetime',
        'date' => 'date',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(200, 200);
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->crop(312, 160);
        $this->addMediaConversion('normal-webp')->crop(312, 160)->format('webp');
        $this->addMediaConversion('large')->keepOriginalImageFormat()->crop(1000, 580);
        $this->addMediaConversion('large-webp')->crop(1000, 580)->format('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('podcasts')->singleFile();
    }

    public function links(){
        return $this->hasMany('App\Models\Link');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'podcast_product')->withPivot('product_id');
    }
}
