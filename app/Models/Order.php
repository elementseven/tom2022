<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'printful_order',
        'status',
        'quantity',
        'variant_id',
        'invoice_id',
        'user_id'
    ];

    public function variant(){
        return $this->belongsTo('App\Models\Variant');
    }

    public function invoice(){
        return $this->belongsTo('App\Models\Invoice');
    }
    
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
