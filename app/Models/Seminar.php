<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seminar extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'category', 
        'price', 
        'start', 
        'end', 
        'location', 
        'link', 
        'line1', 
        'line2', 
        'city', 
        'postcode', 
        'status', 
        'eventbrite'
    ];

    protected $casts = [
        'start' => 'datetime',
        'end' => 'datetime',
    ];

}
