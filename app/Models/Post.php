<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Enums\CropPosition;
use Illuminate\Support\Str;

class Post extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $post->slug = Str::slug($post->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(200, 200);
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->crop(312, 160);
        $this->addMediaConversion('normal-webp')->crop(312, 160)->format('webp');
        $this->addMediaConversion('large')->keepOriginalImageFormat()->crop(1000, 580);
        $this->addMediaConversion('large-webp')->crop(1000, 580)->format('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('posts')->singleFile();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function blogCategories(){
        return $this->belongsToMany('App\Models\BlogCategory', 'blog_category_post')->withPivot('blog_category_id');
    }
    
}
