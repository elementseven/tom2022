<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'path', 
        'type',
        'show_time'
    ];
    
    protected $casts = [
        'show_time' => 'datetime',
    ];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

}
