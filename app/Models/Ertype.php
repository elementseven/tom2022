<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ertype extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $punc = array("?", "!", "'", ",", ":", ";", "/", "|", "(", ")", "&");
            $title = str_replace($punc, '', strtolower($post->title));
            $post->slug = str_replace(' ', '-', $title);
        });
    }
    public function ervideo(){
        return $this->belongsToMany('App\Models\Ervideo', 'ertype_ervideo')->withPivot('ervideo_id','ertype_id');
    }
    public function ervideos(){
        return $this->belongsToMany('App\Models\Ervideo', 'ertype_ervideo')->withPivot('ervideo_id','ertype_id');
    }


}
