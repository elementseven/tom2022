<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Ervideo extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'exercises',
        'description',
        'link',
        'ersession_id',
        'erclass_id',
        'erbodyarea_id',
        'image'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('ervideos')->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->width(300);
        $this->addMediaConversion('normal')->keepOriginalImageFormat();
        $this->addMediaConversion('webp');
    }

    public function ersession(){
        return $this->belongsTo('App\Models\Ersession');
    }
    public function erclass(){
        return $this->belongsTo('App\Models\Erclass');
    }
    public function erbodyarea(){
        return $this->belongsTo('App\Models\Erbodyarea');
    }

    public function erbodypart(){
        return $this->belongsToMany('App\Models\Erbodypart', 'erbodypart_ervideo')->withPivot('erbodypart_id','ervideo_id');
    }
    public function erbodyparts(){
        return $this->belongsToMany('App\Models\Erbodypart', 'erbodypart_ervideo')->withPivot('erbodypart_id','ervideo_id');
    }

    public function erinjury(){
        return $this->belongsToMany('App\Models\Erinjury', 'erinjury_ervideo')->withPivot('erinjury_id','ervideo_id');
    }
    public function erinjuries(){
        return $this->belongsToMany('App\Models\Erinjury', 'erinjury_ervideo')->withPivot('erinjury_id','ervideo_id');
    }

    public function ertype(){
        return $this->belongsToMany('App\Models\Ertype', 'ertype_ervideo')->withPivot('ertype_id','ervideo_id');
    }
    public function ertypes(){
        return $this->belongsToMany('App\Models\Ertype', 'ertype_ervideo')->withPivot('ertype_id','ervideo_id');
    }

    public function ervideogoal(){
        return $this->belongsToMany('App\Models\Ervideogoal', 'ervideo_ervideogoal')->withPivot('ervideogoal_id','ervideo_id');
    }
    public function ervideogoals(){
        return $this->belongsToMany('App\Models\Ervideogoal', 'ervideo_ervideogoal')->withPivot('ervideogoal_id','ervideo_id');
    }

}
