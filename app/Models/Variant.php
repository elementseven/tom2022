<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    use HasFactory;

    protected $fillable = [
        'size', 
        'price', 
        'printful', 
        'merch_id'
    ];

    public function merch(){
        return $this->belongsTo('App\Models\Merch');
    }

    public function order(){
        return $this->hasMany('App\Models\Order');
    }

    public function invoices(){
        return $this->belongsToMany('App\Models\Invoice', 'invoice_product')->withPivot('invoice_id','price','qty');
    }
    
}
