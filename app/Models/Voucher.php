<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'code', 
        'percent', 
        'expiry_date'
    ];

    protected $casts = [
        'expiry_date' => 'datetime',
    ];

    public function invoices(){
        return $this->belongsToMany('App\Models\Invoice', 'invoice_voucher')->withPivot('invoice_id');
    }
    
}
