<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Bundle extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'price',
        'sale_price',
        'stripe_price',
        'stripe_sale_price',
        'status',
        'short',
        'description'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($resource) {
            $resource->slug = Str::slug($resource->title, "-");
        });
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->width(700);
        $this->addMediaConversion('normal-webp')->width(700)->format('webp');
        $this->addMediaConversion('thumbnail')->crop(400, 400);
        $this->addMediaConversion('featured')->crop(600, 600);
        $this->addMediaConversion('featured-webp')->crop(600, 600)->format('webp');
    }

    public function registerMediaCollections(): void
    {
      $this->addMediaCollection('bundles')->singleFile();
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->using(BundleProduct::class)->withPivot('product_id','price');
    }

    public function invoices(){
        return $this->belongsToMany('App\Models\Invoice', 'bundle_invoice')->withPivot('invoice_id', 'price');
    }

    public function potentialUpgrades(){
        return $this->hasMany('App\Models\Upgrade');
    }
    
}
