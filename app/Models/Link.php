<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Link extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'link',
        'image'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(200, 200);
        $this->addMediaConversion('normal')->keepOriginalImageFormat();
        $this->addMediaConversion('normal-webp')->format('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('links')->singleFile();
    }

    public function podcast(){
        return $this->belongsTo('App\Models\Podcast');
    }

}
