<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Erclass extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'type'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $punc = array("?", "!", "'", ",", ":", ";", "/", "|", "(", ")", "&");
            $title = str_replace($punc, '', strtolower($post->title));
            $post->slug = str_replace(' ', '-', $title);
        });
    }
    public function ervideo(){
        return $this->hasMany('App\Models\Ervideo');
    }
    public function ervideos(){
        return $this->hasMany('App\Models\Ervideo');
    }


}
