<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Review extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name', 
        'avatar', 
        'rating', 
        'content',
        'short', 
        'order',
        'product_id'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->crop(130, 130);
        $this->addMediaConversion('normal-webp')->crop(130, 130)->format('webp');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('reviews')->singleFile();
    }

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

}
