<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Product extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name', 
        'short',
        'exerpt', 
        'price', 
        'sale_price', 
        'description',
        'image',
        'status',
        'featured',
        'fbgroup',
        'stripe_id',
        'stripe_price_id',
        'stripe_sale_price_id',
        'category_id',
        'type'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop(200, 200);
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->crop(379, 220);
        $this->addMediaConversion('normal-webp')->crop(379, 220)->format('webp');
        $this->addMediaConversion('large')->keepOriginalImageFormat()->crop(1000, 580);
        $this->addMediaConversion('large-webp')->crop(1000, 580)->format('webp');
    }
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('products')->singleFile();
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function intro(){
        return $this->hasOne('App\Models\Intro');
    }

    public function faqs(){
        return $this->hasMany('App\Models\Faq');
    }

    public function videos(){
        return $this->hasMany('App\Models\Video')->orderBy('created_at', 'asc');
    }

    public function downloads(){
        return $this->hasMany('App\Models\Download');
    }

    public function invoices(){
        return $this->belongsToMany('App\Models\Invoice', 'invoice_product')->withPivot('invoice_id','price','qty');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User', 'product_user')->withPivot('user_id','created_at','updated_at')->withTimestamps();
    }

    public function testimonials(){
        return $this->belongsToMany('App\Models\Testimonial', 'product_testimonial')->withPivot('testimonial_id');
    }

    public function gifts(){
        return $this->belongsToMany('App\Models\Gift', 'gift_product')->withPivot('gift_id');
    }

    public function reviews(){
        return $this->hasMany('App\Models\Review');
    }

    public function potentialUpgrades(){
        return $this->hasMany('App\Models\Upgrade');
    }

    public function upgrades(){
        return $this->belongsToMany(Upgrade::class)->using(ProductUpgrade::class)->withPivot('upgrade_id','price');
    }

    public function bundles(){
        return $this->belongsToMany(Bundle::class)->using(BundleProduct::class)->withPivot('bundle_id','price');
    }

    public function podcasts(){
        return $this->belongsToMany('App\Models\Podcast', 'podcast_product')->withPivot('podcast_id');
    }

}
