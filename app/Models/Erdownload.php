<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Erdownload extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'path',
        'type',
        'ersession_id'
    ];
    public function ersession(){
        return $this->belongsTo('App\Models\Ersession');
    }

}
