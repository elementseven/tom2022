<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ErsubSession extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'ersession_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($post) {
            $punc = array("?", "!", "'", ",", ":", ";", "/", "|", "(", ")", "&");
            $title = str_replace($punc, '', strtolower($post->title));
            $post->slug = str_replace(' ', '-', $title);
        });
    }
    public function ersession(){
        return $this->belongsTo('App\Models\Ersession');
    }
    public function ervideos(){
    return $this->belongsToMany('App\Models\ErVideo', 'ersub_session_ervideo')->withPivot('order','ersub_session_id','ervideo_id');
  }


}
