<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 
        'first_name', 
        'last_name', 
        'email', 
        'when', 
        'date', 
        'message', 
        'invoice_id', 
        'user_id'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'date' => 'date',
    ];


    public function products(){
        return $this->belongsToMany('App\Models\Product', 'gift_product')->withPivot('product_id');
    }

    public function invoice(){
        return $this->belongsTo('App\Models\Invoice');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }


}
