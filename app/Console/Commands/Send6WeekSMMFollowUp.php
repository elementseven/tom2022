<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Models\User;
use Mail;

class Send6WeekSMMFollowUp extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'Send6WeekSMMFollowUp';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Send 6 week follow up emails at scheduled intervals';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    // Get all users who purchased SMM 6 weeks ago today, and have opted into emails.
    // $users = User::where('follow_up', 1)->whereHas('invoices', function($q){

    //   $q->whereDate('created_at', Carbon::now()->subWeeks(6))->whereHas('products', function($i){
    //     $i->whereId(1);
    //   });

    // })->get();

    $users = User::where('follow_up', 1)->whereHas('products', function($p){
        $p->where('id',1)->whereDate('product_user.created_at', Carbon::now()->startOfDay()->subWeeks(6));
    })->get();

    // Loop through each user and send follow up email
    foreach($users as $user){

      $subject = "SMM 6 week check in! ";

      Mail::send('emails.smm.week6',[
        'subject' => $subject,
        'user' => $user
      ], function ($message) use ($subject, $user){

        $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
        $message->subject($subject);
        $message->replyTo('hello@tommorrison.uk');
        $message->to($user->email);

      });

    }
  }
}
