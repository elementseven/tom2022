<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GitPullCommand extends Command
{
    // The name and signature of the console command
    protected $signature = 'git:pull';

    // The console command description
    protected $description = 'Run git pull to update the repository';

    // Execute the console command
    public function handle()
    {
        // Run the git pull command
        $output = shell_exec('git pull 2>&1');

        // Display the output
        $this->info($output);
        Log::info('Git Pull Command Output:', [
            'output' => $output
        ]);

        // Optionally log any errors if needed
        if (strpos($output, 'error') !== false || strpos($output, 'fatal') !== false) {
            Log::error('Git Pull Command Error:', [
                'output' => $output
            ]);
        }
        
    }
}
