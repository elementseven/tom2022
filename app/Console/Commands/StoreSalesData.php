<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Data;
use Carbon\Carbon;

class StoreSalesData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'StoreSalesData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store sales data in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Data::whereNotNull('id')->delete();
        
        $countries = array (
            'AF' => ['code'=> 'AF', 'name'=>'Afghanistan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AX' => ['code'=> 'AX', 'name'=>'Aland Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AL' => ['code'=> 'AL', 'name'=>'Albania', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DZ' => ['code'=> 'DZ', 'name'=>'Algeria', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AS' => ['code'=> 'AS', 'name'=>'American Samoa', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AD' => ['code'=> 'AD', 'name'=>'Andorra', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AO' => ['code'=> 'AO', 'name'=>'Angola', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AI' => ['code'=> 'AI', 'name'=>'Anguilla', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AQ' => ['code'=> 'AQ', 'name'=>'Antarctica', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AG' => ['code'=> 'AG', 'name'=>'Antigua And Barbuda', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AR' => ['code'=> 'AR', 'name'=>'Argentina', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AM' => ['code'=> 'AM', 'name'=>'Armenia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AW' => ['code'=> 'AW', 'name'=>'Aruba', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AU' => ['code'=> 'AU', 'name'=>'Australia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AT' => ['code'=> 'AT', 'name'=>'Austria', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AZ' => ['code'=> 'AZ', 'name'=>'Azerbaijan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BS' => ['code'=> 'BS', 'name'=>'Bahamas', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BH' => ['code'=> 'BH', 'name'=>'Bahrain', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BD' => ['code'=> 'BD', 'name'=>'Bangladesh', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BB' => ['code'=> 'BB', 'name'=>'Barbados', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BY' => ['code'=> 'BY', 'name'=>'Belarus', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BE' => ['code'=> 'BE', 'name'=>'Belgium', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BZ' => ['code'=> 'BZ', 'name'=>'Belize', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BJ' => ['code'=> 'BJ', 'name'=>'Benin', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BM' => ['code'=> 'BM', 'name'=>'Bermuda', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BT' => ['code'=> 'BT', 'name'=>'Bhutan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BO' => ['code'=> 'BO', 'name'=>'Bolivia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BA' => ['code'=> 'BA', 'name'=>'Bosnia And Herzegovina', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BW' => ['code'=> 'BW', 'name'=>'Botswana', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BV' => ['code'=> 'BV', 'name'=>'Bouvet Island', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BR' => ['code'=> 'BR', 'name'=>'Brazil', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IO' => ['code'=> 'IO', 'name'=>'British Indian Ocean Territory', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BN' => ['code'=> 'BN', 'name'=>'Brunei Darussalam', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BG' => ['code'=> 'BG', 'name'=>'Bulgaria', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BF' => ['code'=> 'BF', 'name'=>'Burkina Faso', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BI' => ['code'=> 'BI', 'name'=>'Burundi', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KH' => ['code'=> 'KH', 'name'=>'Cambodia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CM' => ['code'=> 'CM', 'name'=>'Cameroon', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CA' => ['code'=> 'CA', 'name'=>'Canada', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CV' => ['code'=> 'CV', 'name'=>'Cape Verde', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KY' => ['code'=> 'KY', 'name'=>'Cayman Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CF' => ['code'=> 'CF', 'name'=>'Central African Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TD' => ['code'=> 'TD', 'name'=>'Chad', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CL' => ['code'=> 'CL', 'name'=>'Chile', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CN' => ['code'=> 'CN', 'name'=>'China', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CX' => ['code'=> 'CX', 'name'=>'Christmas Island', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CC' => ['code'=> 'CC', 'name'=>'Cocos (Keeling) Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CO' => ['code'=> 'CO', 'name'=>'Colombia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KM' => ['code'=> 'KM', 'name'=>'Comoros', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CG' => ['code'=> 'CG', 'name'=>'Congo', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CD' => ['code'=> 'CD', 'name'=>'Congo, Democratic Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CK' => ['code'=> 'CK', 'name'=>'Cook Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CR' => ['code'=> 'CR', 'name'=>'Costa Rica', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CI' => ['code'=> 'CI', 'name'=>'Cote D\'Ivoire', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HR' => ['code'=> 'HR', 'name'=>'Croatia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CU' => ['code'=> 'CU', 'name'=>'Cuba', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CY' => ['code'=> 'CY', 'name'=>'Cyprus', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CZ' => ['code'=> 'CZ', 'name'=>'Czech Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DK' => ['code'=> 'DK', 'name'=>'Denmark', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DJ' => ['code'=> 'DJ', 'name'=>'Djibouti', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DM' => ['code'=> 'DM', 'name'=>'Dominica', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DO' => ['code'=> 'DO', 'name'=>'Dominican Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'EC' => ['code'=> 'EC', 'name'=>'Ecuador', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'EG' => ['code'=> 'EG', 'name'=>'Egypt', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SV' => ['code'=> 'SV', 'name'=>'El Salvador', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GQ' => ['code'=> 'GQ', 'name'=>'Equatorial Guinea', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ER' => ['code'=> 'ER', 'name'=>'Eritrea', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'EE' => ['code'=> 'EE', 'name'=>'Estonia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ET' => ['code'=> 'ET', 'name'=>'Ethiopia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FK' => ['code'=> 'FK', 'name'=>'Falkland Islands (Malvinas)', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FO' => ['code'=> 'FO', 'name'=>'Faroe Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FJ' => ['code'=> 'FJ', 'name'=>'Fiji', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FI' => ['code'=> 'FI', 'name'=>'Finland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FR' => ['code'=> 'FR', 'name'=>'France', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GF' => ['code'=> 'GF', 'name'=>'French Guiana', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PF' => ['code'=> 'PF', 'name'=>'French Polynesia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TF' => ['code'=> 'TF', 'name'=>'French Southern Territories', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GA' => ['code'=> 'GA', 'name'=>'Gabon', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GM' => ['code'=> 'GM', 'name'=>'Gambia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GE' => ['code'=> 'GE', 'name'=>'Georgia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'DE' => ['code'=> 'DE', 'name'=>'Germany', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GH' => ['code'=> 'GH', 'name'=>'Ghana', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GI' => ['code'=> 'GI', 'name'=>'Gibraltar', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GR' => ['code'=> 'GR', 'name'=>'Greece', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GL' => ['code'=> 'GL', 'name'=>'Greenland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GD' => ['code'=> 'GD', 'name'=>'Grenada', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GP' => ['code'=> 'GP', 'name'=>'Guadeloupe', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GU' => ['code'=> 'GU', 'name'=>'Guam', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GT' => ['code'=> 'GT', 'name'=>'Guatemala', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GG' => ['code'=> 'GG', 'name'=>'Guernsey', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GN' => ['code'=> 'GN', 'name'=>'Guinea', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GW' => ['code'=> 'GW', 'name'=>'Guinea-Bissau', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GY' => ['code'=> 'GY', 'name'=>'Guyana', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HT' => ['code'=> 'HT', 'name'=>'Haiti', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HM' => ['code'=> 'HM', 'name'=>'Heard Island & Mcdonald Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VA' => ['code'=> 'VA', 'name'=>'Holy See (Vatican City State)', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HN' => ['code'=> 'HN', 'name'=>'Honduras', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HK' => ['code'=> 'HK', 'name'=>'Hong Kong', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'HU' => ['code'=> 'HU', 'name'=>'Hungary', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IS' => ['code'=> 'IS', 'name'=>'Iceland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IN' => ['code'=> 'IN', 'name'=>'India', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ID' => ['code'=> 'ID', 'name'=>'Indonesia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IR' => ['code'=> 'IR', 'name'=>'Iran, Islamic Republic Of', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IQ' => ['code'=> 'IQ', 'name'=>'Iraq', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IE' => ['code'=> 'IE', 'name'=>'Ireland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IM' => ['code'=> 'IM', 'name'=>'Isle Of Man', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IL' => ['code'=> 'IL', 'name'=>'Israel', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'IT' => ['code'=> 'IT', 'name'=>'Italy', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'JM' => ['code'=> 'JM', 'name'=>'Jamaica', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'JP' => ['code'=> 'JP', 'name'=>'Japan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'JE' => ['code'=> 'JE', 'name'=>'Jersey', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'JO' => ['code'=> 'JO', 'name'=>'Jordan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KZ' => ['code'=> 'KZ', 'name'=>'Kazakhstan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KE' => ['code'=> 'KE', 'name'=>'Kenya', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KI' => ['code'=> 'KI', 'name'=>'Kiribati', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KR' => ['code'=> 'KR', 'name'=>'Korea', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KW' => ['code'=> 'KW', 'name'=>'Kuwait', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KG' => ['code'=> 'KG', 'name'=>'Kyrgyzstan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LA' => ['code'=> 'LA', 'name'=>'Lao People\'s Democratic Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LV' => ['code'=> 'LV', 'name'=>'Latvia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LB' => ['code'=> 'LB', 'name'=>'Lebanon', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LS' => ['code'=> 'LS', 'name'=>'Lesotho', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LR' => ['code'=> 'LR', 'name'=>'Liberia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LY' => ['code'=> 'LY', 'name'=>'Libyan Arab Jamahiriya', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LI' => ['code'=> 'LI', 'name'=>'Liechtenstein', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LT' => ['code'=> 'LT', 'name'=>'Lithuania', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LU' => ['code'=> 'LU', 'name'=>'Luxembourg', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MO' => ['code'=> 'MO', 'name'=>'Macao', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MK' => ['code'=> 'MK', 'name'=>'Macedonia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MG' => ['code'=> 'MG', 'name'=>'Madagascar', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MW' => ['code'=> 'MW', 'name'=>'Malawi', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MY' => ['code'=> 'MY', 'name'=>'Malaysia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MV' => ['code'=> 'MV', 'name'=>'Maldives', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ML' => ['code'=> 'ML', 'name'=>'Mali', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MT' => ['code'=> 'MT', 'name'=>'Malta', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MH' => ['code'=> 'MH', 'name'=>'Marshall Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MQ' => ['code'=> 'MQ', 'name'=>'Martinique', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MR' => ['code'=> 'MR', 'name'=>'Mauritania', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MU' => ['code'=> 'MU', 'name'=>'Mauritius', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'YT' => ['code'=> 'YT', 'name'=>'Mayotte', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MX' => ['code'=> 'MX', 'name'=>'Mexico', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'FM' => ['code'=> 'FM', 'name'=>'Micronesia, Federated States Of', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MD' => ['code'=> 'MD', 'name'=>'Moldova', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MC' => ['code'=> 'MC', 'name'=>'Monaco', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MN' => ['code'=> 'MN', 'name'=>'Mongolia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ME' => ['code'=> 'ME', 'name'=>'Montenegro', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MS' => ['code'=> 'MS', 'name'=>'Montserrat', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MA' => ['code'=> 'MA', 'name'=>'Morocco', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MZ' => ['code'=> 'MZ', 'name'=>'Mozambique', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MM' => ['code'=> 'MM', 'name'=>'Myanmar', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NA' => ['code'=> 'NA', 'name'=>'Namibia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NR' => ['code'=> 'NR', 'name'=>'Nauru', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NP' => ['code'=> 'NP', 'name'=>'Nepal', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NL' => ['code'=> 'NL', 'name'=>'Netherlands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AN' => ['code'=> 'AN', 'name'=>'Netherlands Antilles', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NC' => ['code'=> 'NC', 'name'=>'New Caledonia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NZ' => ['code'=> 'NZ', 'name'=>'New Zealand', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NI' => ['code'=> 'NI', 'name'=>'Nicaragua', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NE' => ['code'=> 'NE', 'name'=>'Niger', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NG' => ['code'=> 'NG', 'name'=>'Nigeria', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NU' => ['code'=> 'NU', 'name'=>'Niue', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NF' => ['code'=> 'NF', 'name'=>'Norfolk Island', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MP' => ['code'=> 'MP', 'name'=>'Northern Mariana Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'NO' => ['code'=> 'NO', 'name'=>'Norway', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'OM' => ['code'=> 'OM', 'name'=>'Oman', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PK' => ['code'=> 'PK', 'name'=>'Pakistan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PW' => ['code'=> 'PW', 'name'=>'Palau', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PS' => ['code'=> 'PS', 'name'=>'Palestinian Territory, Occupied', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PA' => ['code'=> 'PA', 'name'=>'Panama', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PG' => ['code'=> 'PG', 'name'=>'Papua New Guinea', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PY' => ['code'=> 'PY', 'name'=>'Paraguay', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PE' => ['code'=> 'PE', 'name'=>'Peru', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PH' => ['code'=> 'PH', 'name'=>'Philippines', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PN' => ['code'=> 'PN', 'name'=>'Pitcairn', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PL' => ['code'=> 'PL', 'name'=>'Poland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PT' => ['code'=> 'PT', 'name'=>'Portugal', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PR' => ['code'=> 'PR', 'name'=>'Puerto Rico', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'QA' => ['code'=> 'QA', 'name'=>'Qatar', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'RE' => ['code'=> 'RE', 'name'=>'Reunion', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'RO' => ['code'=> 'RO', 'name'=>'Romania', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'RU' => ['code'=> 'RU', 'name'=>'Russian Federation', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'RW' => ['code'=> 'RW', 'name'=>'Rwanda', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'BL' => ['code'=> 'BL', 'name'=>'Saint Barthelemy', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SH' => ['code'=> 'SH', 'name'=>'Saint Helena', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'KN' => ['code'=> 'KN', 'name'=>'Saint Kitts And Nevis', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LC' => ['code'=> 'LC', 'name'=>'Saint Lucia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'MF' => ['code'=> 'MF', 'name'=>'Saint Martin', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'PM' => ['code'=> 'PM', 'name'=>'Saint Pierre And Miquelon', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VC' => ['code'=> 'VC', 'name'=>'Saint Vincent And Grenadines', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'WS' => ['code'=> 'WS', 'name'=>'Samoa', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SM' => ['code'=> 'SM', 'name'=>'San Marino', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ST' => ['code'=> 'ST', 'name'=>'Sao Tome And Principe', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SA' => ['code'=> 'SA', 'name'=>'Saudi Arabia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SN' => ['code'=> 'SN', 'name'=>'Senegal', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'RS' => ['code'=> 'RS', 'name'=>'Serbia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SC' => ['code'=> 'SC', 'name'=>'Seychelles', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SL' => ['code'=> 'SL', 'name'=>'Sierra Leone', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SG' => ['code'=> 'SG', 'name'=>'Singapore', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SK' => ['code'=> 'SK', 'name'=>'Slovakia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SI' => ['code'=> 'SI', 'name'=>'Slovenia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SB' => ['code'=> 'SB', 'name'=>'Solomon Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SO' => ['code'=> 'SO', 'name'=>'Somalia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ZA' => ['code'=> 'ZA', 'name'=>'South Africa', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GS' => ['code'=> 'GS', 'name'=>'South Georgia And Sandwich Isl.', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ES' => ['code'=> 'ES', 'name'=>'Spain', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'LK' => ['code'=> 'LK', 'name'=>'Sri Lanka', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SD' => ['code'=> 'SD', 'name'=>'Sudan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SR' => ['code'=> 'SR', 'name'=>'Suriname', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SJ' => ['code'=> 'SJ', 'name'=>'Svalbard And Jan Mayen', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SZ' => ['code'=> 'SZ', 'name'=>'Swaziland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SE' => ['code'=> 'SE', 'name'=>'Sweden', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'CH' => ['code'=> 'CH', 'name'=>'Switzerland', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'SY' => ['code'=> 'SY', 'name'=>'Syrian Arab Republic', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TW' => ['code'=> 'TW', 'name'=>'Taiwan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TJ' => ['code'=> 'TJ', 'name'=>'Tajikistan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TZ' => ['code'=> 'TZ', 'name'=>'Tanzania', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TH' => ['code'=> 'TH', 'name'=>'Thailand', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TL' => ['code'=> 'TL', 'name'=>'Timor-Leste', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TG' => ['code'=> 'TG', 'name'=>'Togo', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TK' => ['code'=> 'TK', 'name'=>'Tokelau', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TO' => ['code'=> 'TO', 'name'=>'Tonga', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TT' => ['code'=> 'TT', 'name'=>'Trinidad And Tobago', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TN' => ['code'=> 'TN', 'name'=>'Tunisia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TR' => ['code'=> 'TR', 'name'=>'Turkey', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TM' => ['code'=> 'TM', 'name'=>'Turkmenistan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TC' => ['code'=> 'TC', 'name'=>'Turks And Caicos Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'TV' => ['code'=> 'TV', 'name'=>'Tuvalu', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'UG' => ['code'=> 'UG', 'name'=>'Uganda', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'UA' => ['code'=> 'UA', 'name'=>'Ukraine', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'AE' => ['code'=> 'AE', 'name'=>'United Arab Emirates', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'GB' => ['code'=> 'GB', 'name'=>'United Kingdom', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'US' => ['code'=> 'US', 'name'=>'United States', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'UM' => ['code'=> 'UM', 'name'=>'United States Outlying Islands', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'UY' => ['code'=> 'UY', 'name'=>'Uruguay', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'UZ' => ['code'=> 'UZ', 'name'=>'Uzbekistan', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VU' => ['code'=> 'VU', 'name'=>'Vanuatu', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VE' => ['code'=> 'VE', 'name'=>'Venezuela', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VN' => ['code'=> 'VN', 'name'=>'Viet Nam', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VG' => ['code'=> 'VG', 'name'=>'Virgin Islands, British', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'VI' => ['code'=> 'VI', 'name'=>'Virgin Islands, U.S.', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'WF' => ['code'=> 'WF', 'name'=>'Wallis And Futuna', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'EH' => ['code'=> 'EH', 'name'=>'Western Sahara', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'YE' => ['code'=> 'YE', 'name'=>'Yemen', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ZM' => ['code'=> 'ZM', 'name'=>'Zambia', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'ZW' => ['code'=> 'ZW', 'name'=>'Zimbabwe', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00],
            'N/A' => ['code'=> 'N/A', 'name'=>'Not Applicable', 'sevendaytotal'=> 0.00, 'thirtydaytotal'=> 0.00, 'ninetydaytotal'=> 0.00]
        );

        $yearago = Carbon::now()->startOfDay()->subDays(366);
        $sevendaysago = Carbon::now()->startOfDay()->subDays(7);
        $thirtydaysago = Carbon::now()->startOfDay()->subDays(29);
        $thirtydaysagoprev = Carbon::now()->startOfDay()->subDays(59);
        $ninetydaysago = Carbon::now()->startOfDay()->subDays(89);
        $ninetydaysagoprev = Carbon::now()->startOfDay()->subDays(179);

        $startofmonth = Carbon::now()->startOfDay()->startOfMonth();
        $startofyear = Carbon::now()->startOfDay()->startOfYear();
        $yesterday = Carbon::now()->endOfDay()->subDays(1);
        $yesterdaystart = Carbon::now()->startOfDay()->subDays(1);

        // Totals
        $thirtydays = 0;
        $prevthirtydays = 0;
        $ninetydays = 0;
        $prevninetydays = 0;
        $mtd = 0;
        $ytd = 0;
        $prevday = 0;

        // Top Products
        $smm = array("name" => "Simplistic Mobility Method", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $wiww = array("name" => "Where I Went Wrong", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $ultimatecore = array("name" => "Ultimate Core", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $onlinepersonalcoaching = array("name" => "Online Personal Coaching", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $videocall = array("name" => "Video Call", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $endrange = array("name" => "End Range Training", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $splitsandhips = array("name" => "Splits & Hips", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $barbellbasics = array("name" => "Barbell Basics", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $totalbodyreset = array("name" => "Total Body Reset", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $beginnersbundle = array("name" => "Beginners Bundle", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $beginnersbundleupgrade = array("name" => "Beginner's Bundle Upgrade", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);
        $stabilitybuilder = array("name" => "Stability Builder", "count7" => 0, "count30" => 0, "count90" => 0, "total7" => 0.00, "total30" => 0.00, "total90" => 0.00);

        // Top Countries
        $grouped = null;

        // Sales Volume
        $totalsalesvolume = 0;
        $avordervalue = 0;
        $successtransactions = 0;

        $invoices = Invoice::where('paid', 1)->whereDate('created_at', '>=', $ninetydaysagoprev)->with(['products','user'])->get();

        // Calculate top countries 
        foreach($invoices as $i){

            if($i->user->country == "Northern Ireland"){
                $i->user->country = "GB";
                $i->user->save();
            }

            if($i->user->country == "UK"){
                $i->user->country = "GB";
                $i->user->save();
            }

            if (isset($countries[$i->user->country])) {

                // Calculate totals
                if($i->created_at >= $sevendaysago && $i->created_at <= $yesterday){
                    $countries[$i->user->country]['sevendaytotal'] = number_format($countries[$i->user->country]['sevendaytotal'] + $i->price, 2, '.', '');
                }

                // Calculate totals
                if($i->created_at >= $thirtydaysago && $i->created_at <= $yesterday){
                    $countries[$i->user->country]['thirtydaytotal'] = number_format($countries[$i->user->country]['thirtydaytotal'] + $i->price, 2, '.', '');
                }

                // Calculate totals
                if($i->created_at >= $ninetydaysago && $i->created_at <= $yesterday){
                    $countries[$i->user->country]['ninetydaytotal'] = number_format($countries[$i->user->country]['ninetydaytotal'] + $i->price, 2, '.', '');
                }

                // Calculate totals
                if($i->created_at >= $sevendaysago && $i->created_at <= $yesterday){
     
                    // Calculate top products
                    foreach($i->products as $p){
                        if($p->slug == 'the-simplistic-mobility-method'){
                            $smm['total7'] = number_format($smm['total7'] + $p->pivot->price, 2, '.', '');
                            $smm['count7'] = $smm['count7'] + 1;
                        }
                        else if($p->slug == 'where-i-went-wrong-e-book'){
                            $wiww['total7'] = number_format($wiww['total7'] + $p->pivot->price, 2, '.', '');
                            $wiww['count7'] = $wiww['count7'] + 1;
                        }
                        else if($p->slug == 'ultimate-core-seminar'){
                            $ultimatecore['total7'] = number_format($ultimatecore['total7'] + $p->pivot->price, 2, '.', '');
                            $ultimatecore['count7'] = $ultimatecore['count7'] + 1;
                        }
                        else if($p->slug == 'online-personal-coaching'){
                            $onlinepersonalcoaching['total7'] = number_format($onlinepersonalcoaching['total7'] + $p->pivot->price, 2, '.', '');
                            $onlinepersonalcoaching['count7'] = $onlinepersonalcoaching['count7'] + 1;
                        }
                        else if($p->slug == 'video-call'){
                            $videocall['total7'] = number_format($videocall['total7'] + $p->pivot->price, 2, '.', '');
                            $videocall['count7'] = $videocall['count7'] + 1;
                        }
                        else if($p->slug == 'end-range-training'){
                            $endrange['total7'] = number_format($endrange['total7'] + $p->pivot->price, 2, '.', '');
                            $endrange['count7'] = $endrange['count7'] + 1;
                        }
                        else if($p->slug == 'splits-and-hips'){
                            $splitsandhips['total7'] = number_format($splitsandhips['total7'] + $p->pivot->price, 2, '.', '');
                            $splitsandhips['count7'] = $splitsandhips['count7'] + 1;
                        }
                        else if($p->slug == 'barbell-basics'){
                            $barbellbasics['total7'] = number_format($barbellbasics['total7'] + $p->pivot->price, 2, '.', '');
                            $barbellbasics['count7'] = $barbellbasics['count7'] + 1;
                        }
                        else if($p->slug == 'total-body-reset'){
                            $totalbodyreset['total7'] = number_format($totalbodyreset['total7'] + $p->pivot->price, 2, '.', '');
                            $totalbodyreset['count7'] = $totalbodyreset['count7'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle'){
                            $beginnersbundle['total7'] = number_format($beginnersbundle['total7'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundle['count7'] = $beginnersbundle['count7'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle-upgrade'){
                            $beginnersbundleupgrade['total7'] = number_format($beginnersbundleupgrade['total7'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundleupgrade['count7'] = $beginnersbundleupgrade['count7'] + 1;
                        }
                        else if($p->slug == 'stability-builder'){
                            $stabilitybuilder['total7'] = number_format($stabilitybuilder['total7'] + $p->pivot->price, 2, '.', '');
                            $stabilitybuilder['count7'] = $stabilitybuilder['count7'] + 1;
                        }
                    }

                    // Calculate top countries
                    
         
                }

                // 
                if($i->created_at >= $thirtydaysago && $i->created_at <= $yesterday){
                    $thirtydays = $thirtydays + $i->price;

                    // Calculate top products
                    foreach($i->products as $p){
                        if($p->slug == 'the-simplistic-mobility-method'){
                            $smm['total30'] = number_format($smm['total30'] + $p->pivot->price, 2, '.', '');
                            $smm['count30'] = $smm['count30'] + 1;
                        }
                        else if($p->slug == 'where-i-went-wrong-e-book'){
                            $wiww['total30'] = number_format($wiww['total30'] + $p->pivot->price, 2, '.', '');
                            $wiww['count30'] = $wiww['count30'] + 1;
                        }
                        else if($p->slug == 'ultimate-core-seminar'){
                            $ultimatecore['total30'] = number_format($ultimatecore['total30'] + $p->pivot->price, 2, '.', '');
                            $ultimatecore['count30'] = $ultimatecore['count30'] + 1;
                        }
                        else if($p->slug == 'online-personal-coaching'){
                            $onlinepersonalcoaching['total30'] = number_format($onlinepersonalcoaching['total30'] + $p->pivot->price, 2, '.', '');
                            $onlinepersonalcoaching['count30'] = $onlinepersonalcoaching['count30'] + 1;
                        }
                        else if($p->slug == 'video-call'){
                            $videocall['total30'] = number_format($videocall['total30'] + $p->pivot->price, 2, '.', '');
                            $videocall['count30'] = $videocall['count30'] + 1;
                        }
                        else if($p->slug == 'end-range-training'){
                            $endrange['total30'] = number_format($endrange['total30'] + $p->pivot->price, 2, '.', '');
                            $endrange['count30'] = $endrange['count30'] + 1;
                        }
                        else if($p->slug == 'splits-and-hips'){
                            $splitsandhips['total30'] = number_format($splitsandhips['total30'] + $p->pivot->price, 2, '.', '');
                            $splitsandhips['count30'] = $splitsandhips['count30'] + 1;
                        }
                        else if($p->slug == 'barbell-basics'){
                            $barbellbasics['total30'] = number_format($barbellbasics['total30'] + $p->pivot->price, 2, '.', '');
                            $barbellbasics['count30'] = $barbellbasics['count30'] + 1;
                        }
                        else if($p->slug == 'total-body-reset'){
                            $totalbodyreset['total30'] = number_format($totalbodyreset['total30'] + $p->pivot->price, 2, '.', '');
                            $totalbodyreset['count30'] = $totalbodyreset['count30'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle'){
                            $beginnersbundle['total30'] = number_format($beginnersbundle['total30'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundle['count30'] = $beginnersbundle['count30'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle-upgrade'){
                            $beginnersbundleupgrade['total30'] = number_format($beginnersbundleupgrade['total30'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundleupgrade['count30'] = $beginnersbundleupgrade['count30'] + 1;
                        }
                        else if($p->slug == 'stability-builder'){
                            $stabilitybuilder['total30'] = number_format($stabilitybuilder['total30'] + $p->pivot->price, 2, '.', '');
                            $stabilitybuilder['count30'] = $stabilitybuilder['count30'] + 1;
                        }
                    }

                }
                if($i->created_at >= $thirtydaysagoprev && $i->created_at <= $thirtydaysago){
                    $prevthirtydays = $prevthirtydays + $i->price;
                }
                if($i->created_at >= $ninetydaysago && $i->created_at <= $yesterday){
                    $ninetydays = $ninetydays + $i->price;

                    // Calculate top products
                    foreach($i->products as $p){
                        if($p->slug == 'the-simplistic-mobility-method'){
                            $smm['total90'] = number_format($smm['total90'] + $p->pivot->price, 2, '.', '');
                            $smm['count90'] = $smm['count90'] + 1;
                        }
                        else if($p->slug == 'where-i-went-wrong-e-book'){
                            $wiww['total90'] = number_format($wiww['total90'] + $p->pivot->price, 2, '.', '');
                            $wiww['count90'] = $wiww['count90'] + 1;
                        }
                        else if($p->slug == 'ultimate-core-seminar'){
                            $ultimatecore['total90'] = number_format($ultimatecore['total90'] + $p->pivot->price, 2, '.', '');
                            $ultimatecore['count90'] = $ultimatecore['count90'] + 1;
                        }
                        else if($p->slug == 'online-personal-coaching'){
                            $onlinepersonalcoaching['total90'] = number_format($onlinepersonalcoaching['total90'] + $p->pivot->price, 2, '.', '');
                            $onlinepersonalcoaching['count90'] = $onlinepersonalcoaching['count90'] + 1;
                        }
                        else if($p->slug == 'video-call'){
                            $videocall['total90'] = number_format($videocall['total90'] + $p->pivot->price, 2, '.', '');
                            $videocall['count90'] = $videocall['count90'] + 1;
                        }
                        else if($p->slug == 'end-range-training'){
                            $endrange['total90'] = number_format($endrange['total90'] + $p->pivot->price, 2, '.', '');
                            $endrange['count90'] = $endrange['count90'] + 1;
                        }
                        else if($p->slug == 'splits-and-hips'){
                            $splitsandhips['total90'] = number_format($splitsandhips['total90'] + $p->pivot->price, 2, '.', '');
                            $splitsandhips['count90'] = $splitsandhips['count90'] + 1;
                        }
                        else if($p->slug == 'barbell-basics'){
                            $barbellbasics['total90'] = number_format($barbellbasics['total90'] + $p->pivot->price, 2, '.', '');
                            $barbellbasics['count90'] = $barbellbasics['count90'] + 1;
                        }
                        else if($p->slug == 'total-body-reset'){
                            $totalbodyreset['total90'] = number_format($totalbodyreset['total90'] + $p->pivot->price, 2, '.', '');
                            $totalbodyreset['count90'] = $totalbodyreset['count90'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle'){
                            $beginnersbundle['total90'] = number_format($beginnersbundle['total90'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundle['count90'] = $beginnersbundle['count90'] + 1;
                        }
                        else if($p->slug == 'beginners-bundle-upgrade'){
                            $beginnersbundleupgrade['total90'] = number_format($beginnersbundleupgrade['total90'] + $p->pivot->price, 2, '.', '');
                            $beginnersbundleupgrade['count90'] = $beginnersbundleupgrade['count90'] + 1;
                        }
                        else if($p->slug == 'stability-builder'){
                            $stabilitybuilder['total90'] = number_format($stabilitybuilder['total90'] + $p->pivot->price, 2, '.', '');
                            $stabilitybuilder['count90'] = $stabilitybuilder['count90'] + 1;
                        }
                    }

                }
                if($i->created_at >= $ninetydaysagoprev && $i->created_at <= $ninetydaysago){
                    $prevninetydays = $prevninetydays + $i->price;
                }
                if($i->created_at >= $startofmonth && $i->created_at <= $yesterday){
                    $mtd = $mtd + $i->price;
                }
                if($i->created_at >= $startofyear && $i->created_at <= $yesterday){
                    $ytd = $ytd + $i->price;
                }
                if($i->created_at >= $yesterdaystart && $i->created_at <= $yesterday){
                    $prevday = $prevday + $i->price;
                }

            }


        }

        

        Data::create([
            'name' =>  'Thirty Days',
             'data' => $thirtydays
        ]);

        Data::create([
            'name' =>  'Previous Thirty Days',
             'data' => $prevthirtydays
        ]);

        Data::create([
            'name' =>  'Ninety Days',
             'data' => $ninetydays
        ]);

        Data::create([
            'name' =>  'Previous Ninety Days',
             'data' => $prevninetydays
        ]);

        Data::create([
            'name' =>  'Month To Date',
             'data' => $mtd
        ]);

        Data::create([
            'name' =>  'Year To Date',
             'data' => $ytd
        ]);

        Data::create([
            'name' =>  'Previous Day',
             'data' => $prevday
        ]);

        Data::create([
            'name' =>  'the-simplistic-mobility-method',
             'data' => json_encode($smm)
        ]);

        Data::create([
            'name' =>  'where-i-went-wrong-e-book',
             'data' => json_encode($wiww)
        ]);

        Data::create([
            'name' =>  'ultimate-core-seminar',
             'data' => json_encode($ultimatecore)
        ]);

        Data::create([
            'name' =>  'online-personal-coaching',
             'data' => json_encode($onlinepersonalcoaching)
        ]);

        Data::create([
            'name' =>  'video-call',
             'data' => json_encode($videocall)
        ]);

        Data::create([
            'name' =>  'end-range-training',
             'data' => json_encode($endrange)
        ]);

        Data::create([
            'name' =>  'splits-and-hips',
             'data' => json_encode($splitsandhips)
        ]);

        Data::create([
            'name' =>  'barbell-basics',
             'data' => json_encode($barbellbasics)
        ]);

        Data::create([
            'name' =>  'total-body-reset',
             'data' => json_encode($totalbodyreset)
        ]);

        Data::create([
            'name' =>  'beginners-bundle',
             'data' => json_encode($beginnersbundle)
        ]);

        Data::create([
            'name' =>  'beginners-bundle-upgrade',
             'data' => json_encode($beginnersbundleupgrade)
        ]);

        Data::create([
            'name' =>  'stability-builder',
             'data' => json_encode($stabilitybuilder)
        ]);
        Data::create([
            'name' =>  'countries',
             'data' => json_encode($countries)
        ]);
    }
}
