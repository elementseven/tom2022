<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\Bundle;

class UpdateSalePrices extends Command
{
    protected $signature = 'update:sale-prices';
    protected $description = 'Update sale prices for products and bundles';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Define the sale prices for individual products
        $productSalePrices = [
            1 => 62.10, // SMM
            2 => 6.29, // WIWW
            3 => 25.19, // Ultimate Core    
            6 => 89.96, // End Range Training
            7 => 32.39, // Splits & Hips
            8 => 26.99, // Barbell Basics
            17 => 67.50 // Stability Builder
        ];

        // Define the sale prices for products attached to bundles
        $bundleProductPrices = [
            10 => [ // Beginners Bundle £83.98
                1 => 69.00, // SMM
                2 => 6.99, // WIWW
                3 => 7.99 // Ultimate Core   
            ],
            11 => [ // Intermediate Bundle £133
                6 => 66.50, // End Range Training 
                17 => 66.50 // Stability Builder
            ],
            12 => [ // Complete Bundle £198.55
                1 => 69.00, // SMM
                2 => 6.99, // WIWW
                3 => 20.00, // Ultimate Core   
                6 => 80.00, // End Range Training 
                17 => 22.56 // Stability Builder
            ]
        ];

        // Update the sale prices for individual products
        foreach ($productSalePrices as $productId => $price) {
            $product = Product::find($productId);
            if ($product) {
                $product->sale_price = $price;
                $product->save();
                $this->info("Updated sale price for Product ID {$product->id} to {$price}");
            } else {
                $this->warn("Product ID $productId not found");
            }
        }

        // Update the prices in the pivot table for each bundle
        foreach ($bundleProductPrices as $bundleId => $products) {
            $bundle = Bundle::find($bundleId);
            if ($bundle) {
                $totalPrice = 0;
                foreach ($products as $productId => $price) {
                    $bundle->products()->updateExistingPivot($productId, ['price' => $price]);
                    $totalPrice += $price;
                }

                // Update the bundle's sale price based on the total price of its products
                $bundle->sale_price = $totalPrice;
                $bundle->save();
                $this->info("Updated sale price for Bundle ID {$bundle->id} to {$totalPrice}");
            } else {
                $this->warn("Bundle ID $bundleId not found");
            }
        }

        $this->info('Sale prices updated successfully');
    }
}
