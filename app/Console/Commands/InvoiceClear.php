<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Invoice;
use Carbon\Carbon;

class InvoiceClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoiceClear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all invoices that have not been paid.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $invoices = Invoice::where('paid', 0)->where('created_at', '<', Carbon::now()->subMinutes(30)->toDateTimeString())->get();
        foreach($invoices as $i){
            if(count($i->vouchers)){
                foreach($i->vouchers as $v){
                    $i->vouchers()->detach($v);
                }
            }
            $i->delete();
        }
    }
}
