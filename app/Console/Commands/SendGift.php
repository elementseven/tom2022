<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Gift;
use Carbon\Carbon;
use Mail;
use Illuminate\Support\Facades\Log;

class SendGift extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendGift';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send gifts to people';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gifts = Gift::whereDate('date', Carbon::now()->format('Y-m-d'))->get();
        foreach($gifts as $gift){
            $to = $gift->email;
            Mail::send('emails.giftNotification',[
                'gift' => $gift,
                'to' => $to
            ], function ($message) use ($gift, $to)
            {
                $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject('You have received a gift');
                $message->to($to);
            });

            Log::info("Gift sent to $gift->email");

        }
        
    }
}
