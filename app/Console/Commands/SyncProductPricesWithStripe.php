<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product as LocalProduct;
use Stripe\Stripe;
use Stripe\Product as StripeProduct;
use Stripe\Price;

class SyncProductPricesWithStripe extends Command
{
    protected $signature = 'sync:stripe-prices';

    protected $description = 'Sync product prices with Stripe';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));

        // Get all local products with stripe_id
        $products = LocalProduct::whereNotNull('stripe_id')->get();

        foreach ($products as $product) {
            $this->syncProductPrices($product);
        }

        $this->info('Product prices synced with Stripe successfully.');
    }

    protected function syncProductPrices(LocalProduct $product)
    {
        try {
            // Retrieve the Stripe product
            $stripe_product = StripeProduct::retrieve($product->stripe_id);

            // Retrieve all prices associated with the Stripe product
            $prices = Price::all(['product' => $stripe_product->id]);

            // Find matching price and sale price
            $stripe_price_id = null;
            $stripe_sale_price_id = null;
            $priceInCents = (int) ($product->price * 100);
            $salePriceInCents = $product->sale_price ? (int) ($product->sale_price * 100) : null;

            foreach ($prices->data as $price) {
                if ($price->unit_amount === $priceInCents) {
                    $stripe_price_id = $price->id;
                }
                if ($salePriceInCents && $price->unit_amount === $salePriceInCents) {
                    $stripe_sale_price_id = $price->id;
                }
            }

            // Update the product with the found Stripe price IDs
            $product->stripe_price_id = $stripe_price_id;
            $product->stripe_sale_price_id = $stripe_sale_price_id;
            $product->save();

            $this->info("Product ID {$product->id} synced: stripe_price_id={$stripe_price_id}, stripe_sale_price_id={$stripe_sale_price_id}");

        } catch (\Exception $e) {
            $this->error("Error syncing product ID {$product->id}: " . $e->getMessage());
        }
    }
}
