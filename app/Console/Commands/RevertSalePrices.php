<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use App\Models\Bundle;

class RevertSalePrices extends Command
{
    protected $signature = 'revert:sale-prices';
    protected $description = 'Revert sale prices for products and bundles';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Define the normal prices for products attached to bundles
        $bundleProductPrices = [
            10 => [ // Beginners Bundle £88.40
                1 => 69.00, // SMM
                2 => 6.99,  // WIWW
                3 => 12.41  // Ultimate Core   
            ],
            11 => [ // Intermediate Bundle £140
                6 => 80.00, // End Range Training 
                17 => 60.00 // Stability Builder
            ],
            12 => [ // Complete Bundle £209
                1 => 69.00, // SMM
                2 => 6.99,  // WIWW
                3 => 20.00, // Ultimate Core   
                6 => 80.00, // End Range Training 
                17 => 33.01 // Stability Builder
            ]
        ];

        // Define the product IDs
        $productIds = [
            1, // SMM
            2, // WIWW
            3, // Ultimate Core    
            6, // End Range Training
            7, // Splits & Hips
            8, // Barbell Basics
            17 // Stability Builder
        ];

        // Revert the sale prices for products
        Product::whereIn('id', $productIds)->update(['sale_price' => null]);
        $this->info("Reverted sale prices for individual products not in bundles to null");


        // Update the prices in the pivot table for each bundle
        foreach ($bundleProductPrices as $bundleId => $products) {
            $bundle = Bundle::find($bundleId);
            if ($bundle) {
                $totalPrice = 0;
                foreach ($products as $productId => $price) {
                    $bundle->products()->updateExistingPivot($productId, ['price' => $price]);
                    $totalPrice += $price;
                }

                // Update the bundle's sale price based on the total price of its products
                $bundle->sale_price = $totalPrice;
                $bundle->save();
                $this->info("Updated sale price for Bundle ID {$bundle->id} to {$totalPrice}");
            } else {
                $this->warn("Bundle ID $bundleId not found");
            }
        }

        $this->info('Sale prices reverted successfully');
    }
}
