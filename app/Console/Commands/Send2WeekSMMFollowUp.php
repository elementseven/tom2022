<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Models\User;
use Mail;

class Send2WeekSMMFollowUp extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'Send2WeekSMMFollowUp';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Send 2 week follow up emails at scheduled intervals';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    // Get all users who purchased SMM 2 weeks ago today, and have opted into emails.
    // $users = User::where('follow_up', 1)->whereHas('invoices', function($q){

    //   $q->whereDate('created_at', Carbon::now()->subWeeks(2))->whereHas('products', function($i){
    //     $i->whereId(1);
    //   });

    // })->get();

    $users = User::where('follow_up', 1)->whereHas('products', function($p){
        $p->where('id',1)->whereDate('product_user.created_at', Carbon::now()->startOfDay()->subWeeks(2));
    })->get();

    // Loop through each user and send follow up email
    foreach($users as $user){

      $subject = "It’s been two weeks! Time to investigate!";

      Mail::send('emails.smm.week2',[
        'subject' => $subject,
        'user' => $user
      ], function ($message) use ($subject, $user){

        $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
        $message->subject($subject);
        $message->replyTo('hello@tommorrison.uk');
        $message->to($user->email);

      });

    }
  }
}
