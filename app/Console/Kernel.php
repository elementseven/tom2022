<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Laravel\Nova\Trix\PruneStaleAttachments;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\InvoiceClear',
        'App\Console\Commands\Send2WeekSMMFollowUp',
        'App\Console\Commands\Send4WeekSMMFollowUp',
        'App\Console\Commands\Send6WeekSMMFollowUp',
        'App\Console\Commands\Send8WeekSMMFollowUp',
        'App\Console\Commands\StoreSalesData',
        'App\Console\Commands\GitPullCommand',
    ];
    
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('invoiceClear')->everyThirtyMinutes();
        $schedule->command('sendGift')->dailyAt('00:05');
        
        $schedule->call(function () {
            (new PruneStaleAttachments)();
        })->daily();

        // SMM follow up emails
        // $schedule->command('Send2WeekSMMFollowUp')->dailyAt('09:00');
        // $schedule->command('Send4WeekSMMFollowUp')->dailyAt('11:00');
        // $schedule->command('Send6WeekSMMFollowUp')->dailyAt('13:00');
        // $schedule->command('Send8WeekSMMFollowUp')->dailyAt('15:00');

        $schedule->command('StoreSalesData')->dailyAt('00:01');

        $schedule->command('revert:sale-prices')->dailyAt('22:00')->when(function () {
            return now()->is('2025-02-14');
        });
        $schedule->command('git:pull')->dailyAt('22:00')->when(function () {
            return now()->is('2025-02-14');
        });

        
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
