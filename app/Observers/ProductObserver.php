<?php

namespace App\Observers;

use App\Models\Product as LocalProduct;  // Alias the local Product model
use Stripe\Stripe;
use Stripe\Product as StripeProduct;  // Alias the Stripe Product class
use Stripe\Price;

class ProductObserver
{
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function created(LocalProduct $product)
    {
        $this->syncWithStripe($product, true);
    }

    public function updated(LocalProduct $product)
    {
        $this->syncWithStripe($product, false);
    }

    protected function syncWithStripe(LocalProduct $product, $isNewProduct)
    {
        // Create or update the Stripe product
        if (empty($product->stripe_id)) {
            $stripeProduct = StripeProduct::create([
                'name' => $product->name,
                'description' => $product->excerpt,
            ]);
            $product->stripe_id = $stripeProduct->id;
            $product->save();
        } else {
            $stripeProduct = StripeProduct::retrieve($product->stripe_id);
            $stripeProduct->name = $product->name;
            $stripeProduct->description = $product->excerpt;
            $stripeProduct->save();
        }

        // Handle regular price
        $this->handlePrice($product, $stripeProduct, $product->price, $product->stripe_price_id, 'stripe_price_id', true);

        // Handle sale price
        if (!is_null($product->sale_price)) {
            $this->handlePrice($product, $stripeProduct, $product->sale_price, $product->stripe_sale_price_id, 'stripe_sale_price_id', false);
        } elseif (!is_null($product->stripe_sale_price_id)) {
            // If sale_price is null and there's an existing stripe_sale_price_id, archive the sale price
            $this->archivePrice($product->stripe_sale_price_id);
            $product->stripe_sale_price_id = null;
            $product->save();
        }
    }


    protected function handlePrice(LocalProduct $product, StripeProduct $stripeProduct, $newPrice, $currentPriceId, $priceColumn, $isDefault)
    {
        // Retrieve all prices for the product
        $prices = Price::all(['product' => $stripeProduct->id]);

        $currentPrice = null;
        foreach ($prices->data as $price) {
            if ($price->id === $currentPriceId) {
                $currentPrice = $price;
                break;
            }
        }

        $newPriceInCents = (int) ($newPrice * 100);

        // If the current price is different from the new price, create a new price
        if (!$currentPrice || $currentPrice->unit_amount !== $newPriceInCents) {
            // Create a new price
            $newPriceObject = Price::create([
                'product' => $stripeProduct->id,
                'unit_amount' => $newPriceInCents,
                'currency' => 'gbp',
            ]);

            // Set the new price as the default price if it's the main price
            if ($isDefault) {
                $stripeProduct->default_price = $newPriceObject->id;
                $stripeProduct->save();
            }

            // Archive the current price if it exists and is not the same as the new price
            if ($currentPrice && $currentPrice->unit_amount !== $newPriceInCents) {
                $this->archivePrice($currentPrice->id);
            }

            // Save the new price ID in the product model
            $product->{$priceColumn} = $newPriceObject->id;
            $product->save();
        }
    }


    protected function archivePrice($priceId)
    {
        try {
            $price = Price::retrieve($priceId);
            $price->active = false;
            $price->save();
        } catch (\Exception $e) {
            logger()->error('Error archiving price', ['error' => $e->getMessage()]);
        }
    }
}
