<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\FreeVideo;
use App\Models\VideoTestimonial;
use App\Models\BlogCategory;
use App\Models\Category;
use App\Models\Seminar;
use App\Models\Download;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Review;
use App\Models\Bundle;
use App\Models\Post;
use App\Models\Fact;
use App\Models\User;
use Mail;
use Http;
use Session;
class PageController extends Controller
{

    public function latestSMMEBook(){
        $d = Download::where('id', 34)->first();
        $path = $d->path;

        $pdf_data = Http::withHeaders(['Content-Type' => 'application/pdf'])
          ->get('https://tommorrison.uk/storage/' . $path)->body();

        $headers = [
          "Content-type" => "application/pdf",
          "Content-Disposition" => "attachment; filename=" . $d->path,
          "Access-Control-Expose-Headers" => "Content-Disposition",
          "Pragma" => "no-cache",
          "Access-Control-Allow-Origin" => "https://app.tommorrison.uk",
          "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
          "Expires" => "0"
        ];

        return response()->stream(function() use($pdf_data){
          $file = fopen('php://output', 'w');
          fwrite($file, $pdf_data);
          fclose($file);
        }, 200, $headers);

        // return response()->file(storage_path('app/public/' . $path))->withHeaders([
        //     'Content-Type' => 'application/pdf',
        //     'Access-Control-Allow-Origin' => '*'
        // ]);;
    }

    // public function addTimestampsToProductUser(){

    //     $users = User::get();
    //     foreach($users as $u){
    //         if($u->invoices != null && count($u->invoices) > 0){
    //             foreach($u->invoices as $i){
    //                 foreach($i->products as $p){
    //                     if($p->slug == 'beginners-bundle-upgrade'){
    //                         $u->products()->updateExistingPivot(2, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(3, array('created_at' => $i->created_at));
    //                     }else if($p->slug == 'beginners-bundle'){
    //                         $u->products()->updateExistingPivot(1, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(2, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(3, array('created_at' => $i->created_at));
    //                     }else if($p->slug == 'intermediate-bundle'){
    //                         $u->products()->updateExistingPivot(17, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(6, array('created_at' => $i->created_at));
    //                     }else if($p->slug == 'complete-bundle'){
    //                         $u->products()->updateExistingPivot(1, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(2, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(3, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(17, array('created_at' => $i->created_at));
    //                         $u->products()->updateExistingPivot(6, array('created_at' => $i->created_at));
    //                     }else{
    //                         $u->products()->updateExistingPivot($p->id, array('created_at' => $i->created_at));
    //                     }
    //                 }
    //             }
    //         }else{
    //             foreach($u->products as $p){
    //                 if($p->slug == 'beginners-bundle-upgrade'){
    //                     $u->products()->updateExistingPivot(2, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(3, array('created_at' => $u->created_at));
    //                 }else if($p->slug == 'beginners-bundle'){
    //                     $u->products()->updateExistingPivot(1, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(2, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(3, array('created_at' => $u->created_at));
    //                 }else if($p->slug == 'intermediate-bundle'){
    //                     $u->products()->updateExistingPivot(17, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(6, array('created_at' => $u->created_at));
    //                 }else if($p->slug == 'complete-bundle'){
    //                     $u->products()->updateExistingPivot(1, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(2, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(3, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(17, array('created_at' => $u->created_at));
    //                     $u->products()->updateExistingPivot(6, array('created_at' => $u->created_at));
    //                 }else{
    //                     $u->products()->updateExistingPivot($p->id, array('created_at' => $u->created_at));
    //                 }
    //             }
    //         }
    //         foreach($u->products as $p){
    //             if($u->products()->where('id', $p->id)->first()->pivot->created_at == null){
    //                 $u->products()->updateExistingPivot($p->id, array('created_at' => $u->created_at));
    //             }
    //         }     
    //     }
    //     return 'done';
    // }

    public function programsBeginnersBundle(){
        $product = Product::where('id', 15)->first();
        $bundle = Bundle::where('id', 10)->first();
        $reviews = Review::where('product_id', [1,2,3])->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.bundles.beginners-bundle')->with(['product' => $product, 'reviews' => $reviews, 'bundle' => $bundle]);
    }

    public function programsIntermediateBundle(){
        $product = Product::where('slug', 'intermediate-bundle')->first();
        $bundle = Bundle::where('id', 11)->first();
        $reviews = Review::where('product_id', [17,6])->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.bundles.intermediate-bundle')->with(['product' => $product, 'reviews' => $reviews, 'bundle' => $bundle]);
    }

    public function programsCompleteBundle(){
        $product = Product::where('slug', 'complete-bundle')->first();
        $bundle = Bundle::where('id', 12)->first();
        $reviews = Review::where('product_id', [1,2,3,17,6])->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.bundles.complete-bundle')->with(['product' => $product, 'reviews' => $reviews, 'bundle' => $bundle]);
    }

    public function programsSmm(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->whereNotNull('avatar')->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
    
        return view('shop.programs.smm')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsStabilityBuilder(){
        $product = Product::where('id', 17)->first();

        $reviews = Review::where('product_id', 17)->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.programs.stability-builder')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsUltimateCore(){
        $product = Product::where('id', 3)->first();
        $reviews = Review::where('product_id', 3)->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.programs.ultimate-core')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsEndRangeTraining(){
        $product = Product::where('id', 6)->first();
        $reviews = Review::where('product_id', 6)->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.programs.end-range-training')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsWIWW(){
        $product = Product::where('id', 2)->first();
        return view('shop.programs.wiww')->with(['product' => $product]);
    }

    public function programsSplitsAndHips(){
        $product = Product::where('id', 7)->first();
        $reviews = Review::where('product_id', 7)->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('shop.programs.splits-and-hips')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsBarbellBasics(){
        $product = Product::where('id', 8)->first();
        $reviews = Review::where('product_id', 8)->orderBy('created_at', 'desc')->paginate(6);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
    
        return view('shop.programs.barbell-basics')->with(['product' => $product, 'reviews' => $reviews]);
    }

    public function programsVideoCall(){
        $product = Product::where('id', 5)->first();
        return view('shop.programs.video-call')->with(['product' => $product]);
    }

    public function programsOnlinePersonalCoaching(){
        $product = Product::where('id', 4)->first();
        return view('shop.programs.online-personal-coaching')->with(['product' => $product]);
    }

    public function productMedia(){
        $products = Product::where('image', '!=', null)->get();
        foreach($products as $product){
            $media = $product->getMedia();
            foreach($media as $m){
                $m->delete();
            }
            $product->addMediaFromUrl('https://tommorrison.uk/storage/'. $product->image)->preservingOriginal()->toMediaCollection('products');
            $product->image = null;
            $product->save();
        }
        return 'done';
    }
    public function postMedia(){
        $posts = Post::whereIn('id',[110,111,112,113,114,115,116,117,118,119,120])->get();
        foreach($posts as $post){
            $post->addMediaFromUrl('https://tommorrison.uk/storage/'. $post->photo)->preservingOriginal()->toMediaCollection('posts');
            // $product->image = null;
            // $product->save();
        }
        return 'done';
    }

    public function reviewsMedia(){
        $reviews = Review::where('product_id', 1)->get();
        foreach($reviews as $review){
            $review->addMediaFromUrl('https://tommorrison.uk/storage/'. $review->avatar)->preservingOriginal()->toMediaCollection('reviews');
        }
        return 'done';
    }
    
    public function getReviews($limit){
        $reviews = Review::where('product_id', 1)->where('featured', true)->orderBy('order', 'asc')->paginate($limit);
        foreach($reviews as $r){
          $r->normal = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return $reviews;
    }

    public function about(){
        return view('/pages/about');
    }
    
    public function help(){
        return view('/pages/help');
    }

    public function daysOfAwesome(){
        return view('/landingpages/7-days-of-awesome');
    }

    public function smm2022(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->orderBy('created_at', 'desc')->paginate(9);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/smm2022')->with(['reviews' => $reviews, 'product' => $product]);
    }

    public function smm2022tiktok(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->orderBy('created_at', 'desc')->paginate(9);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/smm2022-tiktok')->with(['reviews' => $reviews, 'product' => $product]);
    }

    public function stabilityBuilder(){
        $product = Product::where('slug', 'stability-builder')->first();
        return view('/landingpages/stability-builder')->with(['product' => $product]);
    }
    
    public function flongAndSexible(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->whereIn('name', ['John Witt','Meg Austin','Sandra Milburn','Jayne Addie','David Burke','Deb McStay'])->orderBy('order', 'asc')->paginate(8);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/flongAndSexible')->with(['reviews' => $reviews, 'product' => $product]);
    }
    
    public function beginnerMobility(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->whereIn('id', [81,58,57,56,33,37])->paginate(8);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/beginnerMobility')->with(['reviews' => $reviews, 'product' => $product]);;
    }

    public function mobilityTraining(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->whereIn('name', ['Matt Rodock','Tom Pashby','Shona Lee','Deb Ludington','Jane Lowe','Conor McKenna','Jenny Coppock'])->orderBy('order', 'asc')->paginate(8);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/mobilityTraining')->with(['reviews' => $reviews, 'product' => $product]);;
    }
    
    public function shareYourJourney(){
        return view('/landingpages/shareYourJourney');
    }
    
    public function smmusa(){
        $product = Product::where('id', 1)->first();
        return view('/landingpages/smm-usa')->with(['product' => $product]);
    }
    
    public function smmaus(){
        $product = Product::where('id', 1)->first();
        return view('/landingpages/smm-aus')->with(['product' => $product]);
    }
    
    public function deliveryReturns(){
        return view('pages.deliveryReturns');
    }
    
    public function videoTestimonials($amount){
        $videos = VideoTestimonial::where('status','!=', 'draft')->orderBy('order', 'asc')->paginate($amount);
        foreach($videos as $v){
          $v->normal = $v->getFirstMediaUrl('video-testimonials', 'normal');
          $v->normalwebp = $v->getFirstMediaUrl('video-testimonials', 'normal-webp');
          $v->mimetype = $v->getFirstMedia('video-testimonials')->mime_type;
        }
        return $videos;
    }

    public function reviews(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->where('featured', null)->orWhere('featured', false)->orderBy('id', 'desc')->paginate(50);
        // $featured = Review::where('product_id', 1)->where('featured', true)->orderBy('order', 'desc')->get();
        $limit = 0;
        // foreach($featured as $key => $f){
        //     $reviews->prepend($f);
        // }
        foreach($reviews as $r){
          $r->normal = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('landingpages.reviews')->with(['reviews' => $reviews, 'product' => $product]);
    }

    public function stretchingRoutine(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.stretching-routine', compact('product'));
    }

    public function stretchingProgram(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.stretching-program', compact('product'));
    }

    public function lowerBackReach(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.lower-back-reach', compact('product'));
    }

    public function stretchingExcercies(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.stretching', compact('product'));
    }
    public function smmYtLp(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.smm-youtube', compact('product'));
    }
    public function smmPhil(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.smm-phil', compact('product'));
    }
    public function mobilityProgram(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.mobility-program', compact('product'));
    }

    public function mobilityProgramP(){
        $product = Product::where('id', 1)->first();
        return view('landingpages.mobility-program-alt', compact('product'));
    }

    public function mobilityForCrossfit(){
        $product = Product::where('id', 1)->first();
        $reviews = Review::where('product_id', 1)->whereIn('name', ['Matt Rodock','Tom Pashby','Shona Lee','Deb Ludington','Jane Lowe','Scott Hutchings','Jenny Coppock'])->orderBy('order', 'asc')->paginate(8);
        foreach($reviews as $r){
          $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
          $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
          $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
        }
        return view('/landingpages/mobility-for-crossfit')->with(['reviews' => $reviews, 'product' => $product]);;
    }

    public function welcome(){
        $blogs = Post::orderBy('created_at','desc')->with('category')->paginate(3);
        return view('newHomepage', compact('blogs'));
    }

    public function tandcs(){
        return view('pages.tandcs');
    }

    public function privacyPolicy(){
        return view('pages.privacy-policy');
    }

    public function freeVideos(){
        $count = FreeVideo::count();
        return view('pages.freevideos', compact('count'));
    }
    public function blog(){
        $count = Post::count();
        $categories = BlogCategory::whereHas('posts')->get();
        return view('blog.index', compact('count', 'categories'));
    }
    public function showBlog($slug){


        $post = Post::where('slug',$slug)->first();
/*
        $next = Post::where([['status','Published'],['id', '>', $post->id]])->orderBy('id')->first();
        $previous = Post::where([['status','Published'],['id', '<', $post->id]])->orderBy('id','desc')->first();
*/
        
        if($post){
            return view('blog.show', compact('post'));
        }else{
            abort(404);
        }

        

    }
    public function seminars(){
        $seminars = Seminar::get();
        return view('seminars.index', compact('seminars'));
    }
    public function getFreeVideos($offset, $limit){
        $videos = FreeVideo::orderBy('created_at','desc')->skip($offset)->take($limit)->get();
        return $videos;
    }
    public function clearSavedSearch(Request $request){
        Session::forget('saved-search');
        return 'success';
    }
    public function getPosts(Request $request){
        Session::forget('saved-search');
        $category = $request->category;
        $search = $request->search;
        $limit = $request->limit;

        $query = Post::where('status', 'Published');

        if($search) {
            $query->where(function($q) use ($search) {
                $q->where('title', 'LIKE', '%'.$search.'%')
                  ->orWhere('exerpt', 'LIKE', '%'.$search.'%');
            });
        }

        if($category != 'all'){
            $query->whereHas('blogCategories', function($q) use($category){
                $q->where('slug', $category);
            });
        }

        $posts = $query->orderBy('created_at','desc')
                      ->with('blogCategories')
                      ->paginate($limit);

        foreach($posts as $p){
            $p->image = $p->getFirstMediaUrl('posts', 'large');
            $p->webp = $p->getFirstMediaUrl('posts', 'large-webp');
            $p->mimetype = $p->getFirstMedia('posts')->mime_type;
        }

        Session::put('saved-search', $request->search);
        Session::save();
        return $posts;
    }
    public function facts(){
        return view('pages.facts');
    }
    public function getFact(){
        $fact = Fact::all()->random(1)->first();
        return $fact;
    }
    public function contact(){
        return view('pages.contact');
    }
    public function contactNew(){
        return view('pages.contact-new');
    }
    public function questionnaire(){
        return view('pages.questionnaire');
    }
    public function videoQuestionnaire(){
        return view('pages.videoQuestionnaire');
    }
}
