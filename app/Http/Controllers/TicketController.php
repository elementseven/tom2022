<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Seminar;
use App\Models\Ticket;
use Cart;
use Auth;

class TicketController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seminar = Seminar::find($request->input('seminar'));

        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
            $request->session()->forget('invoice_id');
            // Create invoice for user
            $invoice = new Invoice();
            $invoice->paid = false;
            $invoice->price = 0;
            $invoice->save();
            $request->session()->put('invoice_id', $invoice->id);
        }
        
        $ticket = new Ticket();
        $ticket->seminar_id = $seminar->id;
        $ticket->user_id = Auth::id();
        $ticket->save();

        // Attach group to invoice and add to basket
        $invoice->tickets()->attach($ticket->id);

        Cart::add( $ticket->id, $seminar->category . " Ticket", 1, $seminar->price);

        $this->updateTotal($invoice);
        
        $items = Cart::content();
        $tax_rate = 0;
        config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'

        foreach ($items as $item){
            $item->setTaxRate($tax_rate);
            Cart::update($item->rowId, $item->qty);
        }
        return 'success';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }

    public function updateTotal(Invoice $invoice){
        $ticketsTotal = 0;
        $productsTotal = 0;
        foreach($invoice->tickets as $t){
            $ticketsTotal = $ticketsTotal + $t->seminar->price;
        }
        foreach($invoice->products as $p){
            $productsTotal = $productsTotal + $p->price;
        }
        $invoice->price = $ticketsTotal + $productsTotal;
        $invoice->save();
    }
}