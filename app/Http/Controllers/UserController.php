<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\User;
use App\Models\Info;
use Spatie\Newsletter\Facades\Newsletter;

use Auth;
use Hash;

class UserController extends Controller
{

    public function addAddress(Request $request){

        // Validate the form data
        $this->validate($request,[
            'agree' => 'accepted',
            'streetAddress' => 'required|string|max:255|min:3',
            'city' => 'required|string|max:255|min:3',
            'postalCode' => 'required|string|max:255|min:3',
            'country' => 'required|string|max:255|min:1',
            'shipping-streetAddress' => 'required|string|max:255|min:3',
            'shipping-city' => 'required|string|max:255|min:3',
            'shipping-postalCode' => 'required|string|max:255|min:3',
            'shipping-country' => 'required|string|max:255|min:1'
        ]);

        $user = Auth::user();

        // Create user addresses
        $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'billing');
        $shippingAddress = $this->createAddress($request->input('shipping-streetAddress'), $request->input('shipping-extendedAddress'),$request->input('shipping-city'), $request->input('shipping-postalCode'), $request->input('shipping-country'), $request->input('shipping-region'), $request->input('shipping-company'), 'shipping');

        if(null !== $request->input('mailinglist')){
            Newsletter::subscribe($user->email);
        }

        $request->session()->put('address-created', $user->first_name);

        return redirect()->to('/dashboard/address');

    }

    public function editAddress(Request $request){

        // Validate the form data
        $this->validate($request,[
            'agree' => 'accepted',
            'streetAddress' => 'required|string|max:255|min:3',
            'city' => 'required|string|max:255|min:3',
            'postalCode' => 'required|string|max:255|min:3',
            'country' => 'required|string|max:255|min:1',
            'shipping-streetAddress' => 'required|string|max:255|min:3',
            'shipping-city' => 'required|string|max:255|min:3',
            'shipping-postalCode' => 'required|string|max:255|min:3',
            'shipping-country' => 'required|string|max:255|min:1'
        ]);

        $user = Auth::user();

        $billingAddress = Address::where([['user_id', Auth::id()],['type', 'billing']])->first();

        $billingAddress->update([
            'streetAddress' => $request->input('streetAddress'),
            'extendedAddress' => $request->input('extendedAddress'),
            'city' => $request->input('city'),
            'postalCode' => $request->input('postalCode'),
            'country' => $request->input('country'),
            'region' => $request->input('region'),
            'company' => $request->input('company'),
        ]);

        $shippingAddress = Address::where([['user_id', Auth::id()],['type', 'shipping']])->first();

        $shippingAddress->update([
            'streetAddress' => $request->input('shipping-streetAddress'),
            'extendedAddress' => $request->input('shipping-extendedAddress'),
            'city' => $request->input('shipping-city'),
            'postalCode' => $request->input('shipping-postalCode'),
            'country' => $request->input('shipping-country'),
            'region' => $request->input('shipping-region'),
            'company' => $request->input('shipping-company'),
        ]);

        $request->session()->put('address-updated', $user->first_name);

        if(null !== $request->input('mailinglist')){
            Newsletter::subscribe($user->email);
        }
        
        return redirect()->to('/dashboard/address');

    }

    public function address(){
        return view('users.account.address');
    }

    public function getUsers(){
        $array = array();
        $users = User::whereHas('products')->withCount('products')->get();
        $userswithtwoproducts = $users->where('products_count', 2)->all();
        foreach($userswithtwoproducts as $u){
            array_push($array, [$u->full_name, $u->email, $u->products_count]);
        }
        return $array;
    }

    /**
     * Toggle SMM follow up emails.
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleFollowUp(){
        $user = Auth::user();

        if($user->follow_up != 1){
            $user->follow_up = 1;
            $user->save();
        }else{
            $user->follow_up = null;
            $user->save();
        }
        return redirect()->to('/dashboard/account-management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addInfo(Request $request)
    {
        $user = Auth::user();
        if($request->input('whyother') != ''){
            $why = $request->input('whyother');
        }else{
            if(!$request->input('why')){
                $why = NULL;
            }else{
                $why = json_encode($request->input('why'));
            }
        }
        if($request->input('jobother') != ''){
            $job = $request->input('jobother');
        }else{
            if(!$request->input('job')){
                $job = NULL;
            }else{
                $job = $request->input('job');
            }
        }
        if($request->input('sportother') != ''){
            $sport = $request->input('sportother');
        }else{
            if(!$request->input('sport')){
                $sport = NULL;
            }else{
                $sport = $request->input('sport');
            }
        }
        if($request->input('hearother') != ''){
            $hear = $request->input('hearother');
        }else{
            if(!$request->input('hear')){
                $hear = NULL;
            }else{
                $hear = $request->input('hear');
            }
        }
        if($user->info){
            $updated = array(   
                'gender' => $request->input('gender'),
                'dob'=> $request->input('dob'),
                'job'=> $job,
                'sport'=> $sport,
                'why'=> $why,
                'hear'=> $hear,
                'injuries'=> $request->input('injuries'),
            );
            $user->info->update($updated);
        }else{
            $info = Info::create([
                'gender' => $request->input('gender'),
                'dob'=> $request->input('dob'),
                'job'=> $job,
                'sport'=> $sport,
                'why'=> $why,
                'hear'=> $hear,
                'injuries'=> $request->input('injuries'),
                'user_id' => Auth::id()
            ]);
        }
        
        session(['action_status'=>'Success']);
        session(['action_message'=>'Info Added successfully.']);
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('users.account.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        if($request->input('email') != $user->email){
            $this->validate($request,[
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'country' => 'required|string',
                'email' => 'required|string|email|max:255|unique:users'
            ]);
            $updated = array(   
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'country' => $request->input('country'),
                'email' => $request->input('email')
            );
            $user->update($updated);
        }else{
            $this->validate($request,[
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'country' => 'required|string'
            ]);
            $updated = array(   
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'country' => $request->input('country')
            );
            $user->update($updated);
        }
        session(['action_status'=>'Success']);
        session(['action_message'=>'Details updated successfully.']);
        return back();
    }

    public function changePassword(Request $request){
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $updated = array(
                'password' => bcrypt($request->input('password'))
            );
            $user->update($updated);
            session(['action_status'=>'Success']);
            session(['action_message'=>'Password changed successfully.']);
            return back();
        }
        session(['action_status'=>'Error']);
        session(['action_message'=>'The password you entered was incorrect.']);
        return back();
    }

    // Function to check password
    private function check($password, $check){
        if(Hash::check($check, $password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        $this->validate($request,[
            'current_password' => 'required|string'
        ]);
        $pwcheck = $this->check($user->password, $request->input('current_password'));
        if($pwcheck == true){
            $user->forceDelete();
        }else{
            session(['action_status'=>'Error']);
            session(['action_message'=>'The password you entered was incorrect.']);
        }
        return back();
    }

    // Function to create an address
    public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $company, $type){

        if(Auth::check()){
            $currentAddresses = Auth::user()->addresses;
            foreach ($currentAddresses as $key => $a) {
                if($type == 'billing' && $a->type == 'billing'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else if($type == 'shipping' && $a->type == 'shipping'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else{
                    return 'same';
                }
            }
            if(count($currentAddresses) == 0){
                $address = Address::create([
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'city' => $city,
                    'postalCode' => $postalCode,
                    'country' => $country,
                    'region' => $region,
                    'company' => $company,
                    'type' => $type,
                    'user_id' => Auth::id()
                ]);
                return $address;
            }
        }else{
            $address = Address::create([
                'streetAddress' => $streetAddress,
                'extendedAddress' => $extendedAddress,
                'city' => $city,
                'postalCode' => $postalCode,
                'country' => $country,
                'region' => $region,
                'company' => $company,
                'type' => $type,
                'user_id' => Auth::id()
            ]);
            return $address;
        }
        
    }

}
