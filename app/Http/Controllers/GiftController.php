<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Category;
use App\Models\Voucher;
use App\Models\Address;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Setting;
use App\Models\Bundle;
use App\Models\User;
use App\Models\Gift;

use Srmklive\PayPal\Services\PayPal as PayPalClient;

use App\Helpers\CartService;

use Cart;
use Stripe;
use Mail;
use Auth;

class GiftController extends Controller
{
    protected $provider;
    

    private function getApiKey(){
        return 'i0s1ic7h-nlpm-s328:utno-ztn4zk5ibkzt';
    }

    /**
     * process transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function processGiftTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $paypalToken = $provider->getAccessToken();

        $items = Array();

        foreach(Cart::content() as $cc){
            if($cc->options->type == "bundle"){
                $bundle = Bundle::where('id', $cc->id)->first();

                foreach($bundle->products as $p){
                    $item = Array(
                        "name" => $p->name,
                        "unit_amount" => Array("value" => number_format((float) $p->pivot->price, 2, '.', ''), "currency_code" => 'GBP'),
                        "quantity" => 1
                    );

                    array_push($items, $item);
                }
                
            }else{
                $item = Array(
                    "name" => $cc->name,
                    "unit_amount" => Array("value" => number_format((float) $cc->price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => $cc->qty
                );
                array_push($items, $item);
            }
        }

        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('successGiftTransaction'),
                "cancel_url" => route('cancelGiftTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "GBP",
                        "value" => Cart::total(),
                        "breakdown" => Array( "item_total" => Array( "value" => Cart::total(), "currency_code" => 'GBP') ),
                    ],
                    "items" => $items
                ]
            ]
        ]);

        if (isset($response['id']) && $response['id'] != null) {

            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }

            return redirect()
                ->route('gift-checkout')
                ->with('error', 'Something went wrong.');

        } else {
            return redirect()
                ->route('checkout')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function successGiftTransaction(Request $request)
    {

        $invoice = Invoice::where('transaction_id', $request['token'])->first();
        if ($invoice) {
            $gift = $invoice->gift;
            return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);
        }else{

            $provider = new PayPalClient;
            $provider->setApiCredentials(config('paypal'));
            $provider->setCurrency('GBP');
            $provider->getAccessToken();
            $response = $provider->capturePaymentOrder($request['token']);
            $user = Auth::user();
            if (isset($response['status']) && $response['status'] == 'COMPLETED') {

                return $this->success($request, $user, 'PayPal', $response['id']);

            } else {
                return redirect()->route('gift-basket')->with('error', $response['message'] ?? 'Something went wrong.');
            }
        }
    }

    /**
     * cancel transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelGiftTransaction(Request $request)
    {
        return redirect()
            ->route('checkout')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }

    public function getIntent(){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();
        $total = Cart::total(2,'.','') * 100;

        if($user->stripe_id == null){
            $this->createStripeCustomer($user);
        }

        $products = Array();
        foreach(Cart::content() as $item){
            if($item->options->type == 'bundle'){
                $bundle = Bundle::where('id', $item->id)->first();
                foreach($bundle->products as $p){
                    $productdata = \Stripe\InvoiceItem::create([
                        'customer' => Auth::user()->stripe_id,
                        'description' => $p->name,
                        'quantity' => 1,
                        'price_data' => [
                            'currency' => 'gbp',
                            'product' => $p->stripe_id,
                            'unit_amount_decimal' => $p->pivot->price * 100
                        ]
                    ]);
                }
            }else if($item->options->type == 'product'){
                $product = Product::where('id', $item->id)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $product->name,
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product->stripe_id,
                        'unit_amount_decimal' => $item->price * 100
                    ]
                ]);
            }else if($item->options->type == 'merch'){
                $variant = Variant::where('id', $item->id)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $variant->merch->name,
                    'quantity' => $item->qty,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $variant->merch->stripe_id,
                        'unit_amount_decimal' => $item->price * 100
                    ]
                ]);
            }
        }


        $stripeinvoice = \Stripe\Invoice::create([
            'customer' => Auth::user()->stripe_id,
            'statement_descriptor' => 'Tom Morrison'
        ]);
        $stripeinvoiceid = $stripeinvoice->id;
        $finalized = $stripeinvoice->finalizeInvoice();
        $intent = \Stripe\PaymentIntent::Retrieve($finalized->payment_intent);  
    
        
        return $intent;
    }

    public function storeGiftDetails(Request $request){
        if($request->input('when') == 'immediately'){
            // Validate the form data
            $this->validate($request,[
                'gift_first_name' => 'required|string|max:255',
                'gift_last_name' => 'required|string|max:255',
                'gift_email' => 'required|string|email|max:255|confirmed',
                'agree' => 'accepted',
            ]);
            $request->session()->put('gift_first_name', $request->input('gift_first_name'));
            $request->session()->put('gift_last_name', $request->input('gift_last_name'));
            $request->session()->put('gift_email', $request->input('gift_email'));
            $request->session()->put('gift_when', 'immediately');
            $request->session()->put('gift_date', null);
            $request->session()->put('gift_message', $request->input('message'));
        }else{
            // Validate the form data
            $this->validate($request,[
                'gift_first_name' => 'required|string|max:255',
                'gift_last_name' => 'required|string|max:255',
                'gift_email' => 'required|string|email|max:255|confirmed',
                'date' => 'required|after:today',
                'agree' => 'accepted',
            ]);
            $request->session()->put('gift_first_name', $request->input('gift_first_name'));
            $request->session()->put('gift_last_name', $request->input('gift_last_name'));
            $request->session()->put('gift_email', $request->input('gift_email'));
            $request->session()->put('gift_when', 'date');
            $request->session()->put('gift_date', $request->input('date'));
            $request->session()->put('gift_when', $request->input('message'));
        }

        return response('Validated', 200)->header('Content-Type', 'text/plain'); 

    }

    public function paymentStatus(Request $request){
    
        $user = Auth::user();

        if($request->query('payment_intent')){

            $invoice = Invoice::where('transaction_id', $request->query('payment_intent'))->first();
            if ($invoice) {
                $gift = $invoice->gift;
                return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);
            }else{
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $clientsecret = $request->query('payment_intent_client_secret');
                $intent = \Stripe\PaymentIntent::retrieve($request->query('payment_intent'));
                if($intent->status == 'succeeded'){
                    return $this->success($request, $user, 'Card', $intent->id);
                }else{
                    // Error with the stripe payment
                    session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
                    return redirect()->to('/gift-basket'); 

                }
            }
            
        }else{
            // Error with the stripe payment
            session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
            return redirect()->to('/gift-basket');
        }
    }


    /*
        Go to success page
    */
    public function success($request, $user, $paymentmethod, $transaction_id){

        // Create Variables for use in the transaction
        $shippingRate = 0;
        $total = ((Cart::total() * 1000) + ($shippingRate * 1000)) / 1000;
        $subscribed = false;
        
        
        // Create invoice 
        $request->session()->forget('invoice_id');
        $invoice = $this->createInvoice(NULL, $total, 0, $paymentmethod);
        $request->session()->put('invoice_id', $invoice->id);

        if (session()->has('voucher_id')) {
            $voucher = Voucher::where('id', session('voucher_id'))->first();
        }else{
            $voucher = null;
        }

        if (session()->has('voucher_id')) {
            $invoice->vouchers()->attach($voucher->id);
            session()->forget('voucher_id');
        }

        // If the transaction ID is set
        if($transaction_id != ""){
            
            $user = Auth::user();
            
            $invoice->paid = true;
            $invoice->transaction_id = $transaction_id;
            $invoice->user_id = $user->id;
            $invoice->save();


            // Create a new gift object
            if($request->input('when') == "immediately"){
                $the_gift = array(
                    'code' => $this->randomCode(),
                    'first_name' => session('gift_first_name'),
                    'last_name' => session('gift_last_name'),
                    'email' => session('gift_email'),
                    'when' => session('gift_when'),
                    'message' => session('gift_message'),
                    'invoice_id' => $invoice->id
                );
            }else{
                $the_gift = array(
                    'code' => $this->randomCode(),
                    'first_name' => session('gift_first_name'),
                    'last_name' => session('gift_last_name'),
                    'email' => session('gift_email'),
                    'when' => session('gift_when'),
                    'date' => Carbon::parse(session('gift_date'))->format('Y-m-d'),
                    'message' => session('gift_message'),
                    'invoice_id' => $invoice->id
                );
            }
            $gift = new Gift($the_gift);
            $gift->save();

            $request->session()->forget('gift_first_name');
            $request->session()->forget('gift_last_name');
            $request->session()->forget('gift_email');
            $request->session()->forget('gift_when');
            $request->session()->forget('gift_date');
            $request->session()->forget('gift_message');

            foreach(Cart::content() as $i){

                if($i->options->type == 'bundle'){
                    $bundle = Bundle::where('id', $i->id)->first();
                    foreach($bundle->products as $p){
                        if(!$gift->products()->where('products.id', $p->id)->exists()){
                            $gift->products()->attach($p->id);
                        }
                    }
                    $invoice->bundles()->attach($i->id, ['price' => $i->price]);

                }else if($i->options->type == 'product'){

                    if($i->id != 4 && $i->id != 5){
                        $gift->products()->attach($i->id);
                    }
                    $invoice->products()->attach($i->id, ['price' => $i->price, 'qty' => 1]);

                }
            }
            
            // Send payment confirmation email
            $this->paymentConfirmed($gift, $user);
            
            // Redirect to my account page
            Cart::destroy();
            session()->forget('intent');
            session()->forget('invoice_id');
            session(['successful_payment' => $invoice->id]);
            
            if($gift->when == "immediately"){
                $this->giftNotification($gift);
            }
            if (session()->has('voucher_id')) {
                session()->forget('voucher_id');
            }
            return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);

        }else{
            // Inform the user of the failed transaction
            session(['general_payment_error' => "There was an error processing your payment."]);
            return back();
        }

        return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);

    }


    /*
        Create the new user and attach gifted products
    */
    public function createAccount(Request $request){

        // Validate information
        $this->validate($request,[
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'country' => 'required|string',
            'consent' => 'accepted',
            'email' => 'required|string|email|max:255|unique:users|confirmed',
            'password' => 'required|string|min:6|confirmed'
        ]);

        // Create the new user
        $newuser = array(   
            'avatar' => 'default.jpg',
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'country' => $request->input('country'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        );
        $user = User::create($newuser);

        $this->accountCreated($user, $request->input('password'));

        // Get the gift and attach products associated with it to the new user
        $gift = Gift::where([['id', $request->input('id')],['code', $request->input('code')]])->first();
        foreach($gift->products as $product){
            $user->products()->attach($product->id);
        }

        $gift->user()->associate($user)->save();
        
        // Login the new user and update the gift with the user id so the system can tell it has been claimed
        Auth::login($user);

        
        $this->giftClaimed($gift);
        return redirect()->to('/dashboard');

    }
    
    /*
        Accept gift to existing account
    */
    public function acceptGift(Request $request, Gift $gift){
        $user = Auth::user();
        foreach($gift->products as $product){
            foreach($user->products as $p){
                if($p->id == $product->id && $product->id != 4 && $product->id != 5){
                    $request->session()->put('already-purchased', $product->name);
                    return redirect()->back();
                }
            }
            $user->products()->attach($product->id);
        }
        $updated = array(   
            'user_id' => $user->id,
        );
        $gift->update($updated);
        $this->giftClaimed($gift);
        return redirect()->to('/dashboard');
    }

    /*
        Claim Gift
    */
    public function claim(Gift $gift, $code){
        if($code == $gift->code){
            if($gift->user_id != null){
                return redirect()->to('/dashboard');
            }else{
                return view('shop.claim-gift',compact('gift'));
            }
        }else{
            abort(404);
        }
    }

    /*
        Go to the gift basket page
    */
    public function giftBasket(Request $request){
        
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                $request->session()->put('non-gift-still-in-basket', true);
                return redirect()->to('/basket');
            }
        }
        if (session()->has('voucher_id')) {
            $voucher = Voucher::where('id', session('voucher_id'))->first();
        }else{
            $voucher = null;
        }
         
        $bundle = Setting::where('id',1)->first();
        $now = Carbon::now();
        $showvoucher=Voucher::whereDate('expiry_date','>=',$now)->count();
        $inbasket = [];
        foreach(Cart::content() as $i){
            array_push($inbasket, $i->id);
        }
        if(count(Cart::content()) > 0){
            $products = Product::where('id','!=', [$inbasket])->paginate(5);
        }else{
            $products = Product::paginate(5);
        }
        return view('shop.gift-basket',compact('bundle','products','showvoucher','voucher'));

    }

    public function applyVoucher(Request $request){

        $this->validate($request,[
            'voucher' => 'required|string|max:225'
        ]);
    
        $voucher = Voucher::where('code',$request->input('voucher'))->first();
    
        if ($voucher) {
    
            // Check if the voucher has expired
            $now = Carbon::now();
            $expires = Carbon::createFromFormat('Y-m-d H:i:s', $voucher->expiry_date);
            if($now > $expires){
                session(['voucher_notification' => 'Sorry this voucher has expired.']);
                return back();
            }

            // Update the cart items
            $items = Cart::content();
            foreach ($items as $i) {

                if($i->options->type == 'product'){
                    $p = Product::where('id',$i->id)->first();
                    $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                    Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['type' => 'product', 'image' => $p->image,'original_price' => $p->price, 'gift' => 'yes']]);
                }else if($i->options->type == 'bundle'){
                    $b = Bundle::where('id',$i->id)->first();
                    $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                    Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['type' => 'bundle', 'image' => $b->image,'original_price' => $b->price, 'gift' => 'yes']]);
                }
            }
            session(['voucher_code' => $voucher->code]);
            session(['voucher_notification' => $voucher->percent . '% voucher applied.']);
            return back();
    
        }else{
            session(['voucher_notification' => 'Voucher not found.']);
            return back();
        }
    
    }


    // Function to create a user
    public function createUser($first_name, $last_name, $email, $country, $password, $customer_id){
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'country' => $country,
            'password' => Hash::make($password),
            'customer_id' => $customer_id,
            'role_id' => 2
        ]);

        Auth::login($user, true);
        return $user;

    }

    // Function to create an address
    public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $company, $type){

        if(Auth::check()){
            $currentAddresses = Auth::user()->addresses;
            foreach ($currentAddresses as $key => $a) {
                if($type == 'billing' && $a->type == 'billing'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else if($type == 'shipping' && $a->type == 'shipping'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else{
                    return 'same';
                }
            }
            if(count($currentAddresses) == 0){
                $address = Address::create([
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'city' => $city,
                    'postalCode' => $postalCode,
                    'country' => $country,
                    'region' => $region,
                    'company' => $company,
                    'type' => $type,
                    'user_id' => Auth::id()
                ]);
                return $address;
            }
        }else{
            $address = Address::create([
                'streetAddress' => $streetAddress,
                'extendedAddress' => $extendedAddress,
                'city' => $city,
                'postalCode' => $postalCode,
                'country' => $country,
                'region' => $region,
                'company' => $company,
                'type' => $type,
                'user_id' => Auth::id()
            ]);
            return $address;
        }
        
    }

    // Function to create a random password
    public function randomPassword() {
       $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
        return implode($pass); //turn the array into a string
    }
    // Function to create a random code
    public function randomCode() {
       $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $code = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $code[] = $alphabet[$n];
       }
        return implode($code); //turn the array into a string
    }

    /*
        Add a product to the gift basket
    */
    public function addProductToBasket(Request $request, Product $product)
    {   

        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                $request->session()->put('non-gift-still-in-basket', true);
                return redirect()->to('/basket');
            }
        }

        // Check if user already has the same product in their gift basket
        $duplicatecheck = 0;
        foreach(Cart::content() as $row){
            if($row->model->id == $product->id){
                $duplicatecheck++;
            }
        }
        if($duplicatecheck > 0){
            $request->session()->put('duplicate-product', $product->name);
            return redirect()->to('/gift-basket');
        }

        // Add to cart
        if($product->sale_price != NULL){
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->sale_price, 'weight' => 0, 'options' => ['type' => 'product', 'image'=>$product->image,'original_price' => $product->price, 'gift' => 'yes']])->associate('App\Models\Product');
        }else{
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['type' => 'product', 'image'=>$product->image, 'gift' => 'yes']])->associate('App\Models\Product');
        }

        // Go to gift basket
        return redirect()->to('/gift-basket')->with('success', $product->name . ' added to your basket');

    }

    public function addBundleToBasket(Request $request, Bundle $bundle)
    {
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                $request->session()->put('non-gift-still-in-basket', true);
                return redirect()->to('/basket');
            }
        }

        $cartService = new CartService();
        $userId = auth()->id(); 

        // Check if the bundle is already in the cart or if it is redundant
        if ($cartService->isItemInCart($bundle->id, 'bundle')) {
            return redirect()->to('/basket')->with('error', 'This bundle is already in your basket.');
        }

        $redundancyMessage = $cartService->checkPurchaseRedundancy(null, $bundle->id, 'bundle');
        if (strpos($redundancyMessage, 'can be added') === false) {
            return redirect()->to('/basket')->with('error', $redundancyMessage);
        }


        // Determine if item is on sale and adjust price accordingly
        $price = $bundle->sale_price ?? $bundle->price;

        // Add the item to the cart
        Cart::add([
            'id' => $bundle->id,
            'name' => $bundle->title,
            'qty' => 1,
            'price' => $price,
            'weight' => 0,
            'options' => ['type' => 'bundle', 'original_price' => $bundle->price, 'image'=> $bundle->getFirstMediaUrl('bundles', 'thumbnail'), 'sale_price' => $bundle->sale_price, 'gift' => 'yes']
        ])->associate('App\Models\Bundle');


        return redirect()->to('/gift-basket')->with('success', $bundle->name . ' added to your basket');
   
    }

    /*
        Go to the gift checkout page
    */
    public function checkout(){
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                return redirect()->to('/checkout');
            }
        }

        if(Auth::check()){
           if (session()->has('intent')) {
                $intent = session('intent');  
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key

                try {
                    $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the intent
                    $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                    $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the intent
                } catch (\Exception $e) {
                    return 'error Failed to cancel the payment intent: ' . $e->getMessage();
                }

            }

            $intent = $this->getIntent();
            session(['intent' => $intent]);

            return view('shop.gift-checkout',compact('intent'));
        }else{
            return view('shop.create-account');
        }

    }


    /*
        Remove Item from cart
    */
    public function remove(Request $request){
        
        $request->validate([
            'rowId' => 'required|string'
        ]);

        $rowId = $request->input('rowId');
        $hasRowId = $this->cartHasRowId($rowId);

        if ($hasRowId) {
            Cart::remove($rowId);
        }else{
            return redirect()->to('/empty-gift-basket');
        }

        if (Cart::count() == 0) {
            return redirect()->to('/empty-gift-basket');
        }

        return redirect()->to("/gift-basket");

    }


    /*
        Empty & destroy the cart
    */
    public function emptyBasket(){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key

        if (session()->has('intent')) {
            $intent = session('intent');  
            try {
                $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the invoice
                $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the invoice
                session()->forget('intent');
            } catch (\Exception $e) {
                session()->forget('intent');
            }

        }
        
        Cart::destroy();

        session()->forget('voucher_code');
        session()->forget('voucher_notification');

        return redirect()->to("/gift-basket");
    }


    /*
        Create a new invoice and add it to the user
    */
    public function createInvoice($transaction_id, $price, $paid, $paymentmethod){

        // Create a new invoice object
        $the_invoice = array(
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
            'payment_method' => $paymentmethod,
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }


    // Function to send new account message
    public function accountCreated($user, $password){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }
        Mail::send('emails.accountCreatedForGift',[
            'user' => $user,
            'to' => $to,
            'password' => $password
        ], function ($message) use ($user, $password, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Account Created');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function giftNotification($gift){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $gift->email;
        }
        Mail::send('emails.giftNotification',[
            'gift' => $gift,
            'to' => $to
        ], function ($message) use ($gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('You have received a gift');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function giftClaimed($gift){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $gift->invoice->user->email;
        }
        Mail::send('emails.giftClaimed',[
            'gift' => $gift,
            'to' => $to
        ], function ($message) use ($gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Your gift was claimed');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function paymentConfirmed($gift, $user){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }
        Mail::send('emails.giftPurchased',[
            'user' => $user,
            'to' => $to,
            'gift' => $gift
        ], function ($message) use ($user, $gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Gift order confirmed - Tom Morrison');
            $message->to($to);
            $message->attach(public_path('docs/21-12-online-gift-voucher.jpg'));
        });

        return "success";
    }

    // Function to PMP message
    public function personalisedMobilityPlanPaymentConfirmed($invoice, $user){

        Mail::send('emails.PMPPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('PMP PROGRAM');
            $message->to('hello@tommorrison.uk');
            //$message->to('luke@elementseven.co');
        });

        return "success";
    }
    
    // Function to send new account message
    public function videoCallPaymentConfirmed($invoice, $user){

        Mail::send('emails.videoCallPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('VIDEO CALL');
            $message->to('hello@tommorrison.uk');
            //$message->to('luke@elementseven.co');
        });

        return "success";
    }

    /**
     * Create a Stripe Customer
     *
     * @return \Illuminate\Http\Response
     */
    public function createStripeCustomer($user){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $billingAddress = Address::where('user_id', $user->id)->where('type','billing')->first();
        $stripecustomer = \Stripe\Customer::create([
            'address' => [
                'city' => $billingAddress->city,
                'country' => $billingAddress->country,
                'line1' => $billingAddress->streetAddress,
                'line2' => $billingAddress->extendedAddress,
                'postal_code' => $billingAddress->postalCode,
                'state' => $billingAddress->region,
            ],
            'email' => $user->email,
            'name' => $user->full_name,
        ]);
        $user->stripe_id = $stripecustomer->id;
        $user->save();
        return $user;
    }

    private function cartHasRowId($rowId)
    {
        // Get all items in the cart
        $cartItems = Cart::content();

        // Check if the specific rowId exists in the cart
        return $cartItems->has($rowId);
    }

}
