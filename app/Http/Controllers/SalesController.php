<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Data;
use Carbon\Carbon;
use DB;

class SalesController extends Controller
{
    // Return sales breakdown
    public function getSales(Request $request){
        $days = $request->input('days');
        $start = Carbon::now()->subDays($days)->startOfDay();
        $days = Invoice::orderBy('created_at', 'asc')
        ->where('paid', 1)
        ->whereDate('created_at', '>=', $start)
        ->get()->groupBy(function($item)
        {
          return $item->created_at->format('d-m-Y');
        });
        $daysarray = array();
        foreach($days as $day){
            
            $card = 0;
            $paypal = 0;
            $total = 0;
            for($i = 0; $i < sizeof($day); ++$i) {
                if(strpos($day[$i]->payment_method, 'PayPal') !== false){
                    $paypal++;
                }else{
                    $card++;
                }
                $total = $total + $day[$i]->price;
            }
            $dayarray = array();
            $dayarray['day'] = Carbon::parse($day[0]->created_at)->format('d-m-Y');
            $dayarray['cardpayments'] = $card;
            $dayarray['paypalpayments'] = $paypal;
            $dayarray['total'] = number_format($total, 2, '.', '');
            array_push($daysarray, $dayarray);
        }
        return $daysarray;
    }

    public function getPercentageChange( $newNumber, $oldNumber,int $precision = 2): float{

        if ($oldNumber == 0) {
            $oldNumber++;
            $newNumber++;
        }
        $change = (($newNumber - $oldNumber) / $oldNumber) * 100;

        return round($change, $precision);
    }

    // Return totals
    public function getTotals(){
        
        $return = array();
        $startofday = Carbon::now()->startOfDay();
        $todaystotal = 0;
        $datas = Data::get();
        $todaystotal = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday)->sum('price');
        $thirty = 0;
        $ninety = 0;
        foreach($datas as $d){
            if($d->name == "Thirty Days"){
                $thirty = $d->data;
                array_push($return, ["Thirty Days" => number_format($d->data + $todaystotal, 2, '.', ',')]);
            }
            else if($d->name == "Previous Thirty Days"){
                $change = $this->getPercentageChange($thirty, $d->data);
                array_push($return, ["Thirty Days change" => number_format($change, 2, '.', ',')]);
            }
            else if($d->name == "Ninety Days"){
                $ninety = $d->data;
                array_push($return, ["Ninety Days" => number_format($d->data + $todaystotal, 2, '.', ',')]);
            }
            else if($d->name == "Previous Ninety Days"){
                $change = $this->getPercentageChange($ninety,$d->data);
                array_push($return, ["Ninety Days change" => number_format($change, 2, '.', ',')]);
            }
            else if($d->name == "Month To Date"){
                array_push($return, ["MTD" => number_format($d->data + $todaystotal, 2, '.', ',')]);
            }
            else if($d->name == "Year To Date"){
                array_push($return, ["YTD" => number_format($d->data + $todaystotal, 2, '.', ',')]);
            }
            else if($d->name == "Previous Day"){
                $change = $this->getPercentageChange($todaystotal, $d->data);
                array_push($return, ["Previous Day change" => number_format($change, 2, '.', ',')]);
            }
        }

        array_push($return, ["Today" => number_format($todaystotal, 2, '.', ',')]);



        return $return;

    }

    // Return top countries
    public function getTopCountries($days){

        $startofday7daysago = Carbon::now()->subDays(7)->startOfDay();
        $startofday = Carbon::now()->startOfDay();
        $todaystotal = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday)->sum('price');
        $sevendaystotal = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday7daysago)->sum('price');
        $thirtydaystotal = null;
        $ninetydaystotal = null;
        $today = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday)->get();
        $datas = Data::whereIn('name', ['countries','Seven Days','Thirty Days','Ninety Days'])->get();
        $countries = array();

        foreach($datas as &$d){
            if($d->name == "Thirty Days"){
                $thirtydaystotal = number_format($d->data + $todaystotal, 2, '.', '');
            }
            else if($d->name == "Ninety Days"){
                $ninetydaystotal = number_format($d->data + $todaystotal, 2, '.', '');
            }
            else if($d->name == "countries"){
                $countries = json_decode($d->data, true);
            }
        }


        $todaystotal = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday)->sum('price');
        usort($countries, function ($a, $b) {
            return $b['thirtydaytotal'] <=> $a['thirtydaytotal'];
        });
        $output = array_slice($countries, 0, 5); 

        foreach($today as $i){
            foreach($output as &$country){
                if($i->user->country == $country['code']){
                    $country['sevendaytotal'] = number_format($country['sevendaytotal'] + $i->price, 2, '.', '');
                    $country['thirtydaytotal'] = number_format($country['thirtydaytotal'] + $i->price, 2, '.', '');
                    $country['ninetydaytotal'] = number_format($country['ninetydaytotal'] + $i->price, 2, '.', '');
                }
            }
        }
        
        foreach($output as &$country){
            $country['sevendaypercent'] = $this->cal_percentage($country['sevendaytotal'], $sevendaystotal);
            $country['thirtydaypercent'] = $this->cal_percentage($country['thirtydaytotal'], $thirtydaystotal);
            $country['ninetydaypercent'] = $this->cal_percentage($country['ninetydaytotal'], $ninetydaystotal);
        }
        
        return json_encode($output, true);

    }

    // Return totals
    public function getProductData($days){
        
        $return = array();
        $startofday = Carbon::now()->startOfDay();
        $sevendaysago = Carbon::now()->subDays(7)->startOfDay();
        $thirtydaysago = Carbon::now()->subDays(30)->startOfDay();
        $ninetydaysago = Carbon::now()->subDays(90)->startOfDay();
        $yesterday = Carbon::now()->subDays(1)->startOfDay();
        $datas = Data::whereIn('name', ['the-simplistic-mobility-method','where-i-went-wrong-e-book','ultimate-core-seminar','online-personal-coaching','video-call','end-range-training','splits-and-hips','barbell-basics','total-body-reset','beginners-bundle','beginners-bundle-upgrade','stability-builder'])->get();
        $today = Invoice::where('paid', 1)->whereDate('created_at', '>=', $startofday)->with('products')->get();

        // Initialise product arrays
        $smm = array("name" => "Simplistic Mobility Method", "percent" => '0%', "count" => 0, "total" => 0.00);
        $wiww = array("name" => "Where I Went Wrong", "percent" => '0%', "count" => 0, "total" => 0.00);
        $ultimatecore = array("name" => "Ultimate Core", "percent" => '0%', "percent30" => '0%', "percent90" => '0%',"count" => 0, "total" => 0.00);
        $onlinepersonalcoaching = array("name" => "Online Personal Coaching", "percent" => '0%', "count" => 0, "total" => 0.00);
        $videocall = array("name" => "Video Call", "percent" => '0%', "count" => 0, "total" => 0.00);
        $endrange = array("name" => "End Range Training", "percent" => '0%', "count" => 0, "total" => 0.00);
        $splitsandhips = array("name" => "Splits & Hips", "percent" => '0%', "count" => 0, "total" => 0.00);
        $barbellbasics = array("name" => "Barbell Basics", "percent" => '0%', "count" => 0, "total" => 0.00);
        $totalbodyreset = array("name" => "Total Body Reset", "percent" => '0%', "count" => 0, "total" => 0.00);
        $beginnersbundle = array("name" => "Beginners Bundle", "percent" => '0%', "count" => 0, "total" => 0.00);
        $beginnersbundleupgrade = array("name" => "Beginner's Bundle Upgrade", "percent" => '0%', "count" => 0, "total" => 0.00);
        $stabilitybuilder = array("name" => "Stability Builder", "percent" => '0%', "count" => 0, "total" => 0.00);


        // Store today's data to relevant product array
        foreach($today as $i){

            // Calculate totals
            if($i->created_at >= $sevendaysago && $i->created_at <= $yesterday && $days == 7){

                // Calculate top products
                foreach($i->products as $p){
                    if($p->slug == 'the-simplistic-mobility-method'){
                        $smm['total'] = number_format($smm['total'] + $p->pivot->price, 2, '.', '');
                        $smm['count'] = $smm['count'] + 1;
                    }
                    else if($p->slug == 'where-i-went-wrong-e-book'){
                        $wiww['total'] = number_format($wiww['total'] + $p->pivot->price, 2, '.', '');
                        $wiww['count'] = $wiww['count'] + 1;
                    }
                    else if($p->slug == 'ultimate-core-seminar'){
                        $ultimatecore['total'] = number_format($ultimatecore['total'] + $p->pivot->price, 2, '.', '');
                        $ultimatecore['count'] = $ultimatecore['count'] + 1;
                    }
                    else if($p->slug == 'online-personal-coaching'){
                        $onlinepersonalcoaching['total'] = number_format($onlinepersonalcoaching['total'] + $p->pivot->price, 2, '.', '');
                        $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + 1;
                    }
                    else if($p->slug == 'video-call'){
                        $videocall['total'] = number_format($videocall['total'] + $p->pivot->price, 2, '.', '');
                        $videocall['count'] = $videocall['count'] + 1;
                    }
                    else if($p->slug == 'end-range-training'){
                        $endrange['total'] = number_format($endrange['total'] + $p->pivot->price, 2, '.', '');
                        $endrange['count'] = $endrange['count'] + 1;
                    }
                    else if($p->slug == 'splits-and-hips'){
                        $splitsandhips['total'] = number_format($splitsandhips['total'] + $p->pivot->price, 2, '.', '');
                        $splitsandhips['count'] = $splitsandhips['count'] + 1;
                    }
                    else if($p->slug == 'barbell-basics'){
                        $barbellbasics['total'] = number_format($barbellbasics['total'] + $p->pivot->price, 2, '.', '');
                        $barbellbasics['count'] = $barbellbasics['count'] + 1;
                    }
                    else if($p->slug == 'total-body-reset'){
                        $totalbodyreset['total'] = number_format($totalbodyreset['total'] + $p->pivot->price, 2, '.', '');
                        $totalbodyreset['count'] = $totalbodyreset['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle'){
                        $beginnersbundle['total'] = number_format($beginnersbundle['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundle['count'] = $beginnersbundle['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle-upgrade'){
                        $beginnersbundleupgrade['total'] = number_format($beginnersbundleupgrade['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + 1;
                    }
                    else if($p->slug == 'stability-builder'){
                        $stabilitybuilder['total'] = number_format($stabilitybuilder['total'] + $p->pivot->price, 2, '.', '');
                        $stabilitybuilder['count'] = $stabilitybuilder['count'] + 1;
                    }
                }
            }

            // 
            if($i->created_at >= $thirtydaysago && $i->created_at <= $yesterday && $days == 30){

                // Calculate top products
                foreach($i->products as $p){
                    if($p->slug == 'the-simplistic-mobility-method'){
                        $smm['total'] = number_format($smm['total'] + $p->pivot->price, 2, '.', '');
                        $smm['count'] = $smm['count'] + 1;
                    }
                    else if($p->slug == 'where-i-went-wrong-e-book'){
                        $wiww['total'] = number_format($wiww['total'] + $p->pivot->price, 2, '.', '');
                        $wiww['count'] = $wiww['count'] + 1;
                    }
                    else if($p->slug == 'ultimate-core-seminar'){
                        $ultimatecore['total'] = number_format($ultimatecore['total'] + $p->pivot->price, 2, '.', '');
                        $ultimatecore['count'] = $ultimatecore['count'] + 1;
                    }
                    else if($p->slug == 'online-personal-coaching'){
                        $onlinepersonalcoaching['total'] = number_format($onlinepersonalcoaching['total'] + $p->pivot->price, 2, '.', '');
                        $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + 1;
                    }
                    else if($p->slug == 'video-call'){
                        $videocall['total'] = number_format($videocall['total'] + $p->pivot->price, 2, '.', '');
                        $videocall['count'] = $videocall['count'] + 1;
                    }
                    else if($p->slug == 'end-range-training'){
                        $endrange['total'] = number_format($endrange['total'] + $p->pivot->price, 2, '.', '');
                        $endrange['count'] = $endrange['count'] + 1;
                    }
                    else if($p->slug == 'splits-and-hips'){
                        $splitsandhips['total'] = number_format($splitsandhips['total'] + $p->pivot->price, 2, '.', '');
                        $splitsandhips['count'] = $splitsandhips['count'] + 1;
                    }
                    else if($p->slug == 'barbell-basics'){
                        $barbellbasics['total'] = number_format($barbellbasics['total'] + $p->pivot->price, 2, '.', '');
                        $barbellbasics['count'] = $barbellbasics['count'] + 1;
                    }
                    else if($p->slug == 'total-body-reset'){
                        $totalbodyreset['total'] = number_format($totalbodyreset['total'] + $p->pivot->price, 2, '.', '');
                        $totalbodyreset['count'] = $totalbodyreset['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle'){
                        $beginnersbundle['total'] = number_format($beginnersbundle['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundle['count'] = $beginnersbundle['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle-upgrade'){
                        $beginnersbundleupgrade['total'] = number_format($beginnersbundleupgrade['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + 1;
                    }
                    else if($p->slug == 'stability-builder'){
                        $stabilitybuilder['total'] = number_format($stabilitybuilder['total'] + $p->pivot->price, 2, '.', '');
                        $stabilitybuilder['count'] = $stabilitybuilder['count'] + 1;
                    }
                }

            }

            if($i->created_at >= $ninetydaysago && $i->created_at <= $yesterday && $days == 90){

                foreach($i->products as $p){
                    if($p->slug == 'the-simplistic-mobility-method'){
                        $smm['total'] = number_format($smm['total'] + $p->pivot->price, 2, '.', '');
                        $smm['count'] = $smm['count'] + 1;
                    }
                    else if($p->slug == 'where-i-went-wrong-e-book'){
                        $wiww['total'] = number_format($wiww['total'] + $p->pivot->price, 2, '.', '');
                        $wiww['count'] = $wiww['count'] + 1;
                    }
                    else if($p->slug == 'ultimate-core-seminar'){
                        $ultimatecore['total'] = number_format($ultimatecore['total'] + $p->pivot->price, 2, '.', '');
                        $ultimatecore['count'] = $ultimatecore['count'] + 1;
                    }
                    else if($p->slug == 'online-personal-coaching'){
                        $onlinepersonalcoaching['total'] = number_format($onlinepersonalcoaching['total'] + $p->pivot->price, 2, '.', '');
                        $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + 1;
                    }
                    else if($p->slug == 'video-call'){
                        $videocall['total'] = number_format($videocall['total'] + $p->pivot->price, 2, '.', '');
                        $videocall['count'] = $videocall['count'] + 1;
                    }
                    else if($p->slug == 'end-range-training'){
                        $endrange['total'] = number_format($endrange['total'] + $p->pivot->price, 2, '.', '');
                        $endrange['count'] = $endrange['count'] + 1;
                    }
                    else if($p->slug == 'splits-and-hips'){
                        $splitsandhips['total'] = number_format($splitsandhips['total'] + $p->pivot->price, 2, '.', '');
                        $splitsandhips['count'] = $splitsandhips['count'] + 1;
                    }
                    else if($p->slug == 'barbell-basics'){
                        $barbellbasics['total'] = number_format($barbellbasics['total'] + $p->pivot->price, 2, '.', '');
                        $barbellbasics['count'] = $barbellbasics['count'] + 1;
                    }
                    else if($p->slug == 'total-body-reset'){
                        $totalbodyreset['total'] = number_format($totalbodyreset['total'] + $p->pivot->price, 2, '.', '');
                        $totalbodyreset['count'] = $totalbodyreset['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle'){
                        $beginnersbundle['total'] = number_format($beginnersbundle['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundle['count'] = $beginnersbundle['count'] + 1;
                    }
                    else if($p->slug == 'beginners-bundle-upgrade'){
                        $beginnersbundleupgrade['total'] = number_format($beginnersbundleupgrade['total'] + $p->pivot->price, 2, '.', '');
                        $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + 1;
                    }
                    else if($p->slug == 'stability-builder'){
                        $stabilitybuilder['total'] = number_format($stabilitybuilder['total'] + $p->pivot->price, 2, '.', '');
                        $stabilitybuilder['count'] = $stabilitybuilder['count'] + 1;
                    }
                }

            }
        }

        // Add data from database to each product array
        foreach($datas as $d){

            if($days == 7){
                if($d->name == "the-simplistic-mobility-method"){
                    $smm['count'] = $smm['count'] + json_decode($d->data)->count7;
                    $smm['total'] = $smm['total'] + json_decode($d->data)->total7;
                    array_push($return, $smm);
                }
                if($d->name == "where-i-went-wrong-e-book"){
                    $wiww['count'] = $wiww['count'] + json_decode($d->data)->count7;
                    $wiww['total'] = $wiww['total'] + json_decode($d->data)->total7;
                    array_push($return, $wiww);
                }
                if($d->name == "ultimate-core-seminar"){
                    $ultimatecore['count'] = $ultimatecore['count'] + json_decode($d->data)->count7;
                    $ultimatecore['total'] = $ultimatecore['total'] + json_decode($d->data)->total7;
                    array_push($return, $ultimatecore);
                }
                if($d->name == "online-personal-coaching"){
                    $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + json_decode($d->data)->count7;
                    $onlinepersonalcoaching['total'] = $onlinepersonalcoaching['total'] + json_decode($d->data)->total7;
                    array_push($return, $onlinepersonalcoaching);
                }
                if($d->name == "video-call"){
                    $videocall['count'] = $videocall['count'] + json_decode($d->data)->count7;
                    $videocall['total'] = $videocall['total'] + json_decode($d->data)->total7;
                    array_push($return, $videocall);
                }
                if($d->name == "end-range-training"){
                    $endrange['count'] = $endrange['count'] + json_decode($d->data)->count7;
                    $endrange['total'] = $endrange['total'] + json_decode($d->data)->total7;
                    array_push($return, $endrange);
                }
                if($d->name == "splits-and-hips"){
                    $splitsandhips['count'] = $splitsandhips['count'] + json_decode($d->data)->count7;
                    $splitsandhips['total'] = $splitsandhips['total'] + json_decode($d->data)->total7;
                    array_push($return, $splitsandhips);
                }
                if($d->name == "barbell-basics"){
                    $barbellbasics['count'] = $barbellbasics['count'] + json_decode($d->data)->count7;
                    $barbellbasics['total'] = $barbellbasics['total'] + json_decode($d->data)->total7;
                    array_push($return, $barbellbasics);
                }
                if($d->name == "total-body-reset"){
                    $totalbodyreset['count'] = $totalbodyreset['count'] + json_decode($d->data)->count7;
                    $totalbodyreset['total'] = $totalbodyreset['total'] + json_decode($d->data)->total7;
                    array_push($return, $totalbodyreset);
                }
                if($d->name == "beginners-bundle"){
                    $beginnersbundle['count'] = $beginnersbundle['count'] + json_decode($d->data)->count7;
                    $beginnersbundle['total'] = $beginnersbundle['total'] + json_decode($d->data)->total7;
                    array_push($return, $beginnersbundle);
                }
                if($d->name == "beginners-bundle-upgrade"){
                    $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + json_decode($d->data)->count7;
                    $beginnersbundleupgrade['total'] = $beginnersbundleupgrade['total'] + json_decode($d->data)->total7;
                    array_push($return, $beginnersbundleupgrade);
                }
                if($d->name == "stability-builder"){
                    $stabilitybuilder['count'] = $stabilitybuilder['count'] + json_decode($d->data)->count7;
                    $stabilitybuilder['total'] = $stabilitybuilder['total'] + json_decode($d->data)->total7;
                    array_push($return, $stabilitybuilder);
                }
            }
            else if($days == 30){
                if($d->name == "the-simplistic-mobility-method"){
                    $smm['count'] = $smm['count'] + json_decode($d->data)->count30;
                    $smm['total'] = $smm['total'] + json_decode($d->data)->total30;
                    array_push($return, $smm);
                }
                if($d->name == "where-i-went-wrong-e-book"){
                    $wiww['count'] = $wiww['count'] + json_decode($d->data)->count30;
                    $wiww['total'] = $wiww['total'] + json_decode($d->data)->total30;
                    array_push($return, $wiww);
                }
                if($d->name == "ultimate-core-seminar"){
                    $ultimatecore['count'] = $ultimatecore['count'] + json_decode($d->data)->count30;
                    $ultimatecore['total'] = $ultimatecore['total'] + json_decode($d->data)->total30;
                    array_push($return, $ultimatecore);
                }
                if($d->name == "online-personal-coaching"){
                    $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + json_decode($d->data)->count30;
                    $onlinepersonalcoaching['total'] = $onlinepersonalcoaching['total'] + json_decode($d->data)->total30;
                    array_push($return, $onlinepersonalcoaching);
                }
                if($d->name == "video-call"){
                    $videocall['count'] = $videocall['count'] + json_decode($d->data)->count30;
                    $videocall['total'] = $videocall['total'] + json_decode($d->data)->total30;
                    array_push($return, $videocall);
                }
                if($d->name == "end-range-training"){
                    $endrange['count'] = $endrange['count'] + json_decode($d->data)->count30;
                    $endrange['total'] = $endrange['total'] + json_decode($d->data)->total30;
                    array_push($return, $endrange);
                }
                if($d->name == "splits-and-hips"){
                    $splitsandhips['count'] = $splitsandhips['count'] + json_decode($d->data)->count30;
                    $splitsandhips['total'] = $splitsandhips['total'] + json_decode($d->data)->total30;
                    array_push($return, $splitsandhips);
                }
                if($d->name == "barbell-basics"){
                    $barbellbasics['count'] = $barbellbasics['count'] + json_decode($d->data)->count30;
                    $barbellbasics['total'] = $barbellbasics['total'] + json_decode($d->data)->total30;
                    array_push($return, $barbellbasics);
                }
                if($d->name == "total-body-reset"){
                    $totalbodyreset['count'] = $totalbodyreset['count'] + json_decode($d->data)->count30;
                    $totalbodyreset['total'] = $totalbodyreset['total'] + json_decode($d->data)->total30;
                    array_push($return, $totalbodyreset);
                }
                if($d->name == "beginners-bundle"){
                    $beginnersbundle['count'] = $beginnersbundle['count'] + json_decode($d->data)->count30;
                    $beginnersbundle['total'] = $beginnersbundle['total'] + json_decode($d->data)->total30;
                    array_push($return, $beginnersbundle);
                }
                if($d->name == "beginners-bundle-upgrade"){
                    $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + json_decode($d->data)->count30;
                    $beginnersbundleupgrade['total'] = $beginnersbundleupgrade['total'] + json_decode($d->data)->total30;
                    array_push($return, $beginnersbundleupgrade);
                }
                if($d->name == "stability-builder"){
                    $stabilitybuilder['count'] = $stabilitybuilder['count'] + json_decode($d->data)->count30;
                    $stabilitybuilder['total'] = $stabilitybuilder['total'] + json_decode($d->data)->total30;
                    array_push($return, $stabilitybuilder);
                }
            }
            else if($days == 90){
                if($d->name == "the-simplistic-mobility-method"){
                    $smm['count'] = $smm['count'] + json_decode($d->data)->count90;
                    $smm['total'] = $smm['total'] + json_decode($d->data)->total90;
                    array_push($return, $smm);
                }
                if($d->name == "where-i-went-wrong-e-book"){
                    $wiww['count'] = $wiww['count'] + json_decode($d->data)->count90;
                    $wiww['total'] = $wiww['total'] + json_decode($d->data)->total90;
                    array_push($return, $wiww);
                }
                if($d->name == "ultimate-core-seminar"){
                    $ultimatecore['count'] = $ultimatecore['count'] + json_decode($d->data)->count90;
                    $ultimatecore['total'] = $ultimatecore['total'] + json_decode($d->data)->total90;
                    array_push($return, $ultimatecore);
                }
                if($d->name == "online-personal-coaching"){
                    $onlinepersonalcoaching['count'] = $onlinepersonalcoaching['count'] + json_decode($d->data)->count90;
                    $onlinepersonalcoaching['total'] = $onlinepersonalcoaching['total'] + json_decode($d->data)->total90;
                    array_push($return, $onlinepersonalcoaching);
                }
                if($d->name == "video-call"){
                    $videocall['count'] = $videocall['count'] + json_decode($d->data)->count90;
                    $videocall['total'] = $videocall['total'] + json_decode($d->data)->total90;
                    array_push($return, $videocall);
                }
                if($d->name == "end-range-training"){
                    $endrange['count'] = $endrange['count'] + json_decode($d->data)->count90;
                    $endrange['total'] = $endrange['total'] + json_decode($d->data)->total90;
                    array_push($return, $endrange);
                }
                if($d->name == "splits-and-hips"){
                    $splitsandhips['count'] = $splitsandhips['count'] + json_decode($d->data)->count90;
                    $splitsandhips['total'] = $splitsandhips['total'] + json_decode($d->data)->total90;
                    array_push($return, $splitsandhips);
                }
                if($d->name == "barbell-basics"){
                    $barbellbasics['count'] = $barbellbasics['count'] + json_decode($d->data)->count90;
                    $barbellbasics['total'] = $barbellbasics['total'] + json_decode($d->data)->total90;
                    array_push($return, $barbellbasics);
                }
                if($d->name == "total-body-reset"){
                    $totalbodyreset['count'] = $totalbodyreset['count'] + json_decode($d->data)->count90;
                    $totalbodyreset['total'] = $totalbodyreset['total'] + json_decode($d->data)->total90;
                    array_push($return, $totalbodyreset);
                }
                if($d->name == "beginners-bundle"){
                    $beginnersbundle['count'] = $beginnersbundle['count'] + json_decode($d->data)->count90;
                    $beginnersbundle['total'] = $beginnersbundle['total'] + json_decode($d->data)->total90;
                    array_push($return, $beginnersbundle);
                }
                if($d->name == "beginners-bundle-upgrade"){
                    $beginnersbundleupgrade['count'] = $beginnersbundleupgrade['count'] + json_decode($d->data)->count90;
                    $beginnersbundleupgrade['total'] = $beginnersbundleupgrade['total'] + json_decode($d->data)->total90;
                    array_push($return, $beginnersbundleupgrade);
                }
                if($d->name == "stability-builder"){
                    $stabilitybuilder['count'] = $stabilitybuilder['count'] + json_decode($d->data)->count90;
                    $stabilitybuilder['total'] = $stabilitybuilder['total'] + json_decode($d->data)->total90;
                    array_push($return, $stabilitybuilder);
                }
            }
        }

        // Calculate percentages
        $total = 0.00;
        foreach($return as &$p){
            $total = number_format($total + $p['total'],2,'.','');
        }
        foreach($return as &$product){
            if($product['total'] > 0){
                $product['percent'] = $this->cal_percentage($product['total'], $total) . '%';
            } 
        }
     
        usort($return, function ($item1, $item2) {
            return $item2['total'] <=> $item1['total'];
        });

        return array_slice($return, 0, 5, true);;

    }

    public function cal_percentage($num_amount, $num_total) {
      $count1 = $num_amount / $num_total;
      $count2 = $count1 * 100;
      $count = number_format($count2, 0);
      return $count;
    }

}
