<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Newsletter\Facades\Newsletter;
use GuzzleHttp\Client; // Make sure to install GuzzleHttp if not already installed

use Mail;
use App\Models\User;

class SendMail extends Controller
{
    private function verifyRecaptcha($token) {
      $client = new Client();
      $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
          'form_params' => [
              'secret' => env('RECAPTCHA_SECRET_KEY'), // Your secret key stored in .env file
              'response' => $token
          ]
      ]);

      return json_decode($response->getBody());
    }

    public function mailingListSignup(Request $request){
        $this->validate($request,[
          'first_name' => 'required|string|max:255',
          'last_name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255',
          'recaptcha_token' => 'required|string'
        ]);
        
        // Verify the reCAPTCHA token
        $recaptchaResponse = $this->verifyRecaptcha($request->recaptcha_token);
        if (!$recaptchaResponse->success) {
            return response()->json(['message' => 'ReCAPTCHA verification failed'], 422);
        }

        Newsletter::subscribe($request->input('email'), ['FNAME'=>$request->input('first_name'), 'LNAME'=>$request->input('last_name')]);
        
        Newsletter::subscribeOrUpdate(
            $request->input('email'), 
            [
                'first_name' => $request->input('FNAME'), 
                'last_name' => $request->input('LNAME')
            ],
            'subscribers',
            ['tags'=> ['7DOA']]
        );

        return "success";
    }
    
    public function sevendaysmailingListSignup(Request $request){
        $this->validate($request,[
          'email' => 'required|string|email|max:255',
          'FNAME' => 'required|string|max:255',
          'LNAME' => 'required|string|max:255',
          'recaptcha_token' => 'required|string'
        ]);

        // Verify the reCAPTCHA token
        $recaptchaResponse = $this->verifyRecaptcha($request->recaptcha_token);
        if (!$recaptchaResponse->success) {
            return response()->json(['message' => 'ReCAPTCHA verification failed'], 422);
        }

        Newsletter::subscribe($request->input('email'), ['FNAME'=>$request->input('FNAME'), 'LNAME'=>$request->input('LNAME')], 'subscribers');

        Newsletter::subscribeOrUpdate(
            $request->input('email'), 
            [
                'first_name' => $request->input('FNAME'), 
                'last_name' => $request->input('LNAME')
            ],
            'subscribers',
            ['tags'=> ['7DOA']]
        );

        return "success";
    }
    
    public function videoCallQuestionnaire(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'day' => 'required',
            'time' => 'required',
            'timezone' => 'required',
            'overview' => 'required|string|max:350',
        ]); 

        $subject = "Video Call Questionnaire";
        $name = $request->input('name');
        $email = $request->input('email');
        $day = $request->input('day');
        $time = $request->input('time');
        $timezone = $request->input('timezone');
        $overview = $request->input('overview');
    
        
        Mail::send('emails.videoCallQuestionnaire',[
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'day' => $day,
            'time' => $time,
            'timezone' => $timezone,
            'overview' => $overview
            ], function ($message) use ($subject, $name, $email, $day, $time, $timezone, $overview){
                $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->to('hello@tommorrison.uk');
            }
        );
        return 'success';
    }

    public function questionnaire(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'age' => 'required|numeric',
            'health_issues' => 'required',
            'past_injuries' => 'required',
            'diet' => 'required',
            'water' => 'required',
            'suppliments' => 'required',
            'training_frequency' => 'required',
            'sports' => 'required',
            'other_training' => 'required',
            'strength_training' => 'required',
            'struggle' => 'required',
            'aggravate' => 'required',
            'tightness' => 'required',
            'dominant_side' => 'required',
            'coach' => 'required',
            'progress' => 'required',
            'short_term_goals' => 'required',
            'long_term_goals' => 'required',
        ]); 

        $subject = "Questionnaire - Online personal coaching";
                $name = $request->input('name');
                $age = $request->input('age');
                $phone = $request->input('phone');
                $email = $request->input('email');
                $health_issues = $request->input('health_issues');
                $past_injuries = $request->input('past_injuries');
                $diet = $request->input('diet');
                $water = $request->input('water');
                $suppliments = $request->input('suppliments');
                $training_frequency = $request->input('training_frequency');
                $sports = $request->input('sports');
                $other_training = $request->input('other_training');
                $strength_training = $request->input('strength_training');
                $struggle = $request->input('struggle');
                $aggravate = $request->input('aggravate');
                $tightness = $request->input('tightness');
                $dominant_side = $request->input('dominant_side');
                $coach = $request->input('coach');
                $progress = $request->input('progress');
                $short_term_goals = $request->input('short_term_goals');
                $long_term_goals = $request->input('long_term_goals');
        
        Mail::send('emails.questionnaire',[
            'name' => $name,
            'subject' => $subject,
            'age' => $age,
            'phone' => $phone,
            'email' => $email,
            'health_issues' => $health_issues,
            'past_injuries' => $past_injuries,
            'diet' => $diet,
            'water' => $water,
            'suppliments' => $suppliments,
            'training_frequency' => $training_frequency,
            'sports' => $sports,
            'other_training' => $other_training,
            'strength_training' => $strength_training,
            'struggle' => $struggle,
            'aggravate' => $aggravate,
                    'tightness' => $tightness,
            'dominant_side' => $dominant_side,
            'coach' => $coach,
            'progress' => $progress,
            'short_term_goals' => $short_term_goals,
            'long_term_goals' => $long_term_goals,
            ], function ($message) use ($subject, $name, $age, $phone, $email, $health_issues, $past_injuries, $diet, $water, $suppliments, $training_frequency, $sports, $other_training, $strength_training, $struggle, $aggravate, $tightness, $dominant_side, $coach, $progress, $short_term_goals, $long_term_goals){
                $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->to('hello@tommorrison.uk');
            }
        );
        return 'success';
    }

    public function enquiry(Request $request){

        // Validate the form data
      $this->validate($request,[
          'name' => 'required|string|max:255',
          'email' => 'required|email|confirmed',
          'message' => 'required|string',
          'recaptcha_token' => 'required|string'
      ]); 

      $subject = $request->input('subject');
      $name = $request->input('name');
      $email = $request->input('email');
      $enquirytype = "Enquiry";
      $content = $request->input('message');
      $to = 'hello@tommorrison.uk';
      if($request->input('subject') != 'Injuries/Pains/The Programs'){
        $to = 'info@tommorrison.uk';
      }
      // Verify the reCAPTCHA token
      $recaptchaResponse = $this->verifyRecaptcha($request->recaptcha_token);
      if (!$recaptchaResponse->success) {
          return response()->json(['message' => 'ReCAPTCHA verification failed'], 422);
      }

      Mail::send('emails.enquiry',[
        'name' => $name,
        'subject' => $request->input('subject'),
        'email' => $email,
        'to' => $to,
        'enquirytype' => $enquirytype,
        'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $to, $enquirytype){
          $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
          $message->subject($name . " - " .  $subject . " - " . $enquirytype);
          $message->replyTo($email);
          $message->to($to);
          }
      );
      Mail::send('emails.enquiryAutoResponse',[
        'name' => $name,
        'subject' => $request->input('subject'),
        'email' => $email,
        'to' => $email,
        'enquirytype' => $enquirytype,
        'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $to, $enquirytype){
          $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
          $message->subject("Thanks for your email!");
          $message->to($email);
          }
      );
      return 'success';
    }
    
    public function shareJourney(Request $request){

        // Validate the form data
      $this->validate($request,[
        'email' => 'required|email',
      ]); 

      $user = User::where('email', $request->input('email'))->first();
      if($user){
        $subject = "Share SMM Journey";
                $user = $user;

          Mail::send('emails.shareJourney',[
            'subject' => $subject,
            'user' => $user
            ], function ($message) use ($subject, $user){
              $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                        $message->subject($subject);
                        $message->replyTo($user->email);
                        $message->to('hello@tommorrison.uk');
                        //$message->to('luke@elementseven.co');
                    }
                );
                return response()->json(['message' => 'success'], 200);
      }else{
        return response()->json(['message' => 'No user found with that email, please use the email you used to create your account.'], 422);
      }
    }

    public function support(Request $request){

        // Validate the form data
      $this->validate($request,[
          'name' => 'required|string|max:255',
          'subject' => 'required|string|max:40',
          'email' => 'required|email',
          'message' => 'required|string',
      ]); 

      $subject = $request->input('subject');
      $name = $request->input('name');
      $email = $request->input('email');
      $content = $request->input('message');
      $enquirytype = "Dashboard Enquiry";
            
      Mail::send('emails.enquiry',[
        'name' => $name,
        'subject' => $request->input('subject'),
        'email' => $email,
        'enquirytype' => $enquirytype,
        'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $enquirytype){
          $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                    $message->subject($name . " - " .  $subject . " - " . $enquirytype);
                    $message->replyTo($email);
                    $message->to('hello@tommorrison.uk');
                }
            );
      return 'success';
    }

    public function askQuestion(Request $request){
            // Validate the form data
      $this->validate($request,[
          'name' => 'required|string|max:255',
          'email' => 'required|email',
          'message' => 'required|string',
      ]); 

      $subject = "Review Page";
      $name = $request->input('name');
      $email = $request->input('email');
      $content = $request->input('message');
      $enquirytype = "Enquiry";

      Mail::send('emails.enquiry',[
        'name' => $name,
        'subject' => $subject,
        'email' => $email,
        'enquirytype' => $enquirytype,
        'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $enquirytype){
          $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                    $message->subject($name . " - " .  $subject . " - " . $enquirytype);
                    $message->replyTo($email);
                    $message->to('hello@tommorrison.uk');
                }
            );
      return 'success';
    }
    public function seminar(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'gym' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "Seminar Request";
        $name = $request->input('name');
        $gym = $request->input('gym');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $content = $request->input('message');
        
        Mail::send('emails.seminarRequest',[
            'name' => $name,
            'subject' => $subject,
            'gym' => $gym,
            'phone' => $phone,
            'email' => $email,
            'content' => $content
            ], function ($message) use ($subject, $email, $phone, $name, $gym, $content){
                $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->replyTo($email);
                $message->to('info@tommorrison.uk');
            }
        );
        return 'success';
    }
}
