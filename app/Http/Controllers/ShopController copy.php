<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

use App\Models\Category;
use App\Models\Voucher;
use App\Models\Variant;
use App\Models\Address;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Setting;
use App\Models\Order;
use App\Models\Merch;
use App\Models\User;

use Srmklive\PayPal\Services\PayPal as PayPalClient;

use Spatie\Newsletter\Facades\Newsletter;
use Session;
use Stripe;
use Stripe\PaymentIntent;
use Cart;
use Mail;
use Auth;

class ShopController extends Controller
{   

    protected $provider;
    
    // Beginners bundle prices
    private $bb_smm_price = 55.83;
    private $bb_wiww_price = 7.89;
    private $bb_uc_price = 24.68;

    // private $bb_smm_price = 42.35;
    // private $bb_wiww_price = 4.79;
    // private $bb_uc_price = 32.42;

    // Intermediate bundle prices
    private $ib_sb_price = 56.38;
    private $ib_er_price = 83.62;

    // private $ib_sb_price = 65.81;
    // private $ib_er_price = 60.19;

    // Complete bundle prices
    private $cb_smm_price = 48.30;
    private $cb_wiww_price = 5.64;
    private $cb_uc_price = 21.86;
    private $cb_sb_price = 54.40;
    private $cb_er_price = 78.80;

    // private $cb_smm_price = 47.92;
    // private $cb_wiww_price = 4.41;
    // private $cb_uc_price = 22.89;
    // private $cb_sb_price = 58.85;
    // private $cb_er_price = 64.48;

    // January Mobility reset bundle prices
    // private $mr_smm_price = 59.00;
    // private $mr_mr_price = 75.00;

    private $mr_smm_price = 43.45;
    private $mr_mr_price = 63.75;

    // August Mobility reset bundle prices
    // private $aug_mr_smm_price = 50.00;
    // private $aug_mr_mr_price = 80.00;

    private $aug_mr_smm_price = 50.00;
    private $aug_mr_mr_price = 65.50;

    // January 24 Mobility reset bundle prices
    // private $aug_mr_smm_price = 69.00;
    // private $aug_mr_mr_price = 85.00;

    private $jan_mr_smm_price = 45.90;
    private $jan_mr_mr_price = 69.60;

    private function getApiKey(){
        return env('PRINTFUL_TOKEN');
    }

    // public function getEmails(){
    //     $product = Product::where('id', 18)->first();
    //     $emails = array();
    //     foreach($product->users as $u){
    //         array_push($emails, $u->email);
    //     }
    //     return $emails;
    // }

    public function updateAddress(){
        return view('shop.updateAddress');
    }

    public function subscribeToMailingList(){

        $user = Auth::user();
        $tags = array();

        foreach($user->products as $p){
            if($p->mailchimptag != null){
                array_push($tags, $p->mailchimptag);
            }
        }

        Newsletter::subscribeOrUpdate(
            $user->email, 
            [
                'first_name' => $user->first_name, 
                'last_name' => $user->last_name
            ],
            'subscribers',
            ['tags'=> $tags]
        );


        return response()->json(['success' => 'success', 200]);
    }

    public function saveAddress(Request $request){
        
        if($request->input('same') == 'on'){
            // Validate the form data
            $this->validate($request,[
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1'
            ]);
        }else{
            // Validate the form data
            $this->validate($request,[
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1',
                'shipping-streetAddress' => 'required|string|max:255|min:3',
                'shipping-city' => 'required|string|max:255|min:3',
                'shipping-postalCode' => 'required|string|max:255|min:3',
                'shipping-country' => 'required|string|max:255|min:1'
            ]);
        }

        $user = Auth::user();

        if(count($user->addresses)){
            foreach($user->addresses as $address){
                $address->delete();
            }
        }

        // Create user address(es)
        $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'billing');
    
        if($request->input('same') == 'on'){
            $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'shipping');
        }else{
            $shippingAddress = $this->createAddress($request->input('shipping-streetAddress'), $request->input('shipping-extendedAddress'),$request->input('shipping-city'), $request->input('shipping-postalCode'), $request->input('shipping-country'), $request->input('shipping-region'), $request->input('shipping-company'), 'shipping');
        }

        $this->createStripeCustomer($user);
        
        return redirect()->to('/checkout');

    }

    /**
     * process transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function processTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $paypalToken = $provider->getAccessToken();

        $items = Array();

        // Get bundle status
        $bundle = Setting::where('id',1)->first();

        // Check if there is a voucher attached to the invoice and update each individual bundle item's price for PayPal
        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
            if(count($invoice->vouchers)){
                $voucher = $invoice->vouchers[0];

                $this->bb_smm_price = number_format((float)($this->bb_smm_price - (($this->bb_smm_price / 100) * $voucher->percent)), 2, '.', '');
                $this->bb_wiww_price = number_format((float)($this->bb_wiww_price - (($this->bb_wiww_price / 100) * $voucher->percent)), 2, '.', '');
                $this->bb_uc_price = number_format((float)($this->bb_uc_price - (($this->bb_uc_price / 100) * $voucher->percent)), 2, '.', '');
                $this->ib_sb_price = number_format((float)($this->ib_sb_price - (($this->ib_sb_price / 100) * $voucher->percent)), 2, '.', '');
                $this->ib_er_price = number_format((float)($this->ib_er_price - (($this->ib_er_price / 100) * $voucher->percent)), 2, '.', '');
                $this->cb_smm_price = number_format((float)($this->cb_smm_price - (($this->cb_smm_price / 100) * $voucher->percent)), 2, '.', '');
                $this->cb_wiww_price = number_format((float)($this->cb_wiww_price - (($this->cb_wiww_price / 100) * $voucher->percent)), 2, '.', '');
                $this->cb_uc_price = number_format((float)($this->cb_uc_price - (($this->cb_uc_price / 100) * $voucher->percent)), 2, '.', '');
                $this->cb_sb_price = number_format((float)($this->cb_sb_price - (($this->cb_sb_price / 100) * $voucher->percent)), 2, '.', '');
                $this->cb_er_price = number_format((float)($this->cb_er_price - (($this->cb_er_price / 100) * $voucher->percent)), 2, '.', '');
                $this->mr_smm_price = number_format((float)($this->mr_smm_price - (($this->mr_smm_price / 100) * $voucher->percent)), 2, '.', '');
                $this->mr_mr_price = number_format((float)($this->mr_mr_price - (($this->mr_mr_price / 100) * $voucher->percent)), 2, '.', '');
                $this->aug_mr_smm_price = number_format((float)($this->aug_mr_smm_price - (($this->aug_mr_smm_price / 100) * $voucher->percent)), 2, '.', '');
                $this->aug_mr_mr_price = number_format((float)($this->aug_mr_mr_price - (($this->aug_mr_mr_price / 100) * $voucher->percent)), 2, '.', '');
                $this->jan_mr_smm_price = number_format((float)($this->jan_mr_smm_price - (($this->jan_mr_smm_price / 100) * $voucher->percent)), 2, '.', '');
                $this->jan_mr_mr_price = number_format((float)($this->jan_mr_mr_price - (($this->jan_mr_mr_price / 100) * $voucher->percent)), 2, '.', '');
            }
        }else{
            $invoice = NULL;
        }

        foreach(Cart::content() as $cc){

            if($cc->name == "Beginners Bundle"){
                
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item1);

                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_wiww_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item2);

                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_uc_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item3);

            }else if($cc->name == "Beginner's Bundle Upgrade"){


                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) 3.40, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) 16.00, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item3);

            }else if($cc->name == "Intermediate Bundle"){

                // 17.34052% discount

                // Stability Builder
                $product1 = Product::where('id', 17)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // End Range
                $product2 = Product::where('id', 6)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Complete Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item1);

                // Where I went wrong ebook
                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_wiww_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item2);

                // Ultimate Core
                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_uc_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item3);

                // Stability Builder
                $product4 = Product::where('id', 17)->first();
                $item4 = Array(
                    "name" => $product4->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item4);

                // End Range
                $product5 = Product::where('id', 6)->first();
                $item5 = Array(
                    "name" => $product5->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item5);

            }else if($cc->name == "Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 22)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Aug 24 Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->aug_mr_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 28)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->aug_mr_mr_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Jan 24 Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->jan_mr_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 26)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->jan_mr_mr_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else{
                $item = Array(
                    "name" => $cc->name,
                    "unit_amount" => Array("value" => number_format((float) $cc->price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => $cc->qty
                );
                array_push($items, $item);
            }
        }
        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('successTransaction'),
                "cancel_url" => route('cancelTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "GBP",
                        "value" => number_format((float)Cart::total(), 2, '.', ''),
                        "breakdown" => Array( "item_total" => Array( "value" => number_format((float)Cart::total(), 2, '.', ''), "currency_code" => 'GBP') ),
                    ],
                    "items" => $items
                ]
            ]
        ]);

        if (isset($response['id']) && $response['id'] != null) {

            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }

            return redirect()
                ->route('checkout')
                ->with('errors', 'Something went wrong.');

        }else {
            if(isset($response['message'])){
                session(['payment_error' => $response['message']]);
                return redirect()
                ->route('checkout')
                ->with('errors', $response['message'] ?? 'Something went wrong.');
            }else{
                session(['payment_error' => 'Something went wrong.']);
                return redirect()
                ->route('checkout')
                ->with('errors', 'Something went wrong.');
            }   
        }
    }

    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request){

        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
            return redirect()->to('/empty-basket');
        }
        if(!$invoice || $invoice == null){
            return redirect()->to('/empty-basket');
        }
        $errors = null;
        $bundle = Setting::where('id',1)->first();
        if(Auth::check()){
            if(count(Auth::user()->addresses) < 2){
                return redirect()->to('/update-address');
            }
            foreach(Cart::content() as $row){
                if($row->options->gift == "yes"){
                    return redirect()->to('/gift-checkout');
                }
            }
            if (session()->has('intent')) {
                $intent = session('intent');  
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key

                try {
                    $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the intent
                    $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                    $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the intent
                    $intentnew = $this->getIntent();
                    session(['intent' => $intentnew]);
                } catch (\Exception $e) {
                    return 'error Failed to cancel the payment intent: ' . $e->getMessage();
                }

            }else{
                $intent = $this->getIntent();
                session(['intent' => $intent]);
            }
            return view('shop.checkout',compact('intent','invoice','bundle','errors'));
        }else{
            return view('shop.create-account',compact('invoice','bundle'));
        }

        
    }


    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function successTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $provider->getAccessToken();
        $response = $provider->capturePaymentOrder($request['token']);
        $invoice = Invoice::where('id', session('invoice_id'))->first();
        $user = Auth::user();
        if (isset($response['status']) && $response['status'] == 'COMPLETED') {

            return $this->success($user, $invoice, 'PayPal', $response['id']);

        }else if(isset($response['message']) && str_contains($response['message'], '"issue":"ORDER_ALREADY_CAPTURED"')){

            return redirect()->to('/login');

        }else {
            return redirect()->route('checkout-error')->withErrors(['error' => $response['message'] ?? 'Something went wrong.']);
        }
    }

    /**
     * Error capture.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkoutError(Request $request){
        return view('shop.checkouterror');
    }

    /**
     * cancel transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelTransaction(Request $request)
    {
        return redirect()
            ->route('checkout')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }


    public function getIntent(){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();

        $invoice_id = null;
        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
            if($invoice){
                $invoice_id = $invoice->id;
            }else{
                return redirect()->to('/empty-basket');
            }
            
        }else{
            return redirect()->to('/empty-basket');
        }

        $total = Cart::total(2,'.','') * 100;

        if($user->stripe_id == null){
            $this->createStripeCustomer($user);
        }

        $products = Array();
        foreach($invoice->products as $product){
            if($product->slug == "beginners-bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->bb_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->bb_wiww_price * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => $this->bb_uc_price * 100
                    ]
                ]);
            }else if($product->slug == "beginners-bundle-upgrade"){
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => 3.40 * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => 16.00 * 100
                    ]
                ]);
            }else if($product->slug == "intermediate-bundle"){
                $product1 = Product::where('id', 17)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Stability Builder",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->ib_sb_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 6)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "End Range Training",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->ib_er_price * 100
                    ]
                ]);
            }else if($product->slug == "complete-bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->cb_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->cb_wiww_price * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => $this->cb_uc_price * 100
                    ]
                ]);
                $product4 = Product::where('id', 17)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Stability Builder",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product4->stripe_id,
                        'unit_amount_decimal' => $this->cb_sb_price * 100
                    ]
                ]);
                $product5 = Product::where('id', 6)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "End Range Training",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product5->stripe_id,
                        'unit_amount_decimal' => $this->cb_er_price * 100
                    ]
                ]);
            }else if($product->slug == "reset-smm-bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 22)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "January 30 Day Mobility Reset",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->mr_mr_price * 100
                    ]
                ]);
            }else if($product->slug == "aug-reset-24-smm-bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 29)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "August Mobility Reset 2024",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_mr_price * 100
                    ]
                ]);
            }else if($product->slug == "jan24-reset-smm-bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->jan_mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 26)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "January Mobility Reset 2024",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->jan_mr_mr_price * 100
                    ]
                ]);
            }else{
                $theprice = $product->pivot->price;
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $product->name,
                    'quantity' => $product->pivot->qty,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product->stripe_id,
                        'unit_amount_decimal' => $theprice * 100
                    ]
                ]);
            }
        }

        foreach($invoice->variants as $variant){
            $theprice = $variant->pivot->price;
            $productdata = \Stripe\InvoiceItem::create([
                'customer' => Auth::user()->stripe_id,
                'description' => $variant->merch->name,
                'quantity' => $variant->pivot->qty,
                'price_data' => [
                    'currency' => 'gbp',
                    'product' => $variant->merch->stripe_id,
                    'unit_amount_decimal' => $theprice * 100
                ]
            ]);
        }

        $stripeinvoice = \Stripe\Invoice::create([
            'customer' => Auth::user()->stripe_id,
            'statement_descriptor' => 'Tom Morrison'
        ]);
        $stripeinvoiceid = $stripeinvoice->id;
        $finalized = $stripeinvoice->finalizeInvoice();
        $intent = \Stripe\PaymentIntent::Retrieve($finalized->payment_intent);

        // $intent = \Stripe\PaymentIntent::create([
        //     'amount' => $total,
        //     'customer' => Auth::user()->stripe_id,
        //     'statement_descriptor' => 'Tom Morrison',
        //     'currency' => 'gbp',
        //     'metadata' => [
        //         'order_id' => $invoice_id,
        //         'integration_check' => 'accept_a_payment'
        //     ]
        // ]);   
    
        return $intent;
    }

    public function createStripeCustomer($user){
        $user = Auth::user();
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $billingAddress = Address::where('user_id', $user->id)->where('type','billing')->first();
        $stripecustomer = \Stripe\Customer::create([
            'address' => [
                'city' => $billingAddress->city,
                'country' => $billingAddress->country,
                'line1' => $billingAddress->streetAddress,
                'line2' => $billingAddress->extendedAddress,
                'postal_code' => $billingAddress->postalCode,
                'state' => $billingAddress->region,
            ],
            'email' => $user->email,
            'name' => $user->full_name,
        ]);
        $user->stripe_id = $stripecustomer->id;
        $user->save();
        return $user;
    }

    public function index(){
        $categories = Category::whereHas('products')->get();
        $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->get();
        return view('shop.index', compact('categories','products'));
    }

    public function newShop(){
        $categories = Category::whereHas('products')->get();
        $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->get();
        return view('shop.shop-new', compact('categories','products'));
    }

    public function createAccount(Request $request){
        
        if($request->input('same') == 'on'){
            // Validate the form data
            $this->validate($request,[ 
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users|confirmed',
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1'
            ]);
        }else{
            // Validate the form data
            $this->validate($request,[
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users|confirmed',
                'agree' => 'accepted',
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1',
                'shipping-streetAddress' => 'required|string|max:255|min:3',
                'shipping-city' => 'required|string|max:255|min:3',
                'shipping-postalCode' => 'required|string|max:255|min:3',
                'shipping-country' => 'required|string|max:255|min:1'
            ]);
        }

        // Generate Password
        $password = $request->input('password');

        // Create User
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => Hash::make($password),
            'country' => $request->input('country'),
            'password' => Hash::make($password),
            'role_id' => 2
        ]);

        // Login User
        Auth::login($user, true);

        // Send account created email
        $this->accountCreated($user, $password);

        // Create user address(es)
        $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'billing');
    
        if($request->input('same') == 'on'){
            $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'shipping');
        }else{
            $shippingAddress = $this->createAddress($request->input('shipping-streetAddress'), $request->input('shipping-extendedAddress'),$request->input('shipping-city'), $request->input('shipping-postalCode'), $request->input('shipping-country'), $request->input('shipping-region'), $request->input('shipping-company'), 'shipping');
        }

        $request->session()->put('account-created', $user->first_name);

        $this->createStripeCustomer($user);

        return redirect()->to('/checkout');

    }

    public function paymentStatus(Request $request, Invoice $invoice){

        $user = Auth::user();

        if($request->query('payment_intent')){
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $clientsecret = $request->query('payment_intent_client_secret');
            $intent = \Stripe\PaymentIntent::retrieve($request->query('payment_intent'));
            if($intent->status == 'succeeded'){
                return $this->success($user, $invoice, 'Card', $intent->id);
            }else{
                // Error with the stripe payment
                session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
                return redirect()->to('/checkout');

            }
        }else{
            // Error with the stripe payment
            session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
            return redirect()->to('/checkout');
        }
    }
    
    public function success($user, $invoice, $paymentmethod, $transaction_id){

        $checkifpaid = Invoice::where('transaction_id', $transaction_id)->first();

        if (is_null($checkifpaid)) {

            // Attach products to user account
            foreach($invoice->products as $i){
                if($i->id != 4 && $i->id != 5){
                    $user->products()->attach($i->id);
                }
            }  
            
            // Update invoice params
            $invoice->paid = true;
            $invoice->transaction_id = $transaction_id;
            $invoice->user_id = $user->id;
            $invoice->payment_method = $paymentmethod;
            $invoice->save();


            // Check for merchandise and order
            if(count($invoice->variants)){
                $orderresult = $this->orderMerch($invoice);
                $object = json_decode(json_encode($orderresult), FALSE);
                if($object->status == 'failed'){
                    session(['general_payment_error' => "There was an error ordering your merchandise, please try again later."]);
                    return back();
                }
                // Create a new merchandise order record for each variant
                foreach (Cart::content() as $item){
                    if($item->options->type == 'merch'){
                        $variant = Variant::where('id', $item->id)->first();
                        $order = $this->createOrder($object->id, $object->status, $item->qty, $variant->id, $invoice->id, $user->id);
                    }
                }
            }
            
            if($invoice->products->contains(15)){
                // Beginners bundle
                if(!$user->products()->where('products.id', 1)->exists()){
                    $user->products()->attach(1);
                }
                if(!$user->products()->where('products.id', 2)->exists()){
                    $user->products()->attach(2);
                }
                if(!$user->products()->where('products.id', 3)->exists()){
                    $user->products()->attach(3);
                }
            }
            
            if($invoice->products->contains(16)){
                // Beginners bundle upgrade
                if(!$user->products()->where('products.id', 2)->exists()){
                    $user->products()->attach(2);
                }
                if(!$user->products()->where('products.id', 3)->exists()){
                    $user->products()->attach(3);
                }
            }

            if($invoice->products->contains(20)){
                // Intermediate bundle
                if(!$user->products()->where('products.id', 17)->exists()){
                    $user->products()->attach(17);
                }
                if(!$user->products()->where('products.id', 6)->exists()){
                    $user->products()->attach(6);
                }
            }

            if($invoice->products->contains(21)){
                // Complete bundle
                if(!$user->products()->where('products.id', 1)->exists()){
                    $user->products()->attach(1);
                }
                if(!$user->products()->where('products.id', 2)->exists()){
                    $user->products()->attach(2);
                }
                if(!$user->products()->where('products.id', 3)->exists()){
                    $user->products()->attach(3);
                }
                if(!$user->products()->where('products.id', 17)->exists()){
                    $user->products()->attach(17);
                }
                if(!$user->products()->where('products.id', 6)->exists()){
                    $user->products()->attach(6);
                }
            }

            if($invoice->products->contains(23)){
                // Mobility Reset + SMM Bundle
                if(!$user->products()->where('products.id', 1)->exists()){
                    $user->products()->attach(1);
                }
                if(!$user->products()->where('products.id', 22)->exists()){
                    $user->products()->attach(22);
                }
            }

            if($invoice->products->contains(29)){
                // Mobility Reset + SMM Bundle
                if(!$user->products()->where('products.id', 1)->exists()){
                    $user->products()->attach(1);
                }
                if(!$user->products()->where('products.id', 28)->exists()){
                    $user->products()->attach(28);
                }
            }

            if($invoice->products->contains(27)){
                // Jan 24 Mobility Reset + SMM Bundle
                if(!$user->products()->where('products.id', 1)->exists()){
                    $user->products()->attach(1);
                }
                if(!$user->products()->where('products.id', 26)->exists()){
                    $user->products()->attach(26);
                }
            }

            if($invoice->products->contains(4)){
                // Send payment confirmation email for PMP Program
                $this->personalisedMobilityPlanPaymentConfirmed($invoice, $user);
            }
            else if($invoice->products->contains(1)){
                // Send payment confirmation email for SMM
                $this->smmPaymentConfirmed($invoice, $user);
            }
            else if($invoice->products->contains(5)){
                // Send payment confirmation email for Video Call
                $this->videoCallPaymentConfirmed($invoice, $user);
            }
            else{
                // Send payment confirmation email
                $this->paymentConfirmed($invoice, $user);
            }

            Cart::destroy();
            session()->forget('intent');
            session()->forget('invoice_id');
            session(['successful_payment' => $invoice->id]);


            // Check for smm and offer upgrade
            $smm = false;
            $bb = false;
            $upgrade = null;
            if(!$invoice->products->contains(20) || !$invoice->products->contains(21) || !$invoice->products->contains(23)){
                if($invoice->products->contains(1)){
                    $user = Auth::user();
                    $test = 0;
                    if($user->products->contains(2)){
                        $test++;
                    }
                    if($user->products->contains(3)){
                        $test++;
                    }
                    if($test == 0){
                        $smm = true;
                    }
                    $upgrade = Product::where('slug', 'beginners-bundle-upgrade')->first();
                }
            }
            if($invoice->products->contains(15)){
                $bb = true;
            }
        }else{
            $invoice = $checkifpaid;
            $smm = null;
            $bb = null;
            $upgrade = null;
        }
        // Show success page
        $this->subscribeToMailingList();
        return view('shop.success')->with(['invoice' => $invoice, 'smm' => $smm, 'bb' => $bb, 'upgrade' => $upgrade]);

    } 
    
    public function getProducts(Request $request, $category, $limit){
        if($category == 'all'){
            if($request->input('featured')){
                $products = Product::where([['status','Available'],['featured', true]])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
            }else{
                $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->paginate($limit);
            }
        }else if($category == 'level-one'){
            $products = Product::whereIn('id',[1,2,3])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-two'){
            $products = Product::whereIn('id',[17])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-three'){
            $products = Product::whereIn('id',[6])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-four'){
            $products = Product::whereIn('id',[7,8])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-five'){
            $products = Product::whereIn('id',[5,4])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'online-camps'){
            $products = Product::where('category_id',13)->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else{
            $products = Product::where('status','Available')->orderBy('created_at','desc')->whereHas('category', function($q) use($category){
                $q->where('slug', $category);
            })->with('category')->paginate($limit);
        }
        foreach($products as $p){
            $p->image = $p->getFirstMediaUrl('products', 'normal');
            $p->webp = $p->getFirstMediaUrl('products', 'normal-webp');
            $p->large = $p->getFirstMediaUrl('products', 'large');
            $p->largewebp = $p->getFirstMediaUrl('products', 'large-webp');
            $p->mimetype = $p->getFirstMedia('products')->mime_type;
        }
        return $products;
    }

    public function getBundles(){
        $bundles = Product::whereIn('slug',['beginners-bundle','intermediate-bundle','complete-bundle'])->orderBy('created_at','asc')->get(['id','name', 'slug', 'short','price','sale_price']);

        foreach($bundles as $b){
            $b->percent = number_format((($b->price - $b->sale_price)*100) /$b->price, 0);
        }

        return $bundles;
    }
    
    public function product($slug){
        if($slug == 'end-range-training'){
            return redirect()->to('https://endrange.tommorrison.uk/purchase');
        }
        $product = Product::where('slug', $slug)->first();
        if($product){
            return view('shop.product', compact('product'));
        }else{
            abort(404);
        }
    }
    
    public function basket(){
        $bundle = Setting::where('id',1)->first();
        $now = Carbon::now();
        $showvoucher = Voucher::where('expiry_date','>=',$now)->count();
        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{ 
            $invoice = NULL;
        }
        $inbasket = [];
        foreach(Cart::content() as $i){
            array_push($inbasket, $i->id);
        }
        if(count(Cart::content()) > 0){
            if (in_array(15, $inbasket)){
                $products = Product::whereIn('id', [8,7,6,5,17])->orderBy('created_at', 'desc')->paginate(5);
            }else{
                $products = Product::where('id','!=', [$inbasket])->paginate(5);
            }
        }else{
            $products = Product::paginate(5);
        }
        return view('shop.basket',compact('invoice','bundle','products','showvoucher'));
    }
    
    public function emptyBasket(){

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key
        if (session()->has('intent')) {
            $intent = session('intent');  
            try {
                $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the invoice
                $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the invoice
                session()->forget('intent');
            } catch (\Exception $e) {
                return 'error Failed to cancel the payment intent: ' . $e->getMessage();
            }

        }
        
        if(session('invoice_id') != NULL){
            Cart::destroy();
            $invoice = Invoice::where("id",session('invoice_id'))->first();
            if($invoice && $invoice != null){
                foreach($invoice->vouchers as $v){
                    $invoice->vouchers()->detach($v);
                }
                $invoice->delete();
            }
            session()->forget('invoice_id');
        }
        return redirect()->to("/basket");
    }
    
    public function remove(Request $request){
        $invoice = Invoice::where("id",session('invoice_id'))->first();
        if(session('invoice_id') != NULL){
            
            $hasProduct = $invoice->products()->where('id', $request->input('id'))->exists();
            if($hasProduct){
                $invoice->products()->detach($request->input('id'));
            }
            $hasVariant = $invoice->variants()->where('id', $request->input('id'))->exists();
            if($hasVariant){
                $invoice->variants()->detach($request->input('id'));
            }
    
            Cart::remove($request->input('rowId'));
    
            $this->addProducts($invoice);

            if(count($invoice->products) == 0 && count($invoice->variants) == 0){
                return redirect()->to('/empty-basket');
            }

        }else{
            return redirect()->to('/empty-basket');
        }
        
        return redirect()->to("/basket");
    }
    
    private function addProducts(Invoice $invoice)
    {
        session()->forget('intent');
        // Attach to invoice and add to basket
        $bundle = Setting::where('id',1)->first();
        
        if($bundle->status == "Active" && count($invoice->products) > 1){

            foreach(Cart::content() as $i){
                if($i->options->type != 'merch'){
                    $p = Product::where('id',$i->id)->first();
                    if($p->sale_price != NULL){
                        $productprice = $p->sale_price - (($p->sale_price / 100) * $bundle->value);
                        $p->invoices()->updateExistingPivot($invoice->id, array('price' => $productprice));
                        Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$p->getFirstMediaUrl('products','normal'),'original_price' => $p->price, 'gift' => 'no']]);
                    }else{
                        $productprice = $p->price - (($p->price / 100) * $bundle->value);
                        $p->invoices()->updateExistingPivot($invoice->id, array('price' => $productprice));
                        Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$p->getFirstMediaUrl('products','normal'),'original_price' => $p->price, 'gift' => 'no']]);
                    }
                }

            }

        }else{
            foreach(Cart::content() as $i){
                if($i->options->type != 'merch'){
                    $p = Product::where('id',$i->id)->first();
                    if($p->sale_price != NULL){
                        $p->invoices()->updateExistingPivot($invoice->id, array('price' => $p->sale_price));
                        Cart::update($i->rowId, ['price' => number_format((float)$p->sale_price, 2, '.', ''), 'options' => ['image'=>$p->getFirstMediaUrl('products','normal'),'original_price' => $p->price, 'gift' => 'no']]);
                    }else{
                        $p->invoices()->updateExistingPivot($invoice->id, array('price' => $p->price));
                        Cart::update($i->rowId, ['price' => number_format((float)$p->price, 2, '.', ''), 'options' => ['image'=>$p->getFirstMediaUrl('products','normal'), 'gift' => 'no']]);
                    }
                }
            }
        }
    
        $this->updateTotal($invoice);
    
        $items = Cart::content();
        $tax_rate = 0;
            config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'
    
            foreach ($items as $item){
                $item->setTaxRate($tax_rate);
                Cart::update($item->rowId, $item->qty); 
            }
            return 'success';
    
        }

        public function upgradeToStabilityBuilder(Request $request){
            if(session('invoice_id') != NULL){
                Cart::destroy();
                $invoice = Invoice::where("id",session('invoice_id'))->first();
                foreach($invoice->vouchers as $v){
                    $invoice->vouchers()->detach($v);
                }
                $invoice->delete();
                session()->forget('invoice_id');
            }
            
            $product = Product::where('slug', 'stability-builder')->first();
            $user = Auth::user();
            
            foreach($user->products as $p){
                if($p->id == 17){
                    $request->session()->put('already-purchased', $p->name);
                    return redirect()->to('/basket');
                }
            }
            
            
            $invoice = $this->createInvoice(NULL, NULL, 0, 0, NULL);
            $request->session()->put('invoice_id', $invoice->id);
        
            // Attach product to invoice and add to basket
   
            $invoice->products()->attach($product->id, array('price' => 67.50, 'qty' => 1));
            
            
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => 67.50, 'weight' => 0, 'options' => ['image'=>$product->image, 'gift' => 'no']])->associate('App\Models\Product');
    
            $this->updateTotal($invoice);
            
            $items = Cart::content();
            $tax_rate = 0;
            config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'
    
            foreach ($items as $item){
                $item->setTaxRate($tax_rate);
                Cart::update($item->rowId, $item->qty); 
            }
            return redirect()->to('/checkout');
        }
        
        public function addUpgradeToBasket(Request $request)
        {
            
            if(session('invoice_id') != NULL){
                Cart::destroy();
                $invoice = Invoice::where("id",session('invoice_id'))->first();
                foreach($invoice->vouchers as $v){
                    $invoice->vouchers()->detach($v);
                }
                $invoice->delete();
                session()->forget('invoice_id');
            }
            if(Auth::check()){
                $product = Product::where('slug', 'beginners-bundle-upgrade')->first();
                $user = Auth::user();
                
                foreach($user->products as $p){
                    if($p->id == 2 && $p->id == 3){
                        $request->session()->put('already-purchased', $p->name);
                        return redirect()->to('/basket');
                    }
                }
                
                
                $invoice = $this->createInvoice(NULL, NULL, 0, 0, NULL);
                $request->session()->put('invoice_id', $invoice->id);
            
                // Attach product to invoice and add to basket
                if($product->sale_price != NULL){
                    $invoice->products()->attach($product->id, array('price' => $product->sale_price, 'qty' => 1));
                }else{
                    $invoice->products()->attach($product->id, array('price' => $product->price, 'qty' => 1));
                }
                
                Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['image'=>$product->image, 'gift' => 'no']])->associate('App\Models\Product');
        
                $this->updateTotal($invoice);
                
                $items = Cart::content();
                $tax_rate = 0;
                config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'
        
                foreach ($items as $item){
                    $item->setTaxRate($tax_rate);
                    Cart::update($item->rowId, $item->qty); 
                }
                return redirect()->to('/checkout');
            }else{
                return redirect()->to('/login');
            }
            
        }
        
        public function addProductToBasket(Request $request, Product $product)
        {
            session()->forget('intent');
            foreach(Cart::content() as $row){
                if($row->options->gift == "yes"){
                    $request->session()->put('gift-still-in-basket', true);
                    return redirect()->to('/gift-basket');
                }
                
                // Check for beginners bundle conflicts
                if($row->id == 1 && $product->slug == "beginners-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the beginners bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 2 && $product->slug == "beginners-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the beginners bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 3 && $product->slug == "beginners-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the beginners bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 1 && $row->name == "Beginners Bundle"){
                    $request->session()->put('duplicate-product','the beginners bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 2 && $row->name == "Beginners Bundle"){
                    $request->session()->put('duplicate-product','the beginners bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 3 && $row->name == "Beginners Bundle"){
                    $request->session()->put('duplicate-product','the beginners bundle');
                    return redirect()->to('/basket');
                }

                // Intermediate bundle checks
                if($row->id == 17 && $product->slug == "intermediate-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the intermediate bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 6 && $product->slug == "intermediate-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the intermediate bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 17 && $row->name == "Intermediate Bundle"){
                    $request->session()->put('duplicate-product','the intermediate bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 6 && $row->name == "Intermediate Bundle"){
                    $request->session()->put('duplicate-product','the intermediate bundle');
                    return redirect()->to('/basket');
                }

                // Check for complete bundle conflicts
                if($row->id == 1 && $product->slug == "complete-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the complete bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 2 && $product->slug == "complete-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the complete bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 3 && $product->slug == "complete-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the complete bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 17 && $product->slug == "complete-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the complete bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 6 && $product->slug == "complete-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the complete bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 1 && $row->name == "Complete Bundle"){
                    $request->session()->put('duplicate-product','the complete bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 2 && $row->name == "Complete Bundle"){
                    $request->session()->put('duplicate-product','the complete bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 3 && $row->name == "Complete Bundle"){
                    $request->session()->put('duplicate-product','the complete bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 17 && $row->name == "Complete Bundle"){
                    $request->session()->put('duplicate-product','the complete bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 6 && $row->name == "Complete Bundle"){
                    $request->session()->put('duplicate-product','the complete bundle');
                    return redirect()->to('/basket');
                }

                // Mobility Reset + SMM bundle checks
                if($row->id == 1 && $product->slug == "reset-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 22 && $product->slug == "reset-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 1 && $row->name == "Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 22 && $row->name == "Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }

                // Aug Mobility Reset + SMM bundle checks
                if($row->id == 1 && $product->slug == "aug-reset-24-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the August mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 28 && $product->slug == "aug-reset-24-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the August mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 1 && $row->name == "Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the August mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 28 && $row->name == "Aug Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the August mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }

                // January 24 Mobility Reset + SMM bundle checks
                if($row->id == 1 && $product->slug == "jan24-reset-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the January mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($row->id == 26 && $product->slug == "jan24-reset-smm-bundle"){
                    $request->session()->put('duplicate-product','one of the products in the January mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 1 && $row->name == "Jan 24 Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the January mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
                if($product->id == 26 && $row->name == "Jan 24 Mobility Reset + SMM Bundle"){
                    $request->session()->put('duplicate-product','the January mobility reset + smm bundle');
                    return redirect()->to('/basket');
                }
            }
            
    
            if(Auth::check()){
                $user = Auth::user();
                foreach($product->users as $u){
                    if($user->id == $u->id && $product->id != 4 && $product->id != 5){
                        $request->session()->put('already-purchased', $product->name);
                        return redirect()->to('/basket');
                    }
                }

                // Beginners Bundle
                if($product->id == 15){
                    if($user->products->contains('id', 1)){
                        $request->session()->put('already-purchased', 'a product in the Beginners Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 2)){
                        $request->session()->put('already-purchased', 'a product in the Beginners Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 3)){
                        $request->session()->put('already-purchased', 'a product in the Beginners Bundle');
                        return redirect()->to('/basket');
                    }
                }

                // Intermediate Bundle
                if($product->id == 20){
                    if($user->products->contains('id', 17)){
                        $request->session()->put('already-purchased', 'a product in the Intermediate Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 6)){
                        $request->session()->put('already-purchased', 'a product in the Intermediate Bundle');
                        return redirect()->to('/basket');
                    }
                }

                // Complete Bundle
                if($product->id == 21){
                    if($user->products->contains('id', 1)){
                        $request->session()->put('already-purchased', 'a product in the Complete Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 2)){
                        $request->session()->put('already-purchased', 'a product in the Complete Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 3)){
                        $request->session()->put('already-purchased', 'a product in the Complete Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 17)){
                        $request->session()->put('already-purchased', 'a product in the Complete Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 6)){
                        $request->session()->put('already-purchased', 'a product in the Complete Bundle');
                        return redirect()->to('/basket');
                    }
                }

                // Mobility Reset + SMM bundle checks
                if($product->id == 23){
                    if($user->products->contains('id', 1)){
                        $request->session()->put('already-purchased', 'a product in the Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 22)){
                        $request->session()->put('already-purchased', 'a product in the Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                }

                // Aug Mobility Reset + SMM bundle checks
                if($product->id == 29){
                    if($user->products->contains('id', 1)){
                        $request->session()->put('already-purchased', 'a product in the August Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 28)){
                        $request->session()->put('already-purchased', 'a product in the August Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                }

                // Aug Mobility Reset + SMM bundle checks
                if($product->id == 27){
                    if($user->products->contains('id', 1)){
                        $request->session()->put('already-purchased', 'a product in the January Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                    if($user->products->contains('id', 26)){
                        $request->session()->put('already-purchased', 'a product in the January Mobility Reset + SMM Bundle');
                        return redirect()->to('/basket');
                    }
                }
            }
    
            if ($request->session()->has('invoice_id')) {
                $invoice = Invoice::where('id', session('invoice_id'))->first();
            }else{
                $request->session()->forget('invoice_id');
                // Create invoice for user 
                $invoice = $this->createInvoice(NULL, NULL, 0, 0, NULL);
                $request->session()->put('invoice_id', $invoice->id);
            }
    
            $hasProduct = false;
            if($invoice != NULL){
                $hasProduct = $invoice->products()->where('id', $product->id)->exists();
            }else{
                $request->session()->forget('invoice_id');
                // Create invoice for user 
                $invoice = $this->createInvoice(NULL, NULL, 0, 0, NULL);
                $request->session()->put('invoice_id', $invoice->id);
            }
    
            if(!$hasProduct){
                // Attach group to invoice and add to basket
                if($product->sale_price != NULL){
                    $invoice->products()->attach($product->id, array('price' => $product->sale_price, 'qty' => 1));
                }else{
                    $invoice->products()->attach($product->id, array('price' => $product->price, 'qty' => 1));
                }
    
                $bundle = Setting::where('id',1)->first();
    
                if($bundle->status == "Active" && count($invoice->products) > 1){
                    foreach(Cart::content() as $i){
                        if($i->options->type != 'merch'){
                            Cart::remove($i->rowId);
                        }
                    }
                    foreach($invoice->products as $p){
                        if($p->sale_price != NULL){
                            $productprice = $p->sale_price - (($p->sale_price / 100) * $bundle->value);
                            $p->invoices()->updateExistingPivot($invoice->id, array('price' => $productprice));
                        }else{
                            $productprice = $p->price - (($p->price / 100) * $bundle->value);
                            $p->invoices()->updateExistingPivot($invoice->id, array('price' => $productprice));
                        }
                        Cart::add(['id' => $p->id, 'name' => $p->name, 'qty' => 1, 'price' => number_format((float)$productprice, 2, '.', ''), 'weight' => 0, 'options' => ['image'=>$p->image,'original_price' => $p->price, 'gift' => 'no']])->associate('App\Models\Product');
                    }
                }else{
                    if($product->sale_price != NULL){
                        $product->invoices()->updateExistingPivot($invoice->id, array('price' => $product->sale_price));
                        Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->sale_price, 'weight' => 0, 'options' => ['image'=>$product->image,'original_price' => $product->price, 'gift' => 'no']])->associate('App\Models\Product');
                    }else{
                        $product->invoices()->updateExistingPivot($invoice->id, array('price' => $product->price));
                        Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['image'=>$product->image, 'gift' => 'no']])->associate('App\Models\Product');
                    }
                }
    
                $this->updateTotal($invoice);
                
                $items = Cart::content();
                $tax_rate = 0;
                config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'
    
                foreach ($items as $item){
                    $item->setTaxRate($tax_rate);
                    Cart::update($item->rowId, $item->qty); 
                }
                return redirect()->to('/basket');
            }else{
                $request->session()->put('duplicate-product', $product->name);
            }
    
            return redirect()->to('/basket');
        }
    
        public function calculateShipping(Request $request){
    
            if(Auth::check()){
                // Validate the form data
                $user = Auth::user();
                $firstname = $user->first_name;
                $lastname = $user->last_name;
    
                if($request->input('same') == 'on'){
                    // Validate the form data
                    $this->validate($request,[
                        'streetAddress' => 'required|string|max:255|min:3',
                        'city' => 'required|string|max:255|min:3',
                        'postalCode' => 'required|string|max:255|min:3',
                        'country' => 'required|string|max:255|min:1'
                    ]);
                }else{
                    // Validate the form data
                    $this->validate($request,[
                        'streetAddress' => 'required|string|max:255|min:3',
                        'city' => 'required|string|max:255|min:3',
                        'postalCode' => 'required|string|max:255|min:3',
                        'country' => 'required|string|max:255|min:1',
                        'shipping-streetAddress' => 'required|string|max:255|min:3',
                        'shipping-city' => 'required|string|max:255|min:3',
                        'shipping-postalCode' => 'required|string|max:255|min:3',
                        'shipping-country' => 'required|string|max:255|min:1'
                    ]);
                }
    
            }else{
    
                if($request->input('same') == 'on'){
                    // Validate the form data
                    $this->validate($request,[
                        'first_name' => 'required|string|max:255',
                        'last_name' => 'required|string|max:255',
                        'streetAddress' => 'required|string|max:255|min:3',
                        'city' => 'required|string|max:255|min:3',
                        'postalCode' => 'required|string|max:255|min:3',
                        'country' => 'required|string|max:255|min:1'
                    ]);
                }else{
                    // Validate the form data
                    $this->validate($request,[
                        'first_name' => 'required|string|max:255',
                        'last_name' => 'required|string|max:255',
                        'streetAddress' => 'required|string|max:255|min:3',
                        'city' => 'required|string|max:255|min:1',
                        'postalCode' => 'required|string|max:255|min:3',
                        'country' => 'required|string|max:255|min:3',
                        'shipping-streetAddress' => 'required|string|max:255|min:3',
                        'shipping-city' => 'required|string|max:255|min:3',
                        'shipping-postalCode' => 'required|string|max:255|min:3',
                        'shipping-country' => 'required|string|max:255|min:1'
                    ]);
                }
                $firstname = $request->input('first_name');
                $lastname = $request->input('last_name');
            }
    
            $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
            $items = array();
            $cart = Cart::content();
            foreach ($cart as $item){
                $variant = Variant::where('id', $item->id)->first();
                $i = [
                    'external_variant_id' => $variant->printful,
                    'name' => $variant->merch->name,
                    'retail_price' => $variant->price, //Retail price for packing slip
                    'quantity' => $item->qty,
                ];
         
                array_push($items, $i);
            }
            if($request->input('same') == 'on'){
                $info = [
                    'recipient' => [
                        'name' => $firstname . ' ' . $lastname,
                        'address1' => $request->input('streetAddress'),
                        'city' => $request->input('city'),
                        'state_code' => $request->input('region'),
                        'country_code' => $request->input('country'),
                        'zip' => $request->input('postalCode'),
                    ],
                    'currency' => 'GBP',
                    'items' => $items,
                    'retail_costs' => [
                        'currency' => 'GBP',
                        'subtotal' => Cart::total(),
                    ],
                ];
            }else{
                $info = [
                    'recipient' => [
                        'name' => $firstname . ' ' . $lastname,
                        'address1' => $request->input('shipping-streetAddress'),
                        'city' => $request->input('shipping-city'),
                        'state_code' => $request->input('shipping-region'),
                        'country_code' => $request->input('shipping-country'),
                        'zip' => $request->input('shipping-postalCode'),
                    ],
                    'currency' => 'GBP',
                    'items' => $items,
                    'retail_costs' => [
                        'currency' => 'GBP',
                        'subtotal' => Cart::total(),
                    ],
                ];
            }
            $shipping = $pf->post('shipping/rates?store_id=1749903', $info);
            $result = json_decode(json_encode($shipping[0]), FALSE);
            $invoice = Invoice::where('id', session('invoice_id'))->first();
            $invoice->shipping = $result->rate;
            $invoice->save();
            return $shipping;
        }
    
        public function orderMerch($invoice){
            $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
            $items = array();
            $cart = Cart::content();
            $info = [];
            $user = Auth::user();
            $shippingAddress = Address::where('user_id', $user->id)->where('type','shipping')->first();
            foreach ($cart as $item){
                if($item->options->type == 'merch'){
                    $variant = Variant::where('id', $item->id)->first();
                    $i = [
                        'external_variant_id' => $variant->printful,
                        'name' => $variant->merch->name,
                        'retail_price' => $variant->price, //Retail price for packing slip
                        'quantity' => $item->qty,
                    ];
                    array_push($items, $i);
                }
            }

            $info = [
                'recipient' => [
                    'name' => $invoice->user->full_name,
                    'address1' => $shippingAddress->streetAddress,
                    'city' => $shippingAddress->city,
                    'state_code' => $shippingAddress->region,
                    'country_code' => $shippingAddress->country,
                    'zip' => $shippingAddress->postalCode,
                ],
                'items' => $items,
                'retail_costs' => [
                    'currency' => 'GBP',
                    'subtotal' => Cart::total(),
                ],
            ];
        
            
            
            $order = $pf->post('orders?store_id=1749903&confirm=1', $info);
            return $order;
        }
    
    public function getPrice(){
        return Cart::total();
    }
    
    public function applyVoucher(Request $request){
        
        $this->validate($request,[
            'voucher' => 'required|string|max:225'
        ]);
    
        $voucher = Voucher::where('code',$request->input('voucher'))->first();
        $invoice = Invoice::where('id', session('invoice_id'))->first();
    
        if ($voucher && session()->has('invoice_id')) {
    
                // Check if the voucher has expired
            $now = Carbon::now();
            $expires = Carbon::createFromFormat('Y-m-d H:i:s', $voucher->expiry_date);
            if($now > $expires){
                session(['voucher_notification' => 'Sorry this voucher has expired.']);
                return back();
            }
    
                // Update the invoice
            $newprice = $invoice->price - (($invoice->price / 100) * $voucher->percent);
            $invoice->price = number_format((float)$newprice, 2, '.', '');
            $invoice->save();
            $invoice->vouchers()->attach($voucher->id);
    
            // Update the cart items
            foreach(Cart::content() as $i){
    
                $p = Product::where('id',$i->id)->first();
                $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                $p->invoices()->updateExistingPivot($invoice->id, array('price' => $productprice));
                Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$p->image,'original_price' => $p->price, 'gift' => 'no']]);
    
            }
            
            session(['voucher_notification' => $voucher->percent . '% voucher applied.']);
            return back();
    
        }else{
            session(['voucher_notification' => 'Voucher not found.']);
            return back();
        }
    
    }
    
    public function updateTotal(Invoice $invoice){
        // $productsTotal = 0;
        // $variantsTotal = 0;
        // foreach($invoice->products as $p){
        //     if($p->sale_price != NULL){
        //         $productsTotal = $productsTotal + $p->sale_price;
        //     }else{
        //         $productsTotal = $productsTotal + $p->price;
        //     }
        // }
        // $bundle = Setting::where('id',1)->first();
    
        // if($bundle->status == "Active" && count($invoice->products) > 1){
        //     $productsTotal = $productsTotal - (($productsTotal / 100) * $bundle->value);
        // }
        // foreach($invoice->variants as $v){
        //     $variantsTotal = $variantsTotal + $v->price;
        // }
        // $combinedTotal = $productsTotal + $variantsTotal;
        $invoice->price = number_format((float)Cart::total(), 2, '.', '');
        $invoice->save();
    }
    

    // Function to create a random password
public function randomPassword() {
   $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
        return implode($pass); //turn the array into a string
    }

    // Function to create a user
    public function createUser($first_name, $last_name, $email, $country, $password, $customer_id){
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'country' => $country,
            'password' => Hash::make($password),
            'customer_id' => $customer_id,
            'role_id' => 2
        ]);

        Auth::login($user, true);
        return $user;

    }

    // Function to create an address
    public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $company, $type){

        if(Auth::check()){
            $currentAddresses = Auth::user()->addresses;
            foreach ($currentAddresses as $key => $a) {
                if($type == 'billing' && $a->type == 'billing'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else if($type == 'shipping' && $a->type == 'shipping'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else{
                    return 'same';
                }
            }
            if(count($currentAddresses) == 0){
                $address = Address::create([
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'city' => $city,
                    'postalCode' => $postalCode,
                    'country' => $country,
                    'region' => $region,
                    'company' => $company,
                    'type' => $type,
                    'user_id' => Auth::id()
                ]);
                return $address;
            }
        }else{
            $address = Address::create([
                'streetAddress' => $streetAddress,
                'extendedAddress' => $extendedAddress,
                'city' => $city,
                'postalCode' => $postalCode,
                'country' => $country,
                'region' => $region,
                'company' => $company,
                'type' => $type,
                'user_id' => Auth::id()
            ]);
            return $address;
        }
        
    }

    // Function to create a new order and add it to the user
    public function createOrder($printful_order, $status, $quantity, $variant_id, $invoice_id, $user_id){

        // Create a new order object
        $the_order = array(
            'printful_order' => $printful_order,
            'status' => $status,
            'quantity' => $quantity,
            'variant_id' => $variant_id,
            'invoice_id' => $invoice_id,
            'user_id' => $user_id
        );

        $order = new Order($the_order);
        $order->save();
        return $order;

    }

    // Function to create a new invoice and add it to the user
    public function createInvoice($customer_id, $transaction_id, $price, $paid, $paymentmethod){

        // Create a new invoice object
        $the_invoice = array(
            'customer_id' => $customer_id,
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
            'payment_method' => $paymentmethod,
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }


    // Function to send new account message
    public function accountCreated($user, $password){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.accountCreated',[
            'user' => $user,
            'to' => $to,
            'password' => $password
        ], function ($message) use ($user, $password, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Account Created');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function paymentConfirmed($invoice, $user){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.receipt',[
            'user' => $user,
            'to' => $to,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($to);
        });

        return "success";
    }

    // Function to send new account message
    public function videoCallPaymentConfirmed($invoice, $user){

        Mail::send('emails.receipt',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($user->email);
        });

        Mail::send('emails.bookVideoCall',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Book your video call with Tom Morrison');
            $message->to($user->email);
        });

        Mail::send('emails.videoCallPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('VIDEO CALL');
            $message->to('hello@tommorrison.uk');
        });

        return "success";
    }
    
    // Send first SMM follow up message
    public function smmPaymentConfirmed($invoice, $user){

        $user = User::whereId($user->id)->first();
        $user->follow_up = true;
        $user->save();

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.receipt',[
            'user' => $user,
            'to' => $to,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($to);
        });

        $subject = 'Welcome to SMM ' . $user->first_name . '! Here’s how to get started!';

        Mail::send('emails.smm.welcome',[
        'subject' => $subject,
        'to' => $to,
        'user' => $user
        ], function ($message) use ($subject, $user, $to){
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->replyTo('hello@tommorrison.uk');
                $message->to($to);
            }
        );

        return "success";
    }
    
    // Function to send new account message
    public function personalisedMobilityPlanPaymentConfirmed($invoice, $user){

        Mail::send('emails.receipt',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($user->email);
        });

        Mail::send('emails.PMPPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('PMP PROGRAM');
            $message->to('hello@tommorrison.uk');
        });

        return "success";
    }
}
