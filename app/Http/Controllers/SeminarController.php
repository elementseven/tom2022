<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seminar;
use Carbon\Carbon;

class SeminarController extends Controller
{
    // Return an array of matching seminars
    public function getSeminars(Request $request){

        $category = rawurldecode($request->input('category'));
        $limit = 100000;
        $now = Carbon::now();

        $seminars = Seminar::whereDate('start','>=',$now)->orderBy('start','asc')->paginate($limit);

        return $seminars;
    }
}
