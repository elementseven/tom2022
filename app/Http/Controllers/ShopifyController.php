<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ShopifyService;
use Log;

class ShopifyController extends Controller
{
    protected $shopifyService;

    public function __construct()
    {
        $this->shopifyService = new ShopifyService(
            env('SHOPIFY_MYSHOPIFY_DOMAIN'),
            env('SHOPIFY_ACCESS_TOKEN')  // Ensure this is the Storefront access token
        );
    }

    public function getPaginatedProducts(Request $request)
    {
        $cursor = $request->query('cursor', null); // Use 'cursor' instead of 'page'
        $products = $this->shopifyService->getPaginatedProducts($cursor, 12); // 12 products per page

        return response()->json($products);
    }


    public function index(){
        return view('shopify.index');
    }

    public function showProduct($id)
    {
        // Reconstruct the full Shopify GID
        $shopifyId = "gid://shopify/Product/$id";

        // Fetch the product details from Shopify using the full GID
        $product = $this->shopifyService->getProductById($shopifyId);

        if (!$product) {
            abort(404, 'Product not found');
        }

        return view('shopify.show', compact('product'));
    }

    public function cart()
    {
        return view('shopify.cart');
    }

    public function getProductsExcludingCart(Request $request)
    {
        $cartItems = $request->input('cartItems'); // Expecting an array of cart items (product IDs)
        $cartProductIds = collect($cartItems)->pluck('productId')->implode(',');

        $products = $this->shopifyService->getProductsExcludingCart($cartProductIds);

        return $products;
    }


}
