<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

use App\Models\Merch;
use App\Models\Variant;
use App\Models\Invoice;
use App\Models\Setting;

use Mail;
use Cart;
use Auth;

class PrintfulController extends Controller
{
    
    public function findSales(){
        if(Auth::user()->role->slug == 'admin'){
            $sales = Invoice::whereHas('variants')->whereDate('created_at','>=','2023-04-01')->with('user')->get();
            return view('printfulSales', compact('sales'));
        }else{
            return redirect()->to('/');
        }
    }

    public function handleWebhook(Request $request){

        $data = $request->json()->all();

        $subject = Str::title(str_replace('_', ' ', $data['type']));
        $name = $data['data']['order']['recipient']['name'];
        $email = $data['data']['order']['recipient']['email'];

        $order_status = $data['data']['order']['status'];
        $order_id = $data['data']['order']['external_id'];
        $items = $data['data']['order']['items'];
        
        if(isset($data['data']['shipment'])){
            $shipment = $data['data']['shipment']; 
        }else{
            $shipment = null;
        }

        Mail::send('emails.printful',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'order_status' => $order_status,
            'order_id' => $order_id,
            'shipment' => $shipment,
            'items' => $items
        ], function ($message) use ($subject, $email, $name, $order_status, $order_id, $items, $shipment){
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->replyTo('support@tommorrison.uk');
                $message->to($email);
            }
        );

        return response(['Message'=>'Webhook executed successfully'], 200);

    }

    private function getApiKey(){
        return env('PRINTFUL_TOKEN');
    }

  public function index(){
    // Replace this with your API key
    $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));

    $merch = Merch::orderBy('order','ASC')->where('status',NULL)->orWhere('status', 0)->whereHas('variants')->get();

    return view('merch.index', compact('merch'));
  }

  public function listProducts(){
    $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
    $products = $pf->get('/store/products?store_id=1749903');
    $products = $pf->get('/store/products/149153854?store_id=1749903');
    return $products;
  }
  
  public function show($slug){

        // Replace this with your API key
        $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));

        $merch = Merch::where('slug', $slug)->with('variants')->first();
        if($merch){
            try {
                $product = $pf->get('/store/products/'.$merch->printful . '?store_id=1749903');
                $merch->product = json_decode(json_encode($product), FALSE);
            }catch (PrintfulApiException $e) { //API response status code was not successful
                echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
            }catch (PrintfulException $e) { //API call failed
                echo 'Printful Exception: ' . $e->getMessage();
                var_export($pf->getLastResponseRaw());
            }
            return view('merch.show', compact('merch'));
        }else{
            abort(404);
        }
        
        
  }

  public function addProductToBasket(Request $request)
    {
        $variant = Variant::where('id', $request->input('variant'))->first();
        $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
        if($variant){
            $merch = Merch::where('id', $variant->merch->id)->with('variants')->first();

            if (session()->has('invoice_id')) {
                $invoice = Invoice::where('id', session('invoice_id'))->first();
            }else{
                $request->session()->forget('invoice_id');
                // Create invoice for user 
                $invoice = $this->createInvoice(NULL, NULL, 0, 0);
                $request->session()->put('invoice_id', $invoice->id);
            }

            $hasVarient = $invoice->variants()->where('id', $variant->id)->exists();

            if(!$hasVarient){
                // Attach group to invoice and add to basket
                $invoice->variants()->attach($variant->id, array('price' => $variant->price, 'qty' => 1));
            }

            try {
                $product = $pf->get('/store/products/'.$merch->printful . '?store_id=1749903');
                $merch->product = json_decode(json_encode($product), FALSE);
            } catch (PrintfulApiException $e) { //API response status code was not successful
                echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
            } catch (PrintfulException $e) { //API call failed
                echo 'Printful Exception: ' . $e->getMessage();
                var_export($pf->getLastResponseRaw());
            }

            if($hasVarient){
                $foundit = 0;
                foreach(Cart::content() as $i){
                    if($i->options->type == 'merch'){
                        if($i->id == $variant->id){
                            $qty = $i->qty;
                            $rowId = $i->rowId;
                            Cart::update($rowId, ($qty + 1));
                        }
                    }
                }
            }else{
                Cart::add(['id' => $variant->id, 'name' => $merch->name, 'qty' => $request->input('quantity'), 'price' => $variant->price, 'weight' => 0, 'options' => ['image'=>$merch->product->sync_product->thumbnail_url,'original_price' => $variant->price, 'size' => $variant->size,'type'=>"merch"]])->associate('App\Variant');
            }

            $this->updateTotal($invoice);
            
            $items = Cart::content();
            $tax_rate = 0;
            config( ['cart.tax' => $tax_rate] ); // change tax rate temporary'

            foreach ($items as $item){
                $item->setTaxRate($tax_rate);
                Cart::update($item->rowId, $item->qty); 
            }
        }
        
        return redirect()->to('/basket');
        
    }

    public function updateTotal(Invoice $invoice){
        // $productsTotal = 0;
        // $variantsTotal = 0;
        // foreach($invoice->products as $p){
        //     if($p->sale_price != NULL){
        //         $productsTotal = $productsTotal + $p->sale_price;
        //     }else{
        //         $productsTotal = $productsTotal + $p->price;
        //     }
        // }
        // $bundle = Setting::where('id',1)->first();

        // if($bundle->status == "Active" && count($invoice->products) > 1){
        //     $productsTotal = $productsTotal - (($productsTotal / 100) * $bundle->value);
        // }
        // foreach($invoice->variants as $v){
        //     $variantsTotal = $variantsTotal + $v->price;
        // }
        // $combinedTotal = $productsTotal + $variantsTotal;
        $invoice->price = number_format((float)Cart::total(), 2, '.', '');
        $invoice->save();
    }
    // Function to create a new invoice and add it to the user
    public function createInvoice($customer_id, $transaction_id, $price, $paid){

        // Create a new invoice object
        $the_invoice = array(
            'customer_id' => $customer_id,
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }

    public function remove(Request $request){
        $invoice = Invoice::where("id",session('invoice_id'))->first();
        if(session('invoice_id') != NULL){
            Cart::remove($request->input('rowId'));
            $invoice->variants()->detach($request->input('id'));
        }
        if(count($invoice->products) == 0 && count($invoice->variants) == 0){
            Cart::destroy();
            $invoice->delete();
            session()->forget('invoice_id');
        }
        return redirect()->to("/basket");
    }
}
