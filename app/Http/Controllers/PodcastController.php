<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Podcast;
use Session;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('podcast.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function fetch(Request $request, $limit)
    {
        Session::forget('saved-search-podcast');
        $podcasts = Podcast::orderBy('episode',$request->input('sort'))->paginate($limit);

        foreach($podcasts as $p){
          $p->image = $p->getFirstMediaUrl('podcasts', 'large');
          $p->webp = $p->getFirstMediaUrl('podcasts', 'large-webp');
          $p->mimetype = $p->getFirstMedia('podcasts')->mime_type;
        }

        Session::put('saved-search-podcast', $request->search);
        Session::save();
        return $podcasts;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Podcast $podcast)
    {
        // Add media URLs to products
        foreach ($podcast->products as $p) {
            $p->image = $p->getFirstMediaUrl('products', 'normal');
            $p->webp = $p->getFirstMediaUrl('products', 'normal-webp');
            $p->mimetype = $p->getFirstMedia('products')->mime_type;
        }

        // Get the previous podcast
        $previous = Podcast::where('date', '<', $podcast->date)
                            ->orderBy('date', 'desc')
                            ->first();

        // Get the next podcast
        $next = Podcast::where('date', '>', $podcast->date)
                       ->orderBy('date', 'asc')
                       ->first();

        return view('podcast.show')->with([
            'podcast' => $podcast,
            'previous' => $previous,
            'next' => $next,
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
