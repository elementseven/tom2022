<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

use App\Models\Category;
use App\Models\Voucher;
use App\Models\Upgrade;
use App\Models\Variant;
use App\Models\Address;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Setting;
use App\Models\Review;
use App\Models\Bundle;
use App\Models\Order;
use App\Models\Merch;
use App\Models\User;

use Srmklive\PayPal\Services\PayPal as PayPalClient;

use App\Helpers\CartService;

use Spatie\Newsletter\Facades\Newsletter;
use Session;
use Stripe;
use Stripe\PaymentIntent;
use Cart;
use Mail;
use Auth;

class ShopController extends Controller
{   

    protected $provider;

    private function getApiKey(){
        return env('PRINTFUL_TOKEN');
    }

    public function updateAddress(){
        return view('shop.updateAddress');
    }

    public function subscribeToMailingList(){

        $user = Auth::user();
        $tags = array();

        if(Auth::check()){
            foreach($user->products as $p){
                if($p->mailchimptag != null){
                    array_push($tags, $p->mailchimptag);
                }
            }
            Newsletter::subscribeOrUpdate(
                $user->email, 
                [
                    'first_name' => $user->first_name, 
                    'last_name' => $user->last_name
                ],
                'subscribers',
                ['tags'=> $tags]
            );
        }
        
        return response()->json(['success' => 'success', 200]);

    }

    public function saveAddress(Request $request){
        
        if($request->input('same') == 'on'){
            // Validate the form data
            $this->validate($request,[
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1'
            ]);
        }else{
            // Validate the form data
            $this->validate($request,[
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1',
                'shipping-streetAddress' => 'required|string|max:255|min:3',
                'shipping-city' => 'required|string|max:255|min:3',
                'shipping-postalCode' => 'required|string|max:255|min:3',
                'shipping-country' => 'required|string|max:255|min:1'
            ]);
        }

        $user = Auth::user();

        if(count($user->addresses)){
            foreach($user->addresses as $address){
                $address->delete();
            }
        }

        // Create user address(es)
        $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'billing');
    
        if($request->input('same') == 'on'){
            $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'shipping');
        }else{
            $shippingAddress = $this->createAddress($request->input('shipping-streetAddress'), $request->input('shipping-extendedAddress'),$request->input('shipping-city'), $request->input('shipping-postalCode'), $request->input('shipping-country'), $request->input('shipping-region'), $request->input('shipping-company'), 'shipping');
        }

        $this->createStripeCustomer($user);
        
        return redirect()->to('/checkout');

    }

    /**
     * process transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function processTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $paypalToken = $provider->getAccessToken();

        $items = Array();

        foreach(Cart::content() as $cc){
            if($cc->options->type == "bundle"){
                $bundle = Bundle::where('id', $cc->id)->first();

                foreach($bundle->products as $p){
                    $item = Array(
                        "name" => $p->name,
                        "unit_amount" => Array("value" => number_format((float) $p->pivot->price, 2, '.', ''), "currency_code" => 'GBP'),
                        "quantity" => 1
                    );

                    array_push($items, $item);
                }
                
            }else if($cc->options->type == "upgrade"){
                $upgrade = Upgrade::where('id', $cc->id)->first();

                foreach($upgrade->products as $p){
                    $item = Array(
                        "name" => $p->name,
                        "unit_amount" => Array("value" => number_format((float) $p->pivot->price, 2, '.', ''), "currency_code" => 'GBP'),
                        "quantity" => 1
                    );

                    array_push($items, $item);
                }
                
            }else{
                $item = Array(
                    "name" => $cc->name,
                    "unit_amount" => Array("value" => number_format((float) $cc->price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => $cc->qty
                );
                array_push($items, $item);
            }
        }

        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('successTransaction'),
                "cancel_url" => route('cancelTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "GBP",
                        "value" => number_format((float)Cart::total(), 2, '.', ''),
                        "breakdown" => Array( "item_total" => Array( "value" => number_format((float)Cart::total(), 2, '.', ''), "currency_code" => 'GBP') ),
                    ],
                    "items" => $items
                ]
            ]
        ]);

        if (isset($response['id']) && $response['id'] != null) {

            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }

            return redirect()
                ->route('checkout')
                ->with('errors', 'Something went wrong.');

        }else {
            if(isset($response['message'])){
                session(['payment_error' => $response['message']]);
                return redirect()
                ->route('checkout')
                ->with('errors', $response['message'] ?? 'Something went wrong.');
            }else{
                session(['payment_error' => 'Something went wrong.']);
                return redirect()
                ->route('checkout')
                ->with('errors', 'Something went wrong.');
            }   
        }
    }

    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request){

        $errors = null;

        if(Auth::check()){
            if(count(Auth::user()->addresses) < 2){
                return redirect()->to('/update-address');
            }
            if(count(Cart::content())){
                foreach(Cart::content() as $row){
                if($row->options->gift == "yes"){
                        return redirect()->to('/gift-checkout');
                    }
                }
                if (session()->has('intent')) {
                    $intent = session('intent');  
                    Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key

                    try {
                        $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the intent
                        $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                        $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the intent
                    } catch (\Exception $e) {
                        return 'error Failed to cancel the payment intent: ' . $e->getMessage();
                    }

                }

                $intent = $this->getIntent();
                session(['intent' => $intent]);

                return view('shop.checkout',compact('intent','errors'));
            }else{
                return redirect()->to('/basket');
            }
            
        }else{
            return view('shop.create-account');
        }
        
    }


    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function successTransaction(Request $request)
    {

        $invoice = Invoice::where('transaction_id', $request['token'])->first();
        if ($invoice) {
            // Check for and offer upgrades
            $upgrade = $this->offerUpgrade($invoice);
        
            return view('shop.success')->with(['invoice' => $invoice, 'upgrade' => $upgrade]);

        }else{

            $provider = new PayPalClient;
            $provider->setApiCredentials(config('paypal'));
            $provider->setCurrency('GBP');
            $provider->getAccessToken();
            $response = $provider->capturePaymentOrder($request['token']);
            $user = Auth::user();
            if (isset($response['status']) && $response['status'] == 'COMPLETED') {

                return $this->success($user, 'PayPal', $response['id']);

            }else if(isset($response['message']) && str_contains($response['message'], '"issue":"ORDER_ALREADY_CAPTURED"')){

                return redirect()->to('/login');

            }else {
                return redirect()->route('checkout-error')->withErrors(['error' => $response['message'] ?? 'Something went wrong.']);
            }
        }
    }

    /**
     * Error capture.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkoutError(Request $request){
        return view('shop.checkouterror');
    }

    /**
     * cancel transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelTransaction(Request $request)
    {
        return redirect()
            ->route('checkout')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }


    public function getIntent(){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();

        $total = Cart::total(2,'.','') * 100;

        if($user->stripe_id == null){
            $this->createStripeCustomer($user);
        }

        $products = Array();

        foreach(Cart::content() as $item){
            if($item->options->type == 'bundle'){
                $bundle = Bundle::where('id', $item->id)->first();
                foreach($bundle->products as $p){
                    $productdata = \Stripe\InvoiceItem::create([
                        'customer' => Auth::user()->stripe_id,
                        'description' => $p->name,
                        'quantity' => 1,
                        'price_data' => [
                            'currency' => 'gbp',
                            'product' => $p->stripe_id,
                            'unit_amount_decimal' => $p->pivot->price * 100
                        ]
                    ]);
                }
            }else if($item->options->type == 'upgrade'){
                $upgrade = Upgrade::where('id', $item->id)->first();
                foreach($upgrade->products as $p){
                    $productdata = \Stripe\InvoiceItem::create([
                        'customer' => Auth::user()->stripe_id,
                        'description' => $p->name,
                        'quantity' => 1,
                        'price_data' => [
                            'currency' => 'gbp',
                            'product' => $p->stripe_id,
                            'unit_amount_decimal' => $p->pivot->price * 100
                        ]
                    ]);
                }
            }else if($item->options->type == 'product'){
                $product = Product::where('id', $item->id)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $product->name,
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product->stripe_id,
                        'unit_amount_decimal' => $item->price * 100
                    ]
                ]);
            }else if($item->options->type == 'merch'){
                $variant = Variant::where('id', $item->id)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $variant->merch->name,
                    'quantity' => $item->qty,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $variant->merch->stripe_id,
                        'unit_amount_decimal' => $item->price * 100
                    ]
                ]);
            }
        }

        $stripeinvoice = \Stripe\Invoice::create([
            'customer' => Auth::user()->stripe_id,
            'statement_descriptor' => 'Tom Morrison'
        ]);
        $stripeinvoiceid = $stripeinvoice->id;
        $finalized = $stripeinvoice->finalizeInvoice();
        $intent = \Stripe\PaymentIntent::Retrieve($finalized->payment_intent);

        return $intent;
    }

    public function createStripeCustomer($user){
        $user = Auth::user();
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $billingAddress = Address::where('user_id', $user->id)->where('type','billing')->first();
        $stripecustomer = \Stripe\Customer::create([
            'address' => [
                'city' => $billingAddress->city,
                'country' => $billingAddress->country,
                'line1' => $billingAddress->streetAddress,
                'line2' => $billingAddress->extendedAddress,
                'postal_code' => $billingAddress->postalCode,
                'state' => $billingAddress->region,
            ],
            'email' => $user->email,
            'name' => $user->full_name,
        ]);
        $user->stripe_id = $stripecustomer->id;
        $user->save();
        return $user;
    }

    public function index(){
        $categories = Category::whereHas('products')->get();
        $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->get();
        return view('shop.index', compact('categories','products'));
    }

    public function newShop(){
        $categories = Category::whereHas('products')->get();
        $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->get();
        return view('shop.shop-new', compact('categories','products'));
    }

    public function createAccount(Request $request){
        
        if($request->input('same') == 'on'){
            // Validate the form data
            $this->validate($request,[ 
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users|confirmed',
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'agree' => 'accepted',
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1'
            ]);
        }else{
            // Validate the form data
            $this->validate($request,[
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users|confirmed',
                'agree' => 'accepted',
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'streetAddress' => 'required|string|max:255|min:3',
                'city' => 'required|string|max:255|min:3',
                'postalCode' => 'required|string|max:255|min:3',
                'country' => 'required|string|max:255|min:1',
                'shipping-streetAddress' => 'required|string|max:255|min:3',
                'shipping-city' => 'required|string|max:255|min:3',
                'shipping-postalCode' => 'required|string|max:255|min:3',
                'shipping-country' => 'required|string|max:255|min:1'
            ]);
        }

        // Generate Password
        $password = $request->input('password');

        // Create User
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'password' => Hash::make($password),
            'country' => $request->input('country'),
            'password' => Hash::make($password),
            'role_id' => 2
        ]);

        // Login User
        Auth::login($user, true);

        // Send account created email
        $this->accountCreated($user, $password);

        // Create user address(es)
        $billingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'billing');
    
        if($request->input('same') == 'on'){
            $shippingAddress = $this->createAddress($request->input('streetAddress'), $request->input('extendedAddress'),$request->input('city'), $request->input('postalCode'), $request->input('country'), $request->input('region'), $request->input('company'), 'shipping');
        }else{
            $shippingAddress = $this->createAddress($request->input('shipping-streetAddress'), $request->input('shipping-extendedAddress'),$request->input('shipping-city'), $request->input('shipping-postalCode'), $request->input('shipping-country'), $request->input('shipping-region'), $request->input('shipping-company'), 'shipping');
        }

        $request->session()->put('account-created', $user->first_name);

        $this->createStripeCustomer($user);

        return redirect()->to('/checkout');

    }

    public function paymentStatus(Request $request){

        $user = Auth::user();

        if($request->query('payment_intent')){
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $clientsecret = $request->query('payment_intent_client_secret');
            $intent = \Stripe\PaymentIntent::retrieve($request->query('payment_intent'));
            if($intent->status == 'succeeded'){
                return $this->success($user, 'Card', $intent->id);
            }else{
                // Error with the stripe payment
                session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
                return redirect()->to('/checkout');

            }
        }else{
            // Error with the stripe payment
            session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
            return redirect()->to('/checkout');
        }
    }
    
    public function success($user, $paymentmethod, $transaction_id)
    {
        $checkifpaid = Invoice::where('transaction_id', $transaction_id)->first();

        if (is_null($checkifpaid)) {
            // Create the invoice
            $invoice = $this->createInvoice(null, $transaction_id, Cart::total(), true, $paymentmethod, $user->id);

            // Attach products to user account
            foreach (Cart::content() as $i) {
                if ($i->options->type == 'bundle') {
                    $bundle = Bundle::find($i->id);
                    foreach ($bundle->products as $p) {
                        if (!$user->products()->where('products.id', $p->id)->exists()) {
                            $user->products()->attach($p->id);
                        }
                    }
                    $invoice->bundles()->attach($i->id, ['price' => $i->price]);
                } elseif ($i->options->type == 'upgrade') {
                    $upgrade = Upgrade::find($i->id);
                    foreach ($upgrade->products as $p) {
                        if (!$user->products()->where('products.id', $p->id)->exists()) {
                            $user->products()->attach($p->id);
                        }
                    }
                    $invoice->upgrades()->attach($i->id, ['price' => $i->price]);
                } elseif ($i->options->type == 'product') {
                    if ($i->id != 4 && $i->id != 5) {
                        $user->products()->attach($i->id);
                    }
                    $invoice->products()->attach($i->id, ['price' => $i->price, 'qty' => 1]);
                } elseif ($i->options->type == 'merch') {
                    $invoice->variants()->attach($i->id, ['price' => $i->price, 'qty' => $i->qty]);

                    $orderresult = $this->orderMerch($invoice);
                    $object = json_decode(json_encode($orderresult), false);
                    if ($object->status == 'failed') {
                        session(['general_payment_error' => "There was an error ordering your merchandise, please try again later."]);
                        return back();
                    }

                    // Create a new merchandise order record for each variant
                    $variant = Variant::find($i->id);
                    $order = $this->createOrder($object->id, $object->status, $i->qty, $variant->id, $invoice->id, $user->id);
                }
            }

            if ($invoice->products->contains(4)) {
                // Send payment confirmation email for PMP Program
                $this->personalisedMobilityPlanPaymentConfirmed($invoice, $user);
            } elseif ($invoice->products->contains(1)) {
                // Send payment confirmation email for SMM
                $this->smmPaymentConfirmed($invoice, $user);
            } elseif ($invoice->products->contains(5)) {
                // Send payment confirmation email for Video Call
                $this->videoCallPaymentConfirmed($invoice, $user);
            } else {
                // Send payment confirmation email
                $this->paymentConfirmed($invoice, $user);
            }

            if (session()->has('voucher_code')) {
                $voucher_code = session('voucher_code');  
                $voucher = Voucher::where('code', $voucher_code)->first();
                if ($voucher) {
                    $invoice->vouchers()->attach($voucher->id);
                }
                session()->forget('voucher_code');
                session()->forget('voucher_notification');
            }

            Cart::destroy();
            session()->forget('intent');
            session()->forget('invoice_id');
            session(['successful_payment' => $invoice->id]);

        } else {
            $invoice = $checkifpaid;
        }

        // Check for and offer upgrades
        $upgrade = $this->offerUpgrade($invoice);


        // Show success page
        $this->subscribeToMailingList();
        return view('shop.success')->with(['invoice' => $invoice, 'upgrade' => $upgrade]);
    }

    private function offerUpgrade(Invoice $invoice){
        if(count($invoice->upgrades) == 0){
            // Get all product IDs and bundle IDs from the invoice
            $productIds = $invoice->products->pluck('id')->toArray();
            $bundleIds = $invoice->bundles->pluck('id')->toArray();

            // Fetch the upgrade with the highest sale price
            $upgrade = Upgrade::whereIn('product_id', $productIds)
                ->orWhereIn('bundle_id', $bundleIds)
                ->orderBy('sale_price', 'desc')
                ->first();

            // Check if upgrade exists
            if ($upgrade) {
                return $upgrade;
            } else {
                return null;
            }
        }else{
            return null;
        }

    }

    
    public function getProducts(Request $request, $category, $limit){
        if($category == 'all'){
            if($request->input('featured')){
                $products = Product::where([['status','Available'],['featured', true]])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
            }else{
                $products = Product::where('status','Available')->orderBy('created_at','desc')->with('category')->paginate($limit);
            }
        }else if($category == 'level-one'){
            $products = Product::whereIn('id',[1,2,3])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-two'){
            $products = Product::whereIn('id',[17])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-three'){
            $products = Product::whereIn('id',[6])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-four'){
            $products = Product::whereIn('id',[7,8])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'level-five'){
            $products = Product::whereIn('id',[5,4])->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else if($category == 'online-camps'){
            $products = Product::where('category_id',13)->where([['status','Available']])->orderBy('created_at','desc')->with('category')->paginate($limit,['id','name', 'slug', 'short','price','sale_price']);
        }else{
            $products = Product::where('status','Available')->orderBy('created_at','desc')->whereHas('category', function($q) use($category){
                $q->where('slug', $category);
            })->with('category')->paginate($limit);
        }
        foreach($products as $p){
            $p->image = $p->getFirstMediaUrl('products', 'normal');
            $p->webp = $p->getFirstMediaUrl('products', 'normal-webp');
            $p->large = $p->getFirstMediaUrl('products', 'large');
            $p->largewebp = $p->getFirstMediaUrl('products', 'large-webp');
            $p->mimetype = $p->getFirstMedia('products')->mime_type;
        }
        return $products;
    }

    public function getBundles(){
        $bundles = Bundle::whereIn('slug',['beginners-bundle','intermediate-bundle','complete-bundle'])->orderBy('created_at','asc')->get(['id','title', 'slug', 'short','price','sale_price']);

        foreach($bundles as $b){
            $b->percent = number_format((($b->price - $b->sale_price)*100) /$b->price, 0);
        }

        return $bundles;
    }
    
    public function product($slug){
        $product = Product::where('slug', $slug)->first();
        if($product){

            $reviews = Review::where('product_id', $product->id)->whereNotNull('avatar')->orderBy('created_at', 'desc')->paginate(6);
            foreach($reviews as $r){
              $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
              $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
              $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
            }

            return view('shop.product', compact('product','reviews'));
        }else{
            abort(404);
        }
    }
    public function bundle($slug){

        $bundle = Bundle::where('slug', $slug)->first();
        if($bundle){
            $productIds = $bundle->products()->pluck('products.id')->toArray();
            $reviews = Review::where('product_id', $productIds)->whereNotNull('avatar')->orderBy('created_at', 'desc')->paginate(6);
            foreach($reviews as $r){
              $r->avatar = $r->getFirstMediaUrl('reviews', 'normal');
              $r->normalwebp = $r->getFirstMediaUrl('reviews', 'normal-webp');
              $r->mimetype = $r->getFirstMedia('reviews')->mime_type;
            }
            return view('shop.bundle', compact('bundle','reviews'));
        }else{
            abort(404);
        }
    }
    
    public function basket(){
        $now = Carbon::now();
        $showvoucher = Voucher::where('expiry_date','>=',$now)->count();
        $inbasket = [];
        foreach(Cart::content() as $i){
            array_push($inbasket, $i->id);
        }
        if(count(Cart::content()) > 0){
            if (in_array(15, $inbasket)){
                $products = Product::whereIn('id', [8,7,6,5,17])->orderBy('created_at', 'desc')->paginate(5);
            }else{
                $products = Product::where('id','!=', [$inbasket])->paginate(5);
            }
        }else{
            $products = Product::paginate(5);
        }
        return view('shop.basket',compact('products','showvoucher'));
    }
    
    public function emptyBasket(){

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));  // Set the Stripe API key

        if (session()->has('intent')) {
            $intent = session('intent');  
            try {
                $cancelintent = \Stripe\PaymentIntent::retrieve($intent->id);  // Retrieve the invoice
                $cancelinvoice = \Stripe\Invoice::retrieve($cancelintent->invoice);
                $canceledinvoice = $cancelinvoice->voidInvoice();  // Cancel the invoice
                session()->forget('intent');
            } catch (\Exception $e) {
                session()->forget('intent');
            }

        }
        
        Cart::destroy();

        session()->forget('voucher_code');
        session()->forget('voucher_notification');

        return redirect()->to("/basket");
    }
    
    public function remove(Request $request){
        
        $request->validate([
            'rowId' => 'required|string'
        ]);

        $rowId = $request->input('rowId');
        $hasRowId = $this->cartHasRowId($rowId);

        if ($hasRowId) {
            Cart::remove($rowId);
        }else{
            return redirect()->to('/empty-basket');
        }

        if (Cart::count() == 0) {
            return redirect()->to('/empty-basket');
        }

        return redirect()->to("/basket");

    }
    
    
    public function addProductToBasket(Request $request, Product $product)
    {
        foreach(Cart::content() as $row){
            if($row->options->gift == "yes"){
                $request->session()->put('gift-still-in-basket', true);
                return redirect()->to('/gift-basket');
            }
        }

        $cartService = new CartService();
        $userId = auth()->id(); 

        // Check if the product is already in the cart or if it is redundant
        if ($cartService->isItemInCart($product->id, 'product')) {
            return redirect()->to('/basket')->with('error', 'This product is already in your basket.');
        }

        $redundancyMessage = $cartService->checkPurchaseRedundancy($userId, $product->id, 'product');
        if (strpos($redundancyMessage, 'can be added') === false) {
            return redirect()->to('/basket')->with('error', $redundancyMessage);
        }

        if($product->sale_price != NULL){
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->sale_price, 'weight' => 0, 'options' => ['type' => 'product', 'image'=>$product->image,'original_price' => $product->price, 'gift' => 'no']])->associate('App\Models\Product');
        }else{
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['type' => 'product', 'image'=>$product->image, 'gift' => 'no']])->associate('App\Models\Product');
        }

        return redirect()->to('/basket')->with('success', $product->name . ' added to your basket');
   
    }

    public function addBundleToBasket(Request $request, Bundle $bundle)
    {
        foreach(Cart::content() as $row){
            if($row->options->gift == "yes"){
                $request->session()->put('gift-still-in-basket', true);
                return redirect()->to('/gift-basket');
            }
        }

        $cartService = new CartService();
        $userId = auth()->id(); 

        // Check if the bundle is already in the cart or if it is redundant
        if ($cartService->isItemInCart($bundle->id, 'bundle')) {
            return redirect()->to('/basket')->with('error', 'This bundle is already in your basket.');
        }

        $redundancyMessage = $cartService->checkPurchaseRedundancy($userId, $bundle->id, 'bundle');
        if (strpos($redundancyMessage, 'can be added') === false) {
            return redirect()->to('/basket')->with('error', $redundancyMessage);
        }


        // Determine if item is on sale and adjust price accordingly
        $price = $bundle->sale_price ?? $bundle->price;

        // Add the item to the cart
        Cart::add([
            'id' => $bundle->id,
            'name' => $bundle->title,
            'qty' => 1,
            'price' => $price,
            'weight' => 0,
            'options' => ['type' => 'bundle', 'original_price' => $bundle->price, 'image'=> $bundle->getFirstMediaUrl('bundles', 'thumbnail'), 'sale_price' => $bundle->sale_price, 'gift' => 'no']
        ])->associate('App\Models\Bundle');


        return redirect()->to('/basket')->with('success', $bundle->name . ' added to your basket');
   
    }

    public function addUpgradeToBasket(Request $request, Upgrade $upgrade, $slug)
    {
        foreach(Cart::content() as $row){
            if($row->options->gift == "yes"){
                $request->session()->put('gift-still-in-basket', true);
                return redirect()->to('/gift-basket');
            }
        }

        $cartService = new CartService();
        $userId = auth()->id(); 

        // Check if the bundle is already in the cart or if it is redundant
        if ($cartService->isItemInCart($upgrade->id, 'upgrade')) {
            return redirect()->to('/basket')->with('error', 'This upgrade is already in your basket.');
        }

        $redundancyMessage = $cartService->checkPurchaseRedundancy($userId, $upgrade->id, 'upgrade');
        if (strpos($redundancyMessage, 'can be added') === false) {
            return redirect()->to('/basket')->with('error', $redundancyMessage);
        }


        // Determine if item is on sale and adjust price accordingly
        $price = $upgrade->sale_price ?? $upgrade->price;

        // Add the item to the cart
        Cart::add([
            'id' => $upgrade->id,
            'name' => $upgrade->title,
            'qty' => 1,
            'price' => $price,
            'weight' => 0,
            'options' => ['type' => 'upgrade', 'original_price' => $upgrade->price, 'image'=> $upgrade->getFirstMediaUrl('upgrades', 'thumbnail'), 'sale_price' => $upgrade->sale_price, 'gift' => 'no']
        ])->associate('App\Models\Upgrade');


        return redirect()->to('/checkout')->with('success', $upgrade->name . ' added to your basket');
   
    }

    public function calculateShipping(Request $request){

        if(Auth::check()){
            // Validate the form data
            $user = Auth::user();
            $firstname = $user->first_name;
            $lastname = $user->last_name;

            if($request->input('same') == 'on'){
                // Validate the form data
                $this->validate($request,[
                    'streetAddress' => 'required|string|max:255|min:3',
                    'city' => 'required|string|max:255|min:3',
                    'postalCode' => 'required|string|max:255|min:3',
                    'country' => 'required|string|max:255|min:1'
                ]);
            }else{
                // Validate the form data
                $this->validate($request,[
                    'streetAddress' => 'required|string|max:255|min:3',
                    'city' => 'required|string|max:255|min:3',
                    'postalCode' => 'required|string|max:255|min:3',
                    'country' => 'required|string|max:255|min:1',
                    'shipping-streetAddress' => 'required|string|max:255|min:3',
                    'shipping-city' => 'required|string|max:255|min:3',
                    'shipping-postalCode' => 'required|string|max:255|min:3',
                    'shipping-country' => 'required|string|max:255|min:1'
                ]);
            }

        }else{

            if($request->input('same') == 'on'){
                // Validate the form data
                $this->validate($request,[
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'streetAddress' => 'required|string|max:255|min:3',
                    'city' => 'required|string|max:255|min:3',
                    'postalCode' => 'required|string|max:255|min:3',
                    'country' => 'required|string|max:255|min:1'
                ]);
            }else{
                // Validate the form data
                $this->validate($request,[
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'streetAddress' => 'required|string|max:255|min:3',
                    'city' => 'required|string|max:255|min:1',
                    'postalCode' => 'required|string|max:255|min:3',
                    'country' => 'required|string|max:255|min:3',
                    'shipping-streetAddress' => 'required|string|max:255|min:3',
                    'shipping-city' => 'required|string|max:255|min:3',
                    'shipping-postalCode' => 'required|string|max:255|min:3',
                    'shipping-country' => 'required|string|max:255|min:1'
                ]);
            }
            $firstname = $request->input('first_name');
            $lastname = $request->input('last_name');
        }

        $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
        $items = array();
        $cart = Cart::content();
        foreach ($cart as $item){
            $variant = Variant::where('id', $item->id)->first();
            $i = [
                'external_variant_id' => $variant->printful,
                'name' => $variant->merch->name,
                'retail_price' => $variant->price, //Retail price for packing slip
                'quantity' => $item->qty,
            ];
     
            array_push($items, $i);
        }
        if($request->input('same') == 'on'){
            $info = [
                'recipient' => [
                    'name' => $firstname . ' ' . $lastname,
                    'address1' => $request->input('streetAddress'),
                    'city' => $request->input('city'),
                    'state_code' => $request->input('region'),
                    'country_code' => $request->input('country'),
                    'zip' => $request->input('postalCode'),
                ],
                'currency' => 'GBP',
                'items' => $items,
                'retail_costs' => [
                    'currency' => 'GBP',
                    'subtotal' => Cart::total(),
                ],
            ];
        }else{
            $info = [
                'recipient' => [
                    'name' => $firstname . ' ' . $lastname,
                    'address1' => $request->input('shipping-streetAddress'),
                    'city' => $request->input('shipping-city'),
                    'state_code' => $request->input('shipping-region'),
                    'country_code' => $request->input('shipping-country'),
                    'zip' => $request->input('shipping-postalCode'),
                ],
                'currency' => 'GBP',
                'items' => $items,
                'retail_costs' => [
                    'currency' => 'GBP',
                    'subtotal' => Cart::total(),
                ],
            ];
        }
        $shipping = $pf->post('shipping/rates?store_id=1749903', $info);
        $result = json_decode(json_encode($shipping[0]), FALSE);
        $invoice = Invoice::where('id', session('invoice_id'))->first();
        $invoice->shipping = $result->rate;
        $invoice->save();
        return $shipping;
    }

    public function orderMerch($invoice){
        $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
        $items = array();
        $cart = Cart::content();
        $info = [];
        $user = Auth::user();
        $shippingAddress = Address::where('user_id', $user->id)->where('type','shipping')->first();
        foreach ($cart as $item){
            if($item->options->type == 'merch'){
                $variant = Variant::where('id', $item->id)->first();
                $i = [
                    'external_variant_id' => $variant->printful,
                    'name' => $variant->merch->name,
                    'retail_price' => $variant->price, //Retail price for packing slip
                    'quantity' => $item->qty,
                ];
                array_push($items, $i);
            }
        }

        $info = [
            'recipient' => [
                'name' => $invoice->user->full_name,
                'address1' => $shippingAddress->streetAddress,
                'city' => $shippingAddress->city,
                'state_code' => $shippingAddress->region,
                'country_code' => $shippingAddress->country,
                'zip' => $shippingAddress->postalCode,
            ],
            'items' => $items,
            'retail_costs' => [
                'currency' => 'GBP',
                'subtotal' => Cart::total(),
            ],
        ];
    
        
        
        $order = $pf->post('orders?store_id=1749903&confirm=1', $info);
        return $order;
    }
    
    
    public function applyVoucher(Request $request){
        
        $this->validate($request,[
            'voucher' => 'required|string|max:225'
        ]);
    
        $voucher = Voucher::where('code',$request->input('voucher'))->first();
    
        if ($voucher) {
    
            // Check if the voucher has expired
            $now = Carbon::now();
            $expires = Carbon::createFromFormat('Y-m-d H:i:s', $voucher->expiry_date);
            if($now > $expires){
                session(['voucher_notification' => 'Sorry this voucher has expired.']);
                return back();
            }

            // Update the cart items
            $items = Cart::content();
            foreach ($items as $i) {
                if($i->options->type == 'product'){
                    $p = Product::where('id',$i->id)->first();
                    $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                    Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['type' => 'product', 'image' => $p->image,'original_price' => $p->price, 'gift' => 'no']]);
                }else if($i->options->type == 'bundle'){
                    $b = Bundle::where('id',$i->id)->first();
                    $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                    Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['type' => 'bundle', 'image' => $b->image,'original_price' => $b->price, 'gift' => 'no']]);
                }else if($i->options->type == 'upgrade'){
                    $u = Upgrade::where('id',$i->id)->first();
                    $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                    Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['type' => 'upgrade', 'image' => $u->image,'original_price' => $u->price, 'gift' => 'no']]);
                }
            }
            session(['voucher_code' => $voucher->code]);
            session(['voucher_notification' => $voucher->percent . '% voucher applied.']);
            return back();
    
        }else{
            session(['voucher_notification' => 'Voucher not found.']);
            return back();
        }
    
    }


    // Function to create a random password
    public function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
        return implode($pass); //turn the array into a string
    }

    // Function to create a user
    public function createUser($first_name, $last_name, $email, $country, $password, $customer_id){
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'country' => $country,
            'password' => Hash::make($password),
            'customer_id' => $customer_id,
            'role_id' => 2
        ]);

        Auth::login($user, true);
        return $user;

    }

    // Function to create an address
    public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $company, $type){

        if(Auth::check()){
            $currentAddresses = Auth::user()->addresses;
            foreach ($currentAddresses as $key => $a) {
                if($type == 'billing' && $a->type == 'billing'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else if($type == 'shipping' && $a->type == 'shipping'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else{
                    return 'same';
                }
            }
            if(count($currentAddresses) == 0){
                $address = Address::create([
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'city' => $city,
                    'postalCode' => $postalCode,
                    'country' => $country,
                    'region' => $region,
                    'company' => $company,
                    'type' => $type,
                    'user_id' => Auth::id()
                ]);
                return $address;
            }
        }else{
            $address = Address::create([
                'streetAddress' => $streetAddress,
                'extendedAddress' => $extendedAddress,
                'city' => $city,
                'postalCode' => $postalCode,
                'country' => $country,
                'region' => $region,
                'company' => $company,
                'type' => $type,
                'user_id' => Auth::id()
            ]);
            return $address;
        }
        
    }

    // Function to create a new order and add it to the user
    public function createOrder($printful_order, $status, $quantity, $variant_id, $invoice_id, $user_id){

        // Create a new order object
        $the_order = array(
            'printful_order' => $printful_order,
            'status' => $status,
            'quantity' => $quantity,
            'variant_id' => $variant_id,
            'invoice_id' => $invoice_id,
            'user_id' => $user_id
        );

        $order = new Order($the_order);
        $order->save();
        return $order;

    }

    // Function to create a new invoice and add it to the user
    public function createInvoice($customer_id, $transaction_id, $price, $paid, $paymentmethod, $user){

        // Create a new invoice object
        $the_invoice = array(
            'customer_id' => $customer_id,
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
            'payment_method' => $paymentmethod,
            'user_id' => $user
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }


    // Function to send new account message
    public function accountCreated($user, $password){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.accountCreated',[
            'user' => $user,
            'to' => $to,
            'password' => $password
        ], function ($message) use ($user, $password, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Account Created');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function paymentConfirmed($invoice, $user){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.receipt',[
            'user' => $user,
            'to' => $to,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($to);
        });

        return "success";
    }

    // Function to send new account message
    public function videoCallPaymentConfirmed($invoice, $user){

        Mail::send('emails.receipt',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($user->email);
        });

        Mail::send('emails.bookVideoCall',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Book your video call with Tom Morrison');
            $message->to($user->email);
        });

        Mail::send('emails.videoCallPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('VIDEO CALL');
            $message->to('hello@tommorrison.uk');
        });

        return "success";
    }
    
    // Send first SMM follow up message
    public function smmPaymentConfirmed($invoice, $user){

        $user = User::whereId($user->id)->first();
        $user->follow_up = true;
        $user->save();

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }

        Mail::send('emails.receipt',[
            'user' => $user,
            'to' => $to,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($to);
        });

        $subject = 'Welcome to SMM ' . $user->first_name . '! Here’s how to get started!';

        Mail::send('emails.smm.welcome',[
        'subject' => $subject,
        'to' => $to,
        'user' => $user
        ], function ($message) use ($subject, $user, $to){
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
                $message->subject($subject);
                $message->replyTo('hello@tommorrison.uk');
                $message->to($to);
            }
        );

        return "success";
    }
    
    // Function to send new account message
    public function personalisedMobilityPlanPaymentConfirmed($invoice, $user){

        Mail::send('emails.receipt',[
            'user' => $user,
            'invoice' => $invoice
        ], function ($message) use ($user, $invoice)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Tom Morrison Order - ' . $invoice->id);
            $message->to($user->email);
        });

        Mail::send('emails.PMPPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('PMP PROGRAM');
            $message->to('hello@tommorrison.uk');
        });

        return "success";
    }

    private function cartHasRowId($rowId)
    {
        // Get all items in the cart
        $cartItems = Cart::content();

        // Check if the specific rowId exists in the cart
        return $cartItems->has($rowId);
    }
}
