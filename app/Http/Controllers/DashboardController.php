<?php

namespace App\Http\Controllers;

use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

use Illuminate\Http\Request;
use App\Models\Download;
use App\Models\Product;
use App\Models\Order;
use App\Models\Video;
use Storage;
use Auth;
use Http;

class DashboardController extends Controller
{
    private function getApiKey(){
        return env('PRINTFUL_TOKEN');
    }

    

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

     public function productDownload($slug, $download)
     {
        if(Auth::check()){
            return Storage::download('/public/' . $download);
        }else{
            return back();
        }
     }

    public function compatible()
    {
        $products = Product::get();
        $showcamps = Product::where('category_id', 13)->where('status', 'Available')->count();
        $onlinecamps = Product::where('category_id', 13)
        ->whereHas('users', function($u){
            $u->where('id', Auth::id());
        })->get();
        $user = Auth::user();
        foreach($user->products as $up){
            foreach ($products as $p) {
                if($p->id == $up->id){
                    $p->bought = true;
                }
            }
        }
        return view('users.compatible', compact('products','showcamps','onlinecamps'));
    }
    public function index()
    {
        $products = Product::get();
        $showcamps = Product::where('category_id', 13)->where('status', 'Available')->count();
        $onlinecamps = Product::where('category_id', 13)
        ->whereHas('users', function($u){
            $u->where('id', Auth::id());
        })->get();
        $user = Auth::user();
        foreach($user->products as $up){
            foreach ($products as $p) {
                if($p->id == $up->id){
                    $p->bought = true;
                }
            }
        }
        return view('users.index', compact('products','showcamps','onlinecamps'));
    }
    public function support()
    {
        $user = Auth::user();
        return view('users.support', compact('user'));
    }
    
    public function orders()
    {
        $pf = PrintfulApiClient::createOauthClient(env('PRINTFUL_TOKEN'));
        $user = Auth::user();
        $orders = $user->orders;
        $test =  'test';
        foreach($orders as $order){
            $printful = $pf->get('orders/'.$order->printful_order.'?store_id=1749903');
            $result = json_decode(json_encode($printful), FALSE);
            $order->status = $result->status;
            $order->save();
            $product = $pf->get('/store/products/'.$order->variant->merch->printful.'?store_id=1749903');
            $json = json_decode(json_encode($product), FALSE);
            $order->thumbnail_url = $json->sync_product->thumbnail_url;
        }
        return view('users.orders')->with($test, $orders);
    }

    public function viewProduct(Request $request, $slug)
    {
        $user = Auth::user();

        // Redirect for specific slug
        if ($slug == 'end-range-training') {
            return redirect()->to('https://endrange.tommorrison.uk/home');
        }

        // Retrieve the product by slug
        $product = Product::where('slug', $slug)->with(['downloads' => function ($q) {
            $q->orderBy('created_at', 'asc');
        }])->first();

        // Check if product exists
        if (!$product) {
            return back()->with('error', 'Product not found.');
        }

        // Check if user has the product
        foreach ($user->products as $up) {
            if ($up->id == $product->id) {
                return view('users.product.index', compact('product'));
            }
        }

        // Allow specific users to access the product
        if (in_array($user->email, ['jenni@tommorrison.uk', 'luke@elementseven.co'])) {
            return view('users.product.index', compact('product'));
        }

        // Default action: redirect back
        return back()->with('error', 'You do not have access to this product.');
    }

    public function productFaqs(Request $request, $slug){
        $user = Auth::user();
        $product = Product::where('slug', $slug)->first();

        foreach($user->products as $up){
            if($up->id == $product->id){
                return view('users.product.faqs', compact('product'));
            }
        }
        if($user->email == 'jenni@tommorrison.uk' || $user->email == 'luke@elementseven.co'){
            return view('users.product.faqs', compact('product'));
        }
        return back();
    }

    public function productVideos(Request $request, $slug){
        $user = Auth::user();
        $product = Product::where('slug', $slug)->first();
        if($product != null){
            foreach($user->products as $up){
                if($up->id == $product->id){

                    foreach($product->videos as $v){
                        $v->thumbnail = $v->getFirstMediaUrl('videos', 'thumbnail');
                    }
                    return view('users.product.videos', compact('product'));
                }
            }
        }
        if($user->email == 'jenni@tommorrison.uk' || $user->email == 'luke@elementseven.co'){
            return view('users.product.videos', compact('product'));
        }
        return back();
    }

    public function singleVideo(Video $video){
        $product = Product::where('id', 1)->first();
        return view('users.product.video', compact('product','video'));
    }

}
