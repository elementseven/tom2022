<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Category;
use App\Models\Voucher;
use App\Models\Address;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Setting;
use App\Models\User;
use App\Models\Gift;

use Srmklive\PayPal\Services\PayPal as PayPalClient;

use Cart;
use Stripe;
use Mail;
use Auth;

class GiftController extends Controller
{
    protected $provider;
    
    // Beginners bundle prices
    private $bb_smm_price = 55.83;
    private $bb_wiww_price = 7.89;
    private $bb_uc_price = 24.68;

    // private $bb_smm_price = 42.35;
    // private $bb_wiww_price = 4.79;
    // private $bb_uc_price = 32.42;

    // Intermediate bundle prices
    private $ib_sb_price = 56.38;
    private $ib_er_price = 83.62;

    // private $ib_sb_price = 65.81;
    // private $ib_er_price = 60.19;

    // Complete bundle prices
    private $cb_smm_price = 48.30;
    private $cb_wiww_price = 5.64;
    private $cb_uc_price = 21.86;
    private $cb_sb_price = 54.40;
    private $cb_er_price = 78.80;

    // private $cb_smm_price = 47.92;
    // private $cb_wiww_price = 4.41;
    // private $cb_uc_price = 22.89;
    // private $cb_sb_price = 58.85;
    // private $cb_er_price = 64.48;

    // January Mobility reset bundle prices
    // private $mr_smm_price = 59.00;
    // private $mr_mr_price = 75.00;

    private $mr_smm_price = 43.45;
    private $mr_mr_price = 63.75;

    // August Mobility reset bundle prices
    // private $aug_mr_smm_price = 50.00;
    // private $aug_mr_mr_price = 80.00;

    private $aug_mr_smm_price = 50.00;
    private $aug_mr_mr_price = 65.50;

    // January 24 Mobility reset bundle prices
    // private $aug_mr_smm_price = 69.00;
    // private $aug_mr_mr_price = 85.00;

    private $jan_mr_smm_price = 45.90;
    private $jan_mr_mr_price = 69.60;

    private function getApiKey(){
        return 'i0s1ic7h-nlpm-s328:utno-ztn4zk5ibkzt';
    }

    /**
     * process transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function processGiftTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $paypalToken = $provider->getAccessToken();

        $items = Array();

        // Get bundle status
        $bundle = Setting::where('id',1)->first();

        foreach(Cart::content() as $cc){
            if($cc->name == "Beginners Bundle"){

                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item1);

                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_wiww_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item2);

                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) $this->bb_uc_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item3);

            }else if($cc->name == "Beginner's Bundle Upgrade"){


                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) 3.40, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) 16.00, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item3);

            }else if($cc->name == "Intermediate Bundle"){

                // 17.34052% discount

                // Stability Builder
                $product1 = Product::where('id', 17)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // End Range
                $product2 = Product::where('id', 6)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Complete Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item1);

                // Where I went wrong ebook
                $product2 = Product::where('id', 2)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_wiww_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item2);

                // Ultimate Core
                $product3 = Product::where('id', 3)->first();
                $item3 = Array(
                    "name" => $product3->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_uc_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );

                array_push($items, $item3);

                // Stability Builder
                $product4 = Product::where('id', 17)->first();
                $item4 = Array(
                    "name" => $product4->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item4);

                // End Range
                $product5 = Product::where('id', 6)->first();
                $item5 = Array(
                    "name" => $product5->name,
                    "unit_amount" => Array("value" => number_format((float) $this->cb_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item5);

            }else if($cc->name == "Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_sb_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 22)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->ib_er_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Aug 24 Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->aug_mr_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 28)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->aug_mr_mr_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else if($cc->name == "Jan 24 Mobility Reset + SMM Bundle"){

                // SMM
                $product1 = Product::where('id', 1)->first();
                $item1 = Array(
                    "name" => $product1->name,
                    "unit_amount" => Array("value" => number_format((float) $this->jan_mr_smm_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item1);

                // Mobility Reset
                $product2 = Product::where('id', 26)->first();
                $item2 = Array(
                    "name" => $product2->name,
                    "unit_amount" => Array("value" => number_format((float) $this->jan_mr_mr_price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => 1
                );
                array_push($items, $item2);

            }else{
                $item = Array(
                    "name" => $cc->name,
                    "unit_amount" => Array("value" => number_format((float)$cc->price, 2, '.', ''), "currency_code" => 'GBP'),
                    "quantity" => $cc->qty
                );
                array_push($items, $item);
            }
        }
        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('successGiftTransaction'),
                "cancel_url" => route('cancelGiftTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "GBP",
                        "value" => Cart::total(),
                        "breakdown" => Array( "item_total" => Array( "value" => Cart::total(), "currency_code" => 'GBP') ),
                    ],
                    "items" => $items
                ]
            ]
        ]);

        if (isset($response['id']) && $response['id'] != null) {

            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }

            return redirect()
                ->route('gift-checkout')
                ->with('error', 'Something went wrong.');

        } else {
            return redirect()
                ->route('checkout')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    /**
     * success transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function successGiftTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('GBP');
        $provider->getAccessToken();
        $response = $provider->capturePaymentOrder($request['token']);
        $user = Auth::user();
        if (isset($response['status']) && $response['status'] == 'COMPLETED') {

            return $this->success($request, $user, 'PayPal', $response['id']);

        } else {
            return redirect()->route('gift-checkout')->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    /**
     * cancel transaction.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelGiftTransaction(Request $request)
    {
        return redirect()
            ->route('checkout')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }

    public function getIntent(){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $user = Auth::user();
        $total = Cart::total(2,'.','') * 100;

        if($user->stripe_id == null){
            $this->createStripeCustomer($user);
        }

        $products = Array();
        foreach(Cart::content() as $cc){
            if($cc->name == "Beginners Bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->bb_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->bb_wiww_price * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => $this->bb_uc_price * 100
                    ]
                ]);
            }else if($cc->name == "Beginner's Bundle Upgrade"){
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => 3.40 * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => 16.00 * 100
                    ]
                ]);
            }else if($cc->name == "Intermediate Bundle"){
                $product1 = Product::where('id', 17)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Stability Builder",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->ib_sb_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 6)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "End Range Training",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->ib_er_price * 100
                    ]
                ]);
            }else if($cc->name == "Complete Bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->cb_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 2)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Where I Went Wrong - E-Book",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->cb_wiww_price * 100
                    ]
                ]);
                $product3 = Product::where('id', 3)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Ultimate Core",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product3->stripe_id,
                        'unit_amount_decimal' => $this->cb_uc_price * 100
                    ]
                ]);
                $product4 = Product::where('id', 17)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "Stability Builder",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product4->stripe_id,
                        'unit_amount_decimal' => $this->cb_sb_price * 100
                    ]
                ]);
                $product5 = Product::where('id', 6)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "End Range Training",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product5->stripe_id,
                        'unit_amount_decimal' => $this->cb_er_price * 100
                    ]
                ]);
            }else if($cc->name == "Mobility Reset + SMM Bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 22)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "January 30 Day Mobility Reset",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->mr_mr_price * 100
                    ]
                ]);
            }else if($cc->name == "Aug 24 Mobility Reset + SMM Bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 28)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "August Mobility Reset 2024",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_mr_price * 100
                    ]
                ]);
            }else if($cc->name == "Jan 24 Mobility Reset + SMM Bundle"){
                $product1 = Product::where('id', 1)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "The Simplistic Mobility Method",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product1->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_smm_price * 100
                    ]
                ]);
                $product2 = Product::where('id', 26)->first();
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => "January Mobility Reset 2024",
                    'quantity' => 1,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $product2->stripe_id,
                        'unit_amount_decimal' => $this->aug_mr_mr_price * 100
                    ]
                ]);
            }else{
                $theprice = $cc->price;
                $productdata = \Stripe\InvoiceItem::create([
                    'customer' => Auth::user()->stripe_id,
                    'description' => $cc->name,
                    'quantity' => $cc->qty,
                    'price_data' => [
                        'currency' => 'gbp',
                        'product' => $cc->model->stripe_id,
                        'unit_amount_decimal' => $theprice * 100
                    ]
                ]);
            }
        }


        $stripeinvoice = \Stripe\Invoice::create([
            'customer' => Auth::user()->stripe_id,
            'statement_descriptor' => 'Tom Morrison'
        ]);
        $stripeinvoiceid = $stripeinvoice->id;
        $finalized = $stripeinvoice->finalizeInvoice();
        $intent = \Stripe\PaymentIntent::Retrieve($finalized->payment_intent);  
    
        
        return $intent;
    }

    public function storeGiftDetails(Request $request){
        if($request->input('when') == 'immediately'){
            // Validate the form data
            $this->validate($request,[
                'gift_first_name' => 'required|string|max:255',
                'gift_last_name' => 'required|string|max:255',
                'gift_email' => 'required|string|email|max:255|confirmed',
                'agree' => 'accepted',
            ]);
            $request->session()->put('gift_first_name', $request->input('gift_first_name'));
            $request->session()->put('gift_last_name', $request->input('gift_last_name'));
            $request->session()->put('gift_email', $request->input('gift_email'));
            $request->session()->put('gift_when', 'immediately');
            $request->session()->put('gift_date', null);
            $request->session()->put('gift_message', $request->input('message'));
        }else{
            // Validate the form data
            $this->validate($request,[
                'gift_first_name' => 'required|string|max:255',
                'gift_last_name' => 'required|string|max:255',
                'gift_email' => 'required|string|email|max:255|confirmed',
                'date' => 'required|after:today',
                'agree' => 'accepted',
            ]);
            $request->session()->put('gift_first_name', $request->input('gift_first_name'));
            $request->session()->put('gift_last_name', $request->input('gift_last_name'));
            $request->session()->put('gift_email', $request->input('gift_email'));
            $request->session()->put('gift_when', 'date');
            $request->session()->put('gift_date', $request->input('date'));
            $request->session()->put('gift_when', $request->input('message'));
        }

        return response('Validated', 200)->header('Content-Type', 'text/plain'); 

    }

    public function paymentStatus(Request $request, Invoice $invoice){
    
        $user = Auth::user();

        if($request->query('payment_intent')){
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $clientsecret = $request->query('payment_intent_client_secret');
            $intent = \Stripe\PaymentIntent::retrieve($request->query('payment_intent'));
            if($intent->status == 'succeeded'){
                return $this->success($user, $invoice, 'Card', $intent->id);
            }else{
                // Error with the stripe payment
                session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
                return redirect()->to('/checkout'); 

            }
        }else{
            // Error with the stripe payment
            session(['payment_error' => "There was an error processing your payment, please try another payment method."]);
            return redirect()->to('/checkout');
        }
    }


    /*
        Go to success page
    */
    public function success($request, $user, $paymentmethod, $transaction_id){

        // Create Variables for use in the transaction
        $shippingRate = 0;
        $total = ((Cart::total() * 1000) + ($shippingRate * 1000)) / 1000;
        $subscribed = false;
        
        
        // Create invoice 
        $request->session()->forget('invoice_id');
        $invoice = $this->createInvoice(NULL, $total, 0, $paymentmethod);
        $request->session()->put('invoice_id', $invoice->id);

        if (session()->has('voucher_id')) {
            $voucher = Voucher::where('id', session('voucher_id'))->first();
        }else{
            $voucher = null;
        }

        if (session()->has('voucher_id')) {
            $invoice->vouchers()->attach($voucher->id);
            session()->forget('voucher_id');
        }

        // If the transaction ID is set
        if($transaction_id != ""){
            
            $user = Auth::user();
            
            $invoice->paid = true;
            $invoice->transaction_id = $transaction_id;
            $invoice->user_id = $user->id;
            $invoice->save();


            // Create a new gift object
            if($request->input('when') == "immediately"){
                $the_gift = array(
                    'code' => $this->randomCode(),
                    'first_name' => session('gift_first_name'),
                    'last_name' => session('gift_last_name'),
                    'email' => session('gift_email'),
                    'when' => session('gift_when'),
                    'message' => session('gift_message'),
                    'invoice_id' => $invoice->id
                );
            }else{
                $the_gift = array(
                    'code' => $this->randomCode(),
                    'first_name' => session('gift_first_name'),
                    'last_name' => session('gift_last_name'),
                    'email' => session('gift_email'),
                    'when' => session('gift_when'),
                    'date' => Carbon::parse(session('gift_date'))->format('Y-m-d'),
                    'message' => session('gift_message'),
                    'invoice_id' => $invoice->id
                );
            }
            $gift = new Gift($the_gift);
            $gift->save();

            $request->session()->forget('gift_first_name');
            $request->session()->forget('gift_last_name');
            $request->session()->forget('gift_email');
            $request->session()->forget('gift_when');
            $request->session()->forget('gift_date');
            $request->session()->forget('gift_message');

            foreach(Cart::content() as $i){
                if($i->options->type != 'merch'){
                    if($i->sale_price != NULL){
                        if (session()->has('voucher_id')) {
                            $invoice->products()->attach($i->id, array('price' => $i->sale_price - (($i->sale_price / 100) * $voucher->percent), 'qty' => 1));
                        }else{
                            $invoice->products()->attach($i->id, array('price' => $i->sale_price, 'qty' => 1));
                        }
                        
                    }else{
                        if (session()->has('voucher_id')) {
                            $invoice->products()->attach($i->id, array('price' => $i->price - (($i->price / 100) * $voucher->percent), 'qty' => 1));
                        }else{
                            $invoice->products()->attach($i->id, array('price' => $i->price, 'qty' => 1));
                        }
                    }
                    if($i->id != 4 && $i->id != 5){
                        $gift->products()->attach($i->id);
                    }
                }
            }
            
            if($invoice->products->contains(15)){
                // Send payment confirmation email for Beginners bundle
                if(!$gift->products()->where('products.id', 1)->exists()){
                    $gift->products()->attach(1);
                }
                if(!$gift->products()->where('products.id', 2)->exists()){
                    $gift->products()->attach(2);
                }
                if(!$gift->products()->where('products.id', 3)->exists()){
                    $gift->products()->attach(3);
                }
            }
            
            if($invoice->products->contains(16)){
                if(!$gift->products()->where('products.id', 2)->exists()){
                    $gift->products()->attach(2);
                }
                if(!$gift->products()->where('products.id', 3)->exists()){
                    $gift->products()->attach(3);
                }
            }

            if($invoice->products->contains(20)){
                // Intermediate bundle
                if(!$gift->products()->where('products.id', 17)->exists()){
                    $gift->products()->attach(17);
                }
                if(!$gift->products()->where('products.id', 6)->exists()){
                    $gift->products()->attach(6);
                }
            }

            if($invoice->products->contains(21)){
                // Complete bundle
                if(!$gift->products()->where('products.id', 1)->exists()){
                    $gift->products()->attach(1);
                }
                if(!$gift->products()->where('products.id', 2)->exists()){
                    $gift->products()->attach(2);
                }
                if(!$gift->products()->where('products.id', 3)->exists()){
                    $gift->products()->attach(3);
                }
                if(!$gift->products()->where('products.id', 17)->exists()){
                    $gift->products()->attach(17);
                }
                if(!$gift->products()->where('products.id', 6)->exists()){
                    $gift->products()->attach(6);
                }
            }

            if($invoice->products->contains(23)){
                // Mobility Reset + SMM Bundle
                if(!$gift->products()->where('products.id', 1)->exists()){
                    $gift->products()->attach(1);
                }
                if(!$gift->products()->where('products.id', 22)->exists()){
                    $gift->products()->attach(22);
                }
            }

            if($invoice->products->contains(29)){
                // Aug Mobility Reset + SMM Bundle
                if(!$gift->products()->where('products.id', 1)->exists()){
                    $gift->products()->attach(1);
                }
                if(!$gift->products()->where('products.id', 28)->exists()){
                    $gift->products()->attach(28);
                }
            }

            if($invoice->products->contains(27)){
                // Jan 24 Mobility Reset + SMM Bundle
                if(!$gift->products()->where('products.id', 1)->exists()){
                    $gift->products()->attach(1);
                }
                if(!$gift->products()->where('products.id', 26)->exists()){
                    $gift->products()->attach(26);
                }
            }

     
            // Send payment confirmation email
            $this->paymentConfirmed($gift, $user);
            

            // Redirect to my account page
            Cart::destroy();
            session()->forget('intent');
            session(['successful_payment' => $invoice->id]);
            
            if($gift->when == "immediately"){
                $this->giftNotification($gift);
            }
            if (session()->has('voucher_id')) {
                session()->forget('voucher_id');
            }
            return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);

        }else{
            // Inform the user of the failed transaction
            session(['general_payment_error' => "There was an error processing your payment."]);
            return back();
        }

        return view('shop.gift-success')->with(['invoice' => $invoice,'gift' => $gift]);

    }


    /*
        Create the new user and attach gifted products
    */
    public function createAccount(Request $request){

        // Validate information
        $this->validate($request,[
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'country' => 'required|string',
            'consent' => 'accepted',
            'email' => 'required|string|email|max:255|unique:users|confirmed',
            'password' => 'required|string|min:6|confirmed'
        ]);

        // Create the new user
        $newuser = array(   
            'avatar' => 'default.jpg',
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'country' => $request->input('country'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        );
        $user = User::create($newuser);

        $this->accountCreated($user, $request->input('password'));

        // Get the gift and attach products associated with it to the new user
        $gift = Gift::where([['id', $request->input('id')],['code', $request->input('code')]])->first();
        foreach($gift->products as $product){
            
            if($product->id == 15){
                
                $user->products()->attach(1);
                $user->products()->attach(2);
                $user->products()->attach(3);
                
            }else if($product->id == 20){

                $user->products()->attach(17);
                $user->products()->attach(6);

            }else if($product->id == 21){

                $user->products()->attach(1);
                $user->products()->attach(2);
                $user->products()->attach(3);
                $user->products()->attach(17);
                $user->products()->attach(6);

            }else if($product->id == 23){

                $user->products()->attach(1);
                $user->products()->attach(22);

            }else if($product->id == 25){

                $user->products()->attach(1);
                $user->products()->attach(24);

            }else if($product->id == 27){

                $user->products()->attach(1);
                $user->products()->attach(26);

            }else if($product->id == 4){
                // Personalised mobility programme
                $this->personalisedMobilityPlanPaymentConfirmed($gift->invoice, $user);
                
            }else if($product->id == 5){
                // Video call
                $this->videoCallPaymentConfirmed($gift->invoice, $user);
                
            }else{
                $user->products()->attach($product->id);
            }
            
        }
        
        $gift->user()->associate($user)->save();
        
        // Login the new user and update the gift with the user id so the system can tell it has been claimed
        Auth::login($user);

        
        $this->giftClaimed($gift);
        return redirect()->to('/dashboard');

    }
    
    /*
        Accept gift to existing account
    */
    public function acceptGift(Request $request, Gift $gift){
        $user = Auth::user();
        foreach($gift->products as $product){
            foreach($user->products as $p){
                if($p->id == $product->id && $product->id != 4 && $product->id != 5){
                    $request->session()->put('already-purchased', $product->name);
                    return redirect()->back();
                }
            }
            $user->products()->attach($product->id);
        }
        $updated = array(   
            'user_id' => $user->id,
        );
        $gift->update($updated);
        $this->giftClaimed($gift);
        return redirect()->to('/dashboard');
    }

    /*
        Claim Gift
    */
    public function claim(Gift $gift, $code){
        if($code == $gift->code){
            if($gift->user_id != null){
                return redirect()->to('/dashboard');
            }else{
                return view('shop.claim-gift',compact('gift'));
            }
        }else{
            abort(404);
        }
    }

    /*
        Go to the gift basket page
    */
    public function giftBasket(Request $request){
        
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                $request->session()->put('non-gift-still-in-basket', true);
                return redirect()->to('/basket');
            }
        }
        if (session()->has('voucher_id')) {
            $voucher = Voucher::where('id', session('voucher_id'))->first();
        }else{
            $voucher = null;
        }
         
        $bundle = Setting::where('id',1)->first();
        $now = Carbon::now();
        $showvoucher=Voucher::whereDate('expiry_date','>=',$now)->count();
        $inbasket = [];
        foreach(Cart::content() as $i){
            array_push($inbasket, $i->id);
        }
        if(count(Cart::content()) > 0){
            $products = Product::where('id','!=', [$inbasket])->paginate(5);
        }else{
            $products = Product::paginate(5);
        }
        return view('shop.gift-basket',compact('bundle','products','showvoucher','voucher'));

    }

    public function applyVoucher(Request $request){
        session()->forget('intent');
        $this->validate($request,[
            'voucher' => 'required|string|max:225'
        ]);
    
        $voucher = Voucher::where('code',$request->input('voucher'))->first();
    
        if ($voucher) {
            
                // Check if the voucher has expired
            $now = Carbon::now();
            $expires = Carbon::createFromFormat('Y-m-d H:i:s', $voucher->expiry_date);
            if($now > $expires){
                session(['voucher_notification' => 'Sorry this voucher has expired.']);
                return back();
            }
            
            $request->session()->put('voucher_id', $voucher->id);

            // Update the cart items
            foreach(Cart::content() as $i){
    
                $p = Product::where('id',$i->id)->first();
                $productprice = $i->price - (($i->price / 100) * $voucher->percent);
                Cart::update($i->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$p->image,'original_price' => $p->price, 'gift' => 'yes']]);
    
            }
            
            session(['voucher_notification' => $voucher->percent . '% voucher applied.']);
            return back();
    
        }else{
            session(['voucher_notification' => 'Voucher not found.']);
            return back();
        }
    
    }

    // Function to and create a new user in Braintree 
    public function registerUserOnBrainTree($first_name, $last_name, $email){

        // Pass the user information to braintree
        $result = Braintree_Customer::create(array(
            'firstName' => $first_name,
            'lastName' => $last_name,
            'email' => $email
        ));

        if ($result->success) {

            // Return the Customer ID
            return $result->customer->id;
        }else{

            // Create empty variable to store errors
            $errorFound = '';

            // Loop through errors and add them to the error string
            foreach ($result->errors->deepAll() as $error) {
                $errorFound .= $error->message . "<br />";
            }

        }
    }

    // Function to create a user
    public function createUser($first_name, $last_name, $email, $country, $password, $customer_id){
        $user = User::create([
            'avatar' => 'default.jpg',
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'country' => $country,
            'password' => Hash::make($password),
            'customer_id' => $customer_id,
            'role_id' => 2
        ]);

        Auth::login($user, true);
        return $user;

    }

    // Function to create an address
    public function createAddress($streetAddress, $extendedAddress, $city, $postalCode, $country, $region, $company, $type){

        if(Auth::check()){
            $currentAddresses = Auth::user()->addresses;
            foreach ($currentAddresses as $key => $a) {
                if($type == 'billing' && $a->type == 'billing'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else if($type == 'shipping' && $a->type == 'shipping'){
                    if($streetAddress != $a->streetAddress){
                        $a->delete();
                        $address = Address::create([
                            'streetAddress' => $streetAddress,
                            'extendedAddress' => $extendedAddress,
                            'city' => $city,
                            'postalCode' => $postalCode,
                            'country' => $country,
                            'region' => $region,
                            'company' => $company,
                            'type' => $type,
                            'user_id' => Auth::id()
                        ]);
                        return $address;
                    }else{
                        $address = $a;
                        return $address;
                    }
                }else{
                    return 'same';
                }
            }
            if(count($currentAddresses) == 0){
                $address = Address::create([
                    'streetAddress' => $streetAddress,
                    'extendedAddress' => $extendedAddress,
                    'city' => $city,
                    'postalCode' => $postalCode,
                    'country' => $country,
                    'region' => $region,
                    'company' => $company,
                    'type' => $type,
                    'user_id' => Auth::id()
                ]);
                return $address;
            }
        }else{
            $address = Address::create([
                'streetAddress' => $streetAddress,
                'extendedAddress' => $extendedAddress,
                'city' => $city,
                'postalCode' => $postalCode,
                'country' => $country,
                'region' => $region,
                'company' => $company,
                'type' => $type,
                'user_id' => Auth::id()
            ]);
            return $address;
        }
        
    }

    // Function to create a random password
    public function randomPassword() {
       $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $pass[] = $alphabet[$n];
       }
        return implode($pass); //turn the array into a string
    }
    // Function to create a random code
    public function randomCode() {
       $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $code = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
           $n = rand(0, $alphaLength);
           $code[] = $alphabet[$n];
       }
        return implode($code); //turn the array into a string
    }

    /*
        Add a product to the gift basket
    */
    public function addProductToBasket(Request $request, Product $product)
    {   
        session()->forget('intent');
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                $request->session()->put('non-gift-still-in-basket', true);
                return redirect()->to('/basket');
            }
        }

        // Check if user already has the same product in their gift basket
        $duplicatecheck = 0;
        foreach(Cart::content() as $row){
            if($row->model->id == $product->id){
                $duplicatecheck++;
            }
        }
        if($duplicatecheck > 0){
            $request->session()->put('duplicate-product', $product->name);
            return redirect()->to('/gift-basket');
        }

        // Add to cart
        if($product->sale_price != NULL){
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->sale_price, 'weight' => 0, 'options' => ['image'=>$product->image,'original_price' => $product->price, 'gift' => 'yes']])->associate('App\Models\Product');
        }else{
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'weight' => 0, 'options' => ['image'=>$product->image, 'gift' => 'yes']])->associate('App\Models\Product');
        }

        // Apply bundle discount if needed
        $bundle = Setting::where('id',1)->first();
        if($bundle->status == "Active" && count(Cart::content()) > 1){
            foreach(Cart::content() as $row){
                if($row->options->type != 'merch'){
                    if($row->model->sale_price != NULL){
                        $productprice = $row->model->sale_price - (($row->model->sale_price / 100) * $bundle->value);
                    }else{
                        $productprice = $row->model->price - (($row->model->price / 100) * $bundle->value);
                    }
                    Cart::update($row->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$row->model->image,'original_price' => $row->model->price, 'gift' => 'yes']]);
                }
            }
        }
        
        // Change tax rate
        $items = Cart::content();
        $tax_rate = 0;
        config( ['cart.tax' => $tax_rate] ); 
        foreach ($items as $item){
            $item->setTaxRate($tax_rate);
            Cart::update($item->rowId, $item->qty); 
        }

        // Go to gift basket
        return redirect()->to('/gift-basket');

    }

    /*
        Go to the gift checkout page
    */
    public function checkout(){
        foreach(Cart::content() as $row){
            if($row->options->gift == "no"){
                return redirect()->to('/checkout');
            }
        }

        if (session()->has('invoice_id')) {
            $invoice = Invoice::where('id', session('invoice_id'))->first();
        }else{
            $invoice = NULL;
        }
        $bundle = Setting::where('id',1)->first();
        if(Auth::check()){
            if (session()->has('intent')) {
                $intent = session('intent');        
            }else{
                $intent = $this->getIntent();
                session(['intent' => $intent]);
            }
            return view('shop.gift-checkout',compact('intent','invoice','bundle'));
        }else{
            return view('shop.create-account',compact('invoice','bundle'));
        }

    }


    /*
        Remove Item from cart
    */
    public function remove(Request $request){
        session()->forget('intent');
        $rowId = $request->input('rowId');
        
        $test = Cart::content()->where('rowId',$rowId);

        if($test->isNotEmpty()){

            // Remove item from cart
            Cart::remove($rowId);

            // Apply bundle discount if needed
            $bundle = Setting::where('id',1)->first();
            if($bundle->status == "Active" && count(Cart::content()) > 1){
                foreach(Cart::content() as $row){
                    if($row->model->sale_price != NULL){
                        $productprice = $row->model->sale_price - (($row->model->sale_price / 100) * $bundle->value);
                    }else{
                        $productprice = $row->model->price - (($row->model->price / 100) * $bundle->value);
                    }
                    Cart::update($row->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$row->model->image,'original_price' => $row->model->price, 'gift' => 'yes']]);
                }
            }else{
                foreach(Cart::content() as $row){
                    if($row->model->sale_price != NULL){
                        $productprice = $row->model->sale_price;
                    }else{
                        $productprice = $row->model->price;
                    }
                    Cart::update($row->rowId, ['price' => number_format((float)$productprice, 2, '.', ''), 'options' => ['image'=>$row->model->image,'original_price' => $row->model->price, 'gift' => 'yes']]);
                }
            }

            // Destroy cart if no items are in it
            if(count(Cart::content()) == 0){
                if (session()->has('voucher_id')) {
                    session()->forget('voucher_id');
                }
                Cart::destroy();
            }

        }else{
            if (session()->has('voucher_id')) {
                session()->forget('voucher_id');
            }
            return redirect()->to("/gift-basket");
        }

        return redirect()->to("/gift-basket");
    }


    /*
        Empty & destroy the cart
    */
    public function emptyBasket(){
        session()->forget('intent');
        Cart::destroy();
        if (session()->has('voucher_id')) {
            session()->forget('voucher_id');
        }
        return redirect()->to("/gift-basket");
    }


    /*
        Create a new invoice and add it to the user
    */
    public function createInvoice($transaction_id, $price, $paid, $paymentmethod){

        // Create a new invoice object
        $the_invoice = array(
            'transaction_id' => $transaction_id,
            'price' => $price,
            'paid' => $paid,
            'payment_method' => $paymentmethod,
        );

        $invoice = new Invoice($the_invoice);
        $invoice->save();
        return $invoice;

    }


    /*
        Perform the transacrtion using Braintree
    */
    public function createTransaction($nonce,$customerId,$planId,$subscribed,$total,$id){

        // Pass the transaction details to Braintree and capture the result
        $result = Braintree_Transaction::sale([
            'customerId' => $customerId,
            'amount' => $total,
            'paymentMethodNonce' => $nonce,
            'orderId' => 'TM_' . Carbon::now()->timestamp . "_" . $id,
            'options' => [
                'submitForSettlement' => True
            ]
        ]);

        if ($result->success) {
            // Return the successful Transaction ID
            return $result->transaction->id;
        }else{

            // Create empty variable to store errors
            $errorFound = '';

            // Loop through errors and add them to the error string
            foreach ($result->errors->deepAll() as $error1) {
                $errorFound .= $error1->message . "<br />";
            }

            session(['payment_error' => $errorFound]);
            return "error";
        }
    }

    // Function to send new account message
    public function accountCreated($user, $password){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }
        Mail::send('emails.accountCreatedForGift',[
            'user' => $user,
            'to' => $to,
            'password' => $password
        ], function ($message) use ($user, $password, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Account Created');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function giftNotification($gift){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $gift->email;
        }
        Mail::send('emails.giftNotification',[
            'gift' => $gift,
            'to' => $to
        ], function ($message) use ($gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('You have received a gift');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function giftClaimed($gift){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $gift->invoice->user->email;
        }
        Mail::send('emails.giftClaimed',[
            'gift' => $gift,
            'to' => $to
        ], function ($message) use ($gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Your gift was claimed');
            $message->to($to);
        });

    }

    // Function to send new account message
    public function paymentConfirmed($gift, $user){

        // Determine which email address to send to
        $to = '';
        if(env('APP_ENV') == 'local'){
            $to = env('ADMIN_EMAIL');
        }else{
            $to = $user->email;
        }
        Mail::send('emails.giftPurchased',[
            'user' => $user,
            'to' => $to,
            'gift' => $gift
        ], function ($message) use ($user, $gift, $to)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('Gift order confirmed - Tom Morrison');
            $message->to($to);
            $message->attach(public_path('docs/21-12-online-gift-voucher.jpg'));
        });

        return "success";
    }

    // Function to PMP message
    public function personalisedMobilityPlanPaymentConfirmed($invoice, $user){

        Mail::send('emails.PMPPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('PMP PROGRAM');
            $message->to('hello@tommorrison.uk');
            //$message->to('luke@elementseven.co');
        });

        return "success";
    }
    
    // Function to send new account message
    public function videoCallPaymentConfirmed($invoice, $user){

        Mail::send('emails.videoCallPurchaseConfirmed',[
            'invoice' => $invoice, 
            'user' => $user
        ], function ($message) use ($invoice, $user)
        {
            $message->from('donotreply@tommorrison.uk', 'Tom Morrison');
            $message->subject('VIDEO CALL');
            $message->to('hello@tommorrison.uk');
            //$message->to('luke@elementseven.co');
        });

        return "success";
    }

    /**
     * Create a Stripe Customer
     *
     * @return \Illuminate\Http\Response
     */
    public function createStripeCustomer($user){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $billingAddress = Address::where('user_id', $user->id)->where('type','billing')->first();
        $stripecustomer = \Stripe\Customer::create([
            'address' => [
                'city' => $billingAddress->city,
                'country' => $billingAddress->country,
                'line1' => $billingAddress->streetAddress,
                'line2' => $billingAddress->extendedAddress,
                'postal_code' => $billingAddress->postalCode,
                'state' => $billingAddress->region,
            ],
            'email' => $user->email,
            'name' => $user->full_name,
        ]);
        $user->stripe_id = $stripecustomer->id;
        $user->save();
        return $user;
    }
}
