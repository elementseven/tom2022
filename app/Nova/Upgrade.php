<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Laravel\Nova\Http\Requests\NovaRequest;

class Upgrade extends Resource
{   

    public static $model = \App\Models\Upgrade::class;
    public static $title = 'title';
    public static $search = ['title'];

    public function fields(NovaRequest $request)
    {
        $statuses = ['published' => 'Published', 'draft' => 'Draft'];
        return [
            Text::make('Title')->sortable(),
            Text::make('Text Area 1', 'textone')->onlyOnForms()->help('A short sentence to convince the customer to buy the upgrade. Eg. Upgrade to the Beginners Bundle NOW & save £15.58!'),
            Text::make('Text Area 2', 'texttwo')->onlyOnForms()->help('A short sentence explaining what they get eg. - Add Ultimate Core™ & Where I Went Wrong to boost your mobility gains!'),
            Image::make('Image')->store(function (Request $request, $model) {
                $model->addMediaFromRequest('image')->toMediaCollection('upgrades');
            })->preview(function () {
                return $this->getFirstMediaUrl('upgrades', 'thumbnail');
            })->thumbnail(function () {
                return $this->getFirstMediaUrl('upgrades', 'thumbnail');
            })->hideFromIndex()->deletable(false),
            Select::make('Status')->options($statuses)->sortable()->help('Only "Published" upgrades will be offered, use "Draft" to temporarily turn disable it.'),
            Number::make('Price')->step(0.01)->nullable()->onlyOnForms()->help('The full price of all the products in the upgrade added together (you need to add them up).'),
            Number::make('Sale Price', 'sale_price')->step(0.01)->nullable()->help('The price the bundle is to be sold for. This will auto-calculate when you add products to this upgrade on the next screen.'),
            BelongsTo::make('Product')->nullable()->help('When a customer purchases this product you want this upgrade to pop up. Please only select a product or a bundle, never both.'),
            BelongsTo::make('Bundle')->nullable()->help('When a customer purchases this bundle you want this upgrade to pop up. Please only select a product or a bundle, never both.'),
            BelongsToMany::make('Products')->fields(function (Request $request, $model) {
                return [
                    Number::make('Price', 'price')->step(0.01),
                ];
            }),

            BelongsToMany::make('Invoices')->fields(function () {
                return [
                    Number::make('Price', 'price')->step(0.01),
                ];
            }),

            Text::make('Upgrade Link (Click to copy)', function () {
                $link = 'https://tommorrison.uk/basket/upgrades/add/' . $this->id . '/' . $this->slug;
                return View::make('vendor.nova.copy-link', ['link' => $link, 'id' => $this->id])->render();
            })->asHtml()->exceptOnForms(),

        ];
    }

    public function cards(NovaRequest $request)
    {
        return [];
    }

    public function filters(NovaRequest $request)
    {
        return [];
    }

    public function lenses(NovaRequest $request)
    {
        return [];
    }

    public function actions(NovaRequest $request)
    {
        return [];
    }
}
