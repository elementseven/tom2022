<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Number;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\Log;

class Bundle extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\Bundle>
     */
    public static $model = \App\Models\Bundle::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        $statuses = array('published' => 'Published', 'draft' => 'Draft');
        return [
            Text::make('Title')->sortable(),
            Text::make('Slug')->onlyOnIndex(),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            Text::make('Short')->hideFromIndex(),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('bundles');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('bundles', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('bundles', 'thumbnail');
                })->hideFromIndex()->deletable(false),
            Select::make('Status')->options($statuses)->sortable(),
            Number::make('Price')->step(0.01)->nullable()->onlyOnForms(),
            Number::make('Sale Price', 'sale_price')->step(0.01)->nullable(),
            Trix::make('Description')->withFiles('public'),
            BelongsToMany::make('Products')->fields(function (Request $request, $model) {
                return [
                    Number::make('Price', 'price')->step(0.01),
                ];
            }),
            BelongsToMany::make('Invoices')->fields(function() {
                return [
                    Number::make('Price', 'price')->step(0.01),
                ];
            }),
        ];
    }



    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
