<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class Seminar extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Seminar::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'location';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'location',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $currencies = array('GBP'=>'GBP','Euro'=>'Euro');
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        $categories = array('Movement & Mobility'=> 'Movement & Mobility','Gymnastics'=>'Gymnastics','Ultimate Core'=>'Ultimate Core');
        return [
            Text::make('Name')->help("The name of the seminar"),
            Select::make('Category')->options($categories)->sortable()->help('Select the type of seminar'),
            Text::make('Eventbrite')->onlyOnForms()->help('The reference number to the seminar on Eventbrite'),
            Select::make('Status')->options($statuses)->sortable()->help('Publish the seminar to the website or save as draft'),
            Select::make('Currency')->options($currencies)->sortable()->help('The currency to be displayed on the website'),
            Number::make('Price')->help('The price of the seminar'),
            DateTime::make('Start')->sortable()->help('The date and time the seminar starts'),
            DateTime::make('End')->onlyOnForms()->help('The date and time the seminar ends'),
            Text::make('Location')->sortable()->help('The name of the location where the seminar is taking place'),
            Text::make('Link')->onlyOnForms()->help("A link to the location's website"),
            Text::make('Line1')->onlyOnForms()->help("Line one of the locaton's address"),
            Text::make('Line2')->onlyOnForms()->help("Line one of the locaton's address"),
            Text::make('City')->onlyOnForms()->help("The locaton's town / city"),
            Text::make('Postcode')->onlyOnForms()->help("The locaton's post code"),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
