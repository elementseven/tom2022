<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Http\Requests\NovaRequest;

class Video extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Video::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Bonus Videos'=>'Bonus Videos','Head To Toe'=>'Head To Toe','Exercise Breakdowns'=>'Exercise Breakdowns','Exercises Videos'=>'Exercises Videos','Test Videos' => 'Test Videos','Reset Welcome Videos' => 'Reset Welcome Videos');
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make('Product')->sortable()->default(1),
            Image::make('Thumbnail')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('thumbnail')->toMediaCollection('videos');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('videos', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('videos', 'thumbnail');
            })->nullable()->deletable(false),
            Select::make('Category')->options($statuses)->nullable()->default('Bonus Videos'),
            Text::make('Name')->sortable(),
            Text::make('Runtime')->sortable()->nullable(),
            Textarea::make('Description'),
            Text::make('Youtube')->sortable(),
            DateTime::make('Show Time')->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
