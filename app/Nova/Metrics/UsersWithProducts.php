<?php

namespace App\Nova\Metrics;

use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Metrics\Partition;
use App\Models\Product;

class UsersWithProducts extends Partition
{
    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        $products = Product::withCount('users')->get();

        return $this->result(
            $products->flatMap(function ($product) {
                if($product->name != "Online Personal Coaching" && $product->name != "Video Call" && $product->name != "Beginner's bundle Upgrade"){
                    return [
                        $product->name => count($product->users)
                    ];
                }
            })->toArray()
        );
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'users-with-products';
    }
}
