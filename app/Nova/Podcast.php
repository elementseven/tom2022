<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;

class Podcast extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\Podcast>
     */
    public static $model = \App\Models\Podcast::class;
    
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Number::make('Episode')->sortable(),
            Text::make('Name')->sortable(),
            Text::make('Excerpt')->sortable(),
            Date::make('Date')->sortable(),
            Select::make('Status')->options($statuses)->sortable(),
            Text::make('YouTube')->onlyOnForms(),
            Trix::make('Description')->withFiles('public'),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('podcasts');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('podcasts', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('podcasts', 'thumbnail');
            })->deletable(false),
            HasMany::make('Link','links'),
            BelongsToMany::make('Product','products'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
