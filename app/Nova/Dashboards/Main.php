<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Cards\Help;
use Laravel\Nova\Dashboards\Main as Dashboard;
use ElementSeven\SalesSplit\SalesSplit;
use Auth;

class Main extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        if(Auth::id() == 673 || Auth::id() == 18){
            return [

                (new SalesSplit)->width('full'),

                // new SalesPerDay,            
                // new InvoicesPerDay,
                // new NewInvoices,
                // new InvoicesSum,
                
                // new \Tightenco\NovaGoogleAnalytics\PageViewsMetric,
                // new \Tightenco\NovaGoogleAnalytics\VisitorsMetric,
                // new \Tightenco\NovaGoogleAnalytics\MostVisitedPagesCard,  
            ];
        }else{
            return [
                // new \Tightenco\NovaGoogleAnalytics\PageViewsMetric,
                // new \Tightenco\NovaGoogleAnalytics\VisitorsMetric,
                // new \Tightenco\NovaGoogleAnalytics\MostVisitedPagesCard,  
            ];
        }
    }
}
