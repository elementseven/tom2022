<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\Log;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Product::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Available'=>'Available','Unavailable'=>'Unavailable','Just for Jenni'=>'Just for Jenni');
        $types = array('ebook'=>'ebook','program'=>'program','merchandise'=>'merchandise','mixed'=>'mixed');
        return [
            Text::make('Name')->sortable(),
            Text::make('Slug')->hideFromIndex(),
            Text::make('Short')->hideFromIndex(),
            Boolean::make('Featured')->hideFromIndex()->nullable(),
            Text::make('Stripe ID', 'stripe_id'),
            Number::make('Price')->step(0.01)->onlyOnForms(),
            Number::make('Sale Price','sale_price')->step(0.01)->onlyOnForms(),
            BelongsTo::make('Category')->hideFromIndex(),
            Select::make('Type')->options($types)->hideFromIndex(),
            Select::make('Status')->options($statuses)->sortable(),
            Text::make('Facebook Group Link', 'fbgroup')->nullable(),
            Text::make('Exerpt')->sortable()->onlyOnForms(),
            Trix::make('Description')->withFiles('public'),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('products');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('products', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('products', 'thumbnail');
            })->hideFromIndex()->deletable(false),
            HasOne::make('Intro'),
            HasMany::make('Faqs'),
            HasMany::make('Videos'),
            HasMany::make('Downloads'),
            HasMany::make('Reviews'),
            BelongsToMany::make('Testimonials'),
            BelongsToMany::make('Users'),
            BelongsToMany::make('Gifts'),
            BelongsToMany::make('Bundles')
            ->fields(function (Request $request, $model) {
                $productid = $model->id;
                return [
                    Number::make('Price', 'price')->step(0.01)
                        ->fillUsing(function ($request, $model, $attribute, $requestAttribute) use ($productid) {

                            // Retrieve the parent model (Bundle) using viaResourceId from the request
                            $bundleId = $request->input('bundles') ?? $request->route('bundles');
                            $bundle = $bundleId ? \App\Models\Bundle::find($bundleId) : null;

                            // Retrieve the pivot data for price
                            $pivotData = $request->input($requestAttribute);
                            // Retrieve the product ID
                            $productId = $productid;

                            // Check if the necessary data is available
                            if ($pivotData !== null && $bundle !== null && $productId !== null) {
                                $price = (float) $pivotData; // Ensure the pivot data is cast to a float

                                // Update the pivot table directly
                                $bundle->products()->updateExistingPivot($productId, ['price' => $price]);

                                // Update the bundle's sale price
                                $this->updateBundleSalePrice($bundle);
                            } else {
                                Log::error('Failed to retrieve necessary data for pivot update', [
                                    'bundle' => $bundle,
                                    'pivotData' => $pivotData,
                                    'productId' => $productId,
                                ]);
                            }
                        }),
                ];
            }),

        ];
    }


    protected function updateBundleSalePrice($bundle)
    {
        $totalPrice = $bundle->products()->sum('bundle_product.price');
        $bundle->sale_price = $totalPrice;
        $bundle->save();
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
