<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/addTimestampsToProductUser', [PageController::class, 'addTimestampsToProductUser'])->name('addTimestampsToProductUser');
Route::get('/shopify', function () {
    return redirect()->to('/merch');
})->name('shopify.redirect');

Route::get('/merch', [ShopifyController::class, 'index'])->name('merch');
Route::get('/shopify/callback', [ShopifyController::class, 'handleShopifyCallback'])->name('shopify.callback');
Route::get('/shopify/products', [ShopifyController::class, 'getPaginatedProducts'])->name('shopify.products');
Route::get('/merch/products/{id}', [ShopifyController::class, 'showProduct'])->name('shopify.product.show');
Route::get('/shopify/basket', [ShopifyController::class, 'cart'])->name('shopify.cart');
Route::post('/shopify/products-excluding-cart', [ShopifyController::class, 'getProductsExcludingCart']);


Route::get('/latest-smm-ebook', [PageController::class, 'latestSMMEBook'])->name('latest-smm-ebook');

Route::get('/product/simplistic-mobility-method', function () {
    return redirect()->to('/products/the-simplistic-mobility-method');
});

Route::get('/products/personalised-mobility-program', function () {
    return redirect()->to('/products/online-personal-coaching');
});

Route::get('/products/mobility-reset ', function () {
    return redirect()->to('/products/mobility-reset-aug-24');
});

Route::get('/products/aug-reset-24-smm-bundle', function(){ return redirect()->to('/bundles/aug-24-mobility-reset-smm-bundle'); });
Route::get('/products/mobility-reset-jan24', function(){ return redirect()->to('/products/mobility-reset-aug-24'); });


Route::get('/product-media', [PageController::class, 'productMedia'])->name('productMedia');
Route::get('/post-media', [PageController::class, 'postMedia'])->name('postMedia');
Route::get('/reviews-media', [PageController::class, 'reviewsMedia'])->name('reviewsMedia');

Route::get('/simplistic-mobility-method-tiktok', [PageController::class, 'smm2022tiktok'])->name('smm-2022-tiktok');
Route::get('/simplistic-mobility-method', [PageController::class, 'smm2022'])->name('smm-2022');
Route::get('/the-simplistic-mobility-method', [PageController::class, 'smmYtLp'])->name('smm-yt-lp');
Route::get('/stability-builder', [PageController::class, 'stabilityBuilder'])->name('stability-builder');
Route::get('/the-simplistic-mobility-method-usa', [PageController::class, 'smmusa'])->name('smm-usa');
Route::get('/the-simplistic-mobility-method-aus', [PageController::class, 'smmaus'])->name('smm-aus');
Route::get('/phil/the-simplistic-mobility-method', [PageController::class, 'smmPhil'])->name('smm-phil');
Route::get('/mobility-program', [PageController::class, 'mobilityProgram'])->name('mobility-program');
Route::get('/stretching-program', [PageController::class, 'stretchingProgram'])->name('stretching-program');
Route::get('/mobility-program-p', [PageController::class, 'mobilityProgramP'])->name('mobility-program-p');
Route::get('/stretching-exercises', [PageController::class, 'stretchingExcercies'])->name('stretching-exercises');
Route::get('/stretching-routine', [PageController::class, 'stretchingRoutine'])->name('stretching-routine');
Route::get('/lower-back-pain', [PageController::class, 'lowerBackReach'])->name('lower-back-reach');
Route::get('/simplistic-mobility-method-reviews', [PageController::class, 'reviews'])->name('simplistic-mobility-method-reviews');
Route::get('/share-your-journey', [PageController::class, 'shareYourJourney'])->name('share-your-journey');
Route::get('/beginner-mobility', [PageController::class, 'beginnerMobility'])->name('beginner-mobility');
Route::get('/mobility-training', [PageController::class, 'mobilityTraining'])->name('mobility-training');
Route::get('/7-days-of-awesome', [PageController::class, 'daysOfAwesome'])->name('7-days-of-awesome');
Route::get('/flong-and-sexible', [PageController::class, 'flongAndSexible'])->name('flong-and-sexible');
Route::get('/mobility-for-crossfit', [PageController::class, 'mobilityForCrossfit'])->name('mobility-for-crossfit');

Route::get('/', [PageController::class, 'welcome'])->name('welcome');
Route::get('/shop', [ShopController::class, 'newShop'])->name('shop');
Route::get('/free-videos', [PageController::class, 'freeVideos'])->name('free-videos');
Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/blog', [PageController::class, 'blog'])->name('blog');
Route::get('/blog/{slug}', [PageController::class, 'showBlog'])->name('blog.show');
Route::get('/podcast', [PodcastController::class, 'index'])->name('podcast.index');
Route::get('/podcast/fetch/{limit}', [PodcastController::class, 'fetch'])->name('podcast.fetch');
Route::get('/podcast/{podcast}/{slug}', [PodcastController::class, 'show'])->name('podcast.show');

Route::get('/facts', [PageController::class, 'facts'])->name('facts');
Route::get('/contact', function(){
    return redirect()->to('/help');
})->name('contact');
Route::get('/seminars', [PageController::class, 'seminars'])->name('seminars');
Route::get('/help', [PageController::class, 'help'])->name('help');

Route::get('/get-reviews/{limit}',[PageController::class, 'getReviews'])->name('get-reviews');
Route::get('/get-posts/{category}/{limit}', [PageController::class, 'getPosts'])->name('get-posts');

Route::get('/reassignblogs', [PageController::class, 'reassignblogs'])->name('reassignblogs');

Route::get('/list-printful-products', [PrintfulController::class, 'listProducts'])->name('list-printful-products');

// Route::get('/merch', [PrintfulController::class, 'index'])->name('merch');
// Route::get('/merch/{slug}', [PrintfulController::class, 'show'])->name('merch-show');
// Route::get('/merch/basket/add', [PrintfulController::class, 'addProductToBasket'])->name('printful-add-to-basket');
// Route::get('/remove-merch', [PrintfulController::class, 'remove'])->name('remove-merch'); 

Route::get('/questionnaire', [PageController::class, 'questionnaire'])->name('questionnaire');
Route::get('/video-call-questionnaire', [PageController::class, 'videoQuestionnaire'])->name('video-call-questionnaire');

Route::get('/products/the-simplistic-mobility-method', [PageController::class, 'programsSmm'])->name('programsSmm');
Route::get('/products/stability-builder', [PageController::class, 'programsStabilityBuilder'])->name('programsStabilityBuilder');
Route::get('/products/where-i-went-wrong-e-book', [PageController::class, 'programsWIWW'])->name('programsWIWW');
Route::get('/products/ultimate-core-seminar', [PageController::class, 'programsUltimateCore'])->name('programsUltimateCore');
Route::get('/products/splits-and-hips', [PageController::class, 'programsSplitsAndHips'])->name('programsSplitsAndHips');
Route::get('/products/barbell-basics', [PageController::class, 'programsBarbellBasics'])->name('programsBarbellBasics');
Route::get('/products/video-call', [PageController::class, 'programsVideoCall'])->name('programsVideoCall');
Route::get('/products/online-personal-coaching', [PageController::class, 'programsOnlinePersonalCoaching'])->name('programsOnlinePersonalCoaching');
Route::get('/products/end-range-training', [PageController::class, 'programsEndRangeTraining'])->name('programsEndRangeTraining');

Route::get('/products/beginners-bundle', [PageController::class, 'programsBeginnersBundle'])->name('programsBeginnersBundle');
Route::get('/products/intermediate-bundle', [PageController::class, 'programsIntermediateBundle'])->name('programsIntermediateBundle');
Route::get('/products/complete-bundle', [PageController::class, 'programsCompleteBundle'])->name('programsCompleteBundle');

Route::get('/products/{slug}', [ShopController::class, 'product'])->name('product');
Route::get('/bundles/{slug}', [ShopController::class, 'bundle'])->name('bundle');

Route::get('/basket/add/{product}', [ShopController::class, 'addProductToBasket'])->name('add-product-to-basket');
Route::get('/basket/bundles/add/{bundle}', [ShopController::class, 'addBundleToBasket'])->name('add-bundle-to-basket');
Route::get('/basket/upgrades/add/{upgrade}/{slug}', [ShopController::class, 'addUpgradeToBasket'])->name('upgrade-basket');
Route::get('/basket/upgrade-to-stability-builder', [ShopController::class, 'upgradeToStabilityBuilder'])->name('upgrade-to-sb');
Route::post('/complete-payment', [ShopController::class, 'completePayment'])->name('complete-payment');
// Route::get('/success', [ShopController::class, 'success'])->name('success');
Route::get('/update-address', [ShopController::class, 'updateAddress'])->name('update-address');
Route::post('/save-address', [ShopController::class, 'saveAddress'])->name('save-address');
Route::post('/calculate-shipping', [ShopController::class, 'calculateShipping'])->name('calculate-shipping');

// Gift Routes
Route::get('/gift-basket/add/{product}', [GiftController::class, 'addProductToBasket'])->name('add-gift-product-to-basket');
Route::get('/gift-basket/bundles/add/{bundle}', [GiftController::class, 'addBundleToBasket'])->name('add-gift-bundle-to-basket');
Route::get('/gift-basket', [GiftController::class, 'giftBasket'])->name('gift-basket');
Route::get('/remove-gift', [GiftController::class, 'remove'])->name('remove-gift');
Route::get('/empty-gift-basket', [GiftController::class, 'emptyBasket'])->name('empty-gift-basket');
Route::get('/gift-checkout', [GiftController::class, 'checkout'])->name('gift-checkout');
Route::get('/claim-your-gift/{gift}/{code}', [GiftController::class, 'claim'])->name('gift-claim');
Route::post('/gift-create-account', [GiftController::class, 'createAccount'])->name('gift-create-account');
Route::post('/complete-gift-payment', [GiftController::class, 'completePayment'])->name('gift-complete-payment');
Route::get('/gift-success', [GiftController::class, 'success'])->name('gift-success');
Route::get('/accept-gift/{gift}/{code}', [GiftController::class, 'acceptGift'])->name('accept-gift');
Route::post('/gift-basket-apply-voucher', [GiftController::class, 'applyVoucher'])->name('gift-basket-apply-voucher');
Route::post('/store-gift-details', [GiftController::class, 'storeGiftDetails'])->name('store-gift-details');
Route::get('/gift-payment-status', [GiftController::class, 'paymentStatus'])->name('gift-payment-status');

Route::get('/basket', [ShopController::class, 'basket'])->name('basket');
Route::get('/checkout', [ShopController::class, 'checkout'])->name('checkout');
Route::get('/checkout-error', [ShopController::class, 'checkoutError'])->name('checkout-error');
Route::get('/empty-basket', [ShopController::class, 'emptyBasket'])->name('empty-basket');
Route::get('/remove', [ShopController::class, 'remove'])->name('remove');
Route::post('/apply-voucher', [ShopController::class, 'applyVoucher'])->name('apply-voucher');
Route::post('/create-account', [ShopController::class, 'createAccount'])->name('create-account');
Route::get('/payment-success', [ShopController::class, 'paymentStatus'])->name('payment-status');

Route::get('/process-transaction', [ShopController::class, 'processTransaction'])->name('processTransaction');
Route::get('/success-transaction', [ShopController::class, 'successTransaction'])->name('successTransaction');
Route::get('/cancel-transaction', [ShopController::class, 'cancelTransaction'])->name('cancelTransaction');

Route::get('/process-gift-transaction', [GiftController::class, 'processGiftTransaction'])->name('processGiftTransaction');
Route::get('/success-gift-transaction', [GiftController::class, 'successGiftTransaction'])->name('successGiftTransaction');
Route::get('/cancel-gift-transaction', [GiftController::class, 'cancelGiftTransaction'])->name('cancelGiftTransaction');

Route::get('/tandcs', [PageController::class, 'tandcs'])->name('tandcs');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
Route::get('/delivery-and-returns', [PageController::class, 'deliveryReturns'])->name('delivery-and-returns');

Route::post('/contact/submit', [SendMail::class, 'enquiry'])->name('send-message');
Route::post('/support/submit', [SendMail::class, 'support'])->name('support-message');
Route::post('/share-journey/submit', [SendMail::class, 'shareJourney'])->name('share-journey-message');
Route::post('/contact/ask-a-question', [SendMail::class, 'askQuestion'])->name('ask-a-question');
Route::post('/request-seminar', [SendMail::class, 'seminar'])->name('request-seminar');
Route::post('/questionnaire/submit', [SendMail::class, 'questionnaire'])->name('questionnaire-submit');
Route::post('/video-call-questionnaire/submit', [SendMail::class, 'videoCallQuestionnaire'])->name('video-call-questionnaire-submit');

// Route::post('/payment_methods/credit_cards', [PaymentController::class, 'cardTokenization'])->name('card-tokenization');
Route::get('/video-testimonials/get/{amount}', [PageController::class, 'videoTestimonials'])->name('video-testimonials-get');

Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailing-list');
Route::post('/seven-days-mailinglist', [SendMail::class, 'sevendaysmailingListSignup'])->name('seven-days-mailinglist');

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->to('/login');
})->name('logout');
Route::get('/not-you', function () {
    Auth::logout();
    return back();
});
Auth::routes();
Route::get('/register', function () {
    return redirect()->to('/login');
});


// Route::get('/get-users-with-2', [UserController::class, 'getUsers'])->name('get-users');

// User Middleware Group
Route::group(['middleware' => 'auth'], function () {

    Route::get('/printful-sales', [PrintfulController::class, 'findSales'])->name('findSales');

    Route::get('/home', function () {
        return redirect()->to('/dashboard');
    });
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard/products/view/{slug}', [DashboardController::class, 'viewProduct'])->name('dashboard-view-product');
    Route::get('/dashboard/products/{slug}/faqs', [DashboardController::class, 'productFaqs'])->name('dashboard-product-faqs');
    Route::get('/dashboard/products/{slug}/videos', [DashboardController::class, 'productVideos'])->name('dashboard-product-videos');
    Route::get('/dashboard/videos/{video}', [DashboardController::class, 'singleVideo'])->name('dashboard-single-video');
    Route::get('/dashboard/products/{slug}/downloads/{download}', [DashboardController::class, 'productDownload'])->name('dashboard-product-downloads');
    Route::get('/dashboard/compatible-links', [DashboardController::class, 'compatible'])->name('compatible');

    Route::get('/dashboard/orders', [DashboardController::class, 'orders'])->name('orders');
    Route::get('/dashboard/support', [DashboardController::class, 'support'])->name('support');

    Route::get('/dashboard/account-management', [UserController::class, 'edit'])->name('account-management');
    Route::post('/update-account', [UserController::class, 'update'])->name('update-account');
    Route::post('/change-password', [UserController::class, 'changePassword'])->name('change-password');
    Route::post('/remove-account', [UserController::class, 'destroy'])->name('remove-account');
    Route::post('/add-info', [UserController::class, 'addInfo'])->name('add-info');
    Route::post('/toggle-follow-up', [UserController::class, 'toggleFollowUp'])->name('toggle-follow-up');

    Route::get('/dashboard/address', [UserController::class, 'address'])->name('address');
    Route::post('/edit-address', [UserController::class, 'editAddress'])->name('edit-address');
    Route::post('/add-address', [UserController::class, 'addAddress'])->name('add-address');
});

Route::get('/blog/2017-12-29/the-only-6-muscle-up-progressions-you-will-ever-need', function () { return redirect('/blog/the-only-6-muscle-up-progressions-you-will-ever-need'); });
Route::get('/blog/2017-12-31/7-real-rules-for-achieving-your-new-year-goals', function () { return redirect('/blog/7-real-rules-for-achieving-your-new-year-goals'); });
Route::get('/blog/2018-02-01/the-5-best-mobility-drills', function () { return redirect('/blog/the-5-best-mobility-drills'); });
Route::get('/blog/2017-01-31/the-ultimate-daily-mobility-routine', function () { return redirect('/blog/the-ultimate-daily-mobility-routine'); });
Route::get('/blog/2017-01-03/why-strength-programs-are-useless', function () { return redirect('/blog/why-strength-programs-are-useless'); });
Route::get('/blog/2017-01-01/2-week-training-program', function () { return redirect('/blog/2-week-training-program'); });
Route::get('/blog/2017-01-08/relatable-core-strength', function () { return redirect('/blog/relatable-core-strength'); });
Route::get('/blog/2017-01-18/how-i-invented-gymnastics', function () { return redirect('/blog/how-i-invented-gymnastics'); });
Route::get('/blog/2017-01-30/how-to-learn-on-a-budget', function () { return redirect('/blog/how-to-learn-on-a-budget'); });
Route::get('/blog/2017-02-02/lose-the-straps', function () { return redirect('/blog/lose-the-straps'); });
Route::get('/blog/2018-02-18/mobility-its-not-that-people-aren’t-doing-it-there’s-just-too-much-out-there', function () { return redirect('/blog/mobility-its-not-that-people-aren’t-doing-it-there’s-just-too-much-out-there'); });
Route::get('/blog/2017-04-04/program-with-you-in-mind', function () { return redirect('/blog/program-with-you-in-mind'); });
Route::get('/blog/2017-02-06/trade-off', function () { return redirect('/blog/trade-off'); });
Route::get('/blog/2017-02-10/party-tricks--sustainability', function () { return redirect('/blog/party-tricks--sustainability'); });
Route::get('/blog/2017-02-17/consistency-and-your-feelings', function () { return redirect('/blog/consistency-and-your-feelings'); });
Route::get('/blog/2017-02-26/perfect-alignment--the-real-world', function () { return redirect('/blog/perfect-alignment--the-real-world'); });
Route::get('/blog/2017-03-26/changing-your-own-flexibility', function () { return redirect('/blog/changing-your-own-flexibility'); });
Route::get('/blog/2017-03-29/back-pain-how-i-recovered-from-a-l4-l5-s1-disc-injury', function () { return redirect('/blog/back-pain-how-i-recovered-from-a-l4-l5-s1-disc-injury'); });
Route::get('/blog/2017-03-31/stop-wasting-your-time', function () { return redirect('/blog/stop-wasting-your-time'); });
Route::get('/blog/2018-04-02/injuries-are-awesome', function () { return redirect('/blog/injuries-are-awesome'); });
Route::get('/blog/2017-04-15/time-management-strength-or-technique', function () { return redirect('/blog/time-management-strength-or-technique'); });
Route::get('/blog/2017-04-30/specific-warm-up-time-wasting-rubbish', function () { return redirect('/blog/specific-warm-up-time-wasting-rubbish'); });
Route::get('/blog/2017-05-09/4-ways-coaches-can-prevent-their-own-injuries', function () { return redirect('/blog/4-ways-coaches-can-prevent-their-own-injuries'); });
Route::get('/blog/2017-05-06/you-have-no-idea-how-special-you-are', function () { return redirect('/blog/you-have-no-idea-how-special-you-are'); });
Route::get('/blog/2017-05-28/corrective-exercises-are-bullshit', function () { return redirect('/blog/corrective-exercises-are-bullshit'); });
Route::get('/blog/2017-05-31/the-familiarity-concept', function () { return redirect('/blog/the-familiarity-concept'); });
Route::get('/blog/2017-05-31/why-is-mobility-harder-for-me-seven-things-that-could-be-holding-you-back', function () { return redirect('/blog/why-is-mobility-harder-for-me-seven-things-that-could-be-holding-you-back'); });
Route::get('/blog/2017-06-14/what-is-the-movement-and-mobility-seminar', function () { return redirect('/blog/what-is-the-movement-and-mobility-seminar'); });
Route::get('/blog/2017-06-15/accept-your-limits-then-break-them', function () { return redirect('/blog/accept-your-limits-then-break-them'); });
Route::get('/blog/2017-07-15/your-training-atmosphere', function () { return redirect('/blog/your-training-atmosphere'); });
Route::get('/blog/2017-07-17/your-body-is-not-broken-its-confused', function () { return redirect('/blog/your-body-is-not-broken-its-confused'); });
Route::get('/blog/2017-07-23/training-vision-to-meet-your-goals', function () { return redirect('/blog/training-vision-to-meet-your-goals'); });
Route::get('/blog/2017-08-06/functional-training-isnt-functional-for-everyone', function () { return redirect('/blog/functional-training-isnt-functional-for-everyone'); });
Route::get('/blog/2017-08-31/youre-too-fat-for-gymnastics', function () { return redirect('/blog/youre-too-fat-for-gymnastics'); });
Route::get('/blog/2017-09-07/when-to-stop-measuring-your-penis', function () { return redirect('/blog/when-to-stop-measuring-your-penis'); });
Route::get('/blog/2017-09-22/smarter-muscles-yes-please', function () { return redirect('/blog/smarter-muscles-yes-please'); });
Route::get('/blog/2017-09-28/creating-your-lifting-mentality', function () { return redirect('/blog/creating-your-lifting-mentality'); });
Route::get('/blog/2017-10-23/are-you-stuck-in-a-rut', function () { return redirect('/blog/are-you-stuck-in-a-rut'); });
Route::get('/blog/2017-10-23/no-you-dont-have-a-weak-f**king-core', function () { return redirect('/blog/no-you-dont-have-a-weak-f**king-core'); });
Route::get('/blog/2017-11-07/always-evaluate-your-exercise-selection', function () { return redirect('/blog/always-evaluate-your-exercise-selection'); });
Route::get('/blog/2017-11-09/so-youre-an-old-fart-but-are-you-really-past-it', function () { return redirect('/blog/so-youre-an-old-fart-but-are-you-really-past-it'); });
Route::get('/blog/2018-12-17/sciatica---lets-kick-its-ass', function () { return redirect('/blog/sciatica---lets-kick-its-ass'); });
Route::get('/blog/2018-01-06/weights-you-cant-even-do-a-warm-up', function () { return redirect('/blog/weights-you-cant-even-do-a-warm-up'); });
Route::get('/blog/2018-02-20/identify-and-eliminate-your-shoulder-problems', function () { return redirect('/blog/identify-and-eliminate-your-shoulder-problems'); });
Route::get('/blog/2018-05-20/does-deadlifting-hurt-your-back', function () { return redirect('/blog/does-deadlifting-hurt-your-back'); });
Route::get('/blog/2018-09-04/essential-flexibility-requirements-for-everyone-yes-that-means-you-too', function () { return redirect('/blog/essential-flexibility-requirements-for-everyone-yes-that-means-you-too'); });
Route::get('/blog/2018-11-11/3-training-mistakes-you-should-avoid', function () { return redirect('/blog/3-training-mistakes-you-should-avoid'); });
Route::get('/blog/2018-12-06/3-mandatory-accessory-exercises-for-strength-athletes', function () { return redirect('/blog/3-mandatory-accessory-exercises-for-strength-athletes'); });
Route::get('/blog/2018-12-19/the-complete-hip-conundrum', function () { return redirect('/blog/the-complete-hip-conundrum'); });
Route::get('/blog/2019-01-04/getting-strong-with-hypermobility', function () { return redirect('/blog/getting-strong-with-hypermobility'); });
Route::get('/blog/2019-01-22/the-dangers-of-misinterpreting-coaching-cues', function () { return redirect('/blog/the-dangers-of-misinterpreting-coaching-cues'); });
Route::get('/blog/2019-02-04/forever-injured-you-might-be-a-big-strong-weakling', function () { return redirect('/blog/forever-injured-you-might-be-a-big-strong-weakling'); });
Route::get('/blog/2019-02-20/you-are-probably-fine', function () { return redirect('/blog/you-are-probably-fine'); });
Route::get('/blog/2019-03-20/5-common-training-questions-answered-with-common-sense', function () { return redirect('/blog/5-common-training-questions-answered-with-common-sense'); });
Route::get('/blog/2019-05-21/is-resting-actually-helpful-for-pain', function () { return redirect('/blog/is-resting-actually-helpful-for-pain'); });
Route::get('/blog/2019-06-18/the-dominant-muscle-debunk', function () { return redirect('/blog/the-dominant-muscle-debunk'); });
Route::get('/blog/2019-07-25/are-you-a-symptom-chaser', function () { return redirect('/blog/are-you-a-symptom-chaser'); });
Route::get('/blog/2019-08-26/spend-time-with-the-kowledge-you-have', function () { return redirect('/blog/spend-time-with-the-kowledge-you-have'); });
Route::get('/blog/2019-09-08/3-tips-to-stop-joint-pain', function () { return redirect('/blog/3-tips-to-stop-joint-pain'); });
Route::get('/blog/2019-09-29/can-you-pass-the-deep-lunge-test', function () { return redirect('/blog/can-you-pass-the-deep-lunge-test'); });
Route::get('/blog/2019-10-16/phils-story-from-scared-to-move-to-getting-his-life-back-in-3-weeks', function () { return redirect('/blog/phils-story-from-scared-to-move-to-getting-his-life-back-in-3-weeks'); });
Route::get('/blog/2019-10-31/can-you-work-out-with-a-herniated-disc', function () { return redirect('/blog/can-you-work-out-with-a-herniated-disc'); });
Route::get('/blog/2019-11-04/easy-stretches-for-the-inflexible', function () { return redirect('/blog/easy-stretches-for-the-inflexible'); });
Route::get('/blog/2019-11-16/got-shoulder-impingement-do-these-2-exercises', function () { return redirect('/blog/got-shoulder-impingement-do-these-2-exercises'); });
Route::get('/blog/2019-11-25/why-better-movement-will-change-your-life', function () { return redirect('/blog/why-better-movement-will-change-your-life'); });
Route::get('/blog/2019-11-28/3-steps-to-bulletproof-knees', function () { return redirect('/blog/3-steps-to-bulletproof-knees'); });
Route::get('/blog/2019-12-09/the-ultimate-stretching-routine', function () { return redirect('/blog/the-ultimate-stretching-routine'); });
Route::get('/blog/2019-12-18/the-greatest-rotator-cuff-exercise', function () { return redirect('/blog/the-greatest-rotator-cuff-exercise'); });
Route::get('/blog/2020-01-09/a-guide-to-squatting-high-bar-low-bar-foot-placement-and-more', function () { return redirect('/blog/a-guide-to-squatting-high-bar-low-bar-foot-placement-and-more'); });
Route::get('/blog/2020-01-16/is-your-piriformis-really-to-blame-for-your-pain', function () { return redirect('/blog/is-your-piriformis-really-to-blame-for-your-pain'); });
Route::get('/blog/2020-02-13/how-to-analyse-your-deadlift-check-your-technique--flexibility', function () { return redirect('/blog/how-to-analyse-your-deadlift-check-your-technique--flexibility'); });
Route::get('/blog/2020-02-21/how-to-fix-a-winged-scapula', function () { return redirect('/blog/how-to-fix-a-winged-scapula'); });
Route::get('/blog/2020-03-06/how-to-stop-tendonitis', function () { return redirect('/blog/how-to-stop-tendonitis'); });
Route::get('/blog/2020-03-17/2020-in-2020-how-your-vision-can-be-trained-and-why-you-should-care', function () { return redirect('/blog/2020-in-2020-how-your-vision-can-be-trained-and-why-you-should-care'); });
Route::get('/blog/2020-03-20/bodyweight-strength-at-home', function () { return redirect('/blog/bodyweight-strength-at-home'); });
Route::get('/blog/2020-03-25/3-reasons-you-should-stretch-and-move-every-day', function () { return redirect('/blog/3-reasons-you-should-stretch-and-move-every-day'); });
Route::get('/blog/2020-04-03/should-you-stretch-if-youre-already-flexible', function () { return redirect('/blog/should-you-stretch-if-youre-already-flexible'); });
Route::get('/blog/2020-04-10/the-best-way-to-increase-your-flexibility', function () { return redirect('/blog/the-best-way-to-increase-your-flexibility'); });
Route::get('/blog/2020-04-17/dont-run-into-running-problems', function () { return redirect('/blog/dont-run-into-running-problems'); });
Route::get('/blog/2020-04-24/why-your-neck-is-so-stiff---and-how-to-fix-it', function () { return redirect('/blog/why-your-neck-is-so-stiff---and-how-to-fix-it'); });
Route::get('/blog/2020-05-01/the-myth-of-better-ankle-flexibility', function () { return redirect('/blog/the-myth-of-better-ankle-flexibility'); });
Route::get('/blog/2020-05-29/simplifying-core-strength', function () { return redirect('/blog/simplifying-core-strength'); });
Route::get('/blog/2020-06-27/why-do-muscle-imbalances-happen-and-what-can-you-do-about-them', function () { return redirect('/blog/why-do-muscle-imbalances-happen-and-what-can-you-do-about-them'); });
Route::get('/blog/2020-08-12/does-your-si-joint-feel-weird', function () { return redirect('/blog/does-your-si-joint-feel-weird'); });
Route::get('/blog/2020-09-25/is-sitting-too-much-really-ruining-your-mobility', function () { return redirect('/blog/is-sitting-too-much-really-ruining-your-mobility'); });
Route::get('/blog/2020-12-25/the-things-you-tell-yourself', function () { return redirect('/blog/the-things-you-tell-yourself'); });
Route::get('/blog/2021-02-13/should-you-foam-roll', function () { return redirect('/blog/should-you-foam-roll'); });
Route::get('/blog/2021-03-26/how-long-does-it-take-to-get-flexible', function () { return redirect('/blog/how-long-does-it-take-to-get-flexible'); });
Route::get('/blog/2021-06-07/how-to-fix-constantly-tight-hamstrings', function () { return redirect('/blog/how-to-fix-constantly-tight-hamstrings'); });
Route::get('/blog/2021-06-18/why-your-squat-depth-is-the-way-it-is', function () { return redirect('/blog/why-your-squat-depth-is-the-way-it-is'); });
Route::get('/blog/2021-06-30/the-best-sleeping-position-sleeping-shouldnt-hurt-you', function () { return redirect('/blog/the-best-sleeping-position-sleeping-shouldnt-hurt-you'); });
Route::get('/blog/2021-07-06/the-anatomical-position', function () { return redirect('/blog/the-anatomical-position'); });
Route::get('/blog/2021-07-07/anatomical-directions', function () { return redirect('/blog/anatomical-directions'); });
Route::get('/blog/2021-07-08/anatomical-movements', function () { return redirect('/blog/anatomical-movements'); });
Route::get('/blog/2021-07-09/anatomical-planes', function () { return redirect('/blog/anatomical-planes'); });
Route::get('/blog/2021-07-10/origin--insertion', function () { return redirect('/blog/origin--insertion'); });
Route::get('/blog/2021-07-11/bilateral--unilateral', function () { return redirect('/blog/bilateral--unilateral'); });
Route::get('/blog/2021-07-12/flexion--extension-in-detail', function () { return redirect('/blog/flexion--extension-in-detail'); });
Route::get('/blog/2021-07-13/the-rotator-cuff', function () { return redirect('/blog/the-rotator-cuff'); });
Route::get('/blog/2021-08-16/why-do-my-joints-pop-and-click', function () { return redirect('/blog/why-do-my-joints-pop-and-click'); });