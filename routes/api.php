<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get-free-videos/{offset}/{limit}', [PageController::class, 'getFreeVideos'])->name('get-free-videos');
Route::get('/get-fact', [PageController::class, 'getFact'])->name('get-fact');
Route::get('/seminars/get', [SeminarController::class, 'getSeminars'])->name('get-seminars');
Route::get('/products/get/{category}/{limit}', [ShopController::class, 'getProducts'])->name('get-products');
Route::get('/products/get/bundles', [ShopController::class, 'getBundles'])->name('get-bundles');
Route::get('/printful-webhook', [PrintfulController::class, 'handleWebhook'])->name('handle-webhook');

Route::get('/sales/get-sales', [SalesController::class, 'getSales'])->name('get-sales');
Route::get('/sales/get-totals', [SalesController::class, 'getTotals'])->name('get-totals');
Route::get('/sales/get-top-countries/{days}', [SalesController::class, 'getTopCountries'])->name('get-top-countries');
Route::get('/sales/get-product-data/{days}', [SalesController::class, 'getProductData'])->name('get-product-data');